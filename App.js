import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  BackHandler,
  Alert,
  DeviceEventEmitter,
  Linking,
  Text,
  TextInput,
  StatusBar
} from 'react-native';

import {
  isFirstTime,
  isRolledBack,
  packageVersion,
  currentVersion,
  checkUpdate,
  downloadUpdate,
  switchVersion,
  switchVersionLater,
  markSuccess,
} from 'react-native-update';


import { Input } from 'react-native-elements'
import NetInfo from '@react-native-community/netinfo'
import SplashScreen from 'react-native-splash-screen';
import { Provider } from 'mobx-react';
import Toast from 'react-native-easy-toast';
//import stores from './App/chart/store';
import AppNavigator from './App/AppNavigator';
import JPush from 'jpush-react-native';
import Url from './App/Url/Url';
import { ToDay } from './App/CommonComponents/Data';
import _ from 'lodash'

import PushUtil from './App/CommonComponents/PushUtil'

import _updateConfig from './update.json';
import { PermissionsAndroid } from 'react-native';
const { appKey } = _updateConfig[Platform.OS];

import { deviceHeight, deviceWidth } from './App/Url/Pixal';
console.log("🚀 ~ file: App.js ~ line 40 ~ deviceWidth", deviceWidth)
console.log("🚀 ~ file: App.js ~ line 40 ~ deviceHeight", deviceHeight)

//console.log = () => { }

//Input.defaultProps = Object.assign({}, TextInput.defaultProps, { defaultProps: false })

const handleConnectivityChange = () => {
  NetInfo.isConnected.fetch().then((isConnected) => {
    if (!isConnected) {
      Alert.alert('提示', '网络已断开，请连接网络重试');
    }
  });
};


class App extends Component {
  constructor() {
    super();
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.onBackButtonPressAndroid,
    );
    NetInfo.isConnected.addEventListener(
      'connectionChange',
      handleConnectivityChange,
    );
  }

  componentWillMount() {
    console.log("🚀 ~ file: App.js:76 ~ App ~ componentWillMount ~ componentWillMount:")
    //JPush.setLoggerEnable(true);
    StatusBar.setTranslucent(false)
    StatusBar.setHidden(false)
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.onBackButtonPressAndroid,
    );
    NetInfo.isConnected.removeEventListener(
      'connectionChange',
      handleConnectivityChange,
    );
  }

  onBackButtonPressAndroid = () => true

  doUpdate = async (info) => {
    try {
      const hash = downloadUpdate(info);
      switchVersionLater(hash);
    } catch (err) {
      Alert.alert('提示', JSON.parse(err));
    }
  };

  checkUpdate = async () => {
    if (__DEV__) {
      // 开发模式不支持热更新，跳过检查
      return;
    }
    let info;
    try {
      info = await checkUpdate(appKey);
    } catch (err) {
      console.warn(err);
      return;
    }
    if (info.expired) {
      Alert.alert('提示', '您的应用版本已更新,请前往应用商店下载新的版本', [
        {
          text: '确定', onPress: () => { info.downloadUrl && Linking.openURL(info.downloadUrl) }
        },
        { text: '下次更新' },
      ]);
    } else if (info.upToDate) {
      //Alert.alert('提示', '您的应用版本已是最新.');
    } else {
      Alert.alert('提示', '检查到新的版本' + info.name + ',是否下载?\n' + info.description, [
        { text: '是', onPress: () => { this.doUpdate(info) } },
        { text: '否', },
      ]);
    }
  };

  checkUpdate1 = () => {
    checkUpdate(appKey).then(info => {
      console.log(info)
      if (info.expired) {
        Alert.alert('提示', '您的应用版本已更新,请前往应用商店下载新的版本', [
          { text: '确定', onPress: () => { info.downloadUrl && Linking.openURL(info.downloadUrl) } },
          { text: '下次更新' },
        ]);
      } else if (info.upToDate) {
        //Alert.alert('提示', '您的应用版本是' + packageVersion + '\n已是最新版本');
      } else {
        //this.doUpdate(info)
      }
    }).catch(err => {
      console.log("🚀 ~ file: App.js ~ line 145 ~ App ~ checkUpdate ~ err", err)
      //Alert.alert('更新提示', JSON.stringify(err));
    });
  };

  addUMNotificationListener = (result) => {
    console.log("🚀 ~ file: App.js ~ line 153 ~ App ~ PushUtil.addUMNotificationListener")
    console.log("🚀 ~ file: App.js ~ line 155 ~ App ~ PushUtil.result", result)

  }

  UMConnectEventListener = (callback) => {
    this.UMNoticeListener = DeviceEventEmitter.addListener("UMNoticeListener", result => {
      callback(result)
    })
  }

  componentDidMount() {
    //markSuccess()
    setTimeout(() => { SplashScreen.hide() }, 2500)  // 需要隐藏，时间可以设置，默认1s
    StatusBar.setBarStyle('light-content')
    JPush.setLoggerEnable(true)
    JPush.init();

    //this.UMConnectEventListener(this.addUMNotificationListener)

    this.UMNoticeListener = DeviceEventEmitter.addListener("UMNoticeListener1", this.addUMNotificationListener);

    this.checkUpdate1();

    //连接状态
    this.connectListener111 = (result) => {
      console.log("connectListener:" + JSON.stringify(result))
    }
    JPush.addConnectEventListener(this.connectListener111);
    console.log('connectListener111')
    //tag alias事件回调
    this.tagAliasListener = result => {
      console.log("tagAliasListener:" + JSON.stringify(result))
    };
    JPush.addTagAliasListener(this.tagAliasListener);

    this.notificationListener = result => {
      console.log("notificationListener:" + JSON.stringify(result))
      /*  if (result.notificationEventType == 'notificationArrived') {
         if (result.content.indexOf('日报') != -1 || result.content.indexOf('周报') != -1 || result.content.indexOf('月报') != -1) {
           this.customMessageListener(result);
         }
       } */
    }
    JPush.addNotificationListener(this.notificationListener);

    JPush.addCustomMessagegListener(this.customMessageListener);

    const granted = PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      //PermissionsAndroid.PERMISSIONS.Get_MAC,
      {
        //第一次请求【拒绝】后提示用户你为什么要这个权限
        'title': '需要使用读写权限',
        'message': '需要读写权限才可正常工作'
      }
    );
  }
  componentWillUnmount() {
    JPush.removeListener()
    this.UMNoticeListener.remove();
  }

  GetUnreadMassage = () => {
    const url = Url.url + Url.factory + 'facId/' + Url.Fid + 'recId/' + Url.EmployeeId + '/GetUnSeenMsgListApp'
    console.log("Login -> url", url)
    fetch(url).then(res => {
      console.log(res.status);
      return res.json()
    }).then(resData => {
      console.log("App -> resData.length", resData.length)
      DeviceEventEmitter.emit('noticecount', resData.length);
    }).catch((err) => {
      console.log("Login -> err", err)
    })
  }


  customMessageListener = result => {
    console.log("customMessageListener:" + JSON.stringify(result))
    Url.NoticeList.push(result);
    DeviceEventEmitter.emit('noticecount', Url.NoticeList.length);
    /* let myDate = new Date();
    let time = myDate.getHours() + ':' + myDate.getMinutes() + ':' + myDate.getSeconds()
    result._createtime = ToDay + ' ' + time
    if (result.content.indexOf("：") != -1 && result.content.indexOf("-->") != -1) {
      console.log('1')
      arr1 = result.content.split("：");
      result.username = arr1[0]
      arr2 = arr1[1].split(" --> ");
      console.log(arr2)
      result.bizName = arr2[0]
      result.content = arr2[1]
      console.log("notificationListener:" + JSON.stringify(result))
      Url.NoticeList.push(result);
    } else if (result.content.indexOf("-->") != -1) {
      console.log('2')
      arr = result.content.split(" --> ");
      console.log(arr)
      result.bizName = arr[0]
      result.content = arr[1]
      Url.NoticeList.push(result);
    } else if (result.content.indexOf("：") != -1) {
      console.log('3')
      arr = result.content.split("：");
      result.username = arr[0]
      result.content = arr[1]
      Url.NoticeList.push(result);
    } else {
      console.log('4')
      
    }

    DeviceEventEmitter.emit('noticecount', Url.NoticeList.length);

    console.log("notificationList:" + JSON.stringify(Url.NoticeList))
    this.setState({ newMessage: true, navi: result.content }) */
  }

  notificationListener1 = result => {
    console.log("notificationListenerOriginal:" + JSON.stringify(result))
    this.GetUnreadMassage();
    /* let myDate = new Date();
    let time = myDate.getHours() + ':' + myDate.getMinutes() + ':' + myDate.getSeconds()
    result._createtime = ToDay + ' ' + time
    if (result.content.indexOf(":") != -1 && result.content.indexOf("-->") != -1) {
      console.log('1')
      arr1 = result.content.split(":");
      result.username = arr1[0]
      arr2 = arr1[1].split(" --> ");
      console.log(arr2)
      result.bizName = arr2[0]
      result.content = arr2[1]
      console.log("notificationListener:" + JSON.stringify(result))
      Url.NoticeList.push(result);
    } else if (result.content.indexOf("-->") != -1) {
      console.log('2')
      arr = result.content.split(" --> ");
      console.log(arr)
      result.bizName = arr[0]
      result.content = arr[1]
      Url.NoticeList.push(result);
    } else if (result.content.indexOf(":") != -1) {
      console.log('3')
      arr = result.content.split(":");
      result.username = arr[0]
      result.content = arr[1]
      Url.NoticeList.push(result);
    } else {
      console.log('4')
      Url.NoticeList.push(result);
    }

    DeviceEventEmitter.emit('noticecount', Url.NoticeList.length);

    console.log("notificationList:" + JSON.stringify(Url.NoticeList))
    this.setState({ newMessage: true, navi: result.content }) */
  }

  render() {
    return (
      <Provider >
        <AppNavigator />
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

export default App;
