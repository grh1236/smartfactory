package com.smartfactory;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import androidx.annotation.Nullable;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

public class PDAScanModule extends ReactContextBaseJavaModule {
    private static ReactApplicationContext reactContext;

    private BroadcastReceiver iDataScanReceiver;
    private IntentFilter iDataIntentFilter;

    private BroadcastReceiver iDataScanReceiver_XDL;
    private IntentFilter iDataIntentFilter_XDL;

    private static final String DURATION_SHORT_KEY = "SHORT";
    private static final String DURATION_LONG_KEY = "LONG";

    /**
     * 扫描条码服务广播-销邦
     */
    public static final String SCN_CUST_ACTION_SCODE = "com.android.server.scannerservice.broadcast";
    /**
     * 条码扫描数据广播-销邦
     */
    public static final String SCN_CUST_EX_SCODE = "scannerdata";

    /**
     * 扫描条码服务广播-新大陆
     */
    public static final String SCN_CUST_ACTION_SCODE_XDL = "nlscan.action.SCANNER_RESULT";
    /**
     * 条码扫描数据广播-新大陆
     */
    public static final String SCN_CUST_EX_SCODE_XDL = "SCAN_BARCODE1";

    public static final String SCN_CUST_ACTION_START = "android.intent.action.SCANNER_BUTTON_DOWN";
    public static final String SCN_CUST_ACTION_CANCEL = "android.intent.action.SCANNER_BUTTON_UP";

    public static final String SCN_CUST_ACTION_START_XDL = "nlscan.action.SCANNER_TRIG";
    public static final String SCN_CUST_ACTION_CANCEL_XDL = "nlscan.action.STOP_SCAN";



    public PDAScanModule(ReactApplicationContext context) {
        super(context);
        reactContext = context;
        initScan();
        Intent intent = new Intent ("ACTION_BAR_SCANCFG");
        intent.putExtra("EXTRA_SCAN_MODE", 3);
        intent.putExtra("EXTRA_OUTPUT_EDITOR_ACTION_ENABLE", 0);
        intent.putExtra("EXTRA_OUTPUT_EDITOR_ACTION", 6);
        getReactApplicationContext().sendBroadcast(intent);
    }

    @Override
    public String getName() {
        return "PDAScan";
    }

    //通过RCTDeviceEventEmitter发送消息到js端
    private void sendEvent(ReactContext reactContext, String eventName, @Nullable WritableMap params) {
        reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }

    /*
     * 调用扫码头
     * */
    private void initScan(){
        //扫描结果的意图过滤器的动作一定要使用"android.intent.action.SCANRESULT"
        iDataIntentFilter = new IntentFilter(SCN_CUST_ACTION_SCODE);
        //注册广播接收者
        iDataScanReceiver = new ScannerResultReceiver();
        getReactApplicationContext().registerReceiver(iDataScanReceiver, iDataIntentFilter);

        //扫描结果的意图过滤器的动作一定要使用"android.intent.action.SCANRESULT"
        iDataIntentFilter_XDL = new IntentFilter(SCN_CUST_ACTION_SCODE_XDL);
        //注册广播接收者
        iDataScanReceiver_XDL = new mSamDataReceiver_xdl();
        getReactApplicationContext().registerReceiver(iDataScanReceiver_XDL, iDataIntentFilter_XDL);
    }

    private class ScannerResultReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(SCN_CUST_ACTION_SCODE)){
                String scanMessage;
                try {
                    scanMessage = intent.getStringExtra(SCN_CUST_EX_SCODE);
                    WritableMap params = Arguments.createMap();
                    if(scanMessage.length() == 16) {
                        scanMessage = scanMessage.substring(3);
                    };
                    params.putString("ScanResult", scanMessage);
                    Log.e("ScanMessage",scanMessage);
                    sendEvent(getReactApplicationContext(), "iDataScan", params);
                } catch (Exception exception) {
                    // TODO: handle exception
                    Log.e("ScanMessage","内部扫描广播接收器异常");
                }
            }
        }
    }

    private class mSamDataReceiver_xdl extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(SCN_CUST_ACTION_SCODE_XDL)){
                String scanMessage;
                try {
                    scanMessage = intent.getStringExtra(SCN_CUST_EX_SCODE_XDL);
                    WritableMap params = Arguments.createMap();
                    if(scanMessage.length() == 16) {
                        scanMessage = scanMessage.substring(3);
                    }
                    params.putString("ScanResult", scanMessage);
                    Log.e("ScanMessage",scanMessage);
                    sendEvent(getReactApplicationContext(), "iDataScan", params);
                } catch (Exception exception) {
                    // TODO: handle exception
                    Log.e("ScanMessage","内部扫描广播接收器异常");
                }
            }
        }
    }

    private void enablePlayBeep(boolean enable){
        Intent intent = new Intent("android.intent.action.BEEP");
        intent.putExtra("android.intent.action.BEEP", enable);
        getReactApplicationContext().sendBroadcast(intent);
    }

    /**
     //     * @描述: 打开扫描器
     //     * @创建人 chezhenlong
     //     * @创建时间 2020/7/21
     //     */
    @ReactMethod
    public void onScan() {
        Intent scannerIntent = new Intent(SCN_CUST_ACTION_START);
        getReactApplicationContext().sendBroadcast(scannerIntent);
        Intent scannerIntent_xdl = new Intent(SCN_CUST_ACTION_START_XDL);
        getReactApplicationContext().sendBroadcast(scannerIntent_xdl);
    }

    /**
     * @描述: 关闭扫描器
     * @创建人 chezhenlong
     * @创建时间 2020/7/21
     */
    @ReactMethod
    public void offScan() {
        Intent scannerIntent = new Intent(SCN_CUST_ACTION_CANCEL);
        getReactApplicationContext().sendBroadcast(scannerIntent);
        //readerManager.stopScanAndDecode();
        Intent scannerIntent_xdl = new Intent(SCN_CUST_ACTION_CANCEL_XDL);
        getReactApplicationContext().sendBroadcast(scannerIntent_xdl);
    }

}