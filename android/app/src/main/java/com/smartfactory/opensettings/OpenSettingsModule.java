package com.smartfactory.opensettings;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReactContextBaseJavaModule;


public class OpenSettingsModule extends ReactContextBaseJavaModule {

    /* constructor */
    public OpenSettingsModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        /**
         * return the string name of the NativeModule which represents this class in JavaScript
         * In JS access this module through React.NativeModules.OpenSettings
         */
        return "OpenSettings";
    }

    @ReactMethod
    public void openNetworkSettings(Callback cb) {
        Activity currentActivity = getCurrentActivity();
        Log.e("Opensettings:",currentActivity.getPackageName());
        Log.e("Opensettings:", String.valueOf(currentActivity.getApplicationInfo().uid));
        if (currentActivity == null) {
            cb.invoke(false);
            return;
        }
        Intent localIntent = new Intent();
        localIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (Build.VERSION.SDK_INT >= 9) {
            localIntent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
            localIntent.setData(Uri.fromParts("package", currentActivity.getPackageName(), null));
        } else if (Build.VERSION.SDK_INT <= 8) {
            localIntent.setAction(Intent.ACTION_VIEW);
            localIntent.setClassName("com.android.settings", "com.android.settings.InstalledAppDetails");
            localIntent.putExtra("com.android.settings.ApplicationPkgName", currentActivity.getPackageName());
        }
        currentActivity.startActivity(localIntent);
//        try {
//            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                Intent intent = new Intent();
//                intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
//                intent.putExtra("app_package", currentActivity.getPackageName());
//                intent.putExtra("app_uid", currentActivity.getApplicationInfo().uid);
//                currentActivity.startActivity(intent);
//            } else if (android.os.Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
//                Intent intent = new Intent();
//                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//                intent.addCategory(Intent.CATEGORY_DEFAULT);
//                intent.setData(Uri.parse("package:" + currentActivity.getPackageName()));
//                currentActivity.startActivity(intent);
//            }
//            } catch (Exception e) {
//            cb.invoke(e.getMessage());
//        }


//        if (currentActivity == null) {
//            cb.invoke(false);
//            return;
//        }
//        try {
//            currentActivity.startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS));
//            cb.invoke(true);
//        } catch (Exception e) {
//            cb.invoke(e.getMessage());
//        }
    }




}


