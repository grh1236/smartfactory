package com.smartfactory;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Parcelable;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.nlscan.android.uhf.TagInfo;
import com.nlscan.android.uhf.UHFManager;
import com.nlscan.android.uhf.UHFReader;

import java.util.Arrays;

public class RFIDModule extends ReactContextBaseJavaModule {

    private static ReactApplicationContext mreactContext;
    private UHFManager mUHFMgr;

    private IntentFilter iRFIDIntentFilter_SDL;
    private BroadcastReceiver iRFIDReceiver_SDL;

    /**读码结果发送的广播action*/
    public static final String RFID_ACTION_CODE_SDL = "nlscan.intent.action.uhf.ACTION_RESULT";
    /**读码结果发送的广播Extra*/
    public final static String EXTRA_TAG_INFO = "tag_info";


    public RFIDModule(@NonNull ReactApplicationContext reactContext) {
        super(reactContext);
        mreactContext = reactContext;
        initRFID();
    }



    @Override
    public String getName() {
        return "RFIDModule";
    }

    //重新检测模块
    private void reLoadModule()
    {
        synchronized (this) {
            new Thread(new Runnable() {

                @Override
                public void run() {
                    mUHFMgr = UHFManager.getInstance();
                    mUHFMgr.loadUHFModule();
                    //mHandler.sendEmptyMessage(MSG_LOAD_MODULE_COMPLETED);
                }
            }).start();
        }
    }

    //通过RCTDeviceEventEmitter发送消息到js端
    public static void sendEvent(ReactContext reactContext, String eventName, @Nullable WritableMap params) {
        reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }

    @ReactMethod
    public void isPowerOn() {
        WritableMap params = Arguments.createMap();
        try {
            mUHFMgr = UHFManager.getInstance();
            boolean isOn = mUHFMgr.isPowerOn();
            Log.e("RFIDMoudle", "isPowerOn: " + String.valueOf(isOn));
            params.putString("isPowerOn", String.valueOf(isOn));
            sendEvent(mreactContext, "isPowerOn", params);
        } catch (Exception err) {
            Log.e("RFID_ERR", err.toString());
            params.putString("isPowerOn", "false");
            sendEvent(mreactContext, "isPowerOn", params);
        }

    }

    private void initRFID(){
        iRFIDIntentFilter_SDL = new IntentFilter(RFID_ACTION_CODE_SDL);
        iRFIDReceiver_SDL = new iRFIDReceiver_SDL();
        getReactApplicationContext().registerReceiver(iRFIDReceiver_SDL, iRFIDIntentFilter_SDL);
    }

    @ReactMethod
    public void startTag() {
        mUHFMgr = UHFManager.getInstance();
        UHFReader.READER_STATE er = mUHFMgr.startTagInventory();
    }

    @ReactMethod
    public void endTag() {
        mUHFMgr = UHFManager.getInstance();
        UHFReader.READER_STATE er = mUHFMgr.stopTagInventory();
    }

    private class iRFIDReceiver_SDL extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(RFID_ACTION_CODE_SDL)){
                String RFIDMessage;
                try {
                    Parcelable[] tagInfos =  intent.getParcelableArrayExtra(EXTRA_TAG_INFO);
                    Log.e("RFIDtaginfos", "tagInfos: " + Arrays.toString(tagInfos));
                    for(Parcelable parcel : tagInfos)
                    {
                        TagInfo tagInfo = (TagInfo)parcel;
                        String EpcId = String.valueOf(UHFReader.bytes_Hexstr(tagInfo.EpcId));
                        Log.d("TAG","Epc ID : " + UHFReader.bytes_Hexstr(tagInfo.EpcId));
                        WritableMap params = Arguments.createMap();
                        params.putString("RFIDResult", EpcId);
                        sendEvent(getReactApplicationContext(), "iDataRFID", params);
                    }
                } catch (Exception exception) {
                    // TODO: handle exception
                    Log.e("RFIDMessage","内部扫描广播接收器异常");
                }
            }
        }
    }


    @ReactMethod
    public void readTag() {
        byte[] rdata = null;
        mUHFMgr = UHFManager.getInstance();
        int bank = UHFReader.BANK_TYPE.EPC.value();//EPC 分区
        String sHexPasswd = null;
        sHexPasswd = "";
        rdata = mUHFMgr.GetTagData(bank, 2, 8, "");
        String val = null;
        if (rdata != null) val =UHFReader.Hex2Str(rdata);
        Log.e("RFIDMoudle", "rdata: " + String.valueOf(rdata));
        Log.e("RFIDMoudle", "rdata_val: " + String.valueOf(val));
        WritableMap paramsRdata = Arguments.createMap();
        paramsRdata.putString("iRdataResult", val);
        sendEvent(mreactContext, "iRdata", paramsRdata);
    }

    @ReactMethod
    public void writeTag(String rdata) {
        Log.e("RFIDMoudle", "writeTag_rdata: " + rdata);
        mUHFMgr = UHFManager.getInstance();
        WritableMap paramsWdata = Arguments.createMap();
        UHFReader.Lock_Obj lobj = UHFReader.Lock_Obj.LOCK_OBJECT_BANK1; //EPC分区;
        String sHexPasswd = "00000000";//访问密码(16进制串)
//        //解锁标签
//        UHFReader.Lock_Type ultyp= UHFReader.Lock_Type.UNLOCK; //解锁定
//        String sHexPasswd = "00000000";//访问密码(16进制串)
//        UHFReader.READER_STATE  uer = mUHFMgr.LockTag(lobj.value(), ultyp.value(),
//                sHexPasswd);
//        if( uer == UHFReader.READER_STATE. OK_ERR) {
//            Log.e("RFIDMoudle", "unlockData_success: " + uer.toString());
//        } else {
//            paramsWdata.putString("iWdataResult", "解锁失败_" + uer.toString());
//            Log.e("RFIDMoudle", "lockData_failed: " + uer.toString());
//            sendEvent(mreactContext, "iWdata", paramsWdata);
//            return;
//        }
        //写标签
        String NewPasswd = "12345678";
        byte[] NewPasswdData=null;
        NewPasswdData = UHFReader.Str2Hex(NewPasswd);//写入的数据（16进制转byte[]）
        int bank = UHFReader.BANK_TYPE.RESERVED.value();//保留区 分区
        UHFReader.READER_STATE er = mUHFMgr.writeTagData(bank, 2, NewPasswdData, sHexPasswd);
        if( er == UHFReader.READER_STATE.OK_ERR) {
            paramsWdata.putString("iWdataResult", "写入成功");
            Log.e("RFIDMoudle", "wData_success: " + er.toString());
        } else {
            paramsWdata.putString("iWdataResult", "写入失败_" + er.toString());
            Log.e("RFIDMoudle", "wData_failed: " + er.toString());
            sendEvent(mreactContext, "iWdata", paramsWdata);
            return;
        }
        //----直接写标签EPC数据
        byte[]data = UHFReader.Str2Hex(rdata);//数据（16进制转byte[]）
        //byte[]data = UHFReader.Str2Hex("F3C7A5E059FA4C94B308A0B5ED6391A2");//数据（16进制转byte[]）
//        String sHexPasswd = "00000001";//访问密码（16进制串），如果没有访问密码设置null
        UHFReader.READER_STATE  erEPC = mUHFMgr.writeTagEpcEx(data, NewPasswd);
        if( erEPC == UHFReader.READER_STATE.OK_ERR) {
            paramsWdata.putString("iWdataResult", "写入EPC成功");
            Log.e("RFIDMoudle", "wDataEPC_success: " + erEPC.toString());
        } else {
            paramsWdata.putString("iWdataResult", "写入EPC失败_" + er.toString());
            Log.e("RFIDMoudle", "wDataEPC_failed: " + erEPC.toString());
            sendEvent(mreactContext, "iWdata", paramsWdata);
            return;
        }
        //锁标签
        UHFReader.Lock_Type ltyp= UHFReader.Lock_Type.LOCK; //暂时锁定
        //String UsHexPasswd = "12345678";//访问密码(16进制串)
        UHFReader.READER_STATE erLock = mUHFMgr.LockTag(lobj.value(), ltyp.value(), NewPasswd);
        if( erLock == UHFReader.READER_STATE. OK_ERR) {
            Log.e("RFIDMoudle", "lockData_success: " + erLock.toString());
        } else {
            paramsWdata.putString("iWdataResult", "锁定失败_" + erLock.toString());
            Log.e("RFIDMoudle", "lockData_failed: " + erLock.toString());
            sendEvent(mreactContext, "iWdata", paramsWdata);
            return;
        }


        sendEvent(mreactContext, "iWdata", paramsWdata);
    }
}