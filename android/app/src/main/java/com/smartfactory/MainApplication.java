package com.smartfactory;

import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.android.scanner.impl.ReaderManager;
import com.facebook.react.PackageList;
import com.facebook.react.ReactApplication;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.facebook.soloader.SoLoader;
import com.netease.nim.rn.push.NIMPushPackage;
import com.nlscan.android.uhf.UHFManager;
import com.nlscan.android.uhf.UHFReader;
import com.smartfactory.opensettings.OpenSettingsPackage;
import com.umeng.commonsdk.UMConfigure;
import com.umeng.message.MsgConstant;
import com.umeng.message.PushAgent;
import com.umeng.message.UTrack;
import com.umeng.message.UmengMessageHandler;
import com.umeng.message.UmengNotificationClickHandler;
import com.umeng.message.api.UPushRegisterCallback;
import com.umeng.message.entity.UMessage;

import org.android.agoo.xiaomi.MiPushRegistar;

import java.util.List;

import cn.jiguang.plugins.push.JPushModule;
import cn.reactnative.modules.update.UpdateContext;

//import com.reactnativecommunity.cameraroll.CameraRollPackage;

//import push.DplusReactPackage;
//import push.RNUMConfigure;
// import com.umeng.commonsdk.UMConfigure;
// import com.umeng.message.PushAgent;
// import com.umeng.message.IUmengRegisterCallback;

//import com.smartfactory.UpgradePackage;


public class MainApplication extends Application implements ReactApplication {

    private static ReactApplicationContext reactContext;

    ReaderManager readerManager;

    private UHFManager mUHFMgr;

    private boolean mModuleAvailable =  false;

    private final static int MSG_UHF_POWER_ON = 0x01;
    private final static int MSG_DISMISS_DIALOG = 0x02;
    private final static int MSG_POWER_ON_COMPLETE = 0x03;
    private final static int MSG_UPDATE_VIEW = 0x04;
    private final static int MSG_RESTORE_SUCCESS = 0x05;
    private final static int MSG_RESTORE_FAILED = 0x06;
    private final static int MSG_RELOAD_MODULE_DELAY = 0x07;
    private final static int MSG_LOAD_MODULE_COMPLETED = 0x08;

    private static final String TAG = MainApplication.class.getName();

    private Handler handler;

    private String phoneModle = Build.MODEL;




  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected String getJSBundleFile() {
        return UpdateContext.getBundleUrl(MainApplication.this);
    }

    @Override
    protected List<ReactPackage> getPackages() {
      @SuppressWarnings("UnnecessaryLocalVariable")
      List<ReactPackage> packages = new PackageList(this).getPackages();
      //packages.add(new UpgradePackage());
      // Packages that cannot be autolinked yet can be added manually here, for example:
      // packages.add(new MyReactNativePackage());
      packages.add(new NIMPushPackage());
      packages.add(new DplusReactPackage());
      packages.add(new OpenSettingsPackage());
      packages.add(new PDAScanPackage());
      packages.add(new RFIDPackage());
      //packages.add(new SplashScreenReactPackage());
      return packages;
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

    private Handler RFIDHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what)
            {
                case MSG_UHF_POWER_ON :
                    boolean on = (Boolean) msg.obj;
                    if(!on == true)
                    {
                        Thread t = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                Looper.prepare();
                                UHFReader.READER_STATE er = mUHFMgr.powerOn();
                                boolean is_Poweron = false;
                                if( er == UHFReader.READER_STATE.OK_ERR) {
                                    is_Poweron = true;
                                    Message.obtain(RFIDHandler, MSG_POWER_ON_COMPLETE, is_Poweron).sendToTarget();
                                } else {
                                    Message.obtain(RFIDHandler, MSG_POWER_ON_COMPLETE, is_Poweron).sendToTarget();
                                }

                                Looper.loop();
                            }
                        });
                        t.start();
                    }
                case MSG_POWER_ON_COMPLETE:
                    Boolean er = (Boolean) msg.obj;
                    Log.e("RFID", "isPowerOn_Handler " + er.toString() );
                    break;
                case MSG_RELOAD_MODULE_DELAY:
                    reLoadModule();
                    break;
                case MSG_LOAD_MODULE_COMPLETED:
                    mUHFMgr = UHFManager.getInstance();
                    mModuleAvailable = (mUHFMgr.getUHFModuleInfo() == null ? false : true);
                    Log.e("RFID", "RFIDHandler_mModuleAvailable:  " + String.valueOf(mModuleAvailable));
                    boolean onModule = mUHFMgr.isPowerOn();
                    Log.e("RFID ",String.valueOf(onModule));
                    Message.obtain(RFIDHandler, MSG_UHF_POWER_ON, onModule);
                    WritableMap paramsReloadModule = Arguments.createMap();
                    paramsReloadModule.putString("result", String.valueOf(mModuleAvailable));
                    RFIDModule.sendEvent(reactContext, "isReloadModule", paramsReloadModule);
                    break;
            }
        }
    };

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
    //调用此方法：点击通知让应用从后台切到前台
    JPushModule.registerActivityLifecycle(this);
    UMConfigure.setLogEnabled(true);
    RNUMConfigure.init(this, "61dd6536c248f857ed2f51f7", "Umeng", UMConfigure.DEVICE_TYPE_PHONE,"790a1a84665b4c6b3610594c5ebb543e");

    MiPushRegistar.register(this, "2882303761518177504", "5851817795504", false);

      ReactInstanceManager mReactInstanceManager = getReactNativeHost().getReactInstanceManager();
      if (null == mReactInstanceManager.getCurrentReactContext()) {   // APP启动过程中，初始化ReactInstanceManager等需要时间，先获取一次上下文，如未获取到
          mReactInstanceManager.addReactInstanceEventListener(new ReactInstanceManager.ReactInstanceEventListener() {  //监听初始化是否完成
              public void onReactContextInitialized(ReactContext validContext) {
                   reactContext = (ReactApplicationContext) validContext;
                  // do something
              }
          });
      }else{
          reactContext  = (ReactApplicationContext) mReactInstanceManager.getCurrentReactContext();
          // do something
      }

    initUpush(this);

      //开启物理扫描
      readerManager = ReaderManager.getInstance();
      if (!readerManager.isEnableScankey()) {
          readerManager.setEnableScankey(true);
      }
      readerManager.setEndCharMode(3);

      Log.e("MODEL", Build.MODEL);

      try {
          mUHFMgr = UHFManager.getInstance();
          mModuleAvailable = (mUHFMgr.getUHFModuleInfo() != null);
          Log.e("RFID", "onCreate_mModuleAvailable: " + String.valueOf(mModuleAvailable));
          if(!mModuleAvailable)
              RFIDHandler.sendEmptyMessageDelayed(MSG_RELOAD_MODULE_DELAY, 50);
          else {
              boolean on = mUHFMgr.isPowerOn();
              Log.e("onCreate_RFID ", "isPowerOn: " + String.valueOf(mUHFMgr.isPowerOn()));
              Log.e("onCreate_RFID ", "on: " + String.valueOf(on));
              if (!on) {
                  Message msg = Message.obtain(RFIDHandler, MSG_UHF_POWER_ON, on);
                  msg.sendToTarget();
              }
          }
      } catch(Exception exception) {
          Log.e("RFIDERR", "getInstance: " + exception.toString());
      };




  }

  private void initUpush(MainApplication mainApplication) {
    PushAgent mPushAgent = PushAgent.getInstance(this);

    handler = new Handler(getMainLooper());

    //sdk开启通知声音
    mPushAgent.setNotificationPlaySound(MsgConstant.NOTIFICATION_PLAY_SDK_ENABLE);
    mPushAgent.setNotificationPlayLights(MsgConstant.NOTIFICATION_PLAY_SDK_ENABLE);
    mPushAgent.setNotificationPlayVibrate(MsgConstant.NOTIFICATION_PLAY_SDK_ENABLE);
    // sdk关闭通知声音
    //		mPushAgent.setNotificationPlaySound(MsgConstant.NOTIFICATION_PLAY_SDK_DISABLE);
    // 通知声音由服务端控制
    //		mPushAgent.setNotificationPlaySound(MsgConstant.NOTIFICATION_PLAY_SERVER);

    //		mPushAgent.setNotificationPlayLights(MsgConstant.NOTIFICATION_PLAY_SDK_DISABLE);
    //		mPushAgent.setNotificationPlayVibrate(MsgConstant.NOTIFICATION_PLAY_SDK_DISABLE);


    UmengMessageHandler messageHandler = new UmengMessageHandler() {

       //处理通知栏消息
      @Override
      public void dealWithNotificationMessage(Context context, UMessage msg) {
          super.dealWithNotificationMessage(context, msg);
          Log.i(TAG, "notification1 receiver:" + msg.getRaw().toString());
          try {
              reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("UMNoticeListener", msg.getRaw().toString());
          } catch (Throwable throwable){
              Log.e("sendEvent error:",throwable.getMessage());
          }
          Log.e("xxxxxx", "PUSHnotification receiver: success" );
          Log.e("xxxxxx", "PUSHnotification receiver:" + msg.getRaw().toString());

      }
        /**
         * 自定义消息的回调方法
         */
        @Override
        public void dealWithCustomMessage(final Context context, final UMessage msg) {

            handler.post(new Runnable() {

                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    // 对自定义消息的处理方式，点击或者忽略
                    boolean isClickOrDismissed = true;
                    if (isClickOrDismissed) {
                        //自定义消息的点击统计
                        UTrack.getInstance(getApplicationContext()).trackMsgClick(msg);
                    } else {
                        //自定义消息的忽略统计
                        UTrack.getInstance(getApplicationContext()).trackMsgDismissed(msg);
                    }
                    Toast.makeText(context, msg.custom, Toast.LENGTH_LONG).show();
                }
            });
        }
    };
    mPushAgent.setMessageHandler(messageHandler);

    /**
     * 自定义行为的回调处理，参考文档：高级功能-通知的展示及提醒-自定义通知打开动作
     * UmengNotificationClickHandler是在BroadcastReceiver中被调用，故
     * 如果需启动Activity，需添加Intent.FLAG_ACTIVITY_NEW_TASK
     * */
    UmengNotificationClickHandler notificationClickHandler = new UmengNotificationClickHandler() {
        @Override
        public void dealWithCustomAction(Context context, UMessage msg) {
          Log.i(TAG, "CustomAction receiver:" + msg.getRaw().toString());
            Toast.makeText(context, msg.custom, Toast.LENGTH_LONG).show();
        }

        @Override
        public void openActivity(Context context, UMessage msg) {
            super.openActivity(context, msg);
            Log.i(TAG, "click openActivity: " + msg.getRaw().toString());
        }

        @Override
        public void launchApp(Context context, UMessage msg) {
            super.launchApp(context, msg);
            try {
                reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("UMNoticeClick", msg.getRaw().toString());
            } catch (Throwable throwable){
                Log.e("clickEvent error:",throwable.getMessage());
            }
            Log.i(TAG, "clickEvent launchApp: " + msg.getRaw().toString());
        }

        @Override
        public void dismissNotification(Context context, UMessage msg) {
            super.dismissNotification(context, msg);
            Log.i(TAG, "click dismissNotification: " + msg.getRaw().toString());
        }
    };
    //使用自定义的NotificationHandler，来结合友盟统计处理消息通知，参考http://bbs.umeng.com/thread-11112-1-1.html
    //CustomNotificationHandler notificationClickHandler = new CustomNotificationHandler();
    mPushAgent.setNotificationClickHandler(notificationClickHandler);


    //注册推送服务 每次调用register都会回调该接口
    mPushAgent.register(new UPushRegisterCallback() {
        @Override
        public void onSuccess(String deviceToken) {
            Log.i(TAG, "device token: " + deviceToken);
        }

        @Override
        public void onFailure(String s, String s1) {
            Log.i(TAG, "register failed: " + s + " " + s1);
        }
    });



}


    //重新检测模块
    private void reLoadModule()
    {
        synchronized (this) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    mUHFMgr = UHFManager.getInstance();
                    mUHFMgr.loadUHFModule();
                    RFIDHandler.sendEmptyMessage(MSG_LOAD_MODULE_COMPLETED);
                }
            }).start();
        }
    }
}

