
# react-native-com-pda-scan

## Getting started

`$ npm install react-native-com-pda-scan --save`

### Mostly automatic installation

`$ react-native link react-native-com-pda-scan`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-com-pda-scan` and add `RNComPdaScan.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNComPdaScan.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.reactlibrary.RNComPdaScanPackage;` to the imports at the top of the file
  - Add `new RNComPdaScanPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-com-pda-scan'
  	project(':react-native-com-pda-scan').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-com-pda-scan/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-com-pda-scan')
  	```

#### Windows
[Read it! :D](https://github.com/ReactWindows/react-native)

1. In Visual Studio add the `RNComPdaScan.sln` in `node_modules/react-native-com-pda-scan/windows/RNComPdaScan.sln` folder to their solution, reference from their app.
2. Open up your `MainPage.cs` app
  - Add `using Com.Pda.Scan.RNComPdaScan;` to the usings at the top of the file
  - Add `new RNComPdaScanPackage()` to the `List<IReactPackage>` returned by the `Packages` method


## Usage
```javascript
import RNComPdaScan from 'react-native-com-pda-scan';

// TODO: What to do with the module?
RNComPdaScan;
```
  