using ReactNative.Bridge;
using System;
using System.Collections.Generic;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;

namespace Com.Pda.Scan.RNComPdaScan
{
    /// <summary>
    /// A module that allows JS to share data.
    /// </summary>
    class RNComPdaScanModule : NativeModuleBase
    {
        /// <summary>
        /// Instantiates the <see cref="RNComPdaScanModule"/>.
        /// </summary>
        internal RNComPdaScanModule()
        {

        }

        /// <summary>
        /// The name of the native module.
        /// </summary>
        public override string Name
        {
            get
            {
                return "RNComPdaScan";
            }
        }
    }
}
