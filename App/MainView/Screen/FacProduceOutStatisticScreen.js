import React from 'react';
import { View, Text, processColor, TouchableOpacity, StyleSheet, ActivityIndicator } from 'react-native';
import LineChart from '../Components/LineChart'
import Url from '../../Url/Url';
import { RFT, deviceWidth } from '../../Url/Pixal';
import { withNavigation } from 'react-navigation';

const greenBlue = "rgb(26, 182, 151)";
const petrel = "rgb(59, 145, 153)";

class facProduceOutStatisticScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      FacProduceOutStatistic: {
        yearMonth: ['1', '2', '3', '4', '5'],
        proVolumeList: [0, 0, 0, 0, 0],
        outVolumeList: [0, 0, 0, 0, 0],
      },
      isLoading: true,
    }
    this.requestUrl = this.requestUrl.bind(this);
  }

  componentDidMount() {
    //this.requestUrl();
  }

  requestUrl = (urlPorops) => {
    const urlTest = Url.url + Url.factory + Url.Fid + 'GetFacProduceOutStatisticYear'
    console.log("🚀 ~ file: FacProduceOutStatisticScreen.js:31 ~ facProduceOutStatisticScreen ~ urlTest:", urlTest)
    
    if (typeof urlPorops != 'undefined') {
      this.setState({ isLoading: true })
      this.requestData(urlPorops);
      console.log("🚀 ~ file: FacProduceOutStatisticScreen.js:31 ~ facProduceOutStatisticScreen ~ urlPorops:", urlPorops)

      return
    }
    this.requestData(urlTest);
  }

  requestData = (url) => {
    let yearMonth = [], proVolumeList = [], outVolumeList = [], Data = {};
    fetch(url, {
      credentials: 'include',
    }).then(res => {
      return res.json();
    }).then(resData => {
      resData.map((item, index) => {
        yearMonth.push(item.yearMonth);
        proVolumeList.push(item.proVolume);
        outVolumeList.push(item.outVolume);
      })
      Data.yearMonth = yearMonth;
      Data.proVolumeList = proVolumeList;
      Data.outVolumeList = outVolumeList;
      this.setState({
        FacProduceOutStatistic: Data,
        isLoading: false,
      })
      console.log(this.state.FacProduceOutStatistic.proVolumeList)
    }).catch((error) => {
      console.log(error);
    });
  }

  handleSelect(event) {
    let entry = event.nativeEvent
    if (entry == null) {
      this.setState({ ...this.state, selectedEntry: null })
    } else {
      this.setState({ ...this.state, selectedEntry: JSON.stringify(entry) })
    }

    console.log(event.nativeEvent)
  }

  render() {
    const { isLoading } = this.state;
    if (isLoading === true) {
      return (
        <View style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#F5FCFF',
        }}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
    } else {
      return (
        <View style={{ flex: 1 }}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('MonthStatistic', {
                title: '月生产与发货统计分析',
              })
            }} >
            <View style={{ flexDirection: 'row' }} >
              <Text style={styles.text}>单位：m³ </Text>
              <Text style={[styles.text, { flex: 1, textAlign: 'right', color: '#419fff' }]}>查看本月详情</Text>
            </View>
          </TouchableOpacity>
          <LineChart
            legend={{
              enabled: true,
              //textSize: RFT * 2.8,
              form: 'CIRCLE',
              xEntrySpace: 30,
              formToTextSpace: 10,
              horizontalAlignment: 'CENTER',
              verticalAlignment: 'TOP'
            }}
            data={{
              dataSets: [
                {
                  values: this.state.FacProduceOutStatistic.proVolumeList,
                  label: "生产量",
                  config: {
                    mode: "HORIZONTAL_BEZIER",
                    drawValues: false,
                    lineWidth: 2,
                    drawCircles: false,
                    circleColor: processColor('#00EA9F'),
                    drawCircleHole: false,
                    circleRadius: 2,
                    highlightColor: processColor("#00EA9F"),
                    highlightLineWidth: 2,
                    drawHighlightIndicators: true,
                    color: processColor('#00EA9F'),
                    drawFilled: true,
                    fillGradient: {
                      colors: [processColor('#00EA9F'), processColor('transparent')],
                      positions: [0, 0.7],
                      angle: 90,
                      orientation: "TOP_BOTTOM"
                    },
                    fillAlpha: 1000,
                    valueTextSize: RFT * 2
                  }
                }, {
                  values: this.state.FacProduceOutStatistic.outVolumeList,
                  label: "发货量",
                  config: {
                    mode: "HORIZONTAL_BEZIER",
                    drawValues: false,
                    lineWidth: 2,
                    drawCircles: false,
                    circleColor: processColor('#229DFF'),
                    drawCircleHole: false,
                    circleRadius: 2,
                    highlightColor: processColor('#229DFF'),
                    highlightLineWidth: 2,
                    color: processColor('#229DFF'),
                    drawFilled: true,
                    fillGradient: {
                      colors: [processColor("#229DFF"), processColor("transparent")],
                      positions: [0, 0.9],
                      angle: 90,
                      orientation: "TOP_BOTTOM"
                    },
                    fillAlpha: 1000,
                    valueTextSize: RFT * 2
                  }
                }
              ]
            }}
            xAxis={{
              enabled: true,
              granularity: 1,
              drawLabels: true,
              position: "BOTTOM",
              axisMaximum: this.state.FacProduceOutStatistic.yearMonth.length - 1,
              axisMinimum: -0.5,
              drawAxisLine: false,
              drawGridLines: false,
              fontFamily: "HelveticaNeue-Medium",
              fontWeight: "bold",
              textSize: RFT * 3,
              textColor: processColor("gray"),
              valueFormatter: this.state.FacProduceOutStatistic.yearMonth,
              //labelRotationAngle: -30
            }}
            animation={{
              durationX: 0,
              durationY: 1500,
              easingY: 'EaseInOutQuart'
            }}
            yAxis={{
              left: {
                enabled: true,
                drawAxisLine: false,
                textSize: RFT * 2.5,
                gridColor: processColor('#e9e9e9'),
                drawLabels: true,
                labelCount: 5
              },
              right: {
                enabled: false
              }
            }}
            visibleRange={{
              x: { min: 1, max: 4 }
            }}
            onSelect={this.handleSelect.bind(this)}
            onChange={(event) => console.log(event.nativeEvent)}
          />
        </View>
      )
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
    backgroundColor: 'transparent'
  },
  text: {
    marginLeft: 0,
    color: 'gray',
    fontSize: 10,
    textAlign: 'left'
  }
});

export default withNavigation(facProduceOutStatisticScreen)