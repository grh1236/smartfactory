import React, { Component } from 'react';
import { View, Text, StyleSheet, processColor, Dimensions, PixelRatio, ScrollView, ActivityIndicator } from 'react-native';
import { Button } from 'react-native-elements';
import BarChart from '../ChildComponent/BarChart';
import Url from '../../Url/Url';
import { RFT, deviceWidth, deviceHeight } from '../../Url/Pixal';

export default class projectAnalysisMonth extends React.Component {
  constructor() {
    super();
    this.state = {
      isLoading: true,
      projectAnalysisMonth: {}
    }
  }

  componentDidMount() {
    //this.requestUrl()
  }

  ReFlash = (url) => {
    console.log("🚀 ~ file: projectProcessAnalysis.js:22 ~ projectAnalysisMonth ~ url:", url)
    Url.Login();
    this.refAnalysis.setStateLoaing();
    this.requestUrl(url)
  }

  // ReFlash = (Data) => {
  //   this.chartDataSet(Data)
  //   //this.refAnalysis.setStateLoaing();
  //   this.setState({
  //     projectAnalysisMonth: Data,
  //     isLoading: false
  //   })
  // }


  requestUrl = (url) => {
    console.log(url)
    const urlTest = 'https://smart.pkpm.cn:910/api/' + Url.factory + Url.Fid + 'GetFacProjectAnalysisMonth'
    this.requestData(url);
  }

  requestData = (url) => {
    console.log("🚀 ~ file: projectProcessAnalysis.js ~ line 33 ~ projectAnalysisMonth ~ url", url)
    let nameList = [], preVolumeList = [], proVolumeList = [], storageVolumeList = [], outVolumeList = [], Data = {};
    fetch(url, {
      credentials: 'include',
    }).then(res => {
      return res.json();
    }).then(resData => {
      if (resData[0] != '登录已超时,请重新登录') {
        resData.map((item, index) => {
          nameList.push(item.compTypeName);
          preVolumeList.push(item.preVolume);
          proVolumeList.push(item.proVolume);
          storageVolumeList.push(item.storageVolume);
          outVolumeList.push(item.outVolume);
        })
        Data.Name = nameList || [];
        Data.Vol1 = preVolumeList || [];
        Data.Vol2 = proVolumeList || [];
        Data.Vol3 = storageVolumeList || [];
        Data.Vol4 = outVolumeList || []; 
      }

      this.chartDataSet(Data);

      this.setState({
        projectAnalysisMonth: Data,
        isLoading: false
      })
      //console.log(Data)
    }).catch((error) => {
      console.log(error);
    });
  }

  chartDataSet = (Data) => {
    let chartData = {
      dataSets: [{
        values: Data.Vol1 || [10, 10, 20, 30, 40],
        label: '合同量',
        config: {
          drawValues: false,
          colors: [processColor('#32D185')],
        }
      }, {
        values: Data.Vol2 || [10, 20, 30, 40, 50],
        label: '生产量',
        config: {
          drawValues: false,
          colors: [processColor('#509FF0')],
        }
      }, {
        values: Data.Vol4 || [10, 20, 30, 40, 50],
        label: '发货量',
        config: {
          drawValues: false,
          colors: [processColor('#FF7A38')],
        }
      }],
      config: {
        barWidth: 0.1,
        group: {
          fromX: 0,
          groupSpace: 0.55,
          barSpace: 0.05,
        },
      }
    }

    let length = Data.Name.length

    let chartXAxis = {
      valueFormatter: Data.Name || [1, 2, 3, 4, 5, 6],
      granularityEnabled: true,
      granularity: 1,
      axisMaximum: length,
      axisMinimum: 0,
      position: "BOTTOM",
      drawAxisLine: false,
      centerAxisLabels: true,
      setDrawGirdLines: false,
      drawGridLines: false,
    }

    this.refAnalysis.ReFlash(chartData, chartXAxis);
  }



  render() {
    return (
      <View style={{ flex: 1 }} >
        <View style={styles.container}>
          <BarChart
            ref={refAnalysis => { this.refAnalysis = refAnalysis }} />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  chart: {
    flex: 1,
    marginBottom: 6
  },
  text: {
    marginLeft: RFT * 2,
    color: 'gray',
    fontSize: 12
  },
  view: {
    flex: 1,
    backgroundColor: '#F9F9F9'
  },
});