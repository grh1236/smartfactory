import React, { Component } from 'react';
import { View, Text, StyleSheet, processColor, Dimensions, PixelRatio, ScrollView, ActivityIndicator } from 'react-native';
import { Button } from 'react-native-elements';
import BarChart from '../ChildComponent/BarChart';
import Url from '../../Url/Url';
import { RFT, deviceWidth, deviceHeight } from '../../Url/Pixal';

export default class projectAnalysisMonth extends React.Component {
  constructor() {
    super();
    this.state = {
      projectAnalysisMonth: {}
    }
  }

  componentDidMount() {
    //this.requestUrl()
  }

  requestUrl = () => {
    const urlTest = Url.url + Url.factory + Url.Fid + Url.StaticType + 'GetFacProjectAnalysisMonth'
    console.log("🚀 ~ file: projectAnalysisMonth.js:22 ~ projectAnalysisMonth ~ urlTest:", urlTest)

    this.requestData(urlTest);
  }

  requestData = (url) => {
    let projectNameList = [], planVolumeList = [], volumeList = [], Data = {};
    fetch(url, {
      credentials: 'include',
    }).then(res => {
      return res.json();
    }).then(resData => {
      resData.map((item, index) => {
        let projectName = ''
        if (item.projectAbb != null) {
          projectName = item.projectAbb
          projectNameList.push(projectName.substring(0, 6));
        } else {
          projectName = item.projectName
          projectNameList.push(projectName.substring(0, 6));
        }
        planVolumeList.push(item.planVolume);
        volumeList.push(item.volume);
      })

      Data.Name = projectNameList;
      Data.Vol1 = planVolumeList;
      Data.Vol2 = volumeList;
      console.log("🚀 ~ file: projectAnalysisMonth.js ~ line 44 ~ projectAnalysisMonth ~ fetch ~ Data", Data)

      this.chartDataSet(Data)

      this.setState({
        projectAnalysisMonth: Data
      })
    }).catch((error) => {
      console.log(error);
    });
  }

  chartDataSet = (Data) => {
    let chartData = {
      dataSets: [{
        values: Data.Vol1 || [10, 10, 20, 30, 40],
        label: '计划产量',
        config: {
          drawValues: false,
          colors: [processColor('#5765FF')],
        }
      }, {
        values: Data.Vol2 || [10, 20, 30, 40, 50],
        label: '实际产量',
        config: {
          drawValues: false,
          colors: [processColor('#0ACC84')],
        }
      }],
      config: {
        barWidth: 0.22,
        group: {
          fromX: 0,
          groupSpace: 0.36,
          barSpace: 0.1,
        },
      }
    }

    let length = Data.Name.length

    let chartXAxis = {
      valueFormatter: Data.Name || [1, 2, 3, 4, 5, 6],
      granularityEnabled: true,
      granularity: 1,
      axisMaximum: length,
      axisMinimum: 0,
      position: "BOTTOM",
      drawAxisLine: false,
      centerAxisLabels: true,
      setDrawGirdLines: false,
      drawGridLines: false,
    }


    this.refPlan.ReFlash(chartData, chartXAxis);
  }


  render() {
    return (
      <View style={{ flex: 1 }} >
        <View style={styles.container}>
          <BarChart
            ref={refPlan => { this.refPlan = refPlan }} />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  chart: {
    flex: 1,
    marginBottom: 6
  },
  text: {
    marginLeft: RFT * 2,
    color: 'gray',
    fontSize: 12
  },
  view: {
    flex: 1,
    backgroundColor: '#F9F9F9'
  },
});
