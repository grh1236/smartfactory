/*
 * @Author: error: error: git config user.name & please set dead value or install git && error: git config user.email & please set dead value or install git & please set dead value or install git
 * @Date: 2023-07-31 11:11:19
 * @LastEditors: 高睿豪 1375308739@qq.com
 * @LastEditTime: 2023-08-04 13:52:31
 * @FilePath: /SmartFactory/App/MainView/ChildPages/LoutuPage.js
 * @Description: 
 * 
 * Copyright (c) 2023 by ${git_name_email}, All Rights Reserved. 
 */
import React, { Component } from 'react';
import { View, Text, ScrollView, ActivityIndicator } from 'react-native';
import WebView from 'react-native-webview';
import Url from '../../Url/Url';
import { deviceHeight } from '../../Url/Pixal';

let isErrorVar = false

export default class LoutuPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      UrlProjectH5Chart: '',
      UrlProjectH5List: '',
      isLoading: false,
      isError: false
    };
  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  componentDidMount() {
    const { navigation } = this.props;
    let urlChart = navigation.getParam('urlChart') || ''
    console.log("🚀 ~ file: LoutuPage.js:35 ~ LoutuPage ~ componentDidMount ~ urlChart:", urlChart)
    console.log("🚀 ~ file: LoutuPage.js:42 ~ LoutuPage ~ componentDidMount ~ Url.isLoutuError:", Url.isLoutuError)

    if (Url.isLoutuError) {
      let UrlProjectH5ChartNew = urlChart
      UrlProjectH5ChartNew = UrlProjectH5ChartNew.replace('projectH5_fullScreen', 'projectH5')
      this.setState({
        UrlProjectH5Chart: UrlProjectH5ChartNew,
      })
      return
    } else {
      this.setState({
        UrlProjectH5Chart: urlChart,
      })
    }

    // setTimeout(() => {
    //   this.ref.scrollToEnd({ animated: false })
    // }, 1000)

  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <ScrollView
          //onContentSizeChange={() => }
          overScrollMode='never'
          ref={(ref) => this.ref = ref}
          style={{ flex: 1 }}
          scrollEnabled={false}
        >
          <View style={{ height: isErrorVar ? 0 : deviceHeight }}>

            <WebView
              source={{ uri: this.state.UrlProjectH5Chart }}
              ref={(ref) => this.ref = ref}
              style={{ flex: 1 }}
              onError={(e) => {
                console.log("🚀 ~ file: LoutuPage.js:64 ~ LoutuPage ~ render ~ e:", e)
              }}
              renderLoading={() => {
                return (
                  <View style={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: '#F5FCFF',
                  }}>
                    <ActivityIndicator
                      animating={true}
                      color='#419FFF'
                      size="large" />
                  </View>
                )
              }}

              // startInLoadingState={true}
              // onHttpError={(e) => {
              //   isErrorVar = true
              //   this.setState({
              //     isError: true
              //   })
              //   console.log("🚀 ~ file: LoutuPage.js:66 ~ LoutuPage ~ render ~ e:", e.eventPhase)
              //   let UrlProjectH5ChartNew = this.state.UrlProjectH5Chart
              //   UrlProjectH5ChartNew = UrlProjectH5ChartNew.replace('projectH5_fullScreen', 'projectH5')
              //   setTimeout(() => {
              //     isErrorVar = false
              //     this.setState({
              //       isError: false,
              //       UrlProjectH5Chart: UrlProjectH5ChartNew
              //     })
              //   }, 500)
              // }}
              // onLoadEnd={() => {
              //   this.ref.injectJavaScript(
              //     () => {
              //       return(
              //         `
              // 								(function () {
              //                   $('.content-contain-1-title').remove()
              // 								} ())
              // 							`
              //       )
              //     }
              //   )
              // }}
              onMessage={(e) => {
                let dataArr = e.nativeEvent.data.split('&')
                console.log("🚀 ~ file: Project.js:576 ~ Project_Screen ~ render ~ dataArr:", dataArr)

                let project = dataArr[1].split('=')
                let projectId = project[1]
                let floorNoIdArr = dataArr[2].split('=')
                let floorNoId = floorNoIdArr[1]
                let floorIdArr = dataArr[3].split('=')
                let floorId = floorIdArr[1]
                let compTypeIdArr = dataArr[4].split('=')
                let compTypeId = compTypeIdArr[1]
                let compstateArr = dataArr[5].split('=')
                let compState = compstateArr[1]
                let floorNoNameArr = dataArr[6].split('=')
                let floorNoName = floorNoNameArr[1]
                let floorNameArr = dataArr[8].split('=')
                let floorName = floorNameArr[1]
                let compTypeNameArr = dataArr[7].split('=')
                let compTypeName = compTypeNameArr[1]

                this.props.navigation.navigate('ChildOneNew', {
                  title: '构件明细',
                  sonTitle: "构件详情",
                  url: Url.url + Url.Produce + 'PostProduceCompDetail',
                  sonUrl: '',
                  projectId: projectId,
                  floorNoId: floorNoId,
                  floorNoName: floorNoName,
                  floorId: floorId,
                  floorName: floorName,
                  compTypeId: compTypeId,
                  compTypeName: compTypeName,
                  compState: compState
                })
              }}
            ></WebView>
          </View>

        </ScrollView>
      </View>

    );
  }
}
