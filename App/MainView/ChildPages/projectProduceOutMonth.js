import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, ScrollView, ActivityIndicator } from 'react-native';
import { Button, ListItem, Badge } from 'react-native-elements';
import BarLineChart from '../ChildComponent/BarLineChart';
import Url from '../../Url/Url';
import { RFT, RVH, deviceWidth, deviceHeight, color } from '../../Url/Pixal';
import LinearGradient from 'react-native-linear-gradient';

export default class Combined extends Component {
  constructor() {
    super();
    this.state = {
      projectProduceOutStatistic: {},
      resproduceData: [
        { projectAbb: '加载中' }
      ],
      resoutData: [
        { projectAbb: '加载中' }
      ],
      ProduceTotal: {
        Num: 0,
        Vol: 0,
      },
      OutTotal: {
        Num: 0,
        Vol: 0,
      },
      isLoading: true,
    }
    this.requestUrl = this.requestUrl.bind(this);
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  componentDidMount() {
    this.requestUrl()
  }

  requestUrl = () => {
    let url = Url.url + Url.factory + Url.Fid + 'GetFacProjectProduceStatisticMonth'
    if (Url.isStaticTypeOpen) {
      if (Url.StaticType != '0/') {
        url = Url.url + Url.factory + Url.Fid + Url.StaticType + 'GetDuctPieceFacProjectProduceStatisticMonth'
      }
    }
    console.log("🚀 ~ file: projectProduceOutMonth.js:47 ~ Combined ~ url:", url)
    this.requestData(url);
  }

  requestData = (url) => {
    fetch(url).then(res => {
      return res.json();
    }).then(resData => {
      this.resProduceData(resData);

      this.resOutData(resData);

      this.setState({
        FacProduceOutStatistic: resData
      })
      //console.log(Data)
    }).catch((error) => {
      console.log(error);
    });
  }

  resProduceData = (resData) => {

    let projectNameList = [], proNumberList = [], proVolumeList = [], produceData = {}, produceList = [], ProduceTotalNum = 0, ProduceTotalVol = 0.000, ProduceTotal = {};
    resData.map((item, index) => {
      ProduceTotalNum += item.proNumber
      ProduceTotalVol += item.proVolume
      item.color = color[index]
      if (item.proNumber != 0 && item.proVolume.proVolume != 0.000) {
        let tmp = {}
        tmp.color = item.color
        tmp.type = 'produce'
        tmp.projectAbb = item.projectAbb.slice(0, 6) || item.projectName
        tmp.proNumber = item.proNumber
        tmp.proVolume = item.proVolume
        produceList.push(tmp)
        if (item.projectAbb != null) {
          projectNameList.push(item.projectAbb);
        } else {
          projectNameList.push(item.projectName);
        }
        proNumberList.push(item.proNumber);
        proVolumeList.push(item.proVolume);
      }
    })

    produceData.Name = projectNameList;
    produceData.Num = proNumberList;
    produceData.Vol = proVolumeList;
    ProduceTotal.Num = ProduceTotalNum
    ProduceTotal.Vol = ProduceTotalVol

    console.log("resProduceData -> resData", resData)
    this.setState({
      ProduceTotal: ProduceTotal,
      resproduceData: produceList,
      isLoading: false
    })

    this.refProduce.ReFlash(produceData);

  }

  resOutData = (resData) => {
    let projectNameList = [], outNumberList = [], outVolumeList = [], outData = {}, outList = [], outTotalNum = 0, outTotalVol = 0, OutTotal = {};
    resData.map((item, index) => {
      outTotalNum += item.outNumber
      outTotalVol += item.outVolume
      item.color = color[index]
      if (item.outNumber != 0 && item.outVolume != 0.000) {
        let tmp = {}
        tmp.color = item.color
        tmp.type = 'out'
        tmp.projectAbb = item.projectAbb.slice(0, 6) || item.projectName
        tmp.outNumber = item.outNumber
        tmp.outVolume = item.outVolume
        outList.push(tmp)
        if (item.projectAbb != null) {
          projectNameList.push(item.projectAbb);
        } else {
          projectNameList.push(item.projectName);
        }
        outNumberList.push(item.outNumber)
        outVolumeList.push(item.outVolume);
      }
    })

    outData.Name = projectNameList;
    outData.Num = outNumberList;
    outData.Vol = outVolumeList;
    OutTotal.Num = outTotalNum
    OutTotal.Vol = outTotalVol

    console.log("resOutData -> resData", resData)
    this.setState({
      OutTotal: OutTotal,
      resoutData: outList,
      isLoading: false
    })

    this.refOut.ReFlash(outData);
  }

  TopCard = () => {
    const { ProduceTotal, OutTotal } = this.state
    return (
      <View style={{ justifyContent: 'center', alignItems: 'center', borderRadius: 12, marginVertical: 8 }}>
        <LinearGradient style={{ width: deviceWidth - 26, borderRadius: 5, }} start={{ x: 1, y: 1 }} end={{ x: 0, y: 1 }}
          colors={['#419FFF', 'rgba(65,216,255,0.88) ']}>
          <View style={styles.text_TotalRow}>
            <View style={[styles.text_TotalRow_block, { flexDirection: 'row', flex: 1.18 }]}>
              <View style={styles.text_TotalRow_block}>
                <View style={styles.text_TotalBadge}></View>
              </View>
              <View style={styles.text_TotalRow_block}>
                <Text style={ProduceTotal.Num < 100 ? styles.text_TotalName : [styles.text_TotalName, { fontSize: RFT * 3.2 }]} >{'本月生产总量'}</Text>
              </View>
            </View>
            <View style={[styles.text_TotalRow_block, { flex: 1 }]}>
              <Text style={ProduceTotal.Num < 100 ? styles.text_TotalValue : [styles.text_TotalValue, { fontSize: RFT * 3.0 }]} >{'总数：' + ProduceTotal.Num + '件'}</Text>
            </View>
            <View style={[styles.text_TotalRow_block, { flex: 1.4 }]}>
              <Text style={ProduceTotal.Num < 100 ? styles.text_TotalValue : [styles.text_TotalValue, { fontSize: RFT * 3.0 }]} >{'总方量：' + ProduceTotal.Vol.toFixed(3) + 'm³'}</Text>
            </View>
          </View>
          <View style={styles.text_TotalRow}>
            <View style={[styles.text_TotalRow_block, { flexDirection: 'row', flex: 1.18 }]}>
              <View style={styles.text_TotalRow_block}>
                <View style={styles.text_TotalBadge}></View>
              </View>
              <View style={styles.text_TotalRow_block}>
                <Text style={ProduceTotal.Num < 100 ? styles.text_TotalName : [styles.text_TotalName, { fontSize: RFT * 3.2 }]} >{'本月发货总量'}</Text>
              </View>
            </View>
            <View style={[styles.text_TotalRow_block, { flex: 1 }]}>
              <Text style={ProduceTotal.Num < 100 ? styles.text_TotalValue : [styles.text_TotalValue, { fontSize: RFT * 3.0 }]} >{'总数：' + OutTotal.Num + '件'}</Text>
            </View>
            <View style={[styles.text_TotalRow_block, { flex: 1.4 }]}>
              <Text style={ProduceTotal.Num < 100 ? styles.text_TotalValue : [styles.text_TotalValue, { fontSize: RFT * 3.0 }]} >{'总方量：' + OutTotal.Vol.toFixed(3) + 'm³'}</Text>
            </View>
          </View>
        </LinearGradient>
      </View>
    )
  }

  render() {
    const { isLoading, resproduceData, resoutData } = this.state;
    let produceLength = resproduceData.length || 0, outLength = resoutData.length || 0
    console.log("render -> outLength", outLength)

    if (isLoading === true) {
      return (
        <View style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#F5FCFF',
        }}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
    } else {
      return (

        <View style={styles.view}>

          {
            this.TopCard()
          }

          <ScrollView style={styles.view} overScrollMode='never'>
            <Text style={styles.text}>月生产统计</Text>
            <View style={[styles.chartview, { height: RVH * RFT * 9 + RVH * RFT * 1.63 * produceLength }]}>

              <View style={styles.chart}>
                <BarLineChart
                  height={deviceWidth * 0.7}
                  ref={refProduce => { this.refProduce = refProduce }}
                />
              </View>

              {/* 表格区 */}
              <View style={styles.chartview_listview}>
                {/* this.ListTitle() */}
                <FlatList
                  // style={{ height: deviceHeight * 0.12 *  produceLength  }}
                  data={resproduceData}
                  renderItem={this._renderItem}
                />
              </View>

            </View>
            <Text style={styles.text}>月发货统计</Text>
            <View style={[styles.chartview, { height: RVH * RFT * 10 + RVH * RFT * 1.63 * produceLength }]}>

              <View style={styles.chart}>
                <BarLineChart
                  height={deviceWidth * 0.7}
                  ref={refOut => { this.refOut = refOut }}
                />
              </View>

              {/* 表格区 */}
              <View style={styles.chartview_listview}>
                {/* this.ListTitle() */}
                <FlatList
                  // style={{ height: deviceHeight * 0.12 *  produceLength }}
                  data={resoutData}
                  renderItem={this._renderItem}
                />
              </View>

            </View>

            <View style={{ height: 20, backgroundColor: 'transparent' }}></View>
          </ScrollView>
        </View>
      )
    }
  }

  _renderItem = ({ item, index }) => {
    console.log("_renderItem -> item", item)
    if (item.type == 'produce') {
      return (
        <ListItem
          //title={item.productLineName + ': ' + item.proNumber + '件' + '(' + item.proVolume + 'm³)'}
          title={
            <View style={{ flexDirection: 'row', backgroundColor: 'transparent' }}>
              <Text style={styles.text_list_name}>{item.projectAbb.slice(0, 6)}</Text>
              <Text style={styles.text_list_num}>{(item.proNumber || 0) + '件'}</Text>
              <Text style={styles.text_list_vol}>{(item.proVolume || 0) + 'm³'}</Text>
            </View>
          }
          //titleStyle={styles.text_list}
          containerStyle={styles.text_list_container}
          bottomDivider
          leftElement={<Badge badgeStyle={{ backgroundColor: item.color }} />}
        />
      )
    } else {
      return (
        <ListItem
          //title={item.productLineName + ': ' + item.proNumber + '件' + '(' + item.proVolume + 'm³)'}
          title={
            <View style={{ flexDirection: 'row', backgroundColor: 'transparent' }}>
              <Text style={styles.text_list_name}>{item.projectAbb.slice(0, 6)}</Text>
              <Text style={styles.text_list_num}>{(item.outNumber || 0) + '件'}</Text>
              <Text style={styles.text_list_vol}>{(item.outVolume || 0) + 'm³'}</Text>
            </View>
          }
          //titleStyle={styles.text_list}
          containerStyle={styles.text_list_container}
          bottomDivider
          leftElement={<Badge badgeStyle={{ backgroundColor: item.color }} />}
        />
      )
    }

  }


}

const styles = StyleSheet.create({
  view: {
    flex: 1,
    backgroundColor: '#F9F9F9'
  },
  text: {
    color: '#535c68',
    fontSize: RFT * 3.5,
    paddingLeft: 15,
    marginTop: 10,
  },
  chartview_listview: {
    width: deviceWidth * 0.86,
    marginLeft: deviceWidth * 0.03,
    marginTop: deviceWidth * 0.03,
    borderRadius: 10,
    backgroundColor: 'transparent'
  },
  text_list_container: {
    backgroundColor: 'transparent',
    height: RVH * RFT * 1.63
  },
  text_list: {
    fontSize: RFT * 3.5,
    textAlignVertical: "center",
    color: '#454545',
    //marginEnd: 12,
    //marginVertical: 10,
    //fontFamily:'STKaiti',
    //fontWeight: 'bold',
  },
  text_list_name: {
    flex: 1.7,
    fontSize: RFT * 3.6,
    textAlign: 'center',
    fontFamily: 'STHeitiSC-Light',
  },
  text_list_num: {
    flex: 1,
    fontSize: RFT * 3.6,
    textAlign: 'center',
    fontFamily: 'STHeitiSC-Light',
  },
  text_list_vol: {
    flex: 1.2,
    fontSize: RFT * 3.6,
    textAlign: 'center',
    fontFamily: 'STHeitiSC-Light',
  },
  text_TotalRow: {
    flexDirection: 'row',
    marginVertical: RFT * 2,
    justifyContent: 'center',
    alignContent: 'center'
  },
  text_TotalRow_block: {
    justifyContent: 'center',
    alignContent: 'center',
  },
  text_TotalBadge: {
    width: RFT * 1.5,
    height: RFT * 1.5,
    marginLeft: 6,
    borderRadius: 100,
    backgroundColor: 'white'
  },
  text_TotalName: {
    fontWeight: "bold",
    marginHorizontal: 10,
    color: 'white',
    textAlignVertical: 'center',
    textAlign: 'left'
  },
  text_TotalValue: {
    color: 'white',
    marginHorizontal: 10,
    textAlignVertical: "bottom",
    textAlign: "left"
  },
  chartview: {
    width: deviceWidth * 0.94,
    height: deviceHeight * 0.58,
    marginTop: 10,
    marginLeft: deviceWidth * 0.03,
    backgroundColor: '#FFFFFF',
    borderRadius: 10
  },
  chart: {
    width: deviceWidth * 0.93,
    height: deviceWidth * 0.58,
    backgroundColor: 'transparent'
  }
})