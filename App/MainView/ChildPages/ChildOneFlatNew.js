import React from 'react'
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Picker, TouchableHighlight, ScrollView, Alert } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, Overlay, BottomSheet } from 'react-native-elements';
import { NavigationEvents } from 'react-navigation'
import Toast from 'react-native-easy-toast';
import SideMenu from 'react-native-side-menu';

import Url from '../../Url/Url';
import { ToDay, YesterDay, BeforeYesterDay } from '../../CommonComponents/Data';
import { RFT, deviceWidth, color } from '../../Url/Pixal';
import TouchableCustom from '../../PDA/Componment/TouchableCustom';
import BottomItem from '../../PDA/Componment/BottomItem';
import LinearGradient from 'react-native-linear-gradient';

const Urlnew = Url.url + Url.Produce;
let url = '', title = '', sonUrl = '';
let pageNo = 1;   //翻页设置,有问题
let pageName = ''
const { height, width } = Dimensions.get('window') //获取宽高

let isCheckList = []; //是否展开状态

let length = 0;

let searchList = {
  productLineNameList: [],
  projectIdList: [],
  projectNameList: [],
  designTypeList: [],
  bigStateList: [],
  compTypeNameList: [],
  compRoomNameList: [],
  editEmployeeNameList: [],
  compLibNameList: [],
  roomLibNameList: [],
  floorNoNameList: [],
  floorNameList: [],
  prjFNNameList: [],
  FNFNameList: [],
}

let selectList = [
  { name: '构件明细' },
  { name: '构件生产统计' },
]

let sideType = ''

let projectList = []

let data = {}

let isShow = false //筛选按钮

//搜索栏搜索图标
const defaultSearchIcon = () => ({
  //type: 'antdesign',
  size: 20,
  name: 'search',
  color: 'white',
});

//搜索栏清理图标
const defaultClearIcon = () => ({
  //type: '',
  size: 20,
  name: 'close',
  color: 'white',
});

//搜索栏搜索图标
const designSearchIcon = () => ({
  type: 'antdesign',
  size: 17,
  name: 'search1',
  color: '#666',
});

//搜索栏清理图标
const designClearIcon = () => ({
  type: 'antdesign',
  size: 18,
  name: 'close',
  color: '#666',
});

export default class ChildOneNew extends React.Component {
  constructor() {
    super();
    this.state = {
      search: '', //搜索关键字保存
      resData: [], //接口数据保存
      item: [], //好像没用
      isLoading: true,  //判断是否加载 加载页
      isRefreshing: false,  //是否刷新
      page: '1', //页码
      open: false, //
      selectedIndex: 0,  //按钮组选择
      selectedIndexTask: 0,//按钮组选择-任务单
      selectedIndexLoaout: 0,//按钮组选择-出库
      typeFocusButton: 0, //tab页按钮
      isCheckList: [],
      //筛选框
      searchList: {
        productLineNameList: [],
        projectIdList: [],
        projectNameList: [],
        designTypeList: [],
        bigStateList: [],
        compTypeNameList: [],
        compRoomNameList: [],
        editEmployeeNameList: [],
        compLibNameList: [],
        roomLibNameList: [],
        floorNoNameList: [],
        floorNameList: [],
        prjFNNameList: [],
        FNFNameList: []
      },
      factoryId: '',
      taskId: '',
      bigstate: '',
      //生产线筛选
      isproductLinePickerVisible: false,
      resDataProductLine: [],
      productLinePicker: '请选择',
      productLineId: '',
      productLineName: '',
      productLineList: [],
      //项目筛选
      isProjectPickerVisible: false,
      resDataProject: [],
      projectPicker: '请选择',
      projectId: '',
      projectName: '',
      projectList: [],
      //库房筛选
      isCompRoomPickerVisible: false,
      resDataCompRoom: [],
      compRoomPicker: '请选择',
      compRoomId: '',
      compRoomName: '',
      compRoomList: [],
      //库位筛选
      isCompLibPickerVisible: false,
      resDataCompLib: [],
      compLibPicker: '请选择',
      compLibId: '',
      compLibName: '',
      compLibList: [],
      //构件类型筛选
      isCompTypePickerVisible: false,
      resDataCompType: [],
      compTypePicker: '请选择',
      compTypeId: '',
      compTypeName: '',
      compTypeList: [],
      //设计型号筛选
      isDesignTypePickerVisible: false,
      resDataDesignType: [],
      designTypePicker: '请选择',
      designTypeId: '',
      designTypeName: '',
      designTypeList: [],
      //构件状态筛选
      isBigStatePickerVisible: false,
      resDataBigState: [],
      bigStatePicker: '请选择',
      bigStateId: '',
      bigStateName: '',
      bigStateList: [],
      //楼号筛选
      isFloorNOPickerVisible: false,
      resDataFloorNO: [],
      FloorNOPicker: '请选择',
      FloorNOId: '',
      FloorNOName: '',
      FloorNOList: [],
      //楼层筛选
      isFloorsPickerVisible: false,
      resDataFloors: [],
      FloorsPicker: '请选择',
      FloorsId: '',
      FloorsName: '',
      //负责人筛选
      isEditEmployeeNamePickerVisible: '',
      editEmployeeName: '',
      editEmployeeNamePicker: '请选择',
      //是否合格筛选
      isInsresultsPickerVisible: false,
      resDataInsresults: ["合格", "不合格"],
      InsresultsPicker: '请选择',
      InsresultsId: '',
      InsresultsName: '',
      //日生产
      urldaily: '',
      //侧边栏
      isSideOpen: false,
      //底部弹出栏
      bottomData: [],
      bottomVisible: false,
      //
      comptypeVisible: false,
      focusButton: '',
      sideType: '',
      pageLoading: true,
      urlnew: '',
      isRequest: false,
      length: 0
      ,
    }
    //函数绑定
    this.requestData = this.requestData.bind(this);
    this.requestData1 = this.requestData1.bind(this);
    this.refreshing = this.refreshing.bind(this);
    this.refreshing1 = this.refreshing1.bind(this);
    this._onload = this._onload.bind(this);
    //this._onload1 = this._onload1.bind(this);
    this._footer = this._footer.bind(this);
  }

  //长列表头部
  _header = () => {
    return <Text style={[styles.txt, { backgroundColor: 'black' }]}>这是头部</Text>;
  }

  //长列表尾部
  _footer = () => {
    return (
      /* <View style={{ flexDirection: 'row', justifyContent: 'center', alignContent: 'center' }}>
        <Text style={[styles.txt, { textAlign: 'right', flex: 1 }]}>第</Text>
        <Input
          containerStyle={{ width: width / 10 }}
          placeholder=' '
          onChangeText={text => this.setState({ page: text })}
          onChange={text => {
            this.setState({ page: text })
            this.requestData();
          }} >
        </Input>
        <Text style={[styles.txt, { flex: 1 }]}> 页</Text>
      </View> */
      <Text style={[{ fontSize: 18, textAlign: 'center', marginHorizontal: 30 }]}>第 {this.state.page} 页</Text>
    );
  }

  //长列表分隔栏
  _separator = () => {
    return <View style={{ height: 2, backgroundColor: '#fffffc' }} />;
  }

  //刷新列表
  refreshing() {
    pageNo = 1
    this.setState({
      resData: [],
      page: 1,
    })
    const { selectedIndex } = this.state;
    const { navigation } = this.props;
    const url = navigation.getParam('url')
    if (url.indexOf('DailyRecord') != -1) {
      if (selectedIndex == 0) {
        const urlnew = url.substring(0, url.length - 10) + ToDay
        this.setState({ urldaily: urlnew })
        this.requestData(urlnew);
      } else if (selectedIndex == 1) {
        const urlnew = url.substring(0, url.length - 10) + YesterDay
        this.setState({ urldaily: urlnew })
        this.requestData(urlnew);
      } else if (selectedIndex == 2) {
        const urlnew = url.substring(0, url.length - 10) + BeforeYesterDay
        this.setState({ urldaily: urlnew })
        this.requestData(urlnew);
      }
      return;
    }
    this.requestData();
  }

  //刷新列表
  refreshing1() {
    pageNo = 1
    data.pageIndex = 1
    this.setState({
      resData: [],
      pageLoading: true,
      page: 1,
    })
    this.requestData1();
  }

  UpdateControl = () => {
    Url.Login()
    const { navigation } = this.props;
    url = navigation.getParam('url');
    sonUrl = navigation.getParam('sonUrl');
    console.log("🚀 ~ file: ChildOneFlat.js ~ line 289 ~ ChildOne ~ componentDidMount ~ sonUrl", sonUrl)
    console.log("🚀 ~ file: ChildOneFlat.js ~ line 285 ~ ChildOne ~ componentDidMount ~ url", url)
    title = navigation.getParam('title'); //从主界面获取标题
    //length = navigation.getParam('length') || '0'
    console.log("🚀 ~ file: ChildOneFlat.js ~ line 288 ~ ChildOne ~ componentDidMount ~ length", length)
    this.setState({ pageLoading: true })
    //待产池
    if (url.indexOf('PostWaitingPoolProjectPage') != -1 && sonUrl == '') {
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "search": '',
        "pageIndex": 1,
        "dataCount": 40
      }
      this.requestDataProject()
    }
    if (sonUrl.indexOf('PostWaitingPoolCompPage2') != -1) {
      const projectId = navigation.getParam('projectId'); //父页面获取项目id
      this.setState({ projectId: projectId })
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "projectId": projectId,
        "search": '',
        "pageIndex": 1,
        "dataCount": 40
      }
    }
    //日生产记录
    if (url.indexOf('PostProduceDailyRecordProjectPage2') != -1 && sonUrl == '') {
      let date = ToDay
      if (this.state.selectedIndex == '1') {
        date = YesterDay
      }
      if (this.state.selectedIndex == '2') {
        date = BeforeYesterDay
      }
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        //"produceDate": "2022-05-16",
        "produceDate": date,
        "search": "",
        "pageIndex": 1,
        "dataCount": 40
      }
      this.requestDataProject()
    }
    if (sonUrl.indexOf('PostProduceDailyRecordPage') != -1) {
      const projectId = navigation.getParam('projectId'); //父页面获取项目id
      const date = navigation.getParam('date'); //父页面获取项目id
      this.setState({ projectId: projectId }, () => {
        this.seachListFunc([], "dailyrecord")
      })
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "projectId": projectId,
        //"produceDate": "2022-05-16",
        "search": '',
        "produceDate": date,
        "compTypeName": "",
        "floorNoName": "",
        "floorName": "",
        "compCode": "",
        "projectName": "",
        "pageIndex": 1,
        "dataCount": 40
      }
    }
    //脱模待检
    if (url.indexOf('PostProduceReleasePage2') != -1) {
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "pageIndex": 1,
        "dataCount": 40
      }
      this.requestDataProject()
    }
    //已生产构件明细
    if (url.indexOf('PostProduceProjectPage') != -1 && sonUrl == '') {
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "projectName": "",
        "pageIndex": 1,
        "dataCount": 40
      }
      this.requestDataProject()
    }
    if (sonUrl.indexOf('PostProduceComponentStatePage') != -1) {
      const projectId = navigation.getParam('projectId'); //父页面获取项目id
      const floorNoName = navigation.getParam('floorNoName');
      const floorName = navigation.getParam('floorName');
      const compTypeName = navigation.getParam('compTypeName');
      this.setState({ projectId: projectId }, () => {
        this.seachListFunc([], "producedcomponents")
      })
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "projectId": projectId,
        "bigState": "",
        "compCode": "",
        "warehouRomName": "",
        "warehouLibName": "",
        "compTypeName": compTypeName,
        "floorNoName": floorNoName,
        "floorName": floorName,
        "designType": "",
        "pageIndex": 1,
        "dataCount": 40
      }

    }
    //任务单
    if (url.indexOf('PostProduceTaskPage') != -1) {
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "formCode": "",
        "productLineName": "",
        "editEmployeeName": "",
        "projectName": "",
        "state": "",
        "pageIndex": 1,
        "dataCount": 40
      }
      if (this.state.selectedIndexTask == '1') {
        data.state = "提交"
      }
      if (this.state.selectedIndexTask == '2') {
        data.state = "未提交"
      }
      this.seachListFunc([], "productLine")
    }
    if (url.indexOf('PostProduceTaskCompStatelistPage') != -1) {
      const state = navigation.getParam('state'); //父页面获取状态
      const taskid = navigation.getParam('taskid'); //父页面获取项目id
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "task_id": taskid,
        "state": state,
        "compCode": "",
        "compTypeName": "",
        "ProjectId": "",
        "floorNoName": "",
        "floorName": "",
        "pageIndex": 1,
        "dataCount": 40
      }
      this.seachListFunc([], "taskcompstate")
    }
    //质量隐检
    if (url.indexOf('PostQualityHidcheckProjectPage2') != -1 && sonUrl == '') {
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "pageIndex": 1,
        "dataCount": 40
      }
      this.requestDataProject()
    }
    if (sonUrl.indexOf('PostQualityHidcheckPage') != -1) {
      const projectId = navigation.getParam('projectId'); //父页面获取项目id
      this.setState({ projectId: projectId })
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "projectId": projectId,
        "pageIndex": 1,
        "dataCount": 40
      }
      //this.seachListFunc([], "producedcomponents")
    }
    //成品检查
    if (url.indexOf('PostQualityFinProductsProjectPage') != -1 && sonUrl == '') {
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "projectName": "",
        "pageIndex": 1,
        "dataCount": 40
      }
      this.requestDataProject()
    }
    if (sonUrl.indexOf('PostQualityFinProductsPage') != -1) {
      const projectId = navigation.getParam('projectId'); //父页面获取项目id
      this.setState({ projectId: projectId }, () => {
        this.seachListFunc([], "qualityfinproducts")
      })
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "ProjectId": projectId,
        "insresults": "",
        "designType": "",
        "compCode": "",
        "floorNoName": "",
        "floorName": "",
        "compTypeName": "",
        "pageIndex": 1,
        "dataCount": 40
      }

    }
    //成品复检
    if (url.indexOf('PostQualityReFinProductsProjectPage') != -1 && sonUrl == '') {
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "pageIndex": 1,
        "dataCount": 40
      }
      this.requestDataProject()
    }
    if (sonUrl.indexOf('PostQualityReFinProductsPage2') != -1) {
      const projectId = navigation.getParam('projectId'); //父页面获取项目id
      this.setState({ projectId: projectId })
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "ProjectId": projectId,
        "pageIndex": 1,
        "dataCount": 40
      }
      this.requestDataProject()
    }
    //构件报废
    if (url.indexOf('PostQualityScrapProjectPage') != -1 && sonUrl == '') {
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "pageIndex": 1,
        "dataCount": 40
      }
      this.requestDataProject()
    }
    if (sonUrl.indexOf('PostQualityScrapPage') != -1) {
      const projectId = navigation.getParam('projectId'); //父页面获取项目id
      this.setState({ projectId: projectId })
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "projectId": projectId,
        "pageIndex": 1,
        "dataCount": 40
      }
    }
    //发货计划
    if (url.indexOf('PostStockLoaoutWarePage') != -1 && title.indexOf('发货计划') != -1) {
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "state": "",
        "search": "",
        "planformCode": "",
        "projectName": "",
        "pageIndex": 1,
        "dataCount": 40
      }
      if (this.state.selectedIndexLoaout == '1') {
        data.state = "已完成"
      }
      if (this.state.selectedIndexLoaout == '2') {
        data.state = "未完成"
      }
      this.requestDataProject()
    }
    //成品出库
    if (url.indexOf('PostStockLoaoutWarePage') != -1 && title.indexOf('成品出库') != -1) {
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "state": "已完成",
        "search": "",
        "planformCode": "",
        "projectName": "",
        "pageIndex": 1,
        "dataCount": 40
      }
      this.requestDataProject()
    }
    if (url.indexOf('PostProduceCompDetail') != -1) {
      const projectId = navigation.getParam('projectId'); //父页面获取项目id
      const floorNoId = navigation.getParam('floorNoId');
      const floorId = navigation.getParam('floorId');
      const compTypeId = navigation.getParam('compTypeId');
      this.setState({ projectId: projectId })
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "projectId": projectId,
        "compTypeId": compTypeId,
        "floorNoId": floorNoId,
        "floorId": floorId,
        "pageIndex": 1,
        "dataCount": 40,
        "isPrj": 0
      }
    }
    /*  if (url.search('GetProduceDailyRecord/') != -1) {
       const selectedIndex = navigation.getParam('selectedIndex');
 
       if (selectedIndex == '0') {
         const urlnew = url.substring(0, url.length - 10) + ToDay
         this.requestData(urlnew);
       } else if (selectedIndex == '1') {
         const urlnew = url.substring(0, url.length - 10) + YesterDay
         this.requestData(urlnew);
       } else {
         const urlnew = url.substring(0, url.length - 10) + BeforeYesterDay
         this.requestData(urlnew);
       }
       this.setState({ selectedIndex: selectedIndex })
     } else this.requestData(); */
    this.requestData1()

    if ((
      url.indexOf('Pool') != -1 ||
      url.indexOf('PostProduceReleasePage2') != -1 ||
      url.indexOf('PostQualityHidcheckProjectPage2') != -1 ||
      url.indexOf('PostQualityHidcheckProjectPage2') != -1 ||
      url.indexOf('PostQualityReFinProductsProjectPage') != -1 ||
      url.indexOf('PostQualityScrapProjectPage') != -1 ||
      url.indexOf('PostProduceCompStatistics') != -1
    ) && sonUrl == '') {
      isShow = false
    }

    if (sonUrl.indexOf('PostWaitingPoolCompPage') != -1 ||
      sonUrl.indexOf('PostQualityHidcheckPage') != -1 ||
      sonUrl.indexOf('PostQualityReFinProductsPage2') != -1 ||
      sonUrl.indexOf('PostQualityReFinProductsPage2') != -1 ||
      sonUrl.indexOf('PostQualityScrapPage') != -1
    ) {
      isShow = false
    }
  }

  //执行函数
  componentDidMount() {
    Url.Login()
    const { navigation } = this.props;
    url = navigation.getParam('url');
    sonUrl = navigation.getParam('sonUrl');
    console.log("🚀 ~ file: ChildOneFlat.js ~ line 289 ~ ChildOne ~ componentDidMount ~ sonUrl", sonUrl)
    console.log("🚀 ~ file: ChildOneFlat.js ~ line 285 ~ ChildOne ~ componentDidMount ~ url", url)
    title = navigation.getParam('title'); //从主界面获取标题
    console.log("🚀 ~ file: ChildOneFlat.js ~ line 568 ~ ChildOne ~ componentDidMount ~ title", title)
    //length = navigation.getParam('length') || '0'
    console.log("🚀 ~ file: ChildOneFlat.js ~ line 288 ~ ChildOne ~ componentDidMount ~ length", length)

    //待产池
    if (url.indexOf('PostWaitingPoolProjectPage') != -1 && sonUrl == '') {
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "search": '',
        "pageIndex": 1,
        "dataCount": 40
      }
      this.requestDataProject()
    }
    if (sonUrl.indexOf('PostWaitingPoolCompPage2') != -1) {
      const projectId = navigation.getParam('projectId'); //父页面获取项目id
      this.setState({ projectId: projectId })
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "projectId": projectId,
        "search": '',
        "pageIndex": 1,
        "dataCount": 40
      }
    }
    //日生产记录
    if (url.indexOf('PostProduceDailyRecordProjectPage2') != -1 && sonUrl == '') {
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        //"produceDate": "2022-05-16",
        "produceDate": ToDay,
        "projectName": "",
        "pageIndex": 1,
        "dataCount": 40
      }
      this.requestDataProject()
    }
    if (sonUrl.indexOf('PostProduceDailyRecordPage') != -1) {
      const projectId = navigation.getParam('projectId'); //父页面获取项目id
      const date = navigation.getParam('date'); //父页面获取项目id
      this.setState({ projectId: projectId }, () => {
        this.seachListFunc([], "dailyrecord")
      })
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "projectId": projectId,
        //"produceDate": "2022-05-16",
        "search": '',
        "produceDate": date,
        "compTypeName": "",
        "floorNoName": "",
        "floorName": "",
        "compCode": "",
        "projectName": "",
        "pageIndex": 1,
        "dataCount": 40
      }
    }
    //脱模待检
    if (url.indexOf('PostProduceReleasePage2') != -1) {
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "pageIndex": 1,
        "dataCount": 40
      }
      this.requestDataProject()
    }
    //已生产构件明细
    if (url.indexOf('PostProduceProjectPage') != -1 && sonUrl == '') {
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "projectName": "",
        "pageIndex": 1,
        "dataCount": 40
      }
      this.SearchPostproject()
    }
    if (sonUrl.indexOf('PostProduceComponentStatePage') != -1) {
      const projectId = navigation.getParam('projectId'); //父页面获取项目id
      const floorNoName = navigation.getParam('floorNoName');
      const floorName = navigation.getParam('floorName');
      const compTypeName = navigation.getParam('compTypeName');
      this.setState({ projectId: projectId }, () => {
        this.seachListFunc([], "producedcomponents")
      })
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "projectId": projectId,
        "bigState": "",
        "compCode": "",
        "warehouRomName": "",
        "warehouLibName": "",
        "compTypeName": compTypeName,
        "floorNoName": floorNoName,
        "floorName": floorName,
        "designType": "",
        "pageIndex": 1,
        "dataCount": 40
      }

    }
    //任务单
    if (url.indexOf('PostProduceTaskPage') != -1) {
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "formCode": "",
        "productLineName": "",
        "editEmployeeName": "",
        "projectName": "",
        "state": "",
        "pageIndex": 1,
        "dataCount": 40
      }
      this.seachListFunc([], "productLine")
    }
    if (url.indexOf('PostProduceTaskCompStatelistPage') != -1) {
      const state = navigation.getParam('state'); //父页面获取状态
      const taskid = navigation.getParam('taskid'); //父页面获取项目id
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "task_id": taskid,
        "state": state,
        "compCode": "",
        "compTypeName": "",
        "ProjectId": "",
        "floorNoName": "",
        "floorName": "",
        "pageIndex": 1,
        "dataCount": 40
      }
      this.seachListFunc([], "taskcompstate")
    }
    //质量隐检
    if (url.indexOf('PostQualityHidcheckProjectPage2') != -1 && sonUrl == '') {
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "pageIndex": 1,
        "dataCount": 40
      }
      this.requestDataProject()
    }
    if (sonUrl.indexOf('PostQualityHidcheckPage') != -1) {
      const projectId = navigation.getParam('projectId'); //父页面获取项目id
      this.setState({ projectId: projectId })
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "projectId": projectId,
        "pageIndex": 1,
        "dataCount": 40
      }
      //this.seachListFunc([], "producedcomponents")
    }
    //成品检查
    if (url.indexOf('PostQualityFinProductsProjectPage') != -1 && sonUrl == '') {
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "projectName": "",
        "pageIndex": 1,
        "dataCount": 40
      }
      this.requestDataProject()
    }
    if (sonUrl.indexOf('PostQualityFinProductsPage') != -1) {
      const projectId = navigation.getParam('projectId'); //父页面获取项目id
      this.setState({ projectId: projectId }, () => {
        this.seachListFunc([], "qualityfinproducts")
      })
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "ProjectId": projectId,
        "insresults": "",
        "designType": "",
        "compCode": "",
        "floorNoName": "",
        "floorName": "",
        "compTypeName": "",
        "pageIndex": 1,
        "dataCount": 40
      }

    }
    //成品复检
    if (url.indexOf('PostQualityReFinProductsProjectPage') != -1 && sonUrl == '') {
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "pageIndex": 1,
        "dataCount": 40
      }
      this.requestDataProject()
    }
    if (sonUrl.indexOf('PostQualityReFinProductsPage2') != -1) {
      const projectId = navigation.getParam('projectId'); //父页面获取项目id
      this.setState({ projectId: projectId })
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "ProjectId": projectId,
        "pageIndex": 1,
        "dataCount": 40
      }
      this.requestDataProject()
    }
    //构件报废
    if (url.indexOf('PostQualityScrapProjectPage') != -1 && sonUrl == '') {
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "pageIndex": 1,
        "dataCount": 40
      }
      this.requestDataProject()
    }
    if (sonUrl.indexOf('PostQualityScrapPage') != -1) {
      const projectId = navigation.getParam('projectId'); //父页面获取项目id
      this.setState({ projectId: projectId })
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "projectId": projectId,
        "pageIndex": 1,
        "dataCount": 40
      }
    }
    //发货计划
    if (url.indexOf('PostStockLoaoutWarePage') != -1 && title.indexOf('发货计划') != -1) {
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "state": "",
        "planformCode": "",
        "projectName": "",
        "pageIndex": 1,
        "dataCount": 40
      }
      this.requestDataProject()
    }
    //成品出库
    if (url.indexOf('PostStockLoaoutWarePage') != -1 && title.indexOf('成品出库') != -1) {
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "state": "已完成",
        "planformCode": "",
        "projectName": "",
        "pageIndex": 1,
        "dataCount": 40
      }
      this.requestDataProject()
    }
    if (url.indexOf('PostProduceCompDetail') != -1) {
      const projectId = navigation.getParam('projectId'); //父页面获取项目id
      const floorNoId = navigation.getParam('floorNoId');
      const floorId = navigation.getParam('floorId');
      const compTypeId = navigation.getParam('compTypeId');
      this.setState({ projectId: projectId })
      data = {
        "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
        "projectId": projectId,
        "compTypeId": compTypeId,
        "floorNoId": floorNoId,
        "floorId": floorId,
        "pageIndex": 1,
        "dataCount": 40,
        "isPrj": 0
      }
    }
    /*  if (url.search('GetProduceDailyRecord/') != -1) {
       const selectedIndex = navigation.getParam('selectedIndex');
 
       if (selectedIndex == '0') {
         const urlnew = url.substring(0, url.length - 10) + ToDay
         this.requestData(urlnew);
       } else if (selectedIndex == '1') {
         const urlnew = url.substring(0, url.length - 10) + YesterDay
         this.requestData(urlnew);
       } else {
         const urlnew = url.substring(0, url.length - 10) + BeforeYesterDay
         this.requestData(urlnew);
       }
       this.setState({ selectedIndex: selectedIndex })
     } else this.requestData(); */
    this.requestData1()


    if ((
      url.indexOf('Pool') != -1 ||
      url.indexOf('PostProduceReleasePage2') != -1 ||
      url.indexOf('PostQualityHidcheckProjectPage2') != -1 ||
      url.indexOf('PostQualityHidcheckProjectPage2') != -1 ||
      url.indexOf('PostQualityReFinProductsProjectPage') != -1 ||
      url.indexOf('PostQualityScrapProjectPage') != -1 ||
      url.indexOf('PostProduceCompStatistics') != -1
    ) && sonUrl == '') {
      isShow = false
    }

    if (sonUrl.indexOf('PostWaitingPoolCompPage') != -1 ||
      sonUrl.indexOf('PostQualityHidcheckPage') != -1 ||
      sonUrl.indexOf('PostQualityReFinProductsPage2') != -1 ||
      sonUrl.indexOf('PostQualityReFinProductsPage2') != -1 ||
      sonUrl.indexOf('PostQualityScrapPage') != -1
    ) {
      isShow = false
    }
  }

  componentWillUnmount() {
    pageNo = 1
    //data = {}
    sonUrl = ''
    //url = '', title = ''
    this.setState({ page: "1" })
    searchList = {
      productLineNameList: [],
      projectIdList: [],
      projectNameList: [],
      designTypeList: [],
      bigStateList: [],
      compTypeNameList: [],
      compRoomNameList: [],
      roomLibNameList: [],
      compLibNameList: [],
      editEmployeeNameList: [],
      floorNoNameList: [],
      floorNameList: [],
      prjFNNameList: [],
      FNFNameList: []
    }
    isShow = false
  }

  //返回接口数据
  requestData = (urlprops) => {
    const { navigation } = this.props;
    const url = navigation.getParam('url'); //从主界面获取链接
    this.setState({ isSideOpen: false })
    if (url.indexOf('GetStockLoaoutWare') != -1) {
      fetch(urlprops ? urlprops : url).then(res => {
        return res.json()
      }).then(resData => {
        this.seachListFunc(resData, "project")
        this.setState({
          resData: resData,
          isLoading: false,
          pageLoading: false,
        });
        //console.log(this.state.resData);
      }).catch((error) => {
        console.log(error);
      });
    }
    //需要翻页的链接返回
    else if (url.indexOf('/40/') != -1) {
      //接口链接需要加上页数，页数上拉会加载下一页
      fetch(url + this.state.page).then(res => {
        return res.json()
      }).then(resData => {
        if (resData.length > 0) {
          let reDataOld = this.state.resData
          let resDataNew = reDataOld.concat(resData)
          if (url.indexOf('GetProduceComponentState') != -1) {
            if (resData.length > 0) {
              length = navigation.getParam('length')
              this.setState({
                factoryId: resData[0].factoryId,
                projectId: resData[0].projectId
              })
              data = {
                "factoryId": resData[0].factoryId,
                "projectId": resData[0].projectId,
                "warehouRomName": "",
                "warehouLibName": "",
                "bigState": "",
                "compCode": "",
                "designType": "",
                "compTypeName": "",
                "floorNoName": "",
                "floorName": ""
              }
              this.seachListFunc(resData, "producedcomponents")
            }
          }
          if (url.indexOf('GetQualityFinProducts/') != -1) {
            if (resData.length > 0) {
              this.setState({
                factoryId: resData[0].factoryId,
                projectId: resData[0].projectId
              })
              data = {
                "factoryId": resData[0].factoryId,
                "projectId": resData[0].projectId,
                "insresults": "",
                "designType": "",
                "compCode": "",
                "floorNoName": "",
                "floorName": "",
                "compTypeName": "",
              }
              this.seachListFunc(resData, "qualityfinproducts")
            }
          }
          this.setState({
            resData: resDataNew,
            isLoading: false,
            pageLoading: false,
          });
          length = navigation.getParam('length')
        } else {
          this.toast.show('无数据')
        }
        //console.log(this.state.resData);
      }).catch((error) => {
        console.log(error);
      });
    } //需要按日期记录的页面数据，日期用按钮控制，从render函数传过来
    else if (url.indexOf('DailyRecord') != -1) {
      if (!urlprops) {
        this.setState({ urlnew: url })
      }
      //点按钮时传当日数据，否则传今天的
      fetch(urlprops ? urlprops : url).then(res => {
        return res.json()
      }).then(resData => {
        try {
          this.setState({
            //searchList: searchList,
            resDataProject: [],
            projectPicker: '请选择',
            projectId: '',
            projectName: '',
          })
        } catch (error) {
        }

        if (resData.length > 0) {
          if (url.indexOf('GetProduceDailyRecordProject') != -1) {
            try {
              //searchList.projectNameList = []
              this.seachListFunc(resData, "project")
            } catch (err) {

            }
          }
          if (url.indexOf('GetProduceDailyRecord/') != -1) {
            try {
              this.setState({ projectId: resData[0].projectId })
            } catch (err) {
            }
            this.seachListFunc(resData, "dailyrecord")
          }
        }
        this.setState({
          resData: resData,
          isLoading: false,
          pageLoading: false,
        });
      }).catch((error) => {
        console.log(error);
      });
    } //通用数据接口
    else {
      fetch(urlprops ? urlprops : url).then(res => {
        return res.json()
      }).then(resData => {
        if (url.indexOf('GetProjectProducedComponents') != -1) {
          if (resData.length > 0) {
            this.setState({
              factoryId: resData[0].factoryId,
              projectId: resData[0].projectId
            })
            this.seachListFunc(resData, "producedcomponents")
          }
        }
        if (url.indexOf('Project') != -1) {
          this.seachListFunc(resData, "project")
        }
        if (url.indexOf('GetProduceTask') != -1) {
          this.seachListFunc(resData, "productLine")
        }
        if (url.indexOf('ViewProduceTaskCompStatelist') != -1) {
          if (resData.length > 0) {
            this.setState({
              taskId: resData[0].task_id,
              bigstate: resData[0].bigstate
            })
            this.seachListFunc(resData, "taskcompstate")
          }
          //this.requestDataFloorNO(resData[0].projectId)
        }
        this.setState({
          resData: resData,
          isLoading: false,
          pageLoading: false,
        });
        //console.log(this.state.resData);
      }).catch((error) => {
        console.log(error);
      });
    }

  }

  requestData1 = (resType) => {
    console.log("🚀 ~ file: ChildOneFlat.js ~ line 807 ~ ChildOne ~ data", data)
    console.log("🚀 ~ file: ChildOneFlatNew.js:1107 ~ JSON.stringify(data):", JSON.stringify(data))
    let resUrl = ''
    if (sonUrl == '') {
      resUrl = url
    } else {
      resUrl = sonUrl
    }
    fetch(resUrl, {
      method: 'POST',
      credentials: 'include',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    }).then(res => {
      return res.json()
    }).then(resData => {
      console.log("🚀 ~ file: ChildOneFlat.js ~ line 747 ~ ChildOne ~ resData", resData)
      length = resData.dataCount
      if (length > 0) {
        let data = resData.data
        this.setState({
          resData: data,
          length: length,
          isLoading: false,
          pageLoading: false,
        });
        console.log("🚀 ~ file: ChildOneFlat.js ~ line 755 ~ ChildOne ~ length == 0", length == 0)

      } else {
        length = 0
        this.setState({ isLoading: false, pageLoading: false, length: 0 })
        this.toastMain.show('没有查询到构件数据')
      }
      //console.log(this.state.resData);
    }).catch((error) => {
      console.log("🚀 ~ file: ChildOneFlat.js ~ line 530 ~ ChildOne ~ error", error)
    });

  }

  //上拉加载下一页
  _onload() {
    if (url.indexOf('/40/') != -1) {
      pageNo += 1
      this.setState({
        page: pageNo
      }, () => {
        this.requestData();
      })


    }

  }

  _onload1() {
    pageNo += 1
    data.pageIndex += 1
    this.toastMain.show('加载中')
    console.log("🚀 ~ file: ChildOneFlat.js ~ line 303 ~ ChildOne ~ _onload ~ data", data)
    if (sonUrl == '') {
      resUrl = url
    } else {
      resUrl = sonUrl
    }
    fetch(resUrl, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    }).then(res => {
      return res.json()
    }).then(resData1 => {
      console.log("🚀 ~ file: ChildOneFlat.js ~ line 1106 ~ ChildOne ~ _onload1 ~ resData1", resData1)
      let resData = []
      resData = resData1.data

      if (resData.length > 0) {
        let reDataOld = this.state.resData
        let resDataNew = reDataOld.concat(resData)
        // if (url.indexOf('Project') != -1 || url.indexOf('Task') != -1 || url.indexOf('PostProduceReleasePage2') != -1 || url.indexOf('PostStockLoaoutWarePage') != -1) {
        //}
        this.setState({
          resData: resDataNew,
          page: pageNo,
          isLoading: false,
          pageLoading: false,
        });
        this.toastMain.show('加载完成')

      } else {
        pageNo -= 1
        data.pageIndex -= 1
        this.toastMain.show('没有更多数据')
      }
    }).catch((error) => {
      console.log("🚀 ~ file: ChildOneFlat.js ~ line 530 ~ ChildOne ~ error", error)
    });
  }

  seachListFunc = (resData, type) => {
    if (type == "project") {
      if (searchList.projectNameList.length > 0) {
        searchList.projectNameList.map((item1, index) => {
          resData.map((item, index) => {
            if (item1 == item.projectName) {
              searchList.projectNameList.splice(index, 1);
            }
          })
        });
      }
      resData.map((item, index) => {
        if (searchList.projectNameList.indexOf(item.projectName) == -1) {
          searchList.projectNameList.push(item.projectName)
        }
      })
    } else if (type == 'productLine') {
      Url.employeeList.map((item, index) => {
        if (searchList.editEmployeeNameList.indexOf(item.editEmployeeName) == -1) {
          searchList.editEmployeeNameList.push(item.editEmployeeName)
        }
      })
      Url.productLineNameList.map((item, index) => {
        if (searchList.productLineNameList.indexOf(item.dataName) == -1) {
          searchList.productLineNameList.push(item.dataName)
        }
      })
      searchList.productLineNameList.sort()
      this.requestDataProject()
    } else if (type == 'producedcomponents') {
      {
        this.requestDataCompType();
        this.requestDataFloorNO(this.state.projectId)
        let bigStateList = ['2+生产中', '3+隐检合格', '14+混凝土浇注', '9+脱模待检', '4+成检合格', '5+入库', '6+出库', '10+成品退库', '7+卸车', '8+安装', '11+报废']
        searchList.bigStateList = bigStateList
        //this.requestDataBigState()
        this.requestDataWareLib()
      }

      //searchList.bigStateList.sort()
      searchList.floorNoNameList.sort()
      searchList.floorNameList.sort()
    } else if (type == 'taskcompstate') {
      this.requestDataProject()
      Url.compTypeNameList.map((item) => {
        searchList.compTypeNameList.push(item.dataName)
      })
    } else if (type == 'dailyrecord') {
      Url.compTypeNameList.map((item, index) => {
        if (searchList.compTypeNameList.indexOf(item.dataName) == -1) {
          searchList.compTypeNameList.push(item.dataName)
        }
      })
      this.requestDataFloorNO(this.state.projectId)
    } else if (type == 'qualityfinproducts') {
      this.requestDataCompType();
      this.requestDataFloorNO(this.state.projectId)
      searchList.floorNoNameList.sort()
      searchList.floorNameList.sort()
      searchList.floorNoNameList.map((item) => {
        let itemarr = item.split("+")
        let prjId = itemarr[0]
        let name = itemarr[1]
        if (prjId == this.state.projectId) {
          if (searchList.prjFNNameList.indexOf(name) == -1) {
            searchList.prjFNNameList.push(name)
          }
        }
      })
    }
    this.setState({
      searchList: searchList
    })
    console.log("🚀 ~ file: ChildOneFlat.js ~ line 597 ~ ChildOne ~ searchList", searchList)

  }

  //生产线筛选
  requestDataProductLine = () => {
    const uri = Url.url + 'Factories/' + Url.Fid + 'ProductionLine';
    fetch(uri, {
      credentials: 'include',
    }).then(resProductLine => {
      return resProductLine.json()
    }).then(resDataProductLine => {
      this.setState({
        resDataProductLine: resDataProductLine,
      })
    })
  }

  //项目筛选
  requestDataProject = () => {
    const uri = Url.url + Url.factory + Url.Fid + 'GetFactoryProjects';
    console.log("🚀 ~ file: ChildOneFlat.js ~ line 909 ~ ChildOne ~ uri", uri)
    console.log("🚀 ~ file: ChildOneFlat.js ~ line 934 ~ ChildOne ~ fetch ~ url.indexOf('PostProduceTaskCompStatelistPage') != -1", url.indexOf('PostProduceTaskCompStatelistPage') != -1)

    fetch(uri, {
      credentials: 'include',
    }).then(resProject => {
      return resProject.json()
    }).then(resDataProject => {
      searchList.projectNameList = []
      this.setState({
        resDataProject: resDataProject,
      }, () => {
        for (let i = 0; i < resDataProject.length; i++) {
          const item = resDataProject[i];
          if (item.projectState == '生产中') {
            if (url.indexOf('PostProduceTaskCompStatelistPage') != -1) {
              if (searchList.projectNameList.indexOf(item.projectId + '+' + item.projectName) == -1) {
                searchList.projectNameList.push(item.projectId + '+' + item.projectName)
              }
            } else {
              if (searchList.projectNameList.indexOf(item.projectName) == -1) {
                searchList.projectNameList.push(item.projectName)
              }
            }

            this.setState({
              searchList: searchList
            })
            console.log("🚀 ~ file: ChildOneFlat.js ~ line 948 ~ ChildOne ~ fetch ~ searchList", searchList)
            /*  projectList.push(item);
             this.setState({
               projectList: projectList
             }, () => {
             }); */
          }
        }
      })
    }).catch(err => {
      console.log("🚀 ~ file: ChildOneFlat.js ~ line 726 ~ ChildOne ~ fetch ~ err", err)
    })
    console.log("🚀 ~ file: ChildOneFlat.js ~ line 713 ~ ChildOne ~ fetch ~ searchList", searchList)

  }

  //构件类型筛选
  requestDataCompType = () => {
    const uri = Url.url + 'ComponentType';
    fetch(uri, {
      credentials: 'include',
    }).then(resCompType => {
      return resCompType.json()
    }).then(resDataCompType => {
      searchList.compTypeNameList = []
      resDataCompType.map((item, index) => {
        if (searchList.compTypeNameList.indexOf(item.compTypeName) == -1) {
          searchList.compTypeNameList.push(item.compname)
        }
      })
      this.setState({
        resDataCompType: resDataCompType,
      })
    })
  }

  //构件状态筛选
  requestDataBigState = () => {
    const uri = Url.url + 'Projects/' + Url.Fid + 'GetTPcProductionComstateDistinct';
    fetch(uri, {
      credentials: 'include',
    }).then(resBigState => {
      return resBigState.json()
    }).then(resDataBigState => {
      searchList.bigStateList = []
      resDataBigState.map((item, index) => {
        if (searchList.bigStateList.indexOf(item.bigstate + '+' + item.bigstatecn) == -1) {
          searchList.bigStateList.push(item.bigstate + '+' + item.bigstatecn)
        }
      })

      this.setState({
        resDataBigState: resDataBigState,
      })
    })
  }

  //库房库位筛选
  requestDataWareLib = () => {
    const uri = Url.url + 'Projects/' + Url.Fid + 'GetViewLibraryInfo';
    fetch(uri, {
      credentials: 'include',
    }).then(resWareLib => {
      return resWareLib.json()
    }).then(resDataWareLib => {
      searchList.warehouLibName = []
      searchList.warehouRomName = []
      resDataWareLib.map((item, index) => {
        if (searchList.compRoomNameList.indexOf(item.roomName) == -1) {
          searchList.compRoomNameList.push(item.roomName)
          item.lstLibraryInfo.map((libItem) => {
            if (searchList.compLibNameList.indexOf(item.roomName + '+' + libItem.libraryName) == -1) {
              searchList.compLibNameList.push(item.roomName + '+' + libItem.libraryName)
            }
          })
        }
      })
      this.setState({
        resDataCompType: resDataCompType,
      })
    })
  }

  //楼号筛选
  requestDataFloorNO = (projectId) => {
    const uri = Url.url + 'Projects/' + Url.Fid + projectId + '/FloorNos';
    console.log("🚀 ~ file: ChildOneFlat.js ~ line 1014 ~ ChildOne ~ uri", uri)
    fetch(uri, {
      credentials: 'include',
    }).then(resFloorNO => {
      return resFloorNO.json()
    }).then(resDataFloorNO => {
      console.log("🚀 ~ file: ChildOneFlat.js ~ line 1016 ~ ChildOne ~ fetch ~ resDataFloorNO", resDataFloorNO)
      searchList.floorNoNameList = []
      resDataFloorNO.map((item, index) => {
        if (searchList.floorNoNameList.indexOf(item.projectId + '+' + item.structurename) == -1) {
          searchList.floorNoNameList.push(item.projectId + '+' + item.structurename)
        }
      })
      searchList.floorNoNameList.map((item) => {
        let itemarr = item.split("+")
        let prjId = itemarr[0]
        let name = itemarr[1]
        if (prjId == this.state.projectId) {
          if (searchList.prjFNNameList.indexOf(name) == -1) {
            searchList.prjFNNameList.push(name)
          }
        }
      })
      this.setState({
        resDataFloorNO: resDataFloorNO,
      })
    })
  }

  //楼层筛选
  requestDataFloor = (projectId, floorNoId) => {
    const uri = Url.url + 'Projects/' + Url.Fid + projectId + '/FloorNos/' + floorNoId + '/Floors';
    fetch(uri).then(resFloors => {
      return resFloors.json()
    }).then(resDataFloors => {
      searchList.floorNameList = []
      resDataFloors.map((item, index) => {
        if (searchList.floorNameList.indexOf(this.state.projectId + '+' + this.state.FloorNOName + '+' + item.structurename) == -1) {
          searchList.floorNameList.push(this.state.projectId + '+' + this.state.FloorNOName + '+' + item.structurename)
        }
      })
      console.log("🚀 ~ file: ChildOneFlat.js ~ line 734 ~ ChildOne ~ resDataFloors.map ~ searchList.floorNameList", searchList.floorNameList)

      searchList.FNFNameList = []
      searchList.floorNameList.map((item) => {
        let itemarr = item.split("+")
        let prjId = itemarr[0]
        let fnnName = itemarr[1]
        let name = itemarr[2]
        if (prjId == projectId) {
          if (fnnName == this.state.FloorNOName) {
            if (searchList.FNFNameList.indexOf(name) == -1) {
              searchList.FNFNameList.push(name)
            }
          }
        }
      })

      this.setState({
        resDataFloors: resDataFloors,
        searchList: searchList
      })
    })
  }

  SearchPostproductLine = (productLine, editEmployeeName, projectName, searchText) => {
    const url = Url.url + 'Produce/' + 'ViewProduceTask';
    //const url = 'http://localhost:5000/api/Produce/ViewProduceTask';
    //const urlprops = navigation.getParam('url')
    console.log("🚀 ~ file: ChildOneFlat.js ~ line 316 ~ ChildOne ~ this.state.selectedIndexTask", this.state.selectedIndexTask)

    let data = {
      "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
      "formCode": searchText,
      "productLineName": productLine,
      "editEmployeeName": editEmployeeName,
      "projectName": projectName,
      "state": this.state.selectedIndexTask == 0 ? "" : (this.state.selectedIndexTask == 1 ? "提交" : '未提交')
    }
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    }).then(res => {
      console.log("🚀 ~ file: ChildOneFlat.js ~ line 627 ~ ChildOne ~ res.status", res.status)
      return res.json();
    }).then(resData => {
      this.setState({
        resData: resData,
        isLoading: false,
        pageLoading: false,
      })
    }).catch(err => {
    })
  }

  SearchPostproject = (project) => {
    const { navigation } = this.props;
    const url = navigation.getParam('url');
    let urlpost = ''
    if (url.indexOf("GetProduceProject") != -1) {
      urlpost = Url.url + 'Produce/' + 'PostProduceProject';
    } else if (url.indexOf("GetQualityFinProductsProject") != -1) {
      urlpost = Url.url + 'Quality/' + 'PostQualityFinProductsProject';
    }
    let data = {
      "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
      "projectName": project,
    }
    console.log("🚀 ~ file: ChildOneFlat.js ~ line 807 ~ ChildOne ~ data", data)
    fetch(urlpost, {
      method: 'POST',
      credentials: 'include',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    }).then(res => {
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: ChildOneFlat.js ~ line 818 ~ ChildOne ~ resData", resData)
      this.setState({
        resData: resData,
        isLoading: false,
        pageLoading: false,
      })
    }).catch(err => {
      console.log("🚀 ~ file: ChildOneFlat.js ~ line 825 ~ ChildOne ~ err", err)

    })
  }

  SearchPostStockLoaoutWare = (project, planformCode) => {
    const { selectedIndexLoaout } = this.state;
    const url = Url.url + 'Stock/' + 'PostStockLoaoutWare';
    let data = {
      "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),

      "state": "",
      "planformCode": planformCode,
      "projectName": project
    }
    if (selectedIndexLoaout == '0') {
      data.state = ""
    } else if (selectedIndexLoaout == '1') {
      data.state = "已完成"
    } else {
      data.state = "未完成"
    }
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.setState({
        resData: resData,
        isLoading: false,
        pageLoading: false,
      })
    }).catch(err => {

    })
  }

  SearchPostDailyRecordProject = (project) => {
    const url = Url.url + 'Produce/' + 'PostProduceDailyRecordProject';
    const { selectedIndex } = this.state
    let data = {
      "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
      "produceDate": "",
      "projectName": project,
    }
    this.setState({ pageLoading: true })
    if (selectedIndex == '0') {
      data.produceDate = ToDay
    } else if (selectedIndex == '1') {
      data.produceDate = YesterDay
    } else {
      data.produceDate = BeforeYesterDay
    }
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.setState({
        resData: resData,
        isLoading: false,
        pageLoading: false,
      })
    }).catch(err => {

    })
  }

  SearchPostTaskList = (compTypeName, ProjectId, floorNoName, floorName, searchText) => {
    const { taskId, bigstate } = this.state
    const { navigation } = this.props;
    const urlprops = navigation.getParam('url')
    const url = Url.url + 'Produce/' + 'ViewProduceTaskCompStatelist';
    //const url = 'http://localhost:5000/api/Produce/ViewProduceTaskCompStatelist';

    let data = {
      "task_id": taskId,
      "state": bigstate,
      "compCode": searchText,
      "compTypeName": compTypeName,
      "ProjectId": ProjectId,
      "floorNoName": floorNoName || "",
      "floorName": floorName || ""
    }
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.setState({
        resData: resData,
        isLoading: false,
        pageLoading: false,
      })
    })
  }

  SearchPostGetProduceDailyRecord = (compTypeName, floorNoName, floorName, searchText) => {
    const { projectId, selectedIndex } = this.state
    const { navigation } = this.props;
    const urlprops = navigation.getParam('url')
    const url = Url.url + 'Produce/' + 'GetProduceDailyRecord';
    //const url = 'http://localhost:5000/api/Produce/ViewProduceTaskCompStatelist';
    this.setState({ pageLoading: true })
    let data = {
      "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
      "projectId": projectId,
      "produceDate": "",
      "compTypeName": compTypeName,
      "floorNoName": floorNoName || "",
      "floorName": floorName || "",
      "compCode": searchText
    }

    if (selectedIndex == '0') {
      data.produceDate = ToDay
    } else if (selectedIndex == '1') {
      data.produceDate = YesterDay
    } else {
      data.produceDate = BeforeYesterDay
    }

    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.setState({
        resData: resData,
        isLoading: false,
        pageLoading: false,
      })
    })
  }

  SearchPostProducedComponents = (data) => {
    const { factoryId, projectId } = this.state
    const url = Url.url + 'Produce/' + 'ViewProduceComponentState';
    //const url = 'http://localhost:5000/api/Produce/ViewProduceTaskCompStatelist';
    this.setState({ pageLoading: true })
    /* let data = {
      "factoryId": factoryId,
      "projectId": projectId,
      "warehouRomName": compRoomName,
      "warehouLibName": compLibName,
      "bigState": bigState,
      "compCode": searchText,
      "designType": designType,
      "compTypeName": compTypeName,
      "floorNoName": floorNoName || "",
      "floorName": floorName || ""
    } */
    /* if (designType == '' && bigState == '' && compTypeName == '' && compRoomName == '' && compLibName == '' && floorNoName == '' && floorName == '' && searchText == '') {
      this.refreshing()
      return
    } */
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    }).then(res => {
      return res.json();
    }).then(resData => {
      length = resData.length
      this.setState({
        resData: resData,
        isLoading: false,
        pageLoading: false,
      })
    })
  }

  SearchPostQualityFinProducts = () => {
    const { factoryId, projectId } = this.state
    const url = Url.url + 'Quality/' + 'PostQualityFinProducts';
    //const url = 'http://localhost:5000/api/Produce/ViewProduceTaskCompStatelist';
    this.setState({ pageLoading: true })
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    }).then(res => {
      return res.json();
    }).then(resData => {
      length = resData.length;
      this.setState({
        resData: resData,
        isLoading: false,
        pageLoading: false,
      })
    }).catch(err => {
    })
  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  //搜索栏搜索功能
  TextChange = (text) => {
    const { compTypeName, projectId, designTypeName, compRoomName, compLibName, bigStateName, FloorNOName, FloorsName, productLineName, selectedIndex, editEmployeeName, projectName } = this.state
    let i = 0;
    let searchData = [];
    const { navigation } = this.props;
    const url = navigation.getParam('url');
    this.setState({ search: text });
    // data.search = text
    // pageNo = 1
    // data.pageIndex = 1
    // this.setState({ resData: [], page: 1 })
    // this.requestData1()
    // if (url.indexOf("PostProduceTaskCompStatelistPage") != -1) {
    //   data.compCode = text
    //   pageNo = 1
    //   data.pageIndex = 1
    //   this.setState({ resData: [], page: 1 })
    //   this.requestData1()
    // } else if (url.indexOf('PostStockLoaoutWarePage') != -1) {
    //   data.planformCode = text
    //   pageNo = 1
    //   data.pageIndex = 1
    //   this.setState({ resData: [], page: 1 })
    //   this.requestData1()
    // } else if (url.indexOf("PostProduceComponentStatePage") != -1) {
    //   data.compCode = text
    //   pageNo = 1
    //   data.pageIndex = 1
    //   this.setState({ resData: [], page: 1 })
    //   this.requestData1()
    // } else if (url.indexOf('PostProduceTaskPage') != -1) {
    //   data.formCode = text
    //   pageNo = 1
    //   data.pageIndex = 1
    //   this.setState({ resData: [], page: 1 })
    //   this.requestData1()
    // } else if (url.indexOf('PostProduceDailyRecordPage') != -1) {
    //   data.compCode = text
    //   pageNo = 1
    //   data.pageIndex = 1
    //   this.setState({ resData: [], page: 1 })
    //   this.requestData1()
    // } else if (url.indexOf("PostQualityFinProductsPage") != -1) {
    //   data.compCode = text
    //   pageNo = 1
    //   data.pageIndex = 1
    //   this.setState({ resData: [], page: 1 })
    //   this.requestData1()
    // } else if (url.indexOf('Project') != -1) {
    //   data.projectName = text
    //   pageNo = 1
    //   data.pageIndex = 1
    //   this.setState({ resData: [], page: 1 })
    //   this.requestData1()
    // } else {
    //   this.state.resData.map((item, index) => {
    //     if (item.projectName.indexOf(text) != -1) {
    //       searchData[i++] = item;
    //     }
    //   })
    //   if (text) {
    //     this.setState({ resData: searchData })
    //   }
    // }
  }

  TextChange1 = (text) => {
    const { compTypeName, projectId, designTypeName, compRoomName, compLibName, bigStateName, FloorNOName, FloorsName, productLineName, selectedIndex, editEmployeeName, projectName } = this.state
    let i = 0;
    let searchData = [];
    const { navigation } = this.props;
    const url = navigation.getParam('url');
    this.setState({ search: text });
    if (url.indexOf('PostProduceProjectPage') != -1) {
      data.pageIndex = 1
      data.projectName = text
      console.log("🚀 ~ file: ChildOneFlat.js ~ line 1237 ~ ChildOne ~ data", data)
      this.setState({ resData: [] })
      this.requestData1()
    }
  }

  TextChangeDesignType = (text) => {
    const { compTypeName, designTypeNamem, bigStateName, FloorNOName, FloorsName, search } = this.state
    this.setState({ designTypeName: text });
    this.SearchPostProducedComponents(text, bigStateName, compTypeName, compRoomName, compLibName, FloorNOName, FloorsName, search)
  }

  //日期按钮组顶栏
  ButtonGroupday = () => {
    const buttons = ['今日', '昨日', '前日']
    const { selectedIndex } = this.state
    return (
      <View style={{ flex: 1.3, flexDirection: 'row', backgroundColor: 'white' }}>
        <View style={{ flexDirection: 'row', flex: 2, left: 10 }}>
          {
            buttons.map((item, index) => {
              return (
                <Button
                  buttonStyle={{
                    borderRadius: -10,
                    borderBottomColor: selectedIndex == index ? '#419fff' : 'transparent',
                    borderBottomWidth: selectedIndex == index ? 2 : 0,
                    // width: 48,
                    height: 50,
                    marginLeft: 10
                  }}
                  containerStyle={{
                    borderColor: this.state.selectedIndex == index ? '#419fff' : 'transparent',
                    //borderBottomWidth: 0,
                  }}
                  titleStyle={{
                    fontFamily: 'STHeitiSC-Light',
                    textAlign: 'center',
                    fontSize: 17,
                    fontWeight: "600",
                    //fontSize: RFT * 3.3,
                    color: this.state.selectedIndex == index ? '#419fff' : '#333333',
                  }}
                  type="clear"
                  title={item}
                  onPress={() => {
                    this.updateIndex(index)
                  }} />
              )
            })
          }
        </View>

        {/*  <Button
          title='筛选'
          type='clear'
          containerStyle={{ top: 5 }}
          style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}
          titleStyle={{ color: '#535c68' }}
          onPress={() => {
            this.setState({
              isSideOpen: true
            })

          }}
          icon={<Icon
            name='filter'
            type='antdesign'
            size={24}
            color='#419FFF' />}
        /> */}

      </View>
    )
  }

  //日期按钮组顶栏
  ButtonGroupType = () => {
    const buttons = ['构件明细', '构件生产统计', '前日']
    const { selectedIndex } = this.state
    return (
      <View style={{ flex: 1.3, flexDirection: 'row', backgroundColor: 'white' }}>
        <View style={{ flexDirection: 'row', flex: 2, left: 10 }}>
          {
            buttons.map((item, index) => {
              return (
                <Button
                  buttonStyle={{
                    borderRadius: -10,
                    borderBottomColor: selectedIndex == index ? '#419fff' : 'transparent',
                    borderBottomWidth: selectedIndex == index ? 2 : 0,
                    // width: 48,
                    height: 50,
                    marginLeft: 10
                  }}
                  containerStyle={{
                    borderColor: this.state.selectedIndex == index ? '#419fff' : 'transparent',
                    //borderBottomWidth: 0,
                  }}
                  titleStyle={{
                    fontFamily: 'STHeitiSC-Light',
                    textAlign: 'center',
                    fontSize: 17,
                    fontWeight: "600",
                    //fontSize: RFT * 3.3,
                    color: this.state.selectedIndex == index ? '#419fff' : '#333333',
                  }}
                  type="clear"
                  title={item}
                  onPress={() => {
                    this.updateIndex(index)
                  }} />
              )
            })
          }
        </View>

        {/*  <Button
          title='筛选'
          type='clear'
          containerStyle={{ top: 5 }}
          style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}
          titleStyle={{ color: '#535c68' }}
          onPress={() => {
            this.setState({
              isSideOpen: true
            })

          }}
          icon={<Icon
            name='filter'
            type='antdesign'
            size={24}
            color='#419FFF' />}
        /> */}

      </View>
    )
  }

  //任务单按钮顶栏
  TaskStateButton = () => {
    const buttons = ['全部', '已提交', '未提交']
    const { selectedIndexTask } = this.state
    console.log("🚀 ~ file: ChildOneFlat.js ~ line 1819 ~ ChildOne ~ selectedIndexTask", selectedIndexTask)
    return (
      <View style={{ flex: 1.3, flexDirection: 'row', backgroundColor: 'white' }}>
        <View style={{ flexDirection: 'row', flex: 2, left: 10 }}>
          {
            buttons.map((item, index) => {
              return (
                <Button
                  buttonStyle={{
                    borderRadius: -10,
                    borderBottomColor: selectedIndexTask == index ? '#419fff' : 'transparent',
                    borderBottomWidth: selectedIndexTask == index ? 2 : 0,
                    // width: 48,
                    height: 50,
                    marginLeft: 10
                  }}
                  containerStyle={{
                    borderColor: this.state.selectedIndexTask == index ? '#419fff' : 'transparent',
                    //borderBottomWidth: 0,
                  }}
                  titleStyle={{
                    fontFamily: 'STHeitiSC-Light',
                    textAlign: 'center',
                    fontSize: 17,
                    fontWeight: "600",
                    //fontSize: RFT * 3.3,
                    color: this.state.selectedIndexTask == index ? '#419fff' : '#333333',
                  }}
                  type="clear"
                  title={item}
                  onPress={() => {
                    this.updateIndexTask(index)
                  }} />
              )
            })
          }
        </View>

        {/* <Button
          title='筛选'
          type='clear'
          containerStyle={{ top: 5 }}
          style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}
          titleStyle={{ color: '#535c68' }}
          onPress={() => {
            this.setState({
              isSideOpen: true
            })

          }}
          icon={<Icon
            name='filter'
            type='antdesign'
            size={24}
            color='#419FFF' />}
        /> */}

      </View>
    )
  }

  //出库按钮顶栏
  LoaoutStateButton = () => {
    const buttons = ['全部', '已发货', '未发货']
    const { selectedIndexLoaout } = this.state
    return (
      <View style={{ flex: 1.3, flexDirection: 'row', backgroundColor: 'white' }}>
        <View style={{ flexDirection: 'row', flex: 2, left: 10 }}>
          {
            buttons.map((item, index) => {
              return (
                <Button
                  buttonStyle={{
                    borderRadius: -10,
                    borderBottomColor: selectedIndexLoaout == index ? '#419fff' : 'transparent',
                    borderBottomWidth: selectedIndexLoaout == index ? 2 : 0,
                    // width: 48,
                    height: 50,
                    marginLeft: 10
                  }}
                  containerStyle={{
                    borderColor: this.state.selectedIndexLoaout == index ? '#419fff' : 'transparent',
                    //borderBottomWidth: 0,
                  }}
                  titleStyle={{
                    fontFamily: 'STHeitiSC-Light',
                    textAlign: 'center',
                    fontSize: 17,
                    fontWeight: "600",
                    //fontSize: RFT * 3.3,
                    color: this.state.selectedIndexLoaout == index ? '#419fff' : '#333333',
                  }}
                  type="clear"
                  title={item}
                  onPress={() => {
                    this.updateIndexLoaout(index)
                  }} />
              )
            })
          }
        </View>

        {/* <Button
          title='筛选'
          type='clear'
          containerStyle={{ top: 5 }}
          style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}
          titleStyle={{ color: '#535c68' }}
          onPress={() => {
            this.setState({
              isSideOpen: true
            })

          }}
          icon={<Icon
            name='filter'
            type='antdesign'
            size={24}
            color='#419FFF' />}
        />
 */}
      </View>
    )
  }

  //顶部记录与筛选按钮
  Top = () => {
    /* if (url.indexOf('/40/') == -1) {
      length = this.state.resData.length
    } */
    let isShow = false
    if ((url.indexOf('PostProduceDailyRecordProjectPage2') != -1 ||
      url.indexOf('PostProduceTaskPage') != -1 ||
      (url.indexOf('PostStockLoaoutWarePage') != -1 && title.indexOf('成品出库') == -1) ||
      url.indexOf('Pool') != -1 ||
      url.indexOf('PostProduceReleasePage2') != -1 ||
      url.indexOf('PostQualityHidcheckProjectPage2') != -1 ||
      url.indexOf('PostQualityHidcheckProjectPage2') != -1 ||
      url.indexOf('PostQualityReFinProductsProjectPage') != -1 ||
      url.indexOf('PostQualityScrapProjectPage') != -1
    ) && sonUrl == '') {
      isShow = false
    }

    if (sonUrl.indexOf('PostWaitingPoolCompPage') != -1 ||
      sonUrl.indexOf('PostQualityHidcheckPage') != -1 ||
      sonUrl.indexOf('PostQualityReFinProductsPage2') != -1 ||
      sonUrl.indexOf('PostQualityReFinProductsPage2') != -1 ||
      sonUrl.indexOf('PostQualityScrapPage') != -1
    ) {
      isShow = false
    }
    /* if (url.indexOf('PostProduceDailyRecordProjectPage2') != -1 && sonUrl == '' ||
      url.indexOf('PostProduceTaskPage') != -1 ||
      (url.indexOf('PostStockLoaoutWarePage') != -1 && title.indexOf('成品出库') == -1)) {
      return
    } else { */
    if (isShow) {
      return (
        <View style={{ flex: 1, flexDirection: 'row', backgroundColor: 'white', justifyContent: 'flex-end', alignContent: 'flex-end' }}>
          {/* <Button
            type='clear'
            title={this.state.length + '条记录'}
            titleStyle={{ color: '#535c68', fontSize: 16, textAlign: 'center' }}
            style={{ flex: 1 }}
            containerStyle={{ flex: 3, left: width * 0.08, top: 1 }}
          /> */}
          {!isShow ?
            <View style={{ flex: 1 }}></View> :
            <Button
              title='筛选'
              type='clear'
              containerStyle={{ width: 80, top: 1, alignContent: 'flex-end', justifyContent: 'flex-end' }}
              style={{ flex: 1, justifyContent: 'flex-end' }}
              titleStyle={{ color: '#535c68', fontSize: 16 }}
              onPress={() => {
                this.setState({
                  isSideOpen: true
                })
              }}
              icon={<Icon
                name='filter'
                type='antdesign'
                color='#419FFF' />}
            />
          }

        </View>
      )
    }

    /* } */

  }

  //按钮组触发函数
  updateIndex(selectedIndex) {
    const { navigation } = this.props;
    const url = navigation.getParam('url')
    //console.log(url.substring(0, url.length - 10));
    this.setState({
      selectedIndex: selectedIndex,
      pageLoading: true
    })
    if (selectedIndex == 0) {
      data.produceDate = ToDay
      data.pageIndex = 1
      this.refreshing1()
      //this.requestData1();
    } else if (selectedIndex == 1) {
      data.produceDate = YesterDay
      data.pageIndex = 1
      this.refreshing1()

    } else if (selectedIndex == 2) {
      data.produceDate = BeforeYesterDay
      data.pageIndex = 1
      this.refreshing1()

    }
  }

  //按钮组触发函数-任务单
  updateIndexTask(selectedIndex) {
    const { navigation } = this.props;
    const { resData } = this.state;
    let stateData = [];
    const url = navigation.getParam('url')
    //console.log(url.substring(0, url.length - 10));
    this.setState({
      selectedIndexTask: selectedIndex,
      isSideOpen: false,
      pageLoading: true,
    })
    if (selectedIndex == 0) {
      data.state = ''
      data.pageIndex = 1
      this.setState({ page: "1", resData: [] })
      this.requestData1();
    } else if (selectedIndex == 1) {
      data.state = '提交'
      data.pageIndex = 1
      this.setState({ page: "1", resData: [] })
      this.requestData1();
    } else if (selectedIndex == 2) {
      data.state = '未提交'
      data.pageIndex = 1
      this.setState({ page: "1", resData: [] })
      this.requestData1();
    }
  }

  //按钮组触发函数-出库
  updateIndexLoaout(selectedIndex) {
    const { navigation } = this.props;
    const { resData } = this.state;
    let stateData = [];
    const url = navigation.getParam('url')
    //console.log(url.substring(0, url.length - 10));
    this.setState({
      selectedIndexLoaout: selectedIndex,
      pageLoading: true,
    })
    if (selectedIndex == 0) {
      data.state = ''
      data.pageIndex = 1
      this.setState({
        resData: [],
        page: 1
      })
      this.requestData1();
    } else if (selectedIndex == 1) {
      data.state = '已完成'
      data.pageIndex = 1
      this.setState({
        resData: [],
        page: 1
      })
      this.requestData1();
    } else if (selectedIndex == 2) {
      data.state = '未完成'
      data.pageIndex = 1
      this.setState({
        resData: [],
        page: 1
      })
      this.requestData1();
    }
  }

  SideSearchItem = (searchname, Picker, resData) => {
    return (
      <View style={{ marginBottom: 10 }}>
        <View style={{ marginLeft: 6, marginTop: 8 }} >
          <Text style={{ fontSize: 16 }}>{searchname}</Text>
        </View>
        <View style={{ height: 40, marginLeft: width * 0.005, marginRight: width * 0.08, backgroundColor: "#f8f8f8", borderRadius: 5, marginTop: 10 }}>
          <Picker
            mode='dropdown'
            selectedValue={productLinePicker}
            style={{ flex: 1, fontSize: 14 }}
            itemStyle={{ fontSize: 14 }}
            onValueChange={(value, position) => {
              let productLineId = ''
              if (value != '请选择') {
                productLineId = resDataProductLine[position - 1].rowguid || ''
                this.SearchPostproductLine(value)
                this.setState({
                  productLinePicker: value,
                  productLineId: productLineId,
                  productLineName: value
                })
              } else {
                this.setState({
                  productLinePicker: value,
                  productLineId: "",
                  productLineName: ""
                })
              }

            }}
          >
            <Picker.Item label={'请选择生产线'} value={'请选择'} />
            {resData.map((item) => {
              return (
                <Picker.Item label={item.productLineName} value={item.productLineName} />
              )
            })}
          </Picker>
        </View>
      </View>
    )
  }

  SideSearchFunc = (value, position, searchType) => {
    if (searchType == '生产线筛选') {
      let productLineId = ''
      if (value != '请选择') {
        productLineId = resDataProductLine[position - 1].rowguid || ''
        this.SearchPostproductLine(value)
        this.setState({
          productLinePicker: value,
          productLineId: productLineId,
          productLineName: value
        })
      } else {
        this.setState({
          productLinePicker: value,
          productLineId: "",
          productLineName: ""
        })
      }
    }
  }

  BottomList = () => {
    const { bottomVisible, bottomData, resDataFloorNO, projectId } = this.state

    return (
      <BottomSheet
        isVisible={bottomVisible}
        onRequestClose={() => {
          this.setState({
            bottomVisible: false
          })
        }}
        onBackdropPress={() => {
          this.setState({
            bottomVisible: false
          })
        }}
      //modalProps={{ style: { height: 100 }, }}
      >
        <ScrollView style={styles.bottomsheetScroll}>
          {
            bottomData.map((item, index) => {
              let title = ''
              if (sideType == 'bigState') {
                let itemarr = item.split("+")
                title = itemarr[1]
              } else if (sideType == 'compRoom') {
                title = item
              } else if (sideType == 'compLib') {
                title = item
              } else if (sideType == 'floorNo') {
                title = item
              } else if (sideType == 'floor') {
                title = item
              } else if (sideType == 'productline') {
                title = item
              } else if (sideType == 'editEmployeeName') {
                title = item
              } else if (sideType == 'compType') {
                title = item
              } else if (sideType == 'project') {
                if (url.indexOf("PostProduceTaskCompStatelistPage") != -1) {
                  let itemarr = item.split("+")
                  title = itemarr[1]
                } else {
                  title = item
                }
              } else {
                title = item
              }
              //title = item
              return (
                <TouchableHighlight
                  onPress={() => {
                    if (sideType == 'project') {
                      if (url.indexOf("PostProduceTaskCompStatelistPage") != -1) {
                        let itemarr = item.split("+")
                        console.log("🚀 ~ file: ChildOneFlat.js ~ line 1841 ~ ChildOne ~ bottomData.map ~ itemarr", itemarr)
                        this.setState({ projectId: itemarr[0] })
                        this.setState({
                          projectId: itemarr[0],
                          projectName: itemarr[1],
                          projectPicker: itemarr[1]
                        })
                        this.requestDataFloorNO(itemarr[0])
                      } else {
                        this.setState({
                          projectName: item,
                          projectPicker: item
                        })
                      }
                    }
                    if (sideType == 'compType') {
                      this.setState({
                        compTypeName: item,
                        compTypePicker: item
                      })
                    }
                    if (sideType == 'bigState') {
                      let itemarr = item.split("+")
                      this.setState({
                        bigStateId: itemarr[0],
                        bigStateName: itemarr[1],
                        bigStatePicker: itemarr[1]
                      })
                    }
                    if (sideType == 'compRoom') {
                      searchList.roomLibNameList = []
                      searchList.compLibNameList.map((itemcomp) => {
                        let itemcomparr = itemcomp.split("+")
                        let compRoom = itemcomparr[0]
                        let compLib = itemcomparr[1]
                        if (compRoom == item) {
                          if (searchList.roomLibNameList.indexOf(compLib) == -1) {
                            searchList.roomLibNameList.push(compLib)
                          }
                        }
                      })
                      this.setState({
                        compRoomName: item,
                        compRoomPicker: item,
                        searchList: searchList
                      })
                      console.log("🚀 ~ file: ChildOneFlat.js ~ line 1483 ~ ChildOne ~ bottomData.map ~ searchList", searchList)
                    }
                    if (sideType == 'floorNo') {
                      this.setState({
                        FloorNOPicker: item,
                        FloorNOName: item,
                        FloorsName: '',
                        FloorsPicker: '',
                      })
                      this.state.resDataFloorNO.map((floorNoItem) => {
                        if (floorNoItem.structurename == item) {
                          this.requestDataFloor(this.state.projectId, floorNoItem.rowguid)
                        }
                      })
                    }
                    if (sideType == 'floor') {
                      this.setState({
                        FloorsPicker: item,
                        FloorsId: "",
                        FloorsName: item
                      })
                    }
                    if (sideType == 'compLib') {
                      this.setState({
                        compLibName: item,
                        compLibPicker: item
                      })
                    }
                    if (sideType == 'productline') {
                      this.setState({
                        productLineName: item,
                        productLinePicker: item
                      })
                    }
                    if (sideType == 'editEmployeeName') {
                      this.setState({
                        editEmployeeName: item,
                        editEmployeeNamePicker: item
                      })
                    }
                    if (sideType == 'insresults') {
                      this.setState({
                        InsresultsName: item,
                        InsresultsPicker: item
                      }, () => {
                        this.forceUpdate()
                      })
                    }
                    this.setState({
                      bottomVisible: false
                    })
                  }}
                >
                  <View>

                    <BottomItem backgroundColor='white' color="#333" title={title} />
                  </View>
                </TouchableHighlight>
              )
            })
          }
        </ScrollView>
        <TouchableHighlight
          onPress={() => {
            this.setState({ bottomVisible: false })
          }}>
          <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
        </TouchableHighlight>
      </BottomSheet>

    )
  }

  isBottomVisible = (data) => {
    if (typeof (data) != "undefined") {
      if (data.length > 0) {
        this.setState({ bottomVisible: true, bottomData: data })
      } else {
        this.toast.show("无数据")
      }
    } else {
      this.toast.show("无数据")
    }
  }

  menu = () => {
    const {
      isCompRoomPickerVisible, compRoomId, compRoomName, compRoomPicker,
      isCompLibPickerVisible, compLibId, compLibName, compLibPicker,
      isBigStatePickerVisible, bigStateId, bigStateName, bigStatePicker,
      isDesignTypePickerVisible, designTypeId, designTypeName, designTypePicker,
      isproductLinePickerVisible, productLineId, productLineName, productLinePicker, resDataProductLine,
      isProjectPickerVisible, projectPicker, pickerValue, projectId, projectName,
      isFloorNOPickerVisible, resDataFloorNO, FloorNOPicker, FloorNOId, FloorNOName,
      isFloorsPickerVisible, resDataFloors, FloorsId, FloorsName, FloorsPicker,
      isCompTypePickerVisible, resDataCompType, compTypeId, compTypeName, compTypePicker,
      isInsresultsPickerVisible, resDataInsresults, InsresultsName, InsresultsPicker,
      isEditEmployeeNamePickerVisible, editEmployeeName, editEmployeeNamePicker,
      pageLoading, search } = this.state
    let menu = <View></View>
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const url = navigation.getParam('url')
    //任务单
    if (url.indexOf("PostProduceTaskPage") != -1) {
      menu =
        <View style={styles.sideMenu_Main} >
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          <View style={{ flex: 6 }}>
            {/* 生产线 */}
            <View style={styles.sideMenu_Bottom}>
              <TouchableHighlight
                underlayColor={"lightgray"}
                onPress={() => {
                  sideType = 'productline'
                  this.setState({ sideType: 'productline' })
                  this.isBottomVisible(searchList.productLineNameList)
                }}
                onLongPress={() => {
                  Alert.alert("提示", "确定需要重置筛选条件吗？", [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '确定',
                    onPress: () => {
                      this.setState({
                        productLineName: '',
                        productLinePicker: '',
                      })
                    }
                  }])
                }}>
                <View style={styles.sideMenu_Top1} >
                  <View style={styles.sideMenu_TitleView}>
                    <Text style={styles.sideMenu_Title}>{"生产线筛选"}</Text>
                  </View>
                  <View style={styles.sideMenu_DownView}>
                    <View style={styles.sideMenu_DownView_View}>
                      {
                        !this.state.productLineName ?
                          <Text style={styles.sideMenu_DownViewTitle}>{"展开"}</Text>
                          :
                          <Text numberOfLines={1} style={[styles.sideMenu_DownViewTitle, { color: '#419fff' }]}>{this.state.productLineName}</Text>
                      }
                      <Icon name='down' color='gray' iconStyle={styles.sideMenu_DownViewIcon} type='antdesign' ></Icon>

                    </View>
                  </View>
                </View>
              </TouchableHighlight>
            </View>
            {/* 项目 */}
            <View style={styles.sideMenu_Bottom}>
              <TouchableHighlight
                underlayColor={"lightgray"}
                onPress={() => {
                  sideType = 'project'
                  this.setState({ sideType: 'project' })
                  this.isBottomVisible(searchList.projectNameList)
                }}
                onLongPress={() => {
                  Alert.alert("提示", "确定需要重置筛选条件吗？", [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '确定',
                    onPress: () => {
                      this.setState({
                        projectName: '',
                        projectPicker: '',
                      })
                    }
                  }])
                }}>
                <View style={styles.sideMenu_Top1} >
                  <View style={styles.sideMenu_TitleView}>
                    <Text style={styles.sideMenu_Title}>{"项目筛选"}</Text>
                  </View>
                  <View style={styles.sideMenu_DownView}>
                    <View style={styles.sideMenu_DownView_View}>
                      {
                        !this.state.projectName ?
                          <Text style={styles.sideMenu_DownViewTitle}>{"展开"}</Text>
                          :
                          <Text numberOfLines={1} style={[styles.sideMenu_DownViewTitle, { color: '#419fff' }]}>{this.state.projectName}</Text>
                      }
                      <Icon name='down' color='gray' iconStyle={styles.sideMenu_DownViewIcon} type='antdesign' ></Icon>

                    </View>
                  </View>
                </View>
              </TouchableHighlight>

            </View>
            {/* 负责人 */}
            <View style={styles.sideMenu_Bottom}>
              <TouchableHighlight
                underlayColor={"lightgray"}
                onPress={() => {
                  sideType = 'editEmployeeName'
                  this.setState({ sideType: 'editEmployeeName' })
                  this.isBottomVisible(searchList.editEmployeeNameList)
                }}
                onLongPress={() => {
                  Alert.alert("提示", "确定需要重置筛选条件吗？", [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '确定',
                    onPress: () => {
                      this.setState({
                        editEmployeeName: '',
                        editEmployeeNamePicker: '',
                      })
                    }
                  }])
                }}>
                <View style={styles.sideMenu_Top1} >
                  <View style={styles.sideMenu_TitleView}>
                    <Text style={styles.sideMenu_Title}>{"负责人筛选"}</Text>
                  </View>
                  <View style={styles.sideMenu_DownView}>
                    <View style={styles.sideMenu_DownView_View}>
                      {
                        !this.state.editEmployeeName ?
                          <Text style={styles.sideMenu_DownViewTitle}>{"展开"}</Text>
                          :
                          <Text numberOfLines={1} style={[styles.sideMenu_DownViewTitle, { color: '#419fff' }]}>{this.state.editEmployeeName}</Text>
                      }
                      <Icon name='down' color='gray' iconStyle={styles.sideMenu_DownViewIcon} type='antdesign' ></Icon>

                    </View>
                  </View>
                </View>
              </TouchableHighlight>

            </View>
          </View>

          {/* 按钮组 */}
          <View style={[styles.sideMenu_Bottom, { flexDirection: 'row', justifyContent: 'center', marginTop: 20 }]}>
            <Button
              type='outline'
              title='重置'
              containerStyle={{ flex: 1, borderRadius: 0 }}
              buttonStyle={{ borderRadius: 0, borderColor: '#419fff' }}
              onPress={() => {
                this.setState({
                  productLineName: '',
                  productLinePicker: '',
                  projectName: '',
                  projectPicker: '',
                  editEmployeeName: '',
                  editEmployeeNamePicker: '',
                  page: 1,
                  resData: []
                })
                pageNo = 1
                data.pageIndex = 1
                data.productLineName = ""
                data.editEmployeeName = ""
                data.projectName = ""
                this.requestData1()
              }}
            />
            <Button
              //type='outline'
              title='搜索'
              containerStyle={{ flex: 1, borderRadius: 0 }}
              ViewComponent={LinearGradient}
              linearGradientProps={{ colors: ['#3FF0CC', 'rgba(65,159,255,0.80)'], start: { x: 1, y: 1 }, end: { x: 0, y: 1 } }}
              buttonStyle={{ borderRadius: 0, /* backgroundColor: '#419fff' */ }}
              onPress={() => {
                this.setState({
                  resData: [],
                  page: '1'
                })
                pageNo = 1
                data.pageIndex = 1
                data.productLineName = this.state.productLineName
                data.editEmployeeName = this.state.editEmployeeName
                data.projectName = this.state.projectName
                this.setState({ isSideOpen: false })
                this.requestData1()
              }}
            />
          </View>

          {this.BottomList()}
        </View>
      menu1 =
        <View style={styles.sideMenu_Main} >
          {/* 生产线 */}
          <View style={styles.sideMenu_Bottom}>
            <View style={styles.sideMenu_Top} >
              <Text style={styles.sideMenu_Title}>{"生产线筛选"}</Text>
            </View>
            <View style={styles.sideMenu_Content}>
              <Picker
                mode='dropdown'
                selectedValue={productLinePicker}
                style={styles.sideMenu_Picker}
                onValueChange={(value, position) => {
                  let productLineId = ''
                  if (value != '请选择') {
                    //productLineId = resDataProductLine[position - 1].rowguid || ''
                    this.SearchPostproductLine(value, search)
                    this.setState({
                      productLinePicker: value,
                      productLineId: productLineId,
                      productLineName: value
                    })
                  } else {
                    this.setState({
                      productLinePicker: value,
                      productLineId: "",
                      productLineName: ""
                    })
                    this.SearchPostproductLine("", search)
                  }

                }}
              >
                <Picker.Item label={'请选择生产线'} value={'请选择'} />
                {searchList.productLineNameList.map((item) => {
                  return (
                    <Picker.Item label={item} value={item} />
                  )
                })}
              </Picker>
            </View>
          </View>
          {/* 项目 */}
          <View style={styles.sideMenu_Bottom}>
            <View style={styles.sideMenu_Top} >
              <Text style={styles.sideMenu_Title}>{"项目筛选"}</Text>
            </View>
            <View style={styles.sideMenu_Content}>
              <Picker
                mode='dropdown'
                selectedValue={projectPicker}
                style={styles.sideMenu_Picker}
                onValueChange={(value, position) => {
                  let projectId = ""
                  if (value != "请选择") {
                    this.SearchPostproductLine(productLineName, editEmployeeName, value, search)
                    this.setState({
                      projectPicker: value,
                      projectName: value,
                    })
                  } else {
                    this.SearchPostproductLine(productLineName, editEmployeeName, "", search)
                    this.setState({
                      projectPicker: "",
                      projectName: "",
                    })
                  }
                }}
              >
                <Picker.Item label={'请选择项目'} value={'请选择'} />
                {searchList.projectNameList.map((item) => {
                  return (
                    <Picker.Item label={item} value={item} />
                  )
                })}
              </Picker>
            </View>
          </View>
          {/* 负责人 */}
          <View style={styles.sideMenu_Bottom}>
            <View style={styles.sideMenu_Top} >
              <Text style={styles.sideMenu_Title}>{"负责人筛选"}</Text>
            </View>
            <View style={styles.sideMenu_Content}>
              <Picker
                mode='dropdown'
                selectedValue={editEmployeeNamePicker}
                style={styles.sideMenu_Picker}
                onValueChange={(value, position) => {
                  if (value != '请选择') {
                    this.SearchPostproductLine(productLineName, value, projectName, search)
                    this.setState({
                      editEmployeeNamePicker: value,
                      editEmployeeName: value
                    })
                  } else {
                    this.setState({
                      editEmployeeNamePicker: '',
                      editEmployeeName: ''
                    })
                    this.SearchPostproductLine(productLineName, "", projectName, search)


                  }
                }}
              >
                <Picker.Item label={'请选择负责人'} value={'请选择'} />
                {searchList.editEmployeeNameList.map((item) => {
                  return (
                    <Picker.Item label={item} value={item} />
                  )
                })}
              </Picker>
            </View>
          </View>
        </View>;
    }
    //发货
    else if (url.indexOf("PostStockLoaoutWarePage") != -1) {
      menu =
        <View style={styles.sideMenu_Main} >
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          <View style={{ flex: 6 }}>
            {/* 项目类型1 */}
            <View style={styles.sideMenu_Bottom}>
              <TouchableHighlight
                underlayColor={"lightgray"}
                onPress={() => {
                  sideType = 'project'
                  console.log("🚀 ~ file: ChildOneFlat.js ~ line 2899 ~ ChildOne ~ render ~ searchList.projectNameList", searchList.projectNameList)

                  this.setState({ sideType: 'project' })
                  this.isBottomVisible(searchList.projectNameList)
                }}
                onLongPress={() => {
                  Alert.alert("提示", "确定需要重置筛选条件吗？", [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '确定',
                    onPress: () => {
                      this.setState({
                        projectName: '',
                        projectPicker: '',
                      })
                    }
                  }])
                }}>
                <View style={styles.sideMenu_Top1} >
                  <View style={styles.sideMenu_TitleView}>
                    <Text style={styles.sideMenu_Title}>{"项目筛选"}</Text>
                  </View>
                  <View style={styles.sideMenu_DownView}>
                    <View style={styles.sideMenu_DownView_View}>
                      {
                        !this.state.projectName ?
                          <Text style={styles.sideMenu_DownViewTitle}>{"展开"}</Text>
                          :
                          <Text numberOfLines={1} style={[styles.sideMenu_DownViewTitle, { color: '#419fff' }]}>{this.state.projectName}</Text>
                      }
                      <Icon name='down' color='gray' iconStyle={styles.sideMenu_DownViewIcon} type='antdesign' ></Icon>

                    </View>
                  </View>
                </View>
              </TouchableHighlight>
            </View>
          </View>

          {/* 按钮组 */}
          <View style={[styles.sideMenu_Bottom, { flexDirection: 'row', justifyContent: 'center', marginTop: 20 }]}>
            <Button
              type='outline'
              title='重置'
              containerStyle={{ flex: 1, borderRadius: 0 }}
              buttonStyle={{ borderRadius: 0, borderColor: '#419fff' }}
              onPress={() => {
                this.setState({
                  projectName: '',
                  projectPicker: '',
                  page: 1,
                  resData: []
                })
                pageNo = 1
                data.pageIndex = 1
                data.search = this.state.search

                this.requestData1()
              }}
            />
            <Button
              //type='outline'
              title='搜索'
              containerStyle={{ flex: 1, borderRadius: 0 }}
              ViewComponent={LinearGradient}
              linearGradientProps={{ colors: ['#3FF0CC', 'rgba(65,159,255,0.80)'], start: { x: 1, y: 1 }, end: { x: 0, y: 1 } }}
              buttonStyle={{ borderRadius: 0, }}
              onPress={() => {
                this.setState({
                  resData: [],
                  page: 1
                })
                pageNo = 1
                data.pageIndex = 1
                data.search = this.state.projectName
                this.setState({ isSideOpen: false })
                this.requestData1()
              }}
            />
          </View>

          {this.BottomList()}
        </View>
      menu1 =
        <View style={styles.sideMenu_Main} >
          {/* 项目 */}
          <View style={styles.sideMenu_Bottom}>
            <View style={styles.sideMenu_Top} >
              <Text style={styles.sideMenu_Title}>{"项目筛选"}</Text>
            </View>
            <View style={styles.sideMenu_Content}>
              <Picker
                mode='dropdown'
                selectedValue={projectPicker}
                style={styles.sideMenu_Picker}
                onValueChange={(value, position) => {
                  let projectId = ""
                  if (value != "请选择") {
                    this.SearchPostStockLoaoutWare(value, search)
                    this.setState({
                      projectPicker: value,
                      projectName: value,
                    })
                  } else {
                    this.SearchPostStockLoaoutWare("", search)
                    this.setState({
                      projectPicker: "",
                      projectName: "",
                    })
                  }
                }}
              >
                <Picker.Item label={'请选择项目'} value={'请选择'} />
                {searchList.projectNameList.map((item) => {
                  return (
                    <Picker.Item label={item} value={item} />
                  )
                })}
              </Picker>
            </View>
          </View>
        </View>
    }
    //任务单详情
    else if (url.indexOf("PostProduceTaskCompStatelistPage") != -1) {
      menu =
        <View style={styles.sideMenu_Main} >
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          <View style={{ flex: 6 }}>
            {/* 构件类型 */}
            <View style={styles.sideMenu_Bottom}>
              <TouchableHighlight
                underlayColor={"lightgray"}
                onPress={() => {
                  sideType = 'compType'
                  console.log("🚀 ~ file: ChildOneFlat.js ~ line 2899 ~ ChildOne ~ render ~ searchList.projectNameList", searchList.projectNameList)
                  this.setState({ sideType: 'compType' })
                  this.isBottomVisible(searchList.compTypeNameList)
                }}
                onLongPress={() => {
                  Alert.alert("提示", "确定需要重置筛选条件吗？", [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '确定',
                    onPress: () => {
                      this.setState({
                        compTypeName: '',
                        compTypePicker: '',
                      })
                    }
                  }])
                }}>
                <View style={styles.sideMenu_Top1} >
                  <View style={styles.sideMenu_TitleView}>
                    <Text style={styles.sideMenu_Title}>{"构件类型筛选"}</Text>
                  </View>
                  <View style={styles.sideMenu_DownView}>
                    <View style={styles.sideMenu_DownView_View}>
                      {
                        !this.state.compTypeName ?
                          <Text style={styles.sideMenu_DownViewTitle}>{"展开"}</Text>
                          :
                          <Text numberOfLines={1} style={[styles.sideMenu_DownViewTitle, { color: '#419fff' }]}>{this.state.compTypeName}</Text>
                      }
                      <Icon name='down' color='gray' iconStyle={styles.sideMenu_DownViewIcon} type='antdesign' ></Icon>

                    </View>
                  </View>
                </View>
              </TouchableHighlight>

            </View>
            {/* 项目 */}
            <View style={styles.sideMenu_Bottom}>
              <TouchableHighlight
                underlayColor={"lightgray"}
                onPress={() => {
                  sideType = 'project'
                  console.log("🚀 ~ file: ChildOneFlat.js ~ line 2899 ~ ChildOne ~ render ~ searchList.projectNameList", searchList.projectNameList)

                  this.setState({ sideType: 'project' })
                  this.isBottomVisible(searchList.projectNameList)
                }}
                onLongPress={() => {
                  Alert.alert("提示", "确定需要重置筛选条件吗？", [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '确定',
                    onPress: () => {
                      this.setState({
                        projectName: '',
                        projectPicker: '',
                      })
                    }
                  }])
                }}>
                <View style={styles.sideMenu_Top1} >
                  <View style={styles.sideMenu_TitleView}>
                    <Text style={styles.sideMenu_Title}>{"项目筛选"}</Text>
                  </View>
                  <View style={styles.sideMenu_DownView}>
                    <View style={styles.sideMenu_DownView_View}>
                      {
                        !this.state.projectName ?
                          <Text style={styles.sideMenu_DownViewTitle}>{"展开"}</Text>
                          :
                          <Text numberOfLines={1} style={[styles.sideMenu_DownViewTitle, { color: '#419fff' }]}>{this.state.projectName}</Text>
                      }
                      <Icon name='down' color='gray' iconStyle={styles.sideMenu_DownViewIcon} type='antdesign' ></Icon>

                    </View>
                  </View>
                </View>
              </TouchableHighlight>
            </View>
            {/* 楼号 */}
            <View style={styles.sideMenu_Bottom}>
              <TouchableHighlight
                underlayColor={"lightgray"}
                onPress={() => {
                  sideType = 'floorNo'
                  console.log("🚀 ~ file: ChildOneFlat.js ~ line 2899 ~ ChildOne ~ render ~ searchList.projectNameList", searchList.projectNameList)
                  this.setState({ sideType: 'floorNo' })
                  if (searchList.prjFNNameList.length == 0) {
                    this.toast.show('请先选择项目')
                    return
                  }
                  this.isBottomVisible(searchList.prjFNNameList)
                }}
                onLongPress={() => {
                  Alert.alert("提示", "确定需要重置筛选条件吗？", [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '确定',
                    onPress: () => {
                      this.setState({
                        FloorNOName: '',
                        FloorNOPicker: '',
                      })
                    }
                  }])
                }}>
                <View style={styles.sideMenu_Top1} >
                  <View style={styles.sideMenu_TitleView}>
                    <Text style={styles.sideMenu_Title}>{"楼号筛选"}</Text>
                  </View>
                  <View style={styles.sideMenu_DownView}>
                    <View style={styles.sideMenu_DownView_View}>
                      {
                        !this.state.FloorNOName ?
                          <Text style={styles.sideMenu_DownViewTitle}>{"展开"}</Text>
                          :
                          <Text numberOfLines={1} style={[styles.sideMenu_DownViewTitle, { color: '#419fff' }]}>{this.state.FloorNOName}</Text>
                      }
                      <Icon name='down' color='gray' iconStyle={styles.sideMenu_DownViewIcon} type='antdesign' ></Icon>

                    </View>
                  </View>
                </View>
              </TouchableHighlight>
            </View>
            {/* 楼层 */}
            <View style={styles.sideMenu_Bottom}>
              <TouchableHighlight
                underlayColor={"lightgray"}
                onPress={() => {
                  sideType = 'floor'
                  console.log("🚀 ~ file: ChildOneFlat.js ~ line 2899 ~ ChildOne ~ render ~ searchList.projectNameList", searchList.projectNameList)
                  this.setState({ sideType: 'floor' })

                  if (this.state.FloorNOName == '') {
                    this.toast.show("请先选择楼号")
                    return
                  }

                  if (searchList.FNFNameList.length == 0) {
                    this.toast.show("没有查询到楼层")
                    return
                  }
                  this.isBottomVisible(searchList.FNFNameList)
                }}
                onLongPress={() => {
                  Alert.alert("提示", "确定需要重置筛选条件吗？", [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '确定',
                    onPress: () => {
                      this.setState({
                        FloorsName: '',
                        FloorsPicker: '',
                      })
                    }
                  }])
                }}>
                <View style={styles.sideMenu_Top1} >
                  <View style={styles.sideMenu_TitleView}>
                    <Text style={styles.sideMenu_Title}>{"楼层筛选"}</Text>
                  </View>
                  <View style={styles.sideMenu_DownView}>
                    <View style={styles.sideMenu_DownView_View}>
                      {
                        !this.state.FloorsName ?
                          <Text style={styles.sideMenu_DownViewTitle}>{"展开"}</Text>
                          :
                          <Text numberOfLines={1} style={[styles.sideMenu_DownViewTitle, { color: '#419fff' }]}>{this.state.FloorsName}</Text>
                      }
                      <Icon name='down' color='gray' iconStyle={styles.sideMenu_DownViewIcon} type='antdesign' ></Icon>

                    </View>
                  </View>
                </View>
              </TouchableHighlight>

            </View>
          </View>

          {/* 按钮组 */}
          <View style={[styles.sideMenu_Bottom, { flexDirection: 'row', justifyContent: 'center', marginTop: 20 }]}>
            <Button
              type='outline'
              title='重置'
              containerStyle={{ flex: 1, borderRadius: 0 }}
              buttonStyle={{ borderRadius: 0, borderColor: '#419fff' }}
              onPress={() => {
                Alert.alert("提示", "确定需要重置筛选条件吗？", [{
                  text: '取消',
                  style: 'cancel'
                }, {
                  text: '确定',
                  onPress: () => {
                    this.setState({
                      compTypeName: '',
                      compTypePicker: '',
                      projectId: '',
                      projectName: '',
                      projectPicker: '',
                      FloorNOName: '',
                      FloorNOPicker: '',
                      FloorsName: '',
                      FloorsPicker: '',
                      page: '1',
                      resData: []
                    })
                    pageNo = 1
                    data.compTypeName = ''
                    data.ProjectId = ''
                    data.floorNoName = ''
                    data.floorName = ''
                    data.pageIndex = 1
                    this.requestData1()
                  }
                }])
              }}
            />
            <Button
              //type='outline'
              title='搜索'
              containerStyle={{ flex: 1, borderRadius: 0 }}
              ViewComponent={LinearGradient}
              linearGradientProps={{ colors: ['#3FF0CC', 'rgba(65,159,255,0.80)'], start: { x: 1, y: 1 }, end: { x: 0, y: 1 } }}
              buttonStyle={{ borderRadius: 0 }}
              onPress={() => {
                this.setState({ resData: [], page: "1" })
                pageNo = 1
                data.compTypeName = this.state.compTypeName
                data.ProjectId = this.state.projectId
                data.floorNoName = this.state.FloorNOName
                data.floorName = this.state.FloorsName
                data.search = this.state.search
                data.pageIndex = 1
                this.setState({ isSideOpen: false })
                this.requestData1()
              }}
            />
          </View>

          {this.BottomList()}
        </View>
      menu1 =
        <View style={styles.sideMenu_Main} >
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          {/* 构件类型 */}
          <View style={styles.sideMenu_Bottom}>
            <View style={styles.sideMenu_Top} >
              <Text style={styles.sideMenu_Title}>{"构件类型筛选"}</Text>
            </View>
            <View style={styles.sideMenu_Content}>
              <Picker
                mode='dropdown'
                selectedValue={compTypePicker}
                style={styles.sideMenu_Picker}
                onValueChange={(value) => {
                  let compTypeId = ''
                  if (value != '请选择') {
                    this.SearchPostTaskList(value, projectId, FloorNOName, FloorsName, search)
                    this.setState({
                      compTypePicker: value,
                      compTypeId: compTypeId,
                      compTypeName: value
                    })
                  } else {
                    this.setState({
                      compTypePicker: value,
                      compTypeId: '',
                      compTypeName: ''
                    })
                    this.SearchPostTaskList("", projectId, FloorNOName, FloorsName, search)
                  }

                }}
              >
                <Picker.Item label={'请选择构件类型'} value={'请选择'} />
                {searchList.compTypeNameList.map((item) => {
                  return (
                    <Picker.Item label={item} value={item} />
                  )
                })}
              </Picker>
            </View>
          </View>
          {/* 项目 */}
          <View style={styles.sideMenu_Bottom}>
            <View style={styles.sideMenu_Top} >
              <Text style={styles.sideMenu_Title}>{"项目筛选"}</Text>
            </View>
            <View style={styles.sideMenu_Content}>
              <Picker
                mode='dropdown'
                selectedValue={projectPicker}
                style={styles.sideMenu_Picker}
                onValueChange={(value, position) => {
                  let projectId = ""
                  if (value != "请选择") {
                    projectId = searchList.projectIdList[position - 1] || ''
                    this.SearchPostTaskList(compTypeName, projectId, '', '', search)
                    this.requestDataFloorNO(projectId)
                    searchList.prjFNNameList = []
                    {
                      searchList.floorNoNameList.map((item) => {
                        let itemarr = item.split("+")
                        let prjId = itemarr[0]
                        let name = itemarr[1]
                        if (prjId == projectId) {
                          if (searchList.prjFNNameList.indexOf(name) == -1) {
                            searchList.prjFNNameList.push(name)
                          }
                        }
                      })
                    }
                    this.setState({
                      projectPicker: value,
                      projectName: value,
                      projectId: projectId,
                      FloorNOId: '',
                      FloorNOName: '',
                      FloorNOPicker: '请选择',
                      FloorsId: '',
                      FloorsName: '',
                      FloorsPicker: '请选择'
                    })
                  } else {
                    this.setState({
                      projectPicker: value,
                      projectName: '',
                      projectId: '',
                      FloorNOId: '',
                      FloorNOName: '',
                      FloorNOPicker: '请选择',
                      FloorsId: '',
                      FloorsName: '',
                      FloorsPicker: '请选择'
                    })
                    this.SearchPostTaskList(compTypeName, "", "", "", search)
                  }
                }}
              >
                <Picker.Item label={'请选择项目'} value={'请选择'} />
                {searchList.projectNameList.map((item) => {
                  return (
                    <Picker.Item label={item} value={item} />
                  )
                })}
              </Picker>
            </View>
          </View>
          {/* 楼号 */}
          <View style={styles.sideMenu_Bottom}>
            <View style={styles.sideMenu_Top} >
              <Text style={styles.sideMenu_Title}>{"楼号筛选"}</Text>
            </View>
            <View style={styles.sideMenu_Content}>
              <Picker
                mode='dropdown'
                selectedValue={FloorNOPicker}
                style={styles.sideMenu_Picker}
                onValueChange={(value) => {
                  let name = value
                  if (value != '请选择') {
                    this.setState({
                      FloorNOPicker: name,
                      FloorNOName: name
                    })
                    searchList.FNFNameList = []
                    searchList.floorNameList.map((item) => {
                      let itemarr = item.split("+")
                      let prjId = itemarr[0]
                      let fnnName = itemarr[1]
                      let name = itemarr[2]
                      if (prjId == projectId) {
                        if (fnnName == value) {
                          if (searchList.FNFNameList.indexOf(name) == -1) {
                            searchList.FNFNameList.push(name)
                          }
                        }
                      }
                    })
                    this.SearchPostTaskList(compTypeName, projectId, value, "", search)
                  } else {
                    this.setState({
                      FloorNOPicker: value,
                      FloorNOId: '',
                      FloorNOName: ''
                    })
                    this.SearchPostTaskList(compTypeName, projectId, "", "", search)
                  }
                }}
              /* onValueChange={(value) => {
                try {
                  if (projectPicker == '请选择') {
                    this.toast.show('请先选择项目')
                    return
                  }
                  if (searchList.floorNoNameList.length == 0) {
                    this.toast.show('无楼号')
                    return
                  }
   
                  let floorNoId = ''
                  
                } catch (error) {
   
                }
   
   
              }} */
              >
                <Picker.Item label={'请选择楼号'} value={'请选择'} />
                {searchList.prjFNNameList.map((item) => {
                  return (
                    <Picker.Item label={item} value={item} />
                  )
                })}
              </Picker>
            </View>
          </View>
          {/* 楼层 */}
          <View style={styles.sideMenu_Bottom}>
            <View style={styles.sideMenu_Top} >
              <Text style={styles.sideMenu_Title}>{"楼层筛选"}</Text>
            </View>
            <View style={styles.sideMenu_Content}>
              <Picker
                mode='dialog'
                selectedValue={FloorsPicker}
                style={styles.sideMenu_Picker}
                onValueChange={(value) => {
                  if (FloorNOPicker == '请选择') {
                    this.toast.show('请先选择楼号')
                    return
                  }
                  if (searchList.floorNameList.length == 0) {
                    this.toast.show('无楼层')
                    return
                  }
                  let floorId = ''
                  if (value != '请选择') {
                    //floorId = resDataFloors[position - 1].rowguid
                    this.SearchPostTaskList(compTypeName, projectId, FloorNOName, value, search)
                    this.setState({
                      FloorsPicker: value,
                      FloorsId: floorId,
                      FloorsName: value
                    })
                  } else {
                    this.setState({
                      FloorsId: '',
                      FloorsName: ''
                    })
                    this.SearchPostTaskList(compTypeName, projectId, FloorNOName, "", search)

                  }
                }}
              >
                <Picker.Item label={'请选择楼层'} value={'请选择'} />
                {searchList.FNFNameList.map((item) => {
                  return (
                    <Picker.Item label={item} value={item} />
                  )
                })}
              </Picker>
            </View>
          </View>
        </View>
    }
    //已生产构件明细
    else if (sonUrl.indexOf("PostProduceComponentStatePage") != -1) {
      menu =
        <View style={styles.sideMenu_Main} >
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          <View style={{ flex: 6 }}>
            {/* 构件类型 */}
            <View style={styles.sideMenu_Bottom}>
              <TouchableHighlight
                underlayColor={"lightgray"}
                onPress={() => {
                  sideType = 'compType'
                  console.log("🚀 ~ file: ChildOneFlat.js ~ line 2899 ~ ChildOne ~ render ~ searchList.projectNameList", searchList.projectNameList)
                  this.setState({ sideType: 'compType' })
                  this.isBottomVisible(searchList.compTypeNameList)
                }}
                onLongPress={() => {
                  Alert.alert("提示", "确定需要重置筛选条件吗？", [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '确定',
                    onPress: () => {
                      this.setState({
                        compTypeName: '',
                        compTypePicker: '',
                      })
                    }
                  }])
                }}>
                <View style={styles.sideMenu_Top1} >
                  <View style={styles.sideMenu_TitleView}>
                    <Text style={styles.sideMenu_Title}>{"构件类型筛选"}</Text>
                  </View>
                  <View style={styles.sideMenu_DownView}>
                    <View style={styles.sideMenu_DownView_View}>
                      {
                        !this.state.compTypeName ?
                          <Text style={styles.sideMenu_DownViewTitle}>{"展开"}</Text>
                          :
                          <Text numberOfLines={1} style={[styles.sideMenu_DownViewTitle, { color: '#419fff' }]}>{this.state.compTypeName}</Text>
                      }
                      <Icon name='down' color='gray' iconStyle={styles.sideMenu_DownViewIcon} type='antdesign' ></Icon>

                    </View>
                  </View>
                </View>
              </TouchableHighlight>

            </View>
            {/* 设计型号 */}
            <View style={styles.sideMenu_Bottom}>
              <View style={styles.sideMenu_Top} >
                <Text style={styles.sideMenu_Title}>{"设计型号筛选"}</Text>
              </View>
              <View style={styles.sideMenu_Content}>
                <SearchBar
                  platform='android'
                  placeholder={'请输入设计型号'}
                  placeholderTextColor='#666'

                  containerStyle={{ backgroundColor: 'transparent' }}
                  inputContainerStyle={{ backgroundColor: 'transparent', height: 25 }}
                  inputStyle={{ color: '#666', fontSize: 16 }}
                  searchIcon={designSearchIcon()}
                  //showLoading={pageLoading}
                  clearIcon={designClearIcon()}
                  showCancel={false}
                  cancelButtonProps={{ buttonStyle: { display: 'none' } }}
                  disabled={pageLoading}
                  //cancelButtonTitle=''
                  //cancelButtonProps={{ buttonTextStyle: { color: "#666", fontSize: RFT * 3.8 } }}
                  value={designTypeName}
                  onChangeText={(text) => {
                    this.setState({ designTypeName: text })
                  }}
                  onClear={() => {
                    this.setState({ designTypeName: '' })
                  }}
                //onChangeText={this.TextChangeDesignType}
                //input={(input) => { this.setState({ designTypeName: input }) }}
                />
              </View>
            </View>
            {/* 构件状态 */}
            <View style={styles.sideMenu_Bottom}>
              <TouchableHighlight
                underlayColor={"lightgray"}
                onPress={() => {
                  sideType = 'bigState'
                  console.log("🚀 ~ file: ChildOneFlat.js ~ line 2899 ~ ChildOne ~ render ~ searchList.projectNameList", searchList.projectNameList)
                  this.setState({ sideType: 'bigState' })
                  this.isBottomVisible(searchList.bigStateList)
                }}
                onLongPress={() => {
                  Alert.alert("提示", "确定需要重置筛选条件吗？", [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '确定',
                    onPress: () => {
                      this.setState({
                        bigStateId: '',
                        bigStateName: '',
                        bigStatePicker: '',
                      })
                    }
                  }])
                }}>
                <View style={styles.sideMenu_Top1} >
                  <View style={styles.sideMenu_TitleView}>
                    <Text style={styles.sideMenu_Title}>{"构件状态筛选"}</Text>
                  </View>
                  <View style={styles.sideMenu_DownView}>
                    <View style={styles.sideMenu_DownView_View}>
                      {
                        !this.state.bigStateName ?
                          <Text style={styles.sideMenu_DownViewTitle}>{"展开"}</Text>
                          :
                          <Text numberOfLines={1} style={[styles.sideMenu_DownViewTitle, { color: '#419fff' }]}>{this.state.bigStateName}</Text>
                      }
                      <Icon name='down' color='gray' iconStyle={styles.sideMenu_DownViewIcon} type='antdesign' ></Icon>

                    </View>
                  </View>
                </View>
              </TouchableHighlight>

            </View>
            {/* 库房 */}
            <View style={styles.sideMenu_Bottom}>
              <TouchableHighlight
                underlayColor={"lightgray"}
                onPress={() => {
                  sideType = 'compRoom'
                  console.log("🚀 ~ file: ChildOneFlat.js ~ line 2899 ~ ChildOne ~ render ~ searchList.projectNameList", searchList.projectNameList)
                  this.setState({ sideType: 'compRoom' })
                  this.isBottomVisible(searchList.compRoomNameList)
                }}
                onLongPress={() => {
                  Alert.alert("提示", "确定需要重置筛选条件吗？", [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '确定',
                    onPress: () => {
                      this.setState({
                        compRoomName: '',
                        compRoomPicker: '',
                      })
                    }
                  }])
                }}>
                <View style={styles.sideMenu_Top1} >
                  <View style={styles.sideMenu_TitleView}>
                    <Text style={styles.sideMenu_Title}>{"库房筛选"}</Text>
                  </View>
                  <View style={styles.sideMenu_DownView}>
                    <View style={styles.sideMenu_DownView_View}>
                      {
                        !this.state.compRoomName ?
                          <Text style={styles.sideMenu_DownViewTitle}>{"展开"}</Text>
                          :
                          <Text numberOfLines={1} style={[styles.sideMenu_DownViewTitle, { color: '#419fff' }]}>{this.state.compRoomName}</Text>
                      }
                      <Icon name='down' color='gray' iconStyle={styles.sideMenu_DownViewIcon} type='antdesign' ></Icon>

                    </View>
                  </View>
                </View>
              </TouchableHighlight>

            </View>
            {/* 库位 */}
            <View style={styles.sideMenu_Bottom}>
              <TouchableHighlight
                underlayColor={"lightgray"}
                onPress={() => {
                  sideType = 'compLib'
                  console.log("🚀 ~ file: ChildOneFlat.js ~ line 2899 ~ ChildOne ~ render ~ searchList.projectNameList", searchList.projectNameList)
                  this.setState({ sideType: 'compLib' })

                  if (this.state.compRoomName == '') {
                    this.toast.show("请先选择库房")
                    return
                  }

                  if (searchList.roomLibNameList.length == 0) {
                    this.toast.show("没有查询到库房")
                    return
                  }

                  this.isBottomVisible(searchList.roomLibNameList)

                }}
                onLongPress={() => {
                  Alert.alert("提示", "确定需要重置筛选条件吗？", [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '确定',
                    onPress: () => {
                      this.setState({
                        compLibName: '',
                        compLibPicker: '',
                      })
                    }
                  }])
                }}>
                <View style={styles.sideMenu_Top1} >
                  <View style={styles.sideMenu_TitleView}>
                    <Text style={styles.sideMenu_Title}>{"库位筛选"}</Text>
                  </View>
                  <View style={styles.sideMenu_DownView}>
                    <View style={styles.sideMenu_DownView_View}>
                      {
                        !this.state.compLibName ?
                          <Text style={styles.sideMenu_DownViewTitle}>{"展开"}</Text>
                          :
                          <Text numberOfLines={1} style={[styles.sideMenu_DownViewTitle, { color: '#419fff' }]}>{this.state.compLibName}</Text>
                      }
                      <Icon name='down' color='gray' iconStyle={styles.sideMenu_DownViewIcon} type='antdesign' ></Icon>

                    </View>
                  </View>
                </View>
              </TouchableHighlight>

            </View>
            {/* 楼号 */}
            <View style={styles.sideMenu_Bottom}>
              <TouchableHighlight
                underlayColor={"lightgray"}
                onPress={() => {
                  sideType = 'floorNo'
                  console.log("🚀 ~ file: ChildOneFlat.js ~ line 2899 ~ ChildOne ~ render ~ searchList.projectNameList", searchList.projectNameList)
                  this.setState({ sideType: 'floorNo' })
                  this.isBottomVisible(searchList.prjFNNameList)
                }}
                onLongPress={() => {
                  Alert.alert("提示", "确定需要重置筛选条件吗？", [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '确定',
                    onPress: () => {
                      this.setState({
                        FloorNOName: '',
                        FloorNOPicker: '',
                      })
                    }
                  }])
                }}>
                <View style={styles.sideMenu_Top1} >
                  <View style={styles.sideMenu_TitleView}>
                    <Text style={styles.sideMenu_Title}>{"楼号筛选"}</Text>
                  </View>
                  <View style={styles.sideMenu_DownView}>
                    <View style={styles.sideMenu_DownView_View}>
                      {
                        !this.state.FloorNOName ?
                          <Text style={styles.sideMenu_DownViewTitle}>{"展开"}</Text>
                          :
                          <Text numberOfLines={1} style={[styles.sideMenu_DownViewTitle, { color: '#419fff' }]}>{this.state.FloorNOName}</Text>
                      }
                      <Icon name='down' color='gray' iconStyle={styles.sideMenu_DownViewIcon} type='antdesign' ></Icon>

                    </View>
                  </View>
                </View>
              </TouchableHighlight>
            </View>
            {/* 楼层 */}
            <View style={styles.sideMenu_Bottom}>
              <TouchableHighlight
                underlayColor={"lightgray"}
                onPress={() => {
                  sideType = 'floor'
                  console.log("🚀 ~ file: ChildOneFlat.js ~ line 2899 ~ ChildOne ~ render ~ searchList.projectNameList", searchList.projectNameList)
                  this.setState({ sideType: 'floor' })

                  if (this.state.FloorNOName == '') {
                    this.toast.show("请先选择楼号")
                    return
                  }

                  if (searchList.FNFNameList.length == 0) {
                    this.toast.show("没有查询到楼层")
                    return
                  }
                  this.isBottomVisible(searchList.FNFNameList)
                }}
                onLongPress={() => {
                  Alert.alert("提示", "确定需要重置筛选条件吗？", [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '确定',
                    onPress: () => {
                      this.setState({
                        FloorsName: '',
                        FloorsPicker: '',
                      })
                    }
                  }])
                }}>
                <View style={styles.sideMenu_Top1} >
                  <View style={styles.sideMenu_TitleView}>
                    <Text style={styles.sideMenu_Title}>{"楼层筛选"}</Text>
                  </View>
                  <View style={styles.sideMenu_DownView}>
                    <View style={styles.sideMenu_DownView_View}>
                      {
                        !this.state.FloorsName ?
                          <Text style={styles.sideMenu_DownViewTitle}>{"展开"}</Text>
                          :
                          <Text numberOfLines={1} style={[styles.sideMenu_DownViewTitle, { color: '#419fff' }]}>{this.state.FloorsName}</Text>
                      }
                      <Icon name='down' color='gray' iconStyle={styles.sideMenu_DownViewIcon} type='antdesign' ></Icon>

                    </View>
                  </View>
                </View>
              </TouchableHighlight>

            </View>
          </View>

          {/* 按钮组 */}
          <View style={[styles.sideMenu_Bottom, { flexDirection: 'row', justifyContent: 'center', marginTop: 20 }]}>
            <Button
              type='outline'
              title='重置'
              containerStyle={{ flex: 1, borderRadius: 0 }}
              buttonStyle={{ borderRadius: 0, borderColor: '#419fff' }}
              onPress={() => {
                Alert.alert("提示", "确定需要重置筛选条件吗？", [{
                  text: '取消',
                  style: 'cancel'
                }, {
                  text: '确定',
                  onPress: () => {
                    this.setState({
                      compTypeName: '',
                      compTypePicker: '',
                      designTypeName: '',
                      designTypePicker: '',
                      bigStateName: '',
                      bigStatePicker: '',
                      compRoomName: '',
                      compRoomPicker: '',
                      compLibName: '',
                      compLibPicker: '',
                      FloorNOName: '',
                      FloorNOPicker: '',
                      FloorsName: '',
                      FloorsPicker: '',
                      page: 1,
                      resData: []
                    })
                    pageNo = 1
                    data.warehouRomName = ""
                    data.warehouLibName = ""
                    data.bigState = ""
                    data.designType = ""
                    data.compTypeName = ""
                    data.floorNoName = ""
                    data.floorName = ""
                    data.pageIndex = 1
                    console.log("🚀 ~ file: ChildOneFlat.js ~ line 2230 ~ ChildOne ~ data", data)
                    this.requestData1()
                  }
                }])
              }}
            />
            <Button
              //type='outline'
              title='搜索'
              containerStyle={{ flex: 1, borderRadius: 0 }}
              ViewComponent={LinearGradient}
              linearGradientProps={{ colors: ['#3FF0CC', 'rgba(65,159,255,0.80)'], start: { x: 1, y: 1 }, end: { x: 0, y: 1 } }}
              buttonStyle={{ borderRadius: 0 }}
              onPress={() => {
                this.setState({ resData: [], page: "1" })
                pageNo = 1
                data.pageIndex = 1
                data.warehouRomName = compRoomName
                data.warehouLibName = compLibName
                data.bigState = this.state.bigStateId
                data.designType = this.state.designTypeName
                data.compTypeName = this.state.compTypeName
                data.floorNoName = this.state.FloorNOName
                data.floorName = this.state.FloorsName
                this.setState({ isSideOpen: false })
                this.requestData1()
              }}
            />
          </View>

          {this.BottomList()}
        </View>
      menu1 = <ScrollView style={styles.sideMenu_Main} >
        <Toast ref={(ref) => { this.toast = ref; }} position="center" />
        {/* 构件类型 */}
        <View style={styles.sideMenu_Bottom}>
          <View style={styles.sideMenu_Top} >
            <Text style={styles.sideMenu_Title}>{"构件类型筛选"}</Text>
          </View>
          <View style={styles.sideMenu_Content}>
            <Picker
              mode='dropdown'
              selectedValue={compTypePicker}
              style={styles.sideMenu_Picker}
              onValueChange={(value) => {
                let compTypeId = ''
                if (value != '请选择') {
                  data.compTypeName = value
                  this.SearchPostProducedComponents(data)
                  this.setState({
                    compTypePicker: value,
                    compTypeId: compTypeId,
                    compTypeName: value
                  })
                } else {
                  this.setState({
                    compTypePicker: value,
                    compTypeId: '',
                    compTypeName: ''
                  })
                  data.compTypeName = ''
                  this.SearchPostProducedComponents(data)
                }

              }}
            >
              <Picker.Item label={'请选择构件类型'} value={'请选择'} />
              {searchList.compTypeNameList.map((item) => {
                return (
                  <Picker.Item label={item} value={item} />
                )
              })}
            </Picker>
          </View>
        </View>
        {/* 设计型号 */}
        <View style={styles.sideMenu_Bottom}>
          <View style={styles.sideMenu_Top} >
            <Text style={styles.sideMenu_Title}>{"设计型号筛选"}</Text>
          </View>
          <View style={styles.sideMenu_Content}>
            <SearchBar
              platform='android'
              placeholder={'请输入设计型号'}
              placeholderTextColor='#666'

              containerStyle={{ backgroundColor: 'transparent' }}
              inputContainerStyle={{ backgroundColor: 'transparent', height: 25 }}
              inputStyle={{ color: '#666', fontSize: RFT * 3.6 }}
              searchIcon={designSearchIcon()}
              //showLoading={pageLoading}
              clearIcon={designClearIcon()}
              showCancel={false}
              cancelButtonProps={{ buttonStyle: { display: 'none' } }}
              disabled={pageLoading}
              //cancelButtonTitle=''
              //cancelButtonProps={{ buttonTextStyle: { color: "#666", fontSize: RFT * 3.8 } }}
              value={designTypeName}
              onEndEditing={() => {
                data.designType = designTypeName
                this.SearchPostProducedComponents(data)
              }}
              onChangeText={(text) => {
                this.setState({ designTypeName: text })
              }}
              onClear={() => {
                this.setState({ designTypeName: '' })
                data.designType = ''
                this.SearchPostProducedComponents(data)
              }}
            //onChangeText={this.TextChangeDesignType}
            //input={(input) => { this.setState({ designTypeName: input }) }}
            />
          </View>
        </View>
        {/* 构件状态 */}
        <View style={styles.sideMenu_Bottom}>
          <View style={styles.sideMenu_Top} >
            <Text style={styles.sideMenu_Title}>{"构件状态筛选"}</Text>
          </View>
          <View style={styles.sideMenu_Content}>
            <Picker
              mode='dropdown'
              selectedValue={bigStatePicker}
              style={styles.sideMenu_Picker}
              enabled={!pageLoading}
              onValueChange={(value) => {
                let bigStateId = ''
                if (value != '请选择') {
                  data.bigState = value
                  this.SearchPostProducedComponents(data)

                  this.setState({
                    bigStatePicker: value,
                    bigStateName: value
                  })
                } else {
                  this.setState({
                    bigStatePicker: value,
                    bigStateId: "",
                    bigStateName: ""
                  })
                  data.bigState = ""
                  this.SearchPostProducedComponents(data)
                }

              }}
            >
              <Picker.Item label={'请选择构件状态'} value={'请选择'} />
              {searchList.bigStateList.map((item) => {
                let itemarr = item.split("+")
                return (
                  <Picker.Item label={itemarr[1]} value={itemarr[1]} />
                )
              })}
            </Picker>
          </View>
        </View>
        {/* 库房 */}
        <View style={styles.sideMenu_Bottom}>
          <View style={styles.sideMenu_Top} >
            <Text style={styles.sideMenu_Title}>{"库房筛选"}</Text>
          </View>
          <View style={styles.sideMenu_Content}>
            <Picker
              mode='dialog'
              selectedValue={compRoomPicker}
              style={{ flex: 1 }}
              onValueChange={(value, position) => {
                let bigStateId = ''
                if (value != '请选择') {
                  searchList.compLibNameList.map((item) => {
                    let itemarr = item.split("+")
                    let compRoom = itemarr[0]
                    let compLib = itemarr[1]
                    if (compRoom == value) {
                      if (searchList.roomLibNameList.indexOf(compLib) == -1) {
                        searchList.roomLibNameList.push(compLib)
                      }
                    }
                  })
                  data.warehouRomName = value
                  this.SearchPostProducedComponents(data)
                  //bigStateId = resDataProductLine[position - 1].rowguid || ''
                  this.setState({
                    compRoomPicker: value,
                    compRoomName: value
                  })
                } else {
                  this.setState({
                    compRoomPicker: value,
                    compRoomName: ""
                  })
                  data.warehouRomName = ""
                  this.SearchPostProducedComponents(data)
                }

              }}
            >
              <Picker.Item label={'请选择库房'} value={'请选择'} />
              {searchList.compRoomNameList.map((item) => {
                return (
                  <Picker.Item label={item} value={item} />
                )
              })}
            </Picker>
          </View>
        </View>
        {/* 库位 */}
        <View style={styles.sideMenu_Bottom}>
          <View style={styles.sideMenu_Top} >
            <Text style={styles.sideMenu_Title}>{"库位筛选"}</Text>
          </View>
          <View style={styles.sideMenu_Content}>
            <Picker
              mode='dialog'
              selectedValue={compLibPicker}
              style={{ flex: 1 }}
              onValueChange={(value, position) => {
                if (value != '请选择') {
                  data.warehouLibName = value
                  this.SearchPostProducedComponents(data)
                  this.setState({
                    compLibPicker: value,
                    compLibName: value
                  })
                } else {
                  this.setState({
                    compLibPicker: "",
                    compLibName: ""
                  })
                  data.warehouLibName = ""
                  this.SearchPostProducedComponents(data)
                }

              }}
            >
              <Picker.Item label={'请选择库位'} value={'请选择'} />
              {searchList.roomLibNameList.map((item) => {
                return (
                  <Picker.Item label={item} value={item} />
                )
              })}
            </Picker>
          </View>
        </View>
        {/* 楼号 */}
        <View style={styles.sideMenu_Bottom}>
          <View style={styles.sideMenu_Top} >
            <Text style={styles.sideMenu_Title}>{"楼号筛选"}</Text>
          </View>
          <View style={styles.sideMenu_Content}>
            <Picker
              mode='dropdown'
              selectedValue={FloorNOPicker}
              style={styles.sideMenu_Picker}
              enabled={!pageLoading}
              onValueChange={(value) => {
                let name = value
                if (value != '请选择') {
                  this.setState({
                    FloorNOPicker: name,
                    FloorNOName: name,
                    FloorsName: '',
                    FloorsPicker: '',
                  })
                  resDataFloorNO.map((floorNoItem) => {
                    if (floorNoItem.structurename == value) {
                      this.requestDataFloor(projectId, floorNoItem.rowguid)
                    }
                  })
                  data.floorNoName = value
                  this.SearchPostProducedComponents(data)

                } else {
                  this.setState({
                    FloorNOPicker: value,
                    FloorNOId: '',
                    FloorNOName: '',
                    FloorsName: '',
                    FloorsPicker: '',
                  })
                  data.floorNoName = ""
                  this.SearchPostProducedComponents(data)

                }
              }}
            >
              <Picker.Item label={'请选择楼号'} value={'请选择'} />
              {searchList.prjFNNameList.map((item) => {
                return (
                  <Picker.Item label={item} value={item} />
                )
              })}
            </Picker>
          </View>
        </View>
        {/* 楼层 */}
        <View style={styles.sideMenu_Bottom}>
          <View style={styles.sideMenu_Top} >
            <Text style={styles.sideMenu_Title}>{"楼层筛选"}</Text>
          </View>
          <View style={styles.sideMenu_Content}>
            <Picker
              mode='dialog'
              selectedValue={FloorsPicker}
              style={styles.sideMenu_Picker}
              enabled={!pageLoading}
              onValueChange={(value) => {
                if (FloorNOPicker == '请选择') {
                  this.toast.show('请先选择楼号')
                  return
                }
                if (searchList.floorNameList.length == 0) {
                  this.toast.show('无楼层')
                  return
                }
                let floorId = ''
                if (value != '请选择') {
                  data.floorName = value
                  this.SearchPostProducedComponents(data)
                  this.setState({
                    FloorsPicker: value,
                    FloorsId: floorId,
                    FloorsName: value
                  })
                } else {
                  this.setState({
                    FloorsId: '',
                    FloorsName: ''
                  })
                  data.floorName = ""
                  this.SearchPostProducedComponents(data)

                }
              }}
            >
              <Picker.Item label={'请选择楼层'} value={'请选择'} />
              {searchList.FNFNameList.map((item) => {
                return (
                  <Picker.Item label={item} value={item} />
                )
              })}
            </Picker>
          </View>
        </View>
      </ScrollView>
    }
    //日生产统计
    else if (sonUrl.indexOf("PostProduceDailyRecordPage") != -1) {
      menu =
        <View style={styles.sideMenu_Main} >
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          <View style={{ flex: 6 }}>
            {/* 构件类型 */}
            <View style={styles.sideMenu_Bottom}>
              <TouchableHighlight
                underlayColor={"lightgray"}
                onPress={() => {
                  sideType = 'compType'
                  console.log("🚀 ~ file: ChildOneFlat.js ~ line 2899 ~ ChildOne ~ render ~ searchList.projectNameList", searchList.projectNameList)
                  this.setState({ sideType: 'compType' })
                  this.isBottomVisible(searchList.compTypeNameList)
                }}
                onLongPress={() => {
                  Alert.alert("提示", "确定需要重置筛选条件吗？", [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '确定',
                    onPress: () => {
                      this.setState({
                        compTypeName: '',
                        compTypePicker: '',
                      })
                    }
                  }])
                }}>
                <View style={styles.sideMenu_Top1} >
                  <View style={styles.sideMenu_TitleView}>
                    <Text style={styles.sideMenu_Title}>{"构件类型筛选"}</Text>
                  </View>
                  <View style={styles.sideMenu_DownView}>
                    <View style={styles.sideMenu_DownView_View}>
                      {
                        !this.state.compTypeName ?
                          <Text style={styles.sideMenu_DownViewTitle}>{"展开"}</Text>
                          :
                          <Text numberOfLines={1} style={[styles.sideMenu_DownViewTitle, { color: '#419fff' }]}>{this.state.compTypeName}</Text>
                      }
                      <Icon name='down' color='gray' iconStyle={styles.sideMenu_DownViewIcon} type='antdesign' ></Icon>

                    </View>
                  </View>
                </View>
              </TouchableHighlight>

            </View>
            {/* 楼号 */}
            <View style={styles.sideMenu_Bottom}>
              <TouchableHighlight
                underlayColor={"lightgray"}
                onPress={() => {
                  sideType = 'floorNo'
                  console.log("🚀 ~ file: ChildOneFlat.js ~ line 2899 ~ ChildOne ~ render ~ searchList.prjFNNameList", searchList.prjFNNameList)
                  this.setState({ sideType: 'floorNo' })
                  this.isBottomVisible(searchList.prjFNNameList)
                }}
                onLongPress={() => {
                  Alert.alert("提示", "确定需要重置筛选条件吗？", [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '确定',
                    onPress: () => {
                      this.setState({
                        FloorNOName: '',
                        FloorNOPicker: '',
                      })
                    }
                  }])
                }}>
                <View style={styles.sideMenu_Top1} >
                  <View style={styles.sideMenu_TitleView}>
                    <Text style={styles.sideMenu_Title}>{"楼号筛选"}</Text>
                  </View>
                  <View style={styles.sideMenu_DownView}>
                    <View style={styles.sideMenu_DownView_View}>
                      {
                        !this.state.FloorNOName ?
                          <Text style={styles.sideMenu_DownViewTitle}>{"展开"}</Text>
                          :
                          <Text numberOfLines={1} style={[styles.sideMenu_DownViewTitle, { color: '#419fff' }]}>{this.state.FloorNOName}</Text>
                      }
                      <Icon name='down' color='gray' iconStyle={styles.sideMenu_DownViewIcon} type='antdesign' ></Icon>

                    </View>
                  </View>
                </View>
              </TouchableHighlight>
            </View>
            {/* 楼层 */}
            <View style={styles.sideMenu_Bottom}>
              <TouchableHighlight
                underlayColor={"lightgray"}
                onPress={() => {
                  sideType = 'floor'
                  console.log("🚀 ~ file: ChildOneFlat.js ~ line 2899 ~ ChildOne ~ render ~ searchList.projectNameList", searchList.projectNameList)
                  this.setState({ sideType: 'floor' })

                  if (this.state.FloorNOName == '') {
                    this.toast.show("请先选择楼号")
                    return
                  }

                  if (searchList.FNFNameList.length == 0) {
                    this.toast.show("没有查询到楼层")
                    return
                  }
                  this.isBottomVisible(searchList.FNFNameList)
                }}
                onLongPress={() => {
                  Alert.alert("提示", "确定需要重置筛选条件吗？", [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '确定',
                    onPress: () => {
                      this.setState({
                        FloorsName: '',
                        FloorsPicker: '',
                      })
                    }
                  }])
                }}>
                <View style={styles.sideMenu_Top1} >
                  <View style={styles.sideMenu_TitleView}>
                    <Text style={styles.sideMenu_Title}>{"楼层筛选"}</Text>
                  </View>
                  <View style={styles.sideMenu_DownView}>
                    <View style={styles.sideMenu_DownView_View}>
                      {
                        !this.state.FloorsName ?
                          <Text style={styles.sideMenu_DownViewTitle}>{"展开"}</Text>
                          :
                          <Text numberOfLines={1} style={[styles.sideMenu_DownViewTitle, { color: '#419fff' }]}>{this.state.FloorsName}</Text>
                      }
                      <Icon name='down' color='gray' iconStyle={styles.sideMenu_DownViewIcon} type='antdesign' ></Icon>

                    </View>
                  </View>
                </View>
              </TouchableHighlight>

            </View>
          </View>

          {/* 按钮组 */}
          <View style={[styles.sideMenu_Bottom, { flexDirection: 'row', justifyContent: 'center', marginTop: 20 }]}>
            <Button
              type='outline'
              title='重置'
              containerStyle={{ flex: 1, borderRadius: 0 }}
              buttonStyle={{ borderRadius: 0, borderColor: '#419fff' }}
              onPress={() => {
                Alert.alert("提示", "确定需要重置筛选条件吗？", [{
                  text: '取消',
                  style: 'cancel'
                }, {
                  text: '确定',
                  onPress: () => {
                    this.setState({
                      compTypeName: '',
                      compTypePicker: '',
                      FloorNOName: '',
                      FloorNOPicker: '',
                      FloorsName: '',
                      FloorsPicker: '',
                      resData: [],
                      page: "1",
                    })
                    data.pageIndex = 1
                    data.compTypeName = ""
                    data.floorNoName = ""
                    data.floorName = ""
                    console.log("🚀 ~ file: ChildOneFlat.js ~ line 2230 ~ ChildOne ~ data", data)
                    this.requestData1()
                  }
                }])
              }}
            />
            <Button
              //type='outline'
              title='搜索'
              containerStyle={{ flex: 1, borderRadius: 0 }}
              ViewComponent={LinearGradient}
              linearGradientProps={{ colors: ['#3FF0CC', 'rgba(65,159,255,0.80)'], start: { x: 1, y: 1 }, end: { x: 0, y: 1 } }}
              buttonStyle={{ borderRadius: 0 }}
              onPress={() => {
                this.setState({ resData: [], page: "1" })
                pageNo = 1
                data.pageIndex = 1
                data.compTypeName = this.state.compTypeName
                data.floorNoName = this.state.FloorNOName
                data.floorName = this.state.FloorsName
                this.setState({ isSideOpen: false })
                console.log("🚀 ~ file: ChildOneFlat.js ~ line 3510 ~ ChildOne ~ data", data)
                this.requestData1()
              }}
            />
          </View>

          {this.BottomList()}
        </View>
      menu1 =
        <View style={styles.sideMenu_Main} >
          {/* 构件类型 */}
          <View style={styles.sideMenu_Bottom}>
            <View style={styles.sideMenu_Top} >
              <Text style={styles.sideMenu_Title}>{"构件类型筛选"}</Text>
            </View>
            <View style={styles.sideMenu_Content}>
              <Picker
                mode='dropdown'
                selectedValue={compTypePicker}
                style={styles.sideMenu_Picker}
                enabled={!pageLoading}
                onValueChange={(value) => {
                  let compTypeId = ''
                  if (value != '请选择') {
                    this.SearchPostGetProduceDailyRecord(value, FloorNOName, FloorsName, search)
                    this.setState({
                      compTypePicker: value,
                      compTypeId: compTypeId,
                      compTypeName: value
                    })
                  } else {
                    this.setState({
                      compTypePicker: value,
                      compTypeId: '',
                      compTypeName: ''
                    })
                    this.SearchPostGetProduceDailyRecord("", FloorNOName, FloorsName, search)
                  }

                }}
              >
                <Picker.Item label={'请选择构件类型'} value={'请选择'} />
                {searchList.compTypeNameList.map((item) => {
                  return (
                    <Picker.Item label={item} value={item} />
                  )
                })}
              </Picker>
            </View>
          </View>
          {/* 楼号 */}
          <View style={styles.sideMenu_Bottom}>
            <View style={styles.sideMenu_Top} >
              <Text style={styles.sideMenu_Title}>{"楼号筛选"}</Text>
            </View>
            <View style={styles.sideMenu_Content}>
              <Picker
                mode='dropdown'
                selectedValue={FloorNOPicker}
                style={styles.sideMenu_Picker}
                onValueChange={(value) => {
                  let name = value
                  if (value != '请选择') {
                    this.setState({
                      FloorNOPicker: name,
                      FloorNOName: name
                    })
                    searchList.FNFNameList = []
                    searchList.floorNameList.map((item) => {
                      let itemarr = item.split("+")
                      let prjId = itemarr[0]
                      let fnnName = itemarr[1]
                      let name = itemarr[2]
                      if (prjId == projectId) {
                        if (fnnName == value) {
                          if (searchList.FNFNameList.indexOf(name) == -1) {
                            searchList.FNFNameList.push(name)
                          }
                        }
                      }
                    })
                    this.SearchPostGetProduceDailyRecord(compTypeName, FloorNOName, FloorsName, search)
                  } else {
                    this.setState({
                      FloorNOPicker: value,
                      FloorNOId: '',
                      FloorNOName: ''
                    })
                    this.SearchPostGetProduceDailyRecord(compTypeName, "", FloorsName, search)
                  }
                }}

              >
                <Picker.Item label={'请选择楼号'} value={'请选择'} />
                {searchList.prjFNNameList.map((item) => {
                  return (
                    <Picker.Item label={item} value={item} />
                  )
                })}
              </Picker>
            </View>
          </View>
          {/* 楼层 */}
          <View style={styles.sideMenu_Bottom}>
            <View style={styles.sideMenu_Top} >
              <Text style={styles.sideMenu_Title}>{"楼层筛选"}</Text>
            </View>
            <View style={styles.sideMenu_Content}>
              <Picker
                mode='dialog'
                selectedValue={FloorsPicker}
                style={styles.sideMenu_Picker}
                onValueChange={(value) => {
                  if (FloorNOPicker == '请选择') {
                    this.toast.show('请先选择楼号')
                    return
                  }
                  if (searchList.floorNameList.length == 0) {
                    this.toast.show('无楼层')
                    return
                  }
                  let floorId = ''
                  if (value != '请选择') {
                    //floorId = resDataFloors[position - 1].rowguid
                    this.SearchPostGetProduceDailyRecord(compTypeName, FloorNOName, value, search)
                    this.setState({
                      FloorsPicker: value,
                      FloorsId: floorId,
                      FloorsName: value
                    })
                  } else {
                    this.setState({
                      FloorsId: '',
                      FloorsName: ''
                    })
                    this.SearchPostGetProduceDailyRecord(compTypeName, FloorNOName, "", search)

                  }
                }}
              >
                <Picker.Item label={'请选择楼层'} value={'请选择'} />
                {searchList.FNFNameList.map((item) => {
                  return (
                    <Picker.Item label={item} value={item} />
                  )
                })}
              </Picker>
            </View>
          </View>
        </View>
    }
    //成品检查
    else if (sonUrl.indexOf("PostQualityFinProductsPage") != -1) {
      menu =
        <View style={styles.sideMenu_Main} >
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          <View style={{ flex: 6 }}>
            {/* 是否合格 */}
            <View style={styles.sideMenu_Bottom}>
              <TouchableHighlight
                underlayColor={"lightgray"}
                onPress={() => {
                  sideType = 'insresults'
                  console.log("🚀 ~ file: ChildOneFlat.js ~ line 2899 ~ ChildOne ~ render ~ searchList.projectNameList", searchList.projectNameList)
                  this.setState({ sideType: 'insresults' })
                  this.isBottomVisible(resDataInsresults)
                }}
                onLongPress={() => {
                  Alert.alert("提示", "确定需要重置筛选条件吗？", [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '确定',
                    onPress: () => {
                      this.setState({
                        InsresultsName: '',
                        InsresultsPicker: '',
                      })
                    }
                  }])
                }}>
                <View style={styles.sideMenu_Top1} >
                  <View style={styles.sideMenu_TitleView}>
                    <Text style={styles.sideMenu_Title}>{"是否合格"}</Text>
                  </View>
                  <View style={styles.sideMenu_DownView}>
                    <View style={styles.sideMenu_DownView_View}>
                      {
                        !this.state.InsresultsName ?
                          <Text style={styles.sideMenu_DownViewTitle}>{"展开"}</Text>
                          :
                          <Text numberOfLines={1} style={[styles.sideMenu_DownViewTitle, { color: '#419fff' }]}>{this.state.InsresultsName}</Text>
                      }
                      <Icon name='down' color='gray' iconStyle={styles.sideMenu_DownViewIcon} type='antdesign' ></Icon>

                    </View>
                  </View>
                </View>
              </TouchableHighlight>

            </View>
            {/* 构件类型 */}
            <View style={styles.sideMenu_Bottom}>
              <TouchableHighlight
                underlayColor={"lightgray"}
                onPress={() => {
                  sideType = 'compType'
                  console.log("🚀 ~ file: ChildOneFlat.js ~ line 2899 ~ ChildOne ~ render ~ searchList.projectNameList", searchList.projectNameList)
                  this.setState({ sideType: 'compType' })
                  this.isBottomVisible(searchList.compTypeNameList)
                }}
                onLongPress={() => {
                  Alert.alert("提示", "确定需要重置筛选条件吗？", [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '确定',
                    onPress: () => {
                      this.setState({
                        compTypeName: '',
                        compTypePicker: '',
                      })
                    }
                  }])
                }}>
                <View style={styles.sideMenu_Top1} >
                  <View style={styles.sideMenu_TitleView}>
                    <Text style={styles.sideMenu_Title}>{"构件类型筛选"}</Text>
                  </View>
                  <View style={styles.sideMenu_DownView}>
                    <View style={styles.sideMenu_DownView_View}>
                      {
                        !this.state.compTypeName ?
                          <Text style={styles.sideMenu_DownViewTitle}>{"展开"}</Text>
                          :
                          <Text numberOfLines={1} style={[styles.sideMenu_DownViewTitle, { color: '#419fff' }]}>{this.state.compTypeName}</Text>
                      }
                      <Icon name='down' color='gray' iconStyle={styles.sideMenu_DownViewIcon} type='antdesign' ></Icon>

                    </View>
                  </View>
                </View>
              </TouchableHighlight>

            </View>
            {/* 设计型号 */}
            <View style={styles.sideMenu_Bottom}>
              <View style={styles.sideMenu_Top} >
                <Text style={styles.sideMenu_Title}>{"设计型号筛选"}</Text>
              </View>
              <View style={styles.sideMenu_Content}>
                <SearchBar
                  platform='android'
                  placeholder={'请输入设计型号'}
                  placeholderTextColor='#666'

                  containerStyle={{ backgroundColor: 'transparent' }}
                  inputContainerStyle={{ backgroundColor: 'transparent', height: 25 }}
                  inputStyle={{ color: '#666', fontSize: 16 }}
                  searchIcon={designSearchIcon()}
                  //showLoading={pageLoading}
                  clearIcon={designClearIcon()}
                  showCancel={false}
                  cancelButtonProps={{ buttonStyle: { display: 'none' } }}
                  disabled={pageLoading}
                  //cancelButtonTitle=''
                  //cancelButtonProps={{ buttonTextStyle: { color: "#666", fontSize: RFT * 3.8 } }}
                  value={designTypeName}
                  onChangeText={(text) => {
                    this.setState({ designTypeName: text })
                  }}
                  onClear={() => {
                    this.setState({ designTypeName: '' })
                  }}
                //onChangeText={this.TextChangeDesignType}
                //input={(input) => { this.setState({ designTypeName: input }) }}
                />
              </View>
            </View>
            {/* 楼号 */}
            <View style={styles.sideMenu_Bottom}>
              <TouchableHighlight
                underlayColor={"lightgray"}
                onPress={() => {
                  sideType = 'floorNo'
                  console.log("🚀 ~ file: ChildOneFlat.js ~ line 2899 ~ ChildOne ~ render ~ searchList.projectNameList", searchList.projectNameList)
                  this.setState({ sideType: 'floorNo' })
                  this.isBottomVisible(searchList.prjFNNameList)
                }}
                onLongPress={() => {
                  Alert.alert("提示", "确定需要重置筛选条件吗？", [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '确定',
                    onPress: () => {
                      this.setState({
                        FloorNOName: '',
                        FloorNOPicker: '',
                      })
                    }
                  }])
                }}>
                <View style={styles.sideMenu_Top1} >
                  <View style={styles.sideMenu_TitleView}>
                    <Text style={styles.sideMenu_Title}>{"楼号筛选"}</Text>
                  </View>
                  <View style={styles.sideMenu_DownView}>
                    <View style={styles.sideMenu_DownView_View}>
                      {
                        !this.state.FloorNOName ?
                          <Text style={styles.sideMenu_DownViewTitle}>{"展开"}</Text>
                          :
                          <Text numberOfLines={1} style={[styles.sideMenu_DownViewTitle, { color: '#419fff' }]}>{this.state.FloorNOName}</Text>
                      }
                      <Icon name='down' color='gray' iconStyle={styles.sideMenu_DownViewIcon} type='antdesign' ></Icon>

                    </View>
                  </View>
                </View>
              </TouchableHighlight>
            </View>
            {/* 楼层 */}
            <View style={styles.sideMenu_Bottom}>
              <TouchableHighlight
                underlayColor={"lightgray"}
                onPress={() => {
                  sideType = 'floor'
                  console.log("🚀 ~ file: ChildOneFlat.js ~ line 2899 ~ ChildOne ~ render ~ searchList.projectNameList", searchList.projectNameList)
                  this.setState({ sideType: 'floor' })

                  if (this.state.FloorNOName == '') {
                    this.toast.show("请先选择楼号")
                    return
                  }

                  if (searchList.FNFNameList.length == 0) {
                    this.toast.show("没有查询到楼层")
                    return
                  }
                  this.isBottomVisible(searchList.FNFNameList)
                }}
                onLongPress={() => {
                  Alert.alert("提示", "确定需要重置筛选条件吗？", [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '确定',
                    onPress: () => {
                      this.setState({
                        FloorsName: '',
                        FloorsPicker: '',
                      })
                    }
                  }])
                }}>
                <View style={styles.sideMenu_Top1} >
                  <View style={styles.sideMenu_TitleView}>
                    <Text style={styles.sideMenu_Title}>{"楼层筛选"}</Text>
                  </View>
                  <View style={styles.sideMenu_DownView}>
                    <View style={styles.sideMenu_DownView_View}>
                      {
                        !this.state.FloorsName ?
                          <Text style={styles.sideMenu_DownViewTitle}>{"展开"}</Text>
                          :
                          <Text numberOfLines={1} style={[styles.sideMenu_DownViewTitle, { color: '#419fff' }]}>{this.state.FloorsName}</Text>
                      }
                      <Icon name='down' color='gray' iconStyle={styles.sideMenu_DownViewIcon} type='antdesign' ></Icon>

                    </View>
                  </View>
                </View>
              </TouchableHighlight>

            </View>
          </View>

          {/* 按钮组 */}
          <View style={[styles.sideMenu_Bottom, { flexDirection: 'row', justifyContent: 'center', marginTop: 20 }]}>
            <Button
              type='outline'
              title='重置'
              containerStyle={{ flex: 1, borderRadius: 0 }}
              buttonStyle={{ borderRadius: 0, borderColor: '#419fff' }}
              onPress={() => {
                Alert.alert("提示", "确定需要重置筛选条件吗？", [{
                  text: '取消',
                  style: 'cancel'
                }, {
                  text: '确定',
                  onPress: () => {
                    this.setState({
                      compTypeName: '',
                      compTypePicker: '',
                      designTypeName: '',
                      designTypePicker: '',
                      InsresultsName: '',
                      InsresultsPicker: '',
                      FloorNOName: '',
                      FloorNOPicker: '',
                      FloorsName: '',
                      FloorsPicker: '',
                      resData: [],
                      page: 1
                    })
                    pageNo = 1
                    data.insresults = ''
                    data.designType = ''
                    data.floorNoName = ''
                    data.floorName = ''
                    data.compTypeName = ''
                    data.pageIndex = 1

                    console.log("🚀 ~ file: ChildOneFlat.js ~ line 2230 ~ ChildOne ~ data", data)
                    this.requestData1()
                  }
                }])
              }}
            />
            <Button
              //type='outline'
              title='搜索'
              ViewComponent={LinearGradient}
              linearGradientProps={{ colors: ['#3FF0CC', 'rgba(65,159,255,0.80)'], start: { x: 1, y: 1 }, end: { x: 0, y: 1 } }}
              containerStyle={{ flex: 1, borderRadius: 0 }}
              buttonStyle={{ borderRadius: 0 }}
              onPress={() => {
                this.setState({ resData: [], page: "1" })
                pageNo = 1
                data.insresults = this.state.InsresultsName
                data.designType = this.state.designTypeName
                data.floorNoName = this.state.FloorNOName
                data.floorName = this.state.FloorsName
                data.compTypeName = this.state.compTypeName
                data.pageIndex = 1
                this.setState({ isSideOpen: false })
                this.requestData1()
              }}
            />
          </View>

          {this.BottomList()}
        </View>
      menu1 = <View style={styles.sideMenu_Main} >
        {/* 是否合格 */}
        <View style={styles.sideMenu_Bottom}>
          <View style={styles.sideMenu_Top} >
            <Text style={styles.sideMenu_Title}>{"是否合格"}</Text>
          </View>
          <View style={styles.sideMenu_Content}>
            <Picker
              mode='dropdown'
              selectedValue={InsresultsPicker}
              style={styles.sideMenu_Picker}
              enabled={!pageLoading}
              onValueChange={(value) => {
                if (value != '请选择') {
                  data.insresults = value
                  this.SearchPostQualityFinProducts()
                  this.setState({
                    InsresultsPicker: value,
                    InsresultsName: value,
                  })
                } else {
                  data.insresults = ""
                  this.setState({
                    InsresultsPicker: "请选择",
                    InsresultsName: ""
                  })
                  this.SearchPostQualityFinProducts()
                }

              }}
            >
              <Picker.Item label={'请选择是否合格'} value={'请选择'} />
              {resDataInsresults.map((item) => {
                return (
                  <Picker.Item label={item} value={item} />
                )
              })}
            </Picker>
          </View>
        </View>
        {/* 构件类型 */}
        <View style={styles.sideMenu_Bottom}>
          <View style={styles.sideMenu_Top} >
            <Text style={styles.sideMenu_Title}>{"构件类型筛选"}</Text>
          </View>
          <View style={styles.sideMenu_Content}>
            <Picker
              mode='dropdown'
              selectedValue={compTypePicker}
              style={styles.sideMenu_Picker}
              enabled={!pageLoading}
              onValueChange={(value) => {
                let compTypeId = ''
                if (value != '请选择') {
                  data.compTypeName = value
                  this.SearchPostQualityFinProducts()
                  this.setState({
                    compTypePicker: value,
                    compTypeId: compTypeId,
                    compTypeName: value
                  })
                } else {
                  data.compTypeName = ""
                  this.setState({
                    compTypePicker: value,
                    compTypeId: '',
                    compTypeName: ''
                  })
                  this.SearchPostQualityFinProducts()
                }

              }}
            >
              <Picker.Item label={'请选择构件类型'} value={'请选择'} />
              {searchList.compTypeNameList.map((item) => {
                return (
                  <Picker.Item label={item} value={item} />
                )
              })}
            </Picker>
          </View>
        </View>
        {/* 设计型号 */}
        <View style={styles.sideMenu_Bottom}>
          <View style={styles.sideMenu_Top} >
            <Text style={styles.sideMenu_Title}>{"设计型号筛选"}</Text>
          </View>
          <View style={styles.sideMenu_Content}>
            <SearchBar
              platform='android'
              placeholder={'请输入设计型号'}
              placeholderTextColor='#666'

              containerStyle={{ backgroundColor: 'transparent' }}
              inputContainerStyle={{ backgroundColor: 'transparent', height: 25 }}
              inputStyle={{ color: '#666', fontSize: RFT * 3.6 }}
              searchIcon={designSearchIcon()}
              //showLoading={pageLoading}
              clearIcon={designClearIcon()}
              showCancel={false}
              cancelButtonProps={{ buttonStyle: { display: 'none' } }}
              disabled={pageLoading}
              //cancelButtonTitle=''
              //cancelButtonProps={{ buttonTextStyle: { color: "#666", fontSize: RFT * 3.8 } }}
              value={designTypeName}
              onEndEditing={() => {
                data.designType = designTypeName
                this.SearchPostQualityFinProducts()
              }}
              onChangeText={(text) => {
                this.setState({ designTypeName: text })
              }}
              onClear={() => {
                data.designType = ""
                this.setState({ designTypeName: '' })
                this.SearchPostQualityFinProducts()
              }}
            //onChangeText={this.TextChangeDesignType}
            //input={(input) => { this.setState({ designTypeName: input }) }}
            />
          </View>
        </View>
        {/* 楼号 */}
        <View style={styles.sideMenu_Bottom}>
          <View style={styles.sideMenu_Top} >
            <Text style={styles.sideMenu_Title}>{"楼号筛选"}</Text>
          </View>
          <View style={styles.sideMenu_Content}>
            <Picker
              mode='dropdown'
              selectedValue={FloorNOPicker}
              style={styles.sideMenu_Picker}
              onValueChange={(value) => {
                let name = value
                if (value != '请选择') {
                  this.setState({
                    FloorNOPicker: name,
                    FloorNOName: name
                  })
                  data.floorNoName = name
                  data.floorName = ""

                  searchList.FNFNameList = []
                  searchList.floorNameList.map((item) => {
                    let itemarr = item.split("+")
                    let prjId = itemarr[0]
                    let fnnName = itemarr[1]
                    let name = itemarr[2]
                    if (prjId == projectId) {
                      if (fnnName == value) {
                        if (searchList.FNFNameList.indexOf(name) == -1) {
                          searchList.FNFNameList.push(name)
                        }
                      }
                    }
                  })
                } else {
                  data.floorNoName = ""
                  data.floorName = ""
                  this.setState({
                    FloorNOPicker: value,
                    FloorNOId: '',
                    FloorNOName: ''
                  })
                }
                this.SearchPostQualityFinProducts()
              }}

            >
              <Picker.Item label={'请选择楼号'} value={'请选择'} />
              {searchList.prjFNNameList.map((item) => {
                return (
                  <Picker.Item label={item} value={item} />
                )
              })}
            </Picker>
          </View>
        </View>
        {/* 楼层 */}
        <View style={styles.sideMenu_Bottom}>
          <View style={styles.sideMenu_Top} >
            <Text style={styles.sideMenu_Title}>{"楼层筛选"}</Text>
          </View>
          <View style={styles.sideMenu_Content}>
            <Picker
              mode='dialog'
              selectedValue={FloorsPicker}
              style={styles.sideMenu_Picker}
              onValueChange={(value) => {
                if (FloorNOPicker == '请选择') {
                  this.toast.show('请先选择楼号')
                  return
                }
                if (searchList.floorNameList.length == 0) {
                  this.toast.show('无楼层')
                  return
                }
                let floorId = ''
                if (value != '请选择') {
                  //floorId = resDataFloors[position - 1].rowguid
                  this.setState({
                    FloorsPicker: value,
                    FloorsId: floorId,
                    FloorsName: value
                  })
                  data.floorName = value
                } else {
                  data.floorName = ''
                  this.setState({
                    FloorsId: '',
                    FloorsName: ''
                  })
                }
                this.SearchPostQualityFinProducts()
              }}
            >
              <Picker.Item label={'请选择楼层'} value={'请选择'} />
              {searchList.FNFNameList.map((item) => {
                return (
                  <Picker.Item label={item} value={item} />
                )
              })}
            </Picker>
          </View>
        </View>
      </View>
    }
    //项目
    else if (url.indexOf("Project") != -1) {
      menu =
        <View style={styles.sideMenu_Main} >
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          <View style={{ flex: 6 }}>
            {/* 项目类型1 */}
            <View style={styles.sideMenu_Bottom}>
              <TouchableHighlight
                underlayColor={"lightgray"}
                onPress={() => {
                  sideType = 'project'
                  console.log("🚀 ~ file: ChildOneFlat.js ~ line 2899 ~ ChildOne ~ render ~ searchList.projectNameList", searchList.projectNameList)

                  this.setState({ sideType: 'project' })
                  this.isBottomVisible(searchList.projectNameList)
                }}
                onLongPress={() => {
                  Alert.alert("提示", "确定需要重置筛选条件吗？", [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '确定',
                    onPress: () => {
                      this.setState({
                        projectName: '',
                        projectPicker: '',
                      })
                    }
                  }])
                }}>
                <View style={styles.sideMenu_Top1} >
                  <View style={styles.sideMenu_TitleView}>
                    <Text style={styles.sideMenu_Title}>{"项目筛选"}</Text>
                  </View>
                  <View style={styles.sideMenu_DownView}>
                    <View style={styles.sideMenu_DownView_View}>
                      {
                        !this.state.projectName ?
                          <Text style={styles.sideMenu_DownViewTitle}>{"展开"}</Text>
                          :
                          <Text numberOfLines={1} style={[styles.sideMenu_DownViewTitle, { color: '#419fff' }]}>{this.state.projectName}</Text>
                      }
                      <Icon name='down' color='gray' iconStyle={styles.sideMenu_DownViewIcon} type='antdesign' ></Icon>

                    </View>
                  </View>
                </View>
              </TouchableHighlight>
            </View>
          </View>

          {/* 按钮组 */}
          <View style={[styles.sideMenu_Bottom, { flexDirection: 'row', justifyContent: 'center', marginTop: 20 }]}>
            <Button
              type='outline'
              title='重置'
              containerStyle={{ flex: 1, borderRadius: 0 }}
              buttonStyle={{ borderRadius: 0, borderColor: '#419fff' }}
              onPress={() => {
                this.setState({
                  projectName: '',
                  projectPicker: '',
                  page: 1,
                  resData: []
                })
                pageNo = 1
                data.pageIndex = 1
                data.projectName = ""

                this.requestData1()
              }}
            />
            <Button
              //type='outline'
              title='搜索'
              containerStyle={{ flex: 1, borderRadius: 0 }}
              ViewComponent={LinearGradient}
              linearGradientProps={{ colors: ['#3FF0CC', 'rgba(65,159,255,0.80)'], start: { x: 1, y: 1 }, end: { x: 0, y: 1 } }}
              buttonStyle={{ borderRadius: 0 }}
              onPress={() => {
                this.setState({
                  resData: [],
                  page: 1
                })
                pageNo = 1
                data.pageIndex = 1
                data.search = this.state.projectName
                this.setState({ isSideOpen: false })
                this.requestData1()
              }}
            />
          </View>

          {this.BottomList()}
        </View>
      menu1 =
        <View style={styles.sideMenu_Main} >
          <View style={styles.sideMenu_Bottom}>
            <View style={styles.sideMenu_Top} >
              <Text style={styles.sideMenu_Title}>{"项目筛选"}</Text>
            </View>
            <View style={styles.sideMenu_Content}>
              <Picker
                mode='dropdown'
                selectedValue={projectPicker}
                style={styles.sideMenu_Picker}
                onValueChange={(value, position) => {
                  let projectId = ""
                  if (value != "请选择") {
                    if (url.indexOf("GetProduceDailyRecordProject") != -1) {
                      this.SearchPostDailyRecordProject(value)
                      this.setState({
                        projectPicker: value,
                        projectName: value,
                      })
                    } else {
                      this.setState({
                        projectPicker: value,
                        projectName: value,
                      })
                      this.SearchPostproject(value)
                    }
                  } else {
                    if (url.indexOf("GetProduceDailyRecordProject") != -1) {
                      this.SearchPostDailyRecordProject("")
                      this.setState({
                        projectPicker: "",
                        projectName: "",
                        projectId: "",
                      })
                    } else if (url.indexOf("GetProduceTask") != -1) {
                      this.SearchPostproductLine(productLineName, editEmployeeName, "", search)
                      this.setState({
                        projectPicker: "",
                        projectName: "",
                      })
                    } else {
                      this.SearchPostproject("")
                      this.setState({
                        projectPicker: "",
                        projectName: "",
                      })
                    }

                  }
                }}
              >
                <Picker.Item label={'请选择项目'} value={'请选择'} />
                {searchList.projectNameList.map((item) => {
                  return (
                    <Picker.Item label={item} value={item} />
                  )
                })}
              </Picker>
            </View>
          </View>
        </View>
    } else {

    }
    return menu;
  }

  //渲染页面函数
  render() {
    const {
    } = this.state

    const {
      isCompRoomPickerVisible, compRoomId, compRoomName, compRoomPicker,
      isCompLibPickerVisible, compLibId, compLibName, compLibPicker,
      isBigStatePickerVisible, bigStateId, bigStateName, bigStatePicker,
      isDesignTypePickerVisible, designTypeId, designTypeName, designTypePicker,
      isproductLinePickerVisible, productLineId, productLineName, productLinePicker, resDataProductLine,
      isProjectPickerVisible, projectPicker, pickerValue, projectId, projectName,
      isFloorNOPickerVisible, resDataFloorNO, FloorNOPicker, FloorNOId, FloorNOName,
      isFloorsPickerVisible, resDataFloors, FloorsId, FloorsName, FloorsPicker,
      isCompTypePickerVisible, resDataCompType, compTypeId, compTypeName, compTypePicker,
      isInsresultsPickerVisible, resDataInsresults, InsresultsName, InsresultsPicker,
      isEditEmployeeNamePickerVisible, editEmployeeName, editEmployeeNamePicker,
      searchList, pageLoading, search } = this.state
    //let menu = <View></View>


    console.log(ToDay, YesterDay, BeforeYesterDay)
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const title = navigation.getParam('title')
    //从主页面接收孙子页面标题
    var sonTitletest = navigation.getParam('sonTitle');

    let menu = this.menu()
    menu1 = <ScrollView style={styles.sideMenu_Main} >
      <Toast ref={(ref) => { this.toast = ref; }} position="center" />
      {/* 构件类型1 */}
      <View style={styles.sideMenu_Bottom}>
        <TouchableHighlight
          underlayColor={"lightgray"}
          onPress={() => {
            sideType = 'compType'
            this.setState({ sideType: 'compType' })
            this.isBottomVisible(searchList.compTypeNameList)
          }}
          onLongPress={() => {
            return
          }}>
          <View style={styles.sideMenu_Top1} >
            <View style={styles.sideMenu_TitleView}>
              <Text style={styles.sideMenu_Title}>{"构件类型筛选1"}</Text>
            </View>
            <View style={styles.sideMenu_DownView}>
              {/* <View style={{ flex: 1 }}>
                <Text style={{ fontSize: 15, textAlign: 'right' }}></Text>
              </View> */}
              <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end', alignContent: 'flex-end' }}>
                {
                  !this.state.compTypeName ?
                    <Text style={styles.sideMenu_DownViewTitle}>{"展开"}</Text>
                    :
                    <Text style={[styles.sideMenu_DownViewTitle, { color: '#419fff' }]}>{this.state.compTypeName}</Text>
                }
                <Icon name='down' color='gray' iconStyle={{ fontSize: 13, top: 5, marginLeft: 5 }} type='antdesign' ></Icon>

              </View>
            </View>
          </View>
        </TouchableHighlight>

      </View>
      {/* 构件类型2 */}
      <View style={styles.sideMenu_Bottom}>
        <TouchableHighlight
          underlayColor={"lightgray"}
          onPress={() => {
            sideType = 'compType'
            this.setState({ comptypeVisible: !this.state.comptypeVisible })
            //this.isBottomVisible(searchList.compTypeNameList)
          }}
          onLongPress={() => {
            return
          }}>
          <View style={styles.sideMenu_Top1} >
            <View style={styles.sideMenu_TitleView}>
              <Text style={styles.sideMenu_Title}>{"构件类型筛选2"}</Text>
            </View>
            <View style={styles.sideMenu_DownView}>
              {/* <View style={{ flex: 2 }}>
                <Text style={{ fontSize: 15, textAlign: 'right' }}>{""}</Text>
              </View> */}
              <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end', alignContent: 'flex-end' }}>
                <Text style={styles.sideMenu_DownViewTitle}>展开</Text>
                <Icon name='down' color='gray' iconStyle={{ fontSize: 13, top: 5, marginLeft: 5 }} type='antdesign' ></Icon>
              </View>
            </View>
          </View>

        </TouchableHighlight>
        {
          this.state.comptypeVisible ?
            <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
              {
                searchList.compTypeNameList.map((item, index) => {
                  return (
                    <Button
                      type='outline'
                      title={item}
                      titleProps={{ numberOfLines: 1 }}
                      titleStyle={{ fontSize: 13, color: 'gray', color: this.state.focusButton == index ? 'white' : 'gray' }}
                      buttonStyle={{
                        borderRadius: 10,
                        paddingHorizontal: 5,
                        borderColor: 'gray',
                        //flex: 1,
                        backgroundColor: this.state.focusButton == index ? '#419FFF' : 'white'
                      }}
                      containerStyle={{
                        //flex: 1,
                        //borderRadius: 20,
                        marginTop: 6,
                        marginHorizontal: 6,
                        width: 90
                      }}
                      onPress={() => {
                        this.setState({ focusButton: index })
                      }}
                    />
                  )
                })
              }
            </View>
            : <View></View>
        }
      </View>
      {/* 设计型号 */}
      <View style={styles.sideMenu_Bottom}>
        <View style={styles.sideMenu_Top} >
          <Text style={styles.sideMenu_Title}>{"设计型号筛选"}</Text>
        </View>
        <View style={styles.sideMenu_Content}>
          <SearchBar
            platform='android'
            placeholder={'请输入设计型号'}
            placeholderTextColor='#666'

            containerStyle={{ backgroundColor: 'transparent' }}
            inputContainerStyle={{ backgroundColor: 'transparent', height: 25 }}
            inputStyle={{ color: '#666', fontSize: RFT * 3.6 }}
            searchIcon={designSearchIcon()}
            //showLoading={pageLoading}
            clearIcon={designClearIcon()}
            showCancel={false}
            cancelButtonProps={{ buttonStyle: { display: 'none' } }}
            disabled={pageLoading}
            //cancelButtonTitle=''
            //cancelButtonProps={{ buttonTextStyle: { color: "#666", fontSize: RFT * 3.8 } }}
            value={designTypeName}
            onEndEditing={() => {
              this.SearchPostProducedComponents(designTypeName, bigStateName, compTypeName, compRoomName, compLibName, FloorNOName, FloorsName, search)
            }}
            onChangeText={(text) => {
              this.setState({ designTypeName: text })
            }}
            onClear={() => {
              this.setState({ designTypeName: '' })
              this.SearchPostProducedComponents("", bigStateName, compTypeName, compRoomName, compLibName, FloorNOName, FloorsName, search)
            }}
          //onChangeText={this.TextChangeDesignType}
          //input={(input) => { this.setState({ designTypeName: input }) }}
          />
        </View>
      </View>

      {/* 按钮组 */}
      <View style={[styles.sideMenu_Bottom, { flexDirection: 'row', justifyContent: 'center' }]}>
        <Button
          type='outline'
          title='重置'
          containerStyle={{ flex: 1, borderRadius: 0 }}
          buttonStyle={{ borderRadius: 0, borderColor: '#419fff' }}
        />
        <Button
          //type='outline'
          title='搜索'
          containerStyle={{ flex: 1, borderRadius: 0 }}
          buttonStyle={{ borderRadius: 0, backgroundColor: '#419fff' }}
        />
      </View>

      {this.BottomList()}
    </ScrollView>

    // 是否显示加载页，由接口返回数据判断
    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (

        <SideMenu menu={menu} isOpen={this.state.isSideOpen} menuPosition={'right'}
          onChange={(isOpen) => {
            this.setState({ isSideOpen: isOpen })
          }}
          openMenuOffset={width * 0.85}
        >

          <View style={{ flexDirection: 'column', flex: 1, backgroundColor: '#f9f9f9' }}>
            <NavigationEvents
              onWillFocus={this.UpdateControl} />
            <Toast ref={(ref) => { this.toastMain = ref; }} position="center" />
            <View style={{ flexDirection: 'row', backgroundColor: rgb(65, 159, 255, 0.1) }}>
              {
                selectList.map((item, index) => {
                  return (
                    <Button
                      buttonStyle={{
                        borderRadius: -10,
                        borderBottomColor: this.state.typeFocusButton == index ? '#419fff' : '#999997',
                        borderBottomWidth: 3,
                        // width: 48,
                        height: 50,
                        marginLeft: 25
                      }}
                      containerStyle={{
                        borderColor: this.state.typeFocusButton == index ? '#419fff' : '#999997',
                      }}
                      title={item.name}
                      titleStyle={{
                        fontFamily: 'STHeitiSC-Light',
                        textAlign: 'center',
                        fontSize: 17,
                        fontWeight: "600",
                        //fontSize: RFT * 3.3,
                        color: this.state.typeFocusButton == index ? '#419fff' : '#333333',
                      }}
                      type="clear"
                      onPress={() => {
                        Url.Login()
                        const navigation = this.props.navigation
                        this.setState({ typeFocusButton: index, pageLoading: true }, () => {
                          if (index == 0) {
                            url = Url.url + Url.Produce + 'PostProduceCompDetail'
                            const projectId = navigation.getParam('projectId'); //父页面获取项目id
                            const floorNoId = navigation.getParam('floorNoId');
                            const floorId = navigation.getParam('floorId');
                            const compTypeId = navigation.getParam('compTypeId');
                            this.setState({ projectId: projectId }, () => {
                              this.seachListFunc([], "producedcomponents")
                            })
                            data = {
                              "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
                              "projectId": projectId,
                              "compTypeId": compTypeId,
                              "floorNoId": floorNoId,
                              "floorId": floorId,
                              "pageIndex": 1,
                              "dataCount": 40,
                              "isPrj": 0
                            }
                            this.setState({ resData: [] })
                            this.requestData1()
                          } else {
                            url = Url.url + Url.Produce + 'PostProduceCompStatistics'
                            console.log("🚀 ~ file: ChildOneFlatNew.js:5488 ~ this.setState ~ url:", url)
                            sonUrl = ''
                            const projectId = navigation.getParam('projectId'); //父页面获取项目id
                            const floorNoId = navigation.getParam('floorNoId');
                            const floorId = navigation.getParam('floorId');
                            const compTypeId = navigation.getParam('compTypeId');
                            const compState = navigation.getParam('compState');
                            data = {
                              "factoryId": Url.Fid.substring(0, Url.Fid.length - 1),
                              "projectId": projectId,
                              "compState": compState,
                              "compTypeId": compTypeId,
                              "floorNoId": floorNoId,
                              "floorId": floorId,
                              "pageIndex": 1,
                              "dataCount": 40,
                              "isPrj": 0
                            }
                            this.setState({ resData: [] })
                            this.requestData1()
                          }
                        })

                      }}
                    />
                  )
                })
              }
            </View>
            <View style={{ flex: 1 }}>
              {/* 是否显示日期选择按钮组 */}
              {url.indexOf('DailyRecordProject') != -1 && sonUrl == '' ? this.ButtonGroupday() : <View style={{ backgroundColor: 'white' }}></View>}
              {(url.indexOf('PostProduceTaskPage') != -1 && url.indexOf('PostProduceTaskCompStatelistPage') == -1) ? this.TaskStateButton() : <View></View>}
              {((url.indexOf('PostStockLoaoutWarePage') != -1 && title.indexOf('成品出库') == -1) && url.indexOf('GetStockLoaoutWareCompDetails') == -1) ? this.LoaoutStateButton() : <View></View>}

              {/* 是否显示筛选顶栏 */}
              {/* {this.Top()} */}

              {/* 搜索栏 */}
              <View style={{ flexDirection: 'row', backgroundColor: 'transparent', width: width * 0.96 }}>
                <LinearGradient
                  start={{ x: 1, y: 1 }} end={{ x: 0, y: 1 }}
                  colors={['#3FF0CC', 'rgba(65,159,255,0.80)']}
                  style={{ flex: 10, marginLeft: width * 0.035, borderRadius: 10, marginTop: 15, flexDirection: 'row' }}
                >
                  <SearchBar
                    platform='android'
                    placeholder={'按内容进行搜索...'}
                    placeholderTextColor='white'
                    // style={{ backgroundColor: 'red', borderColor: 'red', borderWidth: 10 }}
                    containerStyle={{ backgroundColor: 'transparent', paddingTop: 0, paddingBottom: 0, flex: 4 }}
                    inputContainerStyle={{ backgroundColor: 'transparent', height: 43, top: 1 }}
                    inputStyle={{ color: '#FFFFFF' }}
                    searchIcon={defaultSearchIcon()}
                    clearIcon={defaultClearIcon()}
                    cancelIcon={defaultClearIcon()}
                    leftIconContainerStyle={{ left: 8 }}
                    cancelButtonTitle={"取消"}
                    cancelButtonProps={{ buttonTextStyle: { color: "#fff" } }}
                    round={true}
                    value={this.state.search}
                    onChangeText={this.TextChange}
                    returnKeyType='search'
                    input={(input) => { this.setState({ search: input }) }}
                    onSubmitEditing={() => {
                      data.search = this.state.search
                      pageNo = 1
                      data.pageIndex = 1
                      this.setState({ pageLoading: true, resData: [], page: 1 })
                      this.requestData1()
                    }} />
                  <Button
                    type='clear'
                    title={this.state.length + '条记录'}
                    titleStyle={{ fontSize: 16, color: 'white', textAlign: 'right' }}
                    style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}
                    containerStyle={{ backgroundColor: 'transparent', top: 1 }}
                  />

                </LinearGradient>
                {
                  isShow ? <TouchableHighlight
                    underlayColor={"lightgray"}
                    onPress={() => {
                      this.setState({
                        isSideOpen: true
                      })
                    }}
                  >
                    <View style={{ top: 23, left: 5, flex: 1 }}>
                      <Icon
                        name='filter'
                        type='antdesign'
                        color='#419FFF'
                        size={28}
                        onPress={() => {
                          this.setState({
                            isSideOpen: true
                          })
                        }} />
                    </View>
                  </TouchableHighlight> :
                    <View style={{ flex: 0 }}></View>
                }



              </View>


              <View style={{ flex: 14.5 }}>
                {/* 长列表 */}
                {
                  this.state.pageLoading ?
                    <View style={styles.loading}>
                      <ActivityIndicator
                        animating={true}
                        color='#419FFF'
                        size="large" />
                    </View> :
                    <FlatList
                      style={{ flex: 1, borderRadius: 10 }}
                      ref={(flatList) => this._flatList = flatList}
                      //ItemSeparatorComponent={this._separator}
                      renderItem={this._renderItem}
                      onRefresh={this.refreshing1}
                      refreshing={false}
                      //控制上拉加载，两个平台需要区分开
                      onEndReachedThreshold={this.state.resData.length > 5 ? 0.0001 : -1}
                      onEndReached={() => {
                        if (this.state.resData.length > 5) {
                          this._onload1()
                        }
                      }}
                      numColumns={1}
                      //ListFooterComponent={this._footer}
                      //columnWrapperStyle={{borderWidth:2,borderColor:'black',paddingLeft:20}}
                      //horizontal={true}
                      data={this.state.resData}
                    />
                }

              </View>
            </View>

          </View>
        </SideMenu>

      )
    }

  }

  //接口返回的对象数组，长列表对数据遍历
  _renderItem = ({ item, index }) => {
    console.log("🚀 ~ file: ChildOneFlatNew.js:5640 ~ url:", url)
    const { navigation } = this.props;
    const itemmain = navigation.getParam('item')
    let sonTitletest = item.bigState ? item.bigState : navigation.getParam('sonTitle');
    let urlNavigate = ''
    let buttonDate = ToDay
    if (url.indexOf('PostWaitingPoolProjectPage') != -1) {
      urlNavigate = Url.url + Url.Produce + '/PostWaitingPoolCompPage2'
    } else if (url.indexOf('PostProduceDailyRecordProjectPage2') != -1) {
      if (this.state.selectedIndex == 0) {
        buttonDate = ToDay
      } else if (this.state.selectedIndex == 1) {
        buttonDate = YesterDay
      } else {
        buttonDate = BeforeYesterDay
      }
      urlNavigate = Url.url + Url.Produce + 'PostProduceDailyRecordPage'
    } else if (url.indexOf('PostQualityHidcheckProjectPage2') != -1) {
      urlNavigate = Url.url + Url.Quality + 'PostQualityHidcheckPage'
    } else if (url.indexOf('PostQualityReFinProductsProjectPage') != -1) {
      urlNavigate = Url.url + Url.Quality + 'PostQualityReFinProductsPage2'
    } else if (url.indexOf('PostQualityScrapProjectPage') != -1) {
      urlNavigate = Url.url + Url.Quality + 'PostQualityScrapPage'
    } else {
      urlNavigate = Url.url + Url.Quality + 'PostQualityFinProductsPage'
    }
    //待产池项目, 日生产记录项目, 隐检项目页面（通用项目页面）
    //if ((typeof (item.compNum) != "undefined" || typeof (item.compNumPass) != "undefined") && item.projectState === undefined && item.projectId !== undefined) {
    if ((url.indexOf("PostWaitingPoolProjectPage") != -1 ||
      url.indexOf("PostProduceDailyRecordProjectPage2") != -1 ||
      url.indexOf("PostQualityHidcheckProjectPage2") != -1 ||
      url.indexOf("PostQualityFinProductsProjectPage") != -1 ||
      url.indexOf("PostQualityReFinProductsProjectPage") != -1 ||
      url.indexOf("PostQualityScrapProjectPage") != -1) && sonUrl == '') {
      return (
        <Card containerStyle={styles.card_containerStyle}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.push('ChildOne', {
                title: '构件列表',
                sonTitle: sonTitletest,
                selectedIndex: this.state.selectedIndex,
                date: buttonDate,
                projectId: item.projectId,
                length: item.compNum,
                url: url,
                sonUrl: urlNavigate
              })
            }}>
            <View style={{ flexDirection: 'row', }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text style={[styles.SN_Text]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={styles.text_View}>
                {/* 项目名 */}
                {
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={[styles.textname, { fontSize: 16, fontWeight: "600" }]}>项目全称：</Text>
                    <Text numberOfLines={1} style={[styles.text, { fontSize: 16, fontWeight: "600", flex: 1 }]}> {item.projectName}</Text>
                  </View>
                }
                {/* {
                  item.projectAbb ? <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.textname]}>项目简称：</Text>
                  <Text style={[styles.text]}> {item.projectAbb}</Text>
                </View> : <View></View>
                } */}
                {/* 开工时间，日生产记录没有,所以只要排除他就好 */}
                <View style={url.indexOf('PostProduceDailyRecordProjectPage2') == -1 ? { flexDirection: 'row' } : styles.hidden}>
                  <Text style={styles.textname}>开工日期：</Text>
                  <Text style={styles.text}> {item.startTime}</Text>
                </View>
                {/* 数量，根据页面选择说明 */}
                <View style={url.indexOf('PostQualityFinProductsProjectPage') != -1 ? styles.hidden : { flexDirection: 'row' }}>
                  <Text style={styles.textname}>{url.indexOf('PostWaitingPoolProjectPage') != -1 ? '待产池数量：' : (url.indexOf('PostProduceDailyRecordProjectPage2') != -1 ? '生产数量：' : (url.indexOf('PostQualityScrapProjectPage') != -1 ? '报废数量：' : '已检数量：'))}</Text>
                  <Text style={styles.text}> {item.compNum}件</Text>
                </View>
                {/* 生产方量，目前只有日生产记录有。 */}
                <View style={url.indexOf('PostProduceDailyRecordProjectPage2') != -1 ? { flexDirection: 'row' } : styles.hidden}>
                  <Text style={styles.textname}>生产方量：</Text>
                  <Text style={styles.text}> {item.compVolume}m³</Text>
                </View>
                {/* 成品检查模块， */}
                <View style={url.indexOf('PostQualityFinProductsProjectPage') != -1 ? { flexDirection: 'column' } : styles.hidden}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.textname}>已检数量：</Text>
                    <Text style={styles.text}> {item.compNumPass + item.compNumNotPass}件</Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.textname}>合格数量：</Text>
                    <Text style={styles.text}> {item.compNumPass}件</Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.textname}>不合格数量：</Text>
                    <Text style={styles.text}> {item.compNumNotPass}件</Text>
                  </View>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    } //已生产构件项目页面
    else if (url.indexOf("PostProduceProjectPage") != -1 && sonUrl == '') {
      return (
        <Card containerStyle={styles.card_containerStyle}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.push('ChildOne', {
                sonTitle: sonTitletest,
                title: '已生产构件明细',
                projectId: item.projectId,
                url: url,
                sonUrl: Url.url + Url.Produce + 'PostProduceComponentStatePage',
              })
            }}>
            <View style={{ flexDirection: 'row', }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={[styles.text_View]}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ flex: 3, flexDirection: 'row' }}>
                    <Text style={styles.textname}>项目名称：</Text>
                    <Text style={[styles.text, { color: '#419FFF', width: width / 2 }]}> {item.projectAbb}</Text>
                  </View>
                  <View style={{ flex: 1 }}>
                    {
                      item.projectState ?
                        <View style={[styles.badgeView, { backgroundColor: 'rgba(98, 178, 250, 0.22)' }]}>
                          <Badge
                            badgeStyle={{ backgroundColor: '#64B1FC' }}
                            containerStyle={{ marginRight: 8, top: 10 }}
                          />
                          <Text style={[styles.text, { color: '#64B1FC', fontWeight: 'bold' }]}>{item.projectState}</Text>
                        </View>
                        :
                        <View></View>
                    }
                  </View>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>开工日期：</Text>
                  <Text style={styles.text}> {item.startTime}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>已生产构件：</Text>
                  <Text style={styles.text}> {item.compNum}件</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>已生产方量：</Text>
                  <Text style={styles.text}> {item.compVolume}m³</Text>
                </View>
              </View>
              <View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    }
    //任务单列表
    else if (url.indexOf("PostProduceTaskCompStatelistPage") != -1) {
      return (
        <Card containerStyle={styles.card_containerStyle}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.push('ChildTwo', {
                sonTitle: sonTitletest,
                compitem: item,
                item: itemmain,
                sonUrl: Url.url + Url.Produce + item.task_id + '/GetProduceTaskCompDetails',
                //sonUrl: Url.url + Url.Produce + item._rowguid + '/GetProduceTaskCompState',
                url: Url.url + Url.Produce + Url.Fid + 'GetProduceTask/0'
              })
            }} >
            <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={styles.text_View}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ flexDirection: 'row', flex: 3 }}>
                    <Text style={styles.textname}>构件编码：</Text>
                    <Text style={[styles.text, { color: 'orange' }]}> {item.compCode}</Text>
                  </View>
                  <View style={{ flex: 1 }}>
                    {
                      item.state ? <View style={{ flexDirection: 'row', justifyContent: 'center', alignContent: 'center' }}>
                        <Text style={[styles.text, { color: '#419FFF' }]}>{item.state}</Text>
                      </View> :
                        <View></View>
                    }
                  </View>
                </View>

                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>构件类型：</Text>
                  <Text style={[styles.text, {}]}> {item.compTypeName}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>设计型号：</Text>
                  <Text style={[styles.text, {}]}> {item.designType}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>项目名称：</Text>
                  <Text style={[styles.text, {}]}> {item.projectName}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>楼号楼层：</Text>
                  <Text style={[styles.text, {}]}> {item.floorNoName + '(' + item.floorName + ')'}</Text>
                </View>

              </View>

            </View>
          </TouchableOpacity>
        </Card>
      )
    }
    //任务单
    else if (url.indexOf("PostProduceTaskPage") != -1 /*&& url.indexOf("GetProduceTaskCompDetails") == -1 */) {
      return (
        <Card containerStyle={styles.card_containerStyle}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('ChildTwo', {
                sonTitle: sonTitletest,
                item: item,
                //sonUrl: Url.url + Url.Produce + item._rowguid + '/GetProduceTaskCompDetails',
                sonUrl: Url.url + Url.Produce + item._rowguid + '/GetProduceTaskCompState',
                url: url
              })
            }} >
            <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={styles.text_View}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ flexDirection: 'row', flex: 3 }}>
                    <Text style={styles.textname}>任务编号：</Text>
                    <Text style={[styles.text, { color: item.state == '提交' ? '#64B1FC' : 'orange' }]}> {item.formCode}</Text>
                  </View>
                  <View style={{ flex: 1 }}>
                    {
                      item.state ?
                        <View style={[styles.badgeView, { backgroundColor: item.state == '提交' ? 'rgba(98, 178, 250, 0.22)' : 'rgba(255, 122, 56, 0.22)' }]}>
                          <Badge
                            badgeStyle={{ backgroundColor: item.state == '提交' ? '#64B1FC' : 'orange' }}
                            containerStyle={{ marginRight: 8, top: 10 }}
                          />
                          <Text style={[styles.text, { color: item.state == '提交' ? '#64B1FC' : 'orange', fontWeight: 'bold' }]}>{item.state}</Text>
                        </View> :
                        <View></View>
                    }
                  </View>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>计划生产日期：</Text>
                  <Text style={[styles.text, {}]}> {item.planDate}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>生产线：</Text>
                  <Text style={[styles.text, {}]}> {item.productLineName}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>劳务队：</Text>
                  <Text style={[styles.text, {}]}> {item.labourName}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>负责人：</Text>
                  <Text style={[styles.text, {}]}> {item.editEmployeeName}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>构件数目：</Text>
                  <Text style={[styles.text, {}]}> {item.compNum}件</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )

    }
    else if (url.indexOf("PostStockLoaoutWarePage") != -1 && title.indexOf('成品出库') != -1) {
      return (
        <Card containerStyle={styles.card_containerStyle}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('ChildTwo', {
                sonTitle: sonTitletest,
                item: item,
                sonUrl: Url.url + Url.Stock + item._rowguid + '/GetStockLoaoutWareCompDetails',
                url: url
              })
            }} >
            <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={styles.text_View}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>运单编号：</Text>
                  <Text style={[styles.text, { color: 'orange' }]}> {item.formCode}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>项目名称：</Text>
                  <Text style={[styles.text, {}]}> {item.projectName}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>楼层楼号：</Text>
                  <Text style={[styles.text, {}]}> {item.buildingNo}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>构件数量：</Text>
                  <Text style={[styles.text, {}]}> {item.compNumber}件</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>发货日期：</Text>
                  <Text style={[styles.text, {}]}> {item.libTime}</Text>
                </View>
              </View>
              <View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    }
    else if (url.indexOf("PostStockLoaoutWarePage") != -1 /* && title.indexOf('成品出库') == -1 */) {
      return (
        <Card containerStyle={styles.card_containerStyle}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('ChildTwo', {
                sonTitle: sonTitletest,
                item: item,
                sonUrl: Url.url + Url.Stock + item._rowguid + '/GetStockLoaoutWareCompDetails',
                url: url
              })
            }} >
            <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={styles.text_View}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ flexDirection: 'row', flex: 3 }}>
                    <Text style={styles.textname}>计划单号：</Text>
                    <Text style={[styles.text, { color: 'orange' }]}> {item.planformCode}</Text>
                  </View>
                  <View style={{ flex: 1 }}>
                    {
                      item.state ?
                        <View style={[styles.badgeView, { backgroundColor: item.state == '已完成' ? 'rgba(98, 178, 250, 0.22)' : 'rgba(255, 122, 56, 0.22)' }]}>
                          <Badge
                            badgeStyle={{ backgroundColor: item.state == '已完成' ? '#64B1FC' : 'orange' }}
                            containerStyle={{ marginRight: 8, top: 10 }}
                          />
                          <Text style={[styles.text, { color: item.state == '已完成' ? '#64B1FC' : 'orange', fontWeight: 'bold' }]}>{item.state}</Text>
                        </View>
                        :
                        <View></View>
                    }
                  </View>
                </View>


                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>项目名称：</Text>
                  <Text style={[styles.text, {}]}> {item.projectName}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>楼层楼号：</Text>
                  <Text style={[styles.text, {}]}> {item.buildingNo}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>构件数量：</Text>
                  <Text style={[styles.text, {}]}> {item.compNumber}件</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>计划发货日期：</Text>
                  <Text style={[styles.text, {}]}> {item.planLibTime}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )

    }

    else if (url.indexOf('PostProduceCompStatistics') != -1) {
      return (
        <Card containerStyle={styles.card_containerStyle}>
          <TouchableOpacity >
            <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={styles.text_View}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ flexDirection: 'row', flex: 1 }}>
                    <Text style={styles.textname}>构件类型：</Text>
                    <Text numberOfLines={1} style={[styles.text]}> {item.compTypeName}</Text>
                  </View>
                  <View style={{ flexDirection: 'row', flex: 1 }}>
                    <Text style={styles.textname}>设计型号</Text>
                    <Text numberOfLines={1} style={[styles.text]}> {item.designType}</Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ flexDirection: 'row', flex: 1 }}>
                    <Text style={styles.textname}>版本号：</Text>
                    <Text numberOfLines={1} style={[styles.text, {}]}> {item.versionName}</Text>
                  </View>
                  <View style={{ flexDirection: 'row', flex: 1 }}>
                    <Text style={styles.textname}>体积：</Text>
                    <Text numberOfLines={1} style={[styles.text, {}]}> {item.compVolume}</Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ flexDirection: 'row', flex: 1 }}>
                    <Text style={styles.textname}>预生产量：</Text>
                    <Text numberOfLines={1} style={[styles.text, {}]}> {item.onlyPreCnt}</Text>
                  </View>
                  <View style={{ flexDirection: 'row', flex: 1 }}>
                    <Text style={styles.textname}>待排产 ：</Text>
                    <Text numberOfLines={1} style={[styles.text, {}]}> {item.toplanCnt}</Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ flexDirection: 'row', flex: 1 }}>
                    <Text style={styles.textname}>待生产 ：</Text>
                    <Text numberOfLines={1} style={[styles.text, {}]}> {item.onlyTodoCnt}</Text>
                  </View>
                  <View style={{ flexDirection: 'row', flex: 1 }}>
                    <Text style={styles.textname}>生产中：</Text>
                    <Text numberOfLines={1} style={[styles.text, {}]}> {item.onlyHideCnt}</Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ flexDirection: 'row', flex: 1 }}>
                    <Text style={styles.textname}>浇注  ：</Text>
                    <Text numberOfLines={1} style={[styles.text, {}]}> {item.onlyPourCnt}</Text>
                  </View>
                  <View style={{ flexDirection: 'row', flex: 1 }}>
                    <Text style={styles.textname}>成检合格：</Text>
                    <Text numberOfLines={1} style={[styles.text, {}]}> {item.onlyHideCnt}</Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ flexDirection: 'row', flex: 1 }}>
                    <Text style={styles.textname}>入库 ：</Text>
                    <Text numberOfLines={1} style={[styles.text, {}]}> {item.onlyPourCnt}</Text>
                  </View>
                  <View style={{ flexDirection: 'row', flex: 1 }}>
                    <Text style={styles.textname}>出库：</Text>
                    <Text numberOfLines={1} style={[styles.text, {}]}> {item.onlyOutCnt}</Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ flexDirection: 'row', flex: 1 }}>
                    <Text style={styles.textname}>退库：</Text>
                    <Text numberOfLines={1} style={[styles.text, {}]}> {item.refundCnt}</Text>
                  </View>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    }
    else if (url.indexOf('PostProduceCompDetail') != -1) {
      return (
        <Card containerStyle={styles.card_containerStyle}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('ChildTwoMain', {
                sonTitle: sonTitletest,
                item: item,
                sonUrl: Url.url + Url.Produce + item._rowguid + '/GetProjectProducedComponentsSingle/',
                url: url
              })
            }} >
            <View style={{ flexDirection: 'row' }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 11 }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, { color: '#419FFF', flex: 3 }]}>{item.compCode}</Text>
                  <View style={{ flex: 1.4 }}>
                    {
                      item.bigstatea ?
                        <View style={[styles.badgeView, { backgroundColor: item.bigstatea == '脱模待检' ? 'rgba(255, 122, 56, 0.22)' : (item.bigstatea == '出库' ? 'rgba(21, 214, 57, 0.22)' : 'rgba(98, 178, 250, 0.22)') }]}>
                          <Badge
                            badgeStyle={{ backgroundColor: item.bigstatea == '脱模待检' ? 'orange' : (item.bigstatea == '出库' ? '#15D639' : '#64B1FC') }}
                            containerStyle={{ marginRight: 8, top: 10 }}
                          />
                          <Text style={[styles.text, { color: item.bigstatea == '脱模待检' ? 'orange' : (item.bigstatea == '出库' ? '#15D639' : '#64B1FC'), fontWeight: 'bold' }]}>{item.bigstatea}</Text>
                        </View>
                        :
                        <View></View>
                    }
                    {
                      item.insresults ?
                        <View style={[styles.badgeView, { backgroundColor: item.insresults == '合格' ? 'rgba(98, 178, 250, 0.22)' : 'rgba(255, 122, 56, 0.22)' }]}>
                          <Badge
                            badgeStyle={{ backgroundColor: item.insresults == '合格' ? '#64B1FC' : 'orange' }}
                            containerStyle={{ marginRight: 8, top: 10 }}
                          />
                          <Text style={[styles.text, { color: item.insresults == '合格' ? '#64B1FC' : 'orange', fontWeight: 'bold' }]}>{item.insresults}</Text>
                        </View>
                        : <View></View>
                    }
                  </View>
                </View>
                <Text style={styles.text} >{item.projectName}</Text>
                <Text style={styles.text} >{item.compTypeName}</Text>
                <Text style={styles.text} >{item.designType}</Text>
                <Text style={styles.text}>{item.floorNoName}({item.floorName})</Text>
                {
                  item.sjtime != null ? <Text style={item.sjtime ? [styles.text, { color: 'gray' }] : styles.hidden}>{'计划生产日期：'}
                    <Text style={styles.text}>{item.sjtime.replace(/T/g, " ")}</Text>
                  </Text> : <View></View>
                }

              </View>
              {/* <View>
                 <Button
                  type='clear'
                  title={item.bigState}
                  titleStyle={{ fontSize: 14 }}
                  containerStyle={{ borderRadius: 60 }}
                  style={[item.bigState ? { flex: 1 } : styles.hidden, { borderRadius: 60, alignContent: 'flex-start' }]}
                  disabled={true}
                  disabledTitleStyle={{ color: '#419FFF' }}
                  icon={<Badge
                    //入库蓝色，脱模待检黄色，出库绿色，剩下黄色
                    status={item.bigState == '脱模待检' ? 'warning' : (item.bigState == '出库' ? 'success' : (item.bigState == '入库' ? 'primary' : (item.bigState == null ? null : 'warning')))}
                    containerStyle={{ marginHorizontal: 4 }}
                  />} />
                <Button
                  type='clear'
                  title={item.insresults}
                  titleStyle={{ fontSize: 15 }}
                  style={[item.insresults ? { flex: 1 } : styles.hidden, { borderRadius: 60, alignContent: 'flex-start' }]}
                  disabled={true}
                  disabledTitleStyle={{ color: '#419FFF' }}
                  icon={<Badge
                    status={item.insresults == '合格' ? 'success' : (item.insresults == null ? null : 'error')}
                    containerStyle={{ marginHorizontal: 4 }}
                  />} /> 
              </View>*/}
            </View>
          </TouchableOpacity>
        </Card>
      )
    }
    //通用页面 估计很快就不通用了？？
    else if (item.compTypeName) {
      return (
        <Card containerStyle={styles.card_containerStyle}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('ChildTwoMain', {
                sonTitle: sonTitletest,
                item: item,
                sonUrl: Url.url + Url.Produce + item._rowguid + '/GetProjectProducedComponentsSingle/',
                url: url
              })
            }} >
            <View style={{ flexDirection: 'row' }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 11 }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, { color: '#419FFF', flex: 3 }]}>{item.compCode}</Text>
                  <View style={{ flex: 1.4 }}>
                    {
                      item.bigState ?
                        <View style={[styles.badgeView, { backgroundColor: item.bigState == '脱模待检' ? 'rgba(255, 122, 56, 0.22)' : (item.bigState == '出库' ? 'rgba(21, 214, 57, 0.22)' : 'rgba(98, 178, 250, 0.22)') }]}>
                          <Badge
                            badgeStyle={{ backgroundColor: item.bigState == '脱模待检' ? 'orange' : (item.bigState == '出库' ? '#15D639' : '#64B1FC') }}
                            containerStyle={{ marginRight: 8, top: 10 }}
                          />
                          <Text style={[styles.text, { color: item.bigState == '脱模待检' ? 'orange' : (item.bigState == '出库' ? '#15D639' : '#64B1FC'), fontWeight: 'bold' }]}>{item.bigState}</Text>
                        </View>
                        :
                        <View></View>
                    }
                    {
                      item.insresults ?
                        <View style={[styles.badgeView, { backgroundColor: item.insresults == '合格' ? 'rgba(98, 178, 250, 0.22)' : 'rgba(255, 122, 56, 0.22)' }]}>
                          <Badge
                            badgeStyle={{ backgroundColor: item.insresults == '合格' ? '#64B1FC' : 'orange' }}
                            containerStyle={{ marginRight: 8, top: 10 }}
                          />
                          <Text style={[styles.text, { color: item.insresults == '合格' ? '#64B1FC' : 'orange', fontWeight: 'bold' }]}>{item.insresults}</Text>
                        </View>
                        : <View></View>
                    }
                  </View>
                </View>
                <Text style={styles.text} >{item.projectName}</Text>
                <Text style={styles.text} >{item.compTypeName}</Text>
                <Text style={styles.text} >{item.designType}</Text>
                {

                  item.warehouRomName != null && item.warehouRomName != "" ?
                    <Text style={styles.text} >{item.warehouRomName + '(' + item.warehouLibName + ')'}</Text> :
                    <View></View>
                }
                <Text style={styles.text}>{item.floorNoName}({item.floorName})</Text>
                <Text style={item.planDate || item.produceDate ? [styles.text, { color: 'gray' }] : styles.hidden}>{item.finChkTime ? '成检日期：' : '计划生产日期：'}
                  <Text style={styles.text}>{item.planDate ? item.planDate : (item.finChkTime ? item.finChkTime : item.produceDate)}</Text>
                </Text>
              </View>
              {/* <View>
                 <Button
                  type='clear'
                  title={item.bigState}
                  titleStyle={{ fontSize: 14 }}
                  containerStyle={{ borderRadius: 60 }}
                  style={[item.bigState ? { flex: 1 } : styles.hidden, { borderRadius: 60, alignContent: 'flex-start' }]}
                  disabled={true}
                  disabledTitleStyle={{ color: '#419FFF' }}
                  icon={<Badge
                    //入库蓝色，脱模待检黄色，出库绿色，剩下黄色
                    status={item.bigState == '脱模待检' ? 'warning' : (item.bigState == '出库' ? 'success' : (item.bigState == '入库' ? 'primary' : (item.bigState == null ? null : 'warning')))}
                    containerStyle={{ marginHorizontal: 4 }}
                  />} />
                <Button
                  type='clear'
                  title={item.insresults}
                  titleStyle={{ fontSize: 15 }}
                  style={[item.insresults ? { flex: 1 } : styles.hidden, { borderRadius: 60, alignContent: 'flex-start' }]}
                  disabled={true}
                  disabledTitleStyle={{ color: '#419FFF' }}
                  icon={<Badge
                    status={item.insresults == '合格' ? 'success' : (item.insresults == null ? null : 'error')}
                    containerStyle={{ marginHorizontal: 4 }}
                  />} /> 
              </View>*/}
            </View>
          </TouchableOpacity>
        </Card>
      )
    }
  }
}

const styles = StyleSheet.create({
  loading: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  card_containerStyle: {
    borderRadius: 5,
    shadowOpacity: 0,
    borderWidth: 0,
    elevation: 0,
  },
  contrain: {
    flexDirection: 'column',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  SN_View: {
    flex: 1
  },
  SN_Text_View: {
    top: 4
  },
  SN_Text: {
    textAlignVertical: 'center',
    fontSize: 16,
    color: '#535c68'
  },
  SN_Text_Comp: {
    fontSize: 13,
  },
  text_View: {
    flexDirection: 'column',
    backgroundColor: '#FFFFFB',
    flex: 11,
  },
  txt: {

    fontSize: 16,
    marginBottom: 8,
    marginTop: 8,
    marginHorizontal: 8
  },
  text: {
    fontSize: 14,
    marginBottom: 4,
    marginTop: 4,
  },
  textname: {
    fontSize: 14,
    marginBottom: 4,
    marginTop: 4,
    color: 'gray'
  },
  badgeView: {
    //height: 30,
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    borderRadius: 20,
    flex: 1
  },
  textView: {

    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  hidden: {
    flex: 0,
    height: 0,
    width: 0,
  },
  sideMenu_Main: {
    flex: 1,
    marginLeft: 16,
    marginRight: 28, //新ui
    marginTop: 14
  },
  sideMenu_Top: {
    //marginLeft: 6,
    marginTop: 8
  },
  sideMenu_Bottom: {
    marginBottom: 20, //新UI样式
    //marginBottom: 6,
    //marginRight: 16
  },
  sideMenu_Title: {
    //flex: 1,
    //fontSize: 16,
    fontSize: 18//新UI样式 
  },
  sideMenu_TitleView: {
    flex: 1,
  },
  sideMenu_DownView: {
    flex: 1,
    //backgroundColor: 'blue',
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  sideMenu_DownView_View: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignContent: 'flex-end'
  },
  sideMenu_DownViewTitle: {
    fontSize: 17,
    color: 'gray',
    top: 2,
    marginLeft: 10,
    //backgroundColor: "blue"

  },
  sideMenu_DownViewIcon: {
    fontSize: 17,
    top: 5,
    marginLeft: 5
  },
  sideMenu_Content: {
    height: 40,
    marginLeft: width * 0.005,
    marginRight: width * 0.08,
    backgroundColor: "#f8f8f8",
    borderRadius: 5,
    marginTop: 10
  },
  sideMenu_Picker: {
    flex: 1,
    fontSize: 14
  },
  sideMenu_Top1: {
    flexDirection: 'row',
    //marginLeft: 6,
    //marginRight: 16,
    marginTop: 8,
    //backgroundColor : 'red'
  },
  bottomsheetScroll: {
    height: 290
  },
})

const cardstyles = StyleSheet.create({
  card_containerStyle: {
    borderRadius: 10,
    shadowOpacity: 0,
    backgroundColor: '#f8f8f8',
    borderWidth: 0,
    paddingVertical: 13,
    elevation: 0
  },
  title: {
    fontSize: 13,
    color: '#999'
  },
  rightTitle: {
    fontSize: 13,
    textAlign: 'right',
    lineHeight: 14,
    flexWrap: 'wrap',
    width: width / 2,
    color: '#333'
  },
  list_container_style: {
    marginVertical: 0,
    paddingVertical: 9,
    backgroundColor: 'transparent'
  },
})

if (deviceWidth <= 330) {
  cardstyles.card_containerStyle = {
    borderRadius: 10,
    shadowOpacity: 0,
    backgroundColor: '#f8f8f8',
    borderWidth: 0,
    paddingVertical: 10,
    paddingHorizontal: 5,
    elevation: 0
  }
  cardstyles.list_container_style = {
    marginVertical: 0,
    paddingVertical: 5,
    backgroundColor: 'transparent'
  }
  cardstyles.rightTitle = {
    fontSize: 13,
    textAlign: 'right',
    lineHeight: 16,
    width: width * 0.4,
    color: '#333'
  }
}