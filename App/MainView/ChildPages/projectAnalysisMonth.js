import React, { Component } from 'react';
import { View, Text, StyleSheet, processColor, Dimensions, PixelRatio, ScrollView, ActivityIndicator } from 'react-native';
import { Button } from 'react-native-elements';
import BarChart from '../ChildComponent/BarChart';
import Url from '../../Url/Url';
import { RFT, deviceWidth, deviceHeight } from '../../Url/Pixal';

export default class projectAnalysisMonth extends React.Component {
  constructor() {
    super();
    this.state = {
      projectAnalysisMonth: {}
    }
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  componentDidMount() {
    this.requestUrl()
  }

  requestUrl = () => {
    const urlTest = Url.url + Url.factory + Url.Fid + 'GetFacProjectAnalysisMonth'
    console.log("🚀 ~ file: projectAnalysisMonth.js:28 ~ projectAnalysisMonth ~ urlTest:", urlTest)
    this.requestData(urlTest);
  }

  requestData = (url) => {
    let projectNameList = [], planVolumeList = [], volumeList = [], Data = {};
    fetch(url).then(res => {
      return res.json();
    }).then(resData => {
      resData.map((item, index) => {
        if (item.projectAbb != null) {
          projectNameList.push(item.projectAbb);
        } else {
          projectNameList.push(item.projectName);
        }
        planVolumeList.push(item.planVolume);
        volumeList.push(item.volume);
      })

      Data.Name = projectNameList;
      Data.Vol1 = planVolumeList;
      Data.Vol2 = volumeList;

      this.refPlan.ReFlash(Data);

      this.setState({
        projectAnalysisMonth: Data
      })
      //console.log(Data)
    }).catch((error) => {
      console.log(error);
    });
  }


  render() {
    return (
      <ScrollView style={styles.view} overScrollMode = 'never'>
          <Text style={styles.text}>月计划与实际产量对比</Text>
          <View style={styles.chartview}>
            <View style={styles.chart}>
              <BarChart
                ref={refPlan => { this.refPlan = refPlan }}
              />
            </View>
          </View>
        </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  view: {
    flex: 1,
    backgroundColor: '#F9F9F9'
  },
  text: {
    color: '#535c68',
    fontSize: RFT * 3.5,
    paddingLeft: 15,
    marginTop: 10,
  },
  chartview: {
    width: deviceWidth * 0.94,
    height: deviceWidth * 1.2,
    marginTop: 10,
    marginLeft: deviceWidth * 0.03,
    backgroundColor: '#FFFFFF',
    borderRadius: 10
  },
  chart: {
    width: deviceWidth * 0.94,
    height: deviceWidth * 1.1,
  }
})
