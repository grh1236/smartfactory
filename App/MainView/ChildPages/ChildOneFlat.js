import React from 'react'
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Picker } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, ListItem, Overlay } from 'react-native-elements';
import Url from '../../Url/Url';
import SideMenu from 'react-native-side-menu';
import { ToDay, YesterDay, BeforeYesterDay } from '../../CommonComponents/Data';
import { RFT } from '../../Url/Pixal';
import SideSearchItem from '../../WorkSpace/ChildComponent/SideSearchItem';
import Toast from 'react-native-easy-toast';

const Urlnew = Url.url + Url.Produce;
let pageNo = 1;   //翻页设置,有问题
const { height, width } = Dimensions.get('window') //获取宽高

let isCheckList = {}; //是否展开状态

let searchList = {
  productLineNameList: [],
  projectIdList: [],
  projectNameList: [],
  designTypeList: [],
  compTypeNameList: [],
  floorNoNameList: [],
  floorNameList: [],
  prjFNNameList: [],
  FNFNameList: []
}

let projectList = []

//搜索栏搜索图标
const defaultSearchIcon = () => ({
  type: 'antdesign',
  size: 18,
  name: 'search1',
  color: 'white',
});

//搜索栏清理图标
const defaultClearIcon = () => ({
  type: 'antdesign',
  size: 18,
  name: 'close',
  color: 'white',
});

//搜索栏搜索图标
const designSearchIcon = () => ({
  type: 'antdesign',
  size: 17,
  name: 'search1',
  color: '#666',
});

//搜索栏清理图标
const designClearIcon = () => ({
  type: 'antdesign',
  size: 18,
  name: 'close',
  color: '#666',
});

export default class ChildOne extends React.Component {
  constructor() {
    super();
    this.state = {
      search: '', //搜索关键字保存
      resData: [], //接口数据保存
      factoryId: '',

      item: [], //好像没用
      isLoading: true,  //判断是否加载 加载页
      isRefreshing: false,  //是否刷新
      page: '1', //页码
      open: false, //
      selectedIndex: 0,  //按钮组选择
      selectedIndexTask: 0,//按钮组选择-任务单
      selectedIndexLoaout: 0,//按钮组选择-出库
      isCheckList: [],
      //筛选框
      searchList: {
        productLineNameList: [],
        projectIdList: [],
        projectNameList: [],
        designTypeList: [],
        compTypeNameList: [],
        floorNoNameList: [],
        floorNameList: [],
        prjFNNameList: [],
        FNFNameList: []
      },
      taskId: '',
      bigState: '',
      //生产线筛选
      isproductLinePickerVisible: false,
      resDataProductLine: [],
      productLinePicker: '请选择',
      productLineId: '',
      productLineName: '',
      productLineList: [],
      //构件类型筛选
      isCompTypePickerVisible: false,
      resDataCompType: [],
      compTypePicker: '请选择',
      compTypeId: '',
      compTypeName: '',
      compTypeList: [],
      //设计型号筛选
      isDesignTypePickerVisible: false,
      resDataDesignType: [],
      designTypePicker: '请选择',
      designTypeId: '',
      designTypeName: '',
      designTypeList: [],
      //项目筛选
      isProjectPickerVisible: false,
      resDataProject: [],
      projectPicker: '请选择',
      projectId: '',
      projectName: '',
      projectList: [],
      //楼号筛选
      isFloorNOPickerVisible: false,
      resDataFloorNO: [],
      FloorNOPicker: '请选择',
      FloorNOId: '',
      FloorNOName: '',
      FloorNOList: [],
      //楼层筛选
      isFloorsPickerVisible: false,
      resDataFloors: [],
      FloorsPicker: '请选择',
      FloorsId: '',
      FloorsName: '',
      //侧边栏
      isSideOpen: false,
      pageLoading: true,
      urlnew: '',
    }
    //函数绑定
    this.requestData = this.requestData.bind(this);
    this.refreshing = this.refreshing.bind(this);
    this._onload = this._onload.bind(this);
    this._footer = this._footer.bind(this);
  }

  //长列表头部
  _header = () => {
    return <Text style={[styles.txt, { backgroundColor: 'black' }]}>这是头部</Text>;
  }

  //长列表尾部
  _footer = () => {
    return (
      /* <View style={{ flexDirection: 'row', justifyContent: 'center', alignContent: 'center' }}>
        <Text style={[styles.txt, { textAlign: 'right', flex: 1 }]}>第</Text>
        <Input
          containerStyle={{ width: width / 10 }}
          placeholder=' '
          onChangeText={text => this.setState({ page: text })}
          onChange={text => {
            this.setState({ page: text })
            this.requestData();
          }} >
        </Input>
        <Text style={[styles.txt, { flex: 1 }]}> 页</Text>
      </View> */
      <Text style={[{ fontSize: 18, textAlign: 'center', marginHorizontal: 30 }]}>第 {this.state.page} 页</Text>
    );
  }

  //长列表分隔栏
  _separator = () => {
    return <View style={{ height: 2, backgroundColor: '#fffffc' }} />;
  }

  //刷新列表
  refreshing() {
    this.requestData();
    console.log('page: ')
    console.log(this.state.page)
  }

  //上拉加载下一页
  _onload() {
    if (this.state.resData.length === 40) {
      this.setState({
        isLoading: true,
      })
      this.state.page++;
      this.requestData();
      console.log('page: ')
      console.log(this.state.page)
    }

  }

  //执行函数
  componentDidMount() {
    this.requestData();
  }

  componentWillUnmount() {
    searchList = {
      productLineNameList: [],
      projectIdList: [],
      projectNameList: [],
      designTypeList: [],
      compTypeNameList: [],
      floorNoNameList: [],
      floorNameList: [],
      prjFNNameList: [],
      FNFNameList: []
    }
  }

  //返回接口数据
  requestData = (urlprops) => {
    const { navigation } = this.props;
    const url = navigation.getParam('url'); //从主界面获取链接
    console.log(url)
    if (url.indexOf('GetStockLoaoutWare') != -1) {
      fetch(urlprops ? urlprops : url).then(res => {
        return res.json()
      }).then(resData => {
        console.log("🚀 ~ file: ChildOneFlat.js ~ line 150 ~ ChildOne ~ fetch ~ resData", resData)
        this.setState({
          resData: resData,
          isLoading: false,
          pageLoading: false,
        });
        //console.log(this.state.resData);
      }).catch((error) => {
        console.log(error);
      });
    }
    //需要翻页的链接返回
    else if (url.indexOf('/40/') != -1) {
      //接口链接需要加上页数，页数上拉会加载下一页
      fetch(url + this.state.page).then(res => {
        return res.json()
      }).then(resData => {
        this.setState({
          resData: resData,
          isLoading: false,
          pageLoading: false,
        });
        //console.log(this.state.resData);
      }).catch((error) => {
        console.log(error);
      });
    } //需要按日期记录的页面数据，日期用按钮控制，从render函数传过来
    else if (url.search('DailyRecord') != -1) {
      //点按钮时传当日数据，否则传今天的
      fetch(urlprops ? urlprops : url).then(res => {
        return res.json()
      }).then(resData => {
        this.setState({
          resData: resData,
          isLoading: false,
          pageLoading: false,
        });
        console.log(this.state.resData);
      }).catch((error) => {
        console.log(error);
      });
    } //通用数据接口
    else {
      fetch(urlprops ? urlprops : url).then(res => {
        return res.json()
      }).then(resData => {
        console.log("🚀 ~ file: ChildOneFlat.js ~ line 150 ~ ChildOne ~ fetch ~ resData", resData)
        if (resData.length > 0) {
          this.setState({
            factoryId: resData[0].factoryId,
            projectId: resData[0].projectId,
            bigState: resData[0].bigState
          })
          console.log("🚀 ~ file: ChildOneFlat.js ~ line 253 ~ ChildOne ~ fetch ~ bigState", this.state.bigState)
          this.seachListFunc(resData)
        }
        this.setState({
          resData: resData,
          isLoading: false,
          pageLoading: false,
        });
        //console.log(this.state.resData);
      }).catch((error) => {
        console.log(error);
      });
    }

  }

  seachListFunc = (resData) => {
    resData.map((item, index) => {
      if (searchList.compTypeNameList.indexOf(item.compTypeName) == -1) {
        searchList.compTypeNameList.push(item.compTypeName)
      }
      if (searchList.designTypeList.indexOf(item.designType) == -1) {
        searchList.designTypeList.push(item.designType)
      }
      if (searchList.floorNoNameList.indexOf(item.projectId + '+' + item.floorNoName) == -1) {
        searchList.floorNoNameList.push(item.projectId + '+' + item.floorNoName)
      }
      if (searchList.floorNameList.indexOf(item.projectId + '+' + item.floorNoName + '+' + item.floorName) == -1) {
        searchList.floorNameList.push(item.projectId + '+' + item.floorNoName + '+' + item.floorName)
      }
    })
    searchList.floorNoNameList.sort()
    searchList.floorNameList.sort()
    searchList.prjFNNameList = []
    searchList.floorNoNameList.map((item) => {
      let itemarr = item.split("+")
      console.log("🚀 ~ file: ChildOneFlat.js ~ line 997 ~ ChildOne ~ {searchList.floorNoNameList.map ~ itemarr", itemarr)
      let prjId = itemarr[0]
      let name = itemarr[1]
      if (prjId == this.state.projectId) {
        if (searchList.prjFNNameList.indexOf(name) == -1) {
          searchList.prjFNNameList.push(name)
        }
      }
    })
    this.setState({ searchList: searchList })
    console.log("🚀 ~ file: ChildOneFlat.js ~ line 247 ~ ChildOne ~ resData.map ~ searchList", searchList)

  }

  SearchPostMain = (designType, compTypeName, floorNoName, floorName, searchText) => {
    const { factoryId, projectId, bigState } = this.state
    console.log("🚀 ~ file: ChildOneFlat.js ~ line 290 ~ ChildOne ~ bigState", bigState)
    const url = Url.url + 'Produce/' + 'ViewProduceComponentStateQuery';
    //const url = 'http://localhost:5000/api/Produce/ViewProduceTaskCompStatelist';
    this.setState({ pageLoading: true })
    let data = {
      "factoryId": factoryId,
      "projectId": projectId,
      "bigState": bigState,
      "compCode": searchText,
      "designType": designType,
      "compTypeName": compTypeName,
      "floorNoName": floorNoName || "",
      "floorName": floorName || ""
    }
    console.log("🚀 ~ file: ChildOneFlat.js ~ line 290 ~ ChildOne ~ data", data)
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    }).then(res => {
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: ChildOneFlat.js ~ line 300 ~ ChildOne ~ resData", resData)
      this.setState({
        resData: resData,
        isLoading: false,
        pageLoading: false,
      })
    })
  }


  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  //搜索栏搜索功能
  TextChange = (text) => {
    const { compTypeName, designTypeName, FloorNOName, FloorsName } = this.state
    let i = 0;
    let searchData = [];
    const { navigation } = this.props;
    const url = navigation.getParam('url');
    console.log(url);
    this.setState({ search: text });
    this.SearchPostMain(designTypeName, compTypeName, FloorNOName, FloorsName, text)
    if (text == '') {
      this.setState({ pageLoading: !this.state.pageLoading })
      this.requestData();
    }
  }

  TextChangeDesignType = (text) => {
    const { compTypeName, designTypeName, FloorNOName, FloorsName, search } = this.state
    this.setState({ designTypeName: text });
    this.SearchPostMain(text, compTypeName, FloorNOName, FloorsName, search)
  }


  //日期按钮组顶栏
  ButtonGroupday = () => {
    const buttons = ['今日', '昨日', '前日']
    const { selectedIndex } = this.state
    return (
      <View style={{ flex: 1.3, flexDirection: 'row', backgroundColor: 'white' }}>
        <ButtonGroup
          onPress={this.updateIndex.bind(this)}
          selectedIndex={selectedIndex}
          buttons={buttons}
          buttonStyle={{ borderWidth: 0, }}
          containerStyle={{ borderWidth: 0, flex: 1 }}
          selectedButtonStyle={{ backgroundColor: '#419FFF', }}
        />
        <Button
          title='筛选'
          type='clear'
          style={{ flex: 1, justifyContent: 'center', alignContent: 'center', }}
          titleStyle={{ color: 'gray' }}
          iconRight={true}
          icon={<Icon
            name='filter'
            type='antdesign'
            color='#419FFF' />} />
      </View>
    )
  }

  //任务单按钮顶栏
  TaskStateButton = () => {
    const buttons = ['全部', '已提交', '未提交']
    const { selectedIndexTask } = this.state
    return (
      <View style={{ flex: 1.3, flexDirection: 'row', backgroundColor: 'white' }}>
        <ButtonGroup
          onPress={this.updateIndexTask.bind(this)}
          selectedIndex={selectedIndexTask}
          buttons={buttons}
          buttonStyle={{ borderWidth: 0, }}
          containerStyle={{ borderWidth: 0, flex: 1 }}
          selectedButtonStyle={{ backgroundColor: '#419FFF', }}
        />
        <Button
          title='筛选'
          type='clear'
          style={{ flex: 1, justifyContent: 'center', alignContent: 'center', }}
          titleStyle={{ color: 'gray' }}
          iconRight={true}
          icon={<Icon
            name='filter'
            type='antdesign'
            color='#419FFF' />} />
      </View>
    )
  }

  //任务单按钮顶栏
  TaskStateButton = () => {
    const buttons = ['全部', '已提交', '未提交']
    const { selectedIndexTask } = this.state
    return (
      <View style={{ flex: 1.3, flexDirection: 'row', backgroundColor: 'white' }}>
        <ButtonGroup
          onPress={this.updateIndexTask.bind(this)}
          selectedIndex={selectedIndexTask}
          buttons={buttons}
          buttonStyle={{ borderWidth: 0, }}
          containerStyle={{ borderWidth: 0, flex: 1 }}
          selectedButtonStyle={{ backgroundColor: '#419FFF', }}
        />
        <Button
          title='筛选'
          type='clear'
          style={{ flex: 1, justifyContent: 'center', alignContent: 'center', }}
          titleStyle={{ color: 'gray' }}
          iconRight={true}
          icon={<Icon
            name='filter'
            type='antdesign'
            color='#419FFF' />} />
      </View>
    )
  }

  //出库按钮顶栏
  LoaoutStateButton = () => {
    const buttons = ['全部', '已发货', '未发货']
    const { selectedIndexLoaout } = this.state
    return (
      <View style={{ flex: 1.3, flexDirection: 'row', backgroundColor: 'white' }}>
        <ButtonGroup
          onPress={this.updateIndexLoaout.bind(this)}
          selectedIndex={selectedIndexLoaout}
          buttons={buttons}
          buttonStyle={{ borderWidth: 0, }}
          containerStyle={{ borderWidth: 0, flex: 1 }}
          selectedButtonStyle={{ backgroundColor: '#419FFF', }}
        />
        <Button
          title='筛选'
          type='clear'
          style={{ flex: 1, justifyContent: 'center', alignContent: 'center', }}
          titleStyle={{ color: 'gray' }}
          iconRight={true}
          icon={<Icon
            name='filter'
            type='antdesign'
            color='#419FFF' />} />
      </View>
    )
  }

  //顶部记录与筛选按钮
  Top = () => {
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const url = navigation.getParam('url')
    const title = navigation.getParam('title')
    let length = 0;
    this.state.resData.map((item, index) => {
      if (item.compNum !== 0) {
        length++
      }
    })
    return (
      <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end', backgroundColor: 'white' }}>
        <Button
          type='clear'
          title={length + '条记录'}
          containerStyle={{ top: 1 }}
          titleStyle={{ color: 'gray' }}
          style={{ width: width / 3, flex: 1 }}
        />
        {(url.indexOf('Daily') != -1 || url.indexOf('GetProduceTask') != -1 || (url.indexOf('GetStockLoaoutWare') != -1 && title.indexOf('成品出库') != -1)) ?
          <View></View> :
          <Button
            title='筛选'
            type='clear'
            style={{ width: width / 3, flex: 1 }}
            titleStyle={{ color: 'gray' }}
            iconRight={true}
            onPress={() => {
              this.setState({
                isSideOpen: true
              })
            }}

            icon={< Icon
              name='filter'
              type='antdesign'
              color='#419FFF' />}
          />
        }

      </View>
    )
  }

  //按钮组触发函数
  updateIndex(selectedIndex) {
    const { navigation } = this.props;
    const url = navigation.getParam('url')
    //console.log(url.substring(0, url.length - 10));
    this.setState({
      selectedIndex: selectedIndex,
      pageLoading: true
    })
    if (selectedIndex == 0) {
      const urlnew = url.substring(0, url.length - 10) + ToDay
      console.log(urlnew)
      this.requestData(urlnew);
    } else if (selectedIndex == 1) {
      const urlnew = url.substring(0, url.length - 10) + YesterDay
      this.requestData(urlnew);
      console.log(urlnew)
    } else if (selectedIndex == 2) {
      const urlnew = url.substring(0, url.length - 10) + BeforeYesterDay
      console.log(urlnew)
      this.requestData(urlnew);
    }
  }

  //按钮组触发函数-任务单
  updateIndexTask(selectedIndex) {
    const { navigation } = this.props;
    const { resData } = this.state;
    let stateData = [];
    const url = navigation.getParam('url')
    //console.log(url.substring(0, url.length - 10));
    this.setState({
      selectedIndexTask: selectedIndex,
      pageLoading: true,
    })
    if (selectedIndex == 0) {
      const urlnew = url.substring(0, url.length - 1) + '0'
      console.log("🚀 ~ file: ChildOneFlat.js ~ line 331 ~ ChildOne ~ updateIndex ~ urlnew", urlnew)
      this.requestData(urlnew);
    } else if (selectedIndex == 1) {
      const urlnew = url.substring(0, url.length - 1) + '1'
      console.log("🚀 ~ file: ChildOneFlat.js ~ line 331 ~ ChildOne ~ updateIndex ~ urlnew", urlnew)
      this.requestData(urlnew);
    } else if (selectedIndex == 2) {
      const urlnew = url.substring(0, url.length - 1) + '2'
      console.log("🚀 ~ file: ChildOneFlat.js ~ line 331 ~ ChildOne ~ updateIndex ~ urlnew", urlnew)
      this.requestData(urlnew);
    }
  }

  //按钮组触发函数-出库
  updateIndexLoaout(selectedIndex) {
    const { navigation } = this.props;
    const { resData } = this.state;
    let stateData = [];
    const url = navigation.getParam('url')
    //console.log(url.substring(0, url.length - 10));
    this.setState({
      selectedIndexLoaout: selectedIndex,
      pageLoading: true,
    })
    if (selectedIndex == 0) {
      const urlnew = url.substring(0, url.length - 1) + '0'
      console.log("🚀 ~ file: ChildOneFlat.js ~ line 331 ~ ChildOne ~ updateIndex ~ urlnew", urlnew)
      this.requestData(urlnew);
    } else if (selectedIndex == 1) {
      const urlnew = url.substring(0, url.length - 1) + '1'
      console.log("🚀 ~ file: ChildOneFlat.js ~ line 331 ~ ChildOne ~ updateIndex ~ urlnew", urlnew)
      this.requestData(urlnew);
    } else if (selectedIndex == 2) {
      const urlnew = url.substring(0, url.length - 1) + '2'
      console.log("🚀 ~ file: ChildOneFlat.js ~ line 331 ~ ChildOne ~ updateIndex ~ urlnew", urlnew)
      this.requestData(urlnew);
    }
  }

  menu = () => {
    const {
      isDesignTypePickerVisible, designTypeId, designTypeName, designTypePicker,
      isProjectPickerVisible, projectPicker, pickerValue, projectId, projectName,
      isFloorNOPickerVisible, resDataFloorNO, FloorNOPicker, FloorNOId, FloorNOName,
      isFloorsPickerVisible, resDataFloors, FloorsId, FloorsName, FloorsPicker,
      isCompTypePickerVisible, resDataCompType, compTypeId, compTypeName, compTypePicker,
      searchList, pageLoading, search } = this.state
    let menu = <View></View>
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const url = navigation.getParam('url')

    menu =
      <View style={styles.sideMenu_Main} >
        <Toast ref={(ref) => { this.toast = ref; }} position="center" />
        {/* 构件类型 */}
        <View style={styles.sideMenu_Bottom}>
          <View style={styles.sideMenu_Top} >
            <Text style={styles.sideMenu_Title}>{"构件类型筛选"}</Text>
          </View>
          <View style={styles.sideMenu_Content}>
            <Picker
              mode='dropdown'
              selectedValue={compTypePicker}
              style={styles.sideMenu_Picker}
              enabled={!pageLoading}
              onValueChange={(value) => {
                let compTypeId = ''
                if (value != '请选择') {
                  this.SearchPostMain(designTypeName, value, FloorNOName, FloorsName, search)
                  this.setState({
                    compTypePicker: value,
                    compTypeId: compTypeId,
                    compTypeName: value
                  })
                } else {
                  this.setState({
                    compTypePicker: value,
                    compTypeId: '',
                    compTypeName: ''
                  })
                  this.SearchPostMain(designTypeName, "", FloorNOName, FloorsName, search)

                }

              }}
            >
              <Picker.Item label={'请选择构件类型'} value={'请选择'} />
              {searchList.compTypeNameList.map((item) => {
                return (
                  <Picker.Item label={item} value={item} />
                )
              })}
            </Picker>
          </View>
        </View>
        {/* 设计型号 */}
        <View style={styles.sideMenu_Bottom}>
          <View style={styles.sideMenu_Top} >
            <Text style={styles.sideMenu_Title}>{"设计型号筛选"}</Text>
          </View>
          <View style={styles.sideMenu_Content}>
            <SearchBar
              platform='android'
              placeholder={'请输入设计型号'}
              placeholderTextColor='#666'
              
              containerStyle={{ backgroundColor: 'transparent' }}
              inputContainerStyle={{ backgroundColor: 'transparent', height: 25 }}
              inputStyle={{ color: '#666', fontSize: RFT * 3.6 }}
              searchIcon={designSearchIcon()}
              //showLoading={pageLoading}
              clearIcon={designClearIcon()}
              showCancel={false}
              cancelButtonProps={{ buttonStyle: { display: 'none' } }}
              disabled={pageLoading}
              //cancelButtonTitle=''
              //cancelButtonProps={{ buttonTextStyle: { color: "#666", fontSize: RFT * 3.8 } }}
              value={designTypeName}
              onSubmitEditing={(value) => {
                this.SearchPostMain(designTypeName, compTypeName, FloorNOName, FloorsName, search)
                //console.log("🚀 ~ file: ChildOneFlat.js ~ line 651 ~ ChildOne ~ render ~ value", value)
              }}
              onChangeText={(text) => {
                this.setState({ designTypeName: text })
              }}
              onClear={() => {
                //console.log("🚀 ~ file: ChildOneFlat.js ~ line 656 ~ ChildOne ~ render ~ designTypeName", designTypeName)
                this.setState({ designTypeName: '' })
                this.SearchPostMain("", compTypeName, FloorNOName, FloorsName, search)
              }}
              //onChangeText={this.TextChangeDesignType}
              //input={(input) => { this.setState({ designTypeName: input }) }}
            />
          </View>
        </View>
        {/* 楼号 */}
        <View style={styles.sideMenu_Bottom}>
          <View style={styles.sideMenu_Top} >
            <Text style={styles.sideMenu_Title}>{"楼号筛选"}</Text>
          </View>
          <View style={styles.sideMenu_Content}>
            <Picker
              mode='dropdown'
              selectedValue={FloorNOPicker}
              style={styles.sideMenu_Picker}
              enabled={!pageLoading}
              onValueChange={(value) => {
                let name = value
                console.log("🚀 ~ file: ChildOneFlat.js ~ line 970 ~ ChildOne ~ value", value)
                if (value != '请选择') {
                  this.setState({
                    FloorNOPicker: name,
                    FloorNOName: name,
                    FloorsName: '',
                    FloorsPicker: '',
                  })
                  searchList.FNFNameList = []
                  searchList.floorNameList.map((item) => {
                    let itemarr = item.split("+")
                    let prjId = itemarr[0]
                    let fnnName = itemarr[1]
                    let name = itemarr[2]
                    if (prjId == projectId) {
                      if (fnnName == value) {
                        if (searchList.FNFNameList.indexOf(name) == -1) {
                          searchList.FNFNameList.push(name)
                        }
                      }
                    }
                  })
                  this.SearchPostMain(designTypeName, compTypeName, value, "", search)
                } else {
                  this.setState({
                    FloorNOPicker: value,
                    FloorNOId: '',
                    FloorNOName: '',
                    FloorsName: '',
                    FloorsPicker: '',
                  })
                  this.SearchPostMain(designTypeName, compTypeName, "", "", search)

                }
              }}
            >
              <Picker.Item label={'请选择楼号'} value={'请选择'} />
              {searchList.prjFNNameList.map((item) => {
                return (
                  <Picker.Item label={item} value={item} />
                )
              })}
            </Picker>
          </View>
        </View>
        {/* 楼层 */}
        <View style={styles.sideMenu_Bottom}>
          <View style={styles.sideMenu_Top} >
            <Text style={styles.sideMenu_Title}>{"楼层筛选"}</Text>
          </View>
          <View style={styles.sideMenu_Content}>
            <Picker
              mode='dialog'
              selectedValue={FloorsPicker}
              style={styles.sideMenu_Picker}
              enabled={!pageLoading}
              onValueChange={(value) => {
                if (FloorNOPicker == '请选择') {
                  this.toast.show('请先选择楼号')
                  return
                }
                if (searchList.floorNameList.length == 0) {
                  this.toast.show('无楼层')
                  return
                }
                let floorId = ''
                if (value != '请选择') {
                  //floorId = resDataFloors[position - 1].rowguid
                  this.SearchPostMain(designTypeName, compTypeName, FloorNOName, value, search)
                  this.setState({
                    FloorsPicker: value,
                    FloorsId: floorId,
                    FloorsName: value
                  })
                } else {
                  this.setState({
                    FloorsId: '',
                    FloorsName: ''
                  })
                  this.SearchPostMain(designTypeName, compTypeName, FloorNOName, "", search)

                }
              }}
            >
              <Picker.Item label={'请选择楼层'} value={'请选择'} />
              {searchList.FNFNameList.map((item) => {
                return (
                  <Picker.Item label={item} value={item} />
                )
              })}
            </Picker>
          </View>
        </View>
      </View>

    return menu;
  }

  //渲染页面函数
  render() {
    const {
      isDesignTypePickerVisible, designTypeId, designTypeName, designTypePicker, resDataProductLine,
      isProjectPickerVisible, projectPicker, pickerValue, projectId, projectName,
      isFloorNOPickerVisible, resDataFloorNO, FloorNOPicker, FloorNOId, FloorNOName,
      isFloorsPickerVisible, resDataFloors, FloorsId, FloorsName, FloorsPicker,
      isCompTypePickerVisible, resDataCompType, compTypeId, compTypeName, compTypePicker,
      search, pageLoading } = this.state
    const menu = this.menu();
    console.log(ToDay, YesterDay, BeforeYesterDay)
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const url = navigation.getParam('url')
    const title = navigation.getParam('title')
    console.log("🚀 ~ file: ChildOneFlat.js ~ line 295 ~ ChildOne ~ render ~ url", url)
    //从主页面接收孙子页面标题
    var sonTitletest = navigation.getParam('sonTitle');
    // 是否显示加载页，由接口返回数据判断
    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (
        <SideMenu menu={menu} isOpen={this.state.isSideOpen} menuPosition={'right'}
          onChange={(isOpen) => {
            this.setState({ isSideOpen: isOpen })
          }}>
          <View style={{ flexDirection: 'column', flex: 1, backgroundColor: '#f9f9f9' }}>
            {/* 搜索栏 */}
            <View style={{ backgroundColor: 'white' }}>
              <SearchBar
                platform='android'
                placeholder={'按编号进行搜索...'}
                placeholderTextColor='white'
                // style={{ backgroundColor: 'red', borderColor: 'red', borderWidth: 10 }}
                containerStyle={{ backgroundColor: '#419FFF', height: 53 }}
                inputContainerStyle={{ backgroundColor: '#419FFF', height: 43 }}
                inputStyle={{ color: '#FFFFFF' }}
                searchIcon={defaultSearchIcon()}
                clearIcon={defaultClearIcon()}
                cancelIcon={defaultClearIcon()}
                cancelButtonTitle={"取消"}
                cancelButtonProps={{ buttonTextStyle: { color: "#fff" } }}
                round={true}
                value={this.state.search}
                onSubmitEditing={(value) => {
                  this.SearchPostMain(designTypeName, compTypeName, FloorNOName, FloorsName, search)
                }}
                onChangeText={(text) => {
                  this.setState({ search: text })
                }}
                onClear={() => {
                  console.log("🚀 ~ file: ChildOneFlat.js ~ line 656 ~ ChildOne ~ render ~ designTypeName", designTypeName)
                  this.setState({ search: '' })
                  this.SearchPostMain(designTypeName, compTypeName, FloorNOName, FloorsName, "")
                }}
                //input={(input) => { this.setState({ search: input }) }}
              />
            </View>
            {/* 是否显示日期选择按钮组 */}
            {url.indexOf('Daily') != -1 ? this.ButtonGroupday() : <View></View>}
            {(url.indexOf('GetProduceTask') != -1 && url.indexOf('GetProduceTaskCompDetails') == -1) ? this.TaskStateButton() : <View></View>}
            {((url.indexOf('GetStockLoaoutWare') != -1 && title.indexOf('成品出库') != -1) && url.indexOf('GetStockLoaoutWareCompDetails') == -1) ? this.LoaoutStateButton() : <View></View>}

            {/* 是否显示筛选顶栏 */}
            {this.Top()}

            <View style={{ flex: 14.5 }}>
              {/* 长列表 */}
              {
                this.state.pageLoading ?
                  <View style={styles.loading}>
                    <ActivityIndicator
                      animating={true}
                      color='#419FFF'
                      size="large" />
                  </View> :
                  <FlatList
                    style={{ flex: 1, borderRadius: 10 }}
                    ref={(flatList) => this._flatList = flatList}
                    ItemSeparatorComponent={this._separator}
                    renderItem={this._renderItem}
                    onRefresh={this.refreshing}
                    refreshing={false}
                    //控制上拉加载，两个平台需要区分开
                    onEndReachedThreshold={(Platform.OS === 'ios') ? -0.3 : 0.0001}
                    onEndReached={this._onload}
                    numColumns={1}
                    ListFooterComponent={this._footer}
                    //columnWrapperStyle={{borderWidth:2,borderColor:'black',paddingLeft:20}}
                    //horizontal={true}
                    data={this.state.resData}
                  />
              }

            </View>


            {/* 构件类型 */}
            <Overlay
              onBackdropPress={() => {
                alert
                this.setState({ isCompTypePickerVisible: !this.state.isCompTypePickerVisible })
              }}
              overlayStyle={{ width: width, height: 200, position: 'relative' }}
              animationType='fade'
              isVisible={isCompTypePickerVisible}
              onRequestClose={() => {
                this.setState({ isCompTypePickerVisible: !this.state.isCompTypePickerVisible });
              }}>
              <Picker
                mode='dialog'
                selectedValue={compTypePicker}
                style={{ flex: 1 }}
                onValueChange={(value, position) => {
                  let compTypeId = ''
                  if (value != '请选择') {
                    //compTypeId = resDataCompType[position - 1].rowguid || ''
                    this.SearchPostMain(designTypeName, value, FloorNOName, FloorsName, search)
                    this.setState({
                      compTypePicker: value,
                      compTypeId: compTypeId,
                      compTypeName: value
                    })
                  } else {
                    this.setState({
                      compTypePicker: value,
                      compTypeId: '',
                      compTypeName: ''
                    })
                    this.SearchPostMain(designTypeName, "", FloorNOName, FloorsName, search)
                  }

                }}
              >
                <Picker.Item label={'请选择构件类型'} value={'请选择'} />
                {searchList.compTypeNameList.map((item) => {
                  return (
                    <Picker.Item label={item} value={item} />
                  )
                })}
              </Picker>
            </Overlay>

            {/* 设计型号 */}
            <Overlay
              onBackdropPress={() => {
                alert
                this.setState({ isDesignTypePickerVisible: !this.state.isDesignTypePickerVisible })
              }}
              overlayStyle={{ width: width, height: 200, position: 'relative' }}
              animationType='fade'
              isVisible={isDesignTypePickerVisible}
              onRequestClose={() => {
                this.setState({ isDesignTypePickerVisible: !this.state.isDesignTypePickerVisible });
              }}>
              <Picker
                mode='dialog'
                selectedValue={designTypePicker}
                style={{ flex: 1 }}
                onValueChange={(value, position) => {
                  let designTypeId = ''
                  if (value != '请选择') {
                    //productLineId = resDataProductLine[position - 1].rowguid || ''
                    this.SearchPostMain(value, compTypeName, FloorNOName, FloorsName, search)
                    this.setState({
                      designTypePicker: value,
                      designTypeId: designTypeId,
                      designTypeName: value
                    })
                  } else {
                    this.setState({
                      designTypePicker: value,
                      designTypeId: "",
                      designTypeName: ""
                    })
                    this.SearchPostMain("", compTypeName, FloorNOName, FloorsName, search)
                  }
                }}
              >
                <Picker.Item label={'请选择构件类型'} value={'请选择'} />
                {searchList.designTypeList.map((item) => {
                  return (
                    <Picker.Item label={item} value={item} />
                  )
                })}
              </Picker>
            </Overlay>

            {/* 楼号 */}
            <Overlay
              onBackdropPress={() => {
                alert
                this.setState({ isFloorNOPickerVisible: !isFloorNOPickerVisible })
              }}
              overlayStyle={{ width: width, height: 200, position: 'relative' }}
              animationType='fade'
              isVisible={isFloorNOPickerVisible}
              onRequestClose={() => {
                this.setState({ isFloorNOPickerVisible: !isFloorNOPickerVisible });
              }}>
              <Picker
                mode='dialog'
                selectedValue={FloorNOPicker}
                style={{ flex: 1 }}
                onValueChange={(value, position) => {
                  let floorNoId = ''
                  if (value != '请选择') {
                    //floorNoId = resDataFloorNO[position - 1].rowguid || ''
                    this.SearchPostMain(designTypeName, compTypeName, value, FloorsName, search)
                    //this.requestDataFloor(projectId, floorNoId)
                    this.setState({
                      FloorNOPicker: value,
                      FloorNOId: floorNoId,
                      FloorNOName: value
                    })
                  } else {
                    this.setState({
                      FloorNOPicker: value,
                      FloorNOId: '',
                      FloorNOName: ''
                    })
                    this.SearchPostMain(designTypeName, compTypeName, "", FloorsName, search)

                  }

                }}
              >
                <Picker.Item label={'请选择楼号'} value={'请选择'} />
                {searchList.floorNoNameList.map((item) => {
                  let itemarr = item.split("+")
                  if (itemarr[0] == projectId) {
                    return (
                      <Picker.Item label={itemarr[1]} value={itemarr[1]} />
                    )
                  }
                })}
              </Picker>
            </Overlay>

            {/* 楼层 */}
            <Overlay
              onBackdropPress={() => {
                alert
                this.setState({ isFloorsPickerVisible: !isFloorsPickerVisible })
              }}
              overlayStyle={{ width: width, height: 200, position: 'relative' }}
              animationType='fade'
              isVisible={isFloorsPickerVisible}
              onRequestClose={() => {
                this.setState({ isFloorsPickerVisible: !isFloorsPickerVisible });
              }}>
              <Picker
                mode='dialog'
                selectedValue={FloorsPicker}
                style={{ flex: 1 }}
                onValueChange={(value, position) => {
                  let floorId = ''
                  if (value != '请选择') {
                    //floorId = resDataFloors[position - 1].rowguid
                    this.SearchPostMain(designTypeName, compTypeName, FloorNOName, value, search)
                    this.setState({
                      FloorsPicker: value,
                      FloorsId: floorId,
                      FloorsName: value
                    })
                  } else {
                    this.setState({
                      FloorsId: '',
                      FloorsName: ''
                    })
                    this.SearchPostMain(designTypeName, compTypeName, FloorNOName, "", search)

                  }
                }}
              >
                <Picker.Item label={'请选择楼层'} value={'请选择'} />
                {searchList.floorNameList.map((item) => {
                  let itemarr = item.split("+")
                  if (itemarr[0] == projectId) {
                    if (itemarr[1] == FloorNOName) {
                      return (
                        <Picker.Item label={itemarr[2]} value={itemarr[2]} />
                      )
                    }
                  }
                })}
              </Picker>
            </Overlay>

          </View>
        </SideMenu>

      )
    }

  }

  //接口返回的对象数组，长列表对数据遍历
  _renderItem = ({ item, index }) => {
    //console.log(item);
    const { navigation } = this.props;
    const url = navigation.getParam('url')
    //console.log("🚀 ~ file: ChildOneFlat.js ~ line 499 ~ ChildOne ~ url.indexOf('GetProduceTask')  != -1", url.indexOf("GetProduceTask") != -1)
    //console.log("🚀 ~ file: ChildOneFlat.js ~ line 376 ~ ChildOne ~ url", url)
    let sonUrl = navigation.getParam('sonUrl');
    let sonTitletest = item.bigState ? item.bigState : navigation.getParam('sonTitle');
    //待产池项目, 日生产记录项目, 隐检项目页面（通用项目页面）
    if ((item.compNum || item.compNumPass) && !item.projectState && item.projectId) {
      return (
        <Card containerStyle={styles.card_containerStyle}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.push('ChildOne', {
                title: '构件列表',
                sonTitle: sonTitletest,
                url:
                  url.indexOf('Pool') != -1 ?
                    Url.url + Url.Produce + Url.Fid + '/GetWaitingPoolComp/' + item.projectId :
                    (url.indexOf('DailyRecordProject') != -1 ?
                      Url.url + Url.Produce + Url.Fid + item.projectId + '/GetProduceDailyRecord/' + ToDay :
                      (url.indexOf('Hidcheck') != -1 ?
                        Url.url + Url.Quality + Url.Fid + '/GetQualityHidcheck/' + item.projectId + '/40/' :
                        (url.indexOf('ReFinProducts') != -1 ?
                          Url.url + Url.Quality + Url.Fid + '/GetQualityReFinProducts/' + item.projectId :
                          (url.indexOf('Scrap') != -1 ?
                            Url.url + Url.Quality + Url.Fid + '/GetQualityScrap/' + item.projectId :
                            //这个是成品检查
                            Url.url + Url.Quality + Url.Fid + '/GetQualityFinProducts/' + item.projectId + '/40/')))
                    ),
              })
            }}>
            <View style={{ flexDirection: 'row', }}>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 1, }}>
                {/* 项目名 */}
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.textname, { fontSize: 17, fontWeight: 'bold' }]}>项目名：</Text>
                  <Text style={[styles.text, { fontSize: 17, fontWeight: 'bold' }]}> {item.projectAbb}</Text>
                </View>
                {/* 开工时间，日生产记录没有,所以只要排除他就好 */}
                <View style={url.indexOf('GetProduceDailyRecordProject') == -1 ? { flexDirection: 'row' } : styles.hidden}>
                  <Text style={styles.textname}>开工日期：</Text>
                  <Text style={styles.text}> {item.startTime}</Text>
                </View>
                {/* 数量，根据页面选择说明 */}
                <View style={url.indexOf('GetQualityFinProductsProject') != -1 ? styles.hidden : { flexDirection: 'row' }}>
                  <Text style={styles.textname}>{url.indexOf('GetWaitingPoolProject') != -1 ? '待产池数量：' : (url.indexOf('GetProduceDailyRecordProject') != -1 ? '生产数量：' : (url.indexOf('GetQualityScrapProject') != -1 ? '报废数量：' : '已检数量：'))}</Text>
                  <Text style={styles.text}> {item.compNum}件</Text>
                </View>
                {/* 生产方量，目前只有日生产记录有。 */}
                <View style={url.indexOf('GetProduceDailyRecordProject') != -1 ? { flexDirection: 'row' } : styles.hidden}>
                  <Text style={styles.textname}>生产方量：</Text>
                  <Text style={styles.text}> {item.compVolume}m³</Text>
                </View>
                {/* 成品检查模块， */}
                <View style={url.indexOf('GetQualityFinProductsProject') != -1 ? { flexDirection: 'column' } : styles.hidden}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.textname}>已检数量：</Text>
                    <Text style={styles.text}> {item.compNumPass + item.compNumNotPass}件</Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.textname}>合格数量：</Text>
                    <Text style={styles.text}> {item.compNumPass}件</Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.textname}>不合格数量：</Text>
                    <Text style={styles.text}> {item.compNumNotPass}件</Text>
                  </View>
                </View>
              </View>
              {/* <Button
                type='clear'
                title={item.state}
                titleStyle={{ fontSize: 14 }}
                containerStyle={{ borderRadius: 60 }}
                style={[item.state ? { flex: 0.1 } : { flex: 0 }, { borderRadius: 60, alignContent: 'flex-start' }]} /> */}
            </View>
          </TouchableOpacity>
        </Card>
      )
    } //已生产构件项目页面
    else if (item.projectState == '生产中') {
      return (
        <Card containerStyle={styles.card_containerStyle}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.push('ChildOne', {
                sonTitle: sonTitletest,
                url: Url.url + Url.Produce + Url.Fid + 'GetProjectProducedComponents/' + item.projectId,
              })
            }}>
            <View style={{ flexDirection: 'row', }}>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 1, }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>项目名称：</Text>
                  <Text style={[styles.text, { color: '#419FFF', width: width / 2 }]}> {item.projectAbb}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>开工日期：</Text>
                  <Text style={styles.text}> {item.startTime}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>已生产构件：</Text>
                  <Text style={styles.text}> {item.compNum}件</Text>
                </View><View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>已生产方量：</Text>
                  <Text style={styles.text}> {item.compVolume}m³</Text>
                </View>
              </View>
              <Button
                type='clear'
                title={item.projectState}
                titleStyle={{ fontSize: 14 }}
                containerStyle={{ borderRadius: 60 }}
                style={{ flex: 0.1, borderRadius: 60, alignContent: 'flex-start' }}
                icon={<Badge
                  status={item.projectState == '生产中' ? 'warning' : (item.projectState == null ? null : 'error')}
                  containerStyle={{ marginHorizontal: 4 }}
                />} />
              <View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    }
    else if (url.indexOf("GetProduceTask") != -1 /*&& url.indexOf("GetProduceTaskCompDetails") == -1 */) {
      return (
        <Card containerStyle={styles.card_containerStyle}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('ChildTwoMain', {
                sonTitle: sonTitletest,
                item: item,
                sonUrl: Url.url + Url.Produce + item._rowguid + '/GetProduceTaskCompDetails',
                url: url
              })
            }} >
            <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', width: width / 1.5 }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>任务编号：</Text>
                  <Text style={[styles.text, { color: 'orange' }]}> {item.formCode}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>计划生产日期：</Text>
                  <Text style={[styles.text, {}]}> {item.planDate}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>生产线：</Text>
                  <Text style={[styles.text, {}]}> {item.productLineName}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>劳务队：</Text>
                  <Text style={[styles.text, {}]}> {item.labourName}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>构件数目：</Text>
                  <Text style={[styles.text, {}]}> {item.compNum}件</Text>
                </View>
              </View>
              <View>
                <Button
                  type='clear'
                  title={item.state}
                  titleStyle={{ fontSize: 14 }}
                  containerStyle={{ borderRadius: 60 }}
                  style={[item.state ? { flex: 1 } : styles.hidden, { borderRadius: 60, alignContent: 'flex-start' }]}
                  disabled={true}
                  disabledTitleStyle={{ color: '#419FFF' }}
                  icon={<Badge
                    //入库蓝色，脱模待检黄色，出库绿色，剩下黄色
                    status={item.state == '提交' ? 'success' : 'error'}
                    containerStyle={{ marginHorizontal: 4 }}
                  />} />
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )

    } else if (url.indexOf("GetStockLoaoutWare") != -1 /*&& url.indexOf("GetProduceTaskCompDetails") == -1 */) {
      return (
        <Card containerStyle={styles.card_containerStyle}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('ChildTwoMain', {
                sonTitle: sonTitletest,
                item: item,
                sonUrl: Url.url + Url.Stock + item._rowguid + '/GetStockLoaoutWareCompDetails',
                url: url
              })
            }} >
            <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', width: width / 1.5 }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>计划单号：</Text>
                  <Text style={[styles.text, { color: 'orange' }]}> {item.planformCode}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>项目名称：</Text>
                  <Text style={[styles.text, {}]}> {item.projectName}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>楼层楼号：</Text>
                  <Text style={[styles.text, {}]}> {item.buildingNo}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>构件数量：</Text>
                  <Text style={[styles.text, {}]}> {item.compNumber}件</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>计划发货日期：</Text>
                  <Text style={[styles.text, {}]}> {item.planLibTime}</Text>
                </View>
              </View>
              <View>
                <Button
                  type='clear'
                  title={item.state}
                  titleStyle={{ fontSize: 14 }}
                  containerStyle={{ borderRadius: 60 }}
                  style={[item.state ? { flex: 1 } : styles.hidden, { borderRadius: 60, alignContent: 'flex-start' }]}
                  disabled={true}
                  disabledTitleStyle={{ color: '#000' }}
                  icon={<Badge
                    //入库蓝色，脱模待检黄色，出库绿色，剩下黄色
                    status={item.state == '已完成' ? 'primary' : 'error'}
                    containerStyle={{ marginHorizontal: 4 }}
                  />} />
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )

    }
    //通用页面 估计很快就不通用了？？
    else if (item.compTypeName) {
      return (
        <Card containerStyle={styles.card_containerStyle}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('ChildTwoMain', {
                sonTitle: sonTitletest,
                item: item,
                sonUrl: Url.url + Url.Produce + Url.Fid + 'GetProjectProducedComponents/' + item.projectId,
                url: url
              })
            }} >
            <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', width: width / 1.65 }}>
                <Text style={[styles.text, { color: 'orange' }]}>{item.compCode}</Text>
                <Text style={styles.text} >{item.projectName}</Text>
                <Text style={styles.text} >{item.compTypeName}</Text>
                <Text style={styles.text} >{item.designType}</Text>
                <Text style={styles.text}>{item.floorNoName}({item.floorName})</Text>
                <Text style={item.planDate || item.produceDate ? [styles.text, { color: 'gray' }] : styles.hidden}>{item.finChkTime ? '成检日期：' : '计划生产日期：'}
                  <Text style={styles.text}>{item.planDate ? item.planDate : (item.finChkTime ? item.finChkTime : item.produceDate)}</Text>
                </Text>
              </View>
              <View>
                <Button
                  type='clear'
                  title={item.bigState}
                  titleStyle={{ fontSize: 13 }}
                  containerStyle={{ borderRadius: 60 }}
                  style={[item.bigState ? { flex: 1 } : styles.hidden, { borderRadius: 60, alignContent: 'flex-start' }]}
                  disabled={true}
                  disabledTitleStyle={{ color: '#419FFF' }}
                  icon={<Badge
                    //入库蓝色，脱模待检黄色，出库绿色，剩下黄色
                    status={item.bigState == '脱模待检' ? 'warning' : (item.bigState == '出库' ? 'success' : (item.bigState == '入库' ? 'primary' : (item.bigState == null ? null : 'warning')))}
                    containerStyle={{ marginHorizontal: 4 }}
                  />} />
                {
                  item.state != '脱模待检' ? <View></View> :
                    <Button
                      type='clear'
                      title={item.insresults}
                      titleStyle={{ fontSize: 15 }}
                      style={[item.insresults ? { flex: 1 } : styles.hidden, { borderRadius: 60, alignContent: 'flex-start' }]}
                      disabled={true}
                      disabledTitleStyle={{ color: '#419FFF' }}
                      icon={<Badge
                        status={item.insresults == '合格' ? 'success' : (item.insresults == null ? null : 'error')}
                        containerStyle={{ marginHorizontal: 4 }}
                      />} />
                }

              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    }
  }
}

const styles = StyleSheet.create({
  loading: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  card_containerStyle: {
    borderRadius: 5,
    shadowOpacity: 0,
    borderWidth: 0,
    paddingVertical: 4
  },
  contrain: {
    flexDirection: 'column',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  txt: {
    fontSize: 16,
    marginBottom: 8,
    marginTop: 8,
    marginHorizontal: 8,
    textAlignVertical: 'center'
  },
  text: {
    fontSize: 14,
    marginBottom: 4,
    marginTop: 4,
    textAlignVertical: 'center'
  },
  textname: {

    fontSize: 14,
    marginBottom: 8,
    marginTop: 8,
    color: 'gray'
  },
  textView: {

    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  hidden: {
    flex: 0,
    height: 0,
    width: 0,
  },
  sideMenu_Main: {
    marginLeft: 8,
    marginTop: 14
  },
  sideMenu_Top: {
    marginLeft: 6,
    marginTop: 8
  },
  sideMenu_Bottom: {
    marginBottom: 10
  },
  sideMenu_Title: {
    fontSize: 16
  },
  sideMenu_Content: {
    height: 40,
    marginLeft: width * 0.005,
    marginRight: width * 0.08,
    backgroundColor: "#f8f8f8",
    borderRadius: 5,
    marginTop: 10
  },
  sideMenu_Picker: {
    flex: 1,
    fontSize: 14
  }
})