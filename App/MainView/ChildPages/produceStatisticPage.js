import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  processColor,
  Dimensions,
  PixelRatio,
  ScrollView,
  ActivityIndicator,
  FlatList,
  Alert,
  TouchableOpacity,
} from 'react-native';
import DatePicker from 'react-native-datepicker'
import { Button, ListItem, Badge, Icon, BottomSheet, SearchBar } from 'react-native-elements';
import BarLineChart from '../ChildComponent/BarLineChart';
import PieChart from "../ChildComponent/PieChart";
import HorizontalBarChart from "../ChildComponent/HorizontalBarChart";
import Url from '../../Url/Url';
import { deviceWidth, deviceHeight, RVW, RFT, RVH, color } from '../../Url/Pixal';
import LinearGradient from 'react-native-linear-gradient';
import Suspension from '../Components/Suspension';
import IconDown from '../../PDA/Componment/IconDown';
import CardMain from '../Components/CardMain';
import BottomItem from '../../PDA/Componment/BottomItem';

const moment = require('moment');

export default class Combined extends Component {
  constructor() {
    super();

    this.state = {
      date: 'Today',
      status: '生产',
      dateFocusButton: 0,
      dateFocusButtonName: 'today',
      typeFocusButton: 0,
      typeFocusButtonName: '生产',
      typeButtonList: [
        { id: '0', name: '生产' },
        { id: '1', name: '入库' },
        { id: '2', name: '发货' },
      ],
      dateButtonList: [
        { id: '0', name: '今日', date: 'today' },
        { id: '1', name: '昨日', date: 'yesterday' },
        { id: '2', name: '本周', date: 'week' },
        { id: '3', name: '本月', date: 'month' },
        { id: '4', name: '更多时间', date: 'custom' },
      ],
      ProductLineChart: [],
      ProductLineData: [],
      TeamChart: [],
      TeamData: [],
      ProjectChart: [],
      ProjectData: [{
        projectAbb: '加载中'
      }],

      CompTypeChart: [],
      CompTypeData: [],
      isLoading: true,
      cardLoading: true,
      isClick: false,
      //自定义时间
      isShowCusDate: false,
      startTime: moment().format('YYYY-MM-DD'),
      endTime: moment().add(1, 'days').format('YYYY-MM-DD'),
      minDate: '2000-01-01',
      maxDate: moment().add(20, 'years').format('YYYY-MM-DD'),
      isDropDownModalOpen: false,
      //项目
      resDataProject: [],
      projectSerchText: '',
      projectId: '',
      projectAbb: '',
      projectSelect: '全部项目',
      projectSelectArr: [],
      isTimeSelected: 0
    };
    this.requestData = this.requestData.bind(this);
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title'),
    };
  };

  componentDidMount() {
    console.log("🚀 ~ file: produceStatisticPage.js:132 ~ componentDidMount ~ Url.isNewProductionCard:", Url.isNewProductionCard)

    const { navigation } = this.props;
    const buttonID = navigation.getParam('buttonID');
    console.log("🚀 ~ file: produceStatisticPage.js:96 ~ componentDidMount ~ buttonID:", buttonID)
    let buttonDay = this.state.dateButtonList[buttonID].date
    const badge = navigation.getParam('badge');
    const typeFocusButtonID = badge === '生产' ? 0 : badge === '入库' ? 1 : 2;
    // 初始化最大日期最小日期
    // let dateStr1 = moment().format('YYYY/MM/DD');
    // let maxDateUnit = parseInt(new Date(dateStr1).getTime())
    // let minDateUnit = maxDateUnit - (183 * 86400 * 1000)
    // let maxDate = moment().add(1, 'days').format('YYYY-MM-DD')
    // let minDate = new Date(minDateUnit)
    // minDate = ((minDate.getFullYear()) + "-" +
    //   (minDate.getMonth() + 1) + "-" +
    //   (minDate.getDate())
    // )
    // this.setState({
    //   minDate: minDate,
    //   maxDate: maxDate,
    // })
    this.setState({
      dateFocusButton: buttonID,
      dateFocusButtonName: buttonDay,
      typeFocusButton: typeFocusButtonID,
      status: badge,
      typeFocusButtonName: badge
    });
    //this.requestData();
    this.resDataProject()

    if (Url.isNewProductionCard) {
      this.requestDataPost(buttonDay, badge)
    } else {
      this.setState({
        dateButtonList: [
          { id: '0', name: '今日', date: 'today' },
          { id: '1', name: '昨日', date: 'yesterday' },
          { id: '2', name: '本周', date: 'week' },
          { id: '3', name: '本月', date: 'month' },
        ],
      }, () => {
        this.requestData()
      })
    }
  }

  resDataProject = () => {
    const uri = Url.url + Url.factory + Url.Fid + Url.StaticType + 'GetFactoryProjectsList';
    fetch(uri, {
      credentials: 'include',
    }).then(resButton => {
      return resButton.json()
    }).then(resDataProject => {
      console.log("🚀 ~ file: produceStatisticPage.js:123 ~ resDataProject:", resDataProject)
      if (typeof resDataProject != 'undefined') {
        this.setState({
          resDataProject: resDataProject,
          projectSelectArr: resDataProject,
        })
      }
      //console.log(this.state.resDataButton)
    }).catch(err => {
      console.log("🚀 ~ file: Project.js:195 ~ Project_Screen ~ fetch ~ err", err.toString())
    })
  }

  //旧版本
  requestData = async (Date, status) => {
    const { navigation } = this.props;
    let TotalNum = navigation.getParam('TotalNum') || 0
    let TotalVol = navigation.getParam('TotalVol') || 0
    let buttonID = navigation.getParam('buttonID') || this.state.dateFocusButton;
    const badge = status || navigation.getParam('badge');
    let buttonDay = this.state.dateButtonList[buttonID].date
    if (typeof Date != 'undefined') {
      buttonDay = Date
    }
    let urlBase = Url.url + Url.factory + Url.Fid;
    let urlProductLine = urlBase + 'GetProductLineStatistic' + (Date || buttonDay);
    let urlTeam = urlBase + Url.StaticType + 'GetTeamStatistic' + (Date || buttonDay);
    let urlProject = urlBase + 'GetProjectStatistic' + (Date || buttonDay);
    let urlCompType = urlBase + 'GetCompTypeStatistic' + (Date || buttonDay);
    if (Url.isStaticTypeOpen) {
      if (Url.StaticType != '0/') {
        urlCompType = urlBase + Url.StaticType + "GetDuctPieceCompTypeStatistic" + (Date || buttonDay)
        urlProject = urlBase + Url.StaticType + "GetDuctPieceProjectStatistic" + (Date || buttonDay)
        urlProductLine = urlBase + Url.StaticType + "GetDuctPieceProductLineStatistic" + (Date || buttonDay)
      }
      if (buttonDay == 'Yesterday') {
        urlCompType = urlBase + Url.StaticType + 'GetProductLineStatistic' + (Date || buttonDay);
        urlProject = urlBase + Url.StaticType + 'GetProjectStatistic' + (Date || buttonDay);
        urlProductLine = urlBase + Url.StaticType + 'GetCompTypeStatistic' + (Date || buttonDay);
      }
    }

    console.log("🚀 ~ file: produceStatisticPage.js:94 ~ Combined ~ requestData= ~ urlProductLine:", urlProductLine)
    console.log("🚀 ~ file: produceStatisticPage.js:96 ~ Combined ~ requestData= ~ urlProject:", urlProject)
    console.log("🚀 ~ file: produceStatisticPage.js:95 ~ Combined ~ requestData= ~ urlTeam:", urlTeam)

    //const badge = this.props.navigation.getParam('badge');
    const typeFocusButtontmp = badge === '生产' ? 0 : badge === '入库' ? 1 : 2;
    this.setState({
      typeFocusButton: typeFocusButtontmp,
      status: badge,
    });
    //await this.refLine.setStateLoaing();
    //生产线
    this.ProductLineResData(urlProductLine, badge, TotalNum, TotalVol);

    //班组
    this.TeamResData(urlTeam, badge, TotalNum, TotalVol);

    //项目
    this.ProjectResData(urlProject, badge, TotalNum, TotalVol);

    //构件类型
    this.CompTypeResData(urlCompType, badge, TotalNum, TotalVol);
  };

  ProductLineResData = (urlProductLine, badge) => {
    let Name = [],
      Num = [],
      Vol = [],
      PieData = [],
      TotalNum = 0,
      TotalVol = 0,
      ProductLine = {},
      NewList = [],
      count = 0;
    fetch(urlProductLine)
      .then(res => {
        return res.json();
      })
      .then(resData => {
        resData.map((item, index) => {
          let tmp = {}
          Name.push(item.productLineName)
          if (badge === '生产') {
            if (item.proNumber != 0 || item.proVolume != 0) {
              item.color = color[count++]
              Num.push(item.proNumber)
              Vol.push(item.proVolume)
              tmp.value = item.proVolume
              tmp.name = item.productLineName
              PieData.push(tmp)
              TotalNum += item.proNumber
              TotalVol += item.proVolume
              NewList.push(item)
            }
          }
        });

        ProductLine.Name = Name
        ProductLine.Num = Num
        ProductLine.Vol = Vol
        ProductLine.TotalNum = TotalNum
        ProductLine.TotalVol = TotalVol.toFixed(3)


        this.setState({
          ProductLineChart: ProductLine,
          ProductLineData: NewList,
          isLoading: false,
          cardLoading: false,
          isClick: false
        });


        this.refLine.ReFlash(PieData)


      })
      .catch(error => {
        console.log(error);
      });
  };

  TeamResData = (urlTeam, badge) => {
    let Name = [],
      Num = [],
      Vol = [],
      Team = {},
      NewList = [],
      count = 0;
    fetch(urlTeam)
      .then(res => {
        return res.json();
      })
      .then(resData => {
        resData.map((item, index) => {
          if (badge === '生产') {
            if (item.proNumber != 0 || item.proVolume != 0) {
              item.color = color[count++]
              Name.push(item.teamName);
              Num.push(item.proNumber);
              Vol.push(item.proVolume);
              NewList.push(item)
            }
          }
        });
        Team.Name = Name;
        Team.Num = Num;
        Team.Vol = Vol;

        console.log("TeamResData -> Team", Team)
        console.log("TeamResData -> NewList", NewList)

        this.setState({
          TeamData: NewList,
          TeamChart: Team,
          isLoading: false,
          cardLoading: false,
          isClick: false
        });

        this.refTeam.ReFlash(Team);

      })
      .catch(error => {
        console.log("TeamResData -> error", error)
      });
  };

  ProjectResData = (urlProject, badge) => {
    let Name = [],
      Num = [],
      Vol = [],
      Project = {},
      NewList = [],
      count = 0;
    fetch(urlProject)
      .then(res => {
        return res.json();
      })
      .then(resData => {
        resData.map((item, index) => {
          //Name.push(item.projectAbb);
          item.projectAbb = item.projectAbb.slice(0, 6)
          if (badge === '生产') {
            if (item.proNumber != 0 || item.proVolume != 0) {
              item.color = color[count++]
              Name.push(item.projectAbb);
              Num.push(item.proNumber);
              Vol.push(item.proVolume);
              NewList.push(item)
            }
          } else if (badge === '入库') {
            if (item.storageNumber != 0 || item.storageVolume != 0) {
              item.color = color[count++]
              Name.push(item.projectAbb);
              Num.push(item.storageNumber);
              Vol.push(item.storageVolume);
              NewList.push(item)
            }
          } else {
            if (item.outNumber != 0 || item.outVolume != 0) {
              item.color = color[count++]
              Name.push(item.projectAbb);
              Num.push(item.outNumber);
              Vol.push(item.outVolume);
              NewList.push(item)
            }
          }
        });
        Project.Name = Name;
        Project.Num = Num;
        Project.Vol = Vol;

        this.setState({
          ProjectData: NewList,
          ProjectChart: Project,
          isLoading: false,
          cardLoading: false,
          isClick: false
        });

        this.refProject.ReFlash(Project);

        console.log(Project);
      })
      .catch(error => {
        console.log(error);
      });
  };

  CompTypeResData = (urlCompType, badge) => {
    let Name = [],
      Num = [],
      Vol = [],
      TotalNum = 0,
      TotalVol = 0,
      CompType = {},
      NewList = [],
      count = 0;
    fetch(urlCompType)
      .then(res => {
        return res.json();
      })
      .then(resData => {
        resData.map((item, index) => {
          //Name.push(item.compTypeName);

          if (badge === '生产') {
            if (item.proNumber != 0 || item.proVolume != 0) {
              item.color = color[count++]
              Name.push(item.compTypeName);
              Num.push(item.proNumber);
              Vol.push(item.proVolume);
              NewList.push(item);
              TotalNum += item.proNumber
              TotalVol += item.proVolume
            }
          } else if (badge === '入库') {
            if (item.storageNumber != 0 || item.storageVolume != 0) {
              item.color = color[count++]
              Name.push(item.compTypeName);
              Num.push(item.storageNumber);
              Vol.push(item.storageVolume);
              NewList.push(item)
              TotalNum += item.storageNumber
              TotalVol += item.storageVolume
            }
          } else {
            if (item.outNumber != 0 || item.outVolume != 0) {
              item.color = color[count++]
              Name.push(item.compTypeName);
              Num.push(item.outNumber);
              Vol.push(item.outVolume);
              NewList.push(item)
              TotalNum += item.outNumber
              TotalVol += item.outVolume
            }

          }
        });
        CompType.Name = Name;
        CompType.Num = Num;
        CompType.Vol = Vol;
        CompType.TotalNum = TotalNum
        CompType.TotalVol = TotalVol.toFixed(3)

        this.setState({
          CompTypeData: NewList,
          CompTypeChart: CompType,
          isLoading: false,
          cardLoading: false,
          isClick: false
        });

        this.refCompType.ReFlash(CompType);

      })
      .catch(error => {
        console.log(error);
      });
  }

  requestDataPost = (Date, status) => {
    console.log("🚀 ~ file: produceStatisticPage.js:218 ~ status:", status)
    const { navigation } = this.props;
    let TotalNum = navigation.getParam('TotalNum') || 0
    let TotalVol = navigation.getParam('TotalVol') || 0
    let urlBase = Url.url + Url.project;
    let urlProductLine = urlBase + 'GetFactoryPrjLineStatisticDateRange';
    let urlTeam = urlBase + 'GetFactoryPrjTeamStatisticDateRange';
    let urlProject = urlBase + 'GetFactoryProjectStatisticDateRange';
    let urlCompType = urlBase + 'GetFactoryPrjCompTypeStatisticDateRange';

    this.ProductLineResDataPost(urlProductLine, Date, status, TotalNum, TotalVol);

    //班组
    this.TeamResDataPost(urlTeam, Date, status, TotalNum, TotalVol);

    //项目
    this.ProjectResDataPost(urlProject, Date, status, TotalNum, TotalVol);

    //构件类型
    this.CompTypeResDataPost(urlCompType, Date, status, TotalNum, TotalVol);
  }

  ProductLineResDataPost = (urlProductLine, buttonDay, badge) => {
    console.log("🚀 ~ file: produceStatisticPage.js:246 ~ urlProductLine:", urlProductLine)
    const { projectId, startTime, endTime } = this.state
    let Name = [],
      Num = [],
      Vol = [],
      PieData = [],
      TotalNum = 0,
      TotalVol = 0,
      ProductLine = {},
      NewList = [],
      count = 0;

    let productLineData = {
      "type": Url.StaticType.substring(0, 1),
      "dateType": buttonDay,
      "factoryId": Url.Fidweb,
      "projectId": projectId,
      "startDate": startTime,
      "endDate": endTime
    }
    console.log("🚀 ~ file: produceStatisticPage.js:257 ~ productLineData:", JSON.stringify(productLineData))

    fetch(urlProductLine, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(productLineData)
    })
      .then(res => {
        return res.json();
      })
      .then(resData => {
        console.log("🚀 ~ file: produceStatisticPage.js:274 ~ resData:", resData)
        resData.map((item, index) => {
          let tmp = {}
          Name.push(item.productLineName)
          if (badge === '生产') {
            if (item.proNumber != 0 || item.proVolume != 0) {
              item.color = color[count++]
              Num.push(item.proNumber)
              Vol.push(item.proVolume)
              tmp.value = item.proVolume
              tmp.name = item.productLineName
              PieData.push(tmp)
              TotalNum += item.proNumber
              TotalVol += item.proVolume
              NewList.push(item)

            }
          }
        });

        ProductLine.Name = Name
        ProductLine.Num = Num
        ProductLine.Vol = Vol
        ProductLine.TotalNum = TotalNum
        ProductLine.TotalVol = TotalVol.toFixed(3)
        console.log("🚀 ~ file: produceStatisticPage.js:303 ~ ProductLine:", ProductLine)
        console.log("🚀 ~ file: produceStatisticPage.js:294 ~ resData.map ~ NewList:", NewList)

        this.setState({
          ProductLineChart: PieData,
          ProductLineData: NewList,
          isLoading: false,
          cardLoading: false,
          isClick: false
        });


        this.refLine.ReFlash(PieData)
      })
      .catch(error => {
        console.log(error);
      });
  };

  TeamResDataPost = (urlTeam, buttonDay, badge) => {
    const { projectId, startTime, endTime } = this.state
    let Name = [],
      Num = [],
      Vol = [],
      Team = {},
      NewList = [],
      count = 0;
    let TeamData = {
      "type": Url.StaticType.substring(0, 1),
      "dateType": buttonDay,
      "factoryId": Url.Fidweb,
      "projectId": projectId,
      "startDate": startTime,
      "endDate": endTime
    }
    console.log("🚀 ~ file: produceStatisticPage.js:345 ~ JSON.stringify(TeamData):", JSON.stringify(TeamData))
    fetch(urlTeam, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(TeamData)

    })
      .then(res => {
        return res.json();
      })
      .then(resData => {
        resData.map((item, index) => {
          if (badge === '生产') {
            if (item.proNumber != 0 || item.proVolume != 0) {
              item.color = color[count++]
              Name.push(item.teamName);
              Num.push(item.proNumber);
              Vol.push(item.proVolume);
              NewList.push(item)
            }
          }
        });
        Team.Name = Name;
        Team.Num = Num;
        Team.Vol = Vol;


        this.setState({
          TeamData: NewList,
          TeamChart: Team,
          isLoading: false,
          cardLoading: false,
          isClick: false
        });

        this.refTeam.ReFlash(Team);

      })
      .catch(error => {
        console.log("TeamResData -> error", error)
      });
  };

  ProjectResDataPost = (urlProject, buttonDay, badge) => {
    const { projectId, startTime, endTime } = this.state
    let Name = [],
      Num = [],
      Vol = [],
      Project = {},
      NewList = [],
      count = 0;

    let ProjectData = {
      "type": Url.StaticType.substring(0, 1),
      "dateType": buttonDay,
      "factoryId": Url.Fidweb,
      "projectId": projectId,
      "startDate": startTime,
      "endDate": endTime
    }
    fetch(urlProject, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(ProjectData)
    })
      .then(res => {
        return res.json();
      })
      .then(resData => {
        resData.map((item, index) => {
          //Name.push(item.projectAbb);
          item.projectAbb = item.projectAbb.slice(0, 6)
          if (badge === '生产') {
            if (item.proNumber != 0 || item.proVolume != 0) {
              item.color = color[count++]
              Name.push(item.projectAbb);
              Num.push(item.proNumber);
              Vol.push(item.proVolume);
              NewList.push(item)
            }
          } else if (badge === '入库') {
            if (item.storageNumber != 0 || item.storageVolume != 0) {
              item.color = color[count++]
              Name.push(item.projectAbb);
              Num.push(item.storageNumber);
              Vol.push(item.storageVolume);
              NewList.push(item)
            }
          } else {
            if (item.outNumber != 0 || item.outVolume != 0) {
              item.color = color[count++]
              Name.push(item.projectAbb);
              Num.push(item.outNumber);
              Vol.push(item.outVolume);
              NewList.push(item)
            }
          }
        });
        Project.Name = Name;
        Project.Num = Num;
        Project.Vol = Vol;

        this.setState({
          ProjectData: NewList,
          ProjectChart: Project,
          isLoading: false,
          cardLoading: false,
          isClick: false
        });

        this.refProject.ReFlash(Project);

        console.log(Project);
      })
      .catch(error => {
        console.log(error);
      });
  };

  CompTypeResDataPost = (urlCompType, buttonDay, badge) => {
    console.log("🚀 ~ file: produceStatisticPage.js:469 ~ badge:", badge)
    console.log("🚀 ~ file: produceStatisticPage.js:467 ~ urlCompType:", urlCompType)
    const { projectId, startTime, endTime } = this.state
    let Name = [],
      Num = [],
      Vol = [],
      TotalNum = 0,
      TotalVol = 0,
      CompType = {},
      NewList = [],
      count = 0;
    let CompTypeData = {
      "type": Url.StaticType.substring(0, 1),
      "dateType": buttonDay,
      "factoryId": Url.Fidweb,
      "projectId": projectId,
      "startDate": startTime,
      "endDate": endTime
    }
    console.log("🚀 ~ file: produceStatisticPage.js:490 ~ JSON.stringify(CompTypeData):", JSON.stringify(CompTypeData))
    fetch(urlCompType, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(CompTypeData)

    })
      .then(res => {
        return res.json();
      })
      .then(resData => {
        console.log("🚀 ~ file: produceStatisticPage.js:501 ~ resData:", resData)
        resData.map((item, index) => {
          //Name.push(item.compTypeName);

          if (badge === '生产') {
            if (item.proNumber != 0 || item.proVolume != 0) {
              item.color = color[count++]
              Name.push(item.compTypeName);
              Num.push(item.proNumber);
              Vol.push(item.proVolume);
              NewList.push(item);
              TotalNum += item.proNumber
              TotalVol += item.proVolume
            }
          } else if (badge === '入库') {
            console.log("🚀 ~ file: produceStatisticPage.js:4691 ~ resData.map ~ badge === '入库':", badge === '入库')
            console.log("🚀 ~ file: produceStatisticPage.js:517 ~ resData.map ~ item.storageVolume != 0:", item.storageVolume != 0)
            console.log("🚀 ~ file: produceStatisticPage.js:517 ~ resData.map ~ item.storageNumber != 0:", item.storageNumber != 0)
            if (item.storageNumber != 0 || item.storageVolume != 0) {
              item.color = color[count++]
              Name.push(item.compTypeName);
              Num.push(item.storageNumber);
              Vol.push(item.storageVolume);
              NewList.push(item)
              TotalNum += item.storageNumber
              TotalVol += item.storageVolume
            }
          } else {
            if (item.outNumber != 0 || item.outVolume != 0) {
              item.color = color[count++]
              Name.push(item.compTypeName);
              Num.push(item.outNumber);
              Vol.push(item.outVolume);
              NewList.push(item)
              TotalNum += item.outNumber
              TotalVol += item.outVolume
            }

          }
        });
        CompType.Name = Name;
        CompType.Num = Num;
        CompType.Vol = Vol;
        CompType.TotalNum = TotalNum
        CompType.TotalVol = TotalVol.toFixed(3)

        this.setState({
          CompTypeData: NewList,
          CompTypeChart: CompType,
          isLoading: false,
          cardLoading: false,
          isClick: false
        });

        this.refCompType.ReFlash(CompType);

      })
      .catch(error => {
        console.log(error);
      });
  };

  TypeButton = () => {
    const { typeButtonList, typeFocusButton, date, isClick } = this.state;

    return (
      <View
        style={[
          styles.buttonview,
          { justifyContent: 'space-evenly', marginVertical: 8 },
        ]}>
        {typeButtonList.map((item, index) => {
          return (
            <Button

              disabled={isClick}
              buttonStyle={{
                borderRadius: 0,
                borderBottomColor: typeFocusButton == index ? '#419FFF' : '#999997',
                borderBottomWidth: 2,
                height: RFT * 8,
              }}
              containerStyle={{
                borderColor: typeFocusButton == index ? '#419FFF' : '#999997',
              }}
              title={'' + item.name + ' '}
              titleStyle={{
                fontFamily: 'STHeitiSC-Light',
                textAlign: 'center',
                //fontSize: RFT * 3.3,
                color: typeFocusButton == index ? '#333333' : '#333333',
              }}
              type="clear"
              onPress={() => {
                let buttonID = this.state.dateFocusButton;
                const buttonDay = this.state.dateButtonList[buttonID].date
                this.refLine.setStateLoaing();
                this.refTeam.setStateLoaing();
                this.refProject.setStateLoaing();
                this.refCompType.setStateLoaing();
                this.requestData(buttonDay, item.name);
                this.setState({
                  cardLoading: true,
                  typeFocusButton: index,
                  status: item.name,
                  isClick: true,
                });
              }}
            />
          );
        })}
      </View>
    );
  };

  TypeButtonNew = () => {
    const { typeButtonList, typeFocusButton, date, isClick, dateFocusButton, dateFocusButtonName } = this.state;
    return (
      <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: 'wihte', marginHorizontal: deviceWidth * 0.06 }}>
        {
          typeButtonList.map((item, index) => {
            return (
              <Button
                disabled={isClick}
                buttonStyle={{
                  //borderRadius: 20,
                  paddingHorizontal: 10,
                  width: 80,
                  height: 40,
                  //flex: 1,
                  backgroundColor: typeFocusButton == index ? 'white' : 'transparent',
                  borderBottomColor: typeFocusButton == index ? '#419FFF' : 'transparent',
                  borderBottomWidth: typeFocusButton == index ? 1 : 0,
                }}
                containerStyle={{
                }}
                titleStyle={{ fontSize: RFT * 3.5, color: typeFocusButton == index ? '#419FFF' : '#333' }}
                type='clear'
                title={item.name}
                onPress={() => {
                  if (Url.isNewProductionCard) {
                    this.requestDataPost(dateFocusButtonName, item.name);
                  } else {
                    this.requestData(dateFocusButtonName, item.name);
                  }
                  this.setState({
                    cardLoading: true,
                    typeFocusButton: index,
                    status: item.name,
                    isClick: true,
                  });
                }}
              />

            )
          })
        }
      </View>
    )
  }

  DateButton = () => {
    const { dateButtonList, dateFocusButton, status, isClick } = this.state;
    return (
      <View style={styles.buttonview}>
        {dateButtonList.map((item, index) => {
          return (
            <Button
              disabled={isClick}
              buttonStyle={{
                borderRadius: 50,
                borderColor: dateFocusButton == index ? '#419FFF' : '#999997',
                //flex: 1,
                backgroundColor: dateFocusButton == index ? '#419FFF' : 'white',
              }}
              title={' ' + item.name + ' '}
              titleStyle={{
                fontFamily: 'STHeitiSC-Light',
                //fontSize: RFT * 3.3,
                color: dateFocusButton == index ? 'white' : '#333333',
              }}
              //type="outline"
              onPress={() => {
                if (index == 4) {

                  this.setState({
                    isShowCusDate: true,
                    dateFocusButton: index,
                  })
                  this.refs.datePicker0.onPressDate()
                  console.log("🚀 ~ file: produceStatisticPage.js:486 ~ Combined ~ {dateButtonList.map ~ this.refs.datePicker0:", this.refs.datePicker0)
                  return
                }
                this.setState({
                  isShowCusDate: false
                })
                //this.updateIndex(index);
                this.refLine.setStateLoaing();
                this.refTeam.setStateLoaing();
                this.refProject.setStateLoaing();
                this.refCompType.setStateLoaing();
                this.setState({
                  cardLoading: true,
                  dateFocusButton: index,
                  date: item.date,
                  isClick: true,
                });
                this.requestData(item.date, status);

              }}
            />
          );
        })}
      </View>
    );
  };

  _DatePicker = (index) => {
    const { startTime, endTime, minDate, maxDate, isTimeSelected } = this.state
    let date = '', minDate1 = '', maxDate1 = ''
    if (index == 0) {
      date = startTime
    }
    if (index == 1) {
      date = endTime
      minDate1 = startTime
    }
    return (
      <DatePicker

        ref={"datePicker" + index}
        customStyles={{
          dateInput: styles.dateInput,
          dateText: styles.dateText,
          dateTouchBody: [styles.dateTouchBody, isTimeSelected == index ? { backgroundColor: 'rgba(254,211,48,0.7)' } : { backgroundColor: '#f8f8f8' }],
          dateIcon: { left: -10 },
        }}
        iconComponent={<Icon name='caretdown' color='#aaa' iconStyle={{ fontSize: 13, left: 1 }} type='antdesign' ></Icon>
        }
        showIcon={true}
        mode='date'
        onOpenModal={() => {
          this.setState({ isTimeSelected: index })
        }}
        onCloseModal={() => {
          this.setState({ isTimeSelected: 2 })
        }}
        date={date}
        minDate={minDate}
        maxDate={maxDate}
        confirmBtnText="确定"
        cancelBtnText="取消"
        format="YYYY-MM-DD"
        onPressMask={() => {
          this.setState({
            isTimeSelected: index
          })
        }}
        onDateChange={(value) => {
          console.log("🚀 ~ file: produceStatisticPage.js:517 ~ Combined ~ value:", value)
          console.log("🚀 ~ file: produceStatisticPage.js:517 ~ Combined ~ typeof value:", typeof value)
          if (index == 0) {
            this.setState({
              startTime: value,
              isTimeSelected: 1
            })

            let minDateUnit = moment(value).format('x')
            let maxDateUnit = parseInt(minDateUnit) + (184 * 86400 * 1000)
            let minDate = moment(value).add(1, 'days').format('YYYY-MM-DD')
            let maxDate = moment(maxDateUnit).format('YYYY-MM-DD')
            let endTimeUnit = moment(endTime).format('x')
            if (endTimeUnit > maxDateUnit) {
              this.setState({
                endTime: maxDate
              })
            }
            if (endTimeUnit < minDateUnit) {
              this.setState({
                endTime: minDate
              })
            }
            console.log("🚀 ~ file: produceStatisticPage.js:778 ~ maxDate:", maxDate)
            this.setState({
              minDate: minDate,
              maxDate: maxDate,
            }, () => {
              this.refs["datePicker1"].onPressDate()
            })

          }
          if (index == 1) {
            let valueUnit = new Date(value).getTime()
            let startTimeUnit = new Date(startTime).getTime()
            if (valueUnit <= startTimeUnit) {
              Alert.alert("终止日期需要大于起始日期");
              return;
            }
            let minDate = '2000-01-01'
            let maxDate = moment().add(1, 'days').format('YYYY-MM-DD')
            this.setState({
              minDate: minDate,
              maxDate: maxDate,
            })
            this.setState({
              endTime: value
            })
            this.requestDataPost('custom', this.state.status)
            this.setState({
              isTimeSelected: 2
            })
          }
        }}
      />
    )
  }

  CusTimeRender = () => {
    return (
      <View style={this.state.isShowCusDate ? {
        flexDirection: 'row',
        marginHorizontal: deviceWidth * 0.05,
        justifyContent: 'space-between',
        //marginTop: 10,
      } :
        { flex: 0, height: 0, width: 0, top: -200 }}>
        {this._DatePicker(0)}
        <Text style={{ lineHeight: 25, height: 25, top: 10, marginHorizontal: RFT * 6 }}>至</Text>
        {this._DatePicker(1)}
      </View>
    )
  }

  TopCard = () => {
    const { ProductLineChart, status, date, CompTypeChart, dateFocusButtonName } = this.state
    let day = dateFocusButtonName == 'today' ? '今日' : (dateFocusButtonName == 'yesterday' ? '昨日' : (dateFocusButtonName == 'week' ? '本周' : dateFocusButtonName == 'month' ? '本月' : ''))
    return (
      <View style={{ justifyContent: 'center', alignItems: 'center', borderRadius: 12, marginVertical: 8 }}>
        <LinearGradient style={{ width: deviceWidth * 0.9, borderRadius: 5, }} start={{ x: 1, y: 1 }} end={{ x: 0, y: 1 }}
          colors={['#419FFF', 'rgba(65,216,255,0.88) ']}>
          <View style={{ flexDirection: 'row', marginVertical: RFT * 2, justifyContent: 'center', alignContent: 'center' }}>
            <View style={{ justifyContent: 'center', alignContent: 'center' }}>
              <Text style={CompTypeChart.TotalVol < 1000 ? styles.text_TotalName : [styles.text_TotalName, { fontSize: RFT * 3.2 }]} >{day + status + '总量'}</Text>
            </View>
            <View style={{ justifyContent: 'center', alignContent: 'center' }}>
              <Text style={CompTypeChart.TotalNum < 1000 ? styles.text_TotalValue : [styles.text_TotalValue, { fontSize: RFT * 3.2 }]} >{'总数：' + (CompTypeChart.TotalNum || 0) + '件'}</Text>
            </View>
            <View style={{ justifyContent: 'center', alignContent: 'center' }}>
              <Text style={CompTypeChart.TotalVol < 1000 ? styles.text_TotalValue : [styles.text_TotalValue, { fontSize: RFT * 3.2 }]} >{'总方量：' + (CompTypeChart.TotalVol || 0) + 'm³'}</Text>
            </View>
          </View>
        </LinearGradient>
      </View>
    )
  }

  ListTitle = () => {
    return (
      <ListItem
        //title={item.productLineName + ': ' + item.proNumber + '件' + '(' + item.proVolume + 'm³)'}
        title={
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ flex: 1.5 }}>名称</Text>
            <Text style={{ flex: 1 }}>数量</Text>
            <Text style={{ flex: 1 }}>方量</Text>
          </View>
        }
        //titleStyle={styles.text_list}
        containerStyle={styles.text_list_container}
        bottomDivider
        leftElement={<Badge badgeStyle={{ backgroundColor: 'white' }} />}
      />
    )
  }

  CardView = (refprops, Dataprops) => {
    if (this.state.cardLoading) {
      return (
        <View
          style={{
            height: deviceHeight * 0.58,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#F5FCFF',
          }}>
          <ActivityIndicator animating={true} color="#419FFF" size="large" />
        </View>
      )
    } else {
      return (
        <View style={[styles.chartview, { height: RVH * RFT * 9 + RVH * RFT * 1.63 * Dataprops.length }]}>

          {/* 图表区 */}
          <View style={{ flexDirection: 'row' }}>

            {/* 图表 */}
            <View style={styles.chart}>
              {
                refprops == 'refCompType' ?
                  <BarLineChart
                    height={deviceWidth * 0.7}
                    ref={refCompType => {
                      this.refCompType = refCompType;
                    }}
                  /> : (
                    refprops == 'refLine' ?
                      <PieChart
                        height={deviceWidth * 0.65}
                        ref={refLine => {
                          this.refLine = refLine;
                        }}
                      /> : (
                        refprops == 'refProject' ?
                          <BarLineChart
                            height={deviceWidth * 0.7}
                            ref={refProject => {
                              this.refProject = refProject;
                            }}
                          /> : <HorizontalBarChart
                            height={deviceWidth * 0.7}
                            ref={refTeam => {
                              this.refTeam = refTeam;
                            }}
                          />
                      )
                  )
              }

            </View>

          </View>

          {/* 表格区 */}
          <View style={styles.chartview_listview}>
            {/* this.ListTitle() */}
            <FlatList
              //style={{ height: RVH * RFT * 3 * Dataprops.length }}
              data={Dataprops}
              renderItem={this._renderItem}
            />

          </View>

        </View>
      )

    }
  }

  //日期按钮
  updateIndex(index) {
    let selectedIndex = index
    let buttonID = index
    let buttonDay = this.state.dateButtonList[buttonID].date
    const { dateButtonList, status } = this.state;
    if (index == 4) {
      this.setState({
        isShowCusDate: true,
        dateFocusButton: index,
        dateFocusButtonName: buttonDay
      })
      // console.log("🚀 ~ file: produceStatisticPage.js:486 ~ Combined ~ {dateButtonList.map ~ this.refs.datePicker0:", this.refs["datePicker0"])
      if (typeof this.refs["datePicker0"] != 'undefined') {
        this.refs["datePicker0"].onPressDate()
      }
      return
    }
    this.setState({
      isShowCusDate: false
    })

    // this.updateIndex(index);
    // this.refLine.setStateLoaing();
    // this.refTeam.setStateLoaing();
    // this.refProject.setStateLoaing();
    // this.refCompType.setStateLoaing();
    this.setState({
      cardLoading: true,
      dateFocusButton: index,
      dateFocusButtonName: buttonDay,
      date: dateButtonList[index].date,
      isClick: true,
    });
    if (Url.isNewProductionCard) {
      this.requestDataPost(dateButtonList[index].date, status);
    } else {
      this.requestData(dateButtonList[index].date, status);
    }
    //this.requestDataPost(dateButtonList[index].date, status);
  }

  SearchProjectBar = () => {
    const { projectAbb, projectId, projectSelect } = this.state
    return (
      <View style={Url.isNewProductionCard ? {
        width: deviceWidth * 0.9,
        marginLeft: deviceWidth * 0.05,
        marginTop: 10,
        borderRadius: 10,
        backgroundColor: '#f8f8f8'
      } : { flex: 0, height: 0, width: 0, top: -200 }}>
        <TouchableOpacity
          onPress={() => {
            this.setState({
              isDropDownModalOpen: true
            })
          }}>
          <View style={{
            backgroundColor: 'transparent',
            color: "#333",
            paddingVertical: 10,
            paddingHorizontal: 15,
            flexDirection: 'row'
          }}>
            <View style={{ flex: 6 }}>
              <Text numberOfLines={1}>{projectSelect}</Text>
            </View>
            <View style={{ flex: 1, }}>
              <Icon name='caretdown' color='#aaa' iconStyle={{ fontSize: 13, marginTop: 1, marginLeft: 8, marginRight: -1 }} type='antdesign' ></Icon>
            </View>
          </View>
        </TouchableOpacity>

      </View>
    )
  }

  onSearchDriver = (text) => {
    const { resDataProject } = this.state
    let re = /[\u4E00-\u9FA5]/g; //测试中文字符的正则
    let textArr = []
    let searchArr = [[]]
    let reasonProjectArr = []
    if (text.length > 0) {
      textArr = text.split("")
      for (let i = 0; i < textArr.length; i++) {
        const tempsearchArr = searchArr.map(subset => {
          const one = subset.concat([]);
          one.push(textArr[i]);
          return one;
        })
        searchArr = searchArr.concat(tempsearchArr);
      }
      console.log("🚀 ~ file: produceStatisticPage.js:896 ~ updateIndex ~ searchArr:", searchArr)

      searchArr.map((item, index) => {
        if (item.length > 0) {
          //项目
          resDataProject.map((projectitem) => {
            let projectAbbStr = projectitem.projectAbb
            if (projectAbbStr.indexOf(item) != -1) {
              let tmp = projectitem
              reasonProjectArr.push(tmp)
            }
          })

        }
      })
      projectSelectArr = [...new Set(reasonProjectArr)]
      console.log("🚀 ~ file: produceStatisticPage.js:912 ~ updateIndex ~ projectSelectArr:", projectSelectArr)
      this.setState({
        projectSelectArr: projectSelectArr
      })
      // if (this.state.resDataProject != resDataProject) {
      //   this.setState({
      //     driversSelectArr: driversSelectArr,
      //     driverHisSelectArr: driverHisSelectArr,
      //     driverTelHisSelectArr: driverTelHisSelectArr,
      //     carnumHisSelectArr: carnumHisSelectArr,
      //     truckHisSelectArr: truckHisSelectArr,
      //   })
      // }
      //this.setState({driversSelectArr: driversSelectArr})
      // console.log("🚀 ~ file: loading.js:2207 ~ SteelCage ~ render ~ driversSelectArr:", driversSelectArr)
    } else {
      this.setState({
        projectSelectArr: resDataProject
      })
    }
  }

  dropDownBottomView = () => {
    const { focusIndex, bottomData, projectSerchText } = this.state
    return (
      <BottomSheet
        isVisible={this.state.isDropDownModalOpen}
        MAX_HEIGHT_CUS={460}
        onBackdropPress={() => {
          this.setState({ isDropDownModalOpen: !this.state.isDropDownModalOpen, bottomData: [] });
        }}
        onRequestClose={() => {
          this.setState({ isDropDownModalOpen: !this.state.isDropDownModalOpen, bottomData: [] });
        }}>
        <View>
          <View style={{ flexDirection: 'row' }}>
            <SearchBar
              platform='android'
              placeholder={'请输入搜索内容...'}
              containerStyle={{ backgroundColor: 'white', shadowColor: '#999', }}
              inputContainerStyle={{ backgroundColor: '#f9f9f9', borderRadius: 45, width: deviceWidth * 0.76 }}
              inputStyle={[{ color: '#333' }, deviceWidth <= 330 && { fontSize: 14 }]}
              round={true}
              cancelIcon={() => ({
                type: 'material',
                size: 25,
                color: 'rgba(0, 0, 0, 0.54)',
                name: 'search',
              })}
              value={projectSerchText}
              onChangeText={(text) => {
                text = text.replace(/\s+/g, "")
                text = text.replace(/<\/?.+?>/g, "")
                text = text.replace(/[\r\n]/g, "")
                this.setState({
                  projectSerchText: text
                })
              }}
              returnKeyType='search'
              onClear={() => {
              }}
              onSubmitEditing={() => {
                this.onSearchDriver(projectSerchText)
              }} />
            <Button
              containerStyle={{ width: deviceWidth * 0.2 }}
              buttonStyle={{ borderRadius: 20, width: deviceWidth * 0.16, top: 12, left: 10 }}
              title='确定'
              onPress={() => {
                this.onSearchDriver(projectSerchText)
              }} />
          </View>
          <TouchableOpacity onPress={() => {
            this.setState({
              projectId: "",
              projectSelect: "全部项目",
              projectSelectArr: this.state.resDataProject,
              isDropDownModalOpen: false,
              cardLoading: true,
              projectSerchText: '',
            }, () => {
              this.requestDataPost(this.state.dateFocusButtonName, this.state.status)
            })

          }}>
            <BottomItem backgroundColor='white' color="#333" title='全部项目' />
          </TouchableOpacity>

          {
            this.flatListSet()
          }
        </View>
      </BottomSheet>
    )
  }

  render() {
    const {
      ProductLineChart,
      ProductLineData,
      TeamChart,
      CompTypeChart,
      ProjectChart,
      status,
      dateButtonList,
      dateFocusButton,
      TeamData,
      ProjectData,
      CompTypeData,
      isLoading,
      cardLoading,
      isShowCusDate
    } = this.state
    const { navigation } = this.props;
    const badge = navigation.getParam('badge');
    const typeFocusButtontmp = badge === '生产' ? 0 : badge === '入库' ? 1 : 2;

    if (isLoading === true) {
      return (
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#F5FCFF',
          }}>
          <ActivityIndicator animating={true} color="#419FFF" size="large" />
        </View>
      );
    } else {
      return (
        <View style={styles.view}>

          <View style={{ backgroundColor: 'white' }}>

            {this.TypeButtonNew()}

            {this.CusTimeRender()}

            {this.SearchProjectBar()}

            {/* {this.TypeButton()} */}

            {this.TopCard()}

          </View>

          <ScrollView showsVerticalScrollIndicator={true} overScrollMode='never' >

            <View>
              <Text style={styles.text}>{'日' + this.state.status + '统计分析（按构件类型）'}</Text>


              {
                this.CardView('refCompType', CompTypeData)
              }


            </View>

            <View style={(status === '入库' || status === '发货') && { flex: 0, height: 0, width: 0, display: 'none' }}>
              <Text style={styles.text}>{'日' + this.state.status + '统计分析（按生产线）'}</Text>

              {
                this.CardView('refLine', ProductLineData)
              }

            </View>

            <View style={(status === '入库' || status === '发货') && { flex: 0, height: 0, width: 0, display: 'none' }}>
              <Text style={styles.text}>
                {'日' + this.state.status + '统计分析（按班组）'}
              </Text>

              {
                this.CardView('refTeam', TeamData)
              }


            </View>

            <View>
              <Text style={styles.text}>
                {'日' + this.state.status + '统计分析（按项目）'}
              </Text>

              {
                this.CardView('refProject', ProjectData)
              }

            </View>

            <View style={{ height: 30 }}></View>
          </ScrollView>

          <Suspension ref={ref => { this.Suspen = ref }} title='今日' offsetY={200} offsetX={6} updateIndex={this.updateIndex.bind(this)} isChildPage={true} dateFocusButton={this.state.dateFocusButton} >

          </Suspension>
          {this.dropDownBottomView()}
        </View>
      );
    }
  }

  _renderItem = ({ item, index }) => {
    const badge = this.state.status || this.props.navigation.getParam('badge')
    let projectAbb = ''
    if (item.projectAbb != undefined) {
      projectAbb = item.projectAbb.slice(0, 6)
    }
    if (badge == '生产') {
      return (
        <ListItem
          //title={item.productLineName + ': ' + item.proNumber + '件' + '(' + item.proVolume + 'm³)'}
          title={
            <View style={{ flexDirection: 'row' }}>
              <Text style={styles.text_list_name}>{item.productLineName || item.compTypeName || item.teamName || projectAbb || ""}</Text>
              <Text style={styles.text_list_num}>{(item.proNumber || 0) + '件'}</Text>
              <Text style={styles.text_list_vol}>{(item.proVolume || 0) + 'm³'}</Text>
            </View>
          }
          //titleStyle={styles.text_list}
          containerStyle={styles.text_list_container}
          bottomDivider
          leftElement={<Badge badgeStyle={{ backgroundColor: item.color }} />}
        />
      )
    } else if (badge == '入库') {
      return (
        <ListItem
          title={
            <View style={{ flexDirection: 'row' }}>
              <Text style={styles.text_list_name}>{item.productLineName || item.compTypeName || item.teamName || projectAbb || '加载中'}</Text>
              <Text style={styles.text_list_num}>{(item.storageNumber || 0) + '件'}</Text>
              <Text style={styles.text_list_vol}>{(item.storageVolume || 0) + 'm³'}</Text>
            </View>
          }
          titleStyle={styles.text_list}
          containerStyle={styles.text_list_container}
          bottomDivider
          leftElement={<Badge badgeStyle={{ backgroundColor: item.color }} />}
        />
      )
    } else {
      return (
        <ListItem
          title={
            <View style={{ flexDirection: 'row' }}>
              <Text style={styles.text_list_name}>{item.productLineName || item.compTypeName || item.teamName || projectAbb || '加载中'}</Text>
              <Text style={styles.text_list_num}>{(item.outNumber || 0) + '件'}</Text>
              <Text style={styles.text_list_vol}>{(item.outVolume || 0) + 'm³'}</Text>
            </View>
          }
          titleStyle={styles.text_list}
          containerStyle={styles.text_list_container}
          bottomDivider
          leftElement={<Badge badgeStyle={{ backgroundColor: item.color }} />}
        />
      )
      {/* <View style={styles.text_list_container}>
          <View style={[styles.text_list_view, { borderBottomColor: '#666666', borderBottomWidth: 0.8, fontFamily: 'STHeitiSC-Light', flexDirection: 'row' }]} >
            <View style = {{ justifyContent: 'center', alignContent: 'center', marginRight: RFT * 2}}>
              <Badge badgeStyle={{ backgroundColor: item.color }} ></Badge>
            </View>
            <Text style={styles.text_list_title}>{item.productLineName || item.compTypeName || item.teamName || item.projectAbb}</Text>
          </View>
          <View style={styles.text_list_view}>
            <Text style={styles.text_list}>{(item.outNumber || 0) + '件'}</Text>
          </View>
          <View style={styles.text_list_view}>
            <Text style={styles.text_list} >{(item.outNumber || 0) + 'm³'}</Text>
          </View>
        </View> */}

    }
  }

  flatListSet = () => {
    const { bottomData, resDataProject, projectSelectArr } = this.state
    return (
      <FlatList
        style={{ height: 345 }}
        ref={(flatList) => this._flatList2 = flatList}
        renderItem={this._renderItemLib.bind(this)}
        keyboardShouldPersistTaps='always'
        refreshing={false}
        numColumns={1}
        data={projectSelectArr}
      />
    )
  }

  _renderItemLib({ item, index }) {
    let title = item.projectAbb
    return (
      <TouchableOpacity onPress={() => {
        this.setState({
          projectId: item.projectId,
          projectSelect: item.projectAbb,
          projectSelectArr: this.state.resDataProject,
          isDropDownModalOpen: false,
          cardLoading: true,
          projectSerchText: '',
        }, () => {
          this.requestDataPost(this.state.dateFocusButtonName, this.state.status)
        })
      }}>
        <BottomItem backgroundColor='white' color="#333" title={title} />
      </TouchableOpacity>
    )
  }

  /*  _renderItem = ({ item, index }) => {
     const badge = status || this.props.navigation.getParam('badge')
     if (badge == '生产') {
       return (
         <ListItem
           title={item.productLineName + ': ' + item.proNumber} />
       )
     } else {
       return (
         <ListItem
           title={item.productLineName + ': ' + ''} />
       )
     }
   } */

}

const styles = StyleSheet.create({
  view: {
    flex: 1,
    backgroundColor: '#f9f9f9',
  },
  buttonview: {
    height: 40,
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: RFT,
    marginBottom: 4,
  },
  text: {
    color: '#535c68',
    fontSize: RFT * 3.5,
    paddingLeft: 15,
    marginTop: 10,
  },
  text_view: {
    textAlign: 'center',
    textAlignVertical: 'center',
    marginVertical: RFT * 16,
    marginHorizontal: RFT * 2,
  },
  text_TotalName: {
    fontWeight: "bold",
    //marginVertical: RFT * 2,
    //fontSize: RFT * 3.5,
    color: 'white',
    marginHorizontal: 10,
    textAlignVertical: 'center'
  },
  text_TotalValue: {
    color: 'white',
    marginHorizontal: 10,
    textAlignVertical: "bottom"
  },
  chartview_listview: {
    width: deviceWidth * 0.86,
    marginLeft: deviceWidth * 0.03,
    marginTop: deviceWidth * 0.03,
    borderRadius: 10,
    backgroundColor: 'transparent'
  },
  text_list_container: {
    backgroundColor: 'transparent',
    height: RVH * RFT * 1.63
  },
  text_list_view: {
    //flex: 1,
    marginVertical: RFT * 3,
    width: RFT * 30,
    justifyContent: 'center',
    alignContent: 'center',
    fontFamily: 'STHeitiSC-Medium'
    //borderColor: 'gray',
    //borderWidth: 1,
    //borderRadius: 20,
  },
  text_list_title: {
    fontSize: RFT * 4,
    textAlign: 'center',
    textAlignVertical: "center",
    color: '#666666',
    fontWeight: 'bold',
    marginVertical: RFT * 2
  },
  text_list: {
    fontSize: RFT * 4,
    textAlign: 'center',
    textAlignVertical: "center",
    color: '#454545',
    fontFamily: 'STHeitiSC-Medium'
    //marginEnd: 12,
    //marginVertical: 10,
    //fontFamily:'STKaiti',
    //fontWeight: 'bold',
  },
  text_list_name: {
    flex: 1.7,
    fontSize: RFT * 3.6,
    textAlign: 'center',
    fontFamily: 'STHeitiSC-Light',
  },
  text_list_num: {
    flex: 1,
    fontSize: RFT * 3.6,
    textAlign: 'center',
    fontFamily: 'STHeitiSC-Light',
  },
  text_list_vol: {
    flex: 1.4,
    fontSize: RFT * 3.6,
    textAlign: 'center',
    fontFamily: 'STHeitiSC-Light',
  },
  chartview: {
    width: deviceWidth * 0.94,
    marginTop: 10,
    marginLeft: deviceWidth * 0.03,
    backgroundColor: '#FFFFFF',
    borderRadius: 10,
    //shadowColor: '#000',
    shadowOpacity: 0.1
  },
  chart: {
    width: deviceWidth * 0.93,
    height: deviceWidth * 0.55,
  },
  dateInput: {
    borderColor: 'transparent',
    backgroundColor: 'transparent',
    //left: 15,
    zIndex: 0,
    height: 25,
  },
  dateTouchBody: {
    //borderColor: 'lightgray',
    backgroundColor: '#f8f8f8',
    borderRadius: 5,
    //borderWidth: 1,
    width: deviceWidth * 0.35,
    height: 35,
    marginTop: 5,
    paddingHorizontal: 10
    //right: -8,
    //width: de / 2.52,
    //backgroundColor: 'yellow'
  },
  dateText: {
    width: deviceWidth * 0.25,
    padding: 0,
    //textAlign: 'right',
    // marginRight: 20,
    //color: '#4D8EF5',
    //backgroundColor: 'blue'
  },
  dateDisabled: {
    textAlign: 'right',
    width: deviceWidth * 0.4,
    color: '#666'
  },
});
