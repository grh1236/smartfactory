import React from 'react'
import { View, Text, Dimensions } from 'react-native'
import { WebView } from 'react-native-webview';
import { ScrollView } from 'react-native-gesture-handler';

const { height, width } = Dimensions.get('window')

const ProcessStack = createStackNavigator({
  Process: {
    screen: webProcess,
    navigationOptions: {
      headerTitle: none
    }
  },
}
)

export default class webProcess extends React.Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title'),
    };
  };
  render() {
    let url = 'http://121.36.55.207:9100/appviews/flowcenter/index.html?s_c_bizrangeid=74e1cbc6-2cfc-4d6f-bcd1-739d28e4c74d&treeno=0181002001&s_employeeid=04c41369-8737-4151-84b6-5da37d32196e'

    console.log("render -> url", url)
    return (
      <ScrollView scrollEnabled={false} >
        <StatusBar
          barStyle='default'
          hidden={false}
          translucent = {false}
        />
        <View style={{ height: height * 0.85 }}>
          <WebView
            source={{ uri: url }}
          ></WebView>
        </View>
      </ScrollView>
    )
  }
}