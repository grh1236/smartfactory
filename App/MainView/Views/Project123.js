import React, { Component } from 'react';
import { Card, Text, Button, Overlay, Header, Icon, Badge } from 'react-native-elements'
import { View, ScrollView, Picker, processColor, ActivityIndicator, Modal, Dimensions, SafeAreaView, Platform, StyleSheet, StatusBar, TouchableWithoutFeedbackBase, TouchableWithoutFeedback, DeviceEventEmitter, PanResponder } from 'react-native';
import { TouchableNativeFeedback, TouchableOpacity } from 'react-native-gesture-handler';
import Url from '../../Url/Url';
import ProjectProcessAnalysis from '../Screen/projectProcessAnalysis';
import FlatListProjectTest from '../Components/FlatListProjectTest1';
import FlatListCount from '../Components/FlatListCount123'
//import ButtonTop from '../Components/ButtonTop';
import { RFT, RVH, deviceWidth, deviceHeight } from '../../Url/Pixal';
import ActionButton from 'react-native-action-button';
import WebView from 'react-native-webview';
import LinearGradient from 'react-native-linear-gradient';
import { NavigationEvents } from 'react-navigation';
import { MainStyles } from '../Components/MainStyles';

let c = 0;
const { height, width } = Dimensions.get('window')

let enableScrollViewScroll = true

const defaultBackIcon = () => ({
	size: 18,
	name: 'ios-back',
	color: '#419FFF',
})

export default class Project_Screen extends Component {
	constructor() {
		super();
		this.state = {
			focusButton: 0,
			buttonVisible: false,
			modalVisible: false,
			Loading: true,
			stateloading: true,
			countloading: false,
			projectName: '',
			projectAbb: '',
			Urlanalysis: '',
			Urlproject: ''//Url.url + Url.project + Url.Fid + 'AE37158119BD4502BA92485B2757248E/GetFacProductionSchedule2'
			,
			UrlCount: '' //'http://119.3.25.101:910/api/StatisticFactory/fa1119c1-ba8e-442c-b2e8-334fafbdae9e/GetStatisticFacPrjProduction/075981F601F2441C82C400C8A06AE08F',
			,
			UrlWeb: '',
			UrlProjectH5Chart: '',
			UrlProjectH5List: '',
			projectId: ' ',
			language: [{ itemValue: {} },],
			resData: [{
				"id": 1,
				"factoryId": "74e1cbc6-2cfc-4d6f-bcd1-739d28e4c74d",
				"projectId": "AE37158119BD4502BA92485B2757248E",
				"projectName": "怡安嘉园",
				"floorNoName": "1#",
				"floorNo": 1,
				"inProduced": 0,
				"notProduced": 2,
				"notInstall": 0,
				"install": 0,
				"release": 0
			},],
			title: '请点击',
			resDataButton: [{
				"factoryId": "74e1cbc6-2cfc-4d6f-bcd1-739d28e4c74d",
				"projectId": "5571FF4AEFC34F1DAFD9BDFFC2D99E7B",
				"projectName": "胜古家园",
				"projectAbb": "胜古家园",
				"city": "",
				"compCount": 892,
				"totalVolume": 758.200,
				"volumed": 39.100
			},],
			resDataCount: [{

			},],
			enableScrollViewScroll: true,
			isLoutuVisible: false,
			isLoutuLoading: false,
			isLoutuShow: true,
		}
		//this.requestData = this.requestData.bind(this);
		this.requestDataButton = this.requestDataButton.bind(this);
		//this.requestDataCount = this.requestDataCount.bind(this);
		this._panResponder = PanResponder.create({
			onStartShouldSetPanResponder: () => !enableScrollViewScroll,
			onStartShouldSetPanResponderCapture: () => !enableScrollViewScroll,
			onMoveShouldSetPanResponder: () => !enableScrollViewScroll,
			onMoveShouldSetPanResponderCapture: () => !enableScrollViewScroll,

			onPanResponderGrant: (evt, gestureState) => {
				console.log('onPanResponderGrant  ---  事件响应、开始')
				// 开始手势操作。给用户一些视觉反馈，让他们知道发生了什么事情！
				console.log("🚀 ~ file: Project.js:89 ~ Project_Screen ~ constructor ~ enableScrollViewScroll:", enableScrollViewScroll)
				if (enableScrollViewScroll == false) {
					console.log("🚀 ~ file: Project.js:89 ~ Project_Screen ~ constructor ~ enableScrollViewScroll:", enableScrollViewScroll)

					enableScrollViewScroll = true
					this.setState({
						enableScrollViewScroll: true
					})
				}

			},
			// onPanResponderRelease: (evt, gestureState) => {
			// 	// 用户放开了所有的触摸点，且此时视图已经成为了响应者。
			// 	// 一般来说这意味着一个手势操作已经成功完成。
			// 	console.log('onPanResponderRelease  ----  事件结束')
			// 	setTimeout(() => {
			// 		if (enableScrollViewScroll == true) {
			// 			enableScrollViewScroll = false
			// 			this.setState({
			// 				enableScrollViewScroll: false
			// 			})
			// 		}
			// 	}, 0)
			// 	console.log("🚀 ~ file: Project.js:89 ~ Project_Screen ~ constructor ~ enableScrollViewScroll:", enableScrollViewScroll)

			// },
			// onPanResponderTerminate: (evt, gestureState) => {
			// 	// 另一个组件已经成为了新的响应者，所以当前手势将被取消。
			// 	console.log('onPanResponderTerminate   ---- 被取消了')
			// 	setTimeout(() => {
			// 		if (enableScrollViewScroll == true) {
			// 			enableScrollViewScroll = false
			// 			this.setState({
			// 				enableScrollViewScroll: false
			// 			})
			// 		}
			// 	}, 0)
			// 	console.log("🚀 ~ file: Project.js:89 ~ Project_Screen ~ constructor ~ enableScrollViewScroll:", enableScrollViewScroll)

			// },
		})
		this._panResponderLouTu = PanResponder.create({
			onStartShouldSetPanResponder: () => this.state.enableScrollViewScroll,
			onPanResponderGrant: (evt, gestureState) => {
				console.log('_panResponderLouTu_onPanResponderGrant  ---  事件响应、开始')
				// 开始手势操作。给用户一些视觉反馈，让他们知道发生了什么事情！
				if (enableScrollViewScroll == true) {
					enableScrollViewScroll = false
					this.setState({
						enableScrollViewScroll: false
					})
				}
				console.log("🚀 ~ file: Project.js:89 ~ Project_Screen ~ constructor ~ enableScrollViewScroll:", enableScrollViewScroll)

			},
			// onPanResponderRelease: (evt, gestureState) => {
			// 	// 用户放开了所有的触摸点，且此时视图已经成为了响应者。
			// 	// 一般来说这意味着一个手势操作已经成功完成。
			// 	console.log('_panResponderLouTu_onPanResponderRelease  ----  事件结束')
			// 	if (enableScrollViewScroll == false) {
			// 		setTimeout(() => {
			// 			enableScrollViewScroll = true
			// 			this.setState({
			// 				enableScrollViewScroll: true
			// 			})
			// 		}, 3000)
			// 	}

			// 	console.log("🚀 ~ file: Project.js:89 ~ Project_Screen ~ constructor ~ enableScrollViewScroll:", enableScrollViewScroll)

			// },
			// onPanResponderTerminate: (evt, gestureState) => {
			// 	// 另一个组件已经成为了新的响应者，所以当前手势将被取消。
			// 	console.log('_panResponderLouTu_onPanResponderTerminate   ---- 被取消了')
			// 	if (enableScrollViewScroll == false) {
			// 		setTimeout(() => {
			// 			enableScrollViewScroll = true
			// 			this.setState({
			// 				enableScrollViewScroll: true
			// 			})

			// 		}, 3000)
			// 	}
			// 	console.log("🚀 ~ file: Project.js:89 ~ Project_Screen ~ constructor ~ enableScrollViewScroll:", enableScrollViewScroll)

			// },
		})
	}


	requestData = (urlprops) => {
		fetch(urlprops, {
			credentials: 'include',
		}).then(res => {
			return res.json()
		}).then(resData => {
			this.setState({
				resData: resData,
				stateloading: false,
			}, () => {
				if (typeof this.refchart != "null" && this.refchart !== null) {
					if (typeof this.refchart != 'undefined') {
						this.refchart.ReflashData(this.state.resData)
					}
				}
				//this.setState({	stateloading: false,})
			});
		}).catch(error => {
			console.log("Project_Screen -> requestData -> error", error)
			let url = this.state.Urlproject.replace(/GetFacProductionSchedule2/g, "GetFacProductionSchedule")
			fetch(url, {
				credentials: 'include',
			}).then(res => {
				return res.json()
			}).then(resData => {
				this.setState({
					resData: resData,
					stateloading: false,
				}, () => {
					if (Url.Menu.indexOf('APP形象进度图') != -1) {
						this.refchart.ReflashData(this.state.resData)
					}

					//this.setState({	stateloading: false,})
				});
			}).catch(err => {
				console.log("Project_Screen -> requestData -> err", err)
			})
		});
	}

	requestDataCount = (urlprops, signal) => {
		// if (countloading) {
		// 	controller.abort();
		// }
		if (typeof this.refcount != "null" && this.refcount !== null) {
			if (typeof this.refcount != 'undefined') {
				this.refcount.SetLoading()
			}
		}
		fetch(urlprops, {
			credentials: 'include',
			signal
		}).then(res => {
			return res.json();
		}).then(resDataCount => {
			console.log("🚀 ~ file: Project.js:245 ~ Project_Screen ~ resDataCount:", resDataCount)
			if (typeof this.refcount != "null" && this.refcount !== null) {
				if (typeof this.refcount != 'undefined') {
					console.log("🚀 ~ file: Project.js:245 ~ Project_Screen ~ this.refcount:", this.refcount)
					this.setState({
						resDataCount: resDataCount,
						countloading: false,
					});
					this.refcount._Reflash(resDataCount)
				}
			}
		}).catch((error) => {
			console.log("🚀 ~ file: Project.js:251 ~ Project_Screen ~ error:", error)
		});
	}

	requestDataAnalysis = () => {
		const { Urlanalysis } = this.state;
		console.log("🚀 ~ file: Project.js:148 ~ Project_Screen ~ Urlanalysis:", Urlanalysis)
		fetch(Urlanalysis, {
			credentials: 'include',
		}).then(res => {
			return res.json();
		}).then(resData => {
			console.log("🚀 ~ file: Project.js:154 ~ Project_Screen ~ resData:", resData)
			if (resData[0] != '登录已超时,请重新登录') {
				resData.map((item, index) => {
					nameList.push(item.compTypeName);
					preVolumeList.push(item.preVolume);
					proVolumeList.push(item.proVolume);
					storageVolumeList.push(item.storageVolume);
					outVolumeList.push(item.outVolume);
				})
				Data.Name = nameList || [];
				Data.Vol1 = preVolumeList || [];
				Data.Vol2 = proVolumeList || [];
				Data.Vol3 = storageVolumeList || [];
				Data.Vol4 = outVolumeList || []; F
			}

			this.refana.ReFlash(Data);

			this.setState({
				projectAnalysisMonth: Data,
				isLoading: false
			})
			//console.log(Data)
		}).catch((error) => {
			console.log(error);
		});
	}

	componentDidMount() {
		Url.Login();
		const route = this.props.navigation.state
		if (Url.StaticType != '2/') {
			this.setState({ isLoutuShow: true })
		} else {
			this.setState({ isLoutuShow: false })
		}
		DeviceEventEmitter.emit('route', route.routeName);
		this.requestDataButton();
		this._navListener = this.props.navigation.addListener('didFocus', () => {
			StatusBar.setBackgroundColor('black');
			StatusBar.setTranslucent(false)
			this.UpdateControl()
		});

		this.staticTypeEmitterPrj = DeviceEventEmitter.addListener('staticType', (result) => {
			console.log("🚀 ~ file: Project.js:291 ~ Project_Screen ~ this.staticTypeEmitter=DeviceEventEmitter.addListener ~ result:", result)
			if (result) {
				if (!this.state.Loading) {
					Url.StaticType = result
					this.setState({ Loading: true })
					this.setState({
						focusButton: 0
					})
					if (result != '2/') {
						this.setState({ isLoutuShow: true })
					} else {
						this.setState({ isLoutuShow: false })
					}
					this.requestDataButton()
				}
			}
		})
	}

	componentWillUnmount() {
		this._navListener.remove();
		this.staticTypeEmitterPrj.remove()
		//controller.abort();
		const controller = new AbortController();
		let { signal } = controller;
		if (this.state.countloading) {
			controller.abort();
		} else {
			signal = null
		}
	}

	requestDataButton = () => {
		const uri = Url.url + Url.factory + Url.Fid + Url.StaticType + 'GetFactoryProjectsList';
		console.log("🚀 ~ file: Project.js:339 ~ Project_Screen ~ uri:", uri)
		fetch(uri, {
			credentials: 'include',
		}).then(resButton => {
			return resButton.json()
		}).then(resDataButton => {
			if (typeof resDataButton != 'undefined') {
				this.setState({
					resDataButton: resDataButton,
					Loading: false,
					countloading: true,
					stateloading: true,
				})
				for (let i = 0; i < resDataButton.length; i++) {
					const item = resDataButton[i];
					if (item.projectState == '生产中') {
						let Urlanalysis = Url.url + Url.project + Url.Fid + item.projectId + '/' + Url.StaticType + 'GetFacProjectProcessAnaysis'
						console.log("🚀 ~ file: Project.js:354 ~ Project_Screen ~ Urlanalysis:", Urlanalysis)
						let Urlproject = Url.url + Url.project + Url.Fid + item.projectId + '/GetFacProductionSchedule2'
						let UrlCount = Url.url + Url.factory + Url.Fid + 'GetStatisticFacPrjProduction/' + Url.StaticType + item.projectId
						this.setState({
							projectId: item.projectId + '/',
							Urlanalysis: Url.url + Url.project + Url.Fid + item.projectId + '/' + Url.StaticType + 'GetFacProjectProcessAnaysis',
							Urlproject: Url.url + Url.project + Url.Fid + item.projectId + '/GetFacProductionSchedule2',
							UrlCount: Url.url + Url.factory + Url.Fid + 'GetStatisticFacPrjProduction/' + Url.StaticType + item.projectId,
							UrlWeb: Url.urlsys + '/pcis/bimbox/WebUI/BIM1_MS.html?factoryId=' + Url.Fidweb + '&' + 'projectId=' + item.projectId,
							//UrlWeb: 'http://10.100.132.80:9100' + '/pcis/bimbox/WebUI/BIM1_S.html?factoryId=' + Url.Fidweb + '&' + 'projectId=' + item.projectId,
							UrlProjectH5Chart: Url.urlsys + '/assets/projectH5/projectSectionOne.html?isapp=1&isprj=0&' + 'factoryid=' + Url.Fidweb + '&' + 'projectid=' + item.projectId,
							UrlProjectH5List: Url.urlsys + '/assets/projectH5/projectSectionTwo.html?isapp=1&isprj=0&' + 'factoryid=' + Url.Fidweb + '&' + 'projectid=' + item.projectId,
							projectName: item.projectName,
							projectAbb: item.projectAbb
						});



						if (typeof this.refana != "null" && this.refana !== null) {
							if (typeof this.refana != 'undefined') {
								this.refana.ReFlash(Urlanalysis)
							}
						}

						this.requestData(Urlproject)
						this.requestDataCount(UrlCount)
						break;
					}
				}
			}

			//console.log(this.state.resDataButton)
		}).catch(err => {
			console.log("🚀 ~ file: Project.js:195 ~ Project_Screen ~ fetch ~ err", err.toString())
			this.requestDataButtonOld()
		})
	}

	requestDataButtonOld = () => {
		const uri = Url.url + Url.factory + Url.Fid + 'GetFactoryProjects';
		console.log(uri);
		fetch(uri, {
			credentials: 'include',
		}).then(resButton => {
			return resButton.json()
		}).then(resDataButton => {
			this.setState({
				resDataButton: resDataButton,
				Loading: false,
				countloading: true,
				stateloading: true,
			}, () => {
				for (let i = 0; i < resDataButton.length; i++) {
					const item = resDataButton[i];
					if (item.projectState == '生产中') {
						this.setState({
							projectId: item.projectId + '/',
							Urlanalysis: Url.url + Url.project + Url.Fid + item.projectId + '/' + Url.StaticType + 'GetFacProjectProcessAnaysis',
							Urlproject: Url.url + Url.project + Url.Fid + item.projectId + 'GetFacProductionSchedule2',
							UrlCount: Url.url + Url.factory + Url.Fid + '/GetStatisticFacPrjProduction/' + item.projectId,
							UrlWeb: Url.urlsys + '/pcis/bimbox/WebUI/BIM1_MS.html?factoryId=' + Url.Fidweb + '&' + 'projectId=' + item.projectId,
							//UrlWeb: 'http://10.100.132.80:9100' + '/pcis/bimbox/WebUI/BIM1_S.html?factoryId=' + Url.Fidweb + '&' + 'projectId=' + item.projectId,
							UrlProjectH5Chart: Url.urlsys + '/assets/projectH5/projectSectionOne.html?isapp=1&isprj=0&' + 'factoryid=' + Url.Fidweb + '&' + 'projectid=' + item.projectId,
							UrlProjectH5List: Url.urlsys + '/assets/projectH5/projectSectionTwo.html?isapp=1&isprj=0&' + 'factoryid=' + Url.Fidweb + '&' + 'projectid=' + item.projectId,
							projectName: item.projectName,
							projectAbb: item.projectAbb
						}, () => {
							console.log("🚀 ~ file: Project.js:3392 ~ Project_Screen ~ Urlanalysis:", this.state.Urlanalysis)
							console.log("🚀 ~ file: Project.js:33922 ~ Project_Screen ~ UrlCount:", this.state.UrlCount)
							this.refana.ReFlash(this.state.Urlanalysis)
							console.log("🚀 ~ file: Project.js:390 ~ Project_Screen ~ this.state.Urlanalysis:", this.state.Urlanalysis)
							this.requestData()
							this.requestDataCount();
						});
						break;
					}
				}

			})
			//console.log(this.state.resDataButton)
		})
	}

	ButtonTop(focusButton) {
		let re = /[\u4e00-\u9fa5]/;
		let buttonWidth = 100
		return (
			<View style={{ flex: 1, marginBottom: 12, marginTop: 12 }}>
				<ScrollView>
					<View style={{ flex: 1, flexDirection: 'row', flexWrap: 'wrap' }}>
						{
							this.state.resDataButton.map((item, index) => {
								if (item.projectState == '生产中') {
									let prjLen = 0, len = 0, ENlen = 0
									for (var i = 0; i < item.projectAbb.length; i++) {
										if (re.test(item.projectAbb.charAt(i))) {
											len++;
										} else {
											ENlen++;
										}
									}

									prjLen = len * 1.5 + ENlen
									if (prjLen > 9) {
										buttonWidth = 150
									}
									if (prjLen > 5 && prjLen <= 9) {
										buttonWidth = 125
									}
									if (prjLen > 0 && prjLen <= 5) {
										buttonWidth = 90
									}
									return (
										<Button
											//key={index}
											buttonStyle={{
												borderRadius: 20,
												paddingHorizontal: 10,
												width: buttonWidth,
												//flex: 1,
												backgroundColor: focusButton == index ? '#419FFF' : 'white'
											}}
											containerStyle={{
												//flex: 1,
												borderRadius: 20,
												marginTop: 6,
												marginHorizontal: 6,
											}}
											title={item.projectAbb}
											titleStyle={{ fontSize: RFT * 3.5, color: focusButton == index ? 'white' : '#419FFF' }}
											type='outline'
											onPress={
												() => {
													this.setState({
														focusButton: index,
														countloading: true,
														stateloading: false,
														buttonVisible: !this.state.buttonVisible,
														projectId: item.projectId + '/',
														Urlanalysis: Url.url + Url.project + Url.Fid + item.projectId + '/' + Url.StaticType + 'GetFacProjectProcessAnaysis',
														Urlproject: Url.url + Url.project + Url.Fid + item.projectId + Url.StaticType + 'GetFacProductionSchedule2',
														UrlCount: Url.url + Url.factory + Url.Fid + '/GetStatisticFacPrjProduction/' + item.projectId,
														UrlWeb: Url.urlsys + '/pcis/bimbox/WebUI/BIM1_MS.html?factoryId=' + Url.Fidweb + '&' + 'projectId=' + item.projectId,
														//UrlWeb: 'http://10.100.132.80:9100' + '/pcis/bimbox/WebUI/BIM1_S.html?factoryId=' + Url.Fidweb + '&' + 'projectId=' + item.projectId,
														UrlProjectH5Chart: Url.urlsys + '/assets/projectH5/projectSectionOne.html?isapp=1&isprj=0&' + 'factoryid=' + Url.Fidweb + '&' + 'projectid=' + item.projectId,
														UrlProjectH5List: Url.urlsys + '/assets/projectH5/projectSectionTwo.html?isapp=1&isprj=0&' + 'factoryid=' + Url.Fidweb + '&' + 'projectid=' + item.projectId,
														projectName: item.projectName,
														projectAbb: item.projectAbb
													});
													let Urlanalysis = Url.url + Url.project + Url.Fid + item.projectId + '/' + Url.StaticType + 'GetFacProjectProcessAnaysis'
													let Urlproject = Url.url + Url.project + Url.Fid + item.projectId + '/GetFacProductionSchedule2'
													let UrlCount = Url.url + Url.factory + Url.Fid + 'GetStatisticFacPrjProduction/' + Url.StaticType + item.projectId

													const controller = new AbortController();
													let { signal } = controller;
													if (this.state.countloading) {
														controller.abort();
													} else {
														signal = null
													}
													if (typeof this.refana != "null" && this.refana !== null) {
														if (typeof this.refana != 'undefined') {
															this.refana.ReFlash(Urlanalysis)
														}
													}
													this.requestData(Urlproject);
													this.requestDataCount(UrlCount, signal);
												}

											}
										/>
									)
								}
							})
						}
					</View>
				</ScrollView>
			</View>
		);
	}

	ButtonTopScreen = () => {
		const { focusButton, resDataButton } = this.state;
		let re = /[\u4e00-\u9fa5]/;
		let buttonWidth = 100

		return (
			resDataButton.map((item, index) => {
				if (item.projectState == '生产中') {
					let prjLen = 0, len = 0, ENlen = 0
					for (var i = 0; i < item.projectAbb.length; i++) {
						if (re.test(item.projectAbb.charAt(i))) {
							len++;
						} else {
							ENlen++;
						}
					}

					prjLen = len * 1.5 + ENlen
					if (prjLen > 9) {
						buttonWidth = 150
					}
					if (prjLen > 5 && prjLen <= 9) {
						buttonWidth = 125
					}
					if (prjLen > 0 && prjLen <= 5) {
						buttonWidth = 90
					}
					return (
						<Button
							//key={index}
							buttonStyle={{
								borderRadius: 20,
								justifyContent: 'flex-start',
								alignContent: 'flex-start',
								width: buttonWidth,
								backgroundColor: focusButton == index ? 'white' : 'rgba(255,255,255,0.3)'
							}}
							containerStyle={{
								justifyContent: 'flex-start',
								alignContent: 'flex-start',
								marginHorizontal: 6,
								borderRadius: 20,
								marginTop: 20,
							}}

							title={item.projectAbb}
							titleProps={{ numberOfLines: 1 }}
							titleStyle={{ fontSize: RFT * 3.5, color: focusButton == index ? 'black' : 'white', opacity: 1, marginLeft: 6, textAlign: 'left' }}
							type='outline'
							iconContainerStyle={{ marginRight: 10 }}
							icon={<Badge containerStyle={{ marginLeft: 6 }} badgeStyle={{ backgroundColor: focusButton == index ? "#419fff" : "white" }} />}
							onPress={() => {
								Url.Login()
								this.setState({
									focusButton: index,
									countloading: true,
									stateloading: true,
									projectId: item.projectId + '/',
									Urlanalysis: Url.url + Url.project + Url.Fid + item.projectId + '/' + Url.StaticType + 'GetFacProjectProcessAnaysis',
									Urlproject: Url.url + Url.project + Url.Fid + item.projectId + 'GetFacProductionSchedule2',
									UrlCount: Url.url + Url.factory + Url.Fid + '/GetStatisticFacPrjProduction/' + Url.StaticType + item.projectId,
									//UrlWeb: 'http://10.100.132.80:9100' + '/pcis/bimbox/WebUI/BIM1_S.html?factoryId=' + Url.Fidweb + '&' + 'projectId=' + item.projectId,
									UrlWeb: Url.urlsys + '/pcis/bimbox/WebUI/BIM1_MS.html?factoryId=' + Url.Fidweb + '&' + 'projectId=' + item.projectId,
									UrlProjectH5Chart: Url.urlsys + '/assets/projectH5/projectSectionOne.html?isapp=1&isprj=0&' + 'factoryid=' + Url.Fidweb + '&' + 'projectid=' + item.projectId + '&t=1',
									UrlProjectH5List: Url.urlsys + '/assets/projectH5/projectSectionTwo.html?isapp=1&isprj=0&' + 'factoryid=' + Url.Fidweb + '&' + 'projectid=' + item.projectId + '&t=1',
									projectName: item.projectName,
									projectAbb: item.projectAbb
								});
								let Urlanalysis = Url.url + Url.project + Url.Fid + item.projectId + '/' + Url.StaticType + 'GetFacProjectProcessAnaysis'
								let Urlproject = Url.url + Url.project + Url.Fid + item.projectId + '/GetFacProductionSchedule2'
								let UrlCount = Url.url + Url.factory + Url.Fid + 'GetStatisticFacPrjProduction/' + Url.StaticType + item.projectId

								const controller = new AbortController();
								let { signal } = controller;
								if (this.state.countloading) {
									controller.abort();
								} else {
									signal = null
								}

								if (typeof this.refana != "null" && this.refana !== null) {
									if (typeof this.refana != 'undefined') {
										this.refana.ReFlash(Urlanalysis)
									}
								}
								this.requestData(Urlproject);
								this.requestDataCount(UrlCount, signal);
							}}
						/>
					)
				}
			})
		)
	}

	UpdateControl = () => {
		Url.Login();
		const route = this.props.navigation.state
		console.log("🚀 ~ file: Daily.js ~ line 472 ~ DailyScreen ~ render ~ route", route.routeName)
		DeviceEventEmitter.emit('route', route.routeName);

	}

	render() {
		const { focusButton, Loading } = this.state;
		const route = this.props.navigation.state
		if (Loading) {
			return (
				<View style={{
					flex: 1,
					flexDirection: 'row',
					justifyContent: 'center',
					alignItems: 'center',
					backgroundColor: '#F5FCFF',
				}}>
					<ActivityIndicator
						animating={true}
						color='#419FFF'
						size="large" />
				</View>
			)
		} else {
			return (
				<View>
					<NavigationEvents
						onWillFocus={this.UpdateControl} />

					<ScrollView
						style={{ backgroundColor: '#F9F9F9', marginBottom: -120 }}
						scrollEnabled={this.state.enableScrollViewScroll}
						overScrollMode='never'
						nestedScrollEnabled={true}

						/* onTouchEnd={(event) => {
							console.log("🚀 ~ file: Project.js ~ line 374 ~ Project_Screen ~ ScrollView ~ onTouchEnd", event)
							this.setState({ enableScrollViewScroll: true })
						}} */
						/* onMoveShouldSetResponderCapture={() => {
							this.setState({ enableScrollViewScroll: true })
						}} */>

						<View style={{ height: 150 }}
						>
							<LinearGradient
								start={{ x: 1, y: 1 }} end={{ x: 0, y: 1 }}
								colors={['rgba(65,216,255,0.88)', '#419FFF']}
								style={{ width: width, height: width * 0.45, }}
							>
							</LinearGradient>
						</View>

						<View style={{ top: -160, }}
						>
							<View style={{ flexDirection: 'row', marginBottom: 13 }}>
								<ScrollView scrollEnabled={true} horizontal={true} showsHorizontalScrollIndicator={false}>
									{this.ButtonTopScreen()}
								</ScrollView>
								<Button
									type='outline'
									title='更多'
									titleStyle={{ fontSize: RFT * 3.5, marginLeft: 6, textAlign: 'left', color: '#333' }}
									buttonStyle={{
										borderRadius: 100,
										backgroundColor: "white",
										justifyContent: 'flex-start',
										alignContent: 'flex-start',
										width: 70,
										flex: 1
									}}
									containerStyle={{
										//marginHorizontal: 6,
										borderRadius: 100,
										marginTop: 20,
									}}
									iconContainerStyle={{ marginRight: 10 }}
									icon={<Badge containerStyle={{ marginLeft: 6 }} badgeStyle={{ backgroundColor: "#419fff" }} />}

									onPress={() => {
										this.setState({ buttonVisible: !this.state.buttonVisible })
									}} />
							</View>
							<Overlay
								fullScreen={true}
								animationType='fade'
								isVisible={this.state.buttonVisible}
								onRequestClose={() => {
									this.setState({ buttonVisible: !this.state.buttonVisible });
								}}>
								<Header
									leftComponent={
										Platform.OS === 'ios' ?
											<RefreshIcon
												onPress={this.requestDataButton.bind(this)} /> :
											null
									}
									centerComponent={{ text: '项目选择表', style: { color: 'gray', fontSize: 20 } }}
									rightComponent={
										<TouchableOpacity
										// style={{ backgroundColor: 'red', width: 40, height: 40 }}
										// onPress={() => {
										// 	console.log("🚀 ~ file: Project.js:708 ~ Project_Screen ~ render ~ onPress:")
										// 	this.setState({
										// 		buttonVisible: false
										// 	});
										// }}
										>
											<BackIcon
											/>
										</TouchableOpacity>

									}
									backgroundColor='white'
								/>
								{this.ButtonTop(focusButton)}
							</Overlay>

							{Url.Menu.indexOf('APP形象进度图') != -1 ?
								<View style={[MainStyles.chartContainerView, { height: width * 0.61 }]}>
									<View style={MainStyles.chartView}>
										<Text style={MainStyles.chartTitle}>{this.state.projectAbb + '工程整体进度'}</Text>
										<View style={MainStyles.chart}>
											<FlatListProjectTest
												ref={refchart => { this.refchart = refchart }}
												Data={this.state.resDataCount}
												stateloading={this.state.stateloading}
												url={this.state.Urlproject} />
										</View>
									</View>
								</View> : <View ></View>}

							<View style={Url.Menu.indexOf('APP项目进度分析') == -1 && { flex: 0, height: 0, width: 0 }}>
								<View style={[MainStyles.chartView, { height: deviceWidth * 0.61, }]}>
									<Text style={MainStyles.chartTitle}>{this.state.projectAbb + '项目进度分析'}</Text>
									<View style={MainStyles.chart}>
										<ProjectProcessAnalysis
											ref={refana => { this.refana = refana }} />
									</View>
								</View>
							</View>


							{
								Url.Menu.indexOf('APP项目进度图') != -1 ?
									this.state.isLoutuShow ?
										<View >
											<View ><Text style={styles.text}>形象进度图</Text></View>
											<View style={{ width: width * 0.94, marginLeft: width * 0.03, height: deviceHeight * 1.35, backgroundColor: '#FFFFFF', borderRadius: 10 }}>
												{/* <Text style={{ fontSize: RFT * 3.6, margin: 10, margin: 10, color: 'black' }}>{this.state.projectAbb + '工程整体进度'}</Text> */}
												<View style={{ height: deviceHeight * 0.7, width: width * 0.94 }} >
													<TouchableWithoutFeedback

														onPress={() => {
															this.setState({
																isLoutuVisible: true,
																isLoutuLoading: true
															})
															setTimeout(() => {
																this.setState({ isLoutuLoading: false })
															}, 3000)
														}}
													>
														<WebView
															//{...this._panResponderLouTu.panHandlers}
															injectedJavaScript={`
													(function () {
															function changeHeight() {
																let height = 0;
																if (document.documentElement && (document.documentElement.scrollHeight)) {
																	height = document.documentElement.scrollHeight;
																} else if (document.body && (document.body.scrollHeight)) {
																	height = document.body.scrollHeight;
																}
																// window.ReactNativeWebView.postMessage(JSON.stringify({
																// 	type: 'setHeight',
																// 	height: height,
																// }))
																window.ReactNativeWebView.postMessage("setHeight=" + height )
															}
															setTimeout(changeHeight, 300);
													} ())
												`}
															source={{ uri: this.state.UrlProjectH5Chart }}
															// onResponderMove={() => {
															// 	//enableScrollViewScroll = false
															// 	this.setState({ enableScrollViewScroll: false })
															// }}
															// onTouchStart={() => {
															// 	console.log("🚀 ~ file: Project.js:625 ~ Project_Screen ~ render ~ onTouchStart:")
															// 	//enableScrollViewScroll = false
															// 	if (this.state.enableScrollViewScroll == true) {
															// 		this.setState({ enableScrollViewScroll: false })
															// 	}

															// }}
															// onTouchMove={() => {
															// 	console.log("🚀 ~ file: Project.js:625 ~ Project_Screen ~ render ~ onTouchMove:")
															// 	//enableScrollViewScroll = false
															// 	if (this.state.enableScrollViewScroll == true) {
															// 		this.setState({ enableScrollViewScroll: false })
															// 	}
															// }}
															// onTouchEnd={() => {
															// 	console.log("🚀 ~ file: Project.js:625 ~ Project_Screen ~ render ~ onTouchEnd:")
															// 	//enableScrollViewScroll = true

															// }}
															// onTouchCancel={() => {
															// 	//enableScrollViewScroll = true
															// 	this.setState({ enableScrollViewScroll: true })
															// }}
															onMessage={(e) => {
																console.log("🚀 ~ file: Project.js ~ line 514 ~ Project_Screen ~ render ~ e.nativeEvent", e.nativeEvent)
																console.log("🚀 ~ file: Project.js ~ line 514 ~ Project_Screen ~ render ~ e.nativeEvent.data", e.nativeEvent.data)
																if (e.nativeEvent.data.indexOf("setHeight") != -1) {
																	return
																}
																let dataArr = e.nativeEvent.data.split('&')
																console.log("🚀 ~ file: Project.js:576 ~ Project_Screen ~ render ~ dataArr:", dataArr)

																let project = dataArr[1].split('=')
																let projectId = project[1]
																let floorNoIdArr = dataArr[2].split('=')
																let floorNoId = floorNoIdArr[1]
																let floorIdArr = dataArr[3].split('=')
																let floorId = floorIdArr[1]
																let compTypeIdArr = dataArr[4].split('=')
																let compTypeId = compTypeIdArr[1]
																let compstateArr = dataArr[5].split('=')
																let compState = compstateArr[1]
																let floorNoNameArr = dataArr[6].split('=')
																let floorNoName = floorNoNameArr[1]
																let floorNameArr = dataArr[8].split('=')
																let floorName = floorNameArr[1]
																let compTypeNameArr = dataArr[7].split('=')
																let compTypeName = compTypeNameArr[1]

																this.props.navigation.navigate('ChildOneNew', {
																	title: '构件明细',
																	sonTitle: "构件详情",
																	url: Url.url + Url.Produce + 'PostProduceCompDetail',
																	sonUrl: '',
																	projectId: projectId,
																	floorNoId: floorNoId,
																	floorNoName: floorNoName,
																	floorId: floorId,
																	floorName: floorName,
																	compTypeId: compTypeId,
																	compTypeName: compTypeName,
																	compState: compState
																})
															}}
														></WebView>
													</TouchableWithoutFeedback>


												</View>
												<View style={{ height: deviceHeight * 0.65, width: width * 0.94 }}>
													<WebView
														injectedJavaScript={`
												(function () {
														function changeHeight() {
															let height = 0;
															if (document.documentElement && (document.documentElement.scrollHeight)) {
																height = document.documentElement.scrollHeight;
															} else if (document.body && (document.body.scrollHeight)) {
																height = document.body.scrollHeight;
															}
															// window.ReactNativeWebView.postMessage(JSON.stringify({
															// 	type: 'setHeight',
															// 	height: height,
															// }))
															window.ReactNativeWebView.postMessage("setHeight=" + height )
														}
														setTimeout(changeHeight, 300);
												} ())
											`}
														onMessage={(e) => {
															console.log("🚀 ~ file: Project.js ~ line 519 ~ Project_Screen ~ render ~ e.nativeEvent", e.nativeEvent)
															console.log("🚀 ~ file: Project.js ~ line 519 ~ Project_Screen ~ render ~ e.nativeEvent.data", e.nativeEvent.data)
															if (e.nativeEvent.data.indexOf("setHeight") != -1) {
																return
															}
														}}
														source={{ uri: this.state.UrlProjectH5List }
														}
													></WebView>
												</View>
											</View>
										</View> : <View></View> : <View></View>
							}




							{/* <View style={Url.Menu.indexOf('APP形象进度图') == -1 && { flex: 0, height: 0, width: 0 }}>
							<Text style={styles.text}>形象进度图</Text>
							<View style={{ width: width * 0.94, marginLeft: width * 0.03, height: deviceWidth, backgroundColor: '#FFFFFF', borderRadius: 10 }}>
								<Text style={{ fontSize: RFT * 3.6, margin: 10, margin: 10, color: 'black' }}>{this.state.projectAbb + '工程整体进度'}</Text>
								<View style={{ height: deviceWidth * 0.9, width: width * 0.94 }}>
									<FlatListProjectTest
										ref={refchart => { this.refchart = refchart }}
										Data={this.state.resDataCount}
										stateloading={this.state.stateloading}
										url={this.state.Urlproject} />
								</View>
							</View>
						</View>
 */}

							<View style={(Url.Menu.indexOf('APP项目生产统计') == -1) && (!this.state.isLoutuShow && { flex: 0, height: 0, width: 0 })}>
								<Text style={styles.text}>项目生产统计</Text>
								<View >
									<View style={{ marginTop: 4 }}>
										<FlatListCount
											navigation={this.props.navigation}
											ref={refcount => { this.refcount = refcount }} />
									</View>
								</View>
							</View>
						</View>


					</ScrollView>

					{
						Url.Menu.indexOf('APP模型') != -1 ?
							<ActionButton
								buttonColor="#419FFF"
								onPress={() => {
									console.log("🚀 ~ file: Project.js:621 ~ Project_Screen ~ render ~ this.state.UrlWeb", this.state.UrlWeb)

									this.props.navigation.navigate('Web', {
										title: '项目模型页面',
										url: this.state.UrlWeb
									})
								}}
								renderIcon={() => (<View style={styles.actionButtonView}><Icon type='font-awesome' name='building' color='#ffffff' />
									<Text style={styles.actionButtonText}>模型</Text>
								</View>)}
							/> : <View></View>
					}

					<Overlay
						overlayStyle={{ width: deviceWidth, height: deviceHeight * 0.7 }}
						isVisible={this.state.isLoutuVisible}
						onRequestClose={() => { this.setState({ isLoutuVisible: false }) }}
						onBackdropPress={() => { this.setState({ isLoutuVisible: false }) }}
					>
						<View style={{ flex: 1 }}>
							{
								<WebView
									//{...this._panResponderLouTu.panHandlers}
									onLoadStart={(e) => {
										console.log("🚀 ~ file: Project.js:943 ~ Project_Screen ~ render ~ e:", e)
										setTimeout(() => {
											this.setState({ isLoutuLoading: false })
										}, 1000)
									}}
									onLoadProgress={(e) => {
										console.log("🚀 ~ file: Project.js:945 ~ Project_Screen ~ render ~ e:", e)
									}}
									onLoad={(e) => {
										console.log("🚀 ~ file: Project.js:940 ~ Project_Screen ~ render ~ e:", e)
									}}
									onLoadEnd={(e) => {
										console.log("🚀 ~ file: Project.js:947 ~ Project_Screen ~ render ~ e:", e)
										this.setState({ isLoutuLoading: false })
									}}
									source={{ uri: this.state.UrlProjectH5Chart }}
									onMessage={(e) => {
										console.log("🚀 ~ file: Project.js ~ line 514 ~ Project_Screen ~ render ~ e.nativeEvent", e.nativeEvent)
										console.log("🚀 ~ file: Project.js ~ line 514 ~ Project_Screen ~ render ~ e.nativeEvent.data", e.nativeEvent.data)
										let dataArr = e.nativeEvent.data.split('&')
										console.log("🚀 ~ file: Project.js:576 ~ Project_Screen ~ render ~ dataArr:", dataArr)

										let project = dataArr[1].split('=')
										let projectId = project[1]
										let floorNoIdArr = dataArr[2].split('=')
										let floorNoId = floorNoIdArr[1]
										let floorIdArr = dataArr[3].split('=')
										let floorId = floorIdArr[1]
										let compTypeIdArr = dataArr[4].split('=')
										let compTypeId = compTypeIdArr[1]
										let compstateArr = dataArr[5].split('=')
										let compState = compstateArr[1]
										let floorNoNameArr = dataArr[6].split('=')
										let floorNoName = floorNoNameArr[1]
										let floorNameArr = dataArr[8].split('=')
										let floorName = floorNameArr[1]
										let compTypeNameArr = dataArr[7].split('=')
										let compTypeName = compTypeNameArr[1]

										this.props.navigation.navigate('ChildOneNew', {
											title: '构件明细',
											sonTitle: "构件详情",
											url: Url.url + Url.Produce + 'PostProduceCompDetail',
											sonUrl: '',
											projectId: projectId,
											floorNoId: floorNoId,
											floorNoName: floorNoName,
											floorId: floorId,
											floorName: floorName,
											compTypeId: compTypeId,
											compTypeName: compTypeName,
											compState: compState
										})
										this.setState({ isLoutuVisible: false })
									}}
								></WebView>
							}

						</View>
					</Overlay>

				</View>

			);
		}
	}
}

const styles = StyleSheet.create({
	text: {
		fontSize: RFT * 4,
		marginTop: 10,
		marginBottom: 10,
		color: '#535c68',
		marginLeft: width * 0.04
	},
	actionButtonIcon: {
		fontSize: 20,
		height: 22,
		color: 'white',
	},
	actionButtonText: {
		color: 'white',
		fontSize: 14,
	}
})

class BackIcon extends React.Component {
	render() {
		return (
			<Icon
				onPress={this.props.onPress}
				name='arrow-back'
				color='#419FFF' />
		)
	}
}

class RefreshIcon extends React.Component {
	render() {
		return (
			<TouchableOpacity
				onPress={this.props.onPress} >
				<Icon
					type='simple-line-icon'
					name='refresh'
					color='#419FFF' />
			</TouchableOpacity>
		)
	}
}


