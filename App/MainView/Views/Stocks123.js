import React from 'react';
import { View, processColor, Dimensions, ActivityIndicator, ScrollView, Picker, PickerIOS, Platform, StyleSheet, StatusBar, DeviceEventEmitter } from 'react-native';
import { Text, Button, Card, Input, Overlay, Icon } from 'react-native-elements';
import { PieChart } from 'react-native-charts-wrapper';
import { Echarts, echarts } from 'react-native-secharts';
import PieChatStock from '../Components/PieChartCommon';
import Url from '../../Url/Url';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import ActionButton from 'react-native-action-button';
import { RFT, RVH, deviceWidth } from '../../Url/Pixal';
import LinearGradient from 'react-native-linear-gradient';
import { MainStyles } from '../Components/MainStyles';

const { height, width } = Dimensions.get('window');
let stockNameData = [], stockName = '', stockData = [];


export default class _StockScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      description: {
        text: ' ',
        textSize: 15,
        textColor: processColor('darkgray'),
      },
      isVisible: false,
      pickerValue: '请选择',
      isLoading: true,
      resData: [{
        "id": 1,
        "factoryId": "a9d6c924-d300-4335-a77d-4a5dce4c28fb",
        "stockType": "TheoryTotalStock",
        "compNum": 8000,
        "compVolume": 0.000
      },
      {
        "id": 2,
        "factoryId": "a9d6c924-d300-4335-a77d-4a5dce4c28fb",
        "stockType": "UsedStock",
        "compNum": 7262,
        "compVolume": 5082.688
      }],
      resDataVisualized: [],
      projectNameList: [],
      resDataProject: [],
      numList: [],
      Totol: {},
    };
    this.request = this.request.bind(this);
    this.resDataStockVisualized = this.resDataStockVisualized.bind(this);
    this.requestProData = this.requestProData.bind(this)
    this._renderItem = this._renderItem.bind(this);
  }

  handleSelect(event) {
    let entry = event.nativeEvent
    if (entry == null) {
      this.setState({ ...this.state, selectedEntry: null })
    } else {
      this.setState({ ...this.state, selectedEntry: JSON.stringify(entry) })
    }

    console.log(event.nativeEvent)
  }
  componentWillMount() {

  }

  componentDidMount() {

    this.StaticTypeSwitch()

    this._navListener = this.props.navigation.addListener('didFocus', () => {
      StatusBar.setBackgroundColor('black');
      StatusBar.setTranslucent(false)
      this.UpdateControl()
    });
    this.staticTypeEmitterSot = DeviceEventEmitter.addListener('staticType', (result) => {
      if (result) {
        this.StaticTypeSwitch(result)

      }
    })
  }

  UpdateControl = () => {
    const route = this.props.navigation.state
    console.log("🚀 ~ file: Daily.js ~ line 472 ~ DailyScreen ~ render ~ route", route.routeName)
    DeviceEventEmitter.emit('route', route.routeName);
  }

  componentWillUnmount() {
    this._navListener.remove();
    this.staticTypeEmitterSot.remove()
  }

  StaticTypeSwitch = (result) => {
    if (typeof result != 'undefined') {
      Url.StaticType = result
    }
    this.setState({ isLoading: true })
    this.request();
    this.resDataStockVisualized();
    if (Url.StaticType == '0/') {
      this.requestProData();
    }
    if (Url.StaticType == '1/') {
      this.requestProData(Url.url + Url.factory + Url.Fid + Url.StaticType + 'GetDuctPieceFacProjStockSaturation');
    }
    if (Url.StaticType == '2/') {
      this.requestProData(Url.url + Url.factory + Url.Fid + Url.StaticType + 'GetDuctPieceFacProjStockSaturation');
    }
  }

  request = () => {
    let StockSaturation = []
    const urlnew = Url.url + Url.factory + Url.Fid + Url.StaticType + 'GetFactoryStockSaturation';
    console.log("🚀 ~ file: Stocks.js:101 ~ _StockScreen ~ urlnew:", urlnew)
    fetch(urlnew, {
      credentials: 'include',
    }).then(res => {
      return res.json()
    }).then(resData => {
      let used = {}, unused = {}
      if (resData[1]) {
        used.value = resData[1].compNum || 0
        used.name = '已用库存'
        StockSaturation.push(used)
        unused.value = resData[0].compNum - (resData[1].compNum || 0)
        unused.name = '剩余库存'
        StockSaturation.push(unused)
        this.setState({
          resData: StockSaturation,
          Totol: resData[0]
          // isLoading: false
        })
      } else {
        used.value = 0
        used.name = '已用库存'
        StockSaturation.push(used)
        unused.value = resData[0].compNum
        unused.name = '剩余库存'
        StockSaturation.push(unused)
        this.setState({
          resData: StockSaturation,
          Totol: resData[0]
          // isLoading: false
        })
      }
      //const numListnew = JSON.parse(JSON.stringify(this.state.numList).replace(/name/g, "label")); //改名语句

      //console.log(this.state.numList)
    }).catch((error) => {
      console.log(error);
    });
  }

  requestProData = (urlProps) => {
    let url = Url.url + Url.factory + Url.Fid + 'GetFacProjStockSaturation'
    if (typeof urlProps != "undefined") {
      url = urlProps
    }
    console.log("🚀 ~ file: Stocks.js:147 ~ _StockScreen ~ url:", url)
    fetch(url, {
      credentials: 'include',
    }).then(res => {
      return res.json()
    }).then(resDataPro => {
      this.setState({
        resDataProject: resDataPro,
        projectNameList: resDataPro.projectNameList,
        numList: resDataPro.numList,
        isLoading: false,
      })
      //const numListnew = JSON.parse(JSON.stringify(this.state.numList).replace(/name/g, "label")); //改名语句
      //this.setState({ numList: numListnew })

      console.log(this.state.resDataProject)
    }).catch((error) => {
      console.log(error);
    });
  }

  resDataStockVisualized = () => {
    const url = Url.url + 'Stock/' + Url.Fid + Url.StaticType + 'GetStockVisualized';
    console.log("🚀 ~ file: Stocks.js:159 ~ _StockScreen ~ url", url)
    fetch(url, {
      credentials: 'include',
    }).then(resVisualized => {
      return resVisualized.json()
    }).then(resDataVisualized => {
      this.setState({
        resDataVisualized: resDataVisualized,
      })
      console.log(this.state.resDataVisualized)
      //const numListnew = JSON.parse(JSON.stringify(this.state.numList).replace(/name/g, "label")); //改名语句
    })
  }

  render() {
    const { resData, resDataVisualized, pickerValue, Totol } = this.state;
    console.log(this.state.resData)
    console.log(this.state.resDataProject)
    let i = 0, length = resDataVisualized.length;
    this.state.resDataVisualized.map((item, index) => {
      if (stockNameData.indexOf(item.roomName) == -1) {
        stockNameData[i++] = item.roomName;
      }
    });
    let initialValue = stockNameData[0];
    stockNameData.length = length;
    i = 0;
    //console.log(initialValue)
    if (this.state.isLoading) {
      return (
        <View style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#F5FCFF',
        }}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
    } else {
      return (
        <View>
          <ScrollView style={{ backgroundColor: '#F9F9F9', marginBottom: -220 }}>

            <View style={{ height: 150 }}>
              <LinearGradient
                start={{ x: 1, y: 1 }} end={{ x: 0, y: 1 }}
                colors={['rgba(16,237,195,0.88)', '#15D639']}
                style={{ width: width, height: width * 0.36, }}
              >
              </LinearGradient>
            </View>

            <View style={{ top: -140, }}>
              {
                Url.Menu.indexOf('APP成品库存饱和度分析-项目') == -1 ? <View></View> :
                  <View style={{ height: width * 0.6 }}>
                    <View style={MainStyles.chartView}>
                      <Text style={MainStyles.chartTitle}>成品库存饱和度分析-项目</Text>
                      <View style={MainStyles.chart}>
                        <Echarts
                          height={deviceWidth * 0.48}
                          width={width}
                          option={{
                            tooltip: {
                              show: true,
                              trigger: 'item',
                              formatter: "{a} <br/>{b}: {c}件 ({d}%)"
                            },
                            legend: {
                              type: 'scroll',
                              orient: 'vertical',
                              x: 'left',
                              textStyle: {
                                fontSize: RFT * 2.6,
                              },
                              itemWidth: RFT * 2.4,
                              itemHeight: RFT * 2.4,
                              data: this.state.projectNameList
                            },
                            color: ['#419FFF', '#41CCFF', '#FF9D18', '#FFFf37', '#8579E8', '#9b59b6', '#27ae60', '#2ecc71', '#c0392b', '#e74c3c', '#f39c12', '#f1c40f'
                            ],
                            series: [
                              {
                                name: '项目名：',
                                type: 'pie',
                                radius: ['50%', '70%'],
                                center: ['59%', '50%'],
                                avoidLabelOverlap: false,
                                label: {
                                  normal: {
                                    show: false,
                                    position: 'center',
                                    textStyle: {
                                      fontSize: RFT * 3.5,
                                      color: '#535c68'
                                    },
                                    formatter: '{b}:' + '{d}%'
                                  },
                                  emphasis: {
                                    show: true,
                                    textStyle: {
                                      fontSize: RFT * 3.5,
                                      fontWeight: 'bold'
                                    }
                                  }
                                },
                                labelLine: {
                                  normal: {
                                    lineStyle: {
                                      show: false,
                                      color: '#235894'
                                    }
                                  }
                                },
                                data: this.state.numList
                              }
                            ]
                          }} />
                        {/* <PieChatStock
                      legend={{
                        enabled: true,
                        textSize: RFT * 2.5,
                        form: 'CIRCLE',
                        wordWrapEnabled: true,
                        xEntrySpace: 13,
                        yEntrySpace: 6,
                      }}
                      data={{
                        dataSets: [{
                          values: this.state.numList,
                          label: ' ',
                          config: {
                            colors: [processColor('#419FFF'), processColor('#41CCFF'), processColor('#FF9D18'), processColor('#FFFf37'), processColor('#8579E8'), processColor('#9b59b6'), processColor('#27ae60'), processColor('#2ecc71'), processColor('#c0392b'), processColor('#e74c3c'), processColor('#f39c12'), processColor('#f1c40f')],
                            valueTextSize: 1,
                            valueTextColor: processColor('transparent'),
                            sliceSpace: 5,
                            selectionShift: 13,
                            //xValuePosition: "OUTSIDE_SLICE",
                            //yValuePosition: "OUTSIDE_SLICE",
                            valueFormatter: "#.#'%'",
                            valueLineColor: processColor('transparent'),
                            valueLinePart1Length: 0.01,
                            valueLineVariableLength: false,
                          }
                        }],
                      }}
                      onSelect={this.handleSelect.bind(this)}
                      onChange={(event) => console.log(event.nativeEvent)}
                      marker={{
                        enabled: true,
                        markerColor: processColor('#999999'),
                        textColor: processColor('white'),
                        markerFontSize: 14,
                      }}
                      style={{ flex: 1 }}
                    /> */}
                      </View>
                    </View>
                  </View>

              }

              {
                Url.Menu.indexOf('APP成品库存饱和度分析-库存') != -1 ?
                  <View style={{ height: deviceWidth * 0.57, marginTop: 10 }}>
                    <View style={MainStyles.chartView}>
                      <Text style={MainStyles.chartTitle}>成品库存饱和度分析-库存</Text>
                      <View style={MainStyles.chart}>
                        <Echarts
                          height={width * 0.48}
                          width={width * 0.9}
                          option={{
                            tooltip: {
                              show: true,
                              trigger: 'item',
                              formatter: "{a} <br/>{b}: {c}件 ({d}%)"
                            },
                            legend: {
                              type: 'scroll',
                              orient: 'vertical',
                              x: 'left',
                              textStyle: {
                                fontSize: RFT * 2.6,
                              },
                              itemWidth: RFT * 2.4,
                              itemHeight: RFT * 2.4,
                              //data: this.state.projectNameList
                            },
                            color: ['#419FFF', '#FF9D18', '#FF9D18', '#FFFf37', '#8579E8', '#9b59b6', '#27ae60', '#2ecc71', '#c0392b', '#e74c3c', '#f39c12', '#f1c40f'
                            ],
                            graphic: [{　　　　　　　　　　　　　　　　//环形图中间添加文字
                              type: 'text',　　　　　　　　　　　　//通过不同top值可以设置上下显示
                              left: '52%',
                              top: '42%',
                              style: {
                                text: "总库存：",
                                textAlign: 'center',
                                fill: '#000',　　　　　　　　//文字的颜色
                                width: 30,
                                height: 30,
                                fontSize: RFT * 3.7,
                                color: "#4d4f5c",
                                //fontFamily: "Microsoft YaHei"
                              }
                            }, {　　　　　　　　　　　　　　　　//环形图中间添加文字
                              type: 'text',　　　　　　　　　　　　//通过不同top值可以设置上下显示
                              left: '52%',
                              top: '49%',
                              style: {
                                text: Totol.compNum || 0 + '件',
                                textAlign: 'center',
                                fill: '#000',　　　　　　　　//文字的颜色
                                width: 30,
                                height: 30,
                                fontSize: RFT * 3.5,
                                color: "#4d4f5c",
                                //fontFamily: "Microsoft YaHei"
                              }
                            }, {　　　　　　　　　　　　　　　　//环形图中间添加文字
                              type: 'text',　　　　　　　　　　　　//通过不同top值可以设置上下显示
                              left: Totol.compVolume == 0 ? '55%' : '49%',
                              top: '56%',
                              style: {
                                text: Totol.compVolume || 0 + 'm³',
                                textAlign: 'right',
                                //fill: '#999',　　　　　　　　//文字的颜色
                                width: RFT * 4,
                                height: RFT * 4,
                                fontSize: RFT * 3.8,
                                //color: "#4d4f5c",
                                //fontFamily: "Microsoft YaHei"
                              }
                            }],
                            series: [
                              {
                                name: '库存量：',
                                type: 'pie',
                                radius: ['50%', '70%'],
                                center: ['59%', '50%'],
                                avoidLabelOverlap: false,
                                label: {
                                  normal: {
                                    show: true,
                                    position: 'outside',
                                    textStyle: {
                                      fontSize: RFT * 3.5,
                                      color: '#535c68'
                                    },
                                    formatter: '{d}%'
                                  },
                                  emphasis: {
                                    show: true,
                                    textStyle: {
                                      fontSize: RFT * 3.5,
                                      fontWeight: 'bold'
                                    }
                                  }
                                },
                                labelLine: {
                                  normal: {
                                    lineStyle: {
                                      show: false,
                                      color: '#235894'
                                    }
                                  }
                                },
                                data: this.state.resData
                              }
                            ]
                          }} />
                      </View>
                    </View>
                  </View> :
                  <View></View>
              }

              {
                Url.Menu.indexOf('APP成品库存可视化') != -1 ?
                  <View style={{ height: width }}>
                    <View style={{ flexDirection: 'row', }}>
                      <Text style={{ flex: 1, fontSize: RFT * 3.6, margin: 15, marginTop: 10, textAlignVertical: 'center', color: '#535c68' }}>成品库存可视化</Text>
                      {Platform.OS === 'ios' ?
                        <TouchableOpacity
                          style={{ flex: 1 }}
                          onPress={() => { this.setState({ isVisible: !this.state.isVisible }) }}  >
                          <Text style={{ color: '#419FFF', fontSize: 14, paddingTop: 13, paddingRight: 15, }}>{pickerValue}</Text>
                        </TouchableOpacity>
                        :
                        <Picker
                          mode='dialog'
                          selectedValue={pickerValue}
                          style={{ flex: 1, fontSize: 10, fontWeight: 'bold', textAlign: 'right', padding: 0, height: 42 }}
                          onValueChange={(value) => {
                            this.setState({ pickerValue: value, })
                          }}
                          itemStyle={{ fontSize: 10, }}

                        >
                          <Picker.Item label={'请选择'} value={'请选择堆场'} />
                          {stockNameData.map((item) => {
                            return (
                              <Picker.Item label={item} value={item} />
                            )
                          })}
                        </Picker>
                      }
                    </View>
                    <View style={{ height: deviceWidth * 0.6 }}>
                      <View style={{ width: width * 0.94, marginLeft: width * 0.03, height: deviceWidth * 0.6, backgroundColor: '#FFFFFF', borderRadius: 10 }}>
                        <FlatList
                          horizontal={true}
                          scrollEnabled={true}
                          //numColumns={2}
                          renderItem={this._renderItem}
                          data={resDataVisualized} />
                      </View>
                    </View>

                  </View> :
                  <View></View>
              }

            </View>


            <Overlay
              onBackdropPress={() => {
                alert
                this.setState({ isVisible: !this.state.isVisible })
              }}
              overlayStyle={{ width: width, height: 200, position: 'relative' }}
              animationType='fade'
              isVisible={this.state.isVisible}
              onRequestClose={() => {
                this.setState({ isVisible: !this.state.isVisible });
              }}>
              <Picker
                mode='dialog'
                selectedValue={pickerValue}
                style={{ flex: 1 }}
                onValueChange={(value) => {
                  this.setState({ pickerValue: value, })
                }}
              >
                <Picker.Item label={'请选择'} value={'请选择堆场'} />
                {stockNameData.map((item) => {
                  return (
                    <Picker.Item label={item} value={item} />
                  )
                })}
              </Picker>
            </Overlay>
          </ScrollView>


        </View>
      );
    }
  }

  _renderItem({ item, index }) {
    console.log(this.state.pickerValue)
    //const { pickerValue } = this.state;
    if (item.roomName == this.state.pickerValue) {
      return (
        <View style={{ flex: 1, height: deviceWidth * 0.6, justifyContent: 'center', alignContent: 'center' }}>
          <PieChart
            style={{ marginHorizontal: 45, height: deviceWidth * 0.55, width: width / 1.5 }}
            legend={{
              enabled: true,
              //textSize: 10,
              form: 'CIRCLE',
              wordWrapEnabled: true,
              xEntrySpace: 13,
              yEntrySpace: 6,
            }}
            data={{
              dataSets: [{
                values: [
                  { value: item.totalNum / item.avainum, label: '已用库存' },
                  { value: item.totalNum / item.avainum <= 1 ? 1 - (item.totalNum / item.avainum) : 0, label: '剩余库存' },
                ],
                label: ' ',
                config: {
                  colors: [processColor('#419FFF'), processColor('gray')],
                  valueTextSize: 14,
                  valueTextColor: processColor('#999999'),
                  sliceSpace: 5,
                  selectionShift: 13,
                  xValuePosition: "OUTSIDE_SLICE",
                  yValuePosition: "OUTSIDE_SLICE",
                  valueFormatter: "#.#'%'",
                  valueLineColor: processColor('#999999'),
                  valueLinePart1Length: 0.35
                }
              }],
            }}

            onSelect={this.handleSelect.bind(this)}
            onChange={(event) => console.log(event.nativeEvent)}
            marker={{
              enabled: true,
              markerColor: processColor('#999999'),
              textColor: processColor('white'),
              markerFontSize: 18,
              digits: 4,
            }}
            highlights={this.state.highlights}
            entryValue={true}
            entryLabelColor={processColor('green')}
            entryLabelTextSize={28}
            drawEntryLabels={false}
            rotationEnabled={true}
            rotationAngle={45}
            usePercentValues={true}
            styledCenterText={{ text: item.libraryName, color: processColor('black'), size: 12 }}
            centerTextRadiusPercent={80}
            holeRadius={65}
            holeColor={processColor('white')}
            transparentCircleRadius={45}
            transparentCircleColor={processColor('white')}
            maxAngle={450}
            chartDescription={this.state.description}
            onSelect={this.handleSelect.bind(this)}
            onChange={(event) => console.log(event.nativeEvent)}
          />
        </View>
      )
    }
  }

}

const styles = StyleSheet.create({
  text: {
    fontSize: RFT * 3.6,
    margin: 15,
    color: '#535c68'
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
  actionButtonText: {
    color: 'white',
    fontSize: 14,
  }
})


