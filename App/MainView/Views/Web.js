import React from 'react'
import { View, Text, Dimensions, StatusBar } from 'react-native'
import { WebView } from 'react-native-webview';
import { ScrollView } from 'react-native-gesture-handler';
import Orientation from '@emanon_/react-native-orientation';
import ActionButton from 'react-native-action-button';
import { Icon } from 'react-native-elements';

const { height, width } = Dimensions.get('window')

export default class webView extends React.Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title'),
    };
  };

  componentDidMount() {
    // Orientation.lockToLandscape();
    Orientation.addOrientationListener(this._orientationDidChange);
  }

  _orientationDidChange = (orientation) => {
    if (orientation === 'LANDSCAPE') {
      Orientation.lockToLandscape();
      // do something with landscape layout
    } else {
      Orientation.lockToPortrait();
      // do something with portrait layout
    }
  }


  componentWillUnmount() {
    Orientation.getOrientation((err, orientation) => {
      console.log(`Current Device Orientation: ${orientation}`);
    });
    Orientation.lockToPortrait();
    // Remember to remove listener
    Orientation.removeOrientationListener(this._orientationDidChange);
  }


  render() {
    let url = this.props.navigation.getParam('url')
    console.log("render -> url", url)
    return (
      <View style={{ flex: 1 }}>
        <ScrollView
          scrollEnabled={false}
          scrollToOverflowEnabled={false}
        >
          <StatusBar
            barStyle='default'
            hidden={false}
            translucent={false}
          />
          <View style={{ height: height }}  >
            <WebView
              source={{ uri: url }}
              incognito={true}
              scalesPageToFit={false}
              scrollEnabled={false}
              onScroll={(event) => {
                console.log("🚀 ~ file: Web.js:66 ~ webView ~ render ~ event:", event)
              }}
              style={{ width: "100%" }}
              injectedJavaScript='window.ReactNativeWebView.postMessage(document.documentElement.scrollHeight)'
            ></WebView>
          </View>
          <ActionButton
            offsetY={-100}
            zIndex={10000}
            buttonColor="#419FFF"
            onPress={() => {
              const orientation = Orientation.getInitialOrientation();
              if (orientation === 'LANDSCAPE') {
                Orientation.lockToPortrait();
                // do something with landscape layout
              } else {
                Orientation.lockToLandscape();
                // do something with portrait layout
              }
            }}
            renderIcon={() => (<View style={styles.actionButtonView}><Icon type='font-awesome' name='building' color='#ffffff' />
            </View>)}
          />
        </ScrollView>

      </View>

    )
  }
}