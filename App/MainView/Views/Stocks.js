import React from 'react';
import { View, processColor, Dimensions, ActivityIndicator, ScrollView, Picker, PickerIOS, Platform, StyleSheet, StatusBar, DeviceEventEmitter } from 'react-native';
import { Text, Button, Card, Input, Overlay, Icon, BottomSheet, SearchBar } from 'react-native-elements';
import { BarChart, PieChart } from 'react-native-charts-wrapper';
import { Echarts, echarts } from 'react-native-secharts';
import PieChatStock from '../Components/PieChartCommon';
import Url from '../../Url/Url';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import ActionButton from 'react-native-action-button';
import { RFT, RVH, deviceWidth } from '../../Url/Pixal';
import LinearGradient from 'react-native-linear-gradient';
import { MainStyles } from '../Components/MainStyles';
import ModalDropdown from 'react-native-modal-dropdown';
import { TouchableHighlight } from 'react-native';
import BottomItem from '../../PDA/Componment/BottomItem';
import Toast from 'react-native-easy-toast';


const { height, width } = Dimensions.get('window');
let stockNameData = []
let libraryArr = []
let librarySelectArr = []
let chartData = {
  dataSets: [
    {
      values: [
        { "y": [0, 0], "marker": ["0", "1"] },
      ],
      label: ' ',
      config: {
        drawValues: false,
        colors: [processColor('#509FF0'), processColor('#FF7A38')],
        stackLabels: ["计划指标全额", "退货数"],
      },
    }
  ],
  config: {
    barWidth: 0.25,
    group: {
      fromX: 0,
      groupSpace: 0.5,
    },
  }
}

export default class _StockScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      isVisible: false,
      pickerValue: '请选择',
      cardData: {
        passNumber: 0,
        passVolume: 0.00,
        refundNumber: 0,
        refundVolume: 0.00,
        stockNumber: 0,
        stockVolume: 0.00
      },
      //库房库位数据
      libraryData: [],
      libraryArr: [],
      libSelect: '请选择',
      libSearch: "",
      libraryId: '',
      libraryName: '',
      librarySelectArr: [],
      //
      libraryTableData: {},
      //
      bottomVisible: false,
      bottomData: [],
      //
      projectNameArr: [],
      planNumberArr: [],
      refundNumberArr: [],
      chartData: {
        dataSets: [
          {
            values: [
              { "y": [0, 0], "marker": ["0", "1"] },
            ],
            label: ' ',
            config: {
              drawValues: false,
              colors: [processColor('#509FF0'), processColor('#FF7A38')],
              stackLabels: ["计划指标全额", "退货数"],

            },
          }
        ],
        config: {
          barWidth: 0.25,
          group: {
            fromX: 0,
            groupSpace: 0.5,
          },
        }
      },
      //卡片数据
      isLoading: true,
      cardLoading: true,
      chartLoading: true,
      //旧版本
      description: {
        text: ' ',
        textSize: 15,
        textColor: processColor('darkgray'),
      },
      resData: [{
        "id": 1,
        "factoryId": "a9d6c924-d300-4335-a77d-4a5dce4c28fb",
        "stockType": "TheoryTotalStock",
        "compNum": 8000,
        "compVolume": 0.000
      },
      {
        "id": 2,
        "factoryId": "a9d6c924-d300-4335-a77d-4a5dce4c28fb",
        "stockType": "UsedStock",
        "compNum": 7262,
        "compVolume": 5082.688
      }],
      resDataVisualized: [],
      projectNameList: [],
      resDataProject: [],
      numList: [],
      Totol: {},
    };
    this._renderItem = this._renderItem.bind(this);
  }

  componentDidMount() {
    Url.Login()
    if (Url.isStaticTypeOpen) {
      this.StaticTypeSwitch()
    } else {
      Url.StaticType = ''
      this.resDataContorl()
    }


    this._navListener = this.props.navigation.addListener('didFocus', () => {
      StatusBar.setBackgroundColor('black');
      StatusBar.setTranslucent(false)
      this.UpdateControl()
    });
    if (Url.isStaticTypeOpen) {
      console.log("🚀 ~ file: Stocks.js:116 ~ _StockScreen ~ componentDidMount ~ Url.isStaticTypeOpen:", Url.isStaticTypeOpen)
      this.staticTypeEmitterSot = DeviceEventEmitter.addListener('staticType', (result) => {
        if (result) {
          this.StaticTypeSwitch(result)
        }
      })
    }

  }

  UpdateControl = () => {
    const route = this.props.navigation.state
    console.log("🚀 ~ file: Daily.js ~ line 472 ~ DailyScreen ~ render ~ route", route.routeName)
    DeviceEventEmitter.emit('route', route.routeName);
  }

  componentWillUnmount() {
    this._navListener.remove();
    this.staticTypeEmitterSot && this.staticTypeEmitterSot.remove()
  }

  StaticTypeSwitch = (result) => {
    if (typeof result != 'undefined') {
      Url.StaticType = result
    }
    this.requestLibraryDetail()
    this.requestStockSummary()
    this.setState({ cardLoading: true })
  }

  resDataContorl = () => {
    this.setState({ isLoading: true })
    this.request();
    this.resDataStockVisualized();
    this.requestProData();
  }

  requestLibraryDetail = () => {
    const url = Url.url + Url.Stock + Url.Fid + Url.StaticType + 'GetLibraryDetail';
    console.log("🚀 ~ file: Stocks.js:143 ~ _StockScreen ~ url:", url)
    fetch(url, {
      credentials: 'include',
    }).then(res => {
      return res.json()
    }).then(resData => {
      libraryArr = []
      if (resData.length > 0) {
        resData.map((item, index) => {
          libraryArr.push(item)
        })
        this.setState({
          libraryData: resData,
          librarySelectArr: resData,
          libraryId: resData[0].libraryId,
          libraryName: resData[0].libraryName,
          libSelect: resData[0].libraryName,
          isLoading: false
        }, () => {
          this.requestLibraryStock()
          this.requestLibraryBarStock()
        })
      } else {
        this.toast.show('没有查询到库房库位数据')
        this.setState({
          libraryData: [],
          librarySelectArr: [],
          libraryId: '',
          libraryName: '',
          libSelect: '',
          isLoading: false
        })
      }


      console.log("🚀 ~ file: Stocks.js:841 ~ _StockScreen ~ resData:", resData)
    })
  }

  requestStockSummary = () => {
    const url = Url.url + Url.Stock + Url.Fid + Url.StaticType + 'GetStatisticFactoryStockSummary';
    console.log("🚀 ~ file: Stocks.js:185 ~ _StockScreen ~ url:", url)
    fetch(url, {
      credentials: 'include',
    }).then(res => {
      return res.json()
    }).then(resData => {
      console.log("🚀 ~ file: Stocks.js:842 ~ _StockScreen ~ resData:", resData)
      this.setState({
        cardData: resData,
        cardLoading: false,
        isLoading: false
      })
    })
  }

  requestLibraryStock = () => {
    const url = Url.url + Url.Stock + Url.Fid + this.state.libraryId + '/' + Url.StaticType + 'GetStatisticFactoryLibraryStock';
    console.log("🚀 ~ file: Stocks.js:125 ~ _StockScreen ~ url:", url)
    fetch(url, {
      credentials: 'include',
    }).then(res => {
      return res.json()
    }).then(resData => {
      console.log("🚀 ~ file: Stocks.js:843 ~ _StockScreen ~ resData:", resData)
      if (resData.status == 404) {
        this.toast.show('没有查询到库存数据')
      }
      if (typeof resData == 'object') {
        this.setState({
          libraryTableData: resData
        })
      }
    }).catch(err => {
      console.log("🚀 ~ file: Stocks.js:133 ~ _StockScreen ~ err:", err)
    })
  }

  requestLibraryBarStock = () => {
    const url = Url.url + Url.Stock + Url.Fid + this.state.libraryId + '/' + Url.StaticType + 'GetStatisticFactoryLibraryBarStock';
    console.log("🚀 ~ file: Stocks.js:190 ~ _StockScreen ~ chartData:", chartData)
    console.log("🚀 ~ file: Stocks.js:125 ~ _StockScreen ~ url:", url)
    fetch(url, {
      credentials: 'include',
    }).then(res => {
      return res.json()
    }).then(resData => {
      console.log("🚀 ~ file: Stocks.js:844 ~ _StockScreen ~ resData:", resData)
      let projectNameArr = [], planNumberArr = [], refundNumberArr = [], dataArr = []

      resData.map((item, index) => {
        let shortProjectName = item.shortProjectName
        shortProjectName = shortProjectName.substr(0, 9)
        let tmp = { y: [], marker: [] }
        tmp.y.push(item.planNumber)
        tmp.y.push(item.refundNumber)
        tmp.marker.push(item.planNumber.toString())
        tmp.marker.push(item.refundNumber.toString())
        projectNameArr.push(shortProjectName)
        planNumberArr.push(item.planNumber)
        refundNumberArr.push(item.refundNumber)
        dataArr.push(tmp)
      })
      console.log("🚀 ~ file: Stocks.js:190 ~ _StockScreen ~ chartData:", (chartData))

      //chartData.dataSets[0].values = dataArr


      this.setState({
        libraryBarData: resData,
        projectNameArr: projectNameArr,
        planNumberArr: planNumberArr,
        refundNumberArr: refundNumberArr,
        chartData: dataArr,
        chartLoading: false
      })
    }).catch(err => {
      console.log("🚀 ~ file: Stocks.js:133 ~ _StockScreen ~ err:", err)
    })
  }

  request = () => {
    let StockSaturation = []
    const urlnew = Url.url + Url.factory + Url.Fid + Url.StaticType + 'GetFactoryStockSaturation';
    console.log("🚀 ~ file: Stocks.js:101 ~ _StockScreen ~ urlnew:", urlnew)
    fetch(urlnew, {
      credentials: 'include',
    }).then(res => {
      return res.json()
    }).then(resData => {
      let used = {}, unused = {}
      if (resData[1]) {
        used.value = resData[1].compNum || 0
        used.name = '已用库存'
        StockSaturation.push(used)
        unused.value = resData[0].compNum - (resData[1].compNum || 0)
        unused.name = '剩余库存'
        StockSaturation.push(unused)
        this.setState({
          resData: StockSaturation,
          Totol: resData[0]
          // isLoading: false
        })
      } else {
        used.value = 0
        used.name = '已用库存'
        StockSaturation.push(used)
        unused.value = resData[0].compNum
        unused.name = '剩余库存'
        StockSaturation.push(unused)
        this.setState({
          resData: StockSaturation,
          Totol: resData[0]
          // isLoading: false
        })
      }
      //const numListnew = JSON.parse(JSON.stringify(this.state.numList).replace(/name/g, "label")); //改名语句

      //console.log(this.state.numList)
    }).catch((error) => {
      console.log(error);
    });
  }

  requestProData = (urlProps) => {
    let url = Url.url + Url.factory + Url.Fid + 'GetFacProjStockSaturation'
    if (typeof urlProps != "undefined") {
      url = urlProps
    }
    console.log("🚀 ~ file: Stocks.js:147 ~ _StockScreen ~ url:", url)
    fetch(url, {
      credentials: 'include',
    }).then(res => {
      return res.json()
    }).then(resDataPro => {
      this.setState({
        resDataProject: resDataPro,
        projectNameList: resDataPro.projectNameList,
        numList: resDataPro.numList,
        isLoading: false,
      })
      //const numListnew = JSON.parse(JSON.stringify(this.state.numList).replace(/name/g, "label")); //改名语句
      //this.setState({ numList: numListnew })

      console.log(this.state.resDataProject)
    }).catch((error) => {
      console.log(error);
    });
  }

  resDataStockVisualized = () => {
    const url = Url.url + 'Stock/' + Url.Fid + Url.StaticType + 'GetStockVisualized';
    console.log("🚀 ~ file: Stocks.js:159 ~ _StockScreen ~ url", url)
    fetch(url, {
      credentials: 'include',
    }).then(resVisualized => {
      return resVisualized.json()
    }).then(resDataVisualized => {
      this.setState({
        resDataVisualized: resDataVisualized,
      })
      console.log(this.state.resDataVisualized)
      //const numListnew = JSON.parse(JSON.stringify(this.state.numList).replace(/name/g, "label")); //改名语句
    })
  }

  BottomList = () => {
    const { bottomVisible, bottomData, libraryData, librarySelectArr } = this.state
    const { navigation } = this.props;
    let pageType = navigation.getParam('pageType') || ''
    return (
      <BottomSheet
        isVisible={bottomVisible}
        MAX_HEIGHT_CUS={400}
        onRequestClose={() => {
          this.setState({
            bottomVisible: false
          })
        }}
        onBackdropPress={() => {
          this.setState({
            bottomVisible: false
          })
        }}
      //modalProps={{ style: { height: 100 }, }}
      >
        <View style={{ flexDirection: 'row' }}>
          <SearchBar
            platform='android'
            placeholder={'请输入或搜索内容...'}
            containerStyle={{ backgroundColor: 'white', shadowColor: '#999', }}
            inputContainerStyle={{ backgroundColor: '#f9f9f9', borderRadius: 45, width: deviceWidth * 0.76 }}
            inputStyle={[{ color: '#333' }, deviceWidth <= 330 && { fontSize: 14 }]}
            round={true}
            keyboardType={'default'}
            cancelIcon={() => ({
              type: 'material',
              size: 25,
              color: 'rgba(0, 0, 0, 0.54)',
              name: 'search',
            })}
            value={this.state.libSearch}
            onChangeText={(text) => {
              text = text.replace(/\s+/g, "")
              text = text.replace(/<\/?.+?>/g, "")
              text = text.replace(/[\r\n]/g, "")
              this.setState({
                libSearch: text
              })
            }}
            returnKeyType='search'
            onClear={() => {
              this.setState({ librarySelectArr: this.state.libraryData })
            }}
            onSubmitEditing={() => {
              this.onSearchDriver(this.state.libSearch)
            }} />
          <Button
            containerStyle={{ width: deviceWidth * 0.2 }}
            buttonStyle={{ borderRadius: 20, width: deviceWidth * 0.16, top: 12, left: 10 }}
            title='确定'
            onPress={() => { this.onSearchDriver(this.state.libSearch) }} />
        </View>
        <FlatList
          //keyExtractor={index}
          data={librarySelectArr}
          renderItem={this.LibraryItem}
          style={{ height: 345 }}
          keyboardShouldPersistTaps='always' />
        <TouchableHighlight
          onPress={() => {
            this.setState({ bottomDriverVisible: false })
          }}>
          <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
        </TouchableHighlight>
      </BottomSheet>

    )
  }

  LibraryItem = ({ item, index }) => {
    let title = ''
    title = item.libraryName
    return (
      <TouchableHighlight
        onPress={() => {
          this.setState({
            libSelect: item.libraryName,
            libraryName: item.libraryName,
            libraryId: item.libraryId,
            bottomVisible: false,
            chartLoading: true
          }, () => {
            this.requestLibraryStock()
            this.requestLibraryBarStock()
          })
        }}>
        <BottomItem backgroundColor='white' color="#333" title={title}></BottomItem>
      </TouchableHighlight>
    )
  }

  onSearchDriver = (text) => {
    const { libraryData } = this.state
    let textArr = []
    let searchArr = [[]]
    let reasonArr = []
    librarySelectArr = []
    if (text.length > 0) {
      console.log("🚀 ~ file: Stocks.js:260 ~ _StockScreen ~ text:", text)
      // textArr = text.split("")
      // for (let i = 0; i < textArr.length; i++) {
      //   const tempsearchArr = searchArr.map(subset => {
      //     const one = subset.concat([]);
      //     one.push(textArr[i]);
      //     return one;
      //   })
      //   searchArr = searchArr.concat(tempsearchArr);
      // }

      libraryData.map((libItem) => {
        if (libItem.libraryName.indexOf(text) != -1) {
          let tmp = libItem
          reasonArr.push(tmp)
        }
      })
      console.log("🚀 ~ file: Stocks.js:275 ~ _StockScreen ~ libraryData.map ~ reasonArr:", reasonArr)
      librarySelectArr = [...new Set(reasonArr)]
      if (this.state.librarySelectArr != librarySelectArr) {
        this.setState({
          librarySelectArr: librarySelectArr,
        })
      }
    } else {
      this.setState({
        librarySelectArr: this.state.libraryData,
      })
    }
  }

  _renderItem({ item, index }) {
    if (typeof this.state.pickerValue == 'undefined') {
      return
    }
    if (item.roomName == this.state.pickerValue) {
      return (
        <View style={{ flex: 1, height: deviceWidth * 0.6, justifyContent: 'center', alignContent: 'center' }}>
          <PieChart
            style={{ marginHorizontal: 45, height: deviceWidth * 0.55, width: width / 1.5 }}
            legend={{
              enabled: true,
              //textSize: 10,
              form: 'CIRCLE',
              wordWrapEnabled: true,
              xEntrySpace: 13,
              yEntrySpace: 6,
            }}
            data={{
              dataSets: [{
                values: [
                  { value: item.totalNum / item.avainum, label: '已用库存' },
                  { value: item.totalNum / item.avainum <= 1 ? 1 - (item.totalNum / item.avainum) : 0, label: '剩余库存' },
                ],
                label: ' ',
                config: {
                  colors: [processColor('#419FFF'), processColor('gray')],
                  valueTextSize: 14,
                  valueTextColor: processColor('#999999'),
                  sliceSpace: 5,
                  selectionShift: 13,
                  xValuePosition: "OUTSIDE_SLICE",
                  yValuePosition: "OUTSIDE_SLICE",
                  valueFormatter: "#.#'%'",
                  valueLineColor: processColor('#999999'),
                  valueLinePart1Length: 0.35
                }
              }],
            }}

            onChange={(event) => console.log(event.nativeEvent)}
            marker={{
              enabled: true,
              markerColor: processColor('#999999'),
              textColor: processColor('white'),
              markerFontSize: 18,
              digits: 4,
            }}
            highlights={this.state.highlights}
            entryValue={true}
            entryLabelColor={processColor('green')}
            entryLabelTextSize={28}
            drawEntryLabels={false}
            rotationEnabled={true}
            rotationAngle={45}
            usePercentValues={true}
            styledCenterText={{ text: item.libraryName, color: processColor('black'), size: 12 }}
            centerTextRadiusPercent={80}
            holeRadius={65}
            holeColor={processColor('white')}
            transparentCircleRadius={45}
            transparentCircleColor={processColor('white')}
            maxAngle={450}
            chartDescription={this.state.description}
          />
        </View>
      )
    }
  }

  render() {

    if (Url.isStaticTypeOpen) {
      return this.renderNew()
    } else {
      return this.renderOld()
    }
  }

  renderNew() {
    const { cardData, cardLoading, libSelect, libraryTableData, libraryName, libraryId, projectNameArr, planNumberArr, refundNumberArr, chartData } = this.state;
    if (this.state.isLoading) {
      return (
        <View style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#F5FCFF',
        }}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          <ScrollView style={{ backgroundColor: '#F9F9F9' }}>
            <View style={{ height: 150 }}>
              <LinearGradient
                start={{ x: 1, y: 1 }} end={{ x: 0, y: 1 }}
                colors={['rgba(16,237,195,0.88)', '#15D639']}
                style={{ width: width, height: width * 0.36, }}
              >
              </LinearGradient>
            </View>

            <View style={{ top: -140, }}>
              <View style={styles.contentView}>

                <View style={{ flexDirection: 'row', justifyContent: 'space-around', flex: 1 }}>
                  <View style={[styles.card, { backgroundColor: '#2DC5F3', marginLeft: 10 }]}>
                    {
                      cardLoading ?
                        <View style={{
                          flex: 1,
                          flexDirection: 'row',
                          justifyContent: 'center',
                          alignItems: 'center',
                          backgroundColor: 'transparent',
                        }}><ActivityIndicator
                            animating={true}
                            color='white'
                            size="large" />
                        </View> :
                        <View>
                          <View style={styles.titleView}>
                            <View style={styles.badge}></View>
                            <Text style={styles.title} >库存总数</Text>
                          </View>
                          <Text style={styles.text1} numberOfLines={1}>{cardData.stockVolume.toFixed(2) + 'm³'}</Text>
                          <Text style={styles.text2}>{cardData.stockNumber + '件'}</Text>
                        </View>}
                  </View>
                  <View style={[styles.card, { backgroundColor: '#77CF1D', }]}>
                    {
                      cardLoading ?
                        <View style={{
                          flex: 1,
                          flexDirection: 'row',
                          justifyContent: 'center',
                          alignItems: 'center',
                          backgroundColor: 'transparent',
                        }}><ActivityIndicator
                            animating={true}
                            color='white'
                            size="large" />
                        </View> : <View>
                          <View style={styles.titleView}>
                            <View style={styles.badge}></View>
                            <Text style={styles.title} >合格品数</Text>
                          </View>
                          <Text style={styles.text1} numberOfLines={1}>{cardData.passVolume.toFixed(2) + 'm³'}</Text>
                          <Text style={styles.text2}>{cardData.passNumber + '件'}</Text>
                        </View>}
                  </View>
                  <View style={[styles.card, { backgroundColor: '#FF7B3F', marginRight: 12 }]}>
                    {
                      cardLoading ?
                        <View style={{
                          flex: 1,
                          flexDirection: 'row',
                          justifyContent: 'center',
                          alignItems: 'center',
                          backgroundColor: 'transparent',
                        }}><ActivityIndicator
                            animating={true}
                            color='white'
                            size="large" />
                        </View> :
                        <View>
                          <View style={styles.titleView}>
                            <View style={styles.badge}></View>
                            <Text style={styles.title} >退货总数</Text>
                          </View>
                          <Text style={styles.text1} numberOfLines={1}>{cardData.refundVolume.toFixed(2) + 'm³'}</Text>
                          <Text style={styles.text2}>{cardData.refundNumber + '件'}</Text>
                        </View>}
                  </View>
                </View>

                <View style={{}}>
                  <TouchableHighlight
                    underlayColor={'lightgray'}
                    onPress={() => {
                      if (this.state.libraryData.length > 0) {
                        this.setState({ bottomVisible: true })
                      } else {
                        this.toast.show('没有查询到库房库位数据')
                      }
                    }} >
                    <View style={{ flexDirection: 'row', height: 50 }}>
                      <Text style={{ flex: 1, fontSize: RFT * 3.6, marginLeft: 14, textAlignVertical: 'center', color: '#535c68' }}>成品库存可视化</Text>
                      <View style={{ flexDirection: 'row', marginRight: 14, }}>
                        <Text onPress={() => {
                          this.setState({ bottomVisible: true })
                        }} style={{ fontSize: RFT * 3.3, lineHeight: 50 }}>{libSelect}</Text>
                        <Icon name='caretdown' color='#4D8EF5' iconStyle={{ top: 3, fontSize: 11, marginLeft: 5, marginRight: -1, lineHeight: 43 }} type='antdesign' ></Icon>
                      </View>
                    </View>
                  </TouchableHighlight>
                  <View style={{ marginHorizontal: 12 }}>
                    <View style={styles.table_line}>
                      <View style={styles.table_item_title}><Text style={[styles.table_item_text, styles.table_item_title_text]}>库房名称</Text></View>
                      <View style={styles.table_item_value}><Text style={[styles.table_item_text, styles.table_item_value_text]}>{libraryTableData.roomName || ""}</Text></View>
                    </View>
                    <View style={styles.table_line}>
                      <View style={styles.table_item_title}><Text style={[styles.table_item_text, styles.table_item_title_text]}>库位名称</Text></View>
                      <View style={styles.table_item_value}><Text style={[styles.table_item_text, styles.table_item_value_text]}>{libraryTableData.libraryName || ""}</Text></View>
                    </View>
                    <View style={styles.table_line}>
                      <View style={styles.table_item_title}><Text style={[styles.table_item_text, styles.table_item_title_text]}>库位编码</Text></View>
                      <View style={styles.table_item_value}><Text style={[styles.table_item_text, styles.table_item_value_text]}>{libraryTableData.libraryCode || ""}</Text></View>
                    </View>
                    <View style={styles.table_line}>
                      <View style={styles.table_item_title}><Text style={[styles.table_item_text, styles.table_item_title_text]}>可存数量（件）</Text></View>
                      <View style={styles.table_item_value}><Text style={[styles.table_item_text, styles.table_item_value_text]}>{libraryTableData.availableNumber || ""}</Text></View>
                    </View>
                    <View style={styles.table_line_double}>
                      <View style={styles.table_item_double_title}>
                        <Text style={[styles.table_item_double_text, styles.table_item_title_text]}>实际库存数</Text>
                      </View>
                      <View style={styles.table_item_double_value}>
                        <View style={{ flex: 1 }}>
                          <View style={[styles.table_item_doubleItem_value, styles.table_item_value_bottomline]}>
                            <Text style={[styles.table_item_text_fir]}>数量/件</Text>
                          </View>
                          <View style={styles.table_item_doubleItem_value}>
                            <Text style={styles.table_item_text_sec}>方量/m³</Text>
                          </View>
                        </View>
                        <View style={{ flex: 1, borderLeftColor: '#e9e9e9', borderLeftWidth: 1 }}>
                          <View style={[styles.table_item_doubleItem_value, styles.table_item_value_bottomline]}>
                            <Text style={[styles.table_item_text_fir]}>{libraryTableData.stockNumber || "0"}</Text>
                          </View>
                          <View style={styles.table_item_doubleItem_value}>
                            <Text style={styles.table_item_text_sec}>{libraryTableData.stockVolume || "0"}</Text>
                          </View>
                        </View>
                      </View>
                    </View>
                    <View style={styles.table_line_double}>
                      <View style={styles.table_item_double_title}>
                        <Text style={[styles.table_item_double_text, styles.table_item_title_text]}>合格品数</Text>
                      </View>
                      <View style={styles.table_item_double_value}>
                        <View style={{ flex: 1 }}>
                          <View style={[styles.table_item_doubleItem_value, styles.table_item_value_bottomline]}>
                            <Text style={[styles.table_item_text_fir]}>数量/件</Text>
                          </View>
                          <View style={styles.table_item_doubleItem_value}>
                            <Text style={styles.table_item_text_sec}>方量/m³</Text>
                          </View>
                        </View>
                        <View style={{ flex: 1, borderLeftColor: '#e9e9e9', borderLeftWidth: 1 }}>
                          <View style={[styles.table_item_doubleItem_value, styles.table_item_value_bottomline]}>
                            <Text style={[styles.table_item_text_fir]}>{libraryTableData.passNumber || "0"}</Text>
                          </View>
                          <View style={styles.table_item_doubleItem_value}>
                            <Text style={styles.table_item_text_sec}>{libraryTableData.passVolume || "0"}</Text>
                          </View>
                        </View>
                      </View>
                    </View>
                    <View style={styles.table_line_double}>
                      <View style={styles.table_item_double_title}>
                        <Text style={[styles.table_item_double_text, styles.table_item_title_text]}>退货数</Text>
                      </View>
                      <View style={styles.table_item_double_value}>
                        <View style={{ flex: 1 }}>
                          <View style={[styles.table_item_doubleItem_value, styles.table_item_value_bottomline]}>
                            <Text style={[styles.table_item_text_fir]}>数量/件</Text>
                          </View>
                          <View style={styles.table_item_doubleItem_value}>
                            <Text style={styles.table_item_text_sec}>方量/m³</Text>
                          </View>
                        </View>
                        <View style={{ flex: 1, borderLeftColor: '#e9e9e9', borderLeftWidth: 1 }}>
                          <View style={[styles.table_item_doubleItem_value, styles.table_item_value_bottomline]}>
                            <Text style={[styles.table_item_text_fir]}>{libraryTableData.refundNumber || "0"}</Text>
                          </View>
                          <View style={styles.table_item_doubleItem_value}>
                            <Text style={styles.table_item_text_sec}>{libraryTableData.refundVolume || "0"}</Text>
                          </View>
                        </View>
                      </View>
                    </View>
                    <View style={[styles.table_line_double, styles.table_item_value_bottomline]}>
                      <View style={styles.table_item_double_title}>
                        <Text style={[styles.table_item_double_text, styles.table_item_title_text]}>待改制</Text>
                      </View>
                      <View style={styles.table_item_double_value}>
                        <View style={{ flex: 1 }}>
                          <View style={[styles.table_item_doubleItem_value, styles.table_item_value_bottomline]}>
                            <Text style={[styles.table_item_text_fir]}>数量/件</Text>
                          </View>
                          <View style={styles.table_item_doubleItem_value}>
                            <Text style={styles.table_item_text_sec}>方量/m³</Text>
                          </View>
                        </View>
                        <View style={{ flex: 1, borderLeftColor: '#e9e9e9', borderLeftWidth: 1 }}>
                          <View style={[styles.table_item_doubleItem_value, styles.table_item_value_bottomline]}>
                            <Text style={[styles.table_item_text_fir]}>{libraryTableData.restructureNumber || "0"}</Text>
                          </View>
                          <View style={styles.table_item_doubleItem_value}>
                            <Text style={styles.table_item_text_sec}>{libraryTableData.restructureVolume || "0"}</Text>
                          </View>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>

              </View>

              <View style={[styles.contentView, { height: 300 }]}>
                <View style={{ width: deviceWidth * 0.9, height: 280 }}>
                  {
                    this.state.chartLoading ? <View style={{
                      flex: 1,
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                      backgroundColor: '#F5FCFF',
                    }}>
                      <ActivityIndicator
                        animating={true}
                        color='#419FFF'
                        size="large" />
                    </View> :
                      <BarChart
                        style={styles.chart_container}
                        xAxis={{
                          valueFormatter: projectNameArr,
                          granularityEnabled: true,
                          granularity: 1,
                          axisMaximum: projectNameArr.length,
                          axisMinimum: -0.5,
                          position: "BOTTOM",
                          drawAxisLine: false,
                          centerAxisLabels: false,
                          setDrawGirdLines: false,
                          drawGridLines: false,
                          avoidFirstLastClipping: false,
                          centerAxisLabels: false
                        }}
                        legend={{
                          enabled: true,
                          // textSize: 14,
                          form: "SQUARE",
                          //formSize: 14,
                          yEntrySpace: 60,
                          formToTextSpace: 16,
                          xEntrySpace: 40,
                          wordWrapEnabled: false,
                          horizontalAlignment: 'CENTER',
                          verticalAlignment: 'Top'
                        }}
                        yAxis={{
                          left: {
                            enabled: true,
                            axisMinimum: 0,
                            gridColor: processColor('#e9e9e9'),
                            labelCount: 4,
                            drawAxisLine: false,
                            labelCount: 4,
                            labelCountForce: 4
                            //drawLimitLinesBehindData: true
                          },
                          right: {
                            enabled: false,
                          }
                        }}
                        data={{
                          dataSets: [
                            {
                              values: this.state.chartData,
                              label: ' ',
                              config: {
                                drawValues: false,
                                colors: [processColor('#509FF0'), processColor('#FF7A38')],
                                stackLabels: ["计划指标全额", "退货数"],
                              },
                            }
                          ],
                          config: {
                            barWidth: 0.25,
                            group: {
                              fromX: 0,
                              groupSpace: 0.99,
                            },
                          }
                        }}
                        marker={{
                          enabled: true,
                          markerColor: processColor('#999999'),
                          textColor: processColor('white'),
                          markerFontSize: 14,
                        }}
                        autoScaleMinMaxEnabled={true}
                        //highlights={[{ x: 1, stackIndex: 2 }, { x: 2, stackIndex: 1 }]}
                        onChange={(event) => console.log(event.nativeEvent)}
                        visibleRange={{
                          x: { min: 4, max: 4 }
                        }}
                        chartDescription={{ text: '' }}
                      />
                  }
                </View>


              </View>

            </View>

          </ScrollView>

          {this.BottomList()}
        </View>
      );
    }
  }

  renderOld() {
    const { resData, resDataVisualized, pickerValue, Totol } = this.state;
    let i = 0, length = resDataVisualized.length;
    this.state.resDataVisualized.map((item, index) => {
      if (stockNameData.indexOf(item.roomName) == -1) {
        stockNameData[i++] = item.roomName;
      }
    });
    let initialValue = stockNameData[0];
    stockNameData.length = length;
    i = 0;
    //console.log(initialValue)
    if (this.state.isLoading) {
      return (
        <View style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#F5FCFF',
        }}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
    } else {
      return (
        <View>
          <ScrollView style={{ backgroundColor: '#F9F9F9', marginBottom: -220 }}>

            <View style={{ height: 150 }}>
              <LinearGradient
                start={{ x: 1, y: 1 }} end={{ x: 0, y: 1 }}
                colors={['rgba(16,237,195,0.88)', '#15D639']}
                style={{ width: width, height: width * 0.36, }}
              >
              </LinearGradient>
            </View>

            <View style={{ top: -140, }}>
              {
                Url.Menu.indexOf('APP成品库存饱和度分析-项目') == -1 ? <View></View> :
                  <View style={{ height: width * 0.6 }}>
                    <View style={MainStyles.chartView}>
                      <Text style={MainStyles.chartTitle}>成品库存饱和度分析-项目</Text>
                      <View style={MainStyles.chart}>
                        <Echarts
                          height={deviceWidth * 0.48}
                          width={width}
                          option={{
                            tooltip: {
                              show: true,
                              trigger: 'item',
                              formatter: "{a} <br/>{b}: {c}件 ({d}%)"
                            },
                            legend: {
                              type: 'scroll',
                              orient: 'vertical',
                              x: 'left',
                              textStyle: {
                                fontSize: RFT * 2.6,
                              },
                              itemWidth: RFT * 2.4,
                              itemHeight: RFT * 2.4,
                              data: this.state.projectNameList
                            },
                            color: ['#419FFF', '#41CCFF', '#FF9D18', '#FFFf37', '#8579E8', '#9b59b6', '#27ae60', '#2ecc71', '#c0392b', '#e74c3c', '#f39c12', '#f1c40f'
                            ],
                            series: [
                              {
                                name: '项目名：',
                                type: 'pie',
                                radius: ['50%', '70%'],
                                center: ['59%', '50%'],
                                avoidLabelOverlap: false,
                                label: {
                                  normal: {
                                    show: false,
                                    position: 'center',
                                    textStyle: {
                                      fontSize: RFT * 3.5,
                                      color: '#535c68'
                                    },
                                    formatter: '{b}:' + '{d}%'
                                  },
                                  emphasis: {
                                    show: true,
                                    textStyle: {
                                      fontSize: RFT * 3.5,
                                      fontWeight: 'bold'
                                    }
                                  }
                                },
                                labelLine: {
                                  normal: {
                                    lineStyle: {
                                      show: false,
                                      color: '#235894'
                                    }
                                  }
                                },
                                data: this.state.numList
                              }
                            ]
                          }} />
                        {/* <PieChatStock
                      legend={{
                        enabled: true,
                        textSize: RFT * 2.5,
                        form: 'CIRCLE',
                        wordWrapEnabled: true,
                        xEntrySpace: 13,
                        yEntrySpace: 6,
                      }}
                      data={{
                        dataSets: [{
                          values: this.state.numList,
                          label: ' ',
                          config: {
                            colors: [processColor('#419FFF'), processColor('#41CCFF'), processColor('#FF9D18'), processColor('#FFFf37'), processColor('#8579E8'), processColor('#9b59b6'), processColor('#27ae60'), processColor('#2ecc71'), processColor('#c0392b'), processColor('#e74c3c'), processColor('#f39c12'), processColor('#f1c40f')],
                            valueTextSize: 1,
                            valueTextColor: processColor('transparent'),
                            sliceSpace: 5,
                            selectionShift: 13,
                            //xValuePosition: "OUTSIDE_SLICE",
                            //yValuePosition: "OUTSIDE_SLICE",
                            valueFormatter: "#.#'%'",
                            valueLineColor: processColor('transparent'),
                            valueLinePart1Length: 0.01,
                            valueLineVariableLength: false,
                          }
                        }],
                      }}
                      onSelect={this.handleSelect.bind(this)}
                      onChange={(event) => console.log(event.nativeEvent)}
                      marker={{
                        enabled: true,
                        markerColor: processColor('#999999'),
                        textColor: processColor('white'),
                        markerFontSize: 14,
                      }}
                      style={{ flex: 1 }}
                    /> */}
                      </View>
                    </View>
                  </View>

              }

              {
                Url.Menu.indexOf('APP成品库存饱和度分析-库存') != -1 ?
                  <View style={{ height: deviceWidth * 0.57, marginTop: 10 }}>
                    <View style={MainStyles.chartView}>
                      <Text style={MainStyles.chartTitle}>成品库存饱和度分析-库存</Text>
                      <View style={MainStyles.chart}>
                        <Echarts
                          height={width * 0.48}
                          width={width * 0.9}
                          option={{
                            tooltip: {
                              show: true,
                              trigger: 'item',
                              formatter: "{a} <br/>{b}: {c}件 ({d}%)"
                            },
                            legend: {
                              type: 'scroll',
                              orient: 'vertical',
                              x: 'left',
                              textStyle: {
                                fontSize: RFT * 2.6,
                              },
                              itemWidth: RFT * 2.4,
                              itemHeight: RFT * 2.4,
                              //data: this.state.projectNameList
                            },
                            color: ['#419FFF', '#FF9D18', '#FF9D18', '#FFFf37', '#8579E8', '#9b59b6', '#27ae60', '#2ecc71', '#c0392b', '#e74c3c', '#f39c12', '#f1c40f'
                            ],
                            graphic: [{　　　　　　　　　　　　　　　　//环形图中间添加文字
                              type: 'text',　　　　　　　　　　　　//通过不同top值可以设置上下显示
                              left: '52%',
                              top: '42%',
                              style: {
                                text: "总库存：",
                                textAlign: 'center',
                                fill: '#000',　　　　　　　　//文字的颜色
                                width: 30,
                                height: 30,
                                fontSize: RFT * 3.7,
                                color: "#4d4f5c",
                                //fontFamily: "Microsoft YaHei"
                              }
                            }, {　　　　　　　　　　　　　　　　//环形图中间添加文字
                              type: 'text',　　　　　　　　　　　　//通过不同top值可以设置上下显示
                              left: '52%',
                              top: '49%',
                              style: {
                                text: Totol.compNum || 0 + '件',
                                textAlign: 'center',
                                fill: '#000',　　　　　　　　//文字的颜色
                                width: 30,
                                height: 30,
                                fontSize: RFT * 3.5,
                                color: "#4d4f5c",
                                //fontFamily: "Microsoft YaHei"
                              }
                            }, {　　　　　　　　　　　　　　　　//环形图中间添加文字
                              type: 'text',　　　　　　　　　　　　//通过不同top值可以设置上下显示
                              left: Totol.compVolume == 0 ? '55%' : '49%',
                              top: '56%',
                              style: {
                                text: Totol.compVolume || 0 + 'm³',
                                textAlign: 'right',
                                //fill: '#999',　　　　　　　　//文字的颜色
                                width: RFT * 4,
                                height: RFT * 4,
                                fontSize: RFT * 3.8,
                                //color: "#4d4f5c",
                                //fontFamily: "Microsoft YaHei"
                              }
                            }],
                            series: [
                              {
                                name: '库存量：',
                                type: 'pie',
                                radius: ['50%', '70%'],
                                center: ['59%', '50%'],
                                avoidLabelOverlap: false,
                                label: {
                                  normal: {
                                    show: true,
                                    position: 'outside',
                                    textStyle: {
                                      fontSize: RFT * 3.5,
                                      color: '#535c68'
                                    },
                                    formatter: '{d}%'
                                  },
                                  emphasis: {
                                    show: true,
                                    textStyle: {
                                      fontSize: RFT * 3.5,
                                      fontWeight: 'bold'
                                    }
                                  }
                                },
                                labelLine: {
                                  normal: {
                                    lineStyle: {
                                      show: false,
                                      color: '#235894'
                                    }
                                  }
                                },
                                data: this.state.resData
                              }
                            ]
                          }} />
                      </View>
                    </View>
                  </View> :
                  <View></View>
              }

              {
                Url.Menu.indexOf('APP成品库存可视化') != -1 ?
                  <View style={{ height: width }}>
                    <View style={{ flexDirection: 'row', }}>
                      <Text style={{ flex: 1, fontSize: RFT * 3.6, margin: 15, marginTop: 10, textAlignVertical: 'center', color: '#535c68' }}>成品库存可视化</Text>

                      <Picker
                        mode='dialog'
                        selectedValue={pickerValue || ''}
                        style={{ flex: 1, fontSize: 10, fontWeight: 'bold', textAlign: 'right', padding: 0, height: 42 }}
                        onValueChange={(value) => {
                          this.setState({ pickerValue: value, })
                        }}
                        itemStyle={{ fontSize: 10, }}

                      >
                        <Picker.Item label={'请选择'} value={'请选择堆场'} />
                        {stockNameData.map((item) => {
                          return (
                            <Picker.Item label={item} value={item} />
                          )
                        })}
                      </Picker>
                    </View>
                    <View style={{ height: deviceWidth * 0.6 }}>
                      <View style={{ width: width * 0.94, marginLeft: width * 0.03, height: deviceWidth * 0.6, backgroundColor: '#FFFFFF', borderRadius: 10 }}>
                        <FlatList
                          horizontal={true}
                          scrollEnabled={true}
                          //numColumns={2}
                          renderItem={this._renderItem}
                          data={resDataVisualized} />
                      </View>
                    </View>

                  </View> :
                  <View></View>
              }

            </View>

          </ScrollView>


        </View>
      );
    }
  }

  

}

const styles = StyleSheet.create({
  text: {
    fontSize: RFT * 3.6,
    margin: 15,
    color: '#535c68'
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
  actionButtonText: {
    color: 'white',
    fontSize: 14,
  },
  contentView: {
    marginHorizontal: width * 0.04,
    marginTop: width * 0.04,
    paddingHorizontal: 0,
    paddingTop: 5,
    paddingBottom: 15,
    backgroundColor: 'white',
    borderRadius: 10
  },
  card: {
    marginTop: 10,
    borderRadius: 5,
    width: (width * 0.78) / 3,
    height: (width - 120) / 3,
    marginLeft: 0,
  },
  titleView: {
    flexDirection: 'row',
    top: RFT * 2.4,
    left: RFT * 2
  },
  badge: {
    borderRadius: 40,
    backgroundColor: 'white',
    height: RFT * 1.3,
    width: RFT * 1.3,
    marginRight: 6,
    top: 8
  },
  title: {
    //textAlign: '',
    //marginLeft: (RFT * 3) + 12,
    color: 'white',
    fontSize: RFT * 4.1,
    fontWeight: '100',
  },
  text1: {
    //textAlign: 'center',
    marginLeft: (RFT * 2) + 10,
    color: 'white',
    fontSize: RFT * 4.2,
    fontWeight: '700',
    marginTop: 13,
  },
  text2: {
    //textAlign: 'center',
    marginLeft: (RFT * 2) + 10,
    fontSize: RFT * 3.9,
    color: 'white',
    marginTop: 0,
  },
  table_line: {
    flexDirection: 'row',
    borderLeftWidth: 1,
    borderLeftColor: '#E9E9E9',
    borderTopWidth: 1,
    borderTopColor: '#E9E9E9',
    borderRightWidth: 1,
    borderRightColor: '#E9E9E9',
    height: 40
  },
  table_item_title: {
    flex: 1,
    //paddingLeft: 20
  },
  table_item_text: {
    lineHeight: 40,
    fontSize: 12
  },
  table_item_title_text: {
    left: 20
  },
  table_item_value_text: {
    textAlign: 'center'
  },
  table_item_double_text: {
    fontSize: 12,
    lineHeight: 80
  },
  table_item_text_fir: {
    fontSize: 12,
    lineHeight: 39,
    textAlign: 'center'
  },
  table_item_text_sec: {
    fontSize: 12,
    lineHeight: 40,
    textAlign: 'center'
  },
  table_item_value: {
    flex: 1.6,
    //paddingLeft: 20,
    borderLeftWidth: 1,
    borderLeftColor: '#e9e9e9'
  },
  table_line_double: {
    flexDirection: 'row',
    borderLeftWidth: 1,
    borderLeftColor: '#E9E9E9',
    borderTopWidth: 1,
    borderTopColor: '#E9E9E9',
    borderRightWidth: 1,
    borderRightColor: '#E9E9E9',
    height: 80,
  },
  table_item_double_title: {
    flex: 1,
    flexDirection: 'row',
    // borderLeftWidth: 1,
    // borderLeftColor: '#e9e9e9'
  },
  table_item_double_value: {
    flex: 1.6,
    flexDirection: 'row',
    borderLeftWidth: 1,
    borderLeftColor: '#e9e9e9'
  },
  table_item_doubleItem_value: {
    //paddingLeft: 20,
  },
  table_item_value_bottomline: {
    borderBottomWidth: 1,
    borderBottomColor: '#e9e9e9'
  },
  chart_container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
    backgroundColor: 'transparent'
  },
})


