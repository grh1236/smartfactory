import React from 'react';
import {
  View, StyleSheet, ScrollView, Alert, AppState, ActivityIndicator,
  Linking, Animated, DeviceEventEmitter, TouchableOpacity, Dimensions, StatusBar, NativeModules
} from 'react-native';

import { Text, Card, Overlay, Header, Icon, Image, Button } from 'react-native-elements';
import ButtonGroupMain from '../Components/ButtonGroupMain1'
import FacProduceOutStatisticScreen from '../Screen/FacProduceOutStatisticScreen';
import BarChartScreen from '../Components/BarChartScreen'
import ProjectAnalysisMonth from '../Screen/projectAnalysisMonth'
import CtrlLineChart from '../Components/CtrlLineChart'
import { ToDay } from '../../CommonComponents/Data'
import _updateConfig from '../../../update.json';
import Url from '../../Url/Url';
import JPush from 'jpush-react-native';
import DeviceStorage from '../../Url/DeviceStorage';
import { RPX, RFT, deviceWidth, pixelRatio, deviceHeight, } from '../../Url/Pixal';
//import ActionButton from 'react-native-action-button';
import Suspension from '../Components/Suspension';
import PushUtil from '../../CommonComponents/PushUtil'
import OpenSettingsUtil from '../../CommonComponents/OpenSettingsUtil'
import MineView from '../../MineView/MineView'
import LinearGradient from 'react-native-linear-gradient';
import { NavigationEvents } from 'react-navigation';
import CardMain from '../Components/CardMain';
import { MainStyles } from '../Components/MainStyles';

console.log("🚀 ~ Url:", Url)


//import { NativeEventEmitter, NativeModules } from 'react-native';
//const { CommonEventEmitter } = NativeModules;

let listeners = {};
const NotificationEvent = 'NotificationEvent'

const { height, width } = Dimensions.get('screen')

class BackIcon extends React.PureComponent {
  render() {
    return (
      <TouchableOpacity
        onPress={this.props.onPress} >
        <Icon
          name='arrow-back'
          color='#419FFF'
          size={18} />
      </TouchableOpacity>
    )
  }
}

export default class DailyScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      ToDay: ToDay,
      user: '1',
      isVisible: false,
      title: '',
      content: '',
      flag: false,
      navi: '',
      facAnalysisLoading: true,
      isNoticeCheckPage: false,
      isLoading: true,
      resDataToday: {
        proNumber: 0,
        proVolume: 0,
        storageNumber: 0,
        storageVolume: 0,
        outNumber: 0,
        outVolume: 0
      }, resDataYesterday: {
        proNumber: 0,
        proVolume: 0,
        storageNumber: 0,
        storageVolume: 0,
        outNumber: 0,
        outVolume: 0
      }, resDataWeek: {
        proNumber: 0,
        proVolume: 0,
        storageNumber: 0,
        storageVolume: 0,
        outNumber: 0,
        outVolume: 0
      }, resDataMonth: {
        proNumber: 0,
        proVolume: 0,
        storageNumber: 0,
        storageVolume: 0,
        outNumber: 0,
        outVolume: 0
      }
    }
  }

  componentWillMount() {
    Url.Login()
    DeviceStorage.get('user')
      .then(resuser => {
        DeviceStorage.get('FacName')
          .then(resFac => {

          }).catch(err => {
            console.log('error')
          });
      }).catch(err => {
        console.log('error')
      });
  }

  notificationListener = result => {
    /*  console.log("notificationListener:" + JSON.stringify(result))
     let myDate = new Date();
     let time = myDate.getHours() + ':' + myDate.getMinutes() + ':' + myDate.getSeconds()
     result._createtime = ToDay + ' ' + time
     
     //console.log("notificationListener:" + JSON.stringify(result))
     Url.NoticeList.push(result);
     console.log("notificationList:" + JSON.stringify(Url.NoticeList)) */
    this.setState({ newMessage: true, navi: result.content })
  };

  stateHandle = (nextAppState) => {
    console.log("🚀 ~ file: Daily.js ~ line 71 ~ DailyScreen ~ AppState.nextAppState", nextAppState)
    console.log("🚀 ~ file: Daily.js ~ line 74 ~ DailyScreen ~ AppState.currentState", AppState.currentState)

    if (nextAppState != null && nextAppState === 'active') {
      DeviceStorage.get('newMessage').then(newMessage => {
        if (newMessage) {
          DeviceStorage.get('resultBody').then(resultBody => {
            let navi = resultBody
            if (navi.length != 0) {
              if (navi.indexOf('流程消息') != -1) {
                DeviceStorage.get('password')
                  .then(respass => {
                    let url = Url.urlsys + '/appviews/flowcenter/index.html?s_c_bizrangeid=' + Url.Fidweb + '&treeno=0181002001&s_employeeid=' + Url.EmployeeId + '&s_username=' + Url.username + '&s_userpwd=' + respass
                    /* this.props.navigation.navigate('Process', {
                      url: url
                    }) */
                    this.props.navigation.navigate('Mine', {
                      url: url
                    })
                  })
                //DeviceStorage.save("newMessage", false)
              } else if (navi.indexOf('日报') != -1) {
                this.props.navigation.navigate('DayStatistic', {
                  title: '每日统计分析',
                  badge: '生产',
                  buttonID: 0
                })
              } else if (navi.indexOf('周报') != -1) {
                this.props.navigation.navigate('DayStatistic', {
                  title: '每日统计分析',
                  badge: '生产',
                  buttonID: 2
                })
              } else if (navi.indexOf('月报') != -1) {
                this.props.navigation.navigate('DayStatistic', {
                  title: '每日统计分析',
                  badge: '生产',
                  buttonID: 3
                })
              }
            }

          })
        }
      })
    }



    console.log(AppState.currentState === 'active' && nextAppState === 'active');
    /* if (nextAppState != null && nextAppState === 'active') {
      if (this.state.flag === true && this.state.newMessage) {
        // alert("从后台进入前台");
        console.log("APPSTATE", "从后台进入前台");
        if (navi.indexOf('日报') != -1) {
          this.props.navigation.navigate('DayStatistic', {
            title: '每日统计分析',
            badge: '生产',
            buttonID: 0
          })
        } else if (navi.indexOf('周报') != -1) {
          this.props.navigation.navigate('DayStatistic', {
            title: '每日统计分析',
            badge: '生产',
            buttonID: 2
          })
        } else if (navi.indexOf('月报') != -1) {
          this.props.navigation.navigate('DayStatistic', {
            title: '每日统计分析',
            badge: '生产',
            buttonID: 3
          })
        }
      }
      this.setState({ flag: false });
    }
    else if (nextAppState != null && nextAppState === 'background') {
      this.setState({ flag: true, newMessage: false })
    } else if (AppState.currentState === 'active') {
      console.log("在前台");
      if (this.state.flag === true && this.state.newMessage) {
        if (navi.indexOf('日报') != -1) {
          this.props.navigation.navigate('DayStatistic', {
            title: '每日统计分析',
            badge: '生产',
            buttonID: 0
          })
        } else if (navi.indexOf('周报') != -1) {
          this.props.navigation.navigate('DayStatistic', {
            title: '每日统计分析',
            badge: '生产',
            buttonID: 2
          })
        } else if (navi.indexOf('月报') != -1) {
          this.props.navigation.navigate('DayStatistic', {
            title: '每日统计分析',
            badge: '生产',
            buttonID: 3
          })
        }
      }

      this.setState({
        isVisible: !this.state.isVisible,
        title: result.title,
        content: result.content
      });
    } */
  }

  // 登出智慧看板
  logout = () => {
    //DeviceStorage.save('isLogin', false);
    //PushUtil.deleteTag(Url.ID + "_" + Url.deptCode, (code, remain) => {
    //    if (code == 200) {
    //    }
    //})
    //PushUtil.deleteTag(Url.ID, (code, remain) => {
    //    if (code == 200) {
    //    }
    //})
    //PushUtil.deleteTag(Url.deptCode, (code, remain) => {
    //    if (code == 200) {
    //    }
    //})
    //PushUtil.deleteAlias(Url.employeeIdAlias, "employeeId", (code) => {
    //    if (code == 200) {
    //    }
    //})
    Url.employeeIdAlias = ""
    JPush.deleteTags({ sequence: 1 })
    Url.NoticeList = []
    DeviceStorage.save('daily', false);
    DeviceStorage.save('project', false);
    DeviceStorage.save('stocks', false);
    DeviceStorage.save('business', false);
    DeviceStorage.save('model', false);
    DeviceStorage.save('changeFac', false);
    //this.props.navigation.navigate('Login');
    DeviceStorage.get('password')
      .then(res => {
        let formDataAuthor = new FormData();
        let dataAuthor = {};
        dataAuthor = {
          "action": "getmanyfactorypdalimit",
          "servicetype": "pda",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "userName": Url.username,
            "pwd": res
          }
        }
        formDataAuthor.append('jsonParam', JSON.stringify(dataAuthor))
        fetch(Url.PDAurl, {
          method: 'POST',
          headers: {
            'Content-Type': 'multipart/form-data'
          },
          body: formDataAuthor
        }).then(res => {
          return res.json();
        }).then(resData => {
          if (resData.status == '100') {
            //console.log('Daily262-----------' + JSON.stringify(resData))
            //FacName
            DeviceStorage.get('FacName')
              .then(name => {
                for (var i = 0; i < resData.result.length; i++) {
                  //console.log('Daily268-----------' + resData.result[i].factoryName)
                  if (resData.result[i].factoryName == name) {
                    let result = resData.result[i]
                    Url.PDAEmployeeId = result.EmployeeId
                    Url.PDAEmployeeName = result.EmployeeName
                    Url.PDAFid = result.factoryId
                    Url.PDAFname = result.factoryName
                    Url.PDAusername = this.state.username
                    Url.chengJianPaiZhao = result.chengJianPaiZhao
                    Url.isUsedDeliveryPlan = result.isUsedDeliveryPlan
                    Url.jiaoZhuPaiZhao = result.jiaoZhuPaiZhao || "YES"
                    Url.yinJianPaiZhao = result.yinJianPaiZhao || "YES"
                    Url.isUseTaiWanID = result.isUseTaiWanID || "YES"
                    Url.isResizePhoto = result.isResizePhoto || "600"
                    if (result.isUsePhotograph !== undefined) {
                      Url.isUsePhotograph = result.isUsePhotograph
                    }
                    let PDAauthorgray = [], gary = ''
                    gary = result.gray
                    PDAauthorgray = gary.split(",");
                    Url.PDAauthorgray = PDAauthorgray
                    Url.PDAnews = result.news
                    this.props.navigation.navigate('Main1');
                  }
                }
              })
          }
        }).catch(err => {
          console.log('Daily294-----------' + err)
        })
      }).catch(err => { console.log('error242-----------' + err) })
  }

  addUMNotificationListener = (result) => {
    console.log("🚀 ~ file: Daily.js ~ line 145 ~ DailyScreen ~ PushUtil.result", result)
  }

  clickUMNotificationListener = (result) => {
    console.log("🚀 ~ file: Daily.js ~ line 197 ~ DailyScreen ~ PushUtil.clickUMNotificationListener.result", result)
    let resultObj = JSON.parse(result)
    console.log("🚀 ~ file: Daily.js ~ line 198 ~ DailyScreen ~ PushUtil.clickUMNotificationListener.resultObj", resultObj)
    DeviceStorage.save("newMessage", true)
    DeviceStorage.save("resultObj", resultObj)
    DeviceStorage.save("resultBody", JSON.stringify(resultObj.body))
    // 智慧看板内收到随身工厂消息，点击跳转
    if (resultObj.body.title.split('-')[0] == '随身工厂') {
      // 验证是否有PDA权限
      DeviceStorage.get('MenuPDA')
        .then(res => {
          if (res) {
            DeviceStorage.save('changeModelPDA', true);
            this.logout()
            //this.props.navigation.navigate('Main1');
          }
        }).catch(err => { })
    }
    this.NoticeAction()
    this.setState({ newMessage: true, navi: JSON.stringify(resultObj.body) })
  }

  UMNoticeEventListener = (callback) => {
    this.UMNoticeListener = DeviceEventEmitter.addListener("UMNoticeListener", result => {
      callback(result)
    })
  }

  UMClickEventListener = (callback) => {
    this.UMClickListener = DeviceEventEmitter.addListener("UMNoticeClick", result => {
      callback(result)
    })
  }

  NoticeAction = () => {
    DeviceStorage.get('newMessage').then(newMessage => {
      if (newMessage) {
        DeviceStorage.get('resultBody').then(resultBody => {
          let navi = resultBody
          if (navi.length != 0) {
            if (navi.indexOf('流程') != -1) {
              DeviceStorage.get('password')
                .then(respass => {
                  let url = Url.urlsys + '/appviews/flowcenter/index.html?s_c_bizrangeid=' + Url.Fidweb + '&treeno=0181002001&s_employeeid=' + Url.EmployeeId + '&s_username=' + Url.username + '&s_userpwd=' + respass
                  this.props.navigation.navigate('Process', {
                    url: url
                  })
                })
              //DeviceStorage.save("newMessage", false)
            } else if (navi.indexOf('日报') != -1) {
              this.props.navigation.navigate('DayStatistic', {
                title: '每日统计分析',
                badge: '生产',
                buttonID: 0
              })
            } else if (navi.indexOf('周报') != -1) {
              this.props.navigation.navigate('DayStatistic', {
                title: '每日统计分析',
                badge: '生产',
                buttonID: 2
              })
            } else if (navi.indexOf('月报') != -1) {
              this.props.navigation.navigate('DayStatistic', {
                title: '每日统计分析',
                badge: '生产',
                buttonID: 3
              })
            }
          }

        })
      }
    })
  }

  openSettings = () => {
    Alert.alert('跳转消息设置', '是否去设置界面设置消息', [
      {
        text: '确定', onPress: () => {
          OpenSettingsUtil.openNetworkSettings(data => {
            console.log('call back data', data)
            DeviceStorage.save('isNoticeCheck', false);
          })
        }
      },
      { text: '取消' },
    ]);
  }

  componentDidMount() {

    Url.Login()

    if (Url.isStaticTypeOpen) {
      this.StaticTypeSwitch()
    } else {
      this.resDataContorl()
    }

    this._navListener = this.props.navigation.addListener('didFocus', () => {
      this.UpdateControl()
    });

    //JPush.setLoggerEnable(true);
    StatusBar.setTranslucent(false)

    DeviceStorage.get('isNoticeCheck')
      .then(res => {
        //this.openSettings()
        if (res == false) {
          this.setState({
            isNoticeCheckPage: false
          })
          //console.log(res)
        } else {
          this.setState({
            isNoticeCheckPage: true
          })
        }
      }).catch(err => { })



    //JPush.init();

    this.UMNoticeEventListener(this.addUMNotificationListener)

    this.UMClickEventListener(this.clickUMNotificationListener)

    //this.NoticeAction()

    //通知回调
    JPush.addNotificationListener(this.notificationListener);
    //AppState.addEventListener('change', this.stateHandle)
    //this.listener(this.notificationListener);
    //自定义消息回调

    //本地通知回调 todo
    this.localNotificationListener = result => {
      console.log("localNotificationListener:" + JSON.stringify(result))
    };
    //JPush.addLocalNoxtificationListener(this.localNotificationListener);

    this.RegistrationIDListener = result => {
      console.log("RegistrationIDListener:" + JSON.stringify(result))
    }
    JPush.getRegistrationID(this.RegistrationIDListener)

    this._navListener = this.props.navigation.addListener('didFocus', () => {
      StatusBar.setBackgroundColor('black')
      console.log("🚀 ~ file: Daily.js ~ line 457 ~ DailyScreen ~ this._navListener=this.props.navigation.addListener ~ didFocus", "didFocus")
      this.UpdateControl()
      this.addUMNotificationListener()
    });
    this.setTimer();

    if (Url.isStaticTypeOpen) {
      console.log("🚀 ~ file: Daily.js:491 ~ DailyScreen ~ componentDidMount ~ Url.isStaticTypeOpen:", Url.isStaticTypeOpen)
      this.staticTypeEmitter = DeviceEventEmitter.addListener('staticType', (result) => {
        console.log("🚀 ~ file: Daily.js:489 ~ DailyScreen ~ this.staticTypeEmitter=DeviceEventEmitter.addListener ~ result:", result)
        if (result) {
          this.StaticTypeSwitch(result)
        }
      })
    }

    console.log("🚀 ~ file: Daily.js:504 ~ DailyScreen ~ Url.StaticType:", Url.StaticType)

  }

  StaticTypeSwitch = (result) => {
    if (typeof result != 'undefined') {
      Url.StaticType = result
    }
    console.log("🚀 ~ file: Daily.js:497 ~ DailyScreen ~ Url.StaticType:", Url.StaticType)

    this.Suspen.setState({ selectedIndex: 0 })
    //this.ref.requestDataYesterday()
    //this.ref.requestDataWeek()
    this.projectAnalysis.requestUrl()
    this.CtrlLine.requestData()

    //房建
    if (Url.StaticType == '0/') {
      this.FacProduceOutStatistic.requestUrl(Url.url + Url.factory + Url.Fid + 'GetFacProduceOutStatisticYear')
      this.FactorySingleBarChart.requestData(Url.url + Url.factory + Url.Fid + 'GetFactoryAnalysisSingle')
      //this.ref.requestDataMonth(Url.url + Url.factory + Url.Fid + Url.Month)
      this.ref.requestDataPost('today', Url.url + Url.factory + Url.Fid + Url.Today)
    }
    //管片
    if (Url.StaticType == '1/') {
      this.FacProduceOutStatistic.requestUrl(Url.url + Url.factory + Url.Fid + 'GetFacDuctPieceProduceOutStatisticYear')
      this.FactorySingleBarChart.requestData(Url.url + Url.factory + Url.Fid + 'GetFactoryDuctPieceAnalysisSingle')
      //this.ref.requestDataMonth(Url.url + Url.factory + Url.Fid + 'GetFactoryDuctPieceComponentStatisticMonth')
      this.ref.requestDataPost('today', Url.url + Url.factory + Url.Fid + "1/GetFactoryDuctPieceComponentStatisticToday")
    }
    //批量
    if (Url.StaticType == '2/') {
      this.FacProduceOutStatistic.requestUrl(Url.url + Url.factory + Url.Fid + 'GetFacBatchProduceOutStatisticYear')
      this.FactorySingleBarChart.requestData(Url.url + Url.factory + Url.Fid + 'GetFactoryBatchAnalysisSingle')
      //this.ref.requestDataMonth(Url.url + Url.factory + Url.Fid + 'GetFactoryBatchComponentStatisticMonth')
      //this.ref.requestDataToday(Url.url + Url.factory + Url.Fid + "2/GetFactoryDuctPieceComponentStatisticToday")
      this.ref.requestDataPost('today', Url.url + Url.factory + Url.Fid + "2/GetFactoryDuctPieceComponentStatisticToday", "today")
    }
  }

  resDataContorl = () => {
    this.Suspen.setState({ selectedIndex: 0 })
    this.projectAnalysis.requestUrl()
    this.CtrlLine.requestData()
    this.FacProduceOutStatistic.requestUrl(Url.url + Url.factory + Url.Fid + 'GetFacProduceOutStatisticYear')
    this.FactorySingleBarChart.requestData(Url.url + Url.factory + Url.Fid + 'GetFactoryAnalysisSingle')
    this.ref.requestDataToday()
  }

  requestDataToday = (urlProps) => {
    let url = Url.url + Url.factory + Url.Fid + Url.Today;
    if (typeof urlProps != 'undefined') {
      url = urlProps
    }
    console.log("🚀 ~ file: Daily.js:502 ~ DailyScreen ~ url:", url)
    console.log(url)
    fetch(url, {
      credentials: 'include',
    }).then(res => {
      return res.json()
    }).then(resData => {
      console.log("🚀 ~ file: Daily.js:497 ~ DailyScreen ~ requestDataToday:", resData)
      this.setState({
        resDataToday: resData,
      });
    }).catch((error) => {
      console.log(error);
    });
  }

  requestDataYesterday = () => {
    const url = Url.url + Url.factory + Url.Fid + Url.StaticType + Url.Yesterday;
    fetch(url, {
      credentials: 'include',
    }).then(resYesterday => {
      return resYesterday.json()
    }).then(resDataYesterday => {
      this.setState({
        resDataYesterday: resDataYesterday,
      });
      console.log('Yester')
      console.log(resDataYesterday)
    }).catch((error) => {
      console.log(error);
    });
  }

  requestDataWeek = () => {
    const url = Url.url + Url.factory + Url.Fid + Url.StaticType + Url.Week;
    fetch(url, {
      credentials: 'include',
    }).then(resWeek => {
      return resWeek.json();
    }).then(resDataWeek => {
      this.setState({
        resDataWeek: resDataWeek,
      });
    }).catch((error) => {
      console.log(error);
    });
  }

  requestDataMonth = (urlProps) => {
    let url = Url.url + Url.factory + Url.Fid + Url.Month;
    if (typeof urlProps != 'undefined') {
      url = urlProps
    }
    fetch(url, {
      credentials: 'include',
    }).then(resMonth => {
      return resMonth.json()
    }).then(resDataMonth => {
      this.setState({
        resDataMonth: resDataMonth,
      });

    }).catch((error) => {
      console.log(error);
    });
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this.stateHandle)
    this._navListener.remove();
    this.UMNoticeListener.remove();
    this.UMClickListener.remove();
    JPush.removeListener()
    this.staticTypeEmitter && this.staticTypeEmitter.remove()
  }

  setTimer = () => {
    let timer = setTimeout(() => {
      clearTimeout(timer)
      this.setState({
        isLoading: false
      })
    }, 800)
  }

  UpdateControl = () => {
    const route = this.props.navigation.state
    DeviceEventEmitter.emit('route', route.routeName);
  }

  updateIndexFuncOld(selectedIndex) {
    if (this.ref !== null) {
      this.ref.setState({ buttonID: selectedIndex })
      if (Url.isStaticTypeOpen) {
        if (selectedIndex === 0) {
          if (Url.StaticType == '0/') {
            this.ref.requestDataToday(Url.url + Url.factory + Url.Fid + Url.Today);
          }
          if (Url.StaticType == '1/') {
            this.ref.requestDataToday(Url.url + Url.factory + Url.Fid + "1/GetFactoryDuctPieceComponentStatisticToday")
          }
          if (Url.StaticType == '2/') {
            this.ref.requestDataToday(Url.url + Url.factory + Url.Fid + "2/GetFactoryDuctPieceComponentStatisticToday")
          }
        } else if (selectedIndex == 1) {
          this.ref.requestDataYesterday();
        } else if (selectedIndex == 2) {
          this.ref.requestDataWeek();
        } else if (selectedIndex == 3) {
          if (Url.StaticType == '0/') {
            this.ref.requestDataMonth(Url.url + Url.factory + Url.Fid + Url.Month)
          }
          if (Url.StaticType == '1/') {
            this.ref.requestDataMonth(Url.url + Url.factory + Url.Fid + 'GetFactoryDuctPieceComponentStatisticMonth')
          }
          if (Url.StaticType == '2/') {
            this.ref.requestDataMonth(Url.url + Url.factory + Url.Fid + 'GetFactoryBatchComponentStatisticMonth')
          }
        }
      } else {
        if (selectedIndex === 0) {
          this.ref.requestDataToday();
        } else if (selectedIndex == 1) {
          this.ref.requestDataYesterday();
        } else if (selectedIndex == 2) {
          this.ref.requestDataWeek();
        } else if (selectedIndex == 3) {
          this.ref.requestDataMonth(Url.url + Url.factory + Url.Fid + Url.Month)
        }
      }

    }

  }

  updateIndexFunc(selectedIndex) {
    if (this.ref !== null) {
      this.ref.setState({ buttonID: selectedIndex })
      if (Url.isStaticTypeOpen) {
        if (selectedIndex === 0) {
          this.ref.requestDataPost('today')
        } else if (selectedIndex == 1) {
          this.ref.requestDataPost('yesterday')
        } else if (selectedIndex == 2) {
          this.ref.requestDataPost('week')
        } else if (selectedIndex == 3) {
          this.ref.requestDataPost('month')
        }
      } else {
        if (selectedIndex === 0) {
          this.ref.requestDataToday();
        } else if (selectedIndex == 1) {
          this.ref.requestDataYesterday();
        } else if (selectedIndex == 2) {
          this.ref.requestDataWeek();
        } else if (selectedIndex == 3) {
          this.ref.requestDataMonth(Url.url + Url.factory + Url.Fid + Url.Month)
        }
      }

    }

  }

  updateIndex(selectedIndex) {
    if (Url.isNewProductionCard) {
      this.updateIndexFunc(selectedIndex)
    } else {
      this.updateIndexFuncOld(selectedIndex)
    }
  }

  render() {
    return (
      <View>
        <StatusBar translucent={false} />
        <ScrollView style={{ backgroundColor: '#F9F9F9', marginBottom: -100 }} >

          <View style={{ height: 150 }}>
            <LinearGradient
              start={{ x: 1, y: 1 }} end={{ x: 0, y: 1 }}
              colors={['rgba(250,204,86,0.99)', 'rgba(246,172,34,0.99)']}
              style={{ width: width, height: width * 0.36, }}
            >
              <Text style={{ fontSize: RFT * 3.6, color: 'white', paddingLeft: width * 0.036, marginVertical: width * 0.02 }}>今天是{ToDay}</Text>
            </LinearGradient>
          </View>

          <View style={{ top: -115 }}>
            {
              Url.Menu.indexOf('APP月生产与发货分析') != -1 ?
                <View style={{ marginBottom: width * 0.02 }}>
                  <View style={MainStyles.chartContainerView}>
                    <View style={[MainStyles.chartView, { height: width * 0.6 }]}>
                      {/*  <View style={{ height: width * 0.87, marginVertical: width * 0.015 }}> */}
                      <Text style={MainStyles.chartTitle}>月生产与发货分析</Text>
                      <View style={[MainStyles.chart, {}]}>
                        <FacProduceOutStatisticScreen onRef={ref => this.FacProduceOutStatistic = ref} />
                      </View>
                      {/* </View> */}
                    </View>
                  </View>
                </View> : <View></View>
            }

            <CardMain
              onRef={ref => { this.ref = ref }}
              ToDay={this.props.ToDay}
            />

            {
              Url.Menu.indexOf('APP工厂产量分析') != -1 ?
                <View style={{ marginTop: width * 0.04 }}>
                  <View style={MainStyles.chartContainerView}>
                    <View style={[MainStyles.chartView, {}]}>
                      <Text style={MainStyles.chartTitle}>工厂产量分析</Text>
                      <View style={[MainStyles.chart, {}]}>
                        <BarChartScreen onRef={ref => { this.FactorySingleBarChart = ref }} />
                      </View>
                    </View>
                  </View>
                </View> : <View></View>
            }

            {
              Url.Menu.indexOf('APP月计划与实际产量分析') != -1 ?
                <View style={{ marginTop: width * 0.055, }}>
                  <View style={MainStyles.chartContainerView}>
                    <View style={[MainStyles.chartView, { height: width * 0.6 }]}>
                      <Text style={MainStyles.chartTitle}>月计划与实际产量分析</Text>
                      <View style={[MainStyles.chart, {}]}>
                        <ProjectAnalysisMonth ref={(ref) => { this.projectAnalysis = ref; }} />
                      </View>
                    </View>
                  </View>
                </View> : <View></View>
            }

            {
              Url.Menu.indexOf('APP生产曲线图') != -1 &&
              <View style={{ marginTop: width * 0.04, height: deviceWidth * 0.68, }}>
                <CtrlLineChart ref={(ref) => { this.CtrlLine = ref; }} />
              </View>
            }

          </View>


        </ScrollView>
        {/* <ActionButton
          buttonColor="#419FFF"
          onPress={() => {
            this.props.navigation.navigate('Camera', {
              mainpage: "首页",
              title: '构件扫码'
            })
          }}
          renderIcon={() => (<View style={styles.actionButtonView}><Icon type='material-community' name='qrcode-scan' color='#ffffff' />
            <Text style={styles.actionButtonText}>扫码</Text>
          </View>)}
        /> */}


        <Suspension ref={ref => { this.Suspen = ref }} title='今日' offsetY={200} offsetX={6} updateIndex={this.updateIndex.bind(this)} >

        </Suspension>

        {/*  <ActionButton
          buttonColor="#FDBF3A"
          verticalOrientation='up'
          //buttonText = {'今日'}
          renderIcon={() => <Text style={{ color: 'white', fontSize: 16 }}>今日</Text>}
        >
          <ActionButton.Item buttonColor='#FDBF3A'  >
            <Text style={{ color: 'white', fontSize: 16 }}>今日</Text>
          </ActionButton.Item>
          <ActionButton.Item buttonColor='#FDBF3A' title="Notifications" onPress={() => { }}>
          <Text style={{ color: 'white', fontSize: 16 }}>昨日</Text>
          </ActionButton.Item>
          <ActionButton.Item buttonColor='#FDBF3A' onPress={() => { }}>
          <Text style={{ color: 'white', fontSize: 16 }}>本周</Text>
          </ActionButton.Item>
        </ActionButton> */}

        <Overlay
          overlayStyle={{
            width: deviceWidth * 0.7,
            height: deviceWidth * 1.2
          }}
          animationType='fade'
          isVisible={this.state.isNoticeCheckPage}
          onRequestClose={() => {
            this.setState({ isNoticeCheckPage: !this.state.isNoticeCheckPage });
          }}
        >
          <Header
            containerStyle={{ height: 35, paddingTop: 0, top: -5 }}
            centerComponent={{ text: '开启通知引导', style: { color: 'gray', fontSize: 18 } }}
            rightComponent={<BackIcon
              onPress={() => {
                this.setState({
                  isNoticeCheckPage: !this.state.isNoticeCheckPage
                });
              }} />}
            backgroundColor='white'
          />
          <ScrollView horizontal={true}>
            <View >
              <Image
                resizeMethod='scale'
                resizeMode='contain'
                containerStyle={{ justifyContent: 'center', alignContent: 'center' }}
                style={{ height: deviceWidth * 0.8, width: deviceWidth * 0.3, marginRight: width * 0.2, marginLeft: width * 0.16 }}
                source={require('../../viewImg/NoticeSetting1.png')}
              />
              <Text style={{ marginLeft: width * 0.05 }}>1.点击按钮，跳转至APP设置界面</Text>
              <Text style={{ marginLeft: width * 0.15 }}>2.打开自启动选项</Text>
            </View>
            <View>
              <Image
                resizeMethod='scale'
                resizeMode='contain'
                containerStyle={{ justifyContent: 'center', alignContent: 'center' }}
                style={{ height: deviceWidth * 0.8, width: deviceWidth * 0.3, marginRight: width * 0.2, marginLeft: width * 0.1 }}
                source={require('../../viewImg/NoticeSetting2.png')}
              />
              <Text style={{ marginLeft: width * 0.04 }}>3.通知管理中打开通知选项</Text>
            </View>
            <View>
              <Image
                resizeMethod='scale'
                resizeMode='contain'
                containerStyle={{ justifyContent: 'center', alignContent: 'center' }}
                style={{ height: deviceWidth * 0.8, width: deviceWidth * 0.3, marginRight: width * 0.2, marginLeft: width * 0.1 }}
                source={require('../../viewImg/NoticeSetting3.png')}
              />
              <Text style={{ marginLeft: width * 0.06 }}>4.省电策略勾选无限制</Text>
            </View>
            <View>
              <Image
                resizeMethod='scale'
                resizeMode='contain'
                containerStyle={{ justifyContent: 'center', alignContent: 'center' }}
                style={{ height: deviceWidth * 0.8, width: deviceWidth * 0.3, marginRight: width * 0.2, marginLeft: width * 0.1 }}
                source={require('../../viewImg/NoticeSetting4.png')}
              />
              <Text style={{ marginLeft: width * 0.0 }}>5.应用后台管理勾选不会被清除</Text>
            </View>

          </ScrollView>
          <Button
            title='打开通知设置'
            type='outline'
            onPress={() => {
              OpenSettingsUtil.openNetworkSettings(data => {
                console.log('call back data', data)
              })
              DeviceStorage.save('isNoticeCheck', false);
            }}
            style={{ marginVertical: 6 }}
            containerStyle={{ marginVertical: 10 }}
            buttonStyle={{ backgroundColor: '#FFFFFF', borderRadius: 60 }}
          ></Button>


        </Overlay>

      </View>
    );
    //}

  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignContent: 'center',
    flex: 1
  },
  chartMainView: {
    height: width,
    borderRadius: 20,
    width: width / 0.9,
    backgroundColor: 'transparent'
  },
  title: {
    color: '#535c68',
    fontSize: RFT * 3.8,
    marginTop: width * 0.03,
    textAlign: 'center'
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
  actionButtonText: {
    color: 'white',
    fontSize: 14,
  }
})