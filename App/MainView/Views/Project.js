import React, { Component } from 'react';
import { Card, Text, Button, Overlay, Header, Icon, Badge, ListItem, SearchBar } from 'react-native-elements'
import { View, ScrollView, Picker, processColor, ActivityIndicator, Modal, Dimensions, SafeAreaView, Platform, StyleSheet, StatusBar, TouchableWithoutFeedbackBase, TouchableWithoutFeedback, DeviceEventEmitter, PanResponder, FlatList, TouchableOpacity } from 'react-native';
import { TouchableNativeFeedback } from 'react-native-gesture-handler';
import Url from '../../Url/Url';
import ProjectProcessAnalysis from '../Screen/projectProcessAnalysis';
import FlatListProjectTest from '../Components/FlatListProjectTest1';
import FlatListCount from '../Components/FlatListCount'
//import ButtonTop from '../Components/ButtonTop';
import { RFT, RVH, deviceWidth, deviceHeight, color } from '../../Url/Pixal';
import ActionButton from 'react-native-action-button';
import WebView from 'react-native-webview';
import LinearGradient from 'react-native-linear-gradient';
import { NavigationEvents } from 'react-navigation';
import { MainStyles } from '../Components/MainStyles';

const { height, width } = Dimensions.get('window')

let InNum = 0, InVolume = 0, OutNum = 0, OutVolume = 0;

export default class Project_Screen extends Component {
	constructor() {
		super();
		this.state = {
			focusButton: 0,
			buttonVisible: false,
			modalVisible: false,
			Loading: true,
			stateloading: true,
			countloading: true,
			projectName: '',
			projectAbb: '',
			Urlanalysis: '',
			Urlproject: ''//Url.url + Url.project + Url.Fid + 'AE37158119BD4502BA92485B2757248E/GetFacProductionSchedule2'
			,
			UrlCount: '' //'http://119.3.25.101:910/api/StatisticFactory/fa1119c1-ba8e-442c-b2e8-334fafbdae9e/GetStatisticFacPrjProduction/075981F601F2441C82C400C8A06AE08F',
			,
			UrlWeb: '',
			UrlProjectH5Chart: '',
			UrlProjectH5List: '',
			projectId: ' ',
			language: [{ itemValue: {} },],
			resData: [{
				"id": 1,
				"factoryId": "74e1cbc6-2cfc-4d6f-bcd1-739d28e4c74d",
				"projectId": "AE37158119BD4502BA92485B2757248E",
				"projectName": "怡安嘉园",
				"floorNoName": "1#",
				"floorNo": 1,
				"inProduced": 0,
				"notProduced": 2,
				"notInstall": 0,
				"install": 0,
				"release": 0
			},],
			title: '请点击',
			resDataButton: [{
				"factoryId": "74e1cbc6-2cfc-4d6f-bcd1-739d28e4c74d",
				"projectId": "5571FF4AEFC34F1DAFD9BDFFC2D99E7B",
				"projectName": "胜古家园",
				"projectAbb": "胜古家园",
				"city": "",
				"compCount": 892,
				"totalVolume": 758.200,
				"volumed": 39.100
			},],
			resDataCount: [{

			},],
			resCountData: [],
			allCount: {},
			newCountData: [],
			enableScrollViewScroll: true,
			isLoutuVisible: false,
			isLoutuLoading: false,
			isLoutuShow: true,
			isSetLoutuVisible: false,
			InNum: 0,
			InVolume: 0,
			OutNum: 0,
			OutVolume: 0,
			isScroll: true,
			projectSerchText: '',
			projectSelectArr: []
		}
	}

	componentDidMount() {
		Url.Login();

		console.log("🚀 ~ file: Project.js:90 ~ Project_Screen ~ componentDidMount ~ Url.isStaticTypeOpen:", Url.isStaticTypeOpen)
		console.log("🚀 ~ file: Project.js:90 ~ Project_Screen ~ componentDidMount ~ Url.StaticType:", Url.StaticType)

		if (Url.isStaticTypeOpen) {
			console.log("🚀 ~ file: Project.js:89 ~ Project_Screen ~ componentDidMount ~ Url.isStaticTypeOpen:", Url.isStaticTypeOpen)
			this.StaticTypeSwitch()
		} else {
			Url.StaticType = ''
			this.requestDataButton()
		}

		const route = this.props.navigation.state
		if (Url.StaticType != '2/') {
			this.setState({ isLoutuShow: true })
		} else {
			this.setState({ isLoutuShow: false })
		}
		DeviceEventEmitter.emit('route', route.routeName);
		this.requestDataButton();

		this._navListener = this.props.navigation.addListener('didFocus', () => {
			StatusBar.setBackgroundColor('black');
			StatusBar.setTranslucent(false)
			this.UpdateControl()
		});
		if (Url.isStaticTypeOpen) {
			console.log("🚀 ~ file: Project.js:113 ~ Project_Screen ~ componentDidMount ~ Url.isStaticTypeOpen:", Url.isStaticTypeOpen)
			this.staticTypeEmitterPrj = DeviceEventEmitter.addListener('staticType', (result) => {
				console.log("🚀 ~ file: Project.js:291 ~ Project_Screen ~ this.staticTypeEmitter=DeviceEventEmitter.addListener ~ result:", result)
				if (result) {
					if (!this.state.Loading) {
						Url.StaticType = result
						this.StaticTypeSwitch()
					}
				}
			})
		}

	}

	componentWillUnmount() {
		this._navListener.remove();
		this.staticTypeEmitterPrj && this.staticTypeEmitterPrj.remove()
		//controller.abort();
		const controller = new AbortController();
		let { signal } = controller;
		if (this.state.countloading) {
			controller.abort();
		} else {
			signal = null
		}
	}

	StaticTypeSwitch = () => {
		this.setState({ Loading: true })
		this.setState({
			focusButton: 0
		})
		if (Url.StaticType != '2/') {
			this.setState({ isLoutuShow: true })
		} else {
			this.setState({ isLoutuShow: false })
		}
		this.requestDataButton()
	}

	requestData = (urlprops) => {
		fetch(urlprops, {
			credentials: 'include',
		}).then(res => {
			return res.json()
		}).then(resData => {
			this.setState({
				resData: resData,
				stateloading: false,
			}, () => {
				if (typeof this.refchart != "null" && this.refchart !== null) {
					if (typeof this.refchart != 'undefined') {
						this.refchart.ReflashData(this.state.resData)
					}
				}
				//this.setState({	stateloading: false,})
			});
		}).catch(error => {
			console.log("Project_Screen -> requestData -> error", error)
			let url = this.state.Urlproject.replace(/GetFacProductionSchedule2/g, "GetFacProductionSchedule")
			fetch(url, {
				credentials: 'include',
			}).then(res => {
				return res.json()
			}).then(resData => {
				this.setState({
					resData: resData,
					stateloading: false,
				}, () => {
					if (Url.Menu.indexOf('APP形象进度图') != -1) {
						if (typeof this.refchart != "null" && this.refchart !== null) {
							if (typeof this.refchart != 'undefined') {
								this.refchart.ReflashData(this.state.resData)
							}
						}
					}
				});
			}).catch(err => {
				console.log("Project_Screen -> requestData -> err", err)
			})
		});
	}

	requestDataCount = (urlprops, signal) => {
		fetch(urlprops, {
			credentials: 'include',
			signal
		}).then(res => {
			return res.json();
		}).then(resDataCount => {
			console.log("🚀 ~ file: Project.js:245 ~ Project_Screen ~ resDataCount:", resDataCount)
			if ((resDataCount[0].projectId + '/') != this.state.projectId) {
				let url = Url.url + Url.factory + Url.Fid + 'GetStatisticFacPrjProduction/' + Url.StaticType + this.state.projectId
				this.requestDataCount(url)
				return
			}
			let newCountData = [];
			const length = resDataCount.length
			InNum = 0, InVolume = 0;
			this.setState({
				resDataCount: resDataCount,
				countloading: false,
			});
			for (let i = 0; i < resDataCount.length - 2; i++) {
				let item = resDataCount[i];
				resDataCount[i].color = color[i];
				newCountData.push(item)
				// newCountData[i] = item;
				InNum += item.compNum;
				InVolume += item.compVolume;
			};
			if (length == 13) {
				newCountData.splice(3, 0, resDataCount[12])
				newCountData[3].color = '#d35400'
				InNum += resDataCount[12].compNum;
				InVolume += resDataCount[12].compVolume;
				OutNum = resDataCount[length - 2].compNum - InNum;
				OutVolume = resDataCount[length - 2].compVolume - InVolume;
				this.setState({
					//resDataCount来自按钮选择参数传值
					resCountData: resDataCount,
					allCount: resDataCount[length - 2],
					newCountData: newCountData,
					countloading: false
				});
			} else {
				OutNum = resDataCount[length - 1].compNum - InNum;
				OutVolume = resDataCount[length - 1].compVolume - InVolume;
				this.setState({
					//resDataCount来自按钮选择参数传值
					resCountData: resDataCount,
					allCount: resDataCount[length - 1],
					newCountData: newCountData,
				});
			}
			console.log("🚀 ~ file: Project.js:219 ~ Project_Screen ~ allcount:", this.state.allCount)

			console.log("🚀 ~ file: Project.js:183 ~ Project_Screen ~ newCountData:", newCountData)


		}).catch((error) => {
			console.log("🚀 ~ file: Project.js:251 ~ Project_Screen ~ error:", error)
		});
	}

	requestDataAnalysis = (signal) => {
		const { Urlanalysis } = this.state;
		console.log("🚀 ~ file: Project.js:148 ~ Project_Screen ~ Urlanalysis:", Urlanalysis)
		fetch(Urlanalysis, {
			credentials: 'include',
			signal
		}).then(res => {
			return res.json();
		}).then(resData => {
			console.log("🚀 ~ file: Project.js:154 ~ Project_Screen ~ resData:", resData)
			if (resData[0] != '登录已超时,请重新登录') {
				resData.map((item, index) => {
					nameList.push(item.compTypeName);
					preVolumeList.push(item.preVolume);
					proVolumeList.push(item.proVolume);
					storageVolumeList.push(item.storageVolume);
					outVolumeList.push(item.outVolume);
				})
				Data.Name = nameList || [];
				Data.Vol1 = preVolumeList || [];
				Data.Vol2 = proVolumeList || [];
				Data.Vol3 = storageVolumeList || [];
				Data.Vol4 = outVolumeList || [];
			}

			this.refana.ReFlash(Data);

			this.setState({
				projectAnalysisMonth: Data,
				isLoading: false
			})
			//console.log(Data)
		}).catch((error) => {
			console.log(error);
		});
	}

	requestDataButton = () => {

		const uri = Url.url + Url.factory + Url.Fid + Url.StaticType + 'GetFactoryProjectsList';
		console.log("🚀 ~ file: Project.js:339 ~ Project_Screen ~ uri:", uri)
		fetch(uri, {
			credentials: 'include',
		}).then(resButton => {
			return resButton.json()
		}).then(resDataButton => {
			if (typeof resDataButton != 'undefined') {
				let re = /[\u4e00-\u9fa5]/;
				let buttonWidth = 100
				resDataButton.map((item, index) => {
					item.index = index
					let prjLen = 0, len = 0, ENlen = 0
					for (var i = 0; i < item.projectAbb.length; i++) {
						if (re.test(item.projectAbb.charAt(i))) {
							len++;
						} else {
							ENlen++;
						}
					}

					prjLen = len * 1.5 + ENlen
					if (prjLen > 9) {
						buttonWidth = 150
					}
					if (prjLen > 5 && prjLen <= 9) {
						buttonWidth = 125
					}
					if (prjLen > 0 && prjLen <= 5) {
						buttonWidth = 90
					}
					item.buttonWidth = buttonWidth
					if (index == 0) {
						item.buttonWidthTotal = buttonWidth + 6
					}
					// else if (index < 11) {
					// 	item.buttonWidthTotal = buttonWidth + resDataButton[index - 1].buttonWidthTotal + 6 + 6
					// } 
					else {
						item.buttonWidthTotal = buttonWidth + resDataButton[index - 1].buttonWidthTotal + 6 + 6
					}
				})
				console.log("🚀 ~ file: Project.js:334 ~ Project_Screen ~ resDataButton:", resDataButton)
				this.setState({
					resDataButton: resDataButton,
					projectSelectArr: resDataButton,
					Loading: false,
					countloading: true,
					stateloading: true,
				})
				for (let i = 0; i < resDataButton.length; i++) {
					const item = resDataButton[i];
					if (item.projectState == '生产中') {
						console.log("🚀 ~ file: Project.js:309 ~ Project_Screen ~ Url.StaticType:", Url.StaticType)
						let Urlanalysis = Url.url + Url.project + Url.Fid + item.projectId + '/' + Url.StaticType + 'GetFacProjectProcessAnaysis'
						console.log("🚀 ~ file: Project.js:307 ~ Project_Screen ~ Urlanalysis:", Urlanalysis)
						let Urlproject = Url.url + Url.project + Url.Fid + item.projectId + '/GetFacProductionSchedule2'
						let UrlCount = Url.url + Url.factory + Url.Fid + 'GetStatisticFacPrjProduction/' + Url.StaticType + item.projectId
						console.log("🚀 ~ file: Project.js:310 ~ Project_Screen ~ UrlCount:", UrlCount)
						this.setState({
							projectId: item.projectId + '/',
							Urlanalysis: Url.url + Url.project + Url.Fid + item.projectId + '/' + Url.StaticType + 'GetFacProjectProcessAnaysis',
							Urlproject: Url.url + Url.project + Url.Fid + item.projectId + '/GetFacProductionSchedule2',
							UrlCount: Url.url + Url.factory + Url.Fid + 'GetStatisticFacPrjProduction/' + Url.StaticType + item.projectId,
							UrlWeb: Url.urlsys + '/pcis/bimbox/WebUI/BIM1_MS.html?factoryId=' + Url.Fidweb + '&' + 'projectId=' + item.projectId,
							//UrlWeb: 'http://10.100.132.80:9100' + '/pcis/bimbox/WebUI/BIM1_S.html?factoryId=' + Url.Fidweb + '&' + 'projectId=' + item.projectId,
							UrlProjectH5Chart: Url.urlsys + '/assets/projectH5_fullScreen/projectSectionOne.html?isapp=1&isprj=0&' + 'factoryid=' + Url.Fidweb + '&' + 'projectid=' + item.projectId,
							UrlProjectH5List: Url.urlsys + '/assets/projectH5_fullScreen/projectSectionTwo.html?isapp=1&isprj=0&' + 'factoryid=' + Url.Fidweb + '&' + 'projectid=' + item.projectId,
							projectName: item.projectName,
							projectAbb: item.projectAbb
						});

						if (typeof this.refana != "null" && this.refana !== null) {
							if (typeof this.refana != 'undefined') {
								this.refana.ReFlash(Urlanalysis)
							}
						}

						this.requestData(Urlproject)
						this.requestDataCount(UrlCount)
						break;
					}
				}
			}
			//console.log(this.state.resDataButton)
		}).catch(err => {
			console.log("🚀 ~ file: Project.js:195 ~ Project_Screen ~ fetch ~ err", err.toString())
			this.requestDataButtonOld()
		})
	}

	requestDataButtonOld = () => {
		const uri = Url.url + Url.factory + Url.Fid + 'GetFactoryProjects';
		console.log(uri);
		fetch(uri, {
			credentials: 'include',
		}).then(resButton => {
			return resButton.json()
		}).then(resDataButton => {
			this.setState({
				resDataButton: resDataButton,
				Loading: false,
				countloading: true,
				stateloading: true,
			}, () => {
				for (let i = 0; i < resDataButton.length; i++) {
					const item = resDataButton[i];
					if (item.projectState == '生产中') {
						this.setState({
							projectId: item.projectId + '/',
							Urlanalysis: Url.url + Url.project + Url.Fid + item.projectId + '/' + Url.StaticType + 'GetFacProjectProcessAnaysis',
							Urlproject: Url.url + Url.project + Url.Fid + item.projectId + 'GetFacProductionSchedule2',
							UrlCount: Url.url + Url.factory + Url.Fid + '/GetStatisticFacPrjProduction/' + item.projectId,
							UrlWeb: Url.urlsys + '/pcis/bimbox/WebUI/BIM1_MS.html?factoryId=' + Url.Fidweb + '&' + 'projectId=' + item.projectId,
							//UrlWeb: 'http://10.100.132.80:9100' + '/pcis/bimbox/WebUI/BIM1_S.html?factoryId=' + Url.Fidweb + '&' + 'projectId=' + item.projectId,
							UrlProjectH5Chart: Url.urlsys + '/assets/projectH5_fullScreen/projectSectionOne.html?isapp=1&isprj=0&' + 'factoryid=' + Url.Fidweb + '&' + 'projectid=' + item.projectId,
							UrlProjectH5List: Url.urlsys + '/assets/projectH5_fullScreen/projectSectionTwo.html?isapp=1&isprj=0&' + 'factoryid=' + Url.Fidweb + '&' + 'projectid=' + item.projectId,
							projectName: item.projectName,
							projectAbb: item.projectAbb
						}, () => {
							console.log("🚀 ~ file: Project.js:3392 ~ Project_Screen ~ Urlanalysis:", this.state.Urlanalysis)
							console.log("🚀 ~ file: Project.js:33922 ~ Project_Screen ~ UrlCount:", this.state.UrlCount)
							this.refana.ReFlash(this.state.Urlanalysis)
							console.log("🚀 ~ file: Project.js:390 ~ Project_Screen ~ this.state.Urlanalysis:", this.state.Urlanalysis)
							this.requestData()
							this.requestDataCount();
						});
						break;
					}
				}

			})
			//console.log(this.state.resDataButton)
		})
	}

	ButtonTop(focusButton) {
		const { projectSerchText } = this.state
		let re = /[\u4e00-\u9fa5]/;
		let buttonWidth = 100
		return (
			<View style={{ flex: 1, marginBottom: 12, }}>
				<ScrollView>
					<View style={{ flexDirection: 'row' }}>
						<SearchBar
							platform='android'
							placeholder={'请输入搜索内容...'}
							containerStyle={{ backgroundColor: 'white', shadowColor: '#999', }}
							inputContainerStyle={{ backgroundColor: '#f9f9f9', borderRadius: 45, width: deviceWidth * 0.76 }}
							inputStyle={[{ color: '#333' }, deviceWidth <= 330 && { fontSize: 14 }]}
							round={true}
							cancelIcon={() => ({
								type: 'material',
								size: 25,
								color: 'rgba(0, 0, 0, 0.54)',
								name: 'search',
							})}
							value={projectSerchText}
							onChangeText={(text) => {
								text = text.replace(/\s+/g, "")
								text = text.replace(/<\/?.+?>/g, "")
								text = text.replace(/[\r\n]/g, "")
								this.setState({
									projectSerchText: text
								})
							}}
							returnKeyType='search'
							onClear={() => {
							}}
							onSubmitEditing={() => {
								this.onSearchDriver(projectSerchText)
							}} />
						<Button
							containerStyle={{ width: deviceWidth * 0.2 }}
							buttonStyle={{ borderRadius: 20, width: deviceWidth * 0.16, top: 12, left: 10 }}
							title='确定'
							onPress={() => {
								this.onSearchDriver(projectSerchText)
							}} />
					</View>
					<View style={{ flex: 1, flexDirection: 'row', flexWrap: 'wrap' }}>
						{
							this.state.projectSelectArr.map((item, index) => {
								if (item.projectState == '生产中') {
									return (
										<Button
											//key={index}
											buttonStyle={{
												borderRadius: 20,
												paddingHorizontal: 10,
												width: item.buttonWidth,
												//flex: 1,
												backgroundColor: focusButton == item.index ? '#419FFF' : 'white'
											}}
											containerStyle={{
												//flex: 1,
												borderRadius: 20,
												marginTop: 6,
												marginHorizontal: 6,
											}}
											title={item.projectAbb}
											titleStyle={{ fontSize: RFT * 3.5, color: focusButton == item.index ? 'white' : '#419FFF' }}
											type='outline'
											onPress={
												() => {
													this.scrollButton.scrollTo({ x: (item.buttonWidthTotal - item.buttonWidth) })
													this.setState({
														focusButton: item.index,
														countloading: true,
														stateloading: false,
														buttonVisible: !this.state.buttonVisible,
														projectId: item.projectId + '/',
														Urlanalysis: Url.url + Url.project + Url.Fid + item.projectId + '/' + Url.StaticType + 'GetFacProjectProcessAnaysis',
														Urlproject: Url.url + Url.project + Url.Fid + item.projectId + Url.StaticType + 'GetFacProductionSchedule2',
														UrlCount: Url.url + Url.factory + Url.Fid + '/GetStatisticFacPrjProduction/' + item.projectId,
														UrlWeb: Url.urlsys + '/pcis/bimbox/WebUI/BIM1_MS.html?factoryId=' + Url.Fidweb + '&' + 'projectId=' + item.projectId,
														//UrlWeb: 'http://10.100.132.80:9100' + '/pcis/bimbox/WebUI/BIM1_S.html?factoryId=' + Url.Fidweb + '&' + 'projectId=' + item.projectId,
														UrlProjectH5Chart: Url.urlsys + '/assets/projectH5_fullScreen/projectSectionOne.html?isapp=1&isprj=0&' + 'factoryid=' + Url.Fidweb + '&' + 'projectid=' + item.projectId,
														UrlProjectH5List: Url.urlsys + '/assets/projectH5_fullScreen/projectSectionTwo.html?isapp=1&isprj=0&' + 'factoryid=' + Url.Fidweb + '&' + 'projectid=' + item.projectId,
														projectName: item.projectName,
														projectAbb: item.projectAbb,
														projectSerchText: '',
														projectSelectArr: this.state.resDataButton
													});
													let Urlanalysis = Url.url + Url.project + Url.Fid + item.projectId + '/' + Url.StaticType + 'GetFacProjectProcessAnaysis'
													let Urlproject = Url.url + Url.project + Url.Fid + item.projectId + '/GetFacProductionSchedule2'
													let UrlCount = Url.url + Url.factory + Url.Fid + 'GetStatisticFacPrjProduction/' + Url.StaticType + item.projectId

													const controller = new AbortController();
													let { signal } = controller;
													if (this.state.countloading) {
														controller.abort();
													} else {
														signal = null
													}
													if (typeof this.refana != "null" && this.refana !== null) {
														if (typeof this.refana != 'undefined') {
															this.refana.ReFlash(Urlanalysis)
														}
													}
													this.requestData(Urlproject);
													this.requestDataCount(UrlCount, signal);
												}

											}
										/>
									)
								}
							})
						}
					</View>
				</ScrollView>
			</View>
		);
	}

	ButtonTopScreen = () => {
		const { focusButton, resDataButton } = this.state;
		let re = /[\u4e00-\u9fa5]/;
		let buttonWidth = 100

		return (
			resDataButton.map((item, index) => {
				item.index = index
				if (item.projectState == '生产中') {
					return (
						<Button
							//key={index}
							buttonStyle={{
								borderRadius: 20,
								justifyContent: 'flex-start',
								alignContent: 'flex-start',
								width: item.buttonWidth,
								backgroundColor: focusButton == item.index ? 'white' : 'rgba(255,255,255,0.3)'
							}}
							containerStyle={{
								justifyContent: 'flex-start',
								alignContent: 'flex-start',
								marginHorizontal: 6,
								borderRadius: 20,
								marginTop: 20,
							}}

							title={item.projectAbb}
							titleProps={{ numberOfLines: 1 }}
							titleStyle={{ fontSize: RFT * 3.5, color: focusButton == item.index ? 'black' : 'white', opacity: 1, marginLeft: 6, textAlign: 'left' }}
							type='outline'
							iconContainerStyle={{ marginRight: 10 }}
							icon={<Badge containerStyle={{ marginLeft: 6 }} badgeStyle={{ backgroundColor: focusButton == index ? "#419fff" : "white" }} />}
							onPress={() => {
								Url.Login()
								this.setState({
									focusButton: item.index,
									countloading: true,
									stateloading: true,
									projectId: item.projectId + '/',
									Urlanalysis: Url.url + Url.project + Url.Fid + item.projectId + '/' + Url.StaticType + 'GetFacProjectProcessAnaysis',
									Urlproject: Url.url + Url.project + Url.Fid + item.projectId + 'GetFacProductionSchedule2',
									UrlCount: Url.url + Url.factory + Url.Fid + '/GetStatisticFacPrjProduction/' + Url.StaticType + item.projectId,
									//UrlWeb: 'http://10.100.132.80:9100' + '/pcis/bimbox/WebUI/BIM1_S.html?factoryId=' + Url.Fidweb + '&' + 'projectId=' + item.projectId,
									UrlWeb: Url.urlsys + '/pcis/bimbox/WebUI/BIM1_MS.html?factoryId=' + Url.Fidweb + '&' + 'projectId=' + item.projectId,
									UrlProjectH5Chart: Url.urlsys + '/assets/projectH5_fullScreen/projectSectionOne.html?isapp=1&isprj=0&' + 'factoryid=' + Url.Fidweb + '&' + 'projectid=' + item.projectId + '&t=1',
									UrlProjectH5List: Url.urlsys + '/assets/projectH5_fullScreen/projectSectionTwo.html?isapp=1&isprj=0&' + 'factoryid=' + Url.Fidweb + '&' + 'projectid=' + item.projectId + '&t=1',
									projectName: item.projectName,
									projectAbb: item.projectAbb,
									projectSerchText: '',
									projectSelectArr: this.state.resDataButton
								}, () => {
									console.log("🚀 ~ file: Project.js:541 ~ Project_Screen ~ resDataButton.map ~ UrlProjectH5Chart:", this.state.UrlProjectH5Chart)

								});
								let Urlanalysis = Url.url + Url.project + Url.Fid + item.projectId + '/' + Url.StaticType + 'GetFacProjectProcessAnaysis'
								let Urlproject = Url.url + Url.project + Url.Fid + item.projectId + '/GetFacProductionSchedule2'
								let UrlCount = Url.url + Url.factory + Url.Fid + 'GetStatisticFacPrjProduction/' + Url.StaticType + item.projectId

								const controller = new AbortController();
								let { signal } = controller;
								if (this.state.countloading) {
									controller.abort();
								} else {
									signal = null
								}

								if (typeof this.refana != "null" && this.refana !== null) {
									if (typeof this.refana != 'undefined') {
										this.refana.ReFlash(Urlanalysis)
									}
								}
								this.requestData(Urlproject);
								this.requestDataCount(UrlCount, signal);
							}}
						/>
					)
				}
			})
		)
	}

	onSearchDriver = (text) => {
		const { resDataButton } = this.state
		let re = /[\u4E00-\u9FA5]/g; //测试中文字符的正则
		let textArr = []
		let searchArr = [[]]
		let reasonProjectArr = []
		if (text.length > 0) {
			textArr = text.split("")
			for (let i = 0; i < textArr.length; i++) {
				const tempsearchArr = searchArr.map(subset => {
					const one = subset.concat([]);
					one.push(textArr[i]);
					return one;
				})
				searchArr = searchArr.concat(tempsearchArr);
			}
			console.log("🚀 ~ file: produceStatisticPage.js:896 ~ updateIndex ~ searchArr:", searchArr)

			searchArr.map((item, index) => {
				if (item.length > 0) {
					//项目
					resDataButton.map((projectitem) => {
						let projectAbbStr = projectitem.projectAbb
						if (projectAbbStr.indexOf(item) != -1) {
							let tmp = projectitem
							reasonProjectArr.push(tmp)
						}
					})

				}
			})
			projectSelectArr = [...new Set(reasonProjectArr)]
			console.log("🚀 ~ file: produceStatisticPage.js:912 ~ updateIndex ~ projectSelectArr:", projectSelectArr)
			this.setState({
				projectSelectArr: projectSelectArr
			})
		} else {
			this.setState({
				projectSelectArr: resDataButton
			})
		}
	}

	UpdateControl = () => {
		Url.Login();
		const route = this.props.navigation.state
		console.log("🚀 ~ file: Daily.js ~ line 472 ~ DailyScreen ~ render ~ route", route.routeName)
		DeviceEventEmitter.emit('route', route.routeName);
	}

	render() {
		const { focusButton, Loading, isLoutuVisible, allCount, newCountData, resCountData, countloading } = this.state;
		if (Loading) {
			return (
				<View style={{
					flex: 1,
					flexDirection: 'row',
					justifyContent: 'center',
					alignItems: 'center',
					backgroundColor: '#F5FCFF',
				}}>
					<ActivityIndicator
						animating={true}
						color='#419FFF'
						size="large" />
				</View>
			)
		} else {
			return (
				<View>
					<NavigationEvents
						onWillFocus={this.UpdateControl}
						onDidBlur={() => {
							const controller = new AbortController();
							let { signal } = controller;
							if (this.state.countloading) {
								controller.abort();
							} else {
								signal = null
							}
						}} />
					<ScrollView
						ref={(ref) => { this.scroll = ref; }}
						style={{ backgroundColor: '#F9F9F9', marginBottom: -120 }}
						overScrollMode='never'
						nestedScrollEnabled={true}
						canCancelContentTouches={false}
						scrollEnabled={this.state.enableScrollViewScroll}>
						<View style={{ height: 150 }}>
							<LinearGradient
								start={{ x: 1, y: 1 }} end={{ x: 0, y: 1 }}
								colors={['rgba(65,216,255,0.88)', '#419FFF']}
								style={{ width: width, height: width * 0.45, }}
							>
							</LinearGradient>
						</View>
						<View style={{ top: -160, }}>
							<View style={{ flexDirection: 'row', marginBottom: 13 }}>
								<ScrollView ref={(ref) => { this.scrollButton = ref; }} scrollEnabled={true} horizontal={true} showsHorizontalScrollIndicator={false}>
									{this.ButtonTopScreen()}
								</ScrollView>
								<Button
									type='outline'
									title='更多'
									titleStyle={{ fontSize: RFT * 3.5, marginLeft: 6, textAlign: 'left', color: '#333' }}
									buttonStyle={{
										borderRadius: 100,
										backgroundColor: "white",
										justifyContent: 'flex-start',
										alignContent: 'flex-start',
										width: 70,
										flex: 1
									}}
									containerStyle={{
										//marginHorizontal: 6,
										borderRadius: 100,
										marginTop: 20,
									}}
									iconContainerStyle={{ marginRight: 10 }}
									icon={<Badge containerStyle={{ marginLeft: 6 }} badgeStyle={{ backgroundColor: "#419fff" }} />}

									onPress={() => {
										this.setState({
											projectSelectArr: this.state.resDataButton,
											buttonVisible: !this.state.buttonVisible,
											projectSerchText: ''
										})
									}} />
							</View>
							<Overlay
								fullScreen={true}
								animationType='fade'
								isVisible={this.state.buttonVisible}
								onRequestClose={() => {
									this.setState({ buttonVisible: !this.state.buttonVisible });
								}}>
								<Header
									leftComponent={
										Platform.OS === 'ios' ?
											<RefreshIcon
												onPress={this.requestDataButton.bind(this)} /> :
											null
									}
									containerStyle={{ height: 45, paddingTop: 0, top: -5 }}
									centerComponent={{ text: '项目选择表', style: { color: 'gray', fontSize: 20 } }}
									rightComponent={
										<Icon
											onPress={() => {
												console.log("🚀 ~ file: Project.js:708 ~ Project_Screen ~ render ~ onPress:")
												this.setState({
													buttonVisible: false
												});
											}}
											name='arrow-back'
											color='#419FFF' />
									}
									backgroundColor='white'
								/>
								{this.ButtonTop(focusButton)}
							</Overlay>


							<View style={Url.Menu.indexOf('APP项目进度分析') == -1 && { flex: 0, height: 0, width: 0 }}>
								<View style={[MainStyles.chartView, { height: deviceWidth * 0.61, }]}>
									<Text style={MainStyles.chartTitle}>{this.state.projectAbb + '项目进度分析'}</Text>
									<View style={MainStyles.chart}>
										<ProjectProcessAnalysis
											ref={refana => { this.refana = refana }} />
									</View>
								</View>
							</View>

							{
								Url.Menu.indexOf('APP项目进度图') != -1 ?
									this.state.isLoutuShow ?
										<View >
											<View style={{ flexDirection: 'row', flex: 1, justifyContent: 'space-between' }}>
												<Text onPress={() => { this.setState({ isSetLoutuVisible: true, isLoutuVisible: true }) }} style={styles.text}>项目生产统计</Text>
												<TouchableOpacity
													onPress={() => {
														// this.setState({
														// 	isLoutuVisible: true
														// })
														this.props.navigation.navigate('LoutuPage', {
															title: this.state.projectAbb,
															urlChart: this.state.UrlProjectH5Chart,
														})
													}}>
													<Text style={{ textAlign: 'right', left: -18, top: 11, color: '#419fff' }}>点击查看楼图</Text>
												</TouchableOpacity>
											</View>
											<View >
												<View style={{ marginTop: 4 }}>
													{
														countloading ?
															<View style={{
																height: 500,
																flexDirection: 'row',
																justifyContent: 'center',
																alignItems: 'center',
																backgroundColor: '#F5FCFF',
															}}>
																<ActivityIndicator
																	animating={true}
																	color='#419FFF'
																	size="large" />
															</View> : <View style={{ flex: 1, width: width * 0.94, marginLeft: width * 0.03, borderRadius: 10 }}>
																<ListItem
																	title={'生产总数：' + (allCount.compNum || 0) + '件' + '(' + (allCount.compVolume || 0) + 'm³)'}
																	titleStyle={[styles.textvalue, { fontSize: RFT * 4, textvalueAlignVertical: 'center' }]}
																	//contentContainerStyle={styles.content}
																	containerStyle={styles.container}
																	bottomDivider
																// chevron
																/>
																<FlatList
																	data={newCountData}
																	renderItem={this.countRenderItem}
																></FlatList>
																<ListItem
																	title={'已下单数:' + InNum + '件' + '(' + InVolume.toFixed(3) + 'm³)'}
																	titleStyle={styles.textvalue}
																	//contentContainerStyle={styles.content}
																	containerStyle={styles.container}
																	bottomDivider
																	leftElement={<Badge badgeStyle={{ backgroundColor: '#e74c3c' }} />}
																// chevron
																/>
																<ListItem
																	title={'未下单数:' + OutNum + '件' + '(' + OutVolume.toFixed(3) + 'm³)'}
																	titleStyle={styles.textvalue}
																	//contentContainerStyle={styles.content}
																	containerStyle={styles.container}
																	bottomDivider
																	leftElement={<Badge badgeStyle={{ backgroundColor: '#27ae60' }} />}
																// chevron
																/>
															</View>
													}

												</View>
											</View>
										</View> : <View></View>
									: <View></View>
							}

							<Overlay
								//overlayStyle={{ width: deviceWidth, height: deviceHeight * 0.7 }}
								fullScreen={true}
								isVisible={isLoutuVisible}
								onRequestClose={() => { this.setState({ isSetLoutuVisible: false, isLoutuVisible: false }) }}
								onBackdropPress={() => { this.setState({ isSetLoutuVisible: false, isLoutuVisible: false }) }}
							>
								<View style={{ flex: 1 }}>
									<Header
										containerStyle={{ height: 45, paddingTop: 0, top: -5 }}
										centerComponent={{ text: this.state.projectAbb, style: { color: 'gray', fontSize: 20 } }}
										rightComponent={
											<TouchableOpacity>
												<Icon
													onPress={() => {
														this.setState({
															isLoutuVisible: false
														});
													}}
													name='arrow-back'
													color='#419FFF' />
											</TouchableOpacity>
										}
										backgroundColor='white'
									/>
									<WebView
										source={{ uri: this.state.UrlProjectH5Chart }}
										onMessage={(e) => {
											let dataArr = e.nativeEvent.data.split('&')
											console.log("🚀 ~ file: Project.js:576 ~ Project_Screen ~ render ~ dataArr:", dataArr)

											let project = dataArr[1].split('=')
											let projectId = project[1]
											let floorNoIdArr = dataArr[2].split('=')
											let floorNoId = floorNoIdArr[1]
											let floorIdArr = dataArr[3].split('=')
											let floorId = floorIdArr[1]
											let compTypeIdArr = dataArr[4].split('=')
											let compTypeId = compTypeIdArr[1]
											let compstateArr = dataArr[5].split('=')
											let compState = compstateArr[1]
											let floorNoNameArr = dataArr[6].split('=')
											let floorNoName = floorNoNameArr[1]
											let floorNameArr = dataArr[8].split('=')
											let floorName = floorNameArr[1]
											let compTypeNameArr = dataArr[7].split('=')
											let compTypeName = compTypeNameArr[1]

											this.props.navigation.navigate('ChildOneNew', {
												title: '构件明细',
												sonTitle: "构件详情",
												url: Url.url + Url.Produce + 'PostProduceCompDetail',
												sonUrl: '',
												projectId: projectId,
												floorNoId: floorNoId,
												floorNoName: floorNoName,
												floorId: floorId,
												floorName: floorName,
												compTypeId: compTypeId,
												compTypeName: compTypeName,
												compState: compState
											})
											this.setState({ isSetLoutuVisible: true, isLoutuVisible: false })
										}}
									></WebView>
								</View>
							</Overlay>

							<WebView
								style={{ flex: 0, height: 0 }}
								source={{ uri: this.state.UrlProjectH5Chart }}
								onLoad={(event) => {
									let nativeEvent = event.nativeEvent
									console.log("🚀 ~ file: Project.js:8641 ~ Project_Screen ~ render ~ event.nativeEvent:", event.nativeEvent)
									if (nativeEvent.title.indexOf('404') != -1) {
										Url.isLoutuError = true
									} else {
										Url.isLoutuError = false
									}
								}}
							></WebView>

						</View>

					</ScrollView >

					{
						Url.Menu.indexOf('APP模型') != -1 ?
							<ActionButton
								buttonColor="#419FFF"
								onPress={() => {
									console.log("🚀 ~ file: Project.js:621 ~ Project_Screen ~ render ~ this.state.UrlWeb", this.state.UrlWeb)

									this.props.navigation.navigate('Web', {
										title: '项目模型页面',
										url: this.state.UrlWeb
									})
								}}
								renderIcon={() => (<View style={styles.actionButtonView}><Icon type='font-awesome' name='building' color='#ffffff' />
									<Text style={styles.actionButtonText}>模型</Text>
								</View>)}
							/> : <View></View>
					}
				</View >
			)
		}
	}

	countRenderItem = ({ item, index }) => {
		return (
			<TouchableOpacity
				onPress={() => {
					this.props.navigation.navigate('ChildOneMain', {
						url: Url.url + Url.Produce + Url.Fid + '/GetProjectProducedComponents/' + item.projectId + '/state/' + item.state,
						title: item.state,
						sonTitle: '构件信息'
					})
				}}>
				<ListItem
					title={item.state + ':  ' + item.compNum + '件 (' + item.compVolume + 'm³)'}
					titleStyle={styles.textvalue}
					//contentContainerStyle={styles.content}
					containerStyle={styles.container}
					bottomDivider
					leftElement={<Badge badgeStyle={{ backgroundColor: item.color }} />}
					chevron
				/>
			</TouchableOpacity>
		)
	}

}

const styles = StyleSheet.create({
	text: {
		fontSize: RFT * 4,
		marginTop: 10,
		marginBottom: 10,
		color: '#535c68',
		marginLeft: width * 0.04
	},
	actionButtonIcon: {
		fontSize: 20,
		height: 22,
		color: 'white',
	},
	actionButtonText: {
		color: 'white',
		fontSize: 14,
	},
	content: {
		//height: 100,

	},
	container: {
		//height: 10,
	},
	textname: {
		fontSize: RFT * 3.5,
		textAlignVertical: "center",
		color: 'gray'
		//marginEnd: 12,
		//marginVertical: 10,
	},
	textvalue: {
		fontSize: RFT * 3.5,
		textAlignVertical: "center",
		color: '#454545',
		//marginEnd: 12,
		//marginVertical: 10,
		//fontFamily:'STKaiti',
		//fontWeight: 'bold',
	},
})
