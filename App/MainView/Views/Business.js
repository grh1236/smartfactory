import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  ActivityIndicator,
  StatusBar,
  DeviceEventEmitter
} from 'react-native';
import { Card, Icon } from 'react-native-elements';
import StackBarChat from '../Components/SeasonBarChartBusiness';
import LineChartBusiness from '../Components/LineChartBusiness'
import { deviceWidth, RFT } from '../../Url/Pixal';
import Url from '../../Url/Url';
import ActionButton from 'react-native-action-button';
import LinearGradient from 'react-native-linear-gradient';
import { MainStyles } from '../Components/MainStyles';


export default class BusinessScreen extends Component {
  constructor() {
    super()
    this.state = {
      isLoading: true
    }
  }

  componentDidMount() {
    this.setTimer();
    this._navListener = this.props.navigation.addListener('didFocus', () => {
      StatusBar.setBackgroundColor('black');
      StatusBar.setTranslucent(false)
      this.UpdateControl()
    });
  }

  componentWillUnmount() {
    this._navListener.remove();
  }

  UpdateControl = () => {
    const route = this.props.navigation.state
    console.log("🚀 ~ file: Daily.js ~ line 472 ~ DailyScreen ~ render ~ route", route.routeName)
    DeviceEventEmitter.emit('route', route.routeName);
  }

  setTimer = () => {
    let timer = setTimeout(() => {
      clearTimeout(timer)
      this.setState({
        isLoading: false
      })
    }, 800)
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#F5FCFF',
        }}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )

    } else {
      return (
        <View style={{ flex: 1 }}>
          <ScrollView style={{ backgroundColor: '#F9F9F9', marginBottom: -100  }}>
            <View style={{ height: 150 }}>
              <LinearGradient
                start={{ x: 1, y: 1 }} end={{ x: 0, y: 1 }}
                colors={['rgba(80,159,240,0.88)', '#8579E8']}
                style={{ width: deviceWidth, height: deviceWidth * 0.36, }}
              >
              </LinearGradient>
            </View>

            <View style={{ top: -140, }}>
              {
                Url.Menu.indexOf('APP签约指标完成情况') != -1 &&
                <View>
                  <View style={MainStyles.chartContainerView}>
                    <View style={[MainStyles.chartView, { height: deviceWidth * 0.6, }]}>
                      <Text style={MainStyles.chartTitle}>签约指标完成情况</Text>
                      <View style={MainStyles.chart}>
                        <Text style={{ color: 'gray', fontSize: 12, marginHorizontal: 10, marginTop: 5 }}>单位：万元</Text>
                        <StackBarChat />
                      </View>
                    </View>
                  </View>
                </View>
              }

              {
                Url.Menu.indexOf('APP经营分析') != -1 &&
                <View style={{ marginTop: 25 }}>
                  <View style={MainStyles.chartContainerView}>
                    <View style={[MainStyles.chartView, { height: deviceWidth * 0.6, }]}>
                      <Text style={MainStyles.chartTitle}>经营分析</Text>
                      <View style={MainStyles.chart}>
                        <Text style={{ color: 'gray', fontSize: 12, marginHorizontal: 10, marginTop: 5 }}>单位：万元</Text>
                        <LineChartBusiness />
                      </View>
                    </View>
                  </View>
                </View>
              }
            </View>

          </ScrollView>

        </View>
      )
    }

  }
}

const styles = StyleSheet.create({
  text: {
    fontSize: RFT * 3.7,
    paddingLeft: 15,
    marginBottom: deviceWidth * 0.018,
    marginTop: deviceWidth * 0.018,
    textAlign: 'center',
    color: '#535c68'
  },
  chartview: {
    width: deviceWidth * 0.94,
    marginLeft: deviceWidth * 0.03,
    height: deviceWidth * 0.94,
    backgroundColor: '#FFFFFF',
    borderRadius: 10,
    marginBottom: 25
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
  actionButtonText: {
    color: 'white',
    fontSize: 14,
  }
})
