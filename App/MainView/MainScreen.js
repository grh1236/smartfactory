import React from 'react'
import { View, Image, Text, Alert, StatusBar, DeviceEventEmitter, TouchableOpacity } from 'react-native';
import { createStackNavigator, createMaterialTopTabNavigator, BottomTabBar, MaterialTopTabBar } from 'react-navigation';
import ModalDropdown from 'react-native-modal-dropdown';

import Daily from './Views/Daily'
import Project from './Views/Project'
import Web from './Views/Web'
import Stocks from './Views/Stocks';
import Business from './Views/Business'
import ProduceStatisticPage from './ChildPages/produceStatisticPage';
import ProjectProduceOutMonth from './ChildPages/projectProduceOutMonth';
import ProjectAnalysisMonth from './ChildPages/projectAnalysisMonth'
import Camera from '../CommonComponents/Camera';
import ChildOne from './ChildPages/ChildOneFlat'
import ChildOneNew from './ChildPages/ChildOneFlatNew'
import ChildTwo from './ChildPages/ChildrenPageTwo'
import LoutuPage from './ChildPages/LoutuPage'
import Url from '../Url/Url';
import DeviceStorage from '../Url/DeviceStorage'
import LinearGradient from 'react-native-linear-gradient';
import { deviceWidth } from '../Url/Pixal';
import { Badge, Icon, Overlay } from 'react-native-elements';
import { RFT } from '../Url/Pixal';

//LOGO的题头
class LogoTitle extends React.Component {
  constructor() {
    super();
    this.state = {
      Factoryname: '',
      route: '日常',
      colors: ['#F4BC2D', 'rgba(246,172,34,0.88)'],
      isSelectTypeShow: false,
      staticTypeName: '房建',
      selectDisabled: false,
    }
    this.drop = ['房建', '管片', '批量']
  }


  componentDidMount() {

    this.routeEmitter = DeviceEventEmitter.addListener('route',
      (result) => {
        console.log("🚀 ~ file: MainScreen.js ~ line 41 ~ LogoTitle ~ componentDidMount ~ result", result)

        if (result) {
          this.setState({
            route: result
          })
          let colors = []
          if (result == '日常') {
            colors = ['rgba(250,204,86,0.99)', 'rgba(246,172,34,0.99)']
          } else if (result == '项目') {
            colors = ['rgba(65,216,255,0.88)', '#419FFF']
          } else if (result == '堆场') {
            colors = ['rgba(16,237,195,0.88)', '#15D639']
          } else if (result == '经营') {
            colors = ['rgba(80,159,240,0.88)', '#8579E8']
          }
          this.setState({
            colors: colors
          })
        }


      });
    DeviceStorage.get('FacName')
      .then(resfac => {
        this.setState({
          Factoryname: resfac
        })
      }).catch(err => {
        console.log("🚀 ~ file: MainScreen.js:72 ~ LogoTitle ~ componentDidMount ~ err:", err)
      })
    if (Url.isStaticTypeOpen) {
      DeviceStorage.get('staticType')
        .then(resType => {
          console.log("🚀 ~ file: MainScreen.js:76 ~ LogoTitle ~ componentDidMount ~ resType:", resType)
          if (resType) {
            this.setState({
              staticTypeName: resType
            })
            if (resType == '房建') {
              Url.StaticType = '0/'
            }
            if (resType == '管片') {
              Url.StaticType = '1/'
            }
            if (resType == '批量') {
              Url.StaticType = '2/'
            }
            //DeviceEventEmitter.emit('staticType', Url.StaticType);
          }

        }).catch(err => {
          console.log("🚀 ~ file: MainScreen.js:82 ~ LogoTitle ~ componentDidMount ~ err:", err)
        })
    }

  }

  componentWillUnmount() {
    this.routeEmitter.remove();
  }

  render() {
    console.log("🚀 ~ file: MainScreen.js ~ line 74 ~ LogoTitle ~ render ~ render", this.props)
    return (
      <LinearGradient
        start={{ x: 1, y: 1 }} end={{ x: 0, y: 1 }}
        colors={this.state.colors}
        style={{ width: deviceWidth, }}
      >
        <View style={{ flex: 1, left: deviceWidth * 0.07 }}>
          <View style={{ flexDirection: 'row', top: -5 }}>
            <View style={{ left: -8, flex: 0.5 }}>
              {
                Url.isStaticTypeOpen ?
                  <ModalDropdown
                    options={this.drop}
                    disabled={this.state.selectDisabled}
                    defaultValue='选择'
                    style={{ top: 16, borderRadius: 5, borderColor: 'white', borderWidth: 0, width: 60 }}
                    defaultTextStyle={this.state.selectDisabled ? { color: 'lightgray', fontSize: 16, textAlign: 'right' } : { color: 'white', fontSize: 16, textAlign: 'right' }}
                    dropdownStyle={{ height: 39 * 3, width: 80, borderRadius: 5 }}    //下拉框样式
                    dropdownTextStyle={{ fontSize: 13 }}    //下拉框文本样式
                    onSelect={(index, options) => {
                      console.log("🚀 ~ file: MainScreen.js:99 ~ LogoTitle ~ render ~ options:", options)
                      console.log("🚀 ~ file: MainScreen.js:99 ~ LogoTitle ~ render ~ index:", index)
                      DeviceEventEmitter.emit('staticType', index + '/');
                      DeviceStorage.save('staticType', options)

                      this.setState({
                        staticTypeName: options,
                        selectDisabled: true
                      })

                      var timer = setTimeout(() => {
                        clearTimeout(timer)
                        this.setState({
                          selectDisabled: false
                        })
                      }, 2000)

                    }}
                  // renderRow={(option) => {
                  //   return(
                  //     <View style = {{flexDirection: 'row'}}>
                  //       <Badge status= 'success'></Badge>
                  //       <Text>{option}</Text>
                  //     </View>
                  //   )
                  // }}
                  // dropdownTextStyle = {{color: 'white'}}
                  >
                    <View style={{ flexDirection: 'row' }}>

                      <Text style={{ color: 'white', fontSize: 16, }}>{this.state.staticTypeName}</Text>
                      <Icon name='down' color='white' iconStyle={{ fontSize: 14, top: 4, marginLeft: 5, marginRight: -1 }} type='antdesign' ></Icon>
                    </View>
                  </ModalDropdown> : <View></View>
              }


              {/* <Text onPress={() => {
                this.setState({ isSelectTypeShow: true })
              }}>test</Text> */}
            </View>

            <View style={{ flex: 3 }}>
              <Text numberOfLines={1} style={{ marginLeft: 10, marginTop: 15, fontSize: 18, color: 'white', fontWeight: 'bold', textAlign: 'center' }} >{this.state.Factoryname}</Text>
            </View>
            <View style={{ flex: 1, marginTop: 16, left: -(RFT * 4) }}>
              <Icon onPress={() => {
                this.props.navigation.navigate('CameraMain', {
                  mainpage: "首页",
                  title: '构件扫码'
                })
              }}
                type='ant-design' name='scan1' size={24} color='#ffffff' />
            </View>

          </View>
        </View >
        <Overlay
          isVisible={this.state.isSelectTypeShow}
          fullScreen={false}
          overlayStyle={{
            //width: width * 0.55,
            //height: width * 1.1,
            backgroundColor: 'transparent',
            shadowColor: 'transparent',
            shadowOpacity: 0,
            shadowOffset: 0,
            borderColor: 'transparent',
            borderWidth: 0
          }}
          style={{
            backgroundColor: 'transparent',
            shadowColor: 'transparent',
            shadowOpacity: 0,
            shadowOffset: 0,
            backfaceVisibility: 'hidden'
          }}
          animationType='fade'
          onRequestClose={() => { this.setState({ isSelectTypeShow: false }) }}>
          <View style={{ height: 100, width: 100, backgroundColor: 'white' }}>

          </View>
        </Overlay>
      </LinearGradient >

      /* <View style={{ flexDirection: 'row', height: 28 }}>
        <Image
          source={require('../../logo.png')}
          style={{ width: 15, height: 26, marginLeft: 15 }}
        />
        <Text style={{ marginLeft: 10, fontSize: 18, color: 'black' }}>构力科技</Text>
      </View> */
    );
  }
}

const homeTopTab = createMaterialTopTabNavigator(
  {
    日常: {
      screen: Daily,
      navigationOptions: {
        tabBarComponent: props => <CustomtabBarComponent {...props} colors={['rgba(250,204,86,0.99)', 'rgba(246,172,34,0.99)']} />,
        // tabBarLabel:'消息页',    
        tabBarOnPress: (obj) => {
          if (Url.Daily) {
            obj.navigation.navigate('日常')
          } else {
            Alert.alert('权限提醒', '您还没有权限查看日常模块')
          }
        }
      },
    },
    项目: {
      screen: Project,
      navigationOptions: {
        tabBarComponent: props => <CustomtabBarComponent {...props} colors={['rgba(65,216,255,0.88)', '#419FFF']} />,
        // tabBarLabel:'消息页',    
        tabBarOnPress: (obj) => {
          console.log(Url.Project)
          if (Url.Project) {
            obj.navigation.navigate('项目')
          } else {
            Alert.alert('权限提醒', '您还没有权限查看项目模块')
          }
        },
        tabBarOnLongPress: (obj) => {
          console.log(Url.Project)
          if (Url.Project) {
            obj.navigation.navigate('项目')
          } else {
            Alert.alert('权限提醒', '您还没有权限查看项目模块')
          }

        }
      },
    },
    //模型: Web,
    堆场: {
      screen: Stocks,
      navigationOptions: {
        tabBarComponent: props => <CustomtabBarComponent {...props} colors={['rgba(16,237,195,0.88)', '#15D639']} />,
        // tabBarLabel:'消息页', 
        tabBarOnPress: (obj) => {
          if (Url.Stocks) {
            obj.navigation.navigate('堆场')
          } else {
            Alert.alert('权限提醒', '您还没有权限查看堆场模块')
          }
        },
        tabBarOnLongPress: (obj) => {
          if (Url.Stocks) {
            obj.navigation.navigate('堆场')
          } else {
            Alert.alert('权限提醒', '您还没有权限查看堆场模块')
          }
        }
      },
    },
    经营: {
      screen: Business,
      navigationOptions: {
        tabBarComponent: props => <CustomtabBarComponent {...props} colors={['rgba(80,159,240,0.88)', '#8579E8']} />,
        // tabBarLabel:'消息页', 
        tabBarOnPress: (obj) => {
          if (Url.Business) {
            obj.navigation.navigate('经营')
          } else {
            Alert.alert('权限提醒', '您还没有权限查看经营模块')
          }
        },
        tabBarOnLongPress: (obj) => {
          if (Url.Business) {
            obj.navigation.navigate('经营')
          } else {
            Alert.alert('权限提醒', '您还没有权限查看经营模块')
          }
        }
      },
    },
  }, {
  animationEnabled: true,
  tabBarOptions: {
    activeTintColor: 'white',
    tabStyle: {
      width: deviceWidth * 0.14,
      paddingTop: 3
    },
    labelStyle: {
      fontSize: 16,
      fontWeight: 'bold',
      width: deviceWidth * 0.1,
    },
    style: {
      backgroundColor: 'transparent'
    },
    allowFontScaling: false,
    inactiveTintColor: 'white',
    indicatorStyle: {
      backgroundColor: 'white',
      height: 3,
      width: deviceWidth * 0.1,
      bottom: 4,
      left: deviceWidth * 0.02,
      borderRadius: 15
    },
  },
  navigationOptions: {
    headerTransparent: false,
  },
  swipeEnabled: false,
  lazy: true,
  initialRouteName: '日常',
  resetOnBlur: true
}
)

const CustomtabBarComponent = (props) => {
  console.log("🚀 ~ file: MainScreen.js ~ line 172 ~ CustomtabBarComponent ~ props", props)
  return (
    <LinearGradient
      start={{ x: 1, y: 1 }} end={{ x: 0, y: 1 }}
      colors={props.colors}
    >
      <View onPress={Url.Login()}>
        <MaterialTopTabBar {...props} />
        {/*  <Icon style={{width: 15, left: 300, zIndex: 10000, backgroundColor: 'red' }} type='ant-design' name='scan'></Icon> */}
      </View>
    </LinearGradient>

  )
}



const MainStack = createStackNavigator({
  Main: {
    screen: homeTopTab,
    navigationOptions: props => {
      return {
        headerTitle: <LogoTitle {...props} />,
        headerStyle: {//设置导航条的样式。背景色，宽高等
          backgroundColor: 'white',
          borderBottomWidth: 0,
          borderBottomColor: 'transparent',
          elevation: 0,
          height: 35
          //marginTop: StatusBar.currentHeight
        },
      }
    }
  },
  ChildOneMain: ChildOne,
  ChildOneNew: ChildOneNew,
  ChildTwoMain: ChildTwo,
  LoutuPage: LoutuPage,
  Web: {
    screen: Web,
    navigationOptions: {
      headerTintColor: 'black',

    }
  },
  CameraMain: {
    screen: Camera,
    navigationOptions: {
      headerTintColor: 'black',
    }
  }, //决心按照一个共产党人的要求严格要求自己
  WebMain: {
    screen: Web,
    navigationOptions: {
      headerTintColor: 'black',
    }
  },
  DayStatistic: {
    screen: ProduceStatisticPage,
    navigationOptions: {
      headerTintColor: 'black',
    }
  },
  MonthStatistic: {
    screen: ProjectProduceOutMonth,
    navigationOptions: {
      headerTintColor: 'black',
    }
  },
  ProjectAnalysisMonthStatistic: {
    screen: ProjectAnalysisMonth,
    navigationOptions: {
      headerTintColor: 'black',
    }
  },
},
  {

    defaultNavigationOptions: {
      headerStyle: {//设置导航条的样式。背景色，宽高等
        backgroundColor: 'white',
        borderBottomWidth: 0,
        borderBottomColor: 'transparent',
        elevation: 0,
        //marginTop: StatusBar.currentHeight
      },
      headerTintColor: '#000',

      headerTitleStyle: {//设置导航条文字样式
        fontWeight: 'normal'
      }
    },
    navigationOptions: ({
      navigation
    }) => ({
      tabBarVisible: navigation.state.index > 0 ? false : true,
    }),
  }
)

export default MainStack;