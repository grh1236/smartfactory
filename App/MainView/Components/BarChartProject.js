import React from 'react';
import { StyleSheet, View, processColor, } from 'react-native';
import { Text } from 'react-native-elements';
import { HorizontalBarChart } from 'react-native-charts-wrapper';

class StackedBarChartScreen extends React.Component {

  constructor() {
    super();

    this.state = {
      legend: {
        enabled: true,
        textSize: 10,
        form: "SQUARE",
        formSize: 10,
        xEntrySpace: 20,
        yEntrySpace: 15,
        wordWrapEnabled: true, 
        horizontalAlignment: "CENTER",
        
        orientation: "HORIZONTAL",
      },
                                                
      data: {
        dataSets: [{
          values: [{ y: [800, 600, 300, 200], marker: ["已安装", "待安装", "在生产", "未生产"] }],
          label: '',
          config: {
            colors: [processColor('#419FFF'), processColor('#41CCFF'), processColor('#FF9D18'), processColor('#FFCF18')],
            stackLabels: ['已安装', '待安装', '在生产', '未生产'],
            horizontalAlignment: "CENTER",
          }
        }],
      },
      xAxis: {

        valueFormatter: [],
        granularityEnabled: true,
        granularity: 10,
        drawAxisLine: false,
        textColor: processColor("white"),
        gridColor: processColor("white"),

      },
      yAxis: {
        left: {
          enabled: false
        },
        right: {
          enabled: true,
          paddingTop: 40,
          granularityEnabled: true,
          granularity: 30,
          textColor: processColor("gray"),
          axisLineWidth: 0,
          
        },
      },
      description: {
        text: ' ',
        textSize: 15,
        textColor: processColor('darkgray'),

      }



    };
  }

  handleSelect(event) {
    let entry = event.nativeEvent
    if (entry == null) {
      this.setState({ ...this.state, selectedEntry: null })
    } else {
      this.setState({ ...this.state, selectedEntry: JSON.stringify(entry) })
    }

    console.log(event.nativeEvent)
  }

  render() {
    return (

      <View style={{ flex: 1 }}>
        <View style={styles.container}>
          <HorizontalBarChart
            style={styles.chart}
            xAxis={this.state.xAxis}
            yAxis={this.state.yAxis}
            data={this.state.data}
            legend={this.state.legend}
            drawValueAboveBar={false}
            marker={{
              enabled: true,
              markerColor: processColor('#F0C0FF8C'),
              textColor: processColor('white'),
              markerFontSize: 12,
            }}
            highlights={this.state.highlights}
            onSelect={this.handleSelect.bind(this)}
            onChange={(event) => console.log(event.nativeEvent)}
            chartDescription={this.state.description}
          />
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  chart: {
    flex: 1
  }
});


export default StackedBarChartScreen;
