import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { ButtonGroup, Button, Text } from 'react-native-elements'
import CardMain from './CardMain';
import Url from '../../Url/Url';

const Burl = Url.url + Url.factory + Url.Fid;

//按钮组定义，应该移到独立Button文件中
const component1 = () => <Text style={styles.txt}>今日</Text>;
const component2 = () => <Text style={styles.txt}>昨日</Text>
const component3 = () => <Text style={styles.txt}>近7天</Text>
const component4 = () => <Text style={styles.txt}>近30天</Text>
export default class ButtonGroupMain extends Component {
  constructor() {
    super();
    this.state = {
      //按钮组传参，还没明白
      selectedIndex: [],
      resDataToday: {
        proNumber: 0,
        proVolume: 0,
        storageNumber: 0,
        storageVolume: 0,
        outNumber: 0,
        outVolume: 0
      }, resDataYesterday: {
        proNumber: 0,
        proVolume: 0,
        storageNumber: 0,
        storageVolume: 0,
        outNumber: 0,
        outVolume: 0
      }, resDataWeek: {
        proNumber: 0,
        proVolume: 0,
        storageNumber: 0,
        storageVolume: 0,
        outNumber: 0,
        outVolume: 0
      }, resDataMonth: {
        proNumber: 0,
        proVolume: 0,
        storageNumber: 0,
        storageVolume: 0,
        outNumber: 0,
        outVolume: 0
      }
    };
    this.updateIndex = this.updateIndex.bind(this);
    this.requestDataToday = this.requestDataToday.bind(this);
    this.requestDataYesterday = this.requestDataYesterday.bind(this);
    this.requestDataWeek = this.requestDataWeek.bind(this);
    this.requestDataMonth = this.requestDataMonth.bind(this);

  }

  requestDataToday = () => {
    const url = Url.url + Url.factory + Url.Fid + Url.Today;
    console.log(url)
    fetch(url, {
      credentials: 'include',
    }).then(res => {
      return res.json()
    }).then(resData => {
      this.setState({
        resDataToday: resData,
      });
      console.log('Today:')
      console.log(resData)
    }).catch((error) => {
      console.log(error);
    });
  }

  requestDataYesterday = () => {
    const url = Url.url + Url.factory + Url.Fid + Url.Yesterday;
    fetch(url, {
      credentials: 'include',
    }).then(resYesterday => {
      return resYesterday.json()
    }).then(resDataYesterday => {
      this.setState({
        resDataYesterday: resDataYesterday,
      });
      console.log('Yester')
      console.log(resDataYesterday)
    }).catch((error) => {
      console.log(error);
    });
  }

  requestDataWeek = () => {
    const url = Url.url + Url.factory + Url.Fid + Url.Week;
    fetch(url, {
      credentials: 'include',
    }, {
      credentials: 'include',
    }).then(resWeek => {
      return resWeek.json();
    }).then(resDataWeek => {
      this.setState({
        resDataWeek: resDataWeek,
      });
    }).catch((error) => {
      console.log(error);
    });
  }

  requestDataMonth = () => {
    const url = Url.url + Url.factory + Url.Fid + Url.Month;
    fetch(url).then(resMonth => {
      return resMonth.json()
    }).then(resDataMonth => {
      this.setState({
        resDataMonth: resDataMonth,
      });

    }).catch((error) => {
      console.log(error);
    });
  }

 componentDidMount() {
    this.requestDataToday();
    this.requestDataYesterday();
    this.requestDataWeek();
    this.requestDataMonth();
  }

  reflash() {
    this.requestDataToday();
    this.requestDataYesterday();
    this.requestDataWeek();
    this.requestDataMonth();
  }

  updateIndex(selectedIndex) {
    this.reflash();
    this.setState({ selectedIndex })
    if (selectedIndex === 0) {
      //this.refs.testDataReflash.cardReflash(this.state.resDataToday);
      this.ref.cardReflash(this.state.resDataToday);
    } else if (selectedIndex == 1) {
      //this.refs.testDataReflash.cardReflash(this.state.resDataYesterday);
      this.ref.cardReflash(this.state.resDataYesterday);
    } else if (selectedIndex == 2) {
      //this.refs.testDataReflash.cardReflash(this.state.resDataWeek);
      this.ref.cardReflash(this.state.resDataWeek);
    } else if (selectedIndex == 3) {
      //this.refs.testDataReflash.cardReflash(this.state.resDataMonth);
      this.ref.cardReflash(this.state.resDataMonth);
    }
  }

  render() {
    //按钮组元素
    //console.log(this.props.Today)
    const buttons = [{ element: component1 }, { element: component2 }, { element: component3 }, { element: component4 }]
    const { selectedIndex } = this.state;
    return (
      <View>
        <ButtonGroup
          onPress={this.updateIndex}
          selectedIndex={selectedIndex}
          buttons={buttons}
          containerStyle={{ height: 40, borderRadius: 20 }}
          containerBorderRadius={10}
          selectedButtonStyle={{ backgroundColor: '#419FFF', }}

        >
        </ButtonGroup>
        <CardMain
         onRef={ref => {this.ref = ref}}
          ToDay = {this.props.ToDay}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  txt: {
    color: 'black',
  },
});
