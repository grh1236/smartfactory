import React, { Component } from 'react';
import { View, Dimensions, StyleSheet, PixelRatio, } from 'react-native';
import { ButtonGroup, Button, Text } from 'react-native-elements'
import CardMain from './CardMain';
import Url from '../../Url/Url';
//import {RFT, RVW} from '../../Url/Pixal';

const Burl = Url.url + Url.factory + Url.Fid;

const { height, width } = Dimensions.get('screen')
const fontScale = PixelRatio.getFontScale(); // 返回字体大小缩放比例
const pixelRatio = PixelRatio.get(); // 当前设备的像素密度
const RVW = width / 100;
const RFT = RVW / fontScale;

//按钮组定义，应该移到独立Button文件中
const component1 = () => <Text style={styles.txt}>今日</Text>;
const component2 = () => <Text style={styles.txt}>昨日</Text>
const component3 = () => <Text style={styles.txt}>近7天</Text>
const component4 = () => <Text style={styles.txt}>近30天</Text>
export default class ButtonGroupMain extends Component {
  constructor() {
    super();
    this.state = {
      //按钮组传参，还没明白
      selectedIndex: [],
      resDataToday: {
        proNumber: 0,
        proVolume: 0,
        storageNumber: 0,
        storageVolume: 0,
        outNumber: 0,
        outVolume: 0
      }, resDataYesterday: {
        proNumber: 0,
        proVolume: 0,
        storageNumber: 0,
        storageVolume: 0,
        outNumber: 0,
        outVolume: 0
      }, resDataWeek: {
        proNumber: 0,
        proVolume: 0,
        storageNumber: 0,
        storageVolume: 0,
        outNumber: 0,
        outVolume: 0
      }, resDataMonth: {
        proNumber: 0,
        proVolume: 0,
        storageNumber: 0,
        storageVolume: 0,
        outNumber: 0,
        outVolume: 0
      },
      focusButton: 0,
      buttonlist: [{ id: '0', name: '今日' }, { id: '1', name: '昨日' }, { id: '2', name: '本周' }, { id: '3', name: '本月' }],
    };
    this.updateIndex = this.updateIndex.bind(this);
    this.requestDataToday = this.requestDataToday.bind(this);
    this.requestDataYesterday = this.requestDataYesterday.bind(this);
    this.requestDataWeek = this.requestDataWeek.bind(this);
    this.requestDataMonth = this.requestDataMonth.bind(this);

  }

  requestDataToday = () => {
    const url = Url.url + Url.factory + Url.Fid + Url.Today;
    console.log(url)
    fetch(url, {
      credentials: 'include',
    }).then(res => {
      return res.json()
    }).then(resData => {
      this.setState({
        resDataToday: resData,
      });
      console.log('Today:')
      console.log(resData)
    }).catch((error) => {
      console.log(error);
    });
  }

  requestDataYesterday = () => {
    const url = Url.url + Url.factory + Url.Fid + Url.Yesterday;
    fetch(url, {
      credentials: 'include',
    }).then(resYesterday => {
      return resYesterday.json()
    }).then(resDataYesterday => {
      this.setState({
        resDataYesterday: resDataYesterday,
      });
      console.log('Yester')
      console.log(resDataYesterday)
    }).catch((error) => {
      console.log(error);
    });
  }

  requestDataWeek = () => {
    const url = Url.url + Url.factory + Url.Fid + Url.Week;
    fetch(url, {
      credentials: 'include',
    }).then(resWeek => {
      return resWeek.json();
    }).then(resDataWeek => {
      this.setState({
        resDataWeek: resDataWeek,
      });
    }).catch((error) => {
      console.log(error);
    });
  }

  requestDataMonth = () => {
    const url = Url.url + Url.factory + Url.Fid + Url.Month;
    fetch(url, {
      credentials: 'include',
    }).then(resMonth => {
      return resMonth.json()
    }).then(resDataMonth => {
      this.setState({
        resDataMonth: resDataMonth,
      });

    }).catch((error) => {
      console.log(error);
    });
  }

  componentDidMount() {
    this.requestDataToday();
    this.requestDataYesterday();
    this.requestDataWeek();
    this.requestDataMonth();
  }

  reflash() {
    this.requestDataToday();
    this.requestDataYesterday();
    this.requestDataWeek();
    this.requestDataMonth();
  }

  updateIndex(selectedIndex) {
    const { focusButton } = this.state
    this.reflash();
    if (selectedIndex === 0) {
      this.ref.cardReflash(this.state.resDataToday, focusButton);
    } else if (selectedIndex == 1) {
      this.ref.cardReflash(this.state.resDataYesterday, focusButton);
    } else if (selectedIndex == 2) {
      this.ref.cardReflash(this.state.resDataWeek, focusButton);
    } else if (selectedIndex == 3) {
      this.ref.cardReflash(this.state.resDataMonth, focusButton);
    }
  }

  render() {
    const { buttonlist, focusButton } = this.state;
    //按钮组元素
    //console.log(this.props.Today)
    const buttons = [{ element: component1 }, { element: component2 }, { element: component3 }, { element: component4 }]
    const { selectedIndex } = this.state;
    return (
      <View>
        <View style={styles.container}>
          {/* {
            buttonlist.map((item, index) => {
              return (
                <Button
                
                  buttonStyle={{
                    marginRight: 30,
                    borderRadius: 50,
                    shadowOpacity: 10,
                    borderColor: focusButton == index ? '#419FFF' : '#999999',
                    flex: 1,
                    backgroundColor: focusButton == index ? '#419FFF' : 'white'
                  }}
                  containerStyle={{
                    borderRadius: 1,
                  }}
                  title={'  ' + item.name + '  '}
                  titleStyle={{ fontFamily: 'STHeitiSC-Light', fontSize: RFT * 3.4, color: focusButton == index ? 'white' : '#333333' }}
                  type='outline'
                  onPress={() => {
                    this.setState({
                      focusButton: index,
                    }, () => {
                      this.updateIndex(index);
                    });
                  }} />
              );

            })
          } */}
        </View>
        <CardMain
          onRef={ref => { this.ref = ref }}
          ToDay={this.props.ToDay}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    paddingLeft: width * 0.036,
    paddingVertical: width * 0.01
  },
  txt: {
    color: 'black',
  },
});
