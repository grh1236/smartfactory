import React, { Component } from 'react';
import { View, TouchableOpacity, ListView, Dimensions, StyleSheet, PixelRatio, Animated, Easing, ActivityIndicator } from 'react-native';
import { withNavigation } from 'react-navigation';
import { Card, Text } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url'
import { ToDay, YesterDay, BeforeYesterDay } from '../../CommonComponents/Data';

const { height, width } = Dimensions.get('screen')
const fontScale = PixelRatio.getFontScale(); // 返回字体大小缩放比例
const pixelRatio = PixelRatio.get(); // 当前设备的像素密度
const RVW = width / 100;
const RFT = RVW / fontScale;

const moment = require('moment');

//卡片
class CardToday extends Component {
  constructor(props) {
    super(props);
    this.state = {
      buttonID: 0,
      //初始显示数据，应该从接口返回今天的数据
      testData: {
        proNumber: 0,
        proVolume: 0.00,
        storageNumber: 0,
        storageVolume: 0.00,
        outNumber: 0,
        outVolume: 0.00
      },
      animatedValue: new Animated.Value(0),
      color: 'red',
      text: '正面',
      isLoading: false
    }
    this.rotateAnimated = Animated.timing(
      this.state.animatedValue,
      {
        toValue: 1,
        duration: 3000,
        easing: Easing.in,
      }
    );
    this.requestDataToday = this.requestDataToday.bind(this);
  }

  componentDidMount() {
    this.timerToday = setTimeout(() => {
      //this.requestDataToday();
    }, 300)
    //
  }

  _startAnimated() {
    this.toast.show('开发中')
    this.timer = setTimeout(
      () => {
        if (this.state.color === 'red') {
          this.setState({
            color: 'blue',
            text: '反面'
          })
        } else {
          this.setState({
            color: 'red',
            text: '正面'
          })
        }
      },//延时操作
      1000       //延时时间
    );
    this.state.animatedValue.setValue(0);
    this.rotateAnimated.start();
  }

  componentWillUnmount() {
    // 如果存在this.timer，则使用clearTimeout清空。
    // 如果你使用多个timer，那么用多个变量，或者用个数组来保存引用，然后逐个clear
    this.timer && clearTimeout(this.timer);
    this.timerToday && clearTimeout(this.timerToday);
  }

  requestDataPost = (buttonDay, urlprops) => {
    console.log("🚀 ~ file: CardMain.js:85 ~ CardToday ~ urlprops:", urlprops)
    console.log("🚀 ~ file: CardMain.js:85 ~ CardToday ~ buttonDay:", buttonDay)
    this.setState({ isLoading: true })
    let url = Url.url + Url.project + 'GetFactoryPrjComponentStatisticDateRange'
    console.log("🚀 ~ file: CardMain.js:87 ~ CardToday ~ url:", url)
    let data = {
      "type": Url.StaticType.substring(0, 1),
      "dateType": buttonDay,
      "factoryId": Url.Fidweb,
      "projectId": "",
      "startDate": moment().format('YYYY-MM-DD'),
      "endDate": moment().format('YYYY-MM-DD')
    }
    console.log("🚀 ~ file: CardMain.js:87 ~ CardToday ~ data:", JSON.stringify(data))

    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
      .then(res => {
        if (res.status != 200) {
          return res.status
        }
        return res.json();
      })
      .then(resData => {
        if (typeof resData == 'number' && resData != 200) {
          Url.isNewProductionCard = false
          this.requestDataOld(urlprops, buttonDay)
          return
        }
        if (typeof resData != 'undefined') {
          Url.isNewProductionCard = true
          this.setState({
            testData: resData,
            isLoading: false
          });
        }

      }).catch(err => {
        console.log("🚀 ~ file: CardMain.js:132 ~ CardToday ~ err:", err)

      })
  }

  requestDataOld = (urlProps, date) => {
    this.setState({ isLoading: true })
    let url = Url.url + Url.factory + Url.Fid + Url.Today;
    if (typeof urlProps != 'undefined') {
      url = urlProps
    }
    console.log("🚀 ~ file: CardMain.js:83 ~ CardToday ~ url:", url)
    fetch(url, {
      credentials: 'include',
    }).then(res => {
      return res.json()
    }).then(resData => {
      console.log("🚀 ~ file: CardMain.js:94 ~ CardToday ~ resData:", resData)
      if (typeof resData != 'undefined') {
        this.setState({
          testData: resData,
          isLoading: false
        });
      }
    }).catch((error) => {
      console.log("🚀 ~ file: CardMain.js:99 ~ CardToday ~ error:", error)
    });
  }

  requestDataToday = (urlProps) => {
    this.setState({ isLoading: true })
    let url = Url.url + Url.factory + Url.Fid + Url.Today;
    if (typeof urlProps != 'undefined') {
      url = urlProps
    }
    console.log("🚀 ~ file: CardMain.js:83 ~ CardToday ~ url:", url)
    fetch(url, {
      credentials: 'include',
    }).then(res => {
      return res.json()
    }).then(resData => {
      console.log("🚀 ~ file: CardMain.js:94 ~ CardToday ~ resData:", resData)
      if (typeof resData != 'undefined') {
        this.setState({
          testData: resData,
          isLoading: false
        });
      }
    }).catch((error) => {
      console.log("🚀 ~ file: CardMain.js:99 ~ CardToday ~ error:", error)
    });
  }

  requestDataYesterday = () => {
    this.setState({ isLoading: true })
    const url = Url.url + Url.factory + Url.Fid + Url.StaticType + Url.Yesterday;
    console.log("🚀 ~ file: CardMain.js:109 ~ CardToday ~ url:", url)
    fetch(url, {
      credentials: 'include',
    }).then(resYesterday => {
      return resYesterday.json()
    }).then(resDataYesterday => {
      if (typeof resDataYesterday != 'undefined') {
        this.setState({
          testData: resDataYesterday,
          isLoading: false
        });
      }
      console.log('Yester')
      console.log(resDataYesterday)
    }).catch((error) => {
      console.log(error);
    });
  }

  requestDataWeek = () => {
    this.setState({ isLoading: true })
    const url = Url.url + Url.factory + Url.Fid + Url.StaticType + Url.Week;
    console.log("🚀 ~ file: CardMain.js:125 ~ CardToday ~ url:", url)
    fetch(url, {
      credentials: 'include',
    }).then(resWeek => {
      return resWeek.json();
    }).then(resDataWeek => {
      if (typeof resDataWeek != 'undefined') {
        this.setState({
          testData: resDataWeek,
          isLoading: false
        });
      }

    }).catch((error) => {
      console.log(error);
    });
  }

  requestDataMonth = (urlProps) => {
    this.setState({ isLoading: true })
    let url = Url.url + Url.factory + Url.Fid + Url.Month;
    if (typeof urlProps != 'undefined') {
      url = urlProps
    }
    fetch(url, {
      credentials: 'include',
    }).then(resMonth => {
      return resMonth.json()
    }).then(resDataMonth => {
      console.log("🚀 ~ file: CardMain.js:159 ~ CardToday ~ resDataMonth:", resDataMonth)
      if (typeof resDataMonth != 'undefined') {
        this.setState({
          testData: resDataMonth,
          isLoading: false
        });
      }
    }).catch((error) => {
      console.log(error);
    });
  }

  //卡片更新
  cardReflash(propsData, buttonID) {
    console.log("🚀 ~ file: CardMain.js:95 ~ CardToday ~ cardReflash ~ propsData:", propsData)
    console.log("🚀 ~ file: CardMain.js:95 ~ CardToday ~ cardReflash ~ propsData:", propsData.proNumber)
    if (typeof propsData.proNumber != "undefined") {
      //更新卡片中的数据
      this.setState({
        //newTestData来自按钮选择参数传值
        testData: propsData,
        buttonID: buttonID
      })
    } else {
      this.setState({
        testData: {
          proNumber: 0,
          proVolume: 0.00,
          storageNumber: 0,
          storageVolume: 0.00,
          outNumber: 0,
          outVolume: 0.00
        },
      })
    }
  }

  //卡片子函数
  renderCard() {

    const rotateY = this.state.animatedValue.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: ['0deg', '90deg', '0deg']
    });
    const { testData, buttonID, isLoading } = this.state;
    //console.log(this.props.Today)
    return (
      <View style={{ flexDirection: 'row', justifyContent: 'space-around', flex: 1, width: width * 0.96, marginLeft: width * 0.02 }}>
        <Toast ref={toast => { this.toast = toast }} position='center' />
        <TouchableOpacity onPress={() => {
          this.props.navigation.navigate('DayStatistic', {
            title: '统计分析',
            badge: '生产',
            buttonID: buttonID
          })
        }
        }>
          <View style={[styles.card, { backgroundColor: '#8579E8', }]}>
            {
              isLoading ?
                <View style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: 'transparent',
                }}><ActivityIndicator
                    animating={true}
                    color='white'
                    size="large" />
                </View> :
                <View>
                  <View style={styles.titleView}>
                    <View style={styles.badge}></View>
                    <Text style={styles.title} >生产</Text>
                  </View>
                  <Text style={styles.text1} numberOfLines={1}>{typeof testData.proVolume != "undefined" ? (testData.proVolume.toFixed(2) + 'm³') : 0 + 'm³'}</Text>
                  <Text style={styles.text2}>{(testData.proNumber + '件') || 0 + '件'}</Text>
                </View>
            }

          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            this.props.navigation.navigate('DayStatistic', {
              title: '统计分析',
              badge: '入库',
              buttonID: buttonID
            })
          }} >
          <View style={[styles.card, { backgroundColor: '#FF9D18' }]}>
            {
              isLoading ?
                <View style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: 'transparent',
                }}><ActivityIndicator
                    animating={true}
                    color='white'
                    size="large" />
                </View> :
                <View>
                  <View style={styles.titleView}>
                    <View style={styles.badge}></View>
                    <Text style={styles.title} >入库</Text>
                  </View>
                  <Text style={[styles.text1]} numberOfLines={1}>{typeof testData.storageVolume != "undefined" ? (testData.storageVolume.toFixed(2) + 'm³') : 0 + 'm³'}</Text>
                  <Text style={styles.text2} >{(testData.storageNumber + '件') || 0 + '件'}</Text>
                </View>
            }

          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => {
          this.props.navigation.navigate('DayStatistic', {
            title: '统计分析',
            badge: '出库',
            buttonID: buttonID,
          })
        }}>
          <Animated.View style={[styles.card, { backgroundColor: '#FF7A38', transform: [{ rotateY: rotateY },] }]}>
            {
              isLoading ?
                <View style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: 'transparent',
                }}><ActivityIndicator
                    animating={true}
                    color='white'
                    size="large" />
                </View> :
                <View>
                  <View style={styles.titleView}>
                    <View style={styles.badge}></View>
                    <Text style={styles.title} >发货</Text>
                  </View>
                  <Text style={styles.text1} numberOfLines={1} >{typeof testData.outVolume != "undefined" ? (testData.outVolume.toFixed(2) + 'm³') : 0 + 'm³'}</Text>
                  <Text style={styles.text2}>{(testData.outNumber + '件') || 0 + '件'}</Text>
                </View>
            }
          </Animated.View>
        </TouchableOpacity>
      </View>
    )
  }


  render() {
    return (
      <View>
        {this.renderCard()}
      </View>
    );
  }
}

export default withNavigation(CardToday)

const styles = StyleSheet.create({
  card: {
    marginTop: 10,
    borderRadius: 5,
    width: (width - 32) / 3,
    height: (width - 120) / 3,
    marginLeft: 0,
  },
  titleView: {
    flexDirection: 'row',
    top: RFT * 2.4,
    left: RFT * 3
  },
  badge: {
    borderRadius: 40,
    backgroundColor: 'white',
    height: RFT * 1.3,
    width: RFT * 1.3,
    marginRight: 8,
    top: 8
  },
  title: {
    //textAlign: '',
    //marginLeft: (RFT * 3) + 12,
    color: 'white',
    fontSize: RFT * 4.1,
    fontWeight: '100',

    //marginRight: 16
  },
  text1: {
    //textAlign: 'center',
    marginLeft: (RFT * 3) + 15,
    color: 'white',
    fontSize: RFT * 4.5,
    fontWeight: '700',
    marginTop: 13,
  },
  text2: {
    //textAlign: 'center',
    marginLeft: (RFT * 3) + 14,
    fontSize: RFT * 4,
    color: 'white',
    marginTop: 0,

  }
})

