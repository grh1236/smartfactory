import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  processColor,

} from 'react-native';

import { LineChart } from 'react-native-charts-wrapper';
import Url from '../../Url/Url';
import { RFT } from '../../Url/Pixal';




class LineChartScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      yearMonthList: [],
      thisConpriceList: [
        {
          y: 2000,
          x: 0,
          marker: '5800'
        },
        {
          y: 4000,
          x: 1,
          label: '1800'
        },
        {
          y: 9800,
          x: 2,
          marker: '4500'
        },
        {
          y: 6300,
          x: 3,
          marker: "800"
        },
        {
          y: 4300,
          x: 4,
          marker: "6800"
        },
        {
          y: 3600,
          x: 5,
          marker: "5600"
        }
      ],
      lastConpriceList: [
        {
          y: 6000,
          x: 0,
          marker: "35"
        },
        {
          y: 2000,
          x: 1,
          marker: "47 kg"
        },
        {
          y: 6000,
          x: 2,
          marker: "46"
        },
        {
          y: 600,
          x: 3,
          marker: "44 kg"
        },
        {
          y: 1800,
          x: 4,
          marker: "46"
        },
        {
          y: 5600,
          x: 5,
          marker: "Today: 35"
        }
      ]
    }
    this.requestData = this.requestData.bind(this);
  }

  requestData = () => {
    const Burl = Url.url + Url.factory + Url.Fid;
    const url = Burl + 'GetFacBusinessAnalysis';
    fetch(url, {
      credentials: 'include',
    }).then(res => {
      return res.json()
    }).then(resData => {
      resData.yearMonthList =  JSON.parse(JSON.stringify(resData.yearMonthList).replace(/年/g, "-").replace(/月/g, ""));
      this.setState({
        resData: resData,
        yearMonthList: resData.yearMonthList,
        thisConpriceList: resData.thisConpriceList,
        lastConpriceList: resData.lastConpriceList,
      });
     
    }).catch((error) => {
      console.log(error);
    });
  }

  handleSelect(event) {
    this.requestData();
    let entry = event.nativeEvent
    if (entry == null) {
      this.setState({ ...this.state, selectedEntry: null })
    } else {
      this.setState({ ...this.state, selectedEntry: JSON.stringify(entry) })
    }

    console.log(event.nativeEvent)
  }

 componentDidMount() {
    this.requestData();
    this.refs.chart.moveViewToAnimated(10, 0, 'left', 2000)
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={styles.container}>
          <LineChart
            style={styles.chart}
            data={{
              dataSets: [
                {
                  values: this.state.thisConpriceList,
                  label: '合同额',
                  config: {
                    /* mode: "CUBIC_BEZIER", */
                    drawValues: false,
                    lineWidth: 2,
                    drawCircles: true,
                    circleColor: processColor('#509FF0'),
                    drawCircleHole: false,
                    circleRadius: 5,
                    highlightColor: processColor("transparent"),
                    color: processColor('#509FF0'),
                    drawFilled: false,
                    fillAlpha: 1000,
                    valueTextSize: 15
                  }
                },
                {
                  values: this.state.lastConpriceList,
                  label: "去年合同",
                  config: {
                    drawValues: false,
                    lineWidth: 2,
                    drawCircles: true,
                    circleColor: processColor('#FF7A38'),
                    drawCircleHole: false,
                    circleRadius: 5,
                    highlightColor: processColor("transparent"),
                    color: processColor('#FF7A38'),
                    drawFilled: false,
                    fillAlpha: 1000,
                    //valueTextSize: 15
                  }
                }
              ]
            }}
            chartDescription={{ text: '' }}
            legend={{
              enabled: true,
              //textSize: 12,
              form: "SQUARE",
              formSize: 14,
              xEntrySpace: 10,
              //yEntrySpace: 5,
              wordWrapEnabled: true,
              //position: 'center'
              horizontalAlignment: 'RIGHT',
              verticalAlignment: 'TOP'
            }}
            marker={{
              enabled: true,
              markerColor: 'white',
              digits: 2,
              textColor: 'black'
            }}
            xAxis={{
              enabled: true,
              granularity: 1,
              drawLabels: true,
              position: "BOTTOM",
              drawAxisLine: false,
              drawGridLines: false,
              axisMinimum: -0.5,
              fontFamily: "HelveticaNeue-Medium",
              axisMaximum: this.state.yearMonthList.length,
              //fontWeight: "bold",
              textSize: RFT * 3,
              textColor: processColor("black"),
              valueFormatter: this.state.yearMonthList,
              //labelRotationAngle:  -45,
            }}
            yAxis={{
              left: {
                enabled: true,
                drawAxisLine: false,
               // textSize: 13,
                textColor: processColor("black"),
                gridColor: processColor("black"),
              },
              right: {
                enabled: false
              }
            }}
            //autoScaleMinMaxEnabled={true}
            animation={{
              durationX: 0,
              durationY: 1500,
              easingY: 'EaseInOutQuart'
            }}
            touchEnabled={true}
            dragEnabled={true}
            scaleEnabled={false}
            scaleXEnabled={false}
            scaleYEnabled={false}
            pinchZoom={false}
            doubleTapToZoomEnabled={false}
            dragDecelerationEnabled={true}
            dragDecelerationFrictionCoef={1}
            keepPositionOnRotation={false}
            onSelect={this.handleSelect.bind(this)}
            onChange={(event) => console.log(event.nativeEvent)}
            animation={{
              durationX: 1000,
              durationY: 1500,
              easingY: 'EaseInOutQuart'
            }}
            visibleRange={{
              x: { min: 1, max: 4 }
            }}
            ref = "chart"
          ></LineChart>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,


  },
  chart: {
   flex:1
  }
});

export default LineChartScreen;