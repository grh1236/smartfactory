import React from 'react';
import { ActivityIndicator, View, FlatList, StylesSheet, processColor, Dimensions } from 'react-native';
import { Text, Card, Image, Button } from 'react-native-elements'
import { PieChart, HorizontalBarChart } from 'react-native-charts-wrapper';
import { ScrollView } from 'react-native-gesture-handler';
import Url from '../../Url/Url';

const { height, width } = Dimensions.get('window');
const Url1 = '';
let i = 0, j = 0, index = 0;
let No = ' ';
var NoNew = ' ';
let newdata = [];
let newdatatest = [];
var firstFloor = [];
let releaseNo = 0, inProducedNo = 0, notProducedNo = 0, installNo = 0, notInstallNo = 0, allNo = 0;
let releaseNo1 = 0, inProducedNo1 = 0, notProducedNo1 = 0, installNo1 = 0, notInstallNo1 = 0, allNo1 = 0;
let releaseAll = 0, inProducedAll = 0, notProducedAll = 0, installAll = 0, notInstallAll = 0;

//var testData = 
var newdataNo = [];



class FlatListChartcoloum extends React.Component {
  constructor() {
    super()
    this.state = {
      isLoading: true,
      testData: [{
        "id": 1,
        "factoryId": "74e1cbc6-2cfc-4d6f-bcd1-739d28e4c74d",
        "projectId": "AE37158119BD4502BA92485B2757248E",
        "projectName": "中建壹品澜郡项目",
        "floorNoName": "1#",
        "floorNo": 1,
        "inProduced": 0,
        "notProduced": 12,
        "notInstall": 0,
        "install": 0,
        "release": 0
      }],
      newdataNo: [],
      description: {
        text: ' ',
        textSize: 15,
        textColor: processColor('darkgray'),
      },
      resData: [{
        "id": 1,
        "factoryId": "74e1cbc6-2cfc-4d6f-bcd1-739d28e4c74d",
        "projectId": "AE37158119BD4502BA92485B2757248E",
        "projectName": "中建壹品澜郡项目",
        "floorNoName": "1#",
        "floorNo": 1,
        "inProduced": 0,
        "notProduced": 12,
        "notInstall": 0,
        "install": 0,
        "release": 0
      },
      {
        "id": 2,
        "factoryId": "74e1cbc6-2cfc-4d6f-bcd1-739d28e4c74d",
        "projectId": "AE37158119BD4502BA92485B2757248E",
        "projectName": "中建壹品澜郡项目",
        "floorNoName": "1#",
        "floorNo": 2,
        "inProduced": 0,
        "notProduced": 104,
        "notInstall": 1,
        "install": 0,
        "release": 0
      },],
      data: [],

      yAxis: {
        left: {
          enabled: false
        },
        right: {
          enabled: false
        }
      },
      description: {
        text: ' ',
        textSize: 17,
        textColor: processColor('darkgray')
      },
      legend: {
        enabled: false,
        textSize: 10,
        form: "SQUARE",
        formSize: 10,
        xEntrySpace: 20,
        yEntrySpace: 15,
        wordWrapEnabled: true,
      },
      marker: {
        enabled: true,
        markerColor: processColor('#F0C0FFdF'),
        textColor: processColor('white'),
        markerFontSize: 12,
      },
      highlights: [{ x: 1, stackIndex: 2 }, { x: 2, stackIndex: 1 }],
    }
    this.requestData = this.requestData.bind(this);
    this.CountNo = this.CountNo.bind(this)
    this.ReflashData = this.ReflashData.bind(this)
  }



  ReflashData(newTestData) {
    //console.log(newTestData)
    //更新卡片中的数据
    newdata = [];
    releaseAll = 0, inProducedAll = 0, notProducedAll = 0, installAll = 0, notInstallAll = 0;
    releaseNo = 0, inProducedNo = 0, notProducedNo = 0, installNo = 0, notInstallNo = 0, No = '', allNo = 0;
    for (let index = 0; index < newTestData.length; index++) {
      const itemall = newTestData[index];
      releaseAll += itemall.release;
      inProducedAll += itemall.inProduced;
      notProducedAll += itemall.notProduced;
      installAll += itemall.install;
      notInstallAll += itemall.notInstall;
    }
    //releaseNo = 0, inProducedNo = 0, notProducedNo = 0, installNo = 0, notInstallNo = 0, No = '';
    this.setState({
      //newTestData来自按钮选择参数传值
      testData: newTestData,
      isLoading: false,
    }, () => {

    })
  }

  handleSelect(event) {
    let entry = event.nativeEvent
    console.log(entry)
    if (entry == null) {
      this.setState({ ...this.state, selectedEntry: null })
    } else {
      this.setState({ ...this.state, selectedEntry: JSON.stringify(entry) })
    }

    ////console.log(event.nativeEvent)
  }


  componentDidMount() {
    this.requestData();
    this.CountNo();
  }

  requestData = () => {
    const url = this.props.url ? this.props.url : Url1;
    //console.log(this.props.url)
    fetch(url, {
      credentials: 'include',
    }).then(res => {
      return res.json()
    }).then(resData => {
      this.setState({
        resData: resData,
      });
      ////console.log(resData)
    }).catch((error) => {
      //console.log(error);
    });
  }

  CountNo = () => {

    //console.log(this.props.Data)
    for (index = 0; index < this.props.Data.length; index++) {
      const element = this.props.Data[index];
      if (No != element.floorNoName) {
        No = element.floorNoName;
        newdataNo[j++] = newdata
        firstFloor = element;
        i = 0;
      } else {
        newdata[0] = firstFloor;
        newdata[++i] = element;
        NoNew = No;
      }
    }

    //console.log(newdataNo)
  }


  render() {
    //console.log(releaseAll, inProducedAll, notProducedAll, installAll, notInstallAll)
    const { testData } = this.state;
    const length = this.state.testData.length;
    //console.log(testData);
    if (this.props.stateloading === true) {
      return (
        <View style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#F5FCFF',
        }}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
    } else {
      return (
        <View style={{ flex: 1 }}>
          <View style={{ flex: 1 }}>
            <HorizontalBarChart
              scaleEnabled={false}
              chartDescription={this.state.description}
              style={{ flex: 1 }}
              data={{
                dataSets: [{
                  values: [{ y: [notProducedAll, inProducedAll, releaseAll, notInstallAll, installAll,] }],
                  label: '',
                  config: {
                    colors: [processColor('#FFCF18'), processColor('#FF9D18'), processColor('#8579E8'),
                    processColor('#41CCFF'),
                    processColor('#419FFF'),
                    ],
                    drawValues: false,
                    highlightAlpha: 15,
                    valueTextSize: 8,
                    stackLabels: ['未生产', '在生产', '脱模待检', '待安装', '已安装',],
                    horizontalAlignment: "CENTER",
                  }
                }],
              }}
              highlightFullBarEnabled = {true}
              //drawValueAboveBar = {true}
              xAxis={{
                enabled: false,
              }}
              //onSelect={this.handleSelect.bind(this)}
             // onChange={(event) => console.log(event.nativeEvent)}
              yAxis={{
                left: {
                  enabled: false
                },
                right: {
                  enabled: true,
                }
              }}
              description={this.state.description}
              legend={{
                enabled: true,
                textSize: 10,
                form: "SQUARE",
                formSize: 10,
                xEntrySpace: 20,
                //yEntrySpace: 15,
                wordWrapEnabled: true,
                horizontalAlignment: 'CENTER',
                verticalAlignment: 'TOP',
                
              }}
              marker={this.state.marker}
            />
          </View>
          <View style={{ flex: 0.5 }}></View>
          <View style={{ flex: 4 }}>
            <ScrollView horizontal={true} scrollEnabled={true} showsHorizontalScrollIndicator={false}>
              {

                testData.map((item, index) => {
                  //console.log(testData);
                  ////console.log(item)
                  if (No !== item.floorNoName) {
                    No = item.floorNoName;
                    //NoNew = No;
                    //console.log('No:')
                    //console.log(No)
                    firstFloor = item;
                    i = 0;

                   allNo = 0;

                    newdata.map((itemadd, indexadd) => {
                      releaseNo += itemadd.release;
                      inProducedNo += itemadd.inProduced;
                      notProducedNo += itemadd.notProduced;
                      installNo += itemadd.install;
                      notInstallNo += itemadd.notInstall;
                      ////console.log(releaseNo, inProducedNo, notProducedNo, installNo, notInstallNo, itemadd.floorNoName)
                      return (releaseNo, inProducedNo, notProducedNo, installNo, notInstallNo)
                    })

                    releaseNo1 = releaseNo, inProducedNo1 = inProducedNo, notProducedNo1 = notProducedNo, installNo1 = installNo, notInstallNo1 = notInstallNo, allNo1 = allNo;
                    allNo = releaseNo + inProducedNo + notProducedNo + installNo + notInstallNo;
                    allNo1 = allNo;
                    

                    releaseNo = 0, inProducedNo = 0, notProducedNo = 0, installNo = 0, notInstallNo = 0, allNo = 0;
                    allNo = 0;

                   /*  console.log('floorNoName: ' + item.floorNoName);
                    console.log('allNo1:' + allNo1); */

                    if (releaseNo1 == 0 && inProducedNo1 == 0 && notProducedNo1 == 0 && installNo1 == 0 && notInstallNo1 == 0) {
                      return (
                        <View></View>
                      )
                    } else {
                      return (
                        <View style={{ flex: 1, width: width }} >
                          <PieChart
                            noDataText='NO DATA'
                            style={{ flex: 9 }}
                            chartDescription={this.state.description}
                            marker={this.state.marker}
                            logEnabled={true}
                            rotationEnabled={true}
                            drawEntryLabels={false}
                            rotationAngle={45}
                            usePercentValues={false}
                            holeRadius={65}
                            transparentCircleRadius={45}
                            styledCenterText={{ text: '总数:\n' + allNo1 + '件', color: processColor('black'), size: 12 }}
                            centerTextRadiusPercent={80}
                            holeColor={processColor('white')}
                            transparentCircleColor={processColor('white')}
                            data={{
                              dataSets: [{
                                values: [
                                  { value: notProducedNo1, label: '未生产' },
                                  { value: inProducedNo1, label: '已生产' },
                                  { value: releaseNo1, label: '脱模待检' },
                                  { value: notInstallNo1, label: '未安装' },
                                  { value: installNo1, label: '已安装' },

                                ],
                                label: ' ',
                                config: {
                                  drawValues: true,
                                  colors: [processColor('#FFCF18'), processColor('#FF9D18'),
                                  processColor('#8579E8'),
                                  processColor('#41CCFF'),
                                  processColor('#419FFF'),
                                  ],
                                  valueTextSize: 14,
                                  valueTextColor: processColor('#999999'),
                                  sliceSpace: 1,
                                  selectionShift: 10,
                                  xValuePosition: "OUTSIDE_SLICE",
                                  yValuePosition: "OUTSIDE_SLICE",
                                  //valueFormatter: "#.#'%'",
                                  valueLineColor: processColor('#999999'),
                                  valueLinePart1Length: 0.35
                                }
                              }],
                            }}
                            legend={this.state.legend}
                          />
                          <Text style={{ flex: 1, textAlign: 'center', textAlignVertical: 'center' }}>{NoNew}</Text>
                        </View>
                      )
                    }
                  } else if (length == (index + 1)) {
                    //console.log(length == (index + 1))
                    newdata[++i] = item;
                    i = 0;
                    releaseNo = 0, inProducedNo = 0, notProducedNo = 0, installNo = 0, notInstallNo = 0, allNo = 0;
                    allNo = 0;
                    newdata.map((itemadd, indexadd) => {
                      releaseNo += itemadd.release;
                      inProducedNo += itemadd.inProduced;
                      notProducedNo += itemadd.notProduced;
                      installNo += itemadd.install;
                      notInstallNo += itemadd.notInstall;
                      ////console.log(releaseNo, inProducedNo, notProducedNo, installNo, notInstallNo, itemadd.floorNoName)
                      return (releaseNo, inProducedNo, notProducedNo, installNo, notInstallNo)
                    })
                    releaseNo1 = releaseNo, inProducedNo1 = inProducedNo, notProducedNo1 = notProducedNo, installNo1 = installNo, notInstallNo1 = notInstallNo, allNo1 = allNo;
                    allNo = releaseNo + inProducedNo + notProducedNo + installNo + notInstallNo;
                    allNo1 = allNo;
                    releaseNo = 0, inProducedNo = 0, notProducedNo = 0, installNo = 0, notInstallNo = 0, allNo = 0;
                    allNo = 0;
                    return (
                      <View style={{ flex: 1, width: width }} >
                        <PieChart
                          noDataText='NO DATA'
                          style={{ flex: 9 }}
                          chartDescription={this.state.description}
                          marker={this.state.marker}
                          logEnabled={true}
                          rotationEnabled={true}
                          drawEntryLabels={false}
                          rotationAngle={45}
                          usePercentValues={false}
                          holeRadius={65}
                          transparentCircleRadius={45}
                          styledCenterText={{ text: '总数:\n' + allNo1 + '件', color: processColor('black'), size: 12 }}
                          centerTextRadiusPercent={80}
                          holeColor={processColor('white')}
                          transparentCircleColor={processColor('white')}
                          data={{
                            dataSets: [{
                              values: [
                                { value: notProducedNo1, label: '未生产' },
                                { value: inProducedNo1, label: '已生产' },
                                { value: releaseNo1, label: '脱模待检' },
                                { value: notInstallNo1, label: '未安装' },
                                { value: installNo1, label: '已安装' },
                              ],
                              label: ' ',
                              config: {
                                drawValues: true,
                                colors: [processColor('#FFCF18'), processColor('#FF9D18'),
                                processColor('#8579E8'),
                                processColor('#41CCFF'),
                                processColor('#419FFF'),
                                ],
                                valueTextSize: 14,
                                valueTextColor: processColor('#999999'),
                                sliceSpace: 1,
                                selectionShift: 10,
                                xValuePosition: "OUTSIDE_SLICE",
                                yValuePosition: "OUTSIDE_SLICE",
                                //valueFormatter: "#.#'%'",
                                valueLineColor: processColor('#999999'),
                                valueLinePart1Length: 0.35
                              }
                            }],
                          }}
                          legend={this.state.legend}
                        />
                        <Text style={{ flex: 1, textAlign: 'center', textAlignVertical: 'center' }}>{NoNew}</Text>
                      </View>
                    )
                  } else {
                    //newdata[0] = firstFloor;
                    newdata[++i] = item;
                    //releaseNo = 0, inProducedNo = 0, notProducedNo = 0, installNo = 0, notInstallNo = 0;
                    NoNew = No;
                  }
                })
              }
            </ScrollView >
          </View>
        </View>

      )
    }

  }

  _renderItem = ({ item, index }) => {
    ////console.log(item)
    let releaseNo = 0, inProducedNo = 0, notProducedNo = 0, installNo = 0, notInstallNo = 0;
    item.map((itemadd, indexadd) => {
      ////console.log(itemadd)
      releaseNo += itemadd.release;
      inProducedNo += itemadd.inProduced;
      notProducedNo += itemadd.notProduced;
      installNo += itemadd.install;
      notInstallNo += itemadd.notInstall;
    })
    ////console.log(releaseNo, inProducedNo, notProducedNo, installNo, notInstallNo)
    return (
      <View style={{ flex: 1 }}>
        {/* <PieChart
          style={{ flex: 1 }}
          chartDescription={this.state.description}
          marker={this.state.marker}
          logEnabled={true}
          rotationEnabled={true}
          rotationAngle={45}
          usePercentValues={true}
          holeRadius={45}
          transparentCircleRadius={45}
          data={{
            dataSets: [{
              values: [
                { value: releaseNo, label: '脱模待检' },
                { value: installNo, label: '已安装' },
                { value: notInstallNo, label: '未安装' },
                { value: inProducedNo, label: '已生产' },
                { value: notProducedNo, label: '未生产' }
              ],
              label: ' ',
              config: {
                colors: [processColor('#8579E8'), processColor('#419FFF'), processColor('#41CCFF'), processColor('#FF9D18'), processColor('#FFFf37')],
                valueTextSize: 10,
                valueTextColor: processColor('green'),
                sliceSpace: 5,
                selectionShift: 13,
                xValuePosition: "OUTSIDE_SLICE",
                yValuePosition: "OUTSIDE_SLICE",
                valueFormatter: "#.#'%'",
                valueLineColor: processColor('green'),
                valueLinePart1Length: 0.5
              }
            }],
          }}
          legend={this.state.legend}
        />  */}
        <Text style={{ flex: 1, textAlign: 'center', textAlignVertical: 'center' }}>{item[0].floorNoName}</Text>
      </View>
    )
  }
}
export default FlatListChartcoloum




























{/* <View style={{ flexDirection: 'row', flex: 5 }}>
                        <View style={{ flexDirection: 'column-reverse', justifyContent: 'space-evenly', alignContent: 'stretch' }}>
                          {
                            newdata.map((itemfloor, i) => {
                              ////console.log(itemfloor.floorNo)
                              return (
                                <View style={{ flex: 1 }} >
                                   <Text style={{ height: 40, textAlign: 'center', textAlignVertical: 'center' }}>{itemfloor.floorNo}层</Text>
                                 </View>
                              )
                            })
                          }
                        </View>
                        <View style={{ flexDirection: 'column-reverse', justifyContent: 'space-evenly', alignContent: 'stretch' }}>
                          {
                            newdata.map((itemnew, index) => {
                              return (
                                <View style={{ flex: 1 }}>
                                 <HorizontalBarChart
                                    chartDescription={this.state.description}
                                    key={index}
                                    style={{ height: 40, width: 200, }}
                                    data={{
                                      dataSets: [{
                                        values: [{ y: [itemnew.release, itemnew.install, itemnew.notInstall, itemnew.inProduced, itemnew.notProduced] }],
                                        label: '',
                                        config: {
                                          colors: [processColor('#8579E8'), processColor('#419FFF'), processColor('#41CCFF'), processColor('#FF9D18'), processColor('#FFFf37')],
                                          stackLabels: ['脱模待检', '已安装', '待安装', '在生产', '未生产'],
                                          horizontalAlignment: "CENTER",
                                        }
                                      }],
                                    }}
                                    xAxis={{
                                      enabled: false,
                                    }}
                                    yAxis={this.state.yAxis}
                                    description={this.state.description}
                                    legend={this.state.legend}
                                    scaleEnabled={false}

                                    description={
                                      {
                                        text: ' ',
                                        textSize: 15,
                                        textColor: processColor('darkgray'),
                                      }
                                    }
                                  />
                                </View>
                              )
                            })
                          }
                        </View>
                      </View> */}
























/* < ScrollView horizontal={true} scrollEnabled={true} >
            {
              this.props.Data.map((item, index) => {
                ////console.log(item)
                if (No != item.floorNoName) {
                  //console.log('newdata:')
                  //console.log(newdata)
                  No = item.floorNoName;
                  firstFloor = item;
                  i = 0;
                  newdata.map((itemadd, indexadd) => {
                    releaseNo += itemadd.release;
                    inProducedNo += itemadd.inProduced;
                    notProducedNo += itemadd.notProduced;
                    installNo += itemadd.install;
                    notInstallNo += itemadd.notInstall;
                    ////console.log(releaseNo, inProducedNo, notProducedNo, installNo, notInstallNo, itemadd.floorNoName)
                    return (releaseNo, inProducedNo, notProducedNo, installNo, notInstallNo)
                  })
                  var tmp = [];
                  let length = newdata.length;
                  //console.log('length:' + length)
                  for (let ii = 0; ii < length - 1; ii++) {
                    //console.log(newdata);
                    for (let j = 0; j < length - ii - 1; j++) {
                      if (newdata[j].floorNo > newdata[j + 1].floorNo) {
                        tmp = newdata[j];
                        newdata[j] = newdata[j + 1];
                        newdata[j + 1] = tmp;
                        //console.log(newdata[j])
                        //console.log(tmp)
                      }
                    }
                  }
                  //console.log(newdata);
                  return (
                    <View style={{ flex: 1 }} >
                      <View style={{ flex: 1 }}>
                        <PieChart
                          style={{ flex: 3.5 }}
                          chartDescription={this.state.description}
                          marker={this.state.marker}
                          logEnabled={true}
                          rotationEnabled={true}
                          rotationAngle={45}
                          usePercentValues={true}
                          holeRadius={45}
                          transparentCircleRadius={45}
                          data={{
                            dataSets: [{
                              values: [
                                { value: releaseNo, label: '脱模待检' },
                                { value: installNo, label: '已安装' },
                                { value: notInstallNo, label: '未安装' },
                                { value: inProducedNo, label: '已生产' },
                                { value: notProducedNo, label: '未生产' }
                              ],
                              label: ' ',
                              config: {
                                colors: [processColor('#8579E8'), processColor('#419FFF'), processColor('#41CCFF'), processColor('#FF9D18'), processColor('#FFFf37')],
                                valueTextSize: 10,
                                valueTextColor: processColor('green'),
                                sliceSpace: 5,
                                selectionShift: 13,
                                xValuePosition: "OUTSIDE_SLICE",
                                yValuePosition: "OUTSIDE_SLICE",
                                valueFormatter: "#.#'%'",
                                valueLineColor: processColor('green'),
                                valueLinePart1Length: 0.5
                              }
                            }],
                          }}
                          legend={this.state.legend}
                        />
                        <Text style={{ flex: 1, textAlign: 'center', textAlignVertical: 'center' }}>{NoNew}</Text>
                      </View>
                      <View style={{ flexDirection: 'row', flex: 0 }}>
                        <View style={{ flexDirection: 'column-reverse', justifyContent: 'space-evenly', alignContent: 'stretch' }}>
                          {
                            newdata.map((itemfloor, i) => {
                              ////console.log(itemfloor.floorNo)
                              return (
                                <View style={{ flex: 1 }} >
                                   <Text style={{ height: 40, textAlign: 'center', textAlignVertical: 'center' }}>{itemfloor.floorNo}层</Text>
                                 </View>
                              )
                            })
                          }
                        </View>
                        <View style={{ flexDirection: 'column-reverse', justifyContent: 'space-evenly', alignContent: 'stretch' }}>
                          {
                            newdata.map((itemnew, index) => {
                              return (
                                <View style={{ flex: 1 }}>
                                 <HorizontalBarChart
                                    chartDescription={this.state.description}
                                    key={index}
                                    style={{ height: 40, width: 200, }}
                                    data={{
                                      dataSets: [{
                                        values: [{ y: [itemnew.release, itemnew.install, itemnew.notInstall, itemnew.inProduced, itemnew.notProduced] }],
                                        label: '',
                                        config: {
                                          colors: [processColor('#8579E8'), processColor('#419FFF'), processColor('#41CCFF'), processColor('#FF9D18'), processColor('#FFFf37')],
                                          stackLabels: ['脱模待检', '已安装', '待安装', '在生产', '未生产'],
                                          horizontalAlignment: "CENTER",
                                        }
                                      }],
                                    }}
                                    xAxis={{
                                      enabled: false,
                                    }}
                                    yAxis={this.state.yAxis}
                                    description={this.state.description}
                                    legend={this.state.legend}
                                    scaleEnabled={false}

                                    description={
                                      {
                                        text: ' ',
                                        textSize: 15,
                                        textColor: processColor('darkgray'),
                                      }
                                    }
                                  />
                                </View>
                              )
                            })
                          }
                        </View>
                      </View>
                    </View>
                  )

                } else {
                  newdata[0] = firstFloor;
                  newdata[++i] = item;
                  releaseNo = 0, inProducedNo = 0, notProducedNo = 0, installNo = 0, notInstallNo = 0;
                  NoNew = No;

                }
              })
            }
          </ScrollView > */





























