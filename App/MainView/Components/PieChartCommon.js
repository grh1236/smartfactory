import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  processColor,
} from 'react-native';

import { StackNavigator, SafeAreaView } from 'react-navigation';

import { PieChart } from 'react-native-charts-wrapper';

class PieChartScreen extends React.Component {

  constructor() {
    super();

    this.state = {
      legend: {
        enabled: true,
        textSize: 18,
        form: 'CIRCLE',
        wordWrapEnabled: true,
        xEntrySpace: 13,
        yEntrySpace: 6,
      },
      data: {
        dataSets: [{
          values: [
            { value: 1000, label: '同心花苑一期项目' },
            { value: 30, label: '宜兴华腾园' },
            { value: 10, label: '深港新城异型构件' },
            { value: 5, label: '海赛洛城C地段' },
            { value: 5, label: '怡海花园一期' }
          ],
          label: ' ',
          config: {
            colors: [processColor('#419FFF'), processColor('#41CCFF'), processColor('#FF9D18'), processColor('#FFFf37'), processColor('#8579E8')],
            valueTextSize: 0,
            valueTextColor: processColor('white'),
            sliceSpace: 5,
            selectionShift: 13,
            xValuePosition: "OUTSIDE_SLICE",
            yValuePosition: "OUTSIDE_SLICE",
            valueFormatter: "#.#'%'",
            valueLineColor: processColor('green'),
            valueLinePart1Length: 0.5
          }
        }],
      },
     // highlights: [{ x: 2 }],
      description: {
        text: ' ',
        textSize: 15,
        textColor: processColor('darkgray'),

      }
    };
  }

  handleSelect(event) {
    let entry = event.nativeEvent
    if (entry == null) {
      this.setState({ ...this.state, selectedEntry: null })
    } else {
      this.setState({ ...this.state, selectedEntry: JSON.stringify(entry) })
    }

    console.log(event.nativeEvent)
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>

        <View style={styles.container}>
          <PieChart
            marker={this.props.marker ? this.props.marker : { enabled: false }}
            style={styles.chart}
            logEnabled={true}
            chartBackgroundColor={processColor('white')}
            chartDescription={this.state.description}
            data={this.props.data}
            legend={this.props.legend}
           // highlights={this.state.highlights}
            entryValue={true}
            entryLabelColor={processColor('green')}
            entryLabelTextSize={28}
            drawEntryLabels={false}
            rotationEnabled={true}
            rotationAngle={45}
            usePercentValues={true}
            styledCenterText={{ text: this.state.data.dataSets.values, color: processColor('black'), size: 15 }}
            centerTextRadiusPercent={120}
            holeRadius={65}
            holeColor={processColor('white')}
            transparentCircleRadius={45}
            transparentCircleColor={processColor('white')}
            maxAngle={450}
            onSelect={this.handleSelect.bind(this)}
            onChange={(event) => console.log(event.nativeEvent)}
          />
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  chart: {
    flex: 1
  }
});

export default PieChartScreen;

