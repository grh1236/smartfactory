import { StyleSheet } from "react-native";
import { deviceWidth, RFT } from "../../Url/Pixal";

export const MainStyles = StyleSheet.create({
  chartContainerView: {
    height: deviceWidth * 0.58,
    borderRadius: 20,
    width: deviceWidth / 0.9,
    backgroundColor: 'transparent'
  },
  chartView: {
    width: deviceWidth * 0.94,
    height: deviceWidth * 0.57,
    marginLeft: deviceWidth * 0.03,
    backgroundColor: '#FFFFFF',
    borderRadius: 10
  },
  chartTitle: {
    color: '#535c68',
    fontSize: RFT * 3.7,
    marginTop: deviceWidth * 0.02,
    marginBottom: 0,
    textAlign: 'center'
  },
  chart: {
    height: deviceWidth * 0.5,
    marginBottom: deviceWidth * 0.02,
    marginHorizontal:  deviceWidth * 0.03,
  }
});