import React from 'react';
import { View, ScrollView } from 'react-native';
import { Button } from 'react-native-elements';
import Url from '../../Url/Url'
import FAPI from '../../Url/FactoryAPI'

export default class ButtonTop extends React.Component {
	constructor() {
		super()
		this.state = {
			resDataButton: []
		}
	}

	componentWillMount() {
		this.requestDataButton();
	}

	requestDataButton = () => {
		const uri = Url.url + Url.Fid + 'GetFactoryProjects';
		console.log(uri);
		fetch(uri, {
      credentials: 'include',
    }).then(resButton => {
			return resButton.json()
		}).then(resDataButton => {
			this.setState({
				resDataButton: resDataButton,
			})
			//console.log(this.state.resDataButton)
		})
	}

	render() {
    //console.log(this.state.resDataButton)
		return (
			<View style = {{flex: 1, marginBottom: 12, marginTop: 12}}> 
				<ScrollView horizontal={true} scrollEnabled={true} showsHorizontalScrollIndicator={false}>
					{
						this.state.resDataButton.map((item) => {
							console.log(item);
							return (
								<Button
									//key={index}
									buttonStyle={{
                    borderRadius: 20
									}}
									containerStyle={{
										marginHorizontal: 6,
										borderRadius: 20
									}}
									title={item.projectName}
									type='outline'
									onPress
								/>
							)
						})
					}

				</ScrollView>
			</View>
		);
	}
}