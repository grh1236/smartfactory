import React from 'react';
import { StyleSheet, View, processColor, } from 'react-native';
import { Text } from 'react-native-elements';
import { HorizontalBarChart } from 'react-native-charts-wrapper';

class ProjectBarList extends React.Component {

  constructor() {
    super();

    this.state = {
      legend: {
        enabled: false,
        textSize: 10,
        form: "SQUARE",
        formSize: 10,
        xEntrySpace: 10,
        yEntrySpace: 5,
        wordWrapEnabled: true
      },
      data: {
        dataSets: [{
          values: [
            { y: [2000], marker: ["已安装", "待安装", "在生产", "未生产"] },
            { y: [1100, 600, 300, 200], marker: ["已安装", "待安装", "在生产", "未生产"] },
            { y: [1000, 700, 300, 200], marker: ["已安装", "待安装", "在生产", "未生产"] },
            { y: [900, 800, 300, 200], marker: ["已安装", "待安装", "在生产", "未生产"] },
            { y: [800, 900, 300, 200], marker: ["已安装", "待安装", "在生产", "未生产"] },
            { y: [700, 1000, 300, 200], marker: ["已安装", "待安装", "在生产", "未生产"] },
            { y: [600, 1100, 300, 200], marker: ["已安装", "待安装", "在生产", "未生产"] },
            { y: [500, 1200, 300, 200], marker: ["已安装", "待安装", "在生产", "未生产"] },
            { y: [400, 1300, 300, 200], marker: ["已安装", "待安装", "在生产", "未生产"] },
            { y: [300, 1400, 300, 200], marker: ["已安装", "待安装", "在生产", "未生产"] },
            { y: [200, 1500, 300, 200], marker: ["已安装", "待安装", "在生产", "未生产"] },
            { y: [100, 1600, 300, 200], marker: ["已安装", "待安装", "在生产", "未生产"] }
          ],
          label: '',
          config: {
            colors: [processColor('#419FFF'), processColor('#41CCFF'), processColor('#FF9D18'), processColor('#FFCF18')],
            stackLabels: ['已安装', '待安装', '在生产', '未生产']
          },}
        ]
        },
          xAxis: {
          valueFormatter: ['1#','2#','3#','4#','5#', '6#', '7#', '8#', '9#', '10#', '11#', '12#'],
          granularityEnabled: true,
          granularity: 1,
          drawAxisLine: false,
          position: 'BOTTOM',
          textColor: processColor("black"),
          gridColor: processColor("white"),

        },
          yAxis: {
          left: {
            enabled: false
          },
          right: {
            enabled: false
          },
        },
          description: {
          text: ' ',
          textSize: 15,
          textColor: processColor('darkgray'),

        }

        

      };
    }
  

  handleSelect(event) {
    let entry = event.nativeEvent
    if (entry == null) {
      this.setState({ ...this.state, selectedEntry: null })
    } else {
      this.setState({ ...this.state, selectedEntry: JSON.stringify(entry) })
    }

    console.log(event.nativeEvent)
  }

  render() {
    return (

      <View style={{ flex: 1 }}>
        <View style={styles.container}>
          <HorizontalBarChart
            style={styles.chart}
            xAxis={this.state.xAxis}
            yAxis={this.state.yAxis}
            data={this.state.data}
            legend={this.state.legend}
            drawValueAboveBar={false}
            marker={{
              enabled: true,
              markerColor: processColor('#F0C0FF8C'),
              textColor: processColor('white'),
              markerFontSize: 14,
            }}
            highlights={this.state.highlights}
            onSelect={this.handleSelect.bind(this)}
            onChange={(event) => console.log(event.nativeEvent)}
            chartDescription={this.state.description}
          />
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  chart: {
    flex: 1
  }
});


export default ProjectBarList;
