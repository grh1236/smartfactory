import React from 'react';
import {
  AppRegistry,
  StyleSheet,
  View, processColor,
  TouchableOpacity,
  ActivityIndicator
} from 'react-native';
import { Text, Overlay, Header, Icon } from 'react-native-elements'
import { BarChart } from 'react-native-charts-wrapper';
import BarChart1 from '../ChildComponent/BarChart'
import { withNavigation } from 'react-navigation';
import projectAnalysisMonth from '../ChildPages/projectAnalysisMonth';
import Url from '../../Url/Url';
import { RFT, deviceWidth } from '../../Url/Pixal'

//const Burl = Url.url + Url.factory + Url.Fid ;


class StackedBarChartScreen extends React.Component {

  constructor() {
    super();
    this.state = {
      yearMonthList: ['1月', '3月', '5月', '7月', '9月'],
      volumeList: [0, 0, 0, 0, 0, 0],
      aimAmountList: [0, 0, 0, 0, 0, 0],
      capacityList: [0, 0, 0, 0, 0, 0],
      planVolumeList: [0, 0, 0, 0, 0, 0],
      legend: {
        enabled: true,
        textSize: RFT * 3,
        form: "SQUARE",
        //formSize: RFT * 4,
        xEntrySpace: 10,
        //yEntrySpace: 5,
        wordWrapEnabled: true,
        horizontalAlignment: 'RIGHT',
        verticalAlignment: 'TOP',
      },
      data: {
        dataSets: [{
          values: [5, 40, 43, 61, 43],
          label: '理论产量',
          config: {
            drawValues: false,
            colors: [processColor('#32D185')],
          }
        }, {
          values: [40, 5, 50, 23, 59],
          label: '目标产量',
          config: {
            drawValues: false,
            colors: [processColor('#509FF0')],
          }
        }, {
          values: [10, 55, 35, 40, 62],
          label: '计划产量',
          config: {
            drawValues: false,
            colors: [processColor('#8579E8')],
          }
        }, {
          values: [6, 42, 28, 47, 58],
          label: '实际产量',
          config: {
            drawValues: false,
            colors: [processColor('#FF7A38')],
          }
        }],
        config: {
          barWidth: 0.165,
          group: {
            fromX: 0,
            groupSpace: 0.1,
            barSpace: 0.06,
          },
        }
      },
      xAxis: {
        valueFormatter: ['1月', '3月', '5月', '7月', '9月'],
        granularityEnabled: false,
        granularity: 1,
        axisMaximum: 5,
        axisMinimum: 0,
        centerAxisLabels: true,
        setDrawGirdLines: false,
        drawGridLines: false,
        gridLineWidth: 1,
        position: "BOTTOM",
        valueFormatterPattern: '月份'
      },
      scaleEnabled: true,
      autoScaleMinMaxEnabled: true,
      marker: {
        enabled: true,
        digits: 4,
        markerColor: processColor('#999999'),
        textColor: processColor('white'),
        markerFontSize: 16,
      },
      description: {
        text: ' ',
        textSize: 15,
        textColor: processColor('darkgray'),

      },
      chartVisible: false,
      projectAnalysisMonth: {},
      isLoading: true
    };
    this.requestData = this.requestData.bind(this);
    this.requestUrl1 = this.requestUrl1.bind(this);

  }

  requestData = (urlprops) => {
    let url = Url.url + Url.factory + Url.Fid + 'GetFactoryAnalysisSingle';
    if (typeof urlprops != 'undefined') {
      url = urlprops
      this.setState({isLoading: true})
    }
    console.log("🚀 ~ file: BarChartScreen.js ~ line 119 ~ StackedBarChartScreen ~ url", url)
    console.log(url)
    fetch(url, {
      credentials: 'include',
    }).then(res => {
      return res.json()
    }).then(resData => {
      let yearMonthListNEW = []
      resData.yearMonthList.map((item, index) => {
        let tmp = '', tmpnew = '';
        tmp = item;
        tmpnew = tmp.replace(/年/g, "-").replace(/月/g, "");
        yearMonthListNEW.push(tmpnew)
      })
      console.log("🚀 ~ file: BarChartScreen.js ~ line 130 ~ StackedBarChartScreen ~ resData.yearMonthList.map ~ yearMonthListNEW", yearMonthListNEW)

      this.setState({
        resData: resData,
        yearMonthList: yearMonthListNEW,
        volumeList: resData.volumeList,
        aimAmountList: resData.aimAmountList,
        capacityList: resData.capacityList,
        planVolumeList: resData.planVolumeList,
        isLoading: false
      });
      this.refs.chart.moveViewToAnimated(10, 0, 'left', 1000)
      console.log(resData)
    }).catch((error) => {
      console.log(error);
    });
  }

  requestUrl1 = () => {
    const urlTest = Url.url + Url.factory + Url.Fid + 'GetFacProjectAnalysisMonth'
    this.requestData1(urlTest);
  }

  requestData1 = (url) => {
    let projectNameList = [], planVolumeList = [], volumeList = [], Data = {};
    fetch(url, {
      credentials: 'include',
    }).then(res => {
      return res.json();
    }).then(resData => {
      resData.map((item, index) => {
        if (item.projectAbb != null) {
          projectNameList.push(item.projectAbb);
        } else {
          projectNameList.push(item.projectName);
        }
        planVolumeList.push(item.planVolume);
        volumeList.push(item.volume);
      })

      Data.Name = projectNameList;
      Data.Vol1 = planVolumeList;
      Data.Vol2 = volumeList;


      this.ref.ReFlash(Data)
      console.log(Data)

      this.setState({
        projectAnalysisMonth: Data
      })

    }).catch((error) => {
      console.log(error);
    });
  }


  componentDidMount() {
    //this.requestUrl1();
    //this.requestData();

    // in this example, there are line, bar, candle, scatter, bubble in this combined chart.
    // according to MpAndroidChart, the default data sequence is line, bar, scatter, candle, bubble.
    // so 4 should be used as dataIndex to highlight bubble data.

    // if there is only bar, bubble in this combined chart.
    // 1 should be used as dataIndex to highlight bubble data.
    //this.setState({ ...this.state, highlights: [{ x: 1, }, { x: 2, }] });

  }

  handleSelect(event) {

    let entry = event.nativeEvent
    if (entry == null) {
      this.setState({ ...this.state, selectedEntry: null })
    } else {
      this.setState({ ...this.state, selectedEntry: JSON.stringify(entry) })
    }

  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#F5FCFF',
        }}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
    } else {
      return (
        <View style={{ flex: 1 }} >
          <View style={{ flexDirection: 'row', marginBottom: 0 }}>
            <Text style={styles.text}>单位：万m³</Text>
          </View>
          <View style={styles.container}>
            <BarChart
              scaleEnabled={false}
              description={"测试"}
              style={styles.chart}
              //highlightColor = { processColor('red')}
              //highlightFullBarEnabled = {true}
              //drawBorders = {true}
              //drawBarShadow = {true}
              xAxis={{
                enabled: true,
                //yOffset: 100,
                valueFormatter: this.state.yearMonthList,
                granularityEnabled: true,
                granularity: 1,
                axisMaximum: this.state.yearMonthList.length,
                axisMinimum: 0,
                fontFamily: "HelveticaNeue-Medium",
                drawAxisLine: false,
                centerAxisLabels: true,
                setDrawGirdLines: false,
                drawGridLines: false,
                gridLineWidth: 1,
                position: "BOTTOM",
                valueFormatterPattern: '月份',
                //yOffset: 10
              }}
              yAxis={{
                left: {
                  enabled: true,
                  drawAxisLine: false,
                  gridColor: processColor('#e9e9e9'),
                  axisMinimum: 0,
                  labelCount: 4,
                  labelCountForce: 4
                  //granularityEnabled: true
                  //drawLimitLinesBehindData: true
                },
                right: {
                  enabled: false,

                }
              }}
              data={{
                dataSets: [{
                  values: this.state.capacityList,
                  label: '理论产量',
                  config: {
                    drawValues: false,
                    colors: [processColor('#32D185')],
                  }
                }, {
                  values: this.state.planVolumeList,
                  label: '计划产量',
                  config: {
                    drawValues: false,
                    colors: [processColor('#8579E8')],
                  }
                }, {
                  values: this.state.volumeList,
                  label: '实际产量',
                  config: {
                    drawValues: false,
                    colors: [processColor('#FF7A38')],
                  }
                }],
                config: {
                  barWidth: 0.11,
                  group: {
                    fromX: 0,
                    groupSpace: 0.34,
                    barSpace: 0.11,
                  },
                }
              }}
              legend={{
                enabled: true,
                //textSize: RFT * 2.8,
                //form: 'CIRCLE',
                horizontalAlignment: 'CENTER',
                verticalAlignment: 'BOTTOM',
                yEntrySpace: 40,
                formToTextSpace: 16,
                xEntrySpace: 40,
                //formSize: 11,
                //textSize: 11
              }}
              drawValueAboveBar={true}
              onSelect={this.handleSelect.bind(this)}
              onChange={(event) => console.log(event.nativeEvent)}
              highlights={[{
                dataSetIndex: 5
              }]}
              marker={this.state.marker}
              autoScaleMinMaxEnabled={true}
              chartDescription={this.state.description}
              animation={{
                durationX: 0,
                durationY: 1500,
                easingY: 'EaseInOutQuart'
              }}
              visibleRange={{
                x: { min: 1, max: 4 }
              }}
              ref="chart"
            />
            <Overlay
              fullScreen={false}
              animationType='fade'
              overlayStyle={{ width: deviceWidth * 0.9, height: deviceWidth * 1.1 }}
              isVisible={this.state.chartVisible}
              onBackdropPress={() => {
                this.setState({ chartVisible: !this.state.chartVisible });
              }}
              onRequestClose={() => {
                this.setState({ chartVisible: !this.state.chartVisible });
              }} >
              <View style={styles.view}>
                <Text style={styles.text1}>月计划与实际产量对比</Text>
                <View style={styles.chartview1}>
                  <View style={styles.chart1}>
                    <BarChart1
                      //prpsData = {this.state.projectAnalysisMonth}
                      ref={ref => { this.ref = ref }}
                    />
                  </View>
                </View>
              </View>
            </Overlay>
          </View>

        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent'
  },
  chart: {
    flex: 1,
    marginBottom: 6
  },
  text: {
    marginLeft: RFT * 1.5,
    color: 'gray',
    fontSize: 10
  },
  view: {
    flex: 1,
    backgroundColor: '#F9F9F9'
  },
  text1: {
    color: '#535c68',
    fontSize: RFT * 3.5,
    paddingLeft: 15,
    marginTop: 10,
  },
  chartview1: {
    flex: 1,
    marginTop: 10,
    backgroundColor: '#FFFFFF',
    borderRadius: 10
  },
  chart1: {
    flex: 1,

  }
});


//export default StackedBarChartScreen;
export default withNavigation(StackedBarChartScreen)
