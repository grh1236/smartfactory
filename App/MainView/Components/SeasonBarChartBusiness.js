import React from 'react';
import {
  AppRegistry,
  StyleSheet,
  View, processColor,
  ScrollView
} from 'react-native';
import { Text } from 'react-native-elements'
import { BarChart } from 'react-native-charts-wrapper';
import Url from '../../Url/Url';
import { RFT } from '../../Url/Pixal';





class StackedBarChartScreen extends React.Component {

  constructor() {
    super();

    this.state = {
      quarterList: ['第一季度', '第二季度', '第三季度', '第四季度',],
      aimList: [5, 40, 43, 61,],
      conList: [40, 5, 50, 23,],
      completionRate: [1, 2, 3, 4],

      legend: {
        enabled: true,
        //textSize: 12,
        form: "SQUARE",
        formSize: 14,
        xEntrySpace: 10,
        yEntrySpace: 10,
        wordWrapEnabled: true,
        fromToTextSpace: 10,
        horizontalAlignment: 'RIGHT',
        verticalAlignment: 'TOP',
        drawInside: false
      },
      xAxis: {
        valueFormatter: ['一季度', '二季度', '三季度', '四季度'],
        granularityEnabled: true,
        granularity: 1,
        axisMaximum: 4,
        axisMinimum: 0,
        centerAxisLabels: true,
        setDrawGirdLines: false,
        drawGridLines: false,
        gridLineWidth: 0,
        position: "BOTTOM",
        textSize: RFT * 1,
        labelCountForce: false,
        labelCount: 4,
        fromX: 1,
        drawAxisLine: false,
      },
      yAxis: {
        left: {
          enabled: true,
        },
        right: {
          enabled: false,
        }
      },
      scaleEnabled: true,
      autoScaleMinMaxEnabled: true,
      marker: {
        enabled: true,
        digits: 2,
        markerColor: processColor('#999999'),
        textColor: processColor('white'),
        markerFontSize: 14,
      },
      description: {
        text: ' ',
        textSize: 15,
        textColor: processColor('darkgray'),

      },
    };
  }

  requestData = () => {
    const Burl = Url.url + Url.factory + Url.Fid;
    const url = Burl + 'GetSignMoney';
    console.log(url)
    fetch(url, {
      credentials: 'include',
    }).then(res => {
      return res.json()
    }).then(resData => {
      this.setState({
        resData: resData,
        aimList: resData.aimList,
        conList: resData.conList,
        completionRate: resData.completionRate,
        quarterList: resData.quarterList
      });

    }).catch((error) => {
      console.log(error);
    });
  }


  componentDidMount() {
    // in this example, there are line, bar, candle, scatter, bubble in this combined chart.
    // according to MpAndroidChart, the default data sequence is line, bar, scatter, candle, bubble.
    // so 4 should be used as dataIndex to highlight bubble data.

    // if there is only bar, bubble in this combined chart.
    // 1 should be used as dataIndex to highlight bubble data.
    this.requestData();
    this.setState({ ...this.state, highlights: [{ x: 1 }, { x: 2 }] })
  }

  handleSelect(event) {
    this.requestData();
    let entry = event.nativeEvent
    if (entry == null) {
      this.setState({ ...this.state, selectedEntry: null })
    } else {
      this.setState({ ...this.state, selectedEntry: JSON.stringify(entry) })
    }

    console.log(event.nativeEvent)
  }

  render() {
    return (
      <View style={{ flex: 2 }}>
        <View style={styles.container}>
          <BarChart
            scaleEnabled={false}
            style={styles.chart}
            yAxis={this.state.yAxis}
            xAxis={{
              valueFormatter: this.state.quarterList,
              granularityEnabled: true,
              granularity: 1,
              axisMaximum: 4,
              axisMinimum: 0,
              centerAxisLabels: true,
              setDrawGirdLines: false,
              drawGridLines: false,
              gridLineWidth: 0,
              position: "BOTTOM",
              //textSize: RFT*3,
              labelCountForce: false,
              labelCount: 4,
              fromX: 1,
              drawAxisLine: false,
              //labelRotationAngle: -25,
            }}
            data={{
              dataSets: [{
                values: this.state.aimList,
                label: '计划指标全额',
                config: {
                  drawValues: false,
                  colors: [processColor('#509FF0')],
                }
              }, {
                values: this.state.conList,
                label: '实际签约全额',
                config: {
                  drawValues: false,
                  colors: [processColor('#FF7A38')],
                }
              }
              ],
              config: {
                barWidth: 0.3,
                group: {
                  fromX: 0,
                  groupSpace: 0.2,
                  barSpace: 0.1,
                },
              }
            }}
            legend={this.state.legend}
            drawValueAboveBar={false}
            onSelect={this.handleSelect.bind(this)}
            onChange={(event) => console.log(event.nativeEvent)}
            highlights={this.state.highlights}
            marker={this.state.marker}
            autoScaleMinMaxEnabled={true}
            chartDescription={this.state.description}
            animation={{
              durationX: 0,
              durationY: 1500,
              easingY: 'EaseInOutQuart'
            }}
          />
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  chart: {
    flex: 1
  }
});


export default StackedBarChartScreen;
