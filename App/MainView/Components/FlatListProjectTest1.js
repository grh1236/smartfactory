import React from 'react';
import { ActivityIndicator, View, FlatList, StylesSheet, processColor, Dimensions } from 'react-native';
import { Text, Card, Image, Button } from 'react-native-elements'
import { PieChart, HorizontalBarChart } from 'react-native-charts-wrapper';
import { Echarts, echarts } from 'react-native-secharts';
import { ScrollView } from 'react-native-gesture-handler';
import Url from '../../Url/Url';
import { RFT } from '../../Url/Pixal';

const { height, width } = Dimensions.get('window');
const Url1 = '';
let i = 0, j = 0, index = 0;
let No = ' ';
var NoNew = ' ';
let newdata = [];
let newdatatest = [];
var firstFloor = [];
let releaseNo = 0, inProducedNo = 0, notProducedNo = 0, installNo = 0, notInstallNo = 0, allNo = 0;
let releaseNo1 = 0, inProducedNo1 = 0, notProducedNo1 = 0, installNo1 = 0, notInstallNo1 = 0, allNo1 = 0;

let releaseAll = 0, inProducedAll = 0, notProducedAll = 0, installAll = 0, notInstallAll = 0;

//var testData = 
var newdataNo = [];



class FlatListChartcoloum extends React.Component {
  constructor() {
    super()
    this.state = {
      isLoading: true,
      testData: [{
        "id": 1,
        "factoryId": "74e1cbc6-2cfc-4d6f-bcd1-739d28e4c74d",
        "projectId": "AE37158119BD4502BA92485B2757248E",
        "projectName": "中建壹品澜郡项目",
        "floorNoName": "1#",
        "floorNo": 1,
        "inProduced": 0,
        "notProduced": 12,
        "notInstall": 0,
        "install": 0,
        "release": 0
      }],
      newdataNo: [],
      description: {
        text: ' ',
        textSize: 15,
        textColor: processColor('darkgray'),
      },
      resData: [{
        "id": 1,
        "factoryId": "74e1cbc6-2cfc-4d6f-bcd1-739d28e4c74d",
        "projectId": "AE37158119BD4502BA92485B2757248E",
        "projectName": "中建壹品澜郡项目",
        "floorNoName": "1#",
        "floorNo": 1,
        "inProduced": 0,
        "notProduced": 12,
        "notInstall": 0,
        "install": 0,
        "release": 0
      },
      {
        "id": 2,
        "factoryId": "74e1cbc6-2cfc-4d6f-bcd1-739d28e4c74d",
        "projectId": "AE37158119BD4502BA92485B2757248E",
        "projectName": "中建壹品澜郡项目",
        "floorNoName": "1#",
        "floorNo": 2,
        "inProduced": 0,
        "notProduced": 104,
        "notInstall": 1,
        "install": 0,
        "release": 0
      },],
      data: [],

      yAxis: {
        left: {
          enabled: false
        },
        right: {
          enabled: false
        }
      },
      description: {
        text: ' ',
        textSize: 17,
        textColor: processColor('darkgray')
      },
      legend: {
        enabled: false,
        textSize: 10,
        form: "SQUARE",
        formSize: 10,
        xEntrySpace: 20,
        yEntrySpace: 15,
        wordWrapEnabled: true,
      },
      marker: {
        enabled: true,
        markerColor: processColor('#999999'),
        textColor: processColor('white'),
        markerFontSize: 12,
      },
      highlights: [{ x: 1, stackIndex: 2 }, { x: 2, stackIndex: 1 }],

    }
    this.requestData = this.requestData.bind(this);
    this.CountNo = this.CountNo.bind(this)
    this.ReflashData = this.ReflashData.bind(this)
  }



  ReflashData(newTestData) {
    //console.log(newTestData)
    //更新卡片中的数据
    newdata = [];
    newdatatest = [];
    releaseAll = 0, inProducedAll = 0, notProducedAll = 0, installAll = 0, notInstallAll = 0;
    releaseNo = 0, inProducedNo = 0, notProducedNo = 0, installNo = 0, notInstallNo = 0, No = '', allNo = 0;
    for (let index = 0; index < newTestData.length; index++) {
      const itemall = newTestData[index];
      releaseAll += itemall.release;
      inProducedAll += itemall.inProduced;
      notProducedAll += itemall.notProduced;
      installAll += itemall.install;
      notInstallAll += itemall.notInstall;
    }
    this.CountNo(newTestData);
    //releaseNo = 0, inProducedNo = 0, notProducedNo = 0, installNo = 0, notInstallNo = 0, No = '';
    this.setState({
      //newTestData来自按钮选择参数传值
      testData: newTestData,
      isLoading: false,
    }, () => {

    })
  }

  handleSelect(event) {
    let entry = event.nativeEvent
    // console.log(entry)
    if (entry == null) {
      this.setState({ ...this.state, selectedEntry: null })
    } else {
      this.setState({ ...this.state, selectedEntry: JSON.stringify(entry) })
    }

    ////console.log(event.nativeEvent)
  }


  componentDidMount() {
    this.requestData();
  }

  requestData = () => {
    const url = this.props.url ? this.props.url : Url1;
    //console.log(this.props.url)
    fetch(url, {
      credentials: 'include',
    }).then(res => {
      return res.json()
    }).then(resData => {
      this.setState({
        resData: resData,
      });
      this.CountNo(resData);
      ////console.log(resData)
    }).catch((error) => {
      //console.log(error);
    });
  }

  CountNo = (propsData) => {
    //console.log(this.props.Data)
    const length = propsData.length
    propsData.map((item, index) => {
      let tmp = {};
      if (No !== item.floorNoName) {
        No = item.floorNoName;
        firstFloor = item;
        i = 0;

        newdata.map((itemadd, indexadd) => {
          releaseNo += itemadd.release;
          inProducedNo += itemadd.inProduced;
          notProducedNo += itemadd.notProduced;
          installNo += itemadd.install;
          notInstallNo += itemadd.notInstall;
          ////console.log(releaseNo, inProducedNo, notProducedNo, installNo, notInstallNo, itemadd.floorNoName)
          return (releaseNo, inProducedNo, notProducedNo, installNo, notInstallNo)
        });

        allNo = releaseNo + inProducedNo + notProducedNo + installNo + notInstallNo

        tmp.No = NoNew
        tmp.releaseNo = releaseNo
        tmp.inProducedNo = inProducedNo
        tmp.notProducedNo = notProducedNo
        tmp.installNo = installNo
        tmp.notInstallNo = notInstallNo
        tmp.allNo = allNo;

        newdatatest.push(tmp);

        releaseNo1 = releaseNo, inProducedNo1 = inProducedNo, notProducedNo1 = notProducedNo, installNo1 = installNo, notInstallNo1 = notInstallNo, allNo1 = allNo;
        allNo1 = allNo;

        releaseNo = 0, inProducedNo = 0, notProducedNo = 0, installNo = 0, notInstallNo = 0, allNo = 0;
        allNo = 0;
        newdata = []
      } else if (length == (index + 1)) {
        NoNew = No;
        newdata[++i] = item;
        i = 0;

        releaseNo = 0, inProducedNo = 0, notProducedNo = 0, installNo = 0, notInstallNo = 0, allNo = 0;
        allNo = 0;

        newdata.map((itemadd, indexadd) => {
          releaseNo += itemadd.release;
          inProducedNo += itemadd.inProduced;
          notProducedNo += itemadd.notProduced;
          installNo += itemadd.install;
          notInstallNo += itemadd.notInstall;
          ////console.log(releaseNo, inProducedNo, notProducedNo, installNo, notInstallNo, itemadd.floorNoName)
          return (releaseNo, inProducedNo, notProducedNo, installNo, notInstallNo)
        })

        allNo = releaseNo + inProducedNo + notProducedNo + installNo + notInstallNo
        tmp.No = NoNew
        tmp.releaseNo = releaseNo
        tmp.inProducedNo = inProducedNo
        tmp.notProducedNo = notProducedNo
        tmp.installNo = installNo
        tmp.notInstallNo = notInstallNo
        tmp.allNo = allNo;

        newdatatest.push(tmp);

        releaseNo1 = releaseNo, inProducedNo1 = inProducedNo, notProducedNo1 = notProducedNo, installNo1 = installNo, notInstallNo1 = notInstallNo, allNo1 = allNo;

        allNo1 = allNo;


        releaseNo = 0, inProducedNo = 0, notProducedNo = 0, installNo = 0, notInstallNo = 0, allNo = 0;
        allNo = 0;
        newdata = []
      }
      else {
        newdata[0] = firstFloor;
        newdata[++i] = item;
        NoNew = No;
      }
    })
  }



  render() {
    //console.log(releaseAll, inProducedAll, notProducedAll, installAll, notInstallAll)
    const { testData } = this.state;
    const length = this.state.testData.length;
    //console.log(testData);
    if (this.props.stateloading === true) {
      return (
        <View style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#F5FCFF',
        }}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
    } else {
      return (
        <View style={{ flex: 1 }}>
          <View style={{ flex: 1 }}>
            <HorizontalBarChart
              //touchEnabled={false}
              scaleEnabled={false}
              chartDescription={this.state.description}
              style={{ height: width * 0.1, }}
              data={{
                dataSets: [{
                  values: [{ y: [notProducedAll, inProducedAll, releaseAll, notInstallAll, installAll,] }],
                  label: '',
                  config: {
                    colors: [processColor('#FFCF18'), processColor('#FF9D18'), processColor('#8579E8'),
                    processColor('#41CCFF'),
                    processColor('#419FFF'),
                    ],
                    drawValues: false,
                    highlightAlpha: 15,
                    valueTextSize: 8,
                    stackLabels: ['未生产', '在生产', '脱模待检', '成检合格', '已出库',],
                    horizontalAlignment: "CENTER",
                  }
                }],
              }}
              highlightFullBarEnabled={true}
              //drawValueAboveBar={true}
              xAxis={{
                enabled: false,
                //axisMinimum: 0,
              }}
              //onSelect={this.handleSelect.bind(this)}
              // onChange={(event) => console.log(event.nativeEvent)}
              yAxis={{
                left: {
                  enabled: false,
                  axisMinimum: 0,
                },
                right: {
                  enabled: true,
                  axisMinimum: 0,
                  drawLimitLinesBehindData: true
                }
              }}
              description={this.state.description}
              legend={{
                enabled: true,
                textSize: 8,
                form: "SQUARE",
                formSize: 8,
                xEntrySpace: 10,
                //yEntrySpace: 15,
                wordWrapEnabled: true,
                horizontalAlignment: 'CENTER',
                verticalAlignment: 'TOP',

              }}
              marker={this.state.marker}
              animation={{
                durationX: 2000,
                durationY: 1500,
                easingY: 'EaseInOutQuart'
              }}
            />
          </View>
          {/* <View style={{ flex: 0.3 }}></View> */}
          <View style={{ flex: 2 }}>
            <ScrollView horizontal={true} scrollEnabled={true} showsHorizontalScrollIndicator={false}>
              {
                newdatatest.map((item, index) => {
                  let { No, inProducedNo, installNo, notInstallNo, notProducedNo, releaseNo, allNo } = item;
                  if (releaseNo == 0 && inProducedNo == 0 && installNo == 0 && notProducedNo == 0 && notInstallNo == 0) {

                    return (
                      <View ></View>
                    )
                  } else {
                    return (
                      <View style={{ flex: 1, width: width * 0.4 }} >
                        <Echarts
                          height={width * 0.3}
                          width={width * 0.4}
                          option={{
                            tooltip: {
                              trigger: 'item',
                              formatter: "{a} <br/>{b}: {c} ({d}%)"
                            },
                            legend: {
                              show: false,
                              orient: 'vertical',
                              x: 'left',
                              textStyle: {
                                fontSize: RFT * 2,
                              },
                              itemWidth: RFT * 2,
                              itemHeight: RFT * 2,
                              data: ['未生产', '在生产', '脱模待检', '成检合格', '已出库']
                            },
                            color: ['#FFCF18', '#FF9D18',
                              '#8579E8',
                              '#41CCFF',
                              '#419FFF',
                            ],
                            series: [
                              {
                                name: '构件状态：',
                                type: 'pie',
                                radius: ['50%', '70%'],
                                avoidLabelOverlap: false,
                                label: {
                                  normal: {
                                    show: true,
                                    position: 'center',
                                    formatter:
                                      '总数:\n ' + allNo + '件'
                                    ,
                                    textStyle: {
                                      fontSize: RFT * 2.5,
                                      color: '#535c68'
                                    }
                                  },
                                  emphasis: {
                                    show: false,
                                    textStyle: {
                                      fontSize: RFT * 2.6,
                                      fontWeight: 'bold'
                                    }
                                  }
                                },
                                labelLine: {
                                  normal: {
                                    lineStyle: {
                                      show: false,
                                      color: '#235894'
                                    }
                                  }
                                },
                                data: [
                                  { value: notProducedNo, name: '未生产' },
                                  { value: inProducedNo, name: '在生产' },
                                  { value: releaseNo, name: '脱模待检' },
                                  { value: notInstallNo, name: '成检合格' },
                                  { value: installNo, name: '已出库' }
                                ]
                              }
                            ]
                          }} />
                        {/*  <PieChart
                          noDataText='NO DATA'
                          style={{ height: width * 0.6 }}
                          chartDescription={this.state.description}
                          marker={this.state.marker}
                          logEnabled={true}
                          rotationEnabled={true}
                          drawEntryLabels={false}
                          rotationAngle={45}
                          usePercentValues={false}
                          holeRadius={65}
                          transparentCircleRadius={45}
                          styledCenterText={{ text: '总数:\n' + allNo + '件', color: processColor('black'), size: 12 }}
                          centerTextRadiusPercent={80}
                          holeColor={processColor('white')}
                          transparentCircleColor={processColor('white')}
                          data={{
                            dataSets: [{
                              values: [
                                { value: notProducedNo, label: '未生产' },
                                { value: inProducedNo, label: '已生产' },
                                { value: releaseNo, label: '脱模待检' },
                                { value: notInstallNo, label: '未安装' },
                                { value: installNo, label: '已安装' },

                              ],
                              label: ' ',
                              config: {
                                drawValues: true,
                                colors: [processColor('#FFCF18'), processColor('#FF9D18'),
                                processColor('#8579E8'),
                                processColor('#41CCFF'),
                                processColor('#419FFF'),
                                ],
                                valueTextSize: 14,
                                valueTextColor: processColor('#535c68'),
                                sliceSpace: 1,
                                selectionShift: 5,
                                xValuePosition: "OUTSIDE_SLICE",
                                yValuePosition: "OUTSIDE_SLICE",
                                //valueFormatter: "#.#'%'",
                                valueLineColor: processColor('#535c68'),
                                //valueLinePart1Length: 0.35,
                                //valueLinePart2Length: 0,
                                //valueLinePart1OffsetPercentage: 0.5,
                                valueLineVariableLength: true
                              }
                            }],
                          }}
                          legend={this.state.legend}
                          animation={{
                            durationX: 2000,
                            durationY: 1500,
                            easingY: 'EaseInOutQuart'
                          }}
                        /> */}
                        <Text style={{ flex: 1, textAlign: 'center', textAlignVertical: 'center', fontSize: RFT * 2.5, }}>{No}</Text>
                      </View>
                    )
                  }
                })
              }
            </ScrollView >
          </View>
        </View>

      )
    }

  }


}
export default FlatListChartcoloum




























{/* <View style={{ flexDirection: 'row', flex: 5 }}>
                        <View style={{ flexDirection: 'column-reverse', justifyContent: 'space-evenly', alignContent: 'stretch' }}>
                          {
                            newdata.map((itemfloor, i) => {
                              ////console.log(itemfloor.floorNo)
                              return (
                                <View style={{ flex: 1 }} >
                                   <Text style={{ height: 40, textAlign: 'center', textAlignVertical: 'center' }}>{itemfloor.floorNo}层</Text>
                                 </View>
                              )
                            })
                          }
                        </View>
                        <View style={{ flexDirection: 'column-reverse', justifyContent: 'space-evenly', alignContent: 'stretch' }}>
                          {
                            newdata.map((itemnew, index) => {
                              return (
                                <View style={{ flex: 1 }}>
                                 <HorizontalBarChart
                                    chartDescription={this.state.description}
                                    key={index}
                                    style={{ height: 40, width: 200, }}
                                    data={{
                                      dataSets: [{
                                        values: [{ y: [itemnew.release, itemnew.install, itemnew.notInstall, itemnew.inProduced, itemnew.notProduced] }],
                                        label: '',
                                        config: {
                                          colors: [processColor('#8579E8'), processColor('#419FFF'), processColor('#41CCFF'), processColor('#FF9D18'), processColor('#FFFf37')],
                                          stackLabels: ['脱模待检', '已安装', '待安装', '在生产', '未生产'],
                                          horizontalAlignment: "CENTER",
                                        }
                                      }],
                                    }}
                                    xAxis={{
                                      enabled: false,
                                    }}
                                    yAxis={this.state.yAxis}
                                    description={this.state.description}
                                    legend={this.state.legend}
                                    scaleEnabled={false}

                                    description={
                                      {
                                        text: ' ',
                                        textSize: 15,
                                        textColor: processColor('darkgray'),
                                      }
                                    }
                                  />
                                </View>
                              )
                            })
                          }
                        </View>
                      </View> */}
























/* < ScrollView horizontal={true} scrollEnabled={true} >
            {
              this.props.Data.map((item, index) => {
                ////console.log(item)
                if (No != item.floorNoName) {
                  //console.log('newdata:')
                  //console.log(newdata)
                  No = item.floorNoName;
                  firstFloor = item;
                  i = 0;
                  newdata.map((itemadd, indexadd) => {
                    releaseNo += itemadd.release;
                    inProducedNo += itemadd.inProduced;
                    notProducedNo += itemadd.notProduced;
                    installNo += itemadd.install;
                    notInstallNo += itemadd.notInstall;
                    ////console.log(releaseNo, inProducedNo, notProducedNo, installNo, notInstallNo, itemadd.floorNoName)
                    return (releaseNo, inProducedNo, notProducedNo, installNo, notInstallNo)
                  })
                  var tmp = [];
                  let length = newdata.length;
                  //console.log('length:' + length)
                  for (let ii = 0; ii < length - 1; ii++) {
                    //console.log(newdata);
                    for (let j = 0; j < length - ii - 1; j++) {
                      if (newdata[j].floorNo > newdata[j + 1].floorNo) {
                        tmp = newdata[j];
                        newdata[j] = newdata[j + 1];
                        newdata[j + 1] = tmp;
                        //console.log(newdata[j])
                        //console.log(tmp)
                      }
                    }
                  }
                  //console.log(newdata);
                  return (
                    <View style={{ flex: 1 }} >
                      <View style={{ flex: 1 }}>
                        <PieChart
                          style={{ flex: 3.5 }}
                          chartDescription={this.state.description}
                          marker={this.state.marker}
                          logEnabled={true}
                          rotationEnabled={true}
                          rotationAngle={45}
                          usePercentValues={true}
                          holeRadius={45}
                          transparentCircleRadius={45}
                          data={{
                            dataSets: [{
                              values: [
                                { value: releaseNo, label: '脱模待检' },
                                { value: installNo, label: '已安装' },
                                { value: notInstallNo, label: '未安装' },
                                { value: inProducedNo, label: '已生产' },
                                { value: notProducedNo, label: '未生产' }
                              ],
                              label: ' ',
                              config: {
                                colors: [processColor('#8579E8'), processColor('#419FFF'), processColor('#41CCFF'), processColor('#FF9D18'), processColor('#FFFf37')],
                                valueTextSize: 10,
                                valueTextColor: processColor('green'),
                                sliceSpace: 5,
                                selectionShift: 13,
                                xValuePosition: "OUTSIDE_SLICE",
                                yValuePosition: "OUTSIDE_SLICE",
                                valueFormatter: "#.#'%'",
                                valueLineColor: processColor('green'),
                                valueLinePart1Length: 0.5
                              }
                            }],
                          }}
                          legend={this.state.legend}
                        />
                        <Text style={{ flex: 1, textAlign: 'center', textAlignVertical: 'center' }}>{NoNew}</Text>
                      </View>
                      <View style={{ flexDirection: 'row', flex: 0 }}>
                        <View style={{ flexDirection: 'column-reverse', justifyContent: 'space-evenly', alignContent: 'stretch' }}>
                          {
                            newdata.map((itemfloor, i) => {
                              ////console.log(itemfloor.floorNo)
                              return (
                                <View style={{ flex: 1 }} >
                                   <Text style={{ height: 40, textAlign: 'center', textAlignVertical: 'center' }}>{itemfloor.floorNo}层</Text>
                                 </View>
                              )
                            })
                          }
                        </View>
                        <View style={{ flexDirection: 'column-reverse', justifyContent: 'space-evenly', alignContent: 'stretch' }}>
                          {
                            newdata.map((itemnew, index) => {
                              return (
                                <View style={{ flex: 1 }}>
                                 <HorizontalBarChart
                                    chartDescription={this.state.description}
                                    key={index}
                                    style={{ height: 40, width: 200, }}
                                    data={{
                                      dataSets: [{
                                        values: [{ y: [itemnew.release, itemnew.install, itemnew.notInstall, itemnew.inProduced, itemnew.notProduced] }],
                                        label: '',
                                        config: {
                                          colors: [processColor('#8579E8'), processColor('#419FFF'), processColor('#41CCFF'), processColor('#FF9D18'), processColor('#FFFf37')],
                                          stackLabels: ['脱模待检', '已安装', '待安装', '在生产', '未生产'],
                                          horizontalAlignment: "CENTER",
                                        }
                                      }],
                                    }}
                                    xAxis={{
                                      enabled: false,
                                    }}
                                    yAxis={this.state.yAxis}
                                    description={this.state.description}
                                    legend={this.state.legend}
                                    scaleEnabled={false}

                                    description={
                                      {
                                        text: ' ',
                                        textSize: 15,
                                        textColor: processColor('darkgray'),
                                      }
                                    }
                                  />
                                </View>
                              )
                            })
                          }
                        </View>
                      </View>
                    </View>
                  )

                } else {
                  newdata[0] = firstFloor;
                  newdata[++i] = item;
                  releaseNo = 0, inProducedNo = 0, notProducedNo = 0, installNo = 0, notInstallNo = 0;
                  NoNew = No;

                }
              })
            }
          </ScrollView > */





























