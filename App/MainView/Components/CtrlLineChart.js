import React from 'react';
import { View, ScrollView, processColor, Dimensions, PixelRatio, ActivityIndicator } from 'react-native';
import { Button, Text, } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import LineChartGradient from '../Components/LineChart';
import Url from '../../Url/Url'
import { MainStyles } from './MainStyles';
import { deviceWidth } from '../../Url/Pixal';

const { height, width } = Dimensions.get('screen');
const fontScale = PixelRatio.getFontScale(); // 返回字体大小缩放比例
const pixelRatio = PixelRatio.get(); // 当前设备的像素密度
const RVW = width / 100;
const RFT = RVW / fontScale;


export default class CarRoomButton extends React.Component {
  constructor() {
    super();
    this.state = {
      isLoading: true,
      focusButton: [],
      resData: ['1', '2', '3'],
      title: ['1车间', '2车间', '3车间', '4车间', '5车间', '6车间',],
      productLineName: '正在加载生产线信息...',
      yearMonthList: [],
      provolumeList: [
        {
          y: 65,
          x: 0,
          marker: "65 "
        },
        {
          y: 77,
          x: 1,
          marker: "77"
        },
        {
          y: 76,
          x: 2,
          marker: "76"
        },
        {
          y: 74,
          x: 3,
          marker: "74"
        },
        {
          y: 76,
          x: 4,
          marker: "76"
        },
        {
          y: 65,
          x: 5,
          marker: "Today: 65"
        }
      ],
      capacityList: [
        {
          y: 35,
          x: 0,
          marker: "35"
        },
        {
          y: 47,
          x: 1,
          marker: "47"
        },
        {
          y: 46,
          x: 2,
          marker: "46"
        },
        {
          y: 44,
          x: 3,
          marker: "44"
        },
        {
          y: 46,
          x: 4,
          marker: "46"
        },
        {
          y: 35,
          x: 5,
          marker: "Today: 35"
        }
      ]
    }
  }

  handleSelect(event) {
    let entry = event.nativeEvent
    if (entry == null) {
      this.setState({ ...this.state, selectedEntry: null })
    } else {
      this.setState({ ...this.state, selectedEntry: JSON.stringify(entry) })
    }

    console.log(event.nativeEvent)
  }


  requestData = () => {
    const url = Url.url + Url.factory + Url.Fid + Url.StaticType + 'GetFacLineAnalysisSingle';
    this.setState({isLoading: true})
    console.log("🚀 ~ file: CtrlLineChart.js:108 ~ CarRoomButton ~ url:", url)
    fetch(url, {
      credentials: 'include',
    }).then(res => {
      return res.json()
    }).then(resData => {
      resData.map((item, index) => {
        let tmp = '', tmpnew = '';
        tmp = JSON.stringify(item.yearMonthList);
        tmpnew = tmp.replace(/年/g, "-").replace(/月/g, "");
        item.yearMonthList = JSON.parse(tmpnew);
      })
      console.log(resData)
      this.setState({
        resData: resData,
        provolumeList: resData[0].provolumeList,
        capacityList: resData[0].capacityList,
        productLineName: resData[0].productLineName,
        yearMonthList: resData[0].yearMonthList,
        isLoading: false
      });
      //this.refs.chart.moveViewToAnimated(10, 0, 'left', 1000)
    }).catch((error) => {
      console.log(error);
    });
  }

  componentDidMount() {
   // this.requestData();
  }

  render() {
    const { focusButton, isLoading } = this.state;
    if (isLoading) {
      return (
       
          <View style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#F5FCFF',
          }}>
            <ActivityIndicator
              animating={true}
              color='#419fff'
              size="large" />
          </View>
      )
    } else {
      return (
        <View style = {{ top: 0}}>
          <View style={{ flexDirection: 'row', backgroundColor: 'transparent', }}>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{ margin: width * 0.02, marginTop: 0 }}>
              {
                this.state.resData.map((u, i) => {
                  return (
                    <Button
                      key={i}
                      buttonStyle={{
                        borderRadius: -10,
                        borderBottomColor: this.state.focusButton == i ? '#419FFF' : '#999997',
                        borderBottomWidth: this.state.focusButton == i ? 3 : 0,
                        // width: 48,
                        height: 35,
                        marginLeft: 5
                      }}
                      containerStyle={{
                        borderColor: this.state.focusButton == i ? '#419FFF' : '#999997',
                        marginRight: 10
                      }}
                      titleStyle={{
                        fontFamily: 'STHeitiSC-Light',
                        textAlign: 'center',
                        fontSize: 13,
                        fontWeight: "600",
                        //fontSize: RFT * 3.3,
                        color: this.state.focusButton == i ? '#333333' : 'rgba(51,51,51,0.6)',
                      }}
                      type="clear"
                      title={u.productLineName}
                      onPress={() => {
                        console.log(u)
                        this.setState({
                          focusButton: i,
                          yearMonthList: u.yearMonthList,
                          provolumeList: u.provolumeList,
                          capacityList: u.capacityList,
                          productLineName: u.productLineName
                        });

                      }}
                    />
                  )
                })
              }
            </ScrollView>
          </View>
          {/* 数据：微软雅黑， */}
          <View style={MainStyles.chartView}>
            <Text style={MainStyles.chartTitle}>生产曲线图</Text>
              <View style={MainStyles.chart}>
                {/* <Text style={{ fontSize: 20, textAlign: 'center', color: 'white' }}> {this.state.productLineName} </Text> */}
                <Text style={{   marginTop: RFT * 2, color: 'black', fontSize: 12 }}>单位：m³</Text>
                <View style={{ flex: 1}} >
                  <LineChartGradient
                    legend={{
                      enabled: true,
                      //textSize: RFT * 3,
                      form: 'CIRCLE',
                      //wordWrapEnabled: true,
                      //xEntrySpace: 13,
                      // yEntrySpace: 6,
                      textColor: processColor('black'),
                      horizontalAlignment: 'RIGHT',
                      verticalAlignment: 'TOP'
                    }}
                    data={{
                      dataSets: [
                        {
                          values: this.state.provolumeList,
                          label: "实际产能",
                          config: {
                            mode: "HORIZONTAL_BEZIER",
                            drawValues: false,
                            lineWidth: 2,
                            drawCircles: false,
                            circleColor: processColor('#509FF0'),
                            drawCircleHole: false,
                            circleRadius: 3,
                            highlightColor: processColor("transparent"),
                            color: processColor('#509FF0'),
                            drawFilled: false,
                            fillssGradient: {
                              colors: [processColor('#509FF0'), processColor('#509FF0')],
                              positions: [0, 0.5],
                              angle: 90,
                              orientation: "TOP_BOTTOM"
                            },
                            fillAlpha: 1000,
                            valueTextSize: 15
                          }
                        }, {
                          values: this.state.capacityList,
                          label: "理论产能",
                          config: {
                            mode: "HORIZONTAL_BEZIER",
                            drawValues: false,
                            lineWidth: 2,
                            drawCircles: false,
                            circleColor: processColor('#32D185'),
                            drawCircleHole: false,
                            circleRadius: 3,
                            highlightColor: processColor("transparent"),
                            color: processColor('#32D185'),
                            drawFilled: false,
                            fillGradient: {
                              colors: [processColor('#F0FF4B'), processColor('#F0FF4B')],
                              positions: [0, 0.5],
                              angle: 90,
                              orientation: "TOP_BOTTOM"
                            },
                            fillAlpha: 1000,
                            valueTextSize: 15
                          }
                        }
                      ]
                    }}
                    xAxis={{
                      enabled: true,
                      granularity: 1,
                      drawLabels: true,
                      position: "BOTTOM",
                      drawAxisLine: false,
                      drawGridLines: false,
                      axisMaximum: this.state.yearMonthList.length,
                      axisMinimum: -0.5,
                      fontFamily: "HelveticaNeue-Medium",
                      //fontWeight: "bold",
                      textSize: RFT * 3,
                      //textColor: processColor("black"),
                      valueFormatter: this.state.yearMonthList,
                      //labelRotationAngle: -30
                    }}
                    yAxis={{
                      left: {
                        enabled: true,
                        drawAxisLine: false,
                        //textSize: 13,
                        textColor: processColor("#333"),
                        gridColor: processColor("#e9e9e9"),
                        drawLabels: true
                      },
                      right: {
                        enabled: false
                      }
                    }}
                    animation={{
                      durationX: 0,
                      durationY: 1500,
                      easingY: 'EaseInOutQuart'
                    }}
                    onSelect={this.handleSelect.bind(this)}
                    onChange={(event) => console.log(event.nativeEvent)}
                  //ref = "chart"
                  />
                </View>
                <View style={{ height: 10 }}></View>
              </View>
          </View>

        </View>
      )
    }

  }
}
