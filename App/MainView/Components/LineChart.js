import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  processColor,

} from 'react-native';
import { RFT } from '../../Url/Pixal';
import { LineChart } from 'react-native-charts-wrapper';




class LineChartScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      provolumeList: [
        {
          y: 65,
          x: 0,
          marker: "65"
        },
        {
          y: 77,
          x: 1,
          marker: "77"
        },
        {
          y: 76,
          x: 2,
          marker: "76"
        },
        {
          y: 74,
          x: 3,
          marker: "74"
        },
        {
          y: 76,
          x: 4,
          marker: "76"
        },
        {
          y: 65,
          x: 5,
          marker: "Today: 65"
        }
      ],
    }
  }

  componentDidMount() {
    this.refs.chart.moveViewToAnimated(10, 0, 'left', 2000)
  }


  handleSelect(event) {
    let entry = event.nativeEvent;
    if (entry == null) {
      this.setState({ ...this.state, selectedEntry: null });
    } else {
      this.setState({ ...this.state, selectedEntry: JSON.stringify(entry) });
    }

    console.log(event.nativeEvent);
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={styles.container}>
          <LineChart
            {... this.props}
            style={styles.chart}
            data={this.props.data}
            chartDescription={{ text: '' }}
            legend={this.props.legend}
            marker={{
              enabled: true,
              markerColor: 'white',
              textColor: 'black',
              textSize: 18,
              digits: 3,
              form: 'CIRCLE',
              formSize: 14,
              xEntrySpace: 10,
              yEntrySpace: 5,
              wordWrapEnabled: true,
              position: 'center'
            }}
            xAxis={this.props.xAxis}
            yAxis={this.props.yAxis}
            //autoScaleMinMaxEnabled={true}
            animation={{
              durationX: 0,
              durationY: 1500,
              easingY: 'EaseInOutQuart'
            }}
            drawGridBackground={false}
            drawBorders={false}
            touchEnabled={true}
            dragEnabled={true}
            scaleEnabled={false}
            scaleXEnabled={false}
            scaleYEnabled={false}
            pinchZoom={false}
            doubleTapToZoomEnabled={false}
            dragDecelerationEnabled={true}
            maxVisibleValueCount={30}
            keepPositionOnRotation={false}
            animation={{
              durationX: 1000,
              durationY: 1500,
              easingY: 'EaseInOutQuart'
            }}
            visibleRange={{
              x: { min: 4, max: 4 }
            }}
            onSelect={this.handleSelect.bind(this)}
            ref="chart"
          ></LineChart>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,

    //padding: 20
  },
  chart: {
    flex: 1,

    //height: 250
  }
});

export default LineChartScreen;