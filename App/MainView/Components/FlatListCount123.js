import React from 'react';
import { View, FlatList, Text, StyleSheet, ActivityIndicator, Dimensions, TouchableOpacity } from 'react-native';
import Url from '../../Url/Url';
import { Card, Divider, Badge, ListItem } from 'react-native-elements';
import { RFT, RVW } from '../../Url/Pixal';

const { height, width } = Dimensions.get('screen');

let InNum = 0, InVolume = 0, OutNum = 0, OutVolume = 0;
//let newdata = [];
const color =
  [
    '#f1c40f',
    '#f39c12',
    '#41CCFF',
    '#2ecc71',
    '#9b59b6',
    '#449BCE',
    '#e67e22',
    '#d35400',
    '#22BC77',
    '#229DFF',
    '#f1c40f',
  ]


export default class FlatListCount extends React.Component {
  constructor() {
    super();
    this.state = {
      allcount: {
        "id": 8,
        "factoryId": "fa1119c1-ba8e-442c-b2e8-334fafbdae9e",
        "projectId": "075981F601F2441C82C400C8A06AE08F",
        "projectName": "草金路（晋阳路、永康路一线）改造工程（一期）工业化微型管廊及电力通道项目",
        "projectAbb": "草金路项目",
        "projectState": "生产中",
        "state": "总数",
        "compNum": 912,
        "compVolume": 5203.400
      },
      propsData: [],
      testData: [{
        "id": 8,
        "factoryId": "fa1119c1-ba8e-442c-b2e8-334fafbdae9e",
        "projectId": "075981F601F2441C82C400C8A06AE08F",
        "projectName": "草金路（晋阳路、永康路一线）改造工程（一期）工业化微型管廊及电力通道项目",
        "projectAbb": "草金路项目",
        "projectState": "生产中",
        "state": "总数",
        "compNum": 912,
        "compVolume": 5203.400
      }],
      newdata: [],
      Loading: true
    }
  }

  _renderItem({ item, index }) {
    // const { newdata, allcount } = this.state
    const { navigate } = this.props.navigation;
    return (
      <TouchableOpacity
        onPress={() => {
          if (item.state == '待产池') {
            navigate('ChildOneMain', {
              url: Url.url + Url.Produce + Url.Fid + '/GetWaitingPoolComp/' + item.projectId,
              title: '待产池',
              sonTitle: '构件信息'
            })
          } /* else {
          
        } */
        }}>
        <ListItem
          title={item.state + ':  ' + item.compNum + '件 (' + item.compVolume + 'm³)'}
          titleStyle={styles.text}
          //contentContainerStyle={styles.content}
          containerStyle={styles.container}
          bottomDivider
          leftElement={<Badge badgeStyle={{ backgroundColor: item.color }} />}
          chevron
        />
      </TouchableOpacity>
    )
  }

  SetLoading = () => {
    console.log("🚀 ~ file: FlatListCount.js:89 ~ FlatListCount ~ SetLoading:")
    this.setState({
      Loading: true
    })
  }

  _Reflash = (propsData) => {
    console.log("🚀 ~ file: FlatListCount.js:89 ~ FlatListCount ~ _Reflash ~ propsData:", propsData)
    if (typeof propsData == "undefined") {
      return
    }
    if (typeof propsData[0] == "undefined") {
      return
    }
    if (!(propsData instanceof Array)) {
      return
    }
    this.setState({
      Loading: false
    })
    let newdata = [];
    const length = propsData.length
    InNum = 0, InVolume = 0;
    try {
      // for (let i = 0; i < propsData.length - 2; i++) {
      //   let item = propsData[i];
      //   propsData[i].color = color[i];
      //   newdata.push(item)
      //   // newdata[i] = item;
      //   InNum += item.compNum;
      //   InVolume += item.compVolume;
      // };
      // if (length == 13) {
      //   newdata.splice(3, 0, propsData[12])
      //   newdata[3].color = '#d35400'
      //   InNum += propsData[12].compNum;
      //   InVolume += propsData[12].compVolume;
      //   OutNum = propsData[length - 2].compNum - InNum;
      //   OutVolume = propsData[length - 2].compVolume - InVolume;
      //   console.log(newdata);
      //   this.setState({
      //     //propsData来自按钮选择参数传值
      //     testData: propsData,
      //     allcount: propsData[length - 2],
      //     newdata: newdata,
      //     Loading: false
      //   });
      // } else {
      //   OutNum = propsData[length - 1].compNum - InNum;
      //   OutVolume = propsData[length - 1].compVolume - InVolume;
      //   console.log(newdata);
      //   this.setState({
      //     //propsData来自按钮选择参数传值
      //     testData: propsData,
      //     allcount: propsData[length - 1],
      //     newdata: newdata,
      //     Loading: false
      //   });
      // }

    } catch (error) {
      console.log("🚀 ~ file: FlatListCount.js:147 ~ FlatListCount ~ error:", error)
    }

  }

  render() {
    const { newdata, allcount, Loading } = this.state
    //console.log(allcount)
    if (Loading === true) {
      return (
        <View style={{
          height: 400,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#F5FCFF',
        }}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
    } else {
      return (
        <View style={{ flex: 1, width: width * 0.94, marginLeft: width * 0.03, borderRadius: 10 }}>
          <ListItem
            title={'生产总数：' + allcount.compNum + '件' + '(' + allcount.compVolume + 'm³)'}
            titleStyle={[styles.text, { fontSize: RFT * 4, textAlignVertical: 'center' }]}
            //contentContainerStyle={styles.content}
            containerStyle={styles.container}
            bottomDivider
          // chevron
          />

          {/* {
            newdata.map((item, index) => {
              return (
                <TouchableOpacity
                  onPress={() => {
                    //   if (item.state == '待产池') {
                    //    this.props.navigation.navigate('ChildOneMain', {
                    //      url: Url.url + Url.Produce + Url.Fid + '/GetWaitingPoolComp/' + item.projectId,
                    //      title: '待产池',
                    //      sonTitle: '构件信息'
                    //    })
                    //  } else { 
                    this.props.navigation.navigate('ChildOneMain', {
                      url: Url.url + Url.Produce + Url.Fid + '/GetProjectProducedComponents/' + item.projectId + '/state/' + item.state,
                      title: item.state,
                      sonTitle: '构件信息'
                    })
                    //}
                  }}>
                  <ListItem
                    title={item.state + ':  ' + item.compNum + '件 (' + item.compVolume + 'm³)'}
                    titleStyle={styles.text}
                    //contentContainerStyle={styles.content}
                    containerStyle={styles.container}
                    bottomDivider
                    leftElement={<Badge badgeStyle={{ backgroundColor: item.color }} />}
                    chevron
                  />
                </TouchableOpacity>
              )
            })
          } */}
          <ListItem
            title={'已下单数:' + InNum + '件' + '(' + InVolume.toFixed(3) + 'm³)'}
            titleStyle={styles.text}
            //contentContainerStyle={styles.content}
            containerStyle={styles.container}
            bottomDivider
            leftElement={<Badge badgeStyle={{ backgroundColor: '#e74c3c' }} />}
          // chevron
          />
          <ListItem
            title={'未下单数:' + OutNum + '件' + '(' + OutVolume.toFixed(3) + 'm³)'}
            titleStyle={styles.text}
            //contentContainerStyle={styles.content}
            containerStyle={styles.container}
            bottomDivider
            leftElement={<Badge badgeStyle={{ backgroundColor: '#27ae60' }} />}
          // chevron
          />

        </View >
      )
    }




  }
}

const styles = StyleSheet.create({
  content: {
    //height: 100,

  },
  container: {
    //height: 10,
  },
  textname: {
    fontSize: RFT * 3.5,
    textAlignVertical: "center",
    color: 'gray'
    //marginEnd: 12,
    //marginVertical: 10,
  },
  text: {
    fontSize: RFT * 3.5,
    textAlignVertical: "center",
    color: '#454545',
    //marginEnd: 12,
    //marginVertical: 10,
    //fontFamily:'STKaiti',
    //fontWeight: 'bold',
  },
})