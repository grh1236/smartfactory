import React from 'react';
import { View, ScrollView, TouchableOpacity, StyleSheet, Dimensions } from 'react-native';
import { Text, Input, Card, Image, Icon } from 'react-native-elements';
import ActionButton from 'react-native-action-button'
import CardView from '../CommonComponents/Card';
import Url from '../Url/Url';
import Url1 from '../Url/UrlTest'

const { height, width } = Dimensions.get('window');

const CardSource = [
  { img: require('../viewImg/KuCunGuanLi.png'), text: '库存管理', title: '库存管理', sonTitle: '库存管理', url: Url.url + 'Stock/' + Url.Fid + 'GetStockWarehou/10/', sonUrl: '' },
  { img: require('../viewImg/ChengPinRuKu.png'), text: '成品入库', title: '成品入库', sonTitle: '入库单管理', url: Url.url + 'Stock/' + Url.Fid + 'GetStockWarehou/10/', sonUrl: '' },
  { img: require('../viewImg/ChengPinChuKu.png'), text: '成品出库', title: '成品出库', sonTitle: '出库单管理', url: Url.url + 'Stock/' + Url.Fid + 'GetStockLoaoutWare/10/', sonUrl: '' },
  { img: require('../viewImg/YunDanChaXun.png'), text: '运单查询', title: '运单查询', sonTitle: '运单管理', url: Url.url + 'Stock/' + Url.Fid + 'GetStockLoaoutWare/10/', sonUrl: '' },
  { img: require('../viewImg/ChengPinTuiKu.png'), text: '成品退库', title: '成品退库', sonTitle: '退库单管理', url: Url.url + 'Stock/' + Url.Fid + 'GetStockRefund/10/', sonUrl: '' },
]

export default class DuiChangScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      text: null,
      title: {
        first: '已检',
        second: '合格',
        third: '不合格',
      },
      resDataAll: {
        firstNumber: 0,
        firstVolume: 0,
        secondNumber: 0,
        secondVolume: 0,
        thirdNumber: 0,
        thirdVolume: 0
      },
    }
    this.requestData = this.requestData.bind(this);
    this._refresh = this._refresh.bind(this);
  }

  requestData() {
    const url = Url.url + Url.factory + Url.Fid + 'GetStatisticStock';
    console.log(url)
    fetch(url).then(res => {
      return res.json()
    }).then(resData => {
      this.setState({
        resDataAll: resData,
      });
      const resDatanew = JSON.parse(JSON.stringify(this.state.resDataAll)
        .replace(/qualifiedNum/g, "firstNumber")
        .replace(/qualifiedVolume/g, "firstVolume")
        .replace(/returnedNum/g, "secondNumber")
        .replace(/returnedVolume/g, "secondVolume")
        .replace(/totalNum/g, "thirdNumber")
        .replace(/totalVolume/g, "thirdVolume"));
      this.setState({
        resDataAll: resDatanew
      })
      console.log(this.state.resDataAll)
    }).catch((error) => {
      console.log(error);
    });
  }

  _refresh = () => {
    console.log(this.state.resDataAll);
    this.requestData();
    this.refs.testDataReflash.cardReflash(this.state.resDataAll)
  }

 componentDidMount() {
    this.requestData();
  }

  render() {
    console.log(this.state.resDataAll);
    return (
      <ScrollView >
        <CardView
          ref='testDataReflash'
          title={this.state.title}
        />
        <View style={styles.ViewCard}>
          {
            CardSource.map((u, i) => {
              return (
                <View style={{ marginVertical: 20 }}>
                  <TouchableOpacity
                    onPress={() => {
                      this.props.navigation.navigate('ChildOne', {
                        title: u.title,
                        sonTitle: u.sonTitle,
                        url: u.url,
                        sonUrl: u.sonUrl
                      })
                      console.log(u.sonTitle)
                    }} >
                    <Image
                      source={u.img}
                      style={styles.img}
                    ></Image>
                  </TouchableOpacity>
                  <Text style={{ color: 'black', textAlign: 'center' }}> {u.text} </Text>
                </View>
              );
            })
          }

        </View>
        <View style={{ height: 80 }}>
          <ActionButton
            buttonColor='#00CCFF'
            renderIcon={() => {
              return (
                <Icon
                  name='refresh'
                  color='white' />
              )
            }}
            onPress={this._refresh} />
        </View>
      </ScrollView>


    );
  }
}

const styles = StyleSheet.create({
  ViewCard: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    marginTop: 20,
    justifyContent: 'flex-start',
    flex: 1
  },
  img: {
    height: width / 3,
    width: width / 3
  }
})