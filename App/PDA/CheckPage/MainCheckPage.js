import React from 'react'
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, Overlay, Header } from 'react-native-elements';
import Url from '../../Url/Url';
import { createMaterialTopTabNavigator, NavigationEvents } from 'react-navigation'
import { ToDay, YesterDay, BeforeYesterDay } from '../../CommonComponents/Data';
import { RFT, deviceWidth } from '../../Url/Pixal';
import ScrollTab from 'react-native-scroll-tab-page'
import { ScrollView } from 'react-native';
import Toast from 'react-native-easy-toast';
import ListItemScan from '../Componment/ListItemScan';
//import { styles } from '../Componment/PDAStyles';

import { DeviceEventEmitter, NativeModules } from 'react-native';
import { prototype } from 'react-native-sound';
import ScanButton from '../Componment/ScanButton';
import WebView from 'react-native-webview';
import TouchableCustom from '../Componment/TouchableCustom';

const { height, width } = Dimensions.get('window') //获取宽高

let isCheckSecondList = []; //是否展开二级


let pageNum = 1, pageNumMy = 1, page = '', mainNav = '', sontitle = '', text = '', pageType = '', scanQRid = false, isRFID = false
let listData = [], data_All = [], data_my = []

class BackIcon extends React.PureComponent {
  render() {
    return (
      <TouchableCustom
        onPress={this.props.onPress} >
        <Icon
          name='arrow-back'
          color='#419FFF' />
      </TouchableCustom>
    )
  }
}

//搜索栏搜索图标
const defaultSearchIcon = () => ({
  //type: 'antdesign',
  size: 24,
  name: 'search',
  color: '#999',
});

//搜索栏清理图标
const defaultClearIcon = () => ({
  //type: '',
  size: 20,
  name: 'close',
  color: '#999',
});

class MainCheckPageAll extends React.Component {
  constructor() {
    super();
    this.state = {
      search: '', //搜索关键字保存
      pageNum: '1', //页码
      pageNumMy: '1',
      tabIndex: '', //tab栏
      resData: {}, //接口数据保存
      listData: [], //列表数据
      data_All: [],
      data_my: [],
      isLoading: true,  //判断是否加载 加载页
      isRefreshing: false,  //是否刷新
      selectedIndex: 0,  //按钮组选择
      page: 0,
      pageLoading: true,
      tabs: [{ title: "全部" }, { title: "我的" }],
      isCheckSecondList: [], //二级菜单数组
      isBatch: false,
      focusIndex: 0,
      RFID: "",
      isCompModalOpen: false
    }
  }

  componentDidMount() {
    const { navigation } = this.props;
    text = navigation.getParam('text') || ''
    page = navigation.getParam('title') || ''
    pageType = navigation.getParam('pageType') || 'CheckPage'
    if (page.indexOf('批') != -1) {
      this.setState({ isBatch: true })
    }
    mainNav = navigation.getParam('main') || ''
    sontitle = navigation.getParam('sonTitle') || ''
    console.log("🚀 ~ file: MainCheckPage.js ~ line 72 ~ MainCheckPage ~ componentDidMount ~ mainNav", mainNav)
    this.requestData(page, "");

    this.iDataScan = DeviceEventEmitter.addListener('iDataScan', (Event) => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 115 ~ SteelCage ~ Event.ScanResult", Event.ScanResult)
      if (typeof Event.ScanResult != undefined) {
        let data = Event.ScanResult
        let arr = data.split("=");
        let id = ''
        id = arr[arr.length - 1]
        console.log("🚀 ~ file: concrete_pouring.js ~ line 118 ~ SteelCage ~ id", id)
        if (mainNav == 'SampleManage') {
          this.GetcomID(id)
        }

      }
    });
  }

  UpdateControl = () => {
    const { navigation } = this.props;
    text = navigation.getParam('text') || ''
    page = navigation.getParam('title') || ''
    if (page.indexOf('批') != -1) {
      this.setState({ isBatch: true })
    }

    this.requestData(page, "");
    this.forceUpdate()
    let QRid = ''
    // 扫码跳转回来获取参数
    const paramScan = this.props.navigation.dangerouslyGetParent().state.params
    if (typeof paramScan.QRid != 'undefined') {
      QRid = paramScan.QRid || ''
    }
    if (paramScan.type == 'sampleManage' && !scanQRid) {
      // 返回时不再跳转
      scanQRid = true
      this.props.navigation.dangerouslyGetParent().state.params.type == ''
      let qid = QRid.indexOf("000") == 0 ? QRid.replace(/\b(0+)/gi, "") : QRid
      this.GetcomID(qid)
    }

    mainNav = navigation.getParam('main') || ''
    sontitle = navigation.getParam('sonTitle') || ''
    console.log("🚀 ~ file: MainCheckPage.js ~ line 99 ~ MainCheckPage ~ componentDidMount ~ mainNav", mainNav)

  }

  componentWillUnmount() {
    pageNum = 1
    pageNumMy = 1
    listData = [], data_All = [], data_my = []
    scanQRid = false
    this.iDataScan.remove()
  }

  GetcomID = (id) => {
    let formData = new FormData();
    let data = {};
    //let type = this.state.isTitleOne ? '0' : this.state.isTitleTwo ? '1' : '0'
    data = {
      "action": "samplerectifyscan",
      "servicetype": "pda",
      "express": "4287BAEA",
      "ciphertext": "bbbaad34794eef7a726f6b3832a24924",
      "data": {
        "compId": id,
        "factoryId": Url.PDAFid,
        //"pageType": type
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: MainCheckPage.js ~ line 123 ~ MainCheckPage ~ data:", data)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      //console.log("🚀 ~ file: MainCheckPage.js ~ line 131 ~ MainCheckPage ~ res:", res.json())
      return res.json();
    }).then(resData => {
      if (resData.status == '113') {
        this.toast.show(resData.message);
      } else if (resData.status == '100') {
        let guid = resData.result.guid
        setTimeout(() => {
          this.props.navigation.navigate('SampleManage', {
            guid: guid,
            pageType: 'Scan',
            title: '抽检整改单',
            isCheckComID: true
          })
        }, 1000)
      } else {
        console.log('resData', resData)
        this.toast.show(resData.message)
        this.input1.focus()
        var GetError = true
        this.setState({
          GetError: true
        })
      }

    }).catch(err => {
      console.log("🚀 ~ file: MainCheckPage.js ~ line 156 ~ MainCheckPage ~ err", err)
    })
  }

  requestData = (page, strhazy) => {
    console.log("🚀 ~ file: MainCheckPage.js ~ line 79 ~ MainCheckPageAll ~ page", page)
    const { pageNum, tabIndex } = this.state
    const { navigation } = this.props;
    text = navigation.getParam('text') || ''
    let allormy = ''
    strhazy = strhazy.toString()
    if (strhazy != "") {
      allormy = 'all'
    } else {
      allormy = "0"
    }

    let formData = new FormData();
    console.log("🚀 ~ file: MainCheckPage.js:515 ~ MainCheckPageAll ~ mainNav", mainNav)

    let data = {};
    if (page.indexOf('批') != -1) {
      data = {
        "action": "getbatchconctretecastinglist",
        "servicetype": "batchproduction",
        "express": "68FB8B8F",
        "ciphertext": "4b4b28876f125405bc5520e80a668aae",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy,
          "_bizid": Url.PDAFid,
          "pageNum": pageNum.toString(),
          "username": Url.username
        },
      }
      if (mainNav == 'HideInspection') {
        data.action = 'getbatchcomplexhidechecklist'
      }
      if (mainNav == 'StrippingInspection') {
        data.action = 'getbatchdemoldlist'
      }
      if (mainNav == 'Wantconcrete') {
        data = {
          "action": "getcalllist",
          "servicetype": "concrete",
          "express": "C80790DE",
          "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
          "data": {
            "viewRowCt": "10",
            "keyStr": strhazy,
            "_bizid": Url.PDAFid,
            "pageNum": pageNum.toString(),
            "username": Url.username
          },
        }
      }
      if (mainNav == 'SteelCageStorage') {
        data = {
          "action": "getbatchcageinstoragelist",
          "servicetype": "batchsteelcage",
          "express": "C80790DE",
          "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
          "data": {
            "viewRowCt": "10",
            "keyStr": strhazy,
            "_bizid": Url.PDAFid,
            "pageNum": pageNum.toString(),
            "username": Url.username
          }
        }
      }
    } else if (mainNav == 'ConcretePouring') {
      data = {
        "action": "getconctretecastinglist",
        "servicetype": "pdaservice",
        "express": "68FB8B8F",
        "ciphertext": "4b4b28876f125405bc5520e80a668aae",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy,
          "_bizid": Url.PDAFid,
          "pageNum": pageNum.toString(),
          "username": Url.username
        },
      }
    } else if (mainNav == 'Wantconcrete') {
      data = {
        "action": "getcalllist",
        "servicetype": "concrete",
        "express": "C80790DE",
        "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy,
          "_bizid": Url.PDAFid,
          "pageNum": pageNum.toString(),
          "username": Url.username
        },
      }
    } else if (mainNav == 'ChangeLocation') {
      data = {
        "action": "getstoragelocationchangelist",
        "servicetype": "pdaservice",
        "express": "C80790DE",
        "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy,
          "_bizid": Url.PDAFid,
          "pageNum": pageNum.toString(),
          "username": Url.username
        },

      }
    } else if (mainNav == 'EquipmentRepair') {
      data = {
        "action": "getmainapplicationlist",
        "servicetype": "pdaservice",
        "express": "C80790DE",
        "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy,
          "_bizid": Url.PDAFid,
          "pageNum": pageNum.toString(),
          "username": Url.username
        },

      }
    } else if (mainNav == 'EquipmentService') {
      let myAction = 'getrepairlist'
      if ((page == '设备维修确认') && (pageType == 'Scan')) myAction = 'getrepairconfirmlist'
      else if ((page == '设备维修确认') && (pageType == 'CheckPage')) myAction = 'getrepairconfirmlistquery'
      data = {
        "action": myAction,
        "servicetype": "pdaservice",
        "express": "C80790DE",
        "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy,
          "_bizid": Url.PDAFid,
          "pageNum": pageNum.toString(),
          "username": Url.username
        },

      }
    } else if (mainNav == "PDASacnLoading") {
      data = {
        action: "getproductstockoutlist",
        servicetype: "pdaproductstockoutservice",
        express: "E2BCEB1E",
        ciphertext: "077d106a940be035e6d80eb61a1a01c3",
        data: {
          _bizid: Url.PDAFid,
          username: Url.username,
          keyStr: strhazy,
          pageNum: pageNum.toString(),
          viewRowCt: "10"
        }
      }
    } else if (mainNav == 'AdvanceStorage') {
      data = {
        "action": "getpreinstolist",
        "servicetype": "pdapreinsto",
        "express": "C80790DE",
        "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy,
          "_bizid": Url.PDAFid,
          "pageNum": pageNum.toString(),
          "username": Url.username
        },
      }
    } else if (mainNav == 'SpotCheck') {
      data = {
        "action": "getsamplinglist",
        "servicetype": "sampling",
        "express": "C80790DE",
        "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy,
          "_bizid": Url.PDAFid,
          "pageNum": pageNum.toString(),
          "username": Url.username
        },
      }
    } else if (mainNav == 'SafeManageNotice') {
      data = {
        "action": "safemanagenoticequery",
        "servicetype": "pda",
        "express": "FA51025B",
        "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy || "",
          "_bizid": Url.PDAFid,
          "pageNum": pageNum.toString(),
          "username": Url.PDAEmployeeName
        },
      }
    } else if (mainNav == 'SafeManageReceipt') {
      var action = pageType == 'CheckPage' ? 'safemanagerectifyquery' : 'safemanagerectifycheckquery'
      data = {
        "action": action,
        "servicetype": "pda",
        "express": "FA51025B",
        "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy,
          "_bizid": Url.PDAFid,
          "pageNum": pageNum.toString(),
          "username": Url.PDAEmployeeName
        },
      }
    } else if (mainNav == 'PostRiskInspection') {
      data = {
        "action": "postriskinspectionquery",
        "servicetype": "pda",
        "express": "FA51025B",
        "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy || "",
          "_bizid": Url.PDAFid,
          "pageNum": pageNum.toString(),
          "username": Url.PDAEmployeeName
        },
      }
    } else if (mainNav == 'SampleManage') {
      let type = 'samplemanagequery'
      if (sontitle == '质量抽检管理') {
        type = 'samplemanagequery'
      } else if (sontitle == '抽检整改单') {
        type = pageType == 'CheckPage' ? 'samplemanagerectifycheckquery' : 'samplemanagerectifyquery'
      } else if (sontitle == '整改复检') {
        type = pageType == 'CheckPage' ? 'samplemanagerecheckcheckquery' : 'samplemanagerecheckquery'
      }
      data = {
        "action": type,
        "servicetype": "pda",
        "express": "FA51025B",
        "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy || "",
          "_bizid": Url.PDAFid,
          "pageNum": pageNum.toString(),
          "username": Url.PDAEmployeeName
        },
      }
    } else if (mainNav == 'CompRepair') {
      console.log("🚀 ~ file: MainCheckPage.js ~ line 409 ~ MainCheckPageAll ~ mainNav", mainNav)
      data = {
        "action": "initcomprepairquerylist",
        "servicetype": "projectsystem",
        "express": "E2BCEB1E",
        "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
        "data": {
          "factoryId": Url.PDAFid,
          "projectId": "",
          "empId": Url.PDAEmployeeId,
          "isprj": 0,
          "keyWord": "",
          "type": "repair",
          "pageIndex": 1,
          "count": 10
        }
      }
      if (pageType == 'Scan') {
        data = {
          "action": "initcomprepairquerylist",
          "servicetype": "projectsystem",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "factoryId": Url.PDAFid,
            "projectId": "",
            "empId": Url.PDAEmployeeId,
            "isprj": 0,
            "keyWord": "",
            "type": "apply",
            "pageIndex": 1,
            "count": 10
          }
        }
      }
    } else if (mainNav == 'CompMaintain') {
      data = {
        "action": "initcompmaintainquerylist",
        "servicetype": "projectsystem",
        "express": "E2BCEB1E",
        "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
        "data": {
          "factoryId": Url.PDAFid,
          "projectId": "",
          "empId": Url.PDAEmployeeId,
          "keyWord": "",
          "type": "maintain",
          "isprj": "0",
          "pageIndex": 1,
          "count": 10
        }
      }
    } else if (mainNav == 'QualityScrap') {
      data = {
        "action": "qualityscrapquery",
        "servicetype": "pdamaterial",
        "express": "6DFD1C9B",
        "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy || "",
          "_bizid": Url.PDAFid,
          "pageNum": pageNum.toString(),
          "username": Url.PDAEmployeeName
        },
      }
    } else if (mainNav == 'Inveck') {
      data = {
        "action": "getstockinventorylist",
        "servicetype": "pdaservice",
        "express": "E2BCEB1E",
        "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy,
          "_bizid": Url.PDAFid,
          "pageNum": "1",
          "username": Url.PDAusername
        }
      }
    } else if (mainNav == 'HandlingToolBaskets') {
      data = {
        "action": "obtaihandlingtoollist",
        "servicetype": "pda",
        "express": "E2BCEB1E",
        "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy,
          "factoryId": Url.PDAFid,
          "pageNum": "1",
          "username": Url.PDAusername
        }
      }
    } else if (mainNav == 'WaterPoolPH') {
      data = {
        "action": "getwaterpoolphlist",
        "servicetype": "pda",
        "express": "E2BCEB1E",
        "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
        "data": {
          "count": "10",
          "keyWord": strhazy,
          "factoryId": Url.PDAFid,
          "pageIndex": "1",
          "empId": Url.PDAEmployeeId,
        }
      }
    } else if (mainNav == 'WaterPoolDetail') {
      data = {
        "action": "getwaterpooldetaillist",
        "servicetype": "pda",
        "express": "E2BCEB1E",
        "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
        "data": {
          "count": "10",
          "keyWord": strhazy,
          "factoryId": Url.PDAFid,
          "pageIndex": "1",
          "empId": Url.PDAEmployeeId,
        }
      }
    } else {
      data = {
        "action": "LookUp",
        "servicetype": "pda",
        "express": "FA51025B",
        "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
        "data": {
          "PDAuserid": Url.PDAEmployeeId,
          "subscript": page,
          "strhazy": strhazy || "",
          "PageNum": pageNum.toString(),
          "allormy": allormy,
          "factoryId": Url.PDAFid
        }
      }
    }

    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: MainCheckPage.js ~ line 318 ~ MainCheckPage ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.setState({
          isLoading: false,
        })
        console.log("🚀 ~ file: MainCheckPage.js ~ line 329 ~ MainCheckPage ~ resData", resData)
        listData = resData.result
        if (listData.all.length > 0) {
          data_All = listData.all
        } else {
          data_All = []
        }
        if (listData.all.length == 0) {
          data_All = listData.all
          this.toast.show('无数据')
        }
        this.setState({
          listData: listData,
          data_All: data_All,
          //data_my: data_my,
          resData: resData,
          pageLoading: false
        }, () => {
          this.forceUpdate()
        })
      } else {
        console.log("🚀 ~ file: MainCheckPage.js ~ line 311 ~ MainCheckPageAll ~ resData", resData)
        listData = resData.result
        if (listData.all.length == 0) {
          data_All = listData.all
          this.toast.show('无数据')
        }
        // if (typeof listData.my != 'null') {
        //   if (listData.my.length > 0) {
        //     data_my = listData.my
        //   }
        // } else {
        //   data_my = []
        // }
        this.setState({
          isLoading: false,
          pageLoading: false,
          listData: listData,
          data_All: data_All,
          //data_my: data_my,
          resData: resData,
        })
        this.toast.show(resData.message)
      }

    }).catch((error) => {
      console.log("🚀 ~ file: MainCheckPage.js ~ line 310 ~ MainCheckPageAll ~ error", error)
    });

  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: '全部'//navigation.getParam('title')
    }
  }

  //搜索栏搜索功能
  TextChange = (text) => {
    text = text.toString()
    console.log("🚀 ~ file: MainCheckPage.js ~ line 240 ~ MainCheckPageAll ~ text", text)
    this.setState({
      pageLoading: true,
      search: text
    })
    this.requestData(page, text, "all")
    //this.requestData(page, text, "my")
  }

  //顶部记录与筛选按钮
  Top = () => {

  }

  DeleteData = (action, servicetype, guid) => {
    this.toast.show('删除中', 0);

    let formData = new FormData();
    let data = {};
    data = {
      "action": action,
      "servicetype": servicetype,
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        "_rowguid": guid,
        "factoryId": Url.PDAFid
      }
    }

    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: main_compreceive.js ~ line 358 ~ miain_compreceive ~ DeleteData ~ formData", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('删除成功');
        this.refreshing()
      } else {
        this.toast.close()
        Alert.alert('删除失败', resData.message)
      }
    }).catch((error) => {
      console.log("🚀 ~ file: MainCheckPage.js:804 ~ MainCheckPageAll ~ DeleteData ~ error:", error)
      this.toast.show('删除成功');
    });
  }

  render() {
    const { isLoading, isRefreshing, pageLoading, search, resData, listData, data_All, tabs } = this.state

    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else if (mainNav == 'SampleManage' && pageType == 'Scan' && sontitle == '抽检整改单') {
      return (
        <View style={{ flex: 1 }}>
          <Toast ref={(ref) => { this.toast = ref; }} position="center"></Toast>
          <NavigationEvents
            onWillFocus={this.UpdateControl} />
          <View style={{ flex: 1 }}>
            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around' }}>
              <SearchBar
                platform='android'
                placeholder={'按编号进行搜索或扫码...'}
                placeholderTextColor='#999'
                //style={{ backgroundColor: 'red', borderColor: 'red', borderWidth: 10 }}
                //containerStyle={{ flex: 1 }}
                containerStyle={styles.searchContainerStyle}
                inputContainerStyle={{ backgroundColor: '#f9f9f9', width: deviceWidth * 0.85, marginLeft: deviceWidth * 0.03, borderRadius: 45 }}
                inputStyle={[{ color: '#333' }, deviceWidth <= 330 && { fontSize: 14 }]}
                searchIcon={defaultSearchIcon()}
                clearIcon={defaultClearIcon()}
                cancelIcon={defaultSearchIcon()}
                round={true}
                value={search}
                onChangeText={this.TextChange}
                input={(input) => { this.setState({ search: input }) }} />
              <View>
                <Icon onPress={() => {
                  // 返回跳转验证重置
                  scanQRid = false
                  this.props.navigation.navigate('QRCode', {
                    type: 'sampleManage',
                    page: 'MainCheckPage'
                  })
                }} type='material-community' name='qrcode-scan' color='#999' />
              </View>
            </View>
            <FlatList
              style={{ flex: 1, backgroundColor: '#f8f8f8' }}
              ref={(flatList) => this._flatList1 = flatList}
              //ItemSeparatorComponent={this._separator}
              renderItem={this._renderItem}
              onRefresh={this.refreshing}
              refreshing={false}
              showsVerticalScrollIndicator={false}
              //控制上拉加载，两个平台需要区分开
              onEndReachedThreshold={(Platform.OS === 'ios') ? -0.3 : 0.1}
              onEndReached={() => {
                if (data_All.length >= 5) {
                  this._onload("all")
                }
              }
              }
              numColumns={1}
              //ListFooterComponent={this._footer}
              //columnWrapperStyle={{borderWidth:2,borderColor:'black',paddingLeft:20}}
              //horizontal={true}
              data={data_All}
              scrollEnabled={true}
              extraData={this.state}
            />
          </View>

        </View>
      )
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Toast ref={(ref) => { this.toast = ref; }} position="center"></Toast>
          <NavigationEvents
            onWillFocus={this.UpdateControl} />
          <View style={{ flex: 1 }}>
            <SearchBar
              platform='android'
              placeholder={mainNav == 'WaterPoolDetail' || mainNav == 'WaterPoolPH' ? '按水养池进行搜索...' : '按编号进行搜索...'}
              placeholderTextColor='#999'

              // style={{ backgroundColor: 'red', borderColor: 'red', borderWidth: 10 }}
              //containerStyle={{ flex: 1 }}
              containerStyle={styles.searchContainerStyle}
              inputContainerStyle={{ backgroundColor: '#f9f9f9', width: deviceWidth * 0.94, marginLeft: deviceWidth * 0.03, borderRadius: 45 }}
              inputStyle={[{ color: '#333' }, deviceWidth <= 330 && { fontSize: 14 }]}
              searchIcon={defaultSearchIcon()}
              clearIcon={defaultClearIcon()}
              cancelIcon={defaultSearchIcon()}
              round={true}
              value={search}
              onChangeText={this.TextChange}
              input={(input) => { this.setState({ search: input }) }} />
            <FlatList
              style={{ flex: 1, backgroundColor: '#f8f8f8' }}
              ref={(flatList) => this._flatList1 = flatList}
              //ItemSeparatorComponent={this._separator}
              renderItem={this._renderItem}
              onRefresh={this.refreshing}
              refreshing={false}
              showsVerticalScrollIndicator={false}
              //控制上拉加载，两个平台需要区分开
              onEndReachedThreshold={(Platform.OS === 'ios') ? -0.3 : 0.1}
              onEndReached={() => {
                if (data_All.length >= 5) {
                  // let datalengthunder10 = data_All.length / 10
                  // console.log("🚀 ~ file: MainCheckPage.js:823 ~ MainCheckPageAll ~ render ~ pageNum:", pageNum)

                  // console.log("🚀 ~ file: MainCheckPage.js:821 ~ MainCheckPageAll ~ render ~ datalengthunder10:", datalengthunder10)
                  // if (datalengthunder10 >= pageNum)
                  this._onload("all")

                }
              }
              }
              numColumns={1}
              //ListFooterComponent={this._footer}
              //columnWrapperStyle={{borderWidth:2,borderColor:'black',paddingLeft:20}}
              //horizontal={true}
              data={data_All}
              scrollEnabled={true}
              extraData={this.state}
            />
          </View>
          <Overlay
            fullScreen={true}
            animationType='fade'
            isVisible={this.state.isCompModalOpen}
            onRequestClose={() => {
              this.setState({ isCompModalOpen: !this.state.isCompModalOpen });
            }}>
            <Header
              containerStyle={{ height: 45, paddingTop: 0, top: -5 }}
              centerComponent={{ text: '构件详情', style: { color: 'gray', fontSize: 20 } }}
              rightComponent={<BackIcon
                onPress={() => {
                  this.setState({
                    isCompModalOpen: !this.state.isCompModalOpen
                  });
                }} />}
              backgroundColor='white'
            />
            <View style={{ flex: 1 }}>
              <WebView source={{ uri: "http://auth.smart.pkpm.cn/QRPageFrame.aspx?sitecode=" + Url.ID + "&qrbiz=/PCIS/Production/ShowCompInfo.aspx?compid=" + this.state.RFID }}></WebView>
            </View>
          </Overlay>
        </View>
      )
    }
  }

  //接口返回的对象数组，长列表对数据遍历
  _renderItem = ({ item, index }) => {
    let mainNavList2 = ['Wantconcrete', 'ChangeLocation', 'EquipmentRepair', 'EquipmentService', 'PDASacnLoading', 'AdvanceStorage']
    if (mainNav == 'ConcretePouring') {
      return (
        <Card containerStyle={styles.checkCardContainerStyle}>
          <TouchableOpacity
            onPress={() => {
              let _Rowguid = item._Rowguid.toString()
              if (page.indexOf('批') != -1) {
                this.props.navigation.navigate(mainNav, {
                  guid: _Rowguid,
                  isBatch: "true",
                  pageType: 'CheckPage',
                  title: sontitle,
                })
              } else {
                this.props.navigation.navigate(mainNav, {
                  guid: _Rowguid,
                  pageType: 'CheckPage',
                  title: sontitle,
                })
              }
            }} >
            <View style={{ flexDirection: 'row', }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.FormCode}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditName}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text]} numberOfLines={1}> {item.ConcreteGrade}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.ProjectName || ""}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textthi]} numberOfLines={1}> {item.ConcreteType}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditDate}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    } else if (mainNavList2.indexOf(mainNav) != -1) {
      return (
        <Card containerStyle={styles.checkCardContainerStyle}>
          <TouchableOpacity
            onPress={() => {
              let guid = item._Rowguid.toString()
              this.props.navigation.navigate(mainNav, {
                guid: guid,
                pageType: mainNav == 'EquipmentService' ? pageType : 'CheckPage',
                title: sontitle,
              })
            }} >
            <View style={{ flexDirection: 'row', }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.FormCode}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditName}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text]} numberOfLines={1}> {item.ConcreteGrade}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textthi]} numberOfLines={1}> {item.ConcreteType}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditDate}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    } else if (mainNav == 'ProductionProcess') {
      return (
        <Card containerStyle={styles.checkCardContainerStyle}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 1, }}>
              <TouchableOpacity onPress={() => {
                if (this.state.isCheckSecondList.indexOf(item.compCode) == -1) {
                  isCheckSecondList.push(item.compCode)
                } else {
                  //查出取消勾选项的角标
                  let num = isCheckSecondList.indexOf(item.compCode)
                  //删掉！
                  isCheckSecondList.splice(num, 1)
                }
                this.setState({
                  isCheckSecondList: isCheckSecondList
                })
              }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={{ fontSize: 15, flex: 5 }} numberOfLines={1}> {'产品编号    ' + item.compCode}</Text>
                  <Icon containerStyle={{ flex: 1 }} size={18} color='#666' type='antdesign' name='down-square-o' />
                </View>
                {
                  this.state.isCheckSecondList.indexOf(item.compCode) != -1 ?
                    <View style={{ marginTop: 10, height: 1, backgroundColor: '#ccc' }}></View> : <View></View>
                }

              </TouchableOpacity>
              {
                this.state.isCheckSecondList.indexOf(item.compCode) != -1 ?
                  item.component.map((compitem) => {
                    return (
                      <TouchableOpacity
                        onPress={() => {
                          let guid = item.guid.toString()
                          let subguid = compitem.subguid.toString()
                          this.props.navigation.navigate(mainNav, {
                            guid: guid,
                            subguid: subguid,
                            pageType: 'CheckPage',
                            title: sontitle,
                          })
                        }} >
                        <View>
                          <View style={{ marginBottom: 10, }}></View>
                          <View style={{ flexDirection: 'row' }}>
                            <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {compitem.proced}</Text>
                            <Text style={[styles.text, styles.textright]} numberOfLines={1}> {compitem.compCode}</Text>
                          </View>
                          <View style={{ flexDirection: 'row' }}>
                            <Text style={[styles.text]} numberOfLines={1}> {compitem.compTypeName}</Text>
                            <Text style={[styles.text, styles.textright]} numberOfLines={1}> {compitem.PDAuser}</Text>
                          </View>
                          <View style={{ flexDirection: 'row' }}>
                            <Text style={[styles.text, styles.textthi]} numberOfLines={1}> {compitem.designType}</Text>
                            <Text style={[styles.text, styles.textright]} numberOfLines={1}> {compitem.time}</Text>
                          </View>
                          <View style={{ marginTop: 10, height: 1, backgroundColor: '#ccc' }}></View>
                        </View>
                      </TouchableOpacity>
                    )
                  })
                  : <View></View>
              }

            </View>
          </View>
        </Card>
      )
    } else if (mainNav == 'SpotCheck') {
      return (
        <Card containerStyle={styles.checkCardContainerStyle}>
          <TouchableOpacity
            onPress={() => {
              let guid = item._Rowguid.toString()
              this.props.navigation.navigate(mainNav, {
                guid: guid,
                pageType: 'CheckPage',
                title: sontitle,
              })
            }} >
            <View style={{ flexDirection: 'row' }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.FormCode}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditName}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text]} numberOfLines={1}> {item.ConcreteGrade}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.ProjectName}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textthi]} numberOfLines={1}> {item.ConcreteType}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditDate}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    } else if (page.indexOf('批') != -1) {
      if (mainNav == 'HideInspection' || mainNav == 'StrippingInspection') {
        return (
          <Card containerStyle={styles.checkCardContainerStyle}>
            <TouchableOpacity
              onPress={() => {
                let guid = ''
                if (page.indexOf('批') != -1) {
                  guid = item._Rowguid.toString()
                  this.props.navigation.navigate(mainNav, {
                    guid: guid,
                    isBatch: "true",
                    pageType: 'CheckPage',
                    title: sontitle,
                  })
                } else {
                  guid = item.guid.toString()
                  this.props.navigation.navigate(mainNav, {
                    guid: guid,
                    pageType: 'CheckPage',
                    title: sontitle,
                  })
                }
              }} >
              <View style={{ flexDirection: 'row' }}>
                <View style={[styles.SN_View]}>
                  <View style={[styles.SN_Text_View]}>
                    <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.FormCode}</Text>
                    <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditName}</Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={[styles.text]} numberOfLines={1}> {item.ConcreteGrade}</Text>
                    {
                      item.projectName == undefined ? <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.ProjectName.toString() || ""}</Text> :
                        <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.projectName.toString() || ""}</Text>
                    }

                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={[styles.text, styles.textthi]} numberOfLines={1}> {item.ConcreteType}</Text>
                    <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditDate}</Text>
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          </Card>
        )
      }
      if (mainNav == 'SteelCageStorage') {
        return (
          <Card containerStyle={styles.checkCardContainerStyle}>
            <TouchableOpacity
              onPress={() => {
                let guid = ''
                if (page.indexOf('批') != -1) {
                  guid = item._Rowguid.toString()
                  this.props.navigation.navigate(mainNav, {
                    guid: guid,
                    isBatch: "true",
                    pageType: 'CheckPage',
                    title: sontitle,
                  })
                } else {
                  guid = item.guid.toString()
                  this.props.navigation.navigate(mainNav, {
                    guid: guid,
                    pageType: 'CheckPage',
                    title: sontitle,
                  })
                }
              }} >
              <View style={{ flexDirection: 'row' }}>
                <View style={[styles.SN_View]}>
                  <View style={[styles.SN_Text_View]}>
                    <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.FormCode}</Text>
                    <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditName}</Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={[styles.text]} numberOfLines={1}> {item.ConcreteGrade}</Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={[styles.text, styles.textthi]} numberOfLines={1}> {item.ConcreteType}</Text>
                    <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditDate}</Text>
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          </Card>
        )

      }
    } else if (mainNav == 'SafeManageNotice') {
      return (
        <Card containerStyle={styles.checkCardContainerStyle}>
          <TouchableOpacity
            onPress={() => {
              let guid = item._Rowguid.toString()
              this.props.navigation.navigate(mainNav, {
                guid: guid,
                pageType: 'CheckPage',
                title: sontitle,
                //data: item
              })
            }} >
            <View style={{ flexDirection: 'row' }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                <View style={{ flexDirection: 'row' }}>
                  {/*<Text style={[styles.text, styles.textfir]} style={{ flex: 1, fontSize: 18 }} numberOfLines={1}> {'整改单号：'}</Text>*/}
                  {/*<Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.rectifyNoticeInfo.toString() || ""}</Text>*/}

                  <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.FormCode.toString() || ""}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditName.toString() || ""}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  {/*<Text style={[styles.text]} numberOfLines={1}> {'班组：'}</Text>*/}
                  {/*<Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.team.toString() || ""}</Text>*/}

                  <Text style={[styles.text]} numberOfLines={1}> {item.ConcreteGrade.toString() || ""}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditDate.toString() || ""}</Text>
                </View>
                {/*<View style={{ flexDirection: 'row' }}>*/}
                {/*    <Text style={[styles.text]} numberOfLines={1}> {'签发日期：'}</Text>*/}
                {/*    <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.issueDate.toString() || ""}</Text>*/}
                {/*</View>*/}
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    } else if (mainNav == 'SafeManageReceipt') {
      return (
        <Card containerStyle={styles.checkCardContainerStyle}>
          <TouchableOpacity
            onPress={() => {
              let guid = item._Rowguid.toString()
              const { navigation } = this.props;
              let isAppPhotoSetting = navigation.getParam('isAppPhotoSetting')
              let isAppDateSetting = navigation.getParam('isAppDateSetting')
              let isAppPhotoAlbumSetting = navigation.getParam('isAppPhotoAlbumSetting')
              this.props.navigation.navigate(mainNav, {
                guid: guid,
                pageType: pageType,
                title: sontitle,
                isAppPhotoSetting: isAppPhotoSetting,
                isAppDateSetting: isAppDateSetting,
                isAppPhotoAlbumSetting: isAppPhotoAlbumSetting
                //data: item
              })
            }} >
            <View style={{ flexDirection: 'row' }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                <View style={{ flexDirection: 'row' }}>
                  {/*<Text style={[styles.text, styles.textfir]} style={{ flex: 1, fontSize: 18 }} numberOfLines={1}> {'整改单号：'}</Text>*/}
                  {/*<Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.rectifyNoticeInfo.toString() || ""}</Text>*/}

                  <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.FormCode.toString() || ""}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditName.toString() || ""}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  {/*<Text style={[styles.text]} numberOfLines={1}> {'班组：'}</Text>*/}
                  {/*<Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.team.toString() || ""}</Text>*/}

                  <Text style={[styles.text]} numberOfLines={1}> {item.ConcreteGrade.toString() || ""}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditDate.toString() || ""}</Text>
                </View>
                {/*<View style={{ flexDirection: 'row' }}>*/}
                {/*    <Text style={[styles.text]} numberOfLines={1}> {'签发日期：'}</Text>*/}
                {/*    <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.issueDate.toString() || ""}</Text>*/}
                {/*</View>*/}
                {
                  pageType != 'CheckPage' ? <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={[styles.text]} numberOfLines={1}> {'操作：'}</Text>
                    <View>
                      <View style={[styles.text, styles.textright]} numberOfLines={1}>
                        <Button titleStyle={{ fontSize: 14 }}
                          title='整改'
                          type='solid'
                          onPress={() => {
                            let guid = item._Rowguid.toString()
                            const { navigation } = this.props;
                            let isAppPhotoSetting = navigation.getParam('isAppPhotoSetting')
                            let isAppDateSetting = navigation.getParam('isAppDateSetting')
                            let isAppPhotoAlbumSetting = navigation.getParam('isAppPhotoAlbumSetting')
                            this.props.navigation.navigate(mainNav, {
                              guid: guid,
                              pageType: pageType,
                              title: sontitle,
                              isAppPhotoSetting: isAppPhotoSetting,
                              isAppDateSetting: isAppDateSetting,
                              isAppPhotoAlbumSetting: isAppPhotoAlbumSetting
                              //data: item
                            })
                          }}
                          buttonStyle={{ paddingVertical: 6, backgroundColor: '#4D8EF5', marginVertical: 0 }}
                        />
                      </View>
                    </View>
                  </View> : <View />
                }
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    } else if (mainNav == 'PostRiskInspection') {
      return (
        <Card containerStyle={styles.checkCardContainerStyle}>
          <TouchableOpacity
            onPress={() => {
              let guid = item._Rowguid.toString()
              this.props.navigation.navigate(mainNav, {
                guid: guid,
                pageType: 'CheckPage',
                title: sontitle,
                //data: item
              })
            }} >
            <View style={{ flexDirection: 'row' }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                <View style={{ flexDirection: 'row' }}>
                  {/*<Text style={[styles.text, styles.textfir]} style={{ flex: 1, fontSize: 18 }} numberOfLines={1}> {'表单编号：'}</Text>*/}
                  {/*<Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.formNo.toString() || ""}</Text>*/}

                  <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.FormCode.toString() || ""}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditName.toString() || ""}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  {/*<Text style={[styles.text]} numberOfLines={1}> {'班组：'}</Text>*/}
                  {/*<Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.team.toString() || ""}</Text>*/}

                  <Text style={[styles.text]} numberOfLines={1}> {item.ConcreteGrade.toString() || ""}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.ConcreteType.toString() || ""}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  {/*<Text style={[styles.text]} numberOfLines={1}> {'检查时间：'}</Text>*/}
                  {/*<Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.checkDate.toString() || ""}</Text>*/}

                  <Text style={[styles.text]} numberOfLines={1}> {item.ProjectName.toString() || ""}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditDate.toString() || ""}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    } else if (mainNav == 'SampleManage') {
      return (
        <Card containerStyle={styles.checkCardContainerStyle}>
          <TouchableOpacity
            onPress={() => {
              let guid = item._Rowguid.toString()
              const { navigation } = this.props;
              let isAppPhotoSetting = navigation.getParam('isAppPhotoSetting')
              let isAppDateSetting = navigation.getParam('isAppDateSetting')
              let isAppPhotoAlbumSetting = navigation.getParam('isAppPhotoAlbumSetting')
              this.props.navigation.navigate(mainNav, {
                guid: guid,
                pageType: pageType,
                title: sontitle,
                isAppPhotoSetting: isAppPhotoSetting,
                isAppDateSetting: isAppDateSetting,
                isAppPhotoAlbumSetting: isAppPhotoAlbumSetting,
                //data: item
              })
            }} >
            <View style={{ flexDirection: 'row' }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.FormCode.toString() || ""}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditName.toString() || ""}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text]} numberOfLines={1}> {item.ConcreteGrade.toString() || ""}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.ConcreteType.toString() || ""}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text]} numberOfLines={1}> {item.ProjectName.toString() || ""}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditDate.toString() || ""}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    } else if (mainNav == 'CompRepair') {
      return (
        <Card containerStyle={styles.checkCardContainerStyle}>
          <TouchableOpacity
            onPress={() => {
              let guid = item._Rowguid.toString()
              this.props.navigation.navigate(mainNav, {
                guid: guid,
                pageType: pageType,
                title: sontitle,
              })
            }} >
            <View style={{ flexDirection: 'row', }}><View style={[styles.SN_View]}>
              <View style={[styles.SN_Text_View]}>
                <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
              </View>
            </View>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.compCode}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.repairState}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text]} numberOfLines={1}> {item.compTypeName}</Text>
                  {
                    item.projectName == undefined ? <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.ProjectName.toString() || ""}</Text> :
                      <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.projectName.toString() || ""}</Text>
                  }

                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textthi]} numberOfLines={1}> {item.formCode}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.applyDate}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    } else if (mainNav == 'CompMaintain') {
      return (
        <Card containerStyle={styles.checkCardContainerStyle}>
          <TouchableOpacity
            onPress={() => {
              let guid = item._Rowguid.toString()
              this.props.navigation.navigate(mainNav, {
                guid: guid,
                pageType: pageType,
                title: sontitle,
                projectId: item.projectId
              })
            }} >
            <View style={{ flexDirection: 'row', }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.compCode}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.maintainState}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text]} numberOfLines={1}> {item.compTypeName}</Text>
                  {
                    item.projectName == undefined ? <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.ProjectName || ""}</Text> :
                      <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.projectName || ""}</Text>
                  }

                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textthi]} numberOfLines={1}> {item.formCode}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.applyDate}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    } else if (mainNav == 'QualityScrap') {
      return (
        <Card containerStyle={styles.checkCardContainerStyle}>
          <TouchableOpacity
            onPress={() => {
              let guid = item._Rowguid.toString()
              this.props.navigation.navigate(mainNav, {
                guid: guid,
                pageType: pageType,
                title: sontitle,
                //data: item
              })
            }} >
            <View style={{ flexDirection: 'row' }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.FormCode.toString() || ""}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditName.toString() || ""}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text]} numberOfLines={1}> {item.ConcreteType.toString() || ""}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.ProjectName.toString() || ""}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text]} numberOfLines={1}> {item.ConcreteGrade.toString() || ""}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditDate.toString() || ""}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    } else if (mainNav == 'Inveck') {
      return (
        <Card containerStyle={styles.checkCardContainerStyle}>
          <TouchableOpacity
            onPress={() => {
              let _Rowguid = item._Rowguid.toString()
              {
                this.props.navigation.navigate(mainNav, {
                  guid: _Rowguid,
                  pageType: 'CheckPage',
                  title: sontitle,
                })
              }
            }} >
            <View style={{ flexDirection: 'row' }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.FormCode}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditName}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text]} numberOfLines={1}> {item.ConcreteGrade}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.ProjectName || ""}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textthi]} numberOfLines={1}> {item.ConcreteType}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditDate}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    } else if (mainNav == 'HandlingToolBaskets') {
      return (
        <Card containerStyle={styles.checkCardContainerStyle}>
          <TouchableOpacity
            onPress={() => {
              let guid = item._Rowguid.toString()
              this.props.navigation.navigate(mainNav, {
                guid: guid,
                pageType: pageType,
                title: sontitle,
              })
            }} >
            <View style={{ flexDirection: 'row', }}><View style={[styles.SN_View]}>
              <View style={[styles.SN_Text_View]}>
                <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
              </View>
            </View>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.FormCode}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.PRacking}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text]} numberOfLines={1}> {item.comptype}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.projectname || ""}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textthi]} numberOfLines={1}> {item.summain + '件'}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.editdate}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    } else if (mainNav == 'WaterPoolPH') {
      return (
        <Card containerStyle={styles.checkCardContainerStyle}>
          <TouchableOpacity
            onPress={() => {
              let guid = item._rowguid.toString()
              this.props.navigation.navigate(mainNav, {
                guid: guid,
                pageType: pageType,
                title: sontitle,
              })
            }}
            onLongPress={() => {
              Alert.alert(
                '提示信息',
                '是否删除水养池信息',
                [{
                  text: '取消',
                  style: 'cancel'
                }, {
                  text: '删除',
                  onPress: () => {
                    this.DeleteData("deletewaterpoolph", "pda", item._rowguid.toString())
                  }
                }])
            }}  >
            <View style={{ flexDirection: 'row', }}><View style={[styles.SN_View]}>
              <View style={[styles.SN_Text_View]}>
                <Text allowFontScaling={false} style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
              </View>
            </View>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text allowFontScaling={false} style={[styles.text, styles.textfir]} numberOfLines={1}> {item.poolname}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text allowFontScaling={false} style={[styles.text]} numberOfLines={1}> {'温度：' + item.temperature}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text allowFontScaling={false} style={[styles.text, styles.textthi]} numberOfLines={1}> {'PH值：' + item.phval}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text allowFontScaling={false} style={[styles.text]} numberOfLines={1}> {item.recorddate}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    } else if (mainNav == 'WaterPoolDetail') {
      return (
        <Card containerStyle={styles.checkCardContainerStyle}>
          <TouchableOpacity
            onPress={() => {
              let guid = item._rowguid.toString()
              let maintainstatus = item.maintainstatus.toString()
              this.props.navigation.navigate(mainNav, {
                guid: guid,
                pageType: pageType,
                title: sontitle,
                maintainstatus: maintainstatus
              })
            }}
            onLongPress={() => {
              Alert.alert(
                '提示信息',
                '是否删除水养池信息',
                [{
                  text: '取消',
                  style: 'cancel'
                }, {
                  text: '删除',
                  onPress: () => {
                    this.DeleteData("deletewaterpooldetail", "pda", item._rowguid.toString())
                  }
                }])
            }} >
            <View style={{ flexDirection: 'row', }}><View style={[styles.SN_View]}>
              <View style={[styles.SN_Text_View]}>
                <Text allowFontScaling={false} style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
              </View>
            </View>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text allowFontScaling={false} style={[styles.text, styles.textfir]} numberOfLines={1}> {item.poolname}</Text>
                  {
                    item.maintainstatus ?
                      <View style={[styles.badgeView, { backgroundColor: item.maintainstatus == '养护完成' ? 'rgba(98, 178, 250, 0.22)' : 'rgba(21, 214, 57, 0.22)' }]}>
                        <Badge
                          badgeStyle={{ backgroundColor: item.maintainstatus == '养护完成' ? '#64B1FC' : '#15D639' }}
                          containerStyle={{ marginRight: 7, top: 14 }}
                        />
                        <Text allowFontScaling={false} style={[styles.badgeTextView, { color: item.maintainstatus == '养护完成' ? '#64B1FC' : '#15D639', fontWeight: 'bold' }]}>{item.maintainstatus}</Text>
                      </View> :
                      <View></View>
                  }
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text allowFontScaling={false} style={[styles.text]} numberOfLines={1}> {item.datestr}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text allowFontScaling={false} style={[styles.text, styles.textthi]} numberOfLines={1}> {item.sumcount}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    } else {
      return (
        <Card containerStyle={styles.checkCardContainerStyle}>
          <TouchableOpacity
            onPress={() => {
              let guid = item.guid.toString()
              this.props.navigation.navigate(mainNav, {
                guid: guid,
                pageType: 'CheckPage',
                title: sontitle,
              })
            }} >
            <View style={{ flexDirection: 'row', }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.compCode}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.PDAuser}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text]} numberOfLines={1}> {item.compTypeName}</Text>
                  {
                    item.projectName == undefined ? <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.ProjectName.toString() || ""}</Text> :
                      <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.projectName.toString() || ""}</Text>
                  }

                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textthi]} numberOfLines={1}> {item.designType}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.time}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    }

  }

  refreshing = () => {
    this.toast.show('刷新中...', 10000)
    this.setState({
      pageLoading: true
    })
    const { pageNum } = this.state
    let formData = new FormData();
    let data = {};
    if (page.indexOf('批') != -1) {
      data = {
        "action": "getbatchconctretecastinglist",
        "servicetype": "batchproduction",
        "express": "68FB8B8F",
        "ciphertext": "4b4b28876f125405bc5520e80a668aae",
        "data": {
          "viewRowCt": "10",
          "keyStr": "",
          "_bizid": Url.PDAFid,
          "pageNum": "1",
          "username": Url.username
        },
      }
      if (mainNav == 'HideInspection') {
        data.action = 'getbatchcomplexhidechecklist'
      }
      if (mainNav == 'StrippingInspection') {
        data.action = 'getbatchdemoldlist'
      }
      if (mainNav == 'Wantconcrete') {
        data = {
          "action": "getcalllist",
          "servicetype": "concrete",
          "express": "C80790DE",
          "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
          "data": {
            "viewRowCt": "10",
            "keyStr": "",
            "_bizid": Url.PDAFid,
            "pageNum": "1",
            "username": Url.username
          },
        }
      }
      if (mainNav == 'SteelCageStorage') {
        data = {
          "action": "getbatchcageinstoragelist",
          "servicetype": "batchsteelcage",
          "express": "C80790DE",
          "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
          "data": {
            "viewRowCt": "10",
            "keyStr": "",
            "_bizid": Url.PDAFid,
            "pageNum": "1",
            "username": Url.username
          }
        }
      }
    } else if (mainNav == 'ConcretePouring') {
      data = {
        "action": "getconctretecastinglist",
        "servicetype": "pdaservice",
        "express": "68FB8B8F",
        "ciphertext": "4b4b28876f125405bc5520e80a668aae",
        "data": {
          "viewRowCt": "10",
          "keyStr": "",
          "_bizid": Url.PDAFid,
          "pageNum": '1',
          "username": Url.username
        },
      }
    } else if (mainNav == 'Wantconcrete') {
      data = {
        "action": "getcalllist",
        "servicetype": "concrete",
        "express": "C80790DE",
        "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
        "data": {
          "viewRowCt": "10",
          "keyStr": "",
          "_bizid": Url.PDAFid,
          "pageNum": '1',
          "username": Url.username
        },

      }
    } else if (mainNav == 'ChangeLocation') {
      data = {
        "action": "getstoragelocationchangelist",
        "servicetype": "pdaservice",
        "express": "C80790DE",
        "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
        "data": {
          "viewRowCt": "10",
          "keyStr": "",
          "_bizid": Url.PDAFid,
          "pageNum": '1',
          "username": Url.username
        },

      }
    } else if (mainNav == 'EquipmentRepair') {
      data = {
        "action": "getmainapplicationlist",
        "servicetype": "pdaservice",
        "express": "C80790DE",
        "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
        "data": {
          "viewRowCt": "10",
          "keyStr": "",
          "_bizid": Url.PDAFid,
          "pageNum": '1',
          "username": Url.username
        },

      }
    } else if (mainNav == 'EquipmentService') {
      let myAction = 'getrepairlist'
      if ((page == '设备维修确认') && (pageType == 'Scan')) myAction = 'getrepairconfirmlist'
      else if ((page == '设备维修确认') && (pageType == 'CheckPage')) myAction = 'getrepairconfirmlistquery'
      data = {
        "action": myAction,
        "servicetype": "pdaservice",
        "express": "C80790DE",
        "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
        "data": {
          "viewRowCt": "10",
          "keyStr": "",
          "_bizid": Url.PDAFid,
          "pageNum": '1',
          "username": Url.username
        },

      }
    } else if (mainNav == 'SafeManageNotice') {
      data = {
        "action": "safemanagenoticequery",
        "servicetype": "pda",
        "express": "FA51025B",
        "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
        "data": {
          "viewRowCt": "10",
          "keyStr": "",
          "_bizid": Url.PDAFid,
          "pageNum": '1',
          "username": Url.PDAEmployeeName
        },
      }
    } else if (mainNav == 'SafeManageReceipt') {
      var action = pageType == 'CheckPage' ? 'safemanagerectifyquery' : 'safemanagerectifycheckquery'
      data = {
        "action": action,
        "servicetype": "pda",
        "express": "FA51025B",
        "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
        "data": {
          "viewRowCt": "10",
          "keyStr": "",
          "_bizid": Url.PDAFid,
          "pageNum": '1',
          "username": Url.PDAEmployeeName
        },
      }
    } else if (mainNav == 'PostRiskInspection') {
      data = {
        "action": "postriskinspectionquery",
        "servicetype": "pda",
        "express": "FA51025B",
        "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
        "data": {
          "viewRowCt": "10",
          "keyStr": "",
          "_bizid": Url.PDAFid,
          "pageNum": '1',
          "username": Url.PDAEmployeeName
        },
      }
    } else if (mainNav == 'SampleManage') {
      let type = 'samplemanagequery'
      if (sontitle == '质量抽检管理') {
        type = 'samplemanagequery'
      } else if (sontitle == '抽检整改单') {
        type = pageType == 'CheckPage' ? 'samplemanagerectifycheckquery' : 'samplemanagerectifyquery'
      } else if (sontitle == '整改复检') {
        type = pageType == 'CheckPage' ? 'samplemanagerecheckcheckquery' : 'samplemanagerecheckquery'
      }
      data = {
        "action": type,
        "servicetype": "pda",
        "express": "FA51025B",
        "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
        "data": {
          "viewRowCt": "10",
          "keyStr": "",
          "_bizid": Url.PDAFid,
          "pageNum": '1',
          "username": Url.PDAEmployeeName
        },
      }
    } else if (mainNav == 'CompRepair') {
      console.log("🚀 ~ file: MainCheckPage.js ~ line 409 ~ MainCheckPageAll ~ mainNav", mainNav)
      data = {
        "action": "initcomprepairquerylist",
        "servicetype": "projectsystem",
        "express": "E2BCEB1E",
        "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
        "data": {
          "factoryId": Url.PDAFid,
          "projectId": "",
          "empId": Url.PDAEmployeeId,
          "isprj": 0,
          "keyWord": "",
          "type": "repair",
          "pageIndex": 1,
          "count": 10
        }
      }
      if (pageType == 'Scan') {
        data = {
          "action": "initcomprepairquerylist",
          "servicetype": "projectsystem",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "factoryId": Url.PDAFid,
            "projectId": "",
            "empId": Url.PDAEmployeeId,
            "isprj": 0,
            "keyWord": "",
            "type": "apply",
            "pageIndex": 1,
            "count": 10
          }
        }
      }
    } else if (mainNav == 'CompMaintain') {
      data = {
        "action": "initcompmaintainquerylist",
        "servicetype": "projectsystem",
        "express": "E2BCEB1E",
        "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
        "data": {
          "factoryId": Url.PDAFid,
          "projectId": "",
          "empId": Url.PDAEmployeeId,
          "keyWord": "",
          "type": "maintain",
          "isprj": "0",
          "pageIndex": 1,
          "count": 10
        }
      }
    } else if (mainNav == 'QualityScrap') {
      data = {
        "action": "qualityscrapquery",
        "servicetype": "pdamaterial",
        "express": "6DFD1C9B",
        "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
        "data": {
          "viewRowCt": "10",
          "keyStr": "",
          "_bizid": Url.PDAFid,
          "pageNum": '1',
          "username": Url.PDAEmployeeName
        },
      }
    } else if (mainNav == 'Inveck') {
      data = {
        "action": "getstockinventorylist",
        "servicetype": "pdaservice",
        "express": "E2BCEB1E",
        "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
        "data": {
          "viewRowCt": "10",
          "keyStr": "",
          "_bizid": Url.PDAFid,
          "pageNum": "1",
          "username": Url.PDAusername
        }
      }
    } else if (mainNav == 'HandlingToolBaskets') {
      data = {
        "action": "obtaihandlingtoollist",
        "servicetype": "pda",
        "express": "E2BCEB1E",
        "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy,
          "factoryId": Url.PDAFid,
          "pageNum": "1",
          "username": Url.PDAusername
        }
      }
    } else if (mainNav == 'WaterPoolPH') {
      data = {
        "action": "getwaterpoolphlist",
        "servicetype": "pda",
        "express": "E2BCEB1E",
        "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
        "data": {
          "count": "10",
          "keyWord": "",
          "factoryId": Url.PDAFid,
          "pageIndex": "1",
          "empId": Url.PDAEmployeeId,
        }
      }
    } else if (mainNav == 'WaterPoolDetail') {
      data = {
        "action": "getwaterpooldetaillist",
        "servicetype": "pda",
        "express": "E2BCEB1E",
        "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
        "data": {
          "count": "10",
          "keyWord": "",
          "factoryId": Url.PDAFid,
          "pageIndex": "1",
          "empId": Url.PDAEmployeeId,
        }
      }
    } else {
      data = {
        "action": "LookUp",
        "servicetype": "pda",
        "express": "FA51025B",
        "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
        "data": {
          "PDAuserid": Url.PDAEmployeeId,
          "subscript": page,
          "strhazy": "",
          "PageNum": pageNum,
          "allormy": "0",
          "factoryId": Url.PDAFid
        }
      }
    }

    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: MainCheckPage.js ~ line 925 ~ MainCheckPage ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: MainCheckPage.js ~ line 935 ~ MainCheckPage ~ resData", resData)
      if (resData.status == '100') {
        listData = resData.result
        data_All = listData.all
        data_my = listData.my
        this.setState({
          listData: listData,
          data_All: data_All,
          data_my: data_my,
          resData: resData,
          isLoading: false,
          pageLoading: false
        })
        this.toast.show('刷新完成', 1600)
      } else {
        listData = resData.result
        data_All = listData.all
        data_my = listData.my
        this.setState({
          listData: listData,
          data_All: data_All,
          data_my: data_my,
          resData: resData,
          isLoading: false,
          pageLoading: false
        })
        this.toast.show('刷新失败', 1600)
      }
    }).catch((error) => {
    });
  }

  _onload = (user) => {
    /* if (data_my.length < 10 && data_All.length != []) {
      return
    } 
    else  */{
      const { search } = this.state
      let strhazy = search
      this.toast.show('加载中...', 10000)
      pageNum += 1
      let formData = new FormData();
      let data = {};
      if (page.indexOf('批') != -1) {
        data = {
          "action": "getbatchconctretecastinglist",
          "servicetype": "batchproduction",
          "express": "68FB8B8F",
          "ciphertext": "4b4b28876f125405bc5520e80a668aae",
          "data": {
            "viewRowCt": "10",
            "keyStr": strhazy,
            "_bizid": Url.PDAFid,
            "pageNum": pageNum.toString(),
            "username": Url.username
          },
        }
        if (mainNav == 'HideInspection') {
          data.action = 'getbatchcomplexhidechecklist'
        }
        if (mainNav == 'StrippingInspection') {
          data.action = 'getbatchdemoldlist'
        }
        if (mainNav == 'Wantconcrete') {
          data = {
            "action": "getcalllist",
            "servicetype": "concrete",
            "express": "C80790DE",
            "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
            "data": {
              "viewRowCt": "10",
              "keyStr": strhazy,
              "_bizid": Url.PDAFid,
              "pageNum": pageNum.toString(),
              "username": Url.username
            },

          }
        }
        if (mainNav == 'SteelCageStorage') {
          data = {
            "action": "getbatchcageinstoragelist",
            "servicetype": "batchsteelcage",
            "express": "C80790DE",
            "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
            "data": {
              "viewRowCt": "10",
              "keyStr": strhazy,
              "_bizid": Url.PDAFid,
              "pageNum": pageNum.toString(),
              "username": Url.username
            }
          }
        }
      } else if (mainNav == 'ConcretePouring') {
        data = {
          "action": "getconctretecastinglist",
          "servicetype": "pdaservice",
          "express": "68FB8B8F",
          "ciphertext": "4b4b28876f125405bc5520e80a668aae",
          "data": {
            "viewRowCt": "10",
            "keyStr": strhazy,
            "_bizid": Url.PDAFid,
            "pageNum": pageNum.toString(),
            "username": Url.username
          },
        }
      } else if (mainNav == 'Wantconcrete') {
        data = {
          "action": "getcalllist",
          "servicetype": "concrete",
          "express": "C80790DE",
          "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
          "data": {
            "viewRowCt": "10",
            "keyStr": strhazy,
            "_bizid": Url.PDAFid,
            "pageNum": pageNum.toString(),
            "username": Url.username
          },

        }
      } else if (mainNav == 'ChangeLocation') {
        data = {
          "action": "getstoragelocationchangelist",
          "servicetype": "pdaservice",
          "express": "C80790DE",
          "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
          "data": {
            "viewRowCt": "10",
            "keyStr": strhazy,
            "_bizid": Url.PDAFid,
            "pageNum": pageNum.toString(),
            "username": Url.username
          },

        }
      } else if (mainNav == 'EquipmentRepair') {
        data = {
          "action": "getmainapplicationlist",
          "servicetype": "pdaservice",
          "express": "C80790DE",
          "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
          "data": {
            "viewRowCt": "10",
            "keyStr": strhazy,
            "_bizid": Url.PDAFid,
            "pageNum": pageNum.toString(),
            "username": Url.username
          },

        }
      } else if (mainNav == 'EquipmentService') {
        let myAction = 'getrepairlist'
        if ((page == '设备维修确认') && (pageType == 'Scan')) myAction = 'getrepairconfirmlist'
        else if ((page == '设备维修确认') && (pageType == 'CheckPage')) myAction = 'getrepairconfirmlistquery'

        data = {
          "action": myAction,
          "servicetype": "pdaservice",
          "express": "C80790DE",
          "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
          "data": {
            "viewRowCt": "10",
            "keyStr": strhazy,
            "_bizid": Url.PDAFid,
            "pageNum": pageNum.toString(),
            "username": Url.username
          },

        }
      } else if (mainNav == 'AdvanceStorage') {
        data = {
          "action": "getpreinstolist",
          "servicetype": "pdapreinsto",
          "express": "C80790DE",
          "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
          "data": {
            "viewRowCt": "10",
            "keyStr": strhazy,
            "_bizid": Url.PDAFid,
            "pageNum": pageNum.toString(),
            "username": Url.username
          },
        }
      } else if (mainNav == 'SpotCheck') {
        data = {
          "action": "getsamplinglist",
          "servicetype": "sampling",
          "express": "C80790DE",
          "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
          "data": {
            "viewRowCt": "10",
            "keyStr": strhazy,
            "_bizid": Url.PDAFid,
            "pageNum": pageNum.toString(),
            "username": Url.username
          },
        }
      } else if (mainNav == 'SafeManageNotice') {
        data = {
          "action": "safemanagenoticequery",
          "servicetype": "pda",
          "express": "FA51025B",
          "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
          "data": {
            "viewRowCt": "10",
            "keyStr": strhazy || "",
            "_bizid": Url.PDAFid,
            "pageNum": pageNum.toString(),
            "username": Url.PDAEmployeeName
          },
        }
      } else if (mainNav == 'SafeManageReceipt') {
        var action = pageType == 'CheckPage' ? 'safemanagerectifyquery' : 'safemanagerectifycheckquery'
        data = {
          "action": action,
          "servicetype": "pda",
          "express": "FA51025B",
          "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
          "data": {
            "viewRowCt": "10",
            "keyStr": strhazy || "",
            "_bizid": Url.PDAFid,
            "pageNum": pageNum.toString(),
            "username": Url.PDAEmployeeName
          },
        }
      } else if (mainNav == 'PostRiskInspection') {
        data = {
          "action": "postriskinspectionquery",
          "servicetype": "pda",
          "express": "FA51025B",
          "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
          "data": {
            "viewRowCt": "10",
            "keyStr": strhazy || "",
            "_bizid": Url.PDAFid,
            "pageNum": pageNum.toString(),
            "username": Url.PDAEmployeeName
          },
        }
      } else if (mainNav == 'SampleManage') {
        let type = 'samplemanagequery'
        if (sontitle == '质量抽检管理') {
          type = 'samplemanagequery'
        } else if (sontitle == '抽检整改单') {
          type = pageType == 'CheckPage' ? 'samplemanagerectifycheckquery' : 'samplemanagerectifyquery'
        } else if (sontitle == '整改复检') {
          type = pageType == 'CheckPage' ? 'samplemanagerecheckcheckquery' : 'samplemanagerecheckquery'
        }
        data = {
          "action": type,
          "servicetype": "pda",
          "express": "FA51025B",
          "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
          "data": {
            "viewRowCt": "10",
            "keyStr": strhazy || "",
            "_bizid": Url.PDAFid,
            "pageNum": pageNum.toString(),
            "username": Url.PDAEmployeeName
          },
        }
      } else if (mainNav == 'CompRepair') {
        console.log("🚀 ~ file: MainCheckPage.js ~ line 409 ~ MainCheckPageAll ~ mainNav", mainNav)
        data = {
          "action": "initcomprepairquerylist",
          "servicetype": "projectsystem",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "factoryId": Url.PDAFid,
            "projectId": "",
            "empId": Url.PDAEmployeeId,
            "isprj": 0,
            "keyWord": strhazy,
            "type": "repair",
            "pageIndex": pageNum,
            "count": 10
          }
        }
        if (pageType == 'Scan') {
          data = {
            "action": "initcomprepairquerylist",
            "servicetype": "projectsystem",
            "express": "E2BCEB1E",
            "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
            "data": {
              "factoryId": Url.PDAFid,
              "projectId": "",
              "empId": Url.PDAEmployeeId,
              "isprj": 0,
              "keyWord": strhazy,
              "type": "repair",
              "pageIndex": pageNum,
              "count": 10
            }
          }
        }
      } else if (mainNav == 'CompMaintain') {
        data = {
          "action": "initcompmaintainquerylist",
          "servicetype": "projectsystem",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "factoryId": Url.PDAFid,
            "projectId": "",
            "empId": Url.PDAEmployeeId,
            "keyWord": strhazy,
            "type": "maintain",
            "isprj": "0",
            "pageIndex": pageNum,
            "count": 10
          }
        }
      } else if (mainNav == 'QualityScrap') {
        data = {
          "action": "qualityscrapquery",
          "servicetype": "pdamaterial",
          "express": "6DFD1C9B",
          "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
          "data": {
            "viewRowCt": "10",
            "keyStr": strhazy || "",
            "_bizid": Url.PDAFid,
            "pageNum": pageNum.toString(),
            "username": Url.PDAEmployeeName
          },
        }
      } else if (mainNav == 'Inveck') {
        data = {
          "action": "getstockinventorylist",
          "servicetype": "pdaservice",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "viewRowCt": "10",
            "keyStr": strhazy,
            "_bizid": Url.PDAFid,
            "pageNum": pageNum,
            "username": Url.PDAusername
          }
        }
      } else if (mainNav == 'HandlingToolBaskets') {
        data = {
          "action": "obtaihandlingtoollist",
          "servicetype": "pda",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "viewRowCt": "10",
            "keyStr": strhazy,
            "factoryId": Url.PDAFid,
            "pageNum": pageNum,
            "username": Url.PDAusername
          }
        }
      } else if (mainNav == 'WaterPoolPH') {
        data = {
          "action": "getwaterpoolphlist",
          "servicetype": "pda",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "count": "10",
            "keyWord": strhazy,
            "factoryId": Url.PDAFid,
            "pageIndex": pageNum,
            "empId": Url.PDAEmployeeId,
          }
        }
      } else if (mainNav == 'WaterPoolDetail') {
        data = {
          "action": "getwaterpooldetaillist",
          "servicetype": "pda",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "count": "10",
            "keyWord": strhazy,
            "factoryId": Url.PDAFid,
            "pageIndex": pageNum,
            "empId": Url.PDAEmployeeId,
          }
        }
      } else {
        data = {
          "action": "LookUp",
          "servicetype": "pda",
          "express": "FA51025B",
          "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
          "data": {
            "PDAuserid": Url.PDAEmployeeId,
            "subscript": page,
            "strhazy": strhazy,
            "PageNum": pageNum,
            "allormy": user,
            "factoryId": Url.PDAFid
          }
        }
      }

      formData.append('jsonParam', JSON.stringify(data))
      console.log("🚀 ~ file: MainCheckPage.js ~ line 282 ~ MainCheckPage ~ _onload", JSON.stringify(data))
      fetch(Url.PDAurl, {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        body: formData
      }).then(res => {
        return res.json();
      }).then(resData => {
        //console.log("🚀 ~ file: MainCheckPage.js ~ line 1377 ~ MainCheckPage ~ _onload", JSON.stringify(resData))

        if (resData.status == '100') {
          let listDataLoad = resData.result
          let data_AllLoad = listDataLoad.all
          let data_myLoad = listDataLoad.my
          if (data_AllLoad.length > 0) {
            data_All.push.apply(data_All, data_AllLoad)
          }
          else if (data_myLoad.length > 0) {
            data_my.push.apply(data_my, data_myLoad)
          }

          this.setState({
            listData: listData,
            data_All: data_All,
            data_my: data_my,
            resData: resData,
            isLoading: false,
            pageLoading: false
          })
          if (data_AllLoad.length > 0 || data_myLoad.length > 0) {
            this.toast.show('加载完成');
          } else {
            this.toast.show('无更多数据');
          }
        } else if (resData.status != '100') {
          this.toast.show(resData.message)
        } else {
          this.toast.show('加载失败')
        }

      }).catch((error) => {
        console.log("🚀 ~ file: MainCheckPage.js ~ line 365 ~ MainCheckPage ~ error", error)
      });
    }

  }

}

class MainCheckPageMy extends React.Component {
  constructor() {
    super();
    this.state = {
      search: '', //搜索关键字保存
      pageNum: '1', //页码
      pageNumMy: '1',
      tabIndex: '', //tab栏
      resData: {}, //接口数据保存
      listData: [], //列表数据
      data_All: [],
      data_my: [],
      isLoading: true,  //判断是否加载 加载页
      isRefreshing: false,  //是否刷新
      selectedIndex: 0,  //按钮组选择
      isCheckSecondList: [],
      page: 0,
      pageLoading: true,
      tabs: [{ title: "全部" }, { title: "我的" }],
      focusIndex: 0,
      RFID: "",
      isCompModalOpen: false
    }
  }

  componentDidMount() {
    const { navigation } = this.props;
    page = navigation.getParam('title') || ''
    mainNav = navigation.getParam('main') || ''
    sontitle = navigation.getParam('sonTitle') || ''
    console.log("🚀 ~ file: MainCheckPage.js ~ line 61 ~ MainCheckPage ~ componentDidMount ~ mainNav", mainNav)
    this.requestData(page, "");
  }

  UpdateControl = () => {
    const { navigation } = this.props;
    text = navigation.getParam('text') || ''
    page = navigation.getParam('title') || ''
    if (page.indexOf('批') != -1) {
      this.setState({ isBatch: true })
    }
    mainNav = navigation.getParam('main') || ''
    sontitle = navigation.getParam('sonTitle') || ''
    console.log("🚀 ~ file: MainCheckPage.js ~ line 61 ~ MainCheckPage ~ componentDidMount ~ mainNav", mainNav)
    this.requestData(page, "");
  }

  componentWillUnmount() {
    pageNumMy = 1
    listData = [], data_All = [], data_my = []
  }

  requestData = (page, strhazy) => {
    const { pageNumMy, tabIndex } = this.state
    let allormy = ''
    strhazy = strhazy.toString()
    if (strhazy != "") {
      allormy = 'my'
    } else {
      allormy = "0"
    }

    let formData = new FormData();
    let data = {};
    if (page.indexOf('批') != -1) {
      data = {
        "action": "getbatchconctretecastinglist",
        "servicetype": "batchproduction",
        "express": "68FB8B8F",
        "ciphertext": "4b4b28876f125405bc5520e80a668aae",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy,
          "_bizid": Url.PDAFid,
          "pageNum": pageNumMy.toString(),
          "username": Url.username
        },
      }
      if (mainNav == 'HideInspection') {
        data.action = 'getbatchcomplexhidechecklist'
      }
      if (mainNav == 'StrippingInspection') {
        data.action = 'getbatchdemoldlist'
      }
      if (mainNav == 'Wantconcrete') {
        data = {
          "action": "getcalllist",
          "servicetype": "concrete",
          "express": "C80790DE",
          "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
          "data": {
            "viewRowCt": "10",
            "keyStr": strhazy,
            "_bizid": Url.PDAFid,
            "pageNum": pageNumMy.toString(),
            "username": Url.username
          },

        }
      }
      if (mainNav == 'SteelCageStorage') {
        data = {
          "action": "getbatchcageinstoragelist",
          "servicetype": "batchsteelcage",
          "express": "C80790DE",
          "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
          "data": {
            "viewRowCt": "10",
            "keyStr": strhazy,
            "_bizid": Url.PDAFid,
            "pageNum": pageNumMy.toString(),
            "username": Url.username
          }
        }
      }
    } else if (mainNav == 'ConcretePouring') {
      data = {
        "action": "getconctretecastinglist",
        "servicetype": "pdaservice",
        "express": "68FB8B8F",
        "ciphertext": "4b4b28876f125405bc5520e80a668aae",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy,
          "_bizid": Url.PDAFid,
          "pageNum": pageNumMy.toString(),
          "username": Url.username
        },
      }
    } else if (mainNav == 'Wantconcrete') {
      data = {
        "action": "getcalllist",
        "servicetype": "concrete",
        "express": "C80790DE",
        "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy,
          "_bizid": Url.PDAFid,
          "pageNum": pageNumMy.toString(),
          "username": Url.username
        },

      }
    } else if (mainNav == 'ChangeLocation') {
      data = {
        "action": "getstoragelocationchangelist",
        "servicetype": "pdaservice",
        "express": "C80790DE",
        "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy,
          "_bizid": Url.PDAFid,
          "pageNum": pageNumMy.toString(),
          "username": Url.username
        },

      }
    } else if (mainNav == 'EquipmentRepair') {
      data = {
        "action": "getmainapplicationlist",
        "servicetype": "pdaservice",
        "express": "C80790DE",
        "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy,
          "_bizid": Url.PDAFid,
          "pageNum": pageNumMy.toString(),
          "username": Url.username
        },

      }
    } else if (mainNav == 'EquipmentService') {
      let myAction = 'getrepairlist'
      if ((page == '设备维修确认') && (pageType == 'Scan')) myAction = 'getrepairconfirmlist'
      else if ((page == '设备维修确认') && (pageType == 'CheckPage')) myAction = 'getrepairconfirmlistquery'
      data = {
        "action": myAction,
        "servicetype": "pdaservice",
        "express": "C80790DE",
        "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy,
          "_bizid": Url.PDAFid,
          "pageNum": pageNumMy.toString(),
          "username": Url.username
        },

      }
    } else if (mainNav == "PDASacnLoading") {
      data = {
        action: "getproductstockoutlist",
        servicetype: "pdaproductstockoutservice",
        express: "E2BCEB1E",
        ciphertext: "077d106a940be035e6d80eb61a1a01c3",
        data: {
          _bizid: this.state.PDAFid,
          username: this.state.username,
          keyStr: strhazy,
          pageNum: pageNumMy.toString(),
          viewRowCt: "10"
        }
      };
    } else if (mainNav == 'AdvanceStorage') {
      data = {
        "action": "getpreinstolist",
        "servicetype": "pdapreinsto",
        "express": "C80790DE",
        "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy,
          "_bizid": Url.PDAFid,
          "pageNum": pageNumMy.toString(),
          "username": Url.username
        },
      }
    } else if (mainNav == 'SpotCheck') {
      data = {
        "action": "getsamplinglist",
        "servicetype": "sampling",
        "express": "C80790DE",
        "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy,
          "_bizid": Url.PDAFid,
          "pageNum": pageNumMy.toString(),
          "username": Url.username
        },
      }
    } else if (mainNav == 'SafeManageNotice') {
      data = {
        "action": "safemanagenoticequery",
        "servicetype": "pda",
        "express": "FA51025B",
        "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy || "",
          "_bizid": Url.PDAFid,
          "pageNum": pageNumMy.toString(),
          "username": Url.PDAEmployeeName
        },
      }
    } else if (mainNav == 'SafeManageReceipt') {
      var action = pageType == 'CheckPage' ? 'safemanagerectifyquery' : 'safemanagerectifycheckquery'
      data = {
        "action": action,
        "servicetype": "pda",
        "express": "FA51025B",
        "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy || "",
          "_bizid": Url.PDAFid,
          "pageNum": pageNumMy.toString(),
          "username": Url.PDAEmployeeName
        },
      }
    } else if (mainNav == 'PostRiskInspection') {
      data = {
        "action": "postriskinspectionquery",
        "servicetype": "pda",
        "express": "FA51025B",
        "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy || "",
          "_bizid": Url.PDAFid,
          "pageNum": pageNumMy.toString(),
          "username": Url.PDAEmployeeName
        },
      }
    } else if (mainNav == 'SampleManage') {
      let type = 'samplemanagequery'
      if (sontitle == '质量抽检管理') {
        type = 'samplemanagequery'
      } else if (sontitle == '抽检整改单') {
        type = pageType == 'CheckPage' ? 'samplemanagerectifycheckquery' : 'samplemanagerectifyquery'
      } else if (sontitle == '整改复检') {
        type = pageType == 'CheckPage' ? 'samplemanagerecheckcheckquery' : 'samplemanagerecheckquery'
      }
      data = {
        "action": type,
        "servicetype": "pda",
        "express": "FA51025B",
        "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy || "",
          "_bizid": Url.PDAFid,
          "pageNum": pageNumMy.toString(),
          "username": Url.PDAEmployeeName
        },
      }
    } else if (mainNav == 'CompRepair') {
      console.log("🚀 ~ file: MainCheckPage.js ~ line 409 ~ MainCheckPageAll ~ mainNav", mainNav)
      data = {
        "action": "initcomprepairquerylist",
        "servicetype": "projectsystem",
        "express": "E2BCEB1E",
        "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
        "data": {
          "factoryId": Url.PDAFid,
          "projectId": "",
          "empId": Url.PDAEmployeeId,
          "isprj": 0,
          "keyWord": "",
          "type": "repair",
          "pageIndex": 1,
          "count": 10
        }
      }
      if (pageType == 'Scan') {
        data = {
          "action": "initcomprepairquerylist",
          "servicetype": "projectsystem",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "factoryId": Url.PDAFid,
            "projectId": "",
            "empId": Url.PDAEmployeeId,
            "isprj": 0,
            "keyWord": "",
            "type": "apply",
            "pageIndex": 1,
            "count": 10
          }
        }
      }
    } else if (mainNav == 'CompMaintain') {
      data = {
        "action": "initcompmaintainquerylist",
        "servicetype": "projectsystem",
        "express": "E2BCEB1E",
        "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
        "data": {
          "factoryId": Url.PDAFid,
          "projectId": "",
          "empId": Url.PDAEmployeeId,
          "keyWord": "",
          "type": "maintain",
          "isprj": "0",
          "pageIndex": 1,
          "count": 10
        }
      }
    } else if (mainNav == 'QualityScrap') {
      data = {
        "action": "qualityscrapquery",
        "servicetype": "pdamaterial",
        "express": "6DFD1C9B",
        "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy || "",
          "_bizid": Url.PDAFid,
          "pageNum": pageNum.toString(),
          "username": Url.PDAEmployeeName
        },
      }
    } else if (mainNav == 'Inveck') {
      data = {
        "action": "getstockinventorylist",
        "servicetype": "pdaservice",
        "express": "E2BCEB1E",
        "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy,
          "_bizid": Url.PDAFid,
          "pageNum": "1",
          "username": Url.PDAusername
        }
      }
    } else if (mainNav == 'HandlingToolBaskets') {
      data = {
        "action": "obtaihandlingtoollist",
        "servicetype": "pda",
        "express": "E2BCEB1E",
        "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy,
          "factoryId": Url.PDAFid,
          "pageNum": "1",
          "username": Url.PDAusername
        }
      }
    } else if (mainNav == 'WaterPoolPH') {
      data = {
        "action": "getwaterpoolphlist",
        "servicetype": "pda",
        "express": "E2BCEB1E",
        "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
        "data": {
          "count": "10",
          "keyWord": strhazy,
          "factoryId": Url.PDAFid,
          "pageIndex": "1",
          "empId": Url.PDAEmployeeId,
        }
      }
    } else if (mainNav == 'WaterPoolDetail') {
      data = {
        "action": "getwaterpooldetaillist",
        "servicetype": "pda",
        "express": "E2BCEB1E",
        "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
        "data": {
          "count": "10",
          "keyWord": strhazy,
          "factoryId": Url.PDAFid,
          "pageIndex": "1",
          "empId": Url.PDAEmployeeId,
        }
      }
    } else {
      data = {
        "action": "LookUp",
        "servicetype": "pda",
        "express": "FA51025B",
        "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
        "data": {
          "PDAuserid": Url.PDAEmployeeId,
          "subscript": page,
          "strhazy": strhazy || "",
          "PageNum": pageNumMy.toString(),
          "allormy": allormy,
          "factoryId": Url.PDAFid
        }
      }
    }

    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: MainCheckPage.js ~ line 68 ~ MainCheckPage ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        console.log("🚀 ~ file: MainCheckPage.js ~ line 77 ~ MainCheckPage ~ resData", resData)
        this.setState({
          isLoading: false,
        })
        listData = resData.result
        if (listData.my.length > 0) {
          data_my = listData.my
        } else {
          data_my = []
        }
        if (listData.all.length == 0) {
          data_my = listData.my
          this.toast.show('无数据')
        }
        this.setState({
          listData: listData,
          data_All: data_All,
          data_my: data_my,
          resData: resData,
          pageLoading: false
        })
      } else {
        this.setState({
          isLoading: false,
          pageLoading: false
        })
        this.toast.show(resData.message)
      }

    }).catch((error) => {
    });

  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: '我的'//navigation.getParam('title')
    }
  }

  //搜索栏搜索功能
  TextChange = (text) => {
    text = text.toString()
    this.setState({
      pageLoading: true,
      search: text
    })
    this.requestData(page, text, "my")
    //this.requestData(page, text, "my")
  }

  //顶部记录与筛选按钮
  Top = () => {

  }

  DeleteData = (action, servicetype, guid) => {
    this.toast.show('删除中', 10000);

    let formData = new FormData();
    let data = {};
    data = {
      "action": action,
      "servicetype": servicetype,
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        "_rowguid": guid,
        "factoryId": Url.PDAFid
      }
    }

    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: main_compreceive.js ~ line 358 ~ miain_compreceive ~ DeleteData ~ formData", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('删除成功');
        this.refreshing()
      } else {
        this.toast.close()
        Alert.alert('删除失败', resData.message)
      }
    }).catch((error) => {
    });
  }

  render() {
    const { isLoading, isRefreshing, pageLoading, search, resData, listData, data_All, tabs } = this.state

    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (
        <View style={{ flex: 1 }}>
          <NavigationEvents
            onWillFocus={this.UpdateControl} />
          <Toast ref={(ref) => { this.toast = ref; }} position="center"></Toast>
          <View style={{ flex: 1 }}>
            <SearchBar
              platform='android'
              placeholder={mainNav == 'WaterPoolDetail' || mainNav == 'WaterPoolPH' ? '按水养池进行搜索...' : '按编号进行搜索...'}

              placeholderTextColor='#999'
              //style={{ backgroundColor: 'red', borderColor: 'red', borderWidth: 10 }}
              containerStyle={[styles.searchContainerStyle]}
              inputContainerStyle={[{ backgroundColor: '#f9f9f9', width: deviceWidth * 0.94, marginLeft: deviceWidth * 0.03, borderRadius: 45, }]}
              inputStyle={[{ color: '#333' }, deviceWidth <= 330 && { fontSize: 14 }]}
              searchIcon={defaultSearchIcon()}
              clearIcon={defaultClearIcon()}
              cancelIcon={defaultSearchIcon()}
              round={true}
              value={search}
              onChangeText={this.TextChange}
              input={(input) => { this.setState({ search: input }) }} />
            <FlatList
              style={{ flex: 1, backgroundColor: '#f8f8f8' }}
              ref={(flatList) => this._flatList1 = flatList}
              //ItemSeparatorComponent={this._separator}
              renderItem={this._renderItem}
              onRefresh={this.refreshing}
              refreshing={false}
              //控制上拉加载，两个平台需要区分开
              onEndReachedThreshold={(Platform.OS === 'ios') ? -0.3 : 0.1}
              onEndReached={() => {
                if (data_my.length >= 5) {
                  this._onload("my")
                }
              }}
              //numColumns={1}
              //ListFooterComponent={this._footer}
              //columnWrapperStyle={{borderWidth:2,borderColor:'black',paddingLeft:20}}
              //horizontal={true}
              data={data_my}
            />
          </View>
          <Overlay
            fullScreen={true}
            animationType='fade'
            isVisible={this.state.isCompModalOpen}
            onRequestClose={() => {
              this.setState({ isCompModalOpen: !this.state.isCompModalOpen });
            }}>
            <Header
              containerStyle={{ height: 45, paddingTop: 0, top: -5 }}
              centerComponent={{ text: '构件详情', style: { color: 'gray', fontSize: 20 } }}
              rightComponent={<BackIcon
                onPress={() => {
                  this.setState({
                    isCompModalOpen: !this.state.isCompModalOpen
                  });
                }} />}
              backgroundColor='white'
            />
            <View style={{ flex: 1 }}>
              <WebView source={{ uri: "http://auth.smart.pkpm.cn/QRPageFrame.aspx?sitecode=" + Url.ID + "&qrbiz=/PCIS/Production/ShowCompInfo.aspx?compid=" + this.state.RFID }}></WebView>
            </View>
          </Overlay>
        </View>
      )
    }
  }

  _renderTabItem = (item, index) => {
    const { pageLoading, data_All, data_my, search } = this.state
    if (!pageLoading) {
      if (item.title == "全部") {
        return (
          <View style={{ flex: 1 }}>
            <SearchBar
              platform='android'
              placeholder={'按编号进行搜索...'}
              placeholderTextColor='#999'
              // style={{ backgroundColor: 'red', borderColor: 'red', borderWidth: 10 }}
              //containerStyle={{ flex: 1 }}
              inputContainerStyle={{ backgroundColor: '#f9f9f9', width: deviceWidth * 0.94, marginLeft: deviceWidth * 0.03, borderRadius: 45 }}
              inputStyle={{ color: '#333' }}
              searchIcon={defaultSearchIcon()}
              clearIcon={defaultClearIcon()}
              cancelIcon={defaultSearchIcon()}
              round={true}
              value={search}
              onChangeText={this.TextChange}
              input={(input) => { this.setState({ search: input }) }} />
            <FlatList
              style={{ flex: 1, borderRadius: 10 }}
              ref={(flatList) => this._flatList1 = flatList}
              //ItemSeparatorComponent={this._separator}
              renderItem={this._renderItem}
              onRefresh={this.refreshing}
              refreshing={false}
              //控制上拉加载，两个平台需要区分开
              onEndReachedThreshold={(Platform.OS === 'ios') ? -0.3 : 0.1}
              onEndReached={() => {
                if (data_All.length >= 5) {
                  this._onload("all")
                }
              }
              }
              numColumns={1}
              //ListFooterComponent={this._footer}
              //columnWrapperStyle={{borderWidth:2,borderColor:'black',paddingLeft:20}}
              //horizontal={true}
              data={data_All}
              scrollEnabled={true}
              extraData={this.state}
            />
          </View>

        )
      } else {
        return (
          <View style={{ flex: 1 }}>
            <SearchBar
              platform='android'
              placeholder={'按编号进行搜索...'}
              placeholderTextColor='#999'
              // style={{ backgroundColor: 'red', borderColor: 'red', borderWidth: 10 }}
              //containerStyle={{ flex: 1 }}
              inputContainerStyle={{ backgroundColor: '#f9f9f9', width: deviceWidth * 0.94, marginLeft: deviceWidth * 0.03, borderRadius: 45 }}
              inputStyle={{ color: '#333' }}
              searchIcon={defaultSearchIcon()}
              clearIcon={defaultClearIcon()}
              cancelIcon={defaultSearchIcon()}
              round={true}
              value={search}
              onChangeText={this.TextChange}
              input={(input) => { this.setState({ search: input }) }} />
            <FlatList
              style={{ flex: 1, borderRadius: 10 }}
              ref={(flatList) => this._flatList1 = flatList}
              //ItemSeparatorComponent={this._separator}
              renderItem={this._renderItem}
              onRefresh={this.refreshing}
              refreshing={false}
              //控制上拉加载，两个平台需要区分开
              onEndReachedThreshold={(Platform.OS === 'ios') ? -0.3 : 0.1}
              onEndReached={() => {
                if (data_my.length >= 5) {
                  this._onload("my")
                }
              }}
              //numColumns={1}
              //ListFooterComponent={this._footer}
              //columnWrapperStyle={{borderWidth:2,borderColor:'black',paddingLeft:20}}
              //horizontal={true}
              data={data_my}
            />
          </View>

        )
      }

    } else {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
    }
  }

  //接口返回的对象数组，长列表对数据遍历
  _renderItem = ({ item, index }) => {
    let mainNavList2 = ['Wantconcrete', 'ChangeLocation', 'EquipmentRepair', 'EquipmentService', 'PDASacnLoading', 'AdvanceStorage']
    if (mainNav == 'ConcretePouring') {
      return (
        <Card containerStyle={styles.checkCardContainerStyle}>
          <TouchableOpacity
            onPress={() => {
              let _Rowguid = item._Rowguid.toString()
              this.props.navigation.navigate(mainNav, {
                guid: _Rowguid,
                pageType: 'CheckPage',
                title: sontitle,
              })
            }} >
            <View style={{ flexDirection: 'row' }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.FormCode}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditName}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text]} numberOfLines={1}> {item.ConcreteGrade}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.ProjectName || ""}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textthi]} numberOfLines={1}> {item.ConcreteType}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditDate}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    } else if (mainNavList2.indexOf(mainNav) != -1) {
      return (
        <Card containerStyle={styles.checkCardContainerStyle}>
          <TouchableOpacity
            onPress={() => {
              let guid = item._Rowguid.toString()
              this.props.navigation.navigate(mainNav, {
                guid: guid,
                pageType: 'CheckPage',
                title: sontitle,
              })
            }} >
            <View style={{ flexDirection: 'row' }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.FormCode}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditName}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text]} numberOfLines={1}> {item.ConcreteGrade}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textthi]} numberOfLines={1}> {item.ConcreteType}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditDate}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    } else if (mainNav == 'SpotCheck') {
      return (
        <Card containerStyle={styles.checkCardContainerStyle}>
          <TouchableOpacity
            onPress={() => {
              let guid = item._Rowguid.toString()
              this.props.navigation.navigate(mainNav, {
                guid: guid,
                pageType: 'CheckPage',
                title: sontitle,
              })
            }} >
            <View style={{ flexDirection: 'row' }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.FormCode}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditName}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text]} numberOfLines={1}> {item.ConcreteGrade}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.ProjectName}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textthi]} numberOfLines={1}> {item.ConcreteType}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditDate}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    } else if (page.indexOf('批') != -1) {
      if (mainNav == 'HideInspection' || mainNav == 'StrippingInspection') {
        return (
          <Card containerStyle={styles.checkCardContainerStyle}>
            <TouchableOpacity
              onPress={() => {
                let guid = ''
                if (page.indexOf('批') != -1) {
                  guid = item._Rowguid.toString()
                  this.props.navigation.navigate(mainNav, {
                    guid: guid,
                    isBatch: "true",
                    pageType: 'CheckPage',
                    title: sontitle,
                  })
                } else {
                  guid = item.guid.toString()
                  this.props.navigation.navigate(mainNav, {
                    guid: guid,
                    pageType: 'CheckPage',
                    title: sontitle,
                  })
                }
              }} >
              <View style={{ flexDirection: 'row' }}>
                <View style={[styles.SN_View]}>
                  <View style={[styles.SN_Text_View]}>
                    <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.FormCode}</Text>
                    <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditName}</Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={[styles.text]} numberOfLines={1}> {item.ConcreteGrade}</Text>
                    {
                      item.projectName == undefined ? <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.ProjectName.toString() || ""}</Text> :
                        <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.projectName.toString() || ""}</Text>
                    }

                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={[styles.text, styles.textthi]} numberOfLines={1}> {item.ConcreteType}</Text>
                    <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditDate}</Text>
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          </Card>
        )
      }
      if (mainNav == 'SteelCageStorage') {
        return (
          <Card containerStyle={styles.checkCardContainerStyle}>
            <TouchableOpacity
              onPress={() => {
                let guid = ''
                if (page.indexOf('批') != -1) {
                  guid = item._Rowguid.toString()
                  this.props.navigation.navigate(mainNav, {
                    guid: guid,
                    isBatch: "true",
                    pageType: 'CheckPage',
                    title: sontitle,
                  })
                } else {
                  guid = item.guid.toString()
                  this.props.navigation.navigate(mainNav, {
                    guid: guid,
                    pageType: 'CheckPage',
                    title: sontitle,
                  })
                }
              }} >
              <View style={{ flexDirection: 'row' }}>
                <View style={[styles.SN_View]}>
                  <View style={[styles.SN_Text_View]}>
                    <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.FormCode}</Text>
                    <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditName}</Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={[styles.text]} numberOfLines={1}> {item.ConcreteGrade}</Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={[styles.text, styles.textthi]} numberOfLines={1}> {item.ConcreteType}</Text>
                    <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditDate}</Text>
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          </Card>
        )

      }
    } else if (mainNav == 'ProductionProcess') {
      return (
        <Card containerStyle={styles.checkCardContainerStyle}>
          <View >
            <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 1, }}>
              <TouchableOpacity onPress={() => {
                if (this.state.isCheckSecondList.indexOf(item.compCode) == -1) {
                  isCheckSecondList.push(item.compCode)
                } else {
                  //查出取消勾选项的角标
                  let num = isCheckSecondList.indexOf(item.compCode)
                  //删掉！
                  isCheckSecondList.splice(num, 1)
                }
                this.setState({
                  isCheckSecondList: isCheckSecondList
                })
              }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={{ fontSize: 15, flex: 5 }} numberOfLines={1}> {'产品编号    ' + item.compCode}</Text>
                  <Icon containerStyle={{ flex: 1 }} size={18} color='#666' type='antdesign' name='down-square-o' />
                </View>
                {
                  this.state.isCheckSecondList.indexOf(item.compCode) != -1 ?
                    <View style={{ marginTop: 10, height: 1, backgroundColor: '#ccc' }}></View> : <View></View>
                }
              </TouchableOpacity>
              {
                this.state.isCheckSecondList.indexOf(item.compCode) != -1 ?
                  item.component.map((compitem) => {
                    return (
                      <TouchableOpacity
                        onPress={() => {
                          let guid = item.guid.toString()
                          let subguid = compitem.subguid.toString()
                          this.props.navigation.navigate(mainNav, {
                            guid: guid,
                            subguid: subguid,
                            pageType: 'CheckPage',
                            title: sontitle,
                          })
                        }} >
                        <View>
                          <View style={{ marginBottom: 10, }}></View>
                          <View style={{ flexDirection: 'row' }}>
                            <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {compitem.proced}</Text>
                            <Text style={[styles.text, styles.textright]} numberOfLines={1}> {compitem.compCode}</Text>
                          </View>
                          <View style={{ flexDirection: 'row' }}>
                            <Text style={[styles.text]} numberOfLines={1}> {compitem.compTypeName}</Text>
                            <Text style={[styles.text, styles.textright]} numberOfLines={1}> {compitem.PDAuser}</Text>
                          </View>
                          <View style={{ flexDirection: 'row' }}>
                            <Text style={[styles.text, styles.textthi]} numberOfLines={1}> {compitem.designType}</Text>
                            <Text style={[styles.text, styles.textright]} numberOfLines={1}> {compitem.time}</Text>
                          </View>
                          <View style={{ marginTop: 10, height: 1, backgroundColor: '#ccc' }}></View>
                        </View>
                      </TouchableOpacity>
                    )
                  })
                  : <View></View>
              }

            </View>
          </View>
        </Card>
      )
    } else if (mainNav == 'SafeManageNotice') {
      return (
        <Card containerStyle={styles.checkCardContainerStyle}>
          <TouchableOpacity
            onPress={() => {
              let guid = item._Rowguid.toString()
              this.props.navigation.navigate(mainNav, {
                guid: guid,
                pageType: 'CheckPage',
                title: sontitle,
                //data: item
              })
            }} >
            <View style={{ flexDirection: 'row' }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                <View style={{ flexDirection: 'row' }}>
                  {/*<Text style={[styles.text, styles.textfir]} style={{ flex: 1, fontSize: 18 }} numberOfLines={1}> {'整改单号：'}</Text>*/}
                  {/*<Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.rectifyNoticeInfo.toString() || ""}</Text>*/}

                  <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.FormCode.toString() || ""}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditName.toString() || ""}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  {/*<Text style={[styles.text]} numberOfLines={1}> {'班组：'}</Text>*/}
                  {/*<Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.team.toString() || ""}</Text>*/}

                  <Text style={[styles.text]} numberOfLines={1}> {item.ConcreteGrade.toString() || ""}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditDate.toString() || ""}</Text>
                </View>
                {/*<View style={{ flexDirection: 'row' }}>*/}
                {/*    <Text style={[styles.text]} numberOfLines={1}> {'签发日期：'}</Text>*/}
                {/*    <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.issueDate.toString() || ""}</Text>*/}
                {/*</View>*/}
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    } else if (mainNav == 'SafeManageReceipt') {
      return (
        <Card containerStyle={styles.checkCardContainerStyle}>
          <TouchableOpacity
            onPress={() => {
              let guid = item._Rowguid.toString()
              const { navigation } = this.props;
              let isAppPhotoSetting = navigation.getParam('isAppPhotoSetting')
              let isAppDateSetting = navigation.getParam('isAppDateSetting')
              let isAppPhotoAlbumSetting = navigation.getParam('isAppPhotoAlbumSetting')
              this.props.navigation.navigate(mainNav, {
                guid: guid,
                pageType: pageType,
                title: sontitle,
                isAppPhotoSetting: isAppPhotoSetting,
                isAppDateSetting: isAppDateSetting,
                isAppPhotoAlbumSetting: isAppPhotoAlbumSetting,
                //data: item
              })
            }} >
            <View style={{ flexDirection: 'row' }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                <View style={{ flexDirection: 'row' }}>
                  {/*<Text style={[styles.text]} style={{ flex: 1, fontSize: 18 }} numberOfLines={1}> {'整改单号：'}</Text>*/}
                  {/*<Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.rectifyNoticeInfo.toString() || ""}</Text>*/}

                  <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.FormCode.toString() || ""}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditName.toString() || ""}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  {/*<Text style={[styles.text]} numberOfLines={1}> {'班组：'}</Text>*/}
                  {/*<Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.team.toString() || ""}</Text>*/}

                  <Text style={[styles.text]} numberOfLines={1}> {item.ConcreteGrade.toString() || ""}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditDate.toString() || ""}</Text>
                </View>
                {/*<View style={{ flexDirection: 'row' }}>*/}
                {/*    <Text style={[styles.text]} numberOfLines={1}> {'签发日期：'}</Text>*/}
                {/*    <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.issueDate.toString() || ""}</Text>*/}
                {/*</View>*/}
                {
                  pageType != 'CheckPage' ? <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={[styles.text]} numberOfLines={1}> {'操作：'}</Text>
                    <View>
                      <View style={[styles.text, styles.textright]} numberOfLines={1}>
                        <Button titleStyle={{ fontSize: 14 }}
                          title='整改'
                          type='solid'
                          onPress={() => {
                            let guid = item._Rowguid.toString()
                            const { navigation } = this.props
                            let isAppPhotoSetting = navigation.getParam('isAppPhotoSetting')
                            let isAppDateSetting = navigation.getParam('isAppDateSetting')
                            let isAppPhotoAlbumSetting = navigation.getParam('isAppPhotoAlbumSetting')
                            this.props.navigation.navigate(mainNav, {
                              guid: guid,
                              pageType: pageType,
                              title: sontitle,
                              isAppPhotoSetting: isAppPhotoSetting,
                              isAppDateSetting: isAppDateSetting,
                              isAppPhotoAlbumSetting: isAppPhotoAlbumSetting
                              //data: item
                            })
                          }}
                          buttonStyle={{ paddingVertical: 6, backgroundColor: '#4D8EF5', marginVertical: 0 }}
                        />
                      </View>
                    </View>
                  </View> : <View />
                }
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    } else if (mainNav == 'PostRiskInspection') {
      return (
        <Card containerStyle={styles.checkCardContainerStyle}>
          <TouchableOpacity
            onPress={() => {
              let guid = item._Rowguid.toString()
              this.props.navigation.navigate(mainNav, {
                guid: guid,
                pageType: 'CheckPage',
                title: sontitle,
                //data: item
              })
            }} >
            <View style={{ flexDirection: 'row' }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                <View style={{ flexDirection: 'row' }}>
                  {/*<Text style={[styles.text]} style={{ flex: 1, fontSize: 18 }} numberOfLines={1}> {'表单编号：'}</Text>*/}
                  {/*<Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.formNo.toString() || ""}</Text>*/}

                  <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.FormCode.toString() || ""}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditName.toString() || ""}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  {/*<Text style={[styles.text]} numberOfLines={1}> {'班组：'}</Text>*/}
                  {/*<Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.team.toString() || ""}</Text>*/}

                  <Text style={[styles.text]} numberOfLines={1}> {item.ConcreteGrade.toString() || ""}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.ConcreteType.toString() || ""}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  {/*<Text style={[styles.text]} numberOfLines={1}> {'检查时间：'}</Text>*/}
                  {/*<Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.checkDate.toString() || ""}</Text>*/}

                  <Text style={[styles.text]} numberOfLines={1}> {item.ProjectName.toString() || ""}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditDate.toString() || ""}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    } else if (mainNav == 'SampleManage') {
      return (
        <Card containerStyle={styles.checkCardContainerStyle}>
          <TouchableOpacity
            onPress={() => {
              let guid = item._Rowguid.toString()
              const { navigation } = this.props
              let isAppPhotoSetting = navigation.getParam('isAppPhotoSetting')
              let isAppDateSetting = navigation.getParam('isAppDateSetting')
              let isAppPhotoAlbumSetting = navigation.getParam('isAppPhotoAlbumSetting')
              this.props.navigation.navigate(mainNav, {
                guid: guid,
                pageType: pageType,
                title: sontitle,
                isAppPhotoSetting: isAppPhotoSetting,
                isAppDateSetting: isAppDateSetting,
                isAppPhotoAlbumSetting: isAppPhotoAlbumSetting
                //data: item
              })
            }} >
            <View style={{ flexDirection: 'row' }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.FormCode.toString() || ""}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditName.toString() || ""}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text]} numberOfLines={1}> {item.ConcreteGrade.toString() || ""}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.ConcreteType.toString() || ""}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text]} numberOfLines={1}> {item.ProjectName.toString() || ""}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditDate.toString() || ""}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    } else if (mainNav == 'CompRepair') {
      return (
        <Card containerStyle={styles.checkCardContainerStyle}>
          <TouchableOpacity
            onPress={() => {
              let guid = item._Rowguid.toString()
              this.props.navigation.navigate(mainNav, {
                guid: guid,
                pageType: pageType,
                title: sontitle,
              })
            }} >
            <View style={{ flexDirection: 'row', }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.compCode}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.repairState}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text]} numberOfLines={1}> {item.compTypeName}</Text>
                  {
                    item.projectName == undefined ? <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.ProjectName.toString() || ""}</Text> :
                      <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.projectName.toString() || ""}</Text>
                  }

                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textthi]} numberOfLines={1}> {item.formCode}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.applyDate}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    } else if (mainNav == 'CompMaintain') {
      return (
        <Card containerStyle={styles.checkCardContainerStyle}>
          <TouchableOpacity
            onPress={() => {
              let guid = item._Rowguid.toString()
              this.props.navigation.navigate(mainNav, {
                guid: guid,
                pageType: pageType,
                title: sontitle,
                projectId: item.projectId
              })
            }} >
            <View style={{ flexDirection: 'row', }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.compCode}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.maintainState}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text]} numberOfLines={1}> {item.compTypeName}</Text>
                  {
                    item.projectName == undefined ? <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.ProjectName || ""}</Text> :
                      <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.projectName || ""}</Text>
                  }

                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textthi]} numberOfLines={1}> {item.formCode}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.applyDate}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    } else if (mainNav == 'QualityScrap') {
      return (
        <Card containerStyle={styles.checkCardContainerStyle}>
          <TouchableOpacity
            onPress={() => {
              let guid = item._Rowguid.toString()
              this.props.navigation.navigate(mainNav, {
                guid: guid,
                pageType: pageType,
                title: sontitle,
                //data: item
              })
            }} >
            <View style={{ flexDirection: 'row' }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.FormCode.toString() || ""}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditName.toString() || ""}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text]} numberOfLines={1}> {item.ConcreteType.toString() || ""}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.ProjectName.toString() || ""}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text]} numberOfLines={1}> {item.ConcreteGrade.toString() || ""}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditDate.toString() || ""}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )

    } else if (mainNav == 'Inveck') {
      return (
        <Card containerStyle={styles.checkCardContainerStyle}>
          <TouchableOpacity
            onPress={() => {
              let _Rowguid = item._Rowguid.toString()
              {
                this.props.navigation.navigate(mainNav, {
                  guid: _Rowguid,
                  pageType: 'CheckPage',
                  title: sontitle,
                })
              }
            }} >
            <View style={{ flexDirection: 'row' }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.FormCode}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditName}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text]} numberOfLines={1}> {item.ConcreteGrade}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.ProjectName || ""}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textthi]} numberOfLines={1}> {item.ConcreteType}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditDate}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    } else if (mainNav == 'HandlingToolBaskets') {
      return (
        <Card containerStyle={styles.checkCardContainerStyle}>
          <TouchableOpacity
            onPress={() => {
              let guid = item._Rowguid.toString()
              this.props.navigation.navigate(mainNav, {
                guid: guid,
                pageType: pageType,
                title: sontitle,
              })
            }} >
            <View style={{ flexDirection: 'row', }}><View style={[styles.SN_View]}>
              <View style={[styles.SN_Text_View]}>
                <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
              </View>
            </View>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.FormCode}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.PRacking}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text]} numberOfLines={1}> {item.comptype}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.projectname || ""}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textthi]} numberOfLines={1}> {item.summain + '件'}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.editdate}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    } else if (mainNav == 'WaterPoolPH') {
      return (
        <Card containerStyle={styles.checkCardContainerStyle}>
          <TouchableOpacity
            onPress={() => {
              let guid = item._rowguid.toString()
              this.props.navigation.navigate(mainNav, {
                guid: guid,
                pageType: pageType,
                title: sontitle,
              })
            }}
            onLongPress={() => {
              Alert.alert(
                '提示信息',
                '是否删除水养池信息',
                [{
                  text: '取消',
                  style: 'cancel'
                }, {
                  text: '删除',
                  onPress: () => {
                    this.DeleteData("deletewaterpoolph", "pda", item._rowguid.toString())
                  }
                }])
            }} >
            <View style={{ flexDirection: 'row', }}><View style={[styles.SN_View]}>
              <View style={[styles.SN_Text_View]}>
                <Text allowFontScaling={false} style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
              </View>
            </View>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text allowFontScaling={false} style={[styles.text, styles.textfir]} numberOfLines={1}> {item.poolname}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text allowFontScaling={false} style={[styles.text]} numberOfLines={1}> {'温度：' + item.temperature}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text allowFontScaling={false} style={[styles.text, styles.textthi]} numberOfLines={1}> {'PH值：' + item.phval}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text allowFontScaling={false} style={[styles.text]} numberOfLines={1}> {item.recorddate}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    } else if (mainNav == 'WaterPoolDetail') {
      return (
        <Card containerStyle={styles.checkCardContainerStyle}>
          <TouchableOpacity
            onPress={() => {
              let guid = item._rowguid.toString()
              let maintainstatus = item.maintainstatus.toString()
              this.props.navigation.navigate(mainNav, {
                guid: guid,
                pageType: pageType,
                title: sontitle,
                maintainstatus: maintainstatus
              })
            }}
            onLongPress={() => {
              Alert.alert(
                '提示信息',
                '是否删除水养池信息',
                [{
                  text: '取消',
                  style: 'cancel'
                }, {
                  text: '删除',
                  onPress: () => {
                    this.DeleteData("deletewaterpooldetail", "pda", item._rowguid.toString())
                  }
                }])
            }} >
            <View style={{ flexDirection: 'row', }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text allowFontScaling={false} style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text allowFontScaling={false} style={[styles.text, styles.textfir]} numberOfLines={1}> {item.poolname}</Text>
                  {
                    item.maintainstatus ?
                      <View style={[styles.badgeView, { backgroundColor: item.maintainstatus == '养护完成' ? 'rgba(98, 178, 250, 0.22)' : 'rgba(21, 214, 57, 0.22)' }]}>
                        <Badge
                          badgeStyle={{ backgroundColor: item.maintainstatus == '养护完成' ? '#64B1FC' : '#15D639' }}
                          containerStyle={{ marginRight: 7, top: 14 }}
                        />
                        <Text allowFontScaling={false} style={[styles.badgeTextView, { color: item.maintainstatus == '养护完成' ? '#64B1FC' : '#15D639', fontWeight: 'bold' }]}>{item.maintainstatus}</Text>
                      </View> :
                      <View></View>
                  }
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text allowFontScaling={false} style={[styles.text]} numberOfLines={1}> {item.datestr}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text allowFontScaling={false} style={[styles.text, styles.textthi]} numberOfLines={1}> {item.sumcount}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    } else {
      return (
        <Card containerStyle={styles.checkCardContainerStyle}>
          <TouchableOpacity
            onPress={() => {
              let guid = item.guid.toString()
              this.props.navigation.navigate(mainNav, {
                guid: guid,
                pageType: 'CheckPage',
                title: sontitle,
              })
            }} >
            <View style={{ flexDirection: 'row', }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.compCode}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.PDAuser}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text]} numberOfLines={1}> {item.compTypeName}</Text>
                  {
                    item.projectName == undefined ? <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.ProjectName.toString() || ""}</Text> :
                      <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.projectName.toString() || ""}</Text>
                  }
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textthi]} numberOfLines={1}> {item.designType}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.time}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    }

  }

  refreshing = () => {
    this.toast.show('刷新中...', 10000)
    this.setState({
      pageLoading: true
    })
    const { pageNum } = this.state
    let formData = new FormData();
    let data = {};
    if (page.indexOf('批') != -1) {
      data = {
        "action": "getbatchconctretecastinglist",
        "servicetype": "batchproduction",
        "express": "68FB8B8F",
        "ciphertext": "4b4b28876f125405bc5520e80a668aae",
        "data": {
          "viewRowCt": "10",
          "keyStr": "",
          "_bizid": Url.PDAFid,
          "pageNum": "1",
          "username": Url.username
        },
      }
      if (mainNav == 'HideInspection') {
        data.action = 'getbatchcomplexhidechecklist'
      }
      if (mainNav == 'StrippingInspection') {
        data.action = 'getbatchdemoldlist'
      }
      if (mainNav == 'Wantconcrete') {
        data = {
          "action": "getcalllist",
          "servicetype": "concrete",
          "express": "C80790DE",
          "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
          "data": {
            "viewRowCt": "10",
            "keyStr": "",
            "_bizid": Url.PDAFid,
            "pageNum": "1",
            "username": Url.username
          },

        }
      }
      if (mainNav == 'SteelCageStorage') {
        data = {
          "action": "getbatchcageinstoragelist",
          "servicetype": "batchsteelcage",
          "express": "C80790DE",
          "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
          "data": {
            "viewRowCt": "10",
            "keyStr": "",
            "_bizid": Url.PDAFid,
            "pageNum": "1",
            "username": Url.username
          }
        }
      }
    } else if (mainNav == 'ConcretePouring') {
      data = {
        "action": "getconctretecastinglist",
        "servicetype": "pdaservice",
        "express": "68FB8B8F",
        "ciphertext": "4b4b28876f125405bc5520e80a668aae",
        "data": {
          "viewRowCt": "10",
          "keyStr": "",
          "_bizid": Url.PDAFid,
          "pageNum": '1',
          "username": Url.username
        },
      }
    } else if (mainNav == 'Wantconcrete') {
      data = {
        "action": "getcalllist",
        "servicetype": "concrete",
        "express": "C80790DE",
        "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
        "data": {
          "viewRowCt": "10",
          "keyStr": "",
          "_bizid": Url.PDAFid,
          "pageNum": '1',
          "username": Url.username
        },

      }
    } else if (mainNav == 'ChangeLocation') {
      data = {
        "action": "getstoragelocationchangelist",
        "servicetype": "pdaservice",
        "express": "C80790DE",
        "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
        "data": {
          "viewRowCt": "10",
          "keyStr": "",
          "_bizid": Url.PDAFid,
          "pageNum": '1',
          "username": Url.username
        },

      }
    } else if (mainNav == 'EquipmentRepair') {
      data = {
        "action": "getmainapplicationlist",
        "servicetype": "pdaservice",
        "express": "C80790DE",
        "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
        "data": {
          "viewRowCt": "10",
          "keyStr": "",
          "_bizid": Url.PDAFid,
          "pageNum": '1',
          "username": Url.username
        },

      }
    } else if (mainNav == 'EquipmentService') {
      let myAction = 'getrepairlist'
      if ((page == '设备维修确认') && (pageType == 'Scan')) myAction = 'getrepairconfirmlist'
      else if ((page == '设备维修确认') && (pageType == 'CheckPage')) myAction = 'getrepairconfirmlistquery'
      data = {
        "action": myAction,
        "servicetype": "pdaservice",
        "express": "C80790DE",
        "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
        "data": {
          "viewRowCt": "10",
          "keyStr": "",
          "_bizid": Url.PDAFid,
          "pageNum": '1',
          "username": Url.username
        },

      }
    } else if (mainNav == 'AdvanceStorage') {
      data = {
        "action": "getpreinstolist",
        "servicetype": "pdapreinsto",
        "express": "C80790DE",
        "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
        "data": {
          "viewRowCt": "10",
          "keyStr": "",
          "_bizid": Url.PDAFid,
          "pageNum": '1',
          "username": Url.username
        },
      }
    } else if (mainNav == 'SpotCheck') {
      data = {
        "action": "getsamplinglist",
        "servicetype": "sampling",
        "express": "C80790DE",
        "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
        "data": {
          "viewRowCt": "10",
          "keyStr": "",
          "_bizid": Url.PDAFid,
          "pageNum": "1",
          "username": Url.username
        },
      }
    } else if (mainNav == 'SafeManageNotice') {
      data = {
        "action": "safemanagenoticequery",
        "servicetype": "pda",
        "express": "FA51025B",
        "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
        "data": {
          "viewRowCt": "10",
          "keyStr": "",
          "_bizid": Url.PDAFid,
          "pageNum": '1',
          "username": Url.PDAEmployeeName
        },
      }
    } else if (mainNav == 'SafeManageReceipt') {
      var action = pageType == 'CheckPage' ? 'safemanagerectifyquery' : 'safemanagerectifycheckquery'
      data = {
        "action": "safemanagenoticequery",
        "servicetype": "pda",
        "express": "FA51025B",
        "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
        "data": {
          "viewRowCt": "10",
          "keyStr": "",
          "_bizid": Url.PDAFid,
          "pageNum": '1',
          "username": Url.PDAEmployeeName
        },
      }
    } else if (mainNav == 'PostRiskInspection') {
      data = {
        "action": "postriskinspectionquery",
        "servicetype": "pda",
        "express": "FA51025B",
        "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
        "data": {
          "viewRowCt": "10",
          "keyStr": "",
          "_bizid": Url.PDAFid,
          "pageNum": '1',
          "username": Url.PDAEmployeeName
        },
      }
    } else if (mainNav == 'SampleManage') {
      let type = 'samplemanagequery'
      if (sontitle == '质量抽检管理') {
        type = 'samplemanagequery'
      } else if (sontitle == '抽检整改单') {
        type = pageType == 'CheckPage' ? 'samplemanagerectifycheckquery' : 'samplemanagerectifyquery'
      } else if (sontitle == '整改复检') {
        type = pageType == 'CheckPage' ? 'samplemanagerecheckcheckquery' : 'samplemanagerecheckquery'
      }
      data = {
        "action": type,
        "servicetype": "pda",
        "express": "FA51025B",
        "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
        "data": {
          "viewRowCt": "10",
          "keyStr": "",
          "_bizid": Url.PDAFid,
          "pageNum": '1',
          "username": Url.PDAEmployeeName
        },
      }
    } else if (mainNav == 'CompRepair') {
      console.log("🚀 ~ file: MainCheckPage.js ~ line 409 ~ MainCheckPageAll ~ mainNav", mainNav)
      data = {
        "action": "initcomprepairquerylist",
        "servicetype": "projectsystem",
        "express": "E2BCEB1E",
        "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
        "data": {
          "factoryId": Url.PDAFid,
          "projectId": "",
          "empId": Url.PDAEmployeeId,
          "isprj": 0,
          "keyWord": "",
          "type": "repair",
          "pageIndex": 1,
          "count": 10
        }
      }
      if (pageType == 'Scan') {
        data = {
          "action": "initcomprepairquerylist",
          "servicetype": "projectsystem",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "factoryId": Url.PDAFid,
            "projectId": "",
            "empId": Url.PDAEmployeeId,
            "isprj": 0,
            "keyWord": "",
            "type": "repair",
            "pageIndex": 1,
            "count": 10
          }
        }
      }
    } else if (mainNav == 'CompMaintain') {
      data = {
        "action": "initcompmaintainquerylist",
        "servicetype": "projectsystem",
        "express": "E2BCEB1E",
        "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
        "data": {
          "factoryId": Url.PDAFid,
          "projectId": "",
          "empId": Url.PDAEmployeeId,
          "keyWord": "",
          "type": "maintain",
          "isprj": "0",
          "pageIndex": 1,
          "count": 10
        }
      }
    } else if (mainNav == 'QualityScrap') {
      data = {
        "action": "qualityscrapquery",
        "servicetype": "pdamaterial",
        "express": "6DFD1C9B",
        "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
        "data": {
          "viewRowCt": "10",
          "keyStr": "",
          "_bizid": Url.PDAFid,
          "pageNum": '1',
          "username": Url.PDAEmployeeName
        },
      }
    } else if (mainNav == 'Inveck') {
      data = {
        "action": "getstockinventorylist",
        "servicetype": "pdaservice",
        "express": "E2BCEB1E",
        "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
        "data": {
          "viewRowCt": "10",
          "keyStr": "",
          "_bizid": Url.PDAFid,
          "pageNum": "1",
          "username": Url.PDAusername
        }
      }
    } else if (mainNav == 'HandlingToolBaskets') {
      data = {
        "action": "obtaihandlingtoollist",
        "servicetype": "pda",
        "express": "E2BCEB1E",
        "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy,
          "factoryId": Url.PDAFid,
          "pageNum": "1",
          "username": Url.PDAusername
        }
      }
    } else if (mainNav == 'WaterPoolPH') {
      data = {
        "action": "getwaterpoolphlist",
        "servicetype": "pda",
        "express": "E2BCEB1E",
        "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
        "data": {
          "count": "10",
          "keyWord": "",
          "factoryId": Url.PDAFid,
          "pageIndex": "1",
          "empId": Url.PDAEmployeeId,
        }
      }
    } else if (mainNav == 'WaterPoolDetail') {
      data = {
        "action": "getwaterpooldetaillist",
        "servicetype": "pda",
        "express": "E2BCEB1E",
        "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
        "data": {
          "count": "10",
          "keyWord": "",
          "factoryId": Url.PDAFid,
          "pageIndex": "1",
          "empId": Url.PDAEmployeeId,
        }
      }
    } else {
      data = {
        "action": "LookUp",
        "servicetype": "pda",
        "express": "FA51025B",
        "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
        "data": {
          "PDAuserid": Url.PDAEmployeeId,
          "subscript": page,
          "strhazy": "",
          "PageNum": pageNum,
          "allormy": "0",
          "factoryId": Url.PDAFid
        }
      }
    }

    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: MainCheckPage.js ~ line 68 ~ MainCheckPage ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: MainCheckPage.js ~ line 77 ~ MainCheckPage ~ resData", resData)
      if (resData.status == '100') {
        listData = resData.result
        data_All = listData.all
        data_my = listData.my
        this.setState({
          listData: listData,
          data_All: data_All,
          data_my: data_my,
          resData: resData,
          isLoading: false,
          pageLoading: false
        })
        this.toast.show('刷新完成', 1600)
      } else {
        listData = resData.result
        data_All = listData.all
        data_my = listData.my
        this.setState({
          listData: listData,
          data_All: data_All,
          data_my: data_my,
          resData: resData,
          isLoading: false,
          pageLoading: false
        })
        this.toast.show('刷新失败', 1600)
      }
    }).catch((error) => {
    });
  }

  _onload = (user) => {
    /* if (data_my.length < 10 && data_All.length != []) {
      return
    } 
    else  */{
      const { search } = this.state
      let strhazy = search
      this.toast.show('加载中...', 10000)
      pageNumMy += 1
      let formData = new FormData();
      let data = {};
      if (page.indexOf('批') != -1) {
        data = {
          "action": "getbatchconctretecastinglist",
          "servicetype": "batchproduction",
          "express": "68FB8B8F",
          "ciphertext": "4b4b28876f125405bc5520e80a668aae",
          "data": {
            "viewRowCt": "10",
            "keyStr": strhazy,
            "_bizid": Url.PDAFid,
            "pageNum": pageNumMy.toString(),
            "username": Url.username
          },
        }
        if (mainNav == 'HideInspection') {
          data.action = 'getbatchcomplexhidechecklist'
        }
        if (mainNav == 'StrippingInspection') {
          data.action = 'getbatchdemoldlist'
        }
        if (mainNav == 'Wantconcrete') {
          data = {
            "action": "getcalllist",
            "servicetype": "concrete",
            "express": "C80790DE",
            "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
            "data": {
              "viewRowCt": "10",
              "keyStr": strhazy,
              "_bizid": Url.PDAFid,
              "pageNum": pageNumMy.toString(),
              "username": Url.username
            },

          }
        }
        if (mainNav == 'SteelCageStorage') {
          data = {
            "action": "getbatchcageinstoragelist",
            "servicetype": "batchsteelcage",
            "express": "C80790DE",
            "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
            "data": {
              "viewRowCt": "10",
              "keyStr": strhazy,
              "_bizid": Url.PDAFid,
              "pageNum": pageNumMy.toString(),
              "username": Url.username
            }
          }
        }
      } else if (mainNav == 'ConcretePouring') {
        data = {
          "action": "getconctretecastinglist",
          "servicetype": "pdaservice",
          "express": "68FB8B8F",
          "ciphertext": "4b4b28876f125405bc5520e80a668aae",
          "data": {
            "viewRowCt": "10",
            "keyStr": strhazy,
            "_bizid": Url.PDAFid,
            "pageNum": pageNumMy.toString(),
            "username": Url.username
          },
        }
      } else if (mainNav == 'Wantconcrete') {
        data = {
          "action": "getcalllist",
          "servicetype": "concrete",
          "express": "C80790DE",
          "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
          "data": {
            "viewRowCt": "10",
            "keyStr": strhazy,
            "_bizid": Url.PDAFid,
            "pageNum": pageNumMy.toString(),
            "username": Url.username
          },

        }
      } else if (mainNav == 'ChangeLocation') {
        data = {
          "action": "getstoragelocationchangelist",
          "servicetype": "pdaservice",
          "express": "C80790DE",
          "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
          "data": {
            "viewRowCt": "10",
            "keyStr": strhazy,
            "_bizid": Url.PDAFid,
            "pageNum": pageNumMy.toString(),
            "username": Url.username
          },

        }
      } else if (mainNav == 'EquipmentRepair') {
        data = {
          "action": "getmainapplicationlist",
          "servicetype": "pdaservice",
          "express": "C80790DE",
          "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
          "data": {
            "viewRowCt": "10",
            "keyStr": strhazy,
            "_bizid": Url.PDAFid,
            "pageNum": pageNumMy.toString(),
            "username": Url.username
          },

        }
      } else if (mainNav == 'EquipmentService') {
        let myAction = 'getrepairlist'
        if ((page == '设备维修确认') && (pageType == 'Scan')) myAction = 'getrepairconfirmlist'
        else if ((page == '设备维修确认') && (pageType == 'CheckPage')) myAction = 'getrepairconfirmlistquery'
        data = {
          "action": myAction,
          "servicetype": "pdaservice",
          "express": "C80790DE",
          "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
          "data": {
            "viewRowCt": "10",
            "keyStr": strhazy,
            "_bizid": Url.PDAFid,
            "pageNum": pageNumMy.toString(),
            "username": Url.username
          },

        }
      } else if (mainNav == 'AdvanceStorage') {
        data = {
          "action": "getpreinstolist",
          "servicetype": "pdapreinsto",
          "express": "C80790DE",
          "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
          "data": {
            "viewRowCt": "10",
            "keyStr": strhazy,
            "_bizid": Url.PDAFid,
            "pageNum": pageNumMy.toString(),
            "username": Url.username
          },
        }
      } else if (mainNav == 'SpotCheck') {
        data = {
          "action": "getsamplinglist",
          "servicetype": "sampling",
          "express": "C80790DE",
          "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
          "data": {
            "viewRowCt": "10",
            "keyStr": strhazy,
            "_bizid": Url.PDAFid,
            "pageNum": pageNumMy.toString(),
            "username": Url.username
          },
        }
      } else if (mainNav == 'SafeManageNotice') {
        data = {
          "action": "safemanagenoticequery",
          "servicetype": "pda",
          "express": "FA51025B",
          "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
          "data": {
            "viewRowCt": "10",
            "keyStr": strhazy || "",
            "_bizid": Url.PDAFid,
            "pageNum": pageNumMy.toString(),
            "username": Url.PDAEmployeeName
          },
        }
      } else if (mainNav == 'SafeManageReceipt') {
        var action = pageType == 'CheckPage' ? 'safemanagerectifyquery' : 'safemanagerectifycheckquery'
        data = {
          "action": action,
          "servicetype": "pda",
          "express": "FA51025B",
          "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
          "data": {
            "viewRowCt": "10",
            "keyStr": strhazy,
            "_bizid": Url.PDAFid,
            "pageNum": pageNumMy.toString(),
            "username": Url.PDAEmployeeName
          },
        }
      } else if (mainNav == 'PostRiskInspection') {
        data = {
          "action": "postriskinspectionquery",
          "servicetype": "pda",
          "express": "FA51025B",
          "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
          "data": {
            "viewRowCt": "10",
            "keyStr": strhazy || "",
            "_bizid": Url.PDAFid,
            "pageNum": pageNumMy.toString(),
            "username": Url.PDAEmployeeName
          },
        }
      } else if (mainNav == 'SampleManage') {
        let type = 'samplemanagequery'
        if (sontitle == '质量抽检管理') {
          type = 'samplemanagequery'
        } else if (sontitle == '抽检整改单') {
          type = pageType == 'CheckPage' ? 'samplemanagerectifycheckquery' : 'samplemanagerectifyquery'
        } else if (sontitle == '整改复检') {
          type = pageType == 'CheckPage' ? 'samplemanagerecheckcheckquery' : 'samplemanagerecheckquery'
        }
        data = {
          "action": type,
          "servicetype": "pda",
          "express": "FA51025B",
          "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
          "data": {
            "viewRowCt": "10",
            "keyStr": strhazy || "",
            "_bizid": Url.PDAFid,
            "pageNum": pageNumMy.toString(),
            "username": Url.PDAEmployeeName
          },
        }
      } else if (mainNav == 'CompRepair') {
        console.log("🚀 ~ file: MainCheckPage.js ~ line 409 ~ MainCheckPageAll ~ mainNav", mainNav)
        data = {
          "action": "initcomprepairquerylist",
          "servicetype": "projectsystem",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "factoryId": Url.PDAFid,
            "projectId": "",
            "empId": Url.PDAEmployeeId,
            "isprj": 0,
            "keyWord": strhazy,
            "type": "repair",
            "pageIndex": pageNum,
            "count": 10
          }
        }
        if (pageType == "Scan") {
          data = {
            "action": "initcomprepairquerylist",
            "servicetype": "projectsystem",
            "express": "E2BCEB1E",
            "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
            "data": {
              "factoryId": Url.PDAFid,
              "projectId": "",
              "empId": Url.PDAEmployeeId,
              "isprj": 0,
              "keyWord": strhazy,
              "type": "apply",
              "pageIndex": pageNum,
              "count": 10
            }
          }
        }
      } else if (mainNav == 'CompMaintain') {
        data = {
          "action": "initcompmaintainquerylist",
          "servicetype": "projectsystem",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "factoryId": Url.PDAFid,
            "projectId": "",
            "empId": Url.PDAEmployeeId,
            "keyWord": strhazy,
            "type": "maintain",
            "isprj": "0",
            "pageIndex": pageNum,
            "count": 10
          }
        }
      } else if (mainNav == 'QualityScrap') {
        data = {
          "action": "qualityscrapquery",
          "servicetype": "pdamaterial",
          "express": "6DFD1C9B",
          "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
          "data": {
            "viewRowCt": "10",
            "keyStr": strhazy || "",
            "_bizid": Url.PDAFid,
            "pageNum": pageNum.toString(),
            "username": Url.PDAEmployeeName
          },
        }
      } else if (mainNav == 'Inveck') {
        data = {
          "action": "getstockinventorylist",
          "servicetype": "pdaservice",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "viewRowCt": "10",
            "keyStr": strhazy,
            "factoryId": Url.PDAFid,
            "pageNum": pageNumMy,
            "username": Url.PDAusername
          }
        }
      } else if (mainNav == 'HandlingToolBaskets') {
        data = {
          "action": "obtaihandlingtoollist",
          "servicetype": "pda",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "viewRowCt": "10",
            "keyStr": strhazy,
            "_bizid": Url.PDAFid,
            "pageNum": pageNumMy,
            "username": Url.PDAusername
          }
        }
      } else if (mainNav == 'WaterPoolPH') {
        data = {
          "action": "getwaterpoolphlist",
          "servicetype": "pda",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "count": "10",
            "keyWord": strhazy,
            "factoryId": Url.PDAFid,
            "pageIndex": pageNumMy,
            "empId": Url.PDAEmployeeId,
          }
        }
      } else if (mainNav == 'WaterPoolDetail') {
        data = {
          "action": "getwaterpooldetaillist",
          "servicetype": "pda",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "count": "10",
            "keyWord": strhazy,
            "factoryId": Url.PDAFid,
            "pageIndex": pageNumMy,
            "empId": Url.PDAEmployeeId,
          }
        }
      } else {
        data = {
          "action": "LookUp",
          "servicetype": "pda",
          "express": "FA51025B",
          "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
          "data": {
            "PDAuserid": Url.PDAEmployeeId,
            "subscript": page,
            "strhazy": strhazy,
            "PageNum": pageNumMy,
            "allormy": user,
            "factoryId": Url.PDAFid
          }
        }
      }

      formData.append('jsonParam', JSON.stringify(data))
      //console.log("🚀 ~ file: MainCheckPage.js ~ line 282 ~ MainCheckPage ~ _onload", JSON.stringify(data))
      fetch(Url.PDAurl, {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        body: formData
      }).then(res => {
        return res.json();
      }).then(resData => {
        //console.log("🚀 ~ file: MainCheckPage.js ~ line 292 ~ MainCheckPage ~ _onload", JSON.stringify(resData))

        if (resData.status == '100') {
          let listDataLoad = resData.result
          let data_AllLoad = listDataLoad.all
          let data_myLoad = listDataLoad.my
          if (data_myLoad.length > 0) {
            data_my.push.apply(data_my, data_myLoad)
          }
          this.setState({
            listData: listData,
            data_All: data_All,
            data_my: data_my,
            resData: resData,
            isLoading: false,
            pageLoading: false
          })
          console.log("🚀 ~ file: MainCheckPage.js ~ line 2532 ~ MainCheckPage ~ _onload", JSON.stringify(data_my))
          if (data_AllLoad.length > 0 || data_myLoad.length > 0) {
            this.toast.show('加载完成');
          } else {
            this.toast.show('无更多数据');
          }
        } else if (resData.status != '100') {
          this.toast.show(resData.message)
        } else {
          this.toast.show('加载失败')
        }

      }).catch((error) => {
        console.log("🚀 ~ file: MainCheckPage.js ~ line 365 ~ MainCheckPage ~ error", error)
      });
    }

  }

}

const PDATopBottom = createMaterialTopTabNavigator({
  全部: {
    screen: MainCheckPageAll,
  },
  我的: {
    screen: MainCheckPageMy,
  },

}, {
  navigationOptions: ({
    navigation
  }) => ({
    title: navigation.getParam('title'),
  }),
  //tabBarPosition: 'bottom',
  //title: this.props.navigation.getParam('title'),
  animationEnabled: true,
  backBehavior: true,
  tabBarOptions: {
    activeTintColor: '#419FFF',
    tabStyle: {
      width: deviceWidth * 0.22,
      paddingTop: 3
    },
    labelStyle: {
      backgroundColor: 'transparent',
      paddingVertical: deviceWidth <= 330 ? 5 : 0,
      width: deviceWidth * 0.3,
      //height: 15,
      height: deviceWidth <= 330 ? 25 : 15,
      lineHeight: deviceWidth <= 330 ? 10 : 15
    },
    style: {
      backgroundColor: 'white',
      width: deviceWidth * 0.4,
      paddingVertical: 0,
      //height: 38,
      height: deviceWidth <= 330 ? 38 : 38,
      //height: 50,
      elevation: 0
      //marginLeft: deviceWidth * 0.2
    },
    allowFontScaling: false,
    inactiveTintColor: 'black',
    indicatorStyle: {
      backgroundColor: '#419FFF',
      width: deviceWidth * 0.12,
      marginLeft: deviceWidth * 0.05,
    },
  },

})

export default PDATopBottom

const styles = StyleSheet.create({
  loading: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',

  },
  contrain: {
    flexDirection: 'column',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  txt: {

    fontSize: 16,
    marginBottom: 8,
    marginTop: 8,
    marginHorizontal: 8
  },
  text: {
    fontSize: 14,
    marginBottom: 5,
    marginTop: 5,
    flex: 1
  },
  textright: {
    textAlign: 'right',
    color: 'gray'
  },
  textfir: {
    flex: 2,
    fontSize: 18
  },
  textthi: {
    fontSize: 15
  },
  textname: {
    fontSize: 14,
    marginBottom: 8,
    marginTop: 8,
    color: 'gray'
  },
  textView: {

    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  hidden: {
    flex: 0,
    height: 0,
    width: 0,
  },
  searchContainerStyle:
  {
    shadowColor: '#999', shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.3,
    shadowRadius: 6,
  },
  //安卓ios不同
  checkCardContainerStyle: {
    borderRadius: 10,
    elevation: 0
  },
  listView: {
    marginHorizontal: width * 0.06,
    marginTop: width * 0.04,
    paddingHorizontal: 0,
    paddingTop: 5,
    paddingBottom: 15,
    backgroundColor: 'white',
    borderRadius: 10
  },
  SN_View: {
    flex: 1
  },
  SN_Text_View: {
    top: 7
  },
  SN_Text: {
    textAlignVertical: 'center',
    fontSize: 16,
    color: '#535c68'
  },
  SN_Text_Comp: {
    fontSize: 16,
  },
  badgeView: {
    //height: 30,
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    borderRadius: 20,
    flex: 1
  },
  badgeTextView: {
    fontSize: 14,
    lineHeight: 30,
    //marginBottom: 4,
    //marginTop: 4,
    textAlignVertical: 'center'
  },
  focusColor: {
    backgroundColor: 'rgba(254,211,48,1.0)'
  },
})