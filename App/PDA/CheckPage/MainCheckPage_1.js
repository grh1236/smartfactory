import React from 'react'
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge } from 'react-native-elements';
import Url from '../../Url/Url';
import { ToDay, YesterDay, BeforeYesterDay } from '../../CommonComponents/Data';
import { RFT, deviceWidth } from '../../Url/Pixal';
import { createMaterialTopTabNavigator, } from 'react-navigation';
import ScrollTab from 'react-native-scroll-tab-page'
import { ScrollView } from 'react-native';
import Toast from 'react-native-easy-toast';
//import { styles } from '../Componment/PDAStyles';

const { height, width } = Dimensions.get('window') //获取宽高

let pageNum = 1, page = '', mainNav = '', sontitle = ''
let listData = [], data_All = [], data_my = []

//搜索栏搜索图标
const defaultSearchIcon = () => ({
  //type: 'antdesign',
  size: 20,
  name: 'search',
  color: 'white',
});

//搜索栏清理图标
const defaultClearIcon = () => ({
  //type: '',
  size: 20,
  name: 'close',
  color: 'white',
});

/* const Tab = createMaterialTopTabNavigator({
  Tab1: Test_2,
  Tab2: Test_2
})  */

export default class MainCheckPage extends React.Component {
  constructor() {
    super();
    this.state = {
      search: '', //搜索关键字保存
      pageNum: '1', //页码
      tabIndex: '', //tab栏
      resData: {}, //接口数据保存
      listData: [], //列表数据
      data_All: [],
      data_my: [],
      isLoading: true,  //判断是否加载 加载页
      isRefreshing: false,  //是否刷新
      selectedIndex: 0,  //按钮组选择
      pageLoading: true,
      tabs: [{ title: "全部" }, { title: "我的" }]
    }
  }

  componentDidMount() {
    const { navigation } = this.props;
    page = navigation.getParam('title') || ''
    mainNav = navigation.getParam('main') || ''
    sontitle = navigation.getParam('sonTitle') || ''
    console.log("🚀 ~ file: MainCheckPage.js ~ line 61 ~ MainCheckPage ~ componentDidMount ~ mainNav", mainNav)
    this.requestData(page, "");
  }

  componentWillUnmount() {
    pageNum = 1
    listData = [], data_All = [], data_my = []
  }

  requestData = (page, strhazy) => {
    const { pageNum, tabIndex } = this.state
    let allormy = ''
    console.log("🚀 ~ file: MainCheckPage.js ~ line 76 ~ MainCheckPage ~ strhazy", strhazy)
    strhazy = strhazy.toString()
    console.log("🚀 ~ file: MainCheckPage.js ~ line 76 ~ MainCheckPage ~ strhazy", strhazy)
    console.log("🚀 ~ file: MainCheckPage.js ~ line 76 ~ MainCheckPage ~ typeof strhazy", typeof strhazy)
    if (strhazy != "") {
      if (tabIndex == '0') {
        allormy = 'all'
      } else if (tabIndex == '1') {
        allormy = 'my'
      }
    } else {
      allormy = "0"
    }

    let formData = new FormData();
    let data = {};
    if (mainNav == 'ConcretePouring') {
      data = {
        "action": "getconctretecastinglist",
        "servicetype": "pdaservice",
        "express": "68FB8B8F",
        "ciphertext": "4b4b28876f125405bc5520e80a668aae",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy,
          "_bizid": Url.PDAFid,
          "pageNum": pageNum.toString(),
          "username": Url.username
        },
      }
    } else if (mainNav == 'Wantconcrete') {
      data = {
        "action": "getcalllist",
        "servicetype": "concrete",
        "express": "C80790DE",
        "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy,
          "_bizid": Url.PDAFid,
          "pageNum": pageNum.toString(),
          "username": Url.username
        },

      }
    } else if (mainNav == 'ChangeLocation') {
      data = {
        "action": "getstoragelocationchangelist",
        "servicetype": "pdaservice",
        "express": "C80790DE",
        "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy,
          "_bizid": Url.PDAFid,
          "pageNum": pageNum.toString(),
          "username": Url.username
        },

      }
    } else if (mainNav == 'EquipmentRepair') {
      data = {
        "action": "getmainapplicationlist",
        "servicetype": "pdaservice",
        "express": "C80790DE",
        "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy,
          "_bizid": Url.PDAFid,
          "pageNum":pageNum.toString(),
          "username": Url.username
        },
       
      }
    } else if (mainNav == 'EquipmentService') {
      data = {
        "action": "getrepairlist",
        "servicetype": "pdaservice",
        "express": "C80790DE",
        "ciphertext": "146f7ce63caaacc54569d44cb24cb087",
        "data": {
          "viewRowCt": "10",
          "keyStr": strhazy,
          "_bizid": Url.PDAFid,
          "pageNum":pageNum.toString(),
          "username": Url.username
        },
       
      }
    } else {
      data = {
        "action": "LookUp",
        "servicetype": "pda",
        "express": "FA51025B",
        "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
        "data": {
          "PDAuserid": Url.PDAEmployeeId,
          "subscript": page,
          "strhazy": strhazy || "",
          "PageNum": pageNum.toString(),
          "allormy": allormy,
          "factoryId": Url.PDAFid
        }
      }
    }

    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: MainCheckPage.js ~ line 68 ~ MainCheckPage ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: MainCheckPage.js ~ line 77 ~ MainCheckPage ~ resData", resData)
      listData = resData.result
      data_All = listData.all
      if (listData.all.length > 0) {
        data_All = listData.all
      } else {
        data_All = []
      }
      if (listData.my.length > 0) {
        data_my = listData.my
      } else {
        data_my = []
      }
      this.setState({
        listData: listData,
        data_All: data_All,
        data_my: data_my,
        resData: resData,
        isLoading: false,
        pageLoading: false
      })
    }).catch((error) => {
    });

  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  //搜索栏搜索功能
  TextChange = (text) => {
    text = text.toString()
    this.setState({
      pageLoading: true,
      search: text
    })
    this.requestData(page, text, "all")
    //this.requestData(page, text, "my")
  }

  //顶部记录与筛选按钮
  Top = () => {

  }

  render() {
    const { isLoading, isRefreshing, pageLoading, search, resData, listData, data_All, tabs } = this.state

    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Toast ref={(ref) => { this.toast = ref; }} position="center"></Toast>
          <SearchBar
            platform='android'
            placeholder={'按编号进行搜索...'}
            placeholderTextColor='white'
            // style={{ backgroundColor: 'red', borderColor: 'red', borderWidth: 10 }}
            //containerStyle={{ flex: 1 }}
            inputContainerStyle={{ backgroundColor: '#419FFF', width: deviceWidth * 0.94, marginLeft: deviceWidth * 0.03, borderRadius: 45 }}
            inputStyle={{ color: '#FFFFFF' }}
            searchIcon={defaultSearchIcon()}
            clearIcon={defaultClearIcon()}
            cancelIcon={defaultSearchIcon()}
            round={true}
            value={search}
            onChangeText={this.TextChange}
            input={(input) => { this.setState({ search: input }) }} />
          <ScrollView scrollEnabled={false} horizontal={true}>
            <ScrollTab
              tabs={tabs}
              tabBarActiveTextStyle={{ color: '#419FFF' }}
              tabBarUnderlineStyle={{ backgroundColor: '#419FFF' }}
              onChange={(tab, index) => {
                
                this.setState({
                  tabIndex: index
                })
              }}
            >
              {this._renderTabItem}
            </ScrollTab>
          </ScrollView>
        </View>
      )
    }
  }

  _renderTabItem = (item, index) => {
    const { pageLoading, data_All, data_my } = this.state
    if (!pageLoading) {
      if (item.title == "全部") {
        return (
          <FlatList
            style={{ flex: 1, borderRadius: 10 }}
            ref={(flatList) => this._flatList1 = flatList}
            //ItemSeparatorComponent={this._separator}
            renderItem={this._renderItem}
            onRefresh={this.refreshing}
            refreshing={false}
            //控制上拉加载，两个平台需要区分开
            onEndReachedThreshold={(Platform.OS === 'ios') ? -0.3 : 0.1}
            onEndReached={() => {
              if (data_All.length >= 10) {
                this._onload("all")
              }
            }
            }
            numColumns={1}
            //ListFooterComponent={this._footer}
            //columnWrapperStyle={{borderWidth:2,borderColor:'black',paddingLeft:20}}
            //horizontal={true}
            data={data_All}
            scrollEnabled={true}
            extraData={this.state}
          />
        )
      } else {
        return (
          <FlatList
            style={{ flex: 1, borderRadius: 10 }}
            ref={(flatList) => this._flatList1 = flatList}
            //ItemSeparatorComponent={this._separator}
            renderItem={this._renderItem}
            onRefresh={this.refreshing}
            refreshing={false}
            //控制上拉加载，两个平台需要区分开
            onEndReachedThreshold={(Platform.OS === 'ios') ? -0.3 : 0.1}
            onEndReached={() => {
              if (data_my.length >= 10) {
                this._onload("my")
              }
            }}
            //numColumns={1}
            //ListFooterComponent={this._footer}
            //columnWrapperStyle={{borderWidth:2,borderColor:'black',paddingLeft:20}}
            //horizontal={true}
            data={data_my}
          />
        )
      }

    } else {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
    }
  }

  //接口返回的对象数组，长列表对数据遍历
  _renderItem = ({ item, index }) => {
    let mainNavList2 = ['Wantconcrete', 'ChangeLocation', 'EquipmentRepair', 'EquipmentService']
    if (mainNav == 'ConcretePouring') {
      return (
        <Card containerStyle={{ borderRadius: 10 }}>
          <TouchableOpacity
            onPress={() => {
              let _Rowguid = item._Rowguid.toString()
              this.props.navigation.navigate(mainNav, {
                guid: _Rowguid,
                pageType: 'CheckPage',
                title: sontitle,
              })
            }} >
            <View style={{ flexDirection: 'row', }}>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 1, }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.FormCode}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditName}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text]} numberOfLines={1}> {item.ConcreteGrade}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.ProjectName || ""}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textthi]} numberOfLines={1}> {item.ConcreteType}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditDate}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    } else if (mainNavList2.indexOf(mainNav) != -1) {
      return (
        <Card containerStyle={{ borderRadius: 10 }}>
          <TouchableOpacity
            onPress={() => {
              let guid = item._Rowguid.toString()
              this.props.navigation.navigate(mainNav, {
                guid: guid,
                pageType: 'CheckPage',
                title: sontitle,
              })
            }} >
            <View style={{ flexDirection: 'row', }}>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 1, }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.FormCode}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditName}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text]} numberOfLines={1}> {item.ConcreteGrade}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textthi]} numberOfLines={1}> {item.ConcreteType}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.EditDate}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    } else {
      return (
        <Card containerStyle={{ borderRadius: 10 }}>
          <TouchableOpacity
            onPress={() => {
              let guid = item.guid.toString()
              this.props.navigation.navigate(mainNav, {
                guid: guid,
                pageType: 'CheckPage',
                title: sontitle,
              })
            }} >
            <View style={{ flexDirection: 'row', }}>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 1, }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.compCode}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.PDAuser}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text]} numberOfLines={1}> {item.compTypeName}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.projectName.toString() || ""}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textthi]} numberOfLines={1}> {item.designType}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.time}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    }

  }

  refreshing = () => {
    this.toast.show('刷新中...')
    this.setState({
      pageLoading: true
    })
    const { pageNum } = this.state
    let formData = new FormData();
    let data = {};
    data = {
      "action": "LookUp",
      "servicetype": "pda",
      "express": "FA51025B",
      "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
      "data": {
        "PDAuserid": Url.PDAEmployeeId,
        "subscript": page,
        "strhazy": "",
        "PageNum": pageNum,
        "allormy": "0",
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: MainCheckPage.js ~ line 68 ~ MainCheckPage ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: MainCheckPage.js ~ line 77 ~ MainCheckPage ~ resData", resData)
      if (resData.status == '100') {
        listData = resData.result
        data_All = listData.all
        data_my = listData.my
        this.setState({
          listData: listData,
          data_All: data_All,
          data_my: data_my,
          resData: resData,
          isLoading: false,
          pageLoading: false
        })
      } else {
        this.toast.show('加载失败')
      }
    }).catch((error) => {
    });
  }

  _onload = (user) => {
    /* if (data_my.length < 10 && data_All.length != []) {
      return
    } 
    else  */{
      this.toast.show('加载中...')
      //pageNum += 1
      //pageNum = pageNum.toString()
      let formData = new FormData();
      let data = {};
      data = {
        "action": "LookUp",
        "servicetype": "pda",
        "express": "FA51025B",
        "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
        "data": {
          "PDAuserid": Url.PDAEmployeeId,
          "subscript": page,
          "strhazy": "",
          "PageNum": "1",//pageNum.toString(),
          "allormy": user,
          "factoryId": Url.PDAFid
        }
      }
      formData.append('jsonParam', JSON.stringify(data))
      console.log("🚀 ~ file: MainCheckPage.js ~ line 282 ~ MainCheckPage ~ _onload", formData)
      fetch(Url.PDAurl, {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        body: formData
      }).then(res => {
        return res.json();
      }).then(resData => {
        console.log("🚀 ~ file: MainCheckPage.js ~ line 292 ~ MainCheckPage ~ _onload", resData)

        if (resData.status == '100') {
          let listDataLoad = resData.result
          let data_AllLoad = listDataLoad.all
          let data_myLoad = listDataLoad.my
          if (data_AllLoad.length > 0) {
            data_All.push.apply(data_All, data_AllLoad)
          }
          if (data_myLoad.length > 0) {
            data_my.push.apply(data_my, data_myLoad)
          }
          this.setState({
            listData: listData,
            data_All: data_All,
            data_my: data_my,
            resData: resData,
            isLoading: false,
            pageLoading: false
          })
          this.toast.show('加载完成');
        } else {
          this.toast.show('加载失败')
        }

      }).catch((error) => {
        console.log("🚀 ~ file: MainCheckPage.js ~ line 365 ~ MainCheckPage ~ error", error)
      });
    }

  }

}

const styles = StyleSheet.create({
  loading: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  contrain: {
    flexDirection: 'column',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  txt: {

    fontSize: 16,
    marginBottom: 8,
    marginTop: 8,
    marginHorizontal: 8
  },
  text: {
    fontSize: 14,
    marginBottom: 5,
    marginTop: 5,
    flex: 1
  },
  textright: {
    textAlign: 'right',
    color: 'gray'
  },
  textfir: {
    fontSize: 18
  },
  textthi: {
    fontSize: 15
  },
  textname: {
    fontSize: 14,
    marginBottom: 8,
    marginTop: 8,
    color: 'gray'
  },
  textView: {

    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  hidden: {
    flex: 0,
    height: 0,
    width: 0,
  }
})