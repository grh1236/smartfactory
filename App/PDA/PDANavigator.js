import React from 'react'
import { View, Text, StatusBar, DeviceEventEmitter } from 'react-native';
import { Icon } from 'react-native-elements';
import { createStackNavigator, createSwitchNavigator, createMaterialTopTabNavigator } from 'react-navigation';
import PDAMainView from './PDAMainView';
import PDACheckView from './PDACheckView';
import SteelCageStorage from './ChildPage/steel_cage_storage'
import QualityInspection from './ChildPage/qualityinspection'
import ProductionProcess from './ChildPage/production_process'
import BuriedCard from './ChildPage/buried_card'
import BuriedCardCheck from './ChildPage/buried_card_check'
import ConcretePouring from './ChildPage/concrete_pouring'
import StrippingInspection from './ChildPage/stripping_inspection'
import QualityProductInspection from './ChildPage/quality_product_inspection'
import QualityProductInspectionReview from './ChildPage/quality_product_inspection_review'
import Storage from './ChildPage/storage'
import Loading from './ChildPage/loading'
import LoadingTemData from './ChildPage/loadingTemData'
import ScanLoading from './ChildPage/scan_loading'
import Unload from './ChildPage/unload'
import ReturnGoods from './ChildPage/return_goods';
import EquipmentMaintenance from './ChildPage/equipment_maintenance';
import EquipmentInspection from './ChildPage/equipment_inspection';
import Wantconcrete from './ChildPage/wantconcrete';
import PDAInstall from './ChildPage/install';
import ChangeLocation from "./ChildPage/change_location";
import EquipmentRepair from "./ChildPage/equipment_repair";
import EquipmentService from "./ChildPage/equipment_service";
import InventoryCheck from "./ChildPage/inventory_check";
import AdvanceStorage from "./ChildPage/advancestorage";
import ProductionStart from "./ChildPage/production_start";
import SteelCageDie from "./ChildPage/steel_cage_die";
import HideInspection from "./ChildPage/hideinspection";
import MainCheckPage from "./CheckPage/MainCheckPage";
import SpotCheck from "./ChildPage/spotcheck";
import Camera from './Componment/Camera';
import Web from "./ChildPage/Web";
import MineView from "../MineView/MineView";
import PDACamera from './Componment/PDACamera'
import SafeManageNotice from './ChildPage/SafeManageNotice'
import SafeManageReceipt from './ChildPage/SafeManageReceipt'
import PostRiskInspection from './ChildPage/PostRiskInspection'
import SampleManage from './ChildPage/SampleManage'
import SelectMaterial from './ChildPage/SelectMaterial'
import MaterialProjectPicking from './ChildPage/MaterialProjectPicking'
import MaterialPickingInfo from './ChildPage/MaterialPickingInfo'
import MaterialOutStorage from './ChildPage/MaterialOutStorage'
import MaterialOutStorageReturn from './ChildPage/MaterialOutStorageReturn'
import MaterialInventory from './ChildPage/MaterialInventory'
import MaterialStock from './ChildPage/MaterialStock'
import MaterialInStorage from './ChildPage/MaterialInStorage'
import SelectSupplier from './ChildPage/SelectSupplier'
import MaterialCheckPage from './ChildPage/MaterialCheckPage'
import CompMaintain from './ChildPage/compmaintain'
import CompRepair from './ChildPage/comprepair'
import QualityScrap from './ChildPage/QualityScrap'
import Inveck from './ChildPage/inveck'
import CompInveck from './ChildPage/comp_inveck'
import HandlingToolBaskets from './ChildPage/handlingToolBaskets'
import WaterPoolPH from './ChildPage/WaterPoolPH';
import WaterPoolDetail from './ChildPage/WaterPoolDetail';


import { deviceWidth, RFT } from '../Url/Pixal';
import { NativeModules } from 'react-native';
import Toast from 'react-native-easy-toast';
import ScanButton from './Componment/ScanButton';
import Url from '../Url/Url';

class Title extends React.Component {
  constructor() {
    super()
    this.state = {
      isRFIDMoudle: false
    }
  }

  componentDidMount() {
    NativeModules.RFIDModule.isPowerOn();

    //通过使用DeviceEventEmitter模块来监听事件
    this.isPowerOn = DeviceEventEmitter.addListener('isPowerOn', (Event) => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 115 ~ SteelCage ~ Event.isPowerOn", Event.isPowerOn)
      if (typeof Event.isPowerOn != undefined) {
        let isPowerOn = Event.isPowerOn
        console.log("🚀 ~ file: buried_card.js:78 ~ SteelCage ~ this.isPowerOn=DeviceEventEmitter.addListener ~ isPowerOn:", isPowerOn)
        if (isPowerOn == "true") {
          this.setState({ isRFIDMoudle: true })
          this.toast.show('RFID模块初始化成功')
        } else {
          this.setState({ isRFIDMoudle: false })
          //this.toast.show('模块初始化失败')
        }
      }
    });
    this.iRFIDRdataListener = DeviceEventEmitter.addListener('iRdata', (Event) => {
      console.log("🚀 ~ file: buried_card.js:89 ~ SteelCage ~ this.iDataRFID=DeviceEventEmitter.addListener ~ Event:", Event)
      if (typeof Event.iRdataResult != "undefined") {
        let iRdataResult = Event.iRdataResult
        console.log("🚀 ~ file: buried_card.js:96 ~ SteelCage ~ this.iRFIDRdataListener=DeviceEventEmitter.addListener ~ iRdataResult:", iRdataResult)
        if (iRdataResult == null) {
          this.toast.show("读取失败", 1000)
          Url.iRdataResult = "读取失败"
          //Alert.alert('错误提示', "读取失败")
        } else {
          Url.iRdataResult = iRdataResult
          return iRdataResult

          // this.props.navigation.navigate('Web1', {
          //   title: '构件信息',
          //   url: "http://auth.smart.pkpm.cn/QRPageFrame.aspx?sitecode=" + Url.ID + "&qrbiz=/PCIS/Production/ShowCompInfo.aspx?compid=" + iRdataResult
          // })
        }
      } else {
        Url.iRdataResult = "读取失败"
      }
    })
  }

  componentWillUnmount() {
    this.isPowerOn.remove();
    this.iRFIDRdataListener.remove();
  }

  render() {
    const { isRFIDMoudle } = this.state
    return (
      <View style={{ width: deviceWidth }}>
        <Toast ref={(ref) => { this.toast = ref; }} position="center" />
        <View style={{ flex: 1, left: deviceWidth * 0.1 }}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 1 }}>
              <Text style={{ marginLeft: 10, marginTop: 15, fontSize: 18, color: '#333', fontWeight: 'bold', textAlign: isRFIDMoudle ? 'center' : "right" }} >{'随身工厂'}</Text>
            </View>
            <View style={
              this.state.isRFIDMoudle ?
                { flex: 1, marginTop: 10, left: -(RFT * 4), flexDirection: "row", justifyContent: "center" } :
                { flex: 1, marginTop: 10, left: (RFT * 7) }
            }>
              {
                isRFIDMoudle && <ScanButton
                  title='构件详情 '
                  iconRight={true}
                  icon={<Icon type='antdesign' name='wifi' color='white' size={14} />}
                  onPress={async () => {
                    await NativeModules.RFIDModule.readTag()
                    setTimeout(() => {
                      if (Url.iRdataResult != "读取失败") {
                        this.props.navigation.navigate('Web1', {
                          title: '构件信息',
                          url: "http://auth.smart.pkpm.cn/QRPageFrame.aspx?sitecode=" + Url.ID + "&qrbiz=/PCIS/Production/ShowCompInfo.aspx?compid=" + Url.iRdataResult
                        })
                      }
                    }, 100)
                  }}
                />

              }
              {
                isRFIDMoudle &&
                <View style={{ width: 5 }}></View>
              }
              <Icon
                onPress={() => {
                  this.props.navigation.navigate('Camera1', {
                    mainpage: "首页",
                    title: '构件扫码'
                  })
                }}
                iconStyle={{ marginTop: 5 }}
                type='ant-design'
                name='scan1'
                size={24}
                color='#333' />
            </View>

          </View>
        </View >
      </View>
    )
  }
}

//Tab.router = PDAStack.router

const PDATopBottom = createMaterialTopTabNavigator({
  扫码: {
    screen: PDAMainView,
  },
  查询: {
    screen: PDACheckView,
  },
  /* 我的: {
    screen: MineView
  } */
}, {

  animationEnabled: true,
  backBehavior: true,
  headerTransparent: true,
  navigationOptions: {
    //headerTransparent: true,
    /* headerStyle: {
      backgroundColor: 'rgba(255,255,255,0.5)',
      //marginTop: 50,
      //height: 50
    } */
  },
  tabBarOptions: {
    activeTintColor: '#419FFF',
    tabStyle: {
      width: deviceWidth * 0.3,
      //paddingHorizontal: deviceWidth * 0.13,
      boxShadow: 'none',
      //backgroundColor: 'rgba(255,255,255,0.5)',
    },
    labelStyle: {
      backgroundColor: 'transparent',
      width: deviceWidth * 0.3,
    },
    style: {
      backgroundColor: 'white',
      width: deviceWidth * 0.6,
      marginLeft: deviceWidth * 0.2,
      borderBottomWidth: 0.5,
      borderBottomColor: '#eee',
      elevation: 0, //去掉阴影,
      height: deviceWidth <= 330 ? 45 : 50,
      //height: 50
    },
    allowFontScaling: false,
    inactiveTintColor: 'black',
    indicatorStyle: {
      backgroundColor: '#419FFF',
      width: deviceWidth * 0.11,
      marginLeft: deviceWidth * 0.1,
      //paddingHorizontal: deviceWidth * 0.13,
    },
  },
})

const PDAStack = createStackNavigator({
  PDAMainStack: {
    screen: PDATopBottom,
    navigationOptions: props => {
      return {
        headerTitle: <Title {...props} />,
      }

    }
  },
  PDAMainView: {
    screen: PDAMainView,
  },
  PDACheckView: {
    screen: PDACheckView,
  },
  SteelCageStorage: {
    screen: SteelCageStorage,
  },
  QualityInspection: {
    screen: QualityInspection
  },
  BuriedCard: {
    screen: BuriedCard
  },
  BuriedCardCheck: BuriedCardCheck,
  ConcretePouring: ConcretePouring,
  StrippingInspection: StrippingInspection,
  QualityProductInspection: {
    screen: QualityProductInspection
  },
  QualityProductInspectionReview: {
    screen: QualityProductInspectionReview
  },
  PDAStorage: {
    screen: Storage
  },
  PDASacnLoading: ScanLoading,
  SpotCheck: SpotCheck,
  PDALoading: Loading,
  PDALoadingTemData: LoadingTemData,
  AdvanceStorage: AdvanceStorage,
  Unload: Unload,
  PDAInstall: PDAInstall,
  ReturnGoods: ReturnGoods,
  ProductionProcess: ProductionProcess,
  EquipmentMaintenance: EquipmentMaintenance,
  EquipmentInspection: EquipmentInspection,
  Wantconcrete: Wantconcrete,
  InventoryCheck: InventoryCheck,
  ChangeLocation: ChangeLocation,
  EquipmentRepair: EquipmentRepair,
  EquipmentService: EquipmentService,
  ProductionStart: ProductionStart,
  SteelCageDie: SteelCageDie,
  HideInspection: HideInspection,
  MainCheckPage: MainCheckPage,
  Camera1: Camera,
  SafeManageNotice: SafeManageNotice,
  SafeManageReceipt: SafeManageReceipt,
  PostRiskInspection: PostRiskInspection,
  SampleManage: SampleManage,
  SelectMaterial: SelectMaterial,
  MaterialProjectPicking: MaterialProjectPicking,
  MaterialPickingInfo: MaterialPickingInfo,
  MaterialOutStorage: MaterialOutStorage,
  MaterialOutStorageReturn: MaterialOutStorageReturn,
  MaterialInventory: MaterialInventory,
  MaterialStock: MaterialStock,
  MaterialInStorage: MaterialInStorage,
  SelectSupplier: SelectSupplier,
  MaterialCheckPage: MaterialCheckPage,
  QualityScrap: QualityScrap,
  CompMaintain: CompMaintain,
  CompRepair: CompRepair,
  Inveck: Inveck,
  CompInveck: CompInveck,
  HandlingToolBaskets: HandlingToolBaskets,
  WaterPoolPH: WaterPoolPH,
  WaterPoolDetail: WaterPoolDetail,
  Web1: {
    screen: Web,
  },
  QRCode: {
    screen: PDACamera
  }
}, {
  navigationOptions: ({
    navigation
  }) => ({
    tabBarVisible: navigation.state.index > 0 ? false : true
  }),
  defaultNavigationOptions: {
    headerStyle: {
      backgroundColor: 'white',
      height: 50
    },
    headerTitleStyle: { color: 'black' },
    headerBackTitleStyle: { color: 'black' },
    headerBackImage: <Icon type='antdesign' color='black' name='arrowleft' size={22} />
  }
})

if (deviceWidth <= 330) {
  PDAStack.defaultNavigationOptions = {
    headerStyle: {
      backgroundColor: 'white',
      height: 50
    },
    headerTitleStyle: { color: 'black' },
    headerBackTitleStyle: { color: 'black' },
    headerBackImage: <Icon type='antdesign' color='black' name='arrowleft' size={20} />
  }
}


export default PDAStack