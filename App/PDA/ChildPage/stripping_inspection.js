import React from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, ListItem, BottomSheet, CheckBox, Overlay, Header, Avatar } from 'react-native-elements';
import Url from '../../Url/Url';
import Toast from 'react-native-easy-toast';
import { ToDay, YesterDay, BeforeYesterDay } from '../../CommonComponents/Data';
import { RFT } from '../../Url/Pixal';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import CardList from "../Componment/CardList";
import { ScrollView } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker'
import { NavigationEvents } from 'react-navigation';
import { styles } from '../Componment/PDAStyles';
import ScanButton from '../Componment/ScanButton';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import ListItemScan_child from '../Componment/ListItemScan_child';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import CheckBoxScan from '../Componment/CheckBoxScan';
import AvatarAdd from '../Componment/AvatarAdd';
import AvatarImg from '../Componment/AvatarImg';
import { DeviceEventEmitter, NativeModules } from 'react-native';
import { DURATION } from 'react-native-easy-toast';
import overlayImg from '../Componment/OverlayImg';
import OverlayImgZoomPicker from '../Componment/OverlayImgZoomPicker';
let varGetError = false

let fileflag = "14"

let imageArr = [], imageFileArr = [];

class BackIcon extends React.PureComponent {
  render() {
    return (
      <TouchableCustom
        onPress={this.props.onPress} >
        <Icon
          name='arrow-back'
          color='#419FFF' />
      </TouchableCustom>
    )
  }
}

const { height, width } = Dimensions.get('window') //获取宽高

export default class SteelCage extends React.Component {
  constructor() {
    super();
    this.state = {
      buttondisable: false,
      bottomData: [],
      bottomVisible: false,
      resData: [],
      type: 'compid',
      rowguid: '',
      ServerTime: '',
      steelCageIDs: '',
      compData: {},
      compId: '',
      compCode: '',
      productLineName: '',
      teamName: '',
      Operator: "",
      imageArr: [],
      imageFileArr: [],
      imageOverSize: false,
      pictureSelect: false,
      pictureUri: '',
      fileid: "",
      recid: "",
      isPhotoUpdate: false,
      remark: '',
      checked: true,
      isGetcomID: false,
      iscomIDdelete: false,
      buttondisable: false,
      GetError: false,
      focusIndex: 0,
      isLoading: true,
      //控制app拍照，日期，相册选择功能
      isAppPhotoSetting: false,
      currShowImgIndex: 0,
      isAppDateSetting: false,
      isAppPhotoAlbumSetting: false
    }
    //this.requestCameraPermission = this.requestCameraPermission.bind(this)
  }

  componentDidMount() {
    const { navigation } = this.props;
    let guid = navigation.getParam('guid') || ''
    let pageType = navigation.getParam('pageType') || ''
    const QRid = navigation.getParam('QRid') || ''
    const type = navigation.getParam('type') || '';
    let isAppPhotoSetting = navigation.getParam('isAppPhotoSetting')
    let isAppDateSetting = navigation.getParam('isAppDateSetting')
    let isAppPhotoAlbumSetting = navigation.getParam('isAppPhotoAlbumSetting')
    this.setState({
      isAppPhotoSetting: isAppPhotoSetting,
      isAppDateSetting: isAppDateSetting,
      isAppPhotoAlbumSetting: isAppPhotoAlbumSetting
    })
    if (pageType == 'CheckPage') {
      this.checkResData(guid)
    } else {
      this.resData()
    }
    if (type != '') {
      this.comID(QRid)
    }

    //通过使用DeviceEventEmitter模块来监听事件
    this.iDataScan = DeviceEventEmitter.addListener('iDataScan', (Event) => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 115 ~ SteelCage ~ Event.ScanResult", Event.ScanResult)
      if (typeof Event.ScanResult != undefined) {
        let data = Event.ScanResult
        let arr = data.split("=");
        let id = ''
        id = arr[arr.length - 1]
        console.log("🚀 ~ file: concrete_pouring.js ~ line 118 ~ SteelCage ~ id", id)
        if (this.state.type == 'compid') {
          this.GetcomID(id)
        }
      }
    });
  }

  UpdateControl = () => {
    if (typeof this.props.navigation.getParam('QRid') != "undefined") {
      if (this.props.navigation.getParam('QRid') != "") {
        this.toast.show(Url.isLoadingView, 0)
      }
    }
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || ''
    const type = navigation.getParam('type') || '';

    {
      if (type == 'compid') {
        {
          this.GetcomID(QRid)
        }
      }
    }

  }

  componentWillUnmount() {
    imageArr = [], imageFileArr = [];
    this.iDataScan.remove()
  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  checkResData = (guid) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "ObtainDemold",
      "servicetype": "pda",
      "express": "8AC4C0B0",
      "ciphertext": "7df83f712027c63f147f6c2d4a43f08d",
      "data": {
        "guid": guid,
        "factoryId": Url.PDAFid,
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: stripping_inspection.js ~ line 111 ~ SteelCage ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: stripping_inspection.js ~ line 144 ~ SteelCage ~ resData", resData)
      let rowguid = resData.result.guid
      let ServerTime = resData.result.time
      let compCode = resData.result.compCode
      let compId = resData.result.compId
      let Operator = resData.result.Operator
      let remark = resData.result.remark
      let componentImageUrl = resData.result.componentImageUrl
      let imageArr = [], imageFileArr = [];
      let tmp = {}
      tmp.fileid = ""
      tmp.uri = componentImageUrl
      tmp.url = componentImageUrl
      imageFileArr.push(tmp)
      this.setState({
        imageFileArr: imageFileArr
      })
      imageArr.push(componentImageUrl)
      //产品子表
      let compData = {}, result = {}
      result.projectName = resData.result.projectName
      result.compTypeName = resData.result.compTypeName
      result.designType = resData.result.designType
      result.floorNoName = resData.result.floorNoName
      result.floorName = resData.result.floorName
      result.volume = resData.result.volume
      result.weight = resData.result.weight
      compData.result = result
      this.setState({
        resData: resData,
        rowguid: rowguid,
        ServerTime: ServerTime,
        compCode: compCode,
        compId: compId,
        Operator: Operator,
        compData: compData,
        remark: remark,
        imageArr: imageArr,
        imageFileArr: imageFileArr,
        pictureSelect: true,
        isGetcomID: true,
        isCheck: true
      })
      this.setState({
        isLoading: false,
      })
    }).catch((error) => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 215 ~ QualityInspection ~ error", error)
    });
  }

  resData = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "init",
      "servicetype": "pda",
      "express": "26576DAA",
      "ciphertext": "39e7235e54a23c7a9c68b39582cbfd66"
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: steel_cage_storage.js ~ line 35 ~ SteelCage ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      //初始化错误提示
      if (resData.status == 100) {
        let rowguid = resData.result.rowguid
        let ServerTime = resData.result.serverTime
        this.setState({
          resData: resData,
          rowguid: rowguid,
          ServerTime: ServerTime,
          Operator: Url.PDAEmployeeName,
          isLoading: false
        })
      }
      else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }

    }).catch((error) => {
      console.log("🚀 ~ file: steel_cage_storage.js ~ line 75 ~ SteelCage ~ error", error)
    });

  }

  PostData = () => {
    const { compId, ServerTime, rowguid, remark, imageArr, recid } = this.state
    const { navigation } = this.props;
    const QRid = navigation.getParam('QRid') || '';
    if (compId == '') {
      this.toast.show('产品编号不能为空', 0);
      return
    }
    if (this.state.isAppPhotoSetting) {
      if (imageArr.length == 0) {
        this.toast.show('请先拍摄构件照片');
        return
      }
    }


    let formData = new FormData();
    let data = {};
    data = {
      "action": "SaveDemold",
      "servicetype": "pda",
      "express": "36D458E9",
      "ciphertext": "b162f41607481b65b5b54c502ab27edb",
      "data": {
        "Operator": Url.PDAEmployeeName,
        "compId": compId,
        "demouldTime": ServerTime,
        "factoryId": Url.PDAFid,
        "userName": Url.PDAusername,
        "operatorId": Url.PDAEmployeeId,
        "rowguid": rowguid,
        "remark": remark,
        "recid": recid
      }
    }

    this.toast.show(saveLoading, 0)

    console.log("🚀 ~ file: steel_cage_storage.js ~ line 191 ~ SteelCage ~ data", data)
    formData.append('jsonParam', JSON.stringify(data))
    if (!Url.isAppNewUpload) {
      imageArr.map((item, index) => {
        formData.append('img_' + index, {
          uri: item,
          type: 'image/jpeg',
          name: 'img_' + index
        })
      })
    }
    console.log("🚀 ~ file: steel_cage_storage.js ~ line 193 ~ SteelCage ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('保存成功');
        setTimeout(() => {
          this.props.navigation.replace(this.props.navigation.getParam("page"), {
            title: this.props.navigation.getParam("title"),
            pageType: this.props.navigation.getParam("pageType"),
            page: this.props.navigation.getParam("page"),
            isAppPhotoSetting: this.props.navigation.getParam("isAppPhotoSetting"),
            isAppDateSetting: this.props.navigation.getParam("isAppDateSetting"),
            isAppPhotoAlbumSetting: this.props.navigation.getParam("isAppPhotoAlbumSetting"),
          })
          //this.props.navigation.navigate('PDAMainStack')
        }, 400)
      } else {
        this.toast.show('保存失败')
        Alert.alert('保存失败', resData.message)
      }
      console.log("🚀 ~ file: steel_cage_storage.js ~ line 195 ~ SteelCage ~ resData", resData)
    }).catch((error) => {
      this.toast.show('保存失败')
      if (error.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('保存失败', "响应内容不是合法JSON格式")
        return
      }
      if (error.toString().indexOf('Network request faile') != -1) {
        Alert.alert('保存失败', "网络请求错误")
        return
      }
      Alert.alert('保存失败', error.toString())
      console.log("🚀 ~ file: steel_cage_storage.js ~ line 75 ~ SteelCage ~ error", error)
    });
  }

  DeleteData = () => {
    const { navigation } = this.props;
    let guid = navigation.getParam('guid') || ''
    console.log('DeleteData')
    let formData = new FormData();
    let data = {};
    data = {
      "action": "ReviseDemold",
      "servicetype": "pda",
      "express": "3DF00769",
      "ciphertext": "b56f765f10171e4492d741c7baee7783",
      "data": {
        "demouldTime": this.state.ServerTime,
        "guid": guid,
        "factoryId": Url.PDAFid
      }
    }

    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('删除成功');
        this.props.navigation.navigate('MainCheckPage')
      } else {
        Alert.alert(
          '删除失败',
          resData.message,
          [{
            text: '取消',
            style: 'cancel'
          }, {
            text: '返回上一级',
            onPress: () => {
              this.props.navigation.goBack()
            }
          }])
        console.log('resData', resData)
      }
    }).catch((error) => {
    });

  }

  GetcomID = (id) => {
    let compCode = '111'
    let formData = new FormData();
    let data = {};
    data = {
      "action": "GetDemoldInfo",
      "servicetype": "pda",
      "express": "2934F95F",
      "ciphertext": "1bb0c4f99cbd1d1d56267152cb954ab6",
      "data": {
        "compId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        compCode = resData.result.compCode
        let compdata = resData.result
        let compId = resData.result.compId
        let productLineName = resData.result.productLineName
        let teamName = resData.result.teamName
        this.setState({
          compData: resData,
          compCode: compCode,
          compId: compId,
          isGetcomID: true,
          iscomIDdelete: false
        })
      } else {
        console.log('resData', resData)
        Alert.alert('ERROR', resData.message)
        varGetError = true
        this.setState({
          GetError: true
        })
      }

    }).catch(err => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 210 ~ SteelCage ~ err", err)
    })
  }

  _DatePicker = () => {
    const { ServerTime } = this.state
    return (
      <DatePicker
        customStyles={{
          dateInput: styles.dateInput,
          dateTouchBody: styles.dateTouchBody,
          dateText: this.state.isAppDateSetting ? styles.dateText : styles.dateDisabled,
        }}
        onOpenModal={() => { this.setState({ focusIndex: 1 }) }}
        iconComponent={<Icon name='caretdown' color={this.state.isAppDateSetting ? '#419fff' : "#666"} iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
        }
        showIcon={true}
        mode='datetime'
        date={ServerTime}
        format="YYYY-MM-DD HH:mm"
        disabled={!this.state.isAppDateSetting}
        onDateChange={(value) => {
          this.setState({
            ServerTime: value
          })
        }}
      />
    )
  }

  cameraFunc = () => {
    launchCamera(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: parseInt(Url.isResizePhoto),
        maxWidth: parseInt(Url.isResizePhoto),
        saveToPhotos: true,

      },
      (response) => {
        if (response.uri == undefined) {
          return
        }
        imageArr.push(response.uri)
        this.setState({
          //pictureSelect: true,
          pictureUri: response.uri,
          imageArr: imageArr
        })
        if (Url.isAppNewUpload) {
          this.cameraPost(response.uri)
        } else {
          this.setState({
            pictureSelect: true,
          })
        }
        console.log(response)
      },
    )
  }

  camLibraryFunc = () => {
    launchImageLibrary(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: parseInt(Url.isResizePhoto),
        maxWidth: parseInt(Url.isResizePhoto),
      },
      (response) => {
        if (response.uri == undefined) {
          return
        }
        imageArr.push(response.uri)
        this.setState({

          pictureUri: response.uri,
          imageArr: imageArr
        })
        if (Url.isAppNewUpload) {
          this.cameraPost(response.uri)
        } else {
          this.setState({
            pictureSelect: true,
          })
        }
        console.log(response)
      },
    )
  }

  camandlibFunc = () => {
    Alert.alert(
      '提示信息',
      '选择拍照方式',
      [{
        text: '取消',
        style: 'cancel'
      }, {
        text: '拍照',
        onPress: () => {
          this.cameraFunc()
        }
      }, {
        text: '相册',
        onPress: () => {
          this.camLibraryFunc()
        }
      }])
  }

  // 展示/隐藏 放大图片
  handleZoomPicture = (flag, index) => {
    this.setState({
      imageOverSize: false,
      currShowImgIndex: index || 0
    })
  }

  // 加载放大图片弹窗
  renderZoomPicture = () => {
    const { imageOverSize, currShowImgIndex, imageFileArr } = this.state
    return (
      <OverlayImgZoomPicker
        isShowImage={imageOverSize}
        currShowImgIndex={currShowImgIndex}
        zoomImages={imageFileArr}
        callBack={(flag) => this.handleZoomPicture(flag)}
      ></OverlayImgZoomPicker>
    )
  }

  cameraPost = (uri) => {
    const { recid } = this.state
    this.setState({
      isPhotoUpdate: true
    })
    this.toast.show('图片上传中', 2000)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "savephote",
      "servicetype": "photo_file",
      "express": "D2979AB2",
      "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
      "data": {
        "fileflag": fileflag,
        "username": Url.PDAusername,
        "recid": recid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    formData.append('img', {
      uri: uri,
      type: 'image/jpeg',
      name: 'img'
    })
    console.log("🚀 ~ file: concrete_pouring.js ~ line 672 ~ SteelCage ~ formData", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 690 ~ SteelCage ~ resData", resData)
      this.toast.close()
      if (resData.status == '100') {

        let fileid = resData.result.fileid;
        let recid = resData.result.recid;

        let tmp = {}
        tmp.fileid = fileid
        tmp.uri = uri
        tmp.url = uri
        imageFileArr.push(tmp)
        this.setState({
          fileid: fileid,
          recid: recid,
          pictureSelect: true,
          imageFileArr: imageFileArr
        })
        console.log("🚀 ~ file: concrete_pouring.js ~ line 704 ~ SteelCage ~ imageFileArr", imageFileArr)
        this.toast.show('图片上传成功');
        this.setState({
          isPhotoUpdate: false
        })
      } else {
        Alert.alert('图片上传失败', resData.message)
        this.toast.close(1)
      }
    }).catch((error) => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 692 ~ SteelCage ~ error", error)
    });
  }

  cameraDelete = (item) => {
    let i = imageArr.indexOf(item)

    if (Url.isAppNewUpload) {
      let formData = new FormData();
      let data = {};
      data = {
        "action": "deletephote",
        "servicetype": "photo_file",
        "express": "D2979AB2",
        "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
        "data": {
          "fileid": imageFileArr[i].fileid,
        }
      }
      formData.append('jsonParam', JSON.stringify(data))
      fetch(Url.PDAurl, {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        body: formData
      }).then(res => {
        console.log(res.statusText);
        console.log(res);
        return res.json();
      }).then(resData => {
        if (resData.status == '100') {
          if (i > -1) {
            imageArr.splice(i, 1)
            imageFileArr.splice(i, 1)
            this.setState({
              imageArr: imageArr,
              imageFileArr: imageFileArr
            }, () => {
              this.forceUpdate()
            })
          }
          if (imageArr.length == 0) {
            this.setState({
              pictureSelect: false
            })
          }
          this.toast.show('图片删除成功');
        } else {
          Alert.alert('图片删除失败', resData.message)
          this.toast.close(1)
        }
      }).catch((error) => {
        console.log("🚀 ~ file: concrete_pouring.js ~ line 761 ~ SteelCage ~ cameraDelete ~ error", error)
      });
    } else {
      if (i > -1) {
        imageArr.splice(i, 1)
        this.setState({
          imageArr: imageArr,
        }, () => {
          this.forceUpdate()
        })
      }
      if (imageArr.length == 0) {
        this.setState({
          pictureSelect: false
        })
      }
    }
  }

  imageView = () => {
    return (
      <View style={{ flexDirection: 'row' }}>
        {
          this.state.pictureSelect ?
            this.state.imageArr.map((item, index) => {
              return (
                <AvatarImg
                  item={item}
                  index={index}
                  onPress={() => {
                    this.setState({
                      imageOverSize: true,
                      pictureUri: item
                    })
                  }}
                  onLongPress={() => {
                    Alert.alert(
                      '提示信息',
                      '是否删除构件照片',
                      [{
                        text: '取消',
                        style: 'cancel'
                      }, {
                        text: '删除',
                        onPress: () => {
                          this.cameraDelete(item)
                        }
                      }])
                  }} />
              )
            }) : <View></View>
        }
      </View>
    )
  }

  comIDItem = () => {
    const { isGetcomID, buttondisable, compCode } = this.state
    return (
      <View>
        {
          !isGetcomID ?
            <View>
              <ScanButton

                title='扫码'
                type='solid'
                buttonStyle={{ height: 24 }}
                onPress={() => {
                  this.setState({
                    iscomIDdelete: false,
                    buttondisable: true,
                    focusIndex: 0
                  })
                  this.props.navigation.navigate('QRCode', {
                    type: 'compid',
                    page: 'StrippingInspection'
                  })
                }}
                onLongPress={() => {
                  this.setState({
                    type: 'compid',
                    focusIndex: 0
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}
              />
            </View>
            :
            <TouchableOpacity
              onPress={() => {
                console.log('onPress')
              }}
              onLongPress={() => {
                console.log('onLongPress')
                Alert.alert(
                  '提示信息',
                  '是否删除构件信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      this.setState({
                        compData: [],
                        compCode: '',
                        compId: '',
                        iscomIDdelete: true,
                        isGetcomID: false,
                      })
                    }
                  }])
              }} >
              <Text>{compCode}</Text>
            </TouchableOpacity>
        }
      </View>

    )
  }

  render() {
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const QRid = navigation.getParam('QRid') || ''
    const type = navigation.getParam('type') || '';
    let pageType = navigation.getParam('pageType') || ''

    if (type == 'compid') {
      QRcompid = QRid
    }

    const { resData, isGetcomID, focusIndex, compData, compCode, bottomVisible, bottomData, Operator, remark, pictureSelect, pictureUri } = this.state

    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Toast ref={(ref) => { this.toast = ref }} position="center" />
          <NavigationEvents
            onWillFocus={this.UpdateControl} />
          <ScrollView ref={ref => this.scoll = ref} style={styles.mainView} showsVerticalScrollIndicator={false} >
            <View style={styles.listView}>
              <ListItem
                containerStyle={{ paddingBottom: 6 }}
                leftAvatar={
                  <View >
                    <View style={{ flexDirection: 'column' }} >
                      {
                        pageType == 'CheckPage' ? <Avatar size="large"></Avatar> :
                          <AvatarAdd
                            pictureSelect={this.state.pictureSelect}
                            backgroundColor='rgba(77,142,245,0.20)'
                            color='#4D8EF5'
                            title="构"
                            onPress={() => {
                              if (this.state.isAppPhotoAlbumSetting) {
                                this.camandlibFunc()
                              } else {
                                this.cameraFunc()
                              }
                            }} />
                      }
                      {this.imageView()}
                    </View>
                  </View>
                }
              />
              <ListItemScan
                isButton={!isGetcomID}
                focusStyle={focusIndex == '0' ? styles.focusColor : {}}
                title='产品编号'
                onPressIn={() => {
                  this.setState({
                    type: 'compid',
                    focusIndex: 0
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPress={() => {
                  this.setState({
                    type: 'compid',
                    focusIndex: 0
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}
                rightElement={
                  this.comIDItem()
                }
              />
              {
                compCode != '' ?
                  <CardList compData={compData} /> : <View></View>
              }
              <ListItemScan
                focusStyle={focusIndex == '1' ? styles.focusColor : {}}
                title='脱模日期'
                rightElement={this._DatePicker()}
              />
              <ListItemScan
                title='操作人'
                rightTitle={Operator}
              />
              <ListItemScan
                focusStyle={focusIndex == '4' ? styles.focusColor : {}}
                title='备注'
                rightElement={
                  <View>
                    <Input
                      containerStyle={styles.quality_input_container}
                      inputContainerStyle={styles.inputContainerStyle}
                      inputStyle={[styles.quality_input_, { top: 7 }]}
                      placeholder='请输入'
                      value={remark}
                      onChangeText={(value) => {
                        this.setState({
                          remark: value
                        })
                      }} />
                  </View>
                }
              />
            </View>

            <FormButton
              backgroundColor='#17BC29'
              onPress={() => {
                if (pageType == 'CheckPage') {
                  this.DeleteData()
                } else {
                  this.PostData()
                }
              }}
              disabled={this.state.isPhotoUpdate}
              title='保存'
            />

            {this.renderZoomPicture()}

            {/* {overlayImg(obj = {
              onRequestClose: () => {
                this.setState({ imageOverSize: !this.state.imageOverSize }, () => {
                  console.log("🚀 ~ file: equipment_maintenance.js:1696 ~ render ~ imageOverSize:", this.state.imageOverSize)
                })
              },
              onPress: () => {
                this.setState({
                  imageOverSize: !this.state.imageOverSize
                })
              },
              uri: this.state.pictureUri,
              isVisible: this.state.imageOverSize
            })
            }*/}

            <BottomSheet
              isVisible={bottomVisible}
              onRequestClose={() => {
                this.setState({
                  bottomVisible: false
                })
              }}
              onBackdropPress={() => {
                this.setState({
                  bottomVisible: false
                })
              }}

            >
              {
                bottomData.map((item, index) => {
                  return (
                    <TouchableOpacity
                      onPress={() => {
                        if (item.roomId) {
                          this.setState({
                            roomId: item.roomId,
                            roomselect: item.roomName
                          })
                        } else if (item.teamId) {
                          this.setState({
                            teamId: item.teamId,
                            teamselect: item.teamName
                          })
                        } else {
                          this.setState({
                            keeperId: item.keeperId,
                            keeperselect: item.keeperName
                          })
                        }
                        this.setState({
                          bottomVisible: false
                        })
                      }}
                    >
                      <ListItem
                        title={item.roomName ? item.roomName : (item.teamName ? item.teamName : item.keeperName)}
                        //titleStyle={styles.text}
                        //contentContainerStyle={styles.content}
                        // containerStyle={styles.container}
                        bottomDivider
                      />
                    </TouchableOpacity>
                  )

                })
              }
            </BottomSheet>
          </ScrollView>
        </View>
      )
    }
  }

}