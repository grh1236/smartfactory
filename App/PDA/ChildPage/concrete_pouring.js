import React, { Component } from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert, Keyboard } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, ListItem, BottomSheet, CheckBox, Overlay, Header } from 'react-native-elements';
import Url from '../../Url/Url';
import Toast from 'react-native-easy-toast';
import { ToDay, YesterDay, BeforeYesterDay } from '../../CommonComponents/Data';
import { RFT } from '../../Url/Pixal';
import CardList from "../Componment/CardList";
import DatePicker from 'react-native-datepicker'
import { ScrollView } from 'react-native-gesture-handler';
import { NavigationEvents } from 'react-navigation';
import { styles } from '../Componment/PDAStyles';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import ScanButton from '../Componment/ScanButton';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import ListItemScan_child from '../Componment/ListItemScan_child';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import BatchBottomItem from '../Componment/BatchBottomItem';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import CheckBoxScan from '../Componment/CheckBoxScan';
import AvatarAdd from '../Componment/AvatarAdd';
import AvatarImg from '../Componment/AvatarImg';
import reactNativeKeyevent from 'react-native-keyevent';

import { DeviceEventEmitter, NativeModules } from 'react-native';
import overlayImg from '../Componment/OverlayImg';
import OverlayImgZoomPicker from '../Componment/OverlayImgZoomPicker';
const { height, width } = Dimensions.get('window') //获取宽高

var varGetError = false

let fileflag = "2"

let imageArr = [], imageFileArr = [];

let id = ''

class BackIcon extends React.PureComponent {
  render() {
    return (
      <TouchableCustom
        onPress={this.props.onPress} >
        <Icon
          name='arrow-back'
          color='#419FFF' />
      </TouchableCustom>
    )
  }
}
export default class SteelCage extends React.Component {
  constructor() {
    super();
    this.state = {
      buttondisable: false,
      bottomData: [],
      bottomVisible: false,
      resData: [],
      rowguid: '',
      ServerTime: '',
      type: 'compid',
      compData: '',
      compId: '',
      compCode: '',
      productLineName: '',
      teamName: '',
      checked: true,
      isGetcomID: false,
      iscomIDdelete: false,
      GetError: false,
      isLoading: true,
      isCheckPage: false,
      //图片
      imageArr: [],
      imageFileArr: [],
      pictureSelect: false,
      imageOverSize: false,
      pictureUri: '',
      fileid: "",
      recid: "",
      isPhotoUpdate: false,
      remark: '',
      focusIndex: 0,
      //批量
      isBatch: false,
      listBatchNoInfo: [],
      batchCompCode: '',
      batchCompCodeselect: '请选择',
      //控制app拍照，日期，相册选择功能
      isAppPhotoSetting: false,
      currShowImgIndex: 0,
      isAppDateSetting: false,
      isAppPhotoAlbumSetting: false
    }
    //this.requestCameraPermission = this.requestCameraPermission.bind(this)
  }

  componentDidMount() {
    const { navigation } = this.props;
    let guid = navigation.getParam('guid') || ''
    let pageType = navigation.getParam('pageType') || ''
    let isBatch = navigation.getParam('isBatch') || false
    let isAppPhotoSetting = navigation.getParam('isAppPhotoSetting')
    let isAppDateSetting = navigation.getParam('isAppDateSetting')
    let isAppPhotoAlbumSetting = navigation.getParam('isAppPhotoAlbumSetting')
    this.setState({
      isAppPhotoSetting: isAppPhotoSetting,
      isAppDateSetting: isAppDateSetting,
      isAppPhotoAlbumSetting: isAppPhotoAlbumSetting
    })
    if (pageType == 'CheckPage') {
      if (isBatch) {
        this.setState({ isBatch: isBatch })
      }
      this.checkResData(guid)
    } else if (pageType == 'BatchScan') {
      this.batchResData()
    } else {
      this.resData()
    }

    //通过使用DeviceEventEmitter模块来监听事件
    this.iDataScan = DeviceEventEmitter.addListener('iDataScan', (Event) => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 115 ~ SteelCage ~ Event.ScanResult", Event.ScanResult)
      if (typeof Event.ScanResult != undefined) {
        let data = Event.ScanResult
        let arr = data.split("=");
        let id = ''
        id = arr[arr.length - 1]
        console.log("🚀 ~ file: concrete_pouring.js ~ line 118 ~ SteelCage ~ id", id)
        if (this.state.type == 'compid') {
          this.GetcomID(id)
        }

      }
    });
  }

  UpdateControl = () => {
    if (typeof this.props.navigation.getParam('QRid') != "undefined") {
      if (this.props.navigation.getParam('QRid') != "") {
        this.toast.show(Url.isLoadingView, 0)
      }
    }
    const { navigation } = this.props;
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || ''
    const type = navigation.getParam('type') || '';

    navigation.setParams({ QRid: '', type: '' })
    if (type == 'compid') {
      this.GetcomID(QRid)
    }

  }

  componentWillUnmount() {
    imageArr = [], imageFileArr = [];
    this.iDataScan.remove()
  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  checkResData = (guid) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getconctretecastingdetail",
      "servicetype": "pdaservice",
      "express": "1F0C8156",
      "ciphertext": "7b4b025eb6e691cf72b189d3169b8496",
      "data": { "_Rowguid": guid }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: concrete_pouring.js ~ line 162 ~ SteelCage ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 171 ~ SteelCage ~ resData", resData)
      let rowguid = resData.result[0]._Rowguid
      let ServerTime = resData.result[0].castingTime
      let compId = resData.result[0].compId
      let compCode = resData.result[0].compCode
      let productLineName = resData.result[0].productLineName
      let teamName = resData.result[0].teamName
      let remark = resData.result[0].remark
      let componentImageUrl = resData.result[0].componentImageUrl
      //图片数组
      let imageArr = [], imageFileArr = [];
      let tmp = {}
      tmp.fileid = ""
      tmp.uri = componentImageUrl
      tmp.url = componentImageUrl
      imageFileArr.push(tmp)
      this.setState({
        imageFileArr: imageFileArr
      })
      imageArr.push(componentImageUrl)
      let compData = {}, result = {}

      result.compTypeName = resData.result[0].compTypeName
      result.designType = resData.result[0].designType
      result.floorNoName = resData.result[0].floorNoName
      result.floorName = resData.result[0].floorName
      result.volume = resData.result[0].volume
      result.weight = resData.result[0].weight
      result.projectName = resData.result[0].projectName
      compData.result = result
      if (this.state.isBatch) {
        this.setState({ batchCompCodeselect: compCode, batchCompCode: compCode })
      }
      this.setState({
        resData: resData,
        rowguid: rowguid,
        compId: compId,
        compCode: compCode,
        compData: compData,
        remark: remark,
        imageArr: imageArr,
        pictureSelect: true,
        productLineName: productLineName,
        teamName: teamName,
        ServerTime: ServerTime,
        isGetcomID: true
      })
      this.setState({
        isLoading: false,
      })
    }).catch((error) => {
    });
  }

  batchResData = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getconctretecastingbatchno",
      "servicetype": "batchproduction",
      "express": "588DF83A",
      "ciphertext": "ba3f26d8fd1c527a0bc4362f761eab2e",
      "data": { "_bizid": Url.PDAFid }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      //初始化错误提示
      if (resData.status == 100) {
        let rowguid = resData.result.rowguid
        let ServerTime = resData.result.serverTime
        let listBatchNoInfo = resData.result.listBatchNoInfo
        this.setState({
          resData: resData,
          rowguid: rowguid,
          ServerTime: ServerTime,
          listBatchNoInfo: listBatchNoInfo,
          isBatch: true,
          isLoading: false
        })
      }
      else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }

    }).catch((error) => {
    });

  }

  resData = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "init",
      "servicetype": "pda",
      "express": "26576DAA",
      "ciphertext": "39e7235e54a23c7a9c68b39582cbfd66"
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      //初始化错误提示
      if (resData.status == 100) {
        let rowguid = resData.result.rowguid
        let ServerTime = resData.result.serverTime
        this.setState({
          resData: resData,
          rowguid: rowguid,
          ServerTime: ServerTime,
          isLoading: false
        })
      }
      else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }

    }).catch((error) => {
    });

  }

  PostData = () => {
    const { compId, ServerTime, rowguid, remark, imageArr, isBatch, recid } = this.state
    const { navigation } = this.props;
    const QRid = navigation.getParam('QRid') || '';

    if (compId == '') {
      this.toast.show('请扫描构件编码');
      return
    } else if (!isBatch) {
      if (this.state.isAppPhotoSetting) {
        if (imageArr.length == 0) {
          this.toast.show('请先拍摄构件照片');
          return
        }
      }
    }

    this.toast.show('保存中...', 5000)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "saveconctretecasting",
      "servicetype": "pdaservice",
      "express": "D2979AB2",
      "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
      "data": {
        "castingTime": ServerTime,
        "userName": Url.PDAusername,
        "compId": compId,
        "_bizid": Url.PDAFid,
        "rowguid": rowguid,
        "remark": remark,
        "recid": recid
      }
    }
    if (Url.isAppNewUpload) {
      data.action = "saveconctretecastingnew"
    }
    formData.append('jsonParam', JSON.stringify(data))
    if (!Url.isAppNewUpload) {
      imageArr.map((item, index) => {
        formData.append('img_' + index, {
          uri: item,
          type: 'image/jpeg',
          name: 'img_' + index
        })
      })
    }
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('保存成功');
        setTimeout(() => {
          this.props.navigation.replace(this.props.navigation.getParam("page"), {
            title: this.props.navigation.getParam("title"),
            pageType: this.props.navigation.getParam("pageType"),
            page: this.props.navigation.getParam("page"),
            isAppPhotoSetting: this.props.navigation.getParam("isAppPhotoSetting"),
            isAppDateSetting: this.props.navigation.getParam("isAppDateSetting"),
            isAppPhotoAlbumSetting: this.props.navigation.getParam("isAppPhotoAlbumSetting"),
          })
          //this.props.navigation.navigate('PDAMainStack')
        }, 400)
      } else {
        this.toast.show('保存失败')
        Alert.alert('保存失败', resData.message)
        this.toast.close(1)
      }
    }).catch((error) => {
      this.toast.show('保存失败')
      if (error.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('保存失败', "响应内容不是合法JSON格式")
        return
      }
      if (error.toString().indexOf('Network request faile') != -1) {
        Alert.alert('保存失败', "网络请求错误")
        return
      }
      Alert.alert('保存失败', error.toString())
    });
  }

  DeleteData = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "deleteconctretecasting",
      "servicetype": "pdaservice",
      "express": "1F0C8156",
      "ciphertext": "7b4b025eb6e691cf72b189d3169b8496",
      "data": { "_Rowguid": this.state.rowguid }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('删除成功');
        this.props.navigation.navigate('MainCheckPage')
        // setTimeout(() => {
        //   this.props.navigation.goBack()
        // }, 1000)
      } else {
        Alert.alert(
          '删除失败',
          resData.message,
          [{
            text: '取消',
            style: 'cancel'
          }, {
            text: '返回上一级',
            onPress: () => {
              this.props.navigation.goBack()
            }
          }])
        console.log('resData', resData)
      }
    }).catch((error) => {
    });
  }

  ScanButtonID = () => {
    //通过使用DeviceEventEmitter模块来监听事件
    DeviceEventEmitter.addListener('iDataScan', function (Event) {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 115 ~ SteelCage ~ Event.ScanResult", Event.ScanResult)
      if (typeof Event.ScanResult != undefined) {
        let data = Event.ScanResult
        let arr = data.split("=");
        let id = ''
        id = arr[arr.length - 1]
      }
    });
  }

  GetcomID = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getconctretecastingcomp",
      "servicetype": "pdaservice",
      "express": "E89AE9F1",
      "ciphertext": "5c6cc3224949777d1761a2c42f07d47c",
      "data": {
        "_bizid": Url.PDAFid,
        "compId": id
      }
    }
    console.log("🚀 ~ file: concrete_pouring.js ~ line 410 ~ SteelCage ~ data", data)
    Keyboard.dismiss()
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: concrete_pouring.js:494 ~ SteelCage ~ formData:", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let compCode = resData.result[0].compCode
        let compdata = resData.result
        let compId = resData.result[0].compId
        let productLineName = resData.result[0].productLineName
        let teamName = resData.result[0].teamName
        this.setState({
          compData: resData,
          compCode: compCode,
          compId: compId,
          productLineName: productLineName,
          teamName: teamName,
          isGetcomID: true,
          iscomIDdelete: false
        })
        setTimeout(() => { this.scoll.scrollToEnd() }, 300)
      } else {
        Alert.alert('错误', resData.message)
        this.setState({
          compId: ""
        })
        //this.input1.focus()
      }

    }).catch(err => {
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  comIDItem = (page) => {
    const { isGetcomID, buttondisable, compCode, compId } = this.state
    return (
      <View>
        {
          !isGetcomID ?
            <View style={{ flexDirection: 'row' }}>
              <View>
                <Input
                  ref={ref => { this.input1 = ref }}
                  containerStyle={styles.scan_input_container}
                  inputContainerStyle={styles.scan_inputContainerStyle}
                  inputStyle={[styles.scan_input]}
                  placeholder='请输入'
                  keyboardType='numeric'
                  value={compId}
                  onFocus={() => {
                    this.setState({
                      focusIndex: 0,
                      type: 'compid',
                    })
                  }}
                  onChangeText={(value) => {
                    value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
                    value = value.replace(/^\./g, ""); //验证第一个字符是数字
                    value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
                    //value = value.replace(/\b(0+)/gi, ""); //清楚开头的0
                    this.setState({
                      compId: value
                    })
                  }}
                  //onSubmitEditing={() => { this.input1.focus() }}
                  //onFocus={() => { this.setState({ focusIndex: '5' }) }}
                  /*  */
                  //returnKeyType = 'previous'
                  onSubmitEditing={() => {
                    let inputComId = this.state.compId
                    console.log("🚀 ~ file: qualityinspection.js ~ line 468 ~ QualityInspection ~ inputComId", inputComId)
                    inputComId = inputComId.replace(/\b(0+)/gi, "")
                    this.GetcomID(inputComId)
                  }}
                />
              </View>
              <ScanButton
                onPress={() => {
                  this.setState({
                    iscomIDdelete: false,
                    buttondisable: true,
                  })
                  this.props.navigation.navigate('QRCode', {
                    type: 'compid',
                    page: 'ConcretePouring'
                  })
                }}
                onLongPress={() => {
                  this.setState({
                    type: 'compid',
                    focusIndex: 0
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}
              />
            </View>
            :
            <TouchableOpacity
              onPress={() => {
                console.log('onPress')
              }}
              onLongPress={() => {
                console.log('onLongPress')
                Alert.alert(
                  '提示信息',
                  '是否删除构件信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      this.setState({
                        compData: [],
                        compCode: '',
                        compId: '',
                        iscomIDdelete: true,
                        isGetcomID: false,
                      })
                    }
                  }])
              }} >
              <Text>{compCode}</Text>
            </TouchableOpacity>
        }
      </View>

    )
  }

  cameraFunc = () => {
    launchCamera(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: parseInt(Url.isResizePhoto),
        maxWidth: parseInt(Url.isResizePhoto),
        saveToPhotos: true,

      },
      (response) => {
        if (response.uri == undefined) {
          return
        }
        imageArr.push(response.uri)
        this.setState({
          //
          pictureUri: response.uri,
          imageArr: imageArr
        })
        if (Url.isAppNewUpload) {

          this.cameraPost(response.uri)
        } else {
          this.setState({
            pictureSelect: true,
          })
        }
        console.log(response)
      },
    )
  }

  camLibraryFunc = () => {
    launchImageLibrary(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: parseInt(Url.isResizePhoto),
        maxWidth: parseInt(Url.isResizePhoto),
      },
      (response) => {
        if (response.uri == undefined) {
          return
        }
        imageArr.push(response.uri)
        this.setState({
          pictureUri: response.uri,
          imageArr: imageArr
        })
        if (Url.isAppNewUpload) {
          this.cameraPost(response.uri)
        } else {
          this.setState({
            pictureSelect: true,
          })
        }
        console.log(response)
      },
    )
  }

  camandlibFunc = () => {
    Alert.alert(
      '提示信息',
      '选择拍照方式',
      [{
        text: '取消',
        style: 'cancel'
      }, {
        text: '拍照',
        onPress: () => {
          this.cameraFunc()
        }
      }, {
        text: '相册',
        onPress: () => {
          this.camLibraryFunc()
        }
      }])
  }

  CameraView = () => {
    return (
      <ListItem
        containerStyle={{ paddingBottom: 6 }}
        leftAvatar={
          <View >
            <View style={{ flexDirection: 'column' }} >
              {
                this.state.isCheckPage ? <View></View> :
                  <AvatarAdd
                    pictureSelect={this.state.pictureSelect}
                    backgroundColor='rgba(77,142,245,0.20)'
                    color='#4D8EF5'
                    title="构"
                    onPress={() => {
                      if (this.state.isAppPhotoAlbumSetting) {
                        this.camandlibFunc()
                      } else {
                        this.cameraFunc()
                      }
                    }} />
              }
              {this.imageView()}
            </View>
          </View>
        }
      />
    )
  }


  // 展示/隐藏 放大图片
  handleZoomPicture = (flag, index) => {
    this.setState({
      imageOverSize: false,
      currShowImgIndex: index || 0
    })
  }

  // 加载放大图片弹窗
  renderZoomPicture = () => {
    const { imageOverSize, currShowImgIndex, imageFileArr } = this.state
    return (
      <OverlayImgZoomPicker
        isShowImage={imageOverSize}
        currShowImgIndex={currShowImgIndex}
        zoomImages={imageFileArr}
        callBack={(flag) => this.handleZoomPicture(flag)}
      ></OverlayImgZoomPicker>
    )
  }

  cameraPost = (uri) => {
    const { recid } = this.state
    this.setState({
      isPhotoUpdate: true
    })
    this.toast.show('图片上传中', 2000)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "savephote",
      "servicetype": "photo_file",
      "express": "D2979AB2",
      "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
      "data": {
        "fileflag": fileflag,
        "username": Url.PDAusername,
        "recid": recid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    formData.append('img', {
      uri: uri,
      type: 'image/jpeg',
      name: 'img'
    })
    console.log("🚀 ~ file: concrete_pouring.js ~ line 672 ~ SteelCage ~ formData", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 690 ~ SteelCage ~ resData", resData)
      this.toast.close()
      if (resData.status == '100') {
        let fileid = resData.result.fileid;
        let recid = resData.result.recid;

        let tmp = {}
        tmp.fileid = fileid
        tmp.uri = uri
        tmp.url = uri
        imageFileArr.push(tmp)
        this.setState({
          fileid: fileid,
          recid: recid,
          pictureSelect: true,
          imageFileArr: imageFileArr
        })
        console.log("🚀 ~ file: concrete_pouring.js ~ line 704 ~ SteelCage ~ imageFileArr", imageFileArr)
        this.toast.show('图片上传成功');
        this.setState({
          isPhotoUpdate: false
        })
        this.setState({
          isPhotoUpdate: false
        })
      } else {
        Alert.alert('图片上传失败', resData.message)
        this.toast.close(1)
      }
    }).catch((error) => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 692 ~ SteelCage ~ error", error)
    });
  }

  cameraDelete = (item) => {
    let i = imageArr.indexOf(item)

    if (Url.isAppNewUpload) {
      let formData = new FormData();
      let data = {};
      data = {
        "action": "deletephote",
        "servicetype": "photo_file",
        "express": "D2979AB2",
        "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
        "data": {
          "fileid": imageFileArr[i].fileid,
        }
      }
      formData.append('jsonParam', JSON.stringify(data))
      fetch(Url.PDAurl, {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        body: formData
      }).then(res => {
        console.log(res.statusText);
        console.log(res);
        return res.json();
      }).then(resData => {
        if (resData.status == '100') {
          if (i > -1) {
            imageArr.splice(i, 1)
            imageFileArr.splice(i, 1)
            this.setState({
              imageArr: imageArr,
              imageFileArr: imageFileArr
            }, () => {
              this.forceUpdate()
            })
          }
          if (imageArr.length == 0) {
            this.setState({
              pictureSelect: false
            })
          }
          this.toast.show('图片删除成功');
        } else {
          Alert.alert('图片删除失败', resData.message)
          this.toast.close(1)
        }
      }).catch((error) => {
        console.log("🚀 ~ file: concrete_pouring.js ~ line 761 ~ SteelCage ~ cameraDelete ~ error", error)
      });
    } else {
      if (i > -1) {
        imageArr.splice(i, 1)
        this.setState({
          imageArr: imageArr,
        }, () => {
          this.forceUpdate()
        })
      }
      if (imageArr.length == 0) {
        this.setState({
          pictureSelect: false
        })
      }
    }
  }

  imageView = () => {
    return (
      <View style={{ flexDirection: 'row' }}>
        {
          this.state.pictureSelect ?
            this.state.imageArr.map((item, index) => {
              return (
                <AvatarImg
                  item={item}
                  index={index}
                  onPress={() => {
                    this.setState({
                      imageOverSize: true,
                      pictureUri: item
                    })
                  }}
                  onLongPress={() => {
                    Alert.alert(
                      '提示信息',
                      '是否删除构件照片',
                      [{
                        text: '取消',
                        style: 'cancel'
                      }, {
                        text: '删除',
                        onPress: () => {
                          this.cameraDelete(item)
                        }
                      }])
                  }} />
              )
            }) : <View></View>
        }
      </View>
    )
  }

  _DatePicker = (index) => {
    const { ServerTime } = this.state
    return (
      <DatePicker
        customStyles={{
          dateInput: styles.dateInput,
          dateTouchBody: styles.dateTouchBody,
          dateText: this.state.isAppDateSetting ? styles.dateText : styles.dateDisabled,
        }}
        onOpenModal={() => { this.setState({ focusIndex: index }) }}
        iconComponent={<Icon name='caretdown' color={this.state.isAppDateSetting ? '#419fff' : "#666"} iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
        }
        showIcon={true}
        mode='datetime'
        date={ServerTime}
        format="YYYY-MM-DD HH:mm"
        disabled={!this.state.isAppDateSetting}
        onDateChange={(value) => {
          this.setState({
            ServerTime: value,
          })
        }}
      />
    )
  }

  batchListView = () => {
    return (
      <TouchableCustom underlayColor={'lightgray'} onPress={() => {
        this.isBottomVisible(this.state.listBatchNoInfo, "11")
      }}>

        <ListItemScan
          isButton={false}
          focusStyle={this.state.focusIndex == '11' ? styles.focusColor : {}}
          title='生产批次'
          rightElement={
            <IconDown text={this.state.batchCompCodeselect}></IconDown>
          }
        />
      </TouchableCustom>
    )
  }

  isBottomVisible = (data, focusIndex) => {
    if (typeof (data) != "undefined") {
      if (data.length > 0) {
        this.setState({ bottomVisible: true, bottomData: data, focusIndex: focusIndex })
      }
    } else {
      this.toast.show("无数据")
    }
  }

  BottomSheetBatchMain = () => {
    const { bottomVisible, bottomData, isCheckPage } = this.state;
    return (
      <BottomSheet
        isVisible={bottomVisible && !isCheckPage}
        onRequestClose={() => {
          this.setState({
            bottomVisible: false
          })
        }}
        onBackdropPress={() => {
          this.setState({
            bottomVisible: false
          })
        }}
      >
        <ScrollView style={styles.bottomsheetScroll}>
          {
            bottomData.map((item, index) => {
              let dataItem = {}
              dataItem.Title = item.compCode
              dataItem.rightTitle = item.ProductionQuantity
              dataItem.subTitle = item.CompTypeName
              dataItem.subRightTitle = item.ProjectName
              return (
                <TouchableOpacity
                  onPress={() => {
                    let compData = {}
                    compData.result = item
                    this.setState({
                      batchCompCode: item.compCode,
                      batchCompCodeselect: item.compCode,
                      productLineName: item.productLineName,
                      teamName: item.teamName,
                      compId: item.compId,
                      compCode: item.compCode,
                      compData: compData,
                      isGetcomID: true,
                    })
                    this.setState({
                      bottomVisible: false
                    })
                  }}
                >
                  <BatchBottomItem backgroundColor='white' color="#333" dataItem={dataItem} />
                </TouchableOpacity>
              )

            })
          }
        </ScrollView>
        <TouchableCustom
          onPress={() => {
            this.setState({ bottomVisible: false })
          }}>
          <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
        </TouchableCustom>
      </BottomSheet>
    )
  }

  render() {
    const { navigation } = this.props;
    let pageType = navigation.getParam('pageType') || ''

    const { isBatch, resData, isGetcomID, compData, bottomVisible, bottomData, focusIndex, isCheckPage, imageArr, remark } = this.state

    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Toast ref={(ref) => { this.toast = ref }} position="center" />
          <NavigationEvents
            onWillFocus={this.UpdateControl} />
          <ScrollView ref={ref => this.scoll = ref} style={styles.mainView} showsVerticalScrollIndicator={false} >
            <View style={styles.listView}>

              {
                isBatch ? this.batchListView() : this.CameraView()
              }

              {
                isBatch ? <View></View> :
                  <ListItemScan
                    isButton={!isGetcomID}
                    focusStyle={focusIndex == '0' ? styles.focusColor : {}}
                    title='产品编号'
                    onPressIn={() => {
                      this.setState({
                        type: 'compid',
                        focusIndex: 0
                      })
                      NativeModules.PDAScan.onScan();
                    }}
                    onPress={() => {
                      this.setState({
                        type: 'compid',
                        focusIndex: 0
                      })
                      NativeModules.PDAScan.onScan();
                    }}
                    onPressOut={() => {
                      NativeModules.PDAScan.offScan();
                    }}
                    rightElement={
                      this.comIDItem()
                    }
                  />
              }
              {
                this.state.compCode != '' ?
                  <CardList compData={compData} /> : <View></View>
              }
              <ListItemScan
                title='生产线'
                rightTitle={this.state.productLineName}
              />
              <ListItemScan
                title='班组名称'
                rightTitle={this.state.teamName}
              />
              <ListItemScan
                focusStyle={focusIndex == '1' ? styles.focusColor : {}}
                title='浇筑日期'
                rightElement={this._DatePicker(1)}
              />
              <ListItemScan
                title='操作人'
                rightTitle={Url.PDAEmployeeName}
              />
              <ListItemScan
                focusStyle={focusIndex == '5' ? styles.focusColor : {}}
                title='备注'
                rightElement={
                  <View>
                    <Input
                      containerStyle={styles.quality_input_container}
                      inputContainerStyle={styles.inputContainerStyle}
                      inputStyle={[styles.quality_input_, { top: 7 }]}
                      placeholder='请输入'
                      value={remark}
                      onChangeText={(value) => {
                        this.setState({
                          remark: value
                        })
                      }} />
                  </View>
                }
              />
            </View>

            {
              pageType == 'CheckPage' ?
                <FormButton
                  onLongPress={() => {
                    Alert.alert(
                      '提示信息',
                      '是否删除',
                      [{
                        text: '取消',
                        style: 'cancel'
                      }, {
                        text: '删除',
                        onPress: () => {
                          this.DeleteData()
                        }
                      }])
                  }}
                  title='删除'
                  backgroundColor='#EB5D20'
                /> :
                <FormButton
                  onPress={() => {
                    this.PostData()
                  }}
                  disabled={this.state.isPhotoUpdate}
                  title='保存'
                  backgroundColor='#17BC29'
                />
            }

            {this.renderZoomPicture()}

            {/* {overlayImg(obj = {
              onRequestClose: () => {
                this.setState({ imageOverSize: !this.state.imageOverSize }, () => {
                })
              },
              onPress: () => {
                this.setState({
                  imageOverSize: !this.state.imageOverSize
                })
              },
              uri: this.state.pictureUri,
              isVisible: this.state.imageOverSize
            })
            } */}


            {this.BottomSheetBatchMain()}
          </ScrollView>
        </View>
      )
    }
  }

}