import React from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, Icon, Card, ListItem, BottomSheet, CheckBox, Input } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { RFT } from '../../Url/Pixal';
import { ScrollView } from 'react-native-gesture-handler';
import CardList from "../Componment/CardList";
import DatePicker from 'react-native-datepicker'
import { NavigationEvents } from 'react-navigation';
import { styles } from '../Componment/PDAStyles';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import ListItemScan_child from '../Componment/ListItemScan_child';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import CheckBoxScan from '../Componment/CheckBoxScan';
import AvatarAdd from '../Componment/AvatarAdd';
import AvatarImg from '../Componment/AvatarImg';


let visibleArr = [];

const { height, width } = Dimensions.get('window') //获取宽高

export default class unload extends React.Component {
  constructor() {
    super();
    this.state = {
      bottomData: [],
      bottomVisible: false,
      buttomTitle: '',
      resData: [],
      formCodeSetup: '',
      formCode: '',
      rowguid: '',
      ServerTime: '',
      unload: [],
      loadcarId: '',
      formCode: '',
      formCodeselect: '请选择',
      compData: [],
      componentArr: [],
      component: {},
      carNum: '',
      compCode: '',
      compId: '',
      companyname: '',
      comp: {},
      focusIndex: '',
      visibleArr: [],
      isCheckPage: false,
      isLoading: true
    }
    //this.requestCameraPermission = this.requestCameraPermission.bind(this)
    this.Getbatchgetunloadcomponentinfo = this.Getbatchgetunloadcomponentinfo.bind(this)
  }

  componentDidMount() {
    const { navigation } = this.props;
    let guid = navigation.getParam('guid') || ''
    let pageType = navigation.getParam('pageType') || ''
    if (pageType == 'CheckPage') {
      this.checkResData(guid)
      this.setState({ isCheckPage: true })
    } else {
      this.resData()
    }
  }

  componentWillUnmount() {
    visibleArr = []
  }

  UpdateControl = () => {
    if (typeof this.props.navigation.getParam('QRid') != "undefined") {
      if (this.props.navigation.getParam('QRid') != "") {
        this.toast.show(Url.isLoadingView, 0)
      }
    }
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || ''
    const type = navigation.getParam('type') || '';
    if (type == 'compid') {
      this.GetcomID(QRid)
    }
  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  checkResData = (guid) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "ObtainUnload",
      "servicetype": "pda",
      "express": "8AC4C0B0",
      "ciphertext": "7df83f712027c63f147f6c2d4a43f08d",
      "data": {
        "guid": guid,
        "factoryId": Url.PDAFid,
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      let rowguid = resData.result.guid
      let ServerTime = resData.result.time
      let formCode = resData.result.formCode
      let companyname = resData.result.company
      let carNum = resData.result.carNum

      let comp = resData.result.component[0].comp[0]
      let componentArr = resData.result.component

      this.setState({
        resData: resData,
        rowguid: rowguid,
        ServerTime: ServerTime,
        formCode: formCode,
        formCodeselect: formCode,
        companyname: companyname,
        carNum: carNum,
        componentArr: componentArr,
        comp: comp,
        isCheck: true
      })
      this.setState({
        isLoading: false,
      })
    }).catch((error) => {
    });
  }

  resData = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getunloadformcode",
      "servicetype": "pda",
      "express": "54413296",
      "ciphertext": "f515fc4e92cd81567c5da6c10a3a34ea",
      "data": { "factoryId": Url.PDAFid }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      //初始化错误提示
      if (resData.status == 100) {
        console.log("🚀 ~ file: unload.js:172 ~ unload ~ resData:", resData)
        let rowguid = resData.result.rowguid
        let ServerTime = resData.result.ServerTime
        let unload = resData.result.unload
        this.setState({
          resData: resData,
          rowguid: rowguid,
          ServerTime: ServerTime,
          unload: unload,
          isLoading: false
        })
      }
      else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }

    }).catch((error) => {
    });

  }

  PostData = () => {
    const { ServerTime, loadcarId, rowguid } = this.state
    const { navigation } = this.props;
    const QRid = navigation.getParam('QRid') || '';

    if (loadcarId == '') {
      this.toast.show('请选择表单编号');
      return
    }
    let formData = new FormData();
    let data = {};
    data = {
      "action": "saveunloadcar",
      "servicetype": "pda",
      "express": "173B0BD0",
      "ciphertext": "554ae676049cd672b7bb193a10fab046",
      "data": {
        "Operator": Url.PDAEmployeeName,
        "editEmployeeId": Url.PDAEmployeeId,
        "loadcarid": loadcarId,
        "Unloaddate": ServerTime,
        "factoryId": Url.PDAFid,
        "rowguid": rowguid
      }
    }

    this.toast.show(saveLoading, 0)

    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('保存成功');
        setTimeout(() => {
          this.props.navigation.replace(this.props.navigation.getParam("page"), {
            title: this.props.navigation.getParam("title"),
            pageType: this.props.navigation.getParam("pageType"),
            page: this.props.navigation.getParam("page"),
            isAppPhotoSetting: this.props.navigation.getParam("isAppPhotoSetting"),
            isAppDateSetting: this.props.navigation.getParam("isAppDateSetting"),
            isAppPhotoAlbumSetting: this.props.navigation.getParam("isAppPhotoAlbumSetting"),
          })
          //this.props.navigation.navigate('PDAMainStack')
        }, 400)
      } else {
        this.toast.show('保存失败')
        Alert.alert('保存失败', resData.message)

      }
    }).catch((error) => {
      if (error.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('保存失败', "响应内容不是合法JSON格式")
        return
      }
      if (error.toString().indexOf('Network request faile') != -1) {
        Alert.alert('保存失败', "网络请求错误")
        return
      }
      Alert.alert('保存失败', error.toString())
    });
  }

  Getbatchgetunloadcomponentinfo = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "batchgetunloadcomponentinfo",
      "servicetype": "pda",
      "express": "60C6FC13",
      "ciphertext": "6b17bf4baf96babc9bdd5fa2fd66bb69",
      "data": {
        "loadcarId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
       this.toast.close()
      if (resData.status == '100') {
        let result = resData.result
        let carNum = result.carNum
        let companyname = result.companyname
        let componentArr = result.component
        let component = result.component[0]
        let compCode = component.compCode
        let comp = component.comp[0]
        this.setState({
          compData: resData,
          carNum: carNum,
          compCode: compCode,
          companyname: companyname,
          componentArr: componentArr,
          comp: comp,
        })
      } else {
        Alert.alert('ERROR', resData.message)
      }

    }).catch(err => {
    })
  }

  _DatePicker = () => {
    const { ServerTime } = this.state
    return (
      <DatePicker
        customStyles={{
          dateInput: styles.dateInput,
          dateTouchBody: styles.dateTouchBody,
          dateText: styles.dateText,
        }}
        onOpenModal={() => {
          this.setState({ focusIndex: '1' })
        }}
        iconComponent={<Icon name='caretdown' color='#419fff' iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
        }
        showIcon={true}
        mode='datetime'
        date={ServerTime}
        format="YYYY-MM-DD HH:mm"
        onDateChange={(value) => {
          this.setState({
            ServerTime: value
          })
        }}
      />
    )
  }

  isBottomVisible = (data, focusIndex) => {
    if (typeof (data) != "undefined") {
      if (data.length > 0) {
        this.setState({ bottomVisible: true, bottomData: data, focusIndex: focusIndex })
      }
    } else {
      this.toast.show("无数据")
    }
  }

  cardListContont = comp => {
    if (visibleArr.indexOf(comp.compCode) == -1) {
      visibleArr.push(comp.compCode);
      this.setState({ visibleArr: visibleArr });
    } else {
      if (visibleArr.length == 1) {
        visibleArr.splice(0, 1);
        this.setState({ visibleArr: visibleArr }, () => {
          this.forceUpdate;
        });
      } else {
        visibleArr.map((item, index) => {
          if (item == comp.compCode) {
            visibleArr.splice(index, 1);
            this.setState({ visibleArr: visibleArr }, () => {
              this.forceUpdate;
            });
          }
        });
      }
    }
  };

  CardList = (compData) => {
    const { } = this.state
    return (
      compData.map((item, index) => {
        let comp = item.comp[0]
        return (
          <View style={styles.CardList_main}>
            <TouchableCustom
              onPress={() => {
                this.cardListContont(comp);
              }}
            /*  onLongPress={() => {
               Alert.alert(
                 '提示信息',
                 '是否删除构件信息',
                 [{
                   text: '取消',
                   style: 'cancel'
                 }, {
                   text: '删除',
                   onPress: () => {
                     compDataArr.splice(index, 1)
                     compIdArr.splice(index, 1)
                     tmpstr = compIdArr.toString()
                     weightAccount = 0;
                     compDataArr.map(item1 => {
                       weightAccount += item1[0].weight;
                     });
                     compIdArr.map(itemID => {
                       tmpstr = "";
                       tmpstr += itemID.compId + ",";
                     });
                     this.setState({
                       compDataArr: compDataArr,
                       compIdArr: compIdArr
                     })
                   }
                 }])

             }} */
            >
              <View style={styles.CardList_title_main}>
                <View style={styles.title_num}>
                  <Text >{index + 1}</Text>
                </View>
                <View style={styles.title_title}>
                  <Text >{item.compCode}</Text>
                </View>
                <View style={styles.CardList_at_icon}>
                  <IconDown text={'1个'} />
                </View>
              </View>
            </TouchableCustom>
            {
              visibleArr.indexOf(item.compCode) != -1 ?
                <View key={index} style={{ backgroundColor: '#F9F9F9' }} >
                  <ListItemScan_child
                    title='产品编号'
                    rightTitle={comp.compCode}
                  />
                  <ListItemScan_child
                    title='项目名称'
                    rightTitle={comp.projectName}
                  />
                  <ListItemScan_child
                    title='构件类型'
                    rightTitle={comp.compTypeName}
                  />
                  <ListItemScan_child
                    title='设计型号'
                    rightTitle={comp.designType}
                  />
                  <ListItemScan_child
                    title='楼号'
                    rightTitle={comp.floorNoName}
                  />
                  <ListItemScan_child
                    title='层号'
                    rightTitle={comp.floorName}
                  />
                  <ListItemScan_child
                    title='砼方量'
                    rightTitle={comp.volume}
                  />
                  <ListItemScan_child
                    title='重量'
                    rightTitle={comp.weight}
                  />

                </View> : <View></View>
            }

          </View>
        )
      })
    )
  }

  cardListContont = comp => {
    if (visibleArr.indexOf(comp.compCode) == -1) {
      visibleArr.push(comp.compCode);
      this.setState({ visibleArr: visibleArr });
    } else {
      if (visibleArr.length == 1) {
        visibleArr.splice(0, 1);
        this.setState({ visibleArr: visibleArr }, () => {
          this.forceUpdate;
        });
      } else {
        visibleArr.map((item, index) => {
          if (item == comp.compCode) {
            visibleArr.splice(index, 1);
            this.setState({ visibleArr: visibleArr }, () => {
              this.forceUpdate;
            });
          }
        });
      }
    }
  };

  render() {
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起

    const { resData, ServerTime, focusIndex, unload, formCodeselect, comp, carNum, companyname, bottomVisible, bottomData, componentArr } = this.state

    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          <NavigationEvents
            onWillFocus={this.UpdateControl} />
          <ScrollView ref={ref => this.scoll = ref} style={styles.mainView} showsVerticalScrollIndicator={false} >
            <View style={styles.listView}>

              <TouchableCustom disabled={this.state.isCheckPage} onPress={() => { this.isBottomVisible(unload, '0') }} >
                <ListItemScan
                  focusStyle={focusIndex == '0' ? styles.focusColor : {}}
                  title='表单编号'
                  rightElement={
                    <IconDown text={formCodeselect} />
                  }
                />
              </TouchableCustom>
              <ListItemScan
                title='项目名称'
                rightTitle={comp.projectName}
              />

              <ListItemScan
                title='车牌号'
                rightTitle={carNum}
              />
              <ListItemScan
                title='承运单位'
                rightTitle={companyname}
              />
              <ListItemScan
                focusStyle={focusIndex == '1' ? styles.focusColor : {}}
                title='卸车日期'
                rightElement={this._DatePicker}
              />
              <ListItemScan
                title='操作人'
                rightTitle={Url.PDAEmployeeName}
              />
              <ListItemScan
                title='卸车明细'
              />

              {
                componentArr.length != 0 ? this.CardList(componentArr) : <View></View>
              }
            </View>




            <FormButton
              backgroundColor='#17BC29'
              title='保存'
              onPress={() => {
                this.PostData()
              }}
            />

            <BottomSheet
              isVisible={bottomVisible && !this.state.isCheckPage}
              onRequestClose={() => {
                this.setState({
                  bottomVisible: false
                })
              }}
              onBackdropPress={() => {
                this.setState({
                  bottomVisible: false
                })
              }}
            >
              <ScrollView style={styles.bottomsheetScroll}>
                {
                  bottomData.map((item, index) => {
                    let title = ''
                    if (item.loadcarId) {
                      title = item.formCode + ' ' + item.projectName
                    }
                    return (
                      <TouchableOpacity
                        onPress={() => {
                          if (item.loadcarId) {
                            this.setState({
                              loadcarId: item.loadcarId,
                              formCode: item.formCode,
                              projectName: item.projectName,
                              formCodeselect: item.formCode
                            })
                            this.Getbatchgetunloadcomponentinfo(item.loadcarId)
                          }
                          this.setState({
                            bottomVisible: false
                          })
                        }}
                      >
                        <BottomItem backgroundColor='white' color="#333" title={title} />
                      </TouchableOpacity>
                    )

                  })
                }
              </ScrollView>
              <TouchableCustom
                onPress={() => {
                  this.setState({ bottomVisible: false })
                }}>
                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
              </TouchableCustom>
            </BottomSheet>

          </ScrollView>

        </View>
      )
    }
  }

}

class PlanCardList extends React.Component {
  render() {
    const { PlanData } = this.props
    return (
      <View>
        <Card containerStyle={{ borderRadius: 10, shadowOpacity: 0 }}>
          <ListItem
            containerStyle={styles.list_container_style}
            title='产品编号'
            rightTitle={PlanData.compCode}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='项目名称'
            rightTitle={PlanData.projectName}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='构件类型'
            rightTitle={PlanData.compTypeName}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='设计型号'
            rightTitle={PlanData.designType}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='楼号'
            rightTitle={PlanData.floorNoName}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='层号'
            rightTitle={PlanData.floorName}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='砼方量'
            rightTitle={PlanData.floorName}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='重量'
            rightTitle={PlanData.weight}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
        </Card>
      </View>
    )
  }
}