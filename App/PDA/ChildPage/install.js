import React from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert, Keyboard } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, ListItem, BottomSheet, CheckBox } from 'react-native-elements';
import Url from '../../Url/Url';
import Toast from 'react-native-easy-toast';
import { ToDay, YesterDay, BeforeYesterDay } from '../../CommonComponents/Data';
import { RFT } from '../../Url/Pixal';
import CardList from "../Componment/CardList";
import { ScrollView } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker'
import { NavigationEvents } from 'react-navigation';
import ScanButton from '../Componment/ScanButton';
import { styles } from '../Componment/PDAStyles';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import ListItemScan_child from '../Componment/ListItemScan_child';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import CheckBoxScan from '../Componment/CheckBoxScan';
import AvatarAdd from '../Componment/AvatarAdd';
import AvatarImg from '../Componment/AvatarImg';

import { DeviceEventEmitter, NativeModules } from 'react-native';

let varGetError = false

let compDataArr = [], compIdArr = [], QRid2 = '', tmpstr = '',
  questionChickIdArr = [], questionChickArr = [], //子表问题数组
  isCheckSecondList = [], //是否展开二级
  isCheckedAllList = [], //二级菜单是否被全选
  visibleArr = []

const { height, width } = Dimensions.get('window') //获取宽高

export default class Install extends React.Component {
  constructor() {
    super();
    this.state = {
      buttondisable: false,
      bottomData: [],
      bottomVisible: false,
      resData: [],
      type: 'compid',
      rowguid: '',
      ServerTime: '',
      steelCageIDs: '',
      compData: {},
      compId: '',
      compCode: '',
      productLineName: '',
      teamName: '',
      checked: true,
      isGetcomID: false,
      iscomIDdelete: false,
      compDataArr: [],
      compData: '',
      compId: '',
      compIdArr: [],
      compCode: '',
      buttondisable: false,
      GetError: false,
      focusIndex: '0',
      isLoading: true,
      isCheckPage: false
    }
    //this.requestCameraPermission = this.requestCameraPermission.bind(this)
  }

  componentDidMount() {
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const QRid = navigation.getParam('QRid') || ''
    const type = navigation.getParam('type') || ''
    let guid = navigation.getParam('guid') || ''
    let pageType = navigation.getParam('pageType') || ''
    if (pageType == 'CheckPage') {
      this.setState({ isCheckPage: true }, () => {
        this.checkResData(guid)
      })
    } else {
      this.resData()
    }

    //通过使用DeviceEventEmitter模块来监听事件
    this.iDataScan = DeviceEventEmitter.addListener('iDataScan', (Event) => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 115 ~ SteelCage ~ Event.ScanResult", Event.ScanResult)
      if (typeof Event.ScanResult != undefined) {
        let data = Event.ScanResult
        let arr = data.split("=");
        let id = ''
        id = arr[arr.length - 1]
        console.log("🚀 ~ file: concrete_pouring.js ~ line 118 ~ SteelCage ~ id", id)
        if (this.state.type == 'compid') {
          this.GetcomID(id)
        }

      }
    });
  }

  componentWillUnmount() {
    compDataArr = [], QRid2 = '', tmpstr = '', compIdArr = [],
      questionChickIdArr = [], questionChickArr = [], //子表问题数组
      isCheckSecondList = [], //是否展开二级
      isCheckedAllList = [] //二级菜单是否被全选
      , visibleArr = [];
    this.iDataScan.remove()
  }

  UpdateControl = () => {
    if (typeof this.props.navigation.getParam('QRid') != "undefined") {
      if (this.props.navigation.getParam('QRid') != "") {
        this.toast.show(Url.isLoadingView, 0)
      }
    }
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || ''
    const type = navigation.getParam('type') || '';
    if (type == 'compid') {
      this.GetcomID(QRid)
    }

  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  checkResData = (guid) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "ObtainInstall",
      "servicetype": "pda",
      "express": "8AC4C0B0",
      "ciphertext": "7df83f712027c63f147f6c2d4a43f08d",
      "data": {
        "guid": guid,
        "factoryId": Url.PDAFid,
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      let rowguid = resData.result.guid
      let ServerTime = resData.result.time
      let compCode = resData.result.compCode
      let compId = resData.result.compId
      let compData = resData.result
      compDataArr.push(compData)
      //compData.result = result
      this.setState({
        resData: resData,
        rowguid: rowguid,
        ServerTime: ServerTime,
        compCode: compCode,
        compId: compId,
        compDataArr: compDataArr,
        compData: compData,
        isGetcomID: true,

      })

      setTimeout(() => {
        this.setState({
          isLoading: false,
        })
      }, 400)

    }).catch((error) => {
    });
  }

  resData = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "init",
      "servicetype": "pda",
      "express": "26576DAA",
      "ciphertext": "39e7235e54a23c7a9c68b39582cbfd66"
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      //初始化错误提示
      if (resData.status == 100) {
        let rowguid = resData.result.rowguid
        let ServerTime = resData.result.serverTime
        this.setState({
          resData: resData,
          rowguid: rowguid,
          ServerTime: ServerTime,
          isLoading: false
        })
      }
      else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }

    }).catch((error) => {
    });

  }

  PostData = () => {
    const { compId, compIdArr, ServerTime, rowguid } = this.state
    const { navigation } = this.props;
    const QRid = navigation.getParam('QRid') || '';

    let compIdstr = compIdArr.toString()

    this.toast.show(saveLoading, 0)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "batchsavecomponentinstallinfo",
      "servicetype": "pda",
      "express": "36D458E9",
      "ciphertext": "b162f41607481b65b5b54c502ab27edb",
      "data": {
        "Operator": Url.PDAEmployeeName,
        "compIds": compIdstr,
        "installtime": ServerTime,
        "factoryId": Url.PDAFid,
        "factoryName": Url.PDAFname,
        "loginName": Url.PDAusername,
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('保存成功');
        setTimeout(() => {
          this.props.navigation.replace(this.props.navigation.getParam("page"), {
            title: this.props.navigation.getParam("title"),
            pageType: this.props.navigation.getParam("pageType"),
            page: this.props.navigation.getParam("page"),
            isAppPhotoSetting: this.props.navigation.getParam("isAppPhotoSetting"),
            isAppDateSetting: this.props.navigation.getParam("isAppDateSetting"),
            isAppPhotoAlbumSetting: this.props.navigation.getParam("isAppPhotoAlbumSetting"),
          })
          //this.props.navigation.navigate('PDAMainStack')
        }, 400)
      } else {
        this.toast.show('保存失败')
        Alert.alert('保存失败', resData.message)
      }
    }).catch((error) => {
      this.toast.show('保存失败')
      if (error.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('保存失败', "响应内容不是合法JSON格式")
        return
      }
      if (error.toString().indexOf('Network request faile') != -1) {
        Alert.alert('保存失败', "网络请求错误")
        return
      }
      Alert.alert('保存失败', error.toString())
    });
  }

  ReviseData = () => {
    const { rowguid, ServerTime } = this.state;
    let formData = new FormData();
    let data = {};
    data = {
      "action": "ReviseInstall",
      "servicetype": "pda",
      "express": "6CD1492E",
      "ciphertext": "d3b457b89266f5846dddec667d794966",
      "data": {
        "guid": rowguid,
        "factoryId": Url.PDAFid,
        "time": ServerTime
      }
    }

    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('修改成功');
        //this.props.navigation.setParams({backType: "revise"})
        this.props.navigation.pop()
      } else {
        Alert.alert('修改失败', resData.message)
      }
    }).catch((error) => {
    });
  }

  CardList = (compData) => {
    return (
      compData.map((item, index) => {
        let comp = item
        return (
          <View style={styles.CardList_main}>
            <TouchableCustom
              onPress={() => {
                this.cardListContont(comp);
              }}
              onLongPress={() => {
                Alert.alert(
                  '提示信息',
                  '是否删除构件信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      compDataArr.splice(index, 1)
                      compIdArr.splice(index, 1)
                      tmpstr = compIdArr.toString()
                      this.setState({
                        compDataArr: compDataArr,
                        compIdArr: compIdArr
                      })
                    }
                  }])
              }}>
              <View style={styles.CardList_title_main}>
                <View style={styles.title_num}>
                  <Text >{index + 1}</Text>
                </View>
                <View style={styles.title_title}>
                  <Text >{item.compCode}</Text>
                </View>
                <View style={styles.CardList_at_icon}>
                  <IconDown />
                </View>
              </View>
            </TouchableCustom>

            {
              visibleArr.indexOf(item.compCode) != -1 ?
                <View key={index} style={{ backgroundColor: '#F9F9F9' }} >
                  <ListItemScan_child
                    title='产品编号'
                    rightTitle={comp.compCode}
                  />
                  <ListItemScan_child
                    title='项目名称'
                    rightTitle={comp.projectName}
                  />
                  <ListItemScan_child
                    title='构件类型'
                    rightTitle={comp.compTypeName}
                  />
                  <ListItemScan_child
                    title='设计型号'
                    rightTitle={comp.designType}
                  />
                  <ListItemScan_child
                    title='楼号'
                    rightTitle={comp.floorNoName}
                  />
                  <ListItemScan_child
                    title='层号'
                    rightTitle={comp.floorName}
                  />
                  <ListItemScan_child
                    title='砼方量'
                    rightTitle={comp.volume}
                  />
                  <ListItemScan_child
                    title='重量'
                    rightTitle={comp.weight}
                  />
                  <ListItemScan_child
                    title='生产单位'
                    rightTitle={Url.PDAFname}
                  />
                </View> : <View></View>
            }

          </View>
        )
      })
    )
  }

  cardListContont = comp => {
    if (visibleArr.indexOf(comp.compCode) == -1) {
      visibleArr.push(comp.compCode);
      this.setState({ visibleArr: visibleArr });
    } else {
      if (visibleArr.length == 1) {
        visibleArr.splice(0, 1);
        this.setState({ visibleArr: visibleArr }, () => {
          this.forceUpdate;
        });
      } else {
        visibleArr.map((item, index) => {
          if (item == comp.compCode) {
            visibleArr.splice(index, 1);
            this.setState({ visibleArr: visibleArr }, () => {
              this.forceUpdate;
            });
          }
        });
      }
    }
  };

  _DatePicker = () => {
    const { isCheckPage, ServerTime } = this.state
    return (
      <DatePicker
        customStyles={{
          dateInput: styles.dateInput,
          dateTouchBody: styles.dateTouchBody,
          dateText: styles.dateText,
        }}
        onOpenModal={() => {
          this.setState({ focusIndex: '1' })
        }}
        iconComponent={<Icon name='caretdown' color='#419fff' iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
        }
        showIcon={true}
        mode='datetime'
        //duration = {10000}
        //disabled = {isCheckPage}
        date={ServerTime}
        format="YYYY-MM-DD HH:mm"
        onDateChange={(value) => {
          this.setState({
            ServerTime: value,
          })
        }}
      />
    )
  }

  GetcomID = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getComponentInfoForInstall",
      "servicetype": "pda",
      "express": "2934F95F",
      "ciphertext": "1bb0c4f99cbd1d1d56267152cb954ab6",
      "data": {
        "compId": id,
        "factoryId": Url.PDAFid
      }
    }
    Keyboard.dismiss()
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
       this.toast.close()
      if (resData.status == '100') {
        let compData = resData.result
        let compId = compData.compId
        let compCode = compData.compCode
        if (compDataArr.length == 0) {
          compDataArr.push(compData)
          compIdArr.push(compId)
          this.setState({
            compDataArr: compDataArr,
            compIdArr: compIdArr,
            compData: compData,
            compId: "",
            compCode: compCode,
            isGetcomID: true
          })
          tmpstr = tmpstr + compId + ','
        } else {
          if (tmpstr.indexOf(compId) == -1) {
            compIdArr.push(compId)
            compDataArr.push(compData)
            this.setState({
              compDataArr: compDataArr,
              compIdArr: compIdArr,
              compData: compData,
              compId: "",
              compCode: compCode,
              isGetcomID: true
            })
            tmpstr = tmpstr + compId + ','
          } else {
            this.toast.show('已经扫瞄过此构件')
          }
          this.setState({
            compId: ""
          })
        }
      } else {
        console.log('resData', resData)
        Alert.alert('错误', resData.message)
        this.setState({
          compId: ""
        })
        //this.input1.focus()
        varGetError = true
        this.setState({
          GetError: true
        })
      }

    }).catch(err => {
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  comIDItem = (index) => {
    const { isGetcomID, buttondisable, compCode, compId } = this.state
    return (
      <View style={{ flexDirection: 'row' }}>
        <View>
          <Input
            ref={ref => { this.input1 = ref }}
            containerStyle={styles.scan_input_container}
            inputContainerStyle={styles.scan_inputContainerStyle}
            inputStyle={[styles.scan_input]}
            placeholder='请输入'
            keyboardType='numeric'
            value={compId}
            onChangeText={(value) => {
              value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
              value = value.replace(/^\./g, ""); //验证第一个字符是数字
              value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
              //value = value.replace(/\b(0+)/gi, ""); //清楚开头的0
              this.setState({
                compId: value
              })
            }}
            onFocus={() => {
              this.setState({
                focusIndex: index,
                type: 'compid',
              })
            }}
            onSubmitEditing={() => {
              let inputComId = this.state.compId
              console.log("🚀 ~ file: qualityinspection.js ~ line 468 ~ QualityInspection ~ inputComId", inputComId)
              inputComId = inputComId.replace(/\b(0+)/gi, "")
              this.GetcomID(inputComId)
            }}
          />
        </View>
        <ScanButton
          onPress={() => {
            this.setState({
              iscomIDdelete: false,
              buttondisable: true,
              focusIndex: index
            })
            this.props.navigation.navigate('QRCode', {
              type: 'compid',
              page: 'PDAInstall'
            })
          }}
          onLongPress={() => {
            this.setState({
              type: 'compid',
              focusIndex: index
            })
            NativeModules.PDAScan.onScan();
          }}
          onPressOut={() => {
            NativeModules.PDAScan.offScan();
          }}
        />
      </View>
    )
  }

  render() {
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || ''
    const type = navigation.getParam('type') || '';

    if (type == 'compid') {
      QRcompid = QRid
    }



    const { resData, ServerTime, isGetcomID, compData, focusIndex, compCode, bottomVisible, bottomData } = this.state

    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          <NavigationEvents
            onWillFocus={this.UpdateControl} />
          <ScrollView ref={ref => this.scoll = ref} style={styles.mainView}showsVerticalScrollIndicator={false} >
            <View style={styles.listView}>

              <ListItemScan
                focusStyle={focusIndex == '1' ? styles.focusColor : {}}
                title='安装日期'
                rightElement={this._DatePicker()}
              />
              <ListItemScan
                title='操作人'
                rightTitle={Url.PDAEmployeeName}
              />

              <ListItemScan
                isButton={!isGetcomID}
                focusStyle={focusIndex == '0' ? styles.focusColor : {}}
                title='产品编号'
                onPressIn={() => {
                  this.setState({
                    type: 'compid',
                    focusIndex: 0
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPress={() => {
                  this.setState({
                    type: 'compid',
                    focusIndex: 0
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}
                rightElement={
                  this.comIDItem(0)
                }
              />

              {
                compCode.length > 0 ? this.CardList(compDataArr) : <View></View>
              }
            </View>




            {this.state.isCheckPage ? <FormButton
              backgroundColor='#EB5D20'
              onPress={() => {
                this.ReviseData()
              }}
              title='修改'
            /> :
              <FormButton
                backgroundColor='#17BC29'
                title='保存'
                onPress={() => {
                  this.PostData()
                }}
              />}



            <BottomSheet
              isVisible={bottomVisible}
              onRequestClose={() => {
                this.setState({
                  bottomVisible: false
                })
              }}
              onBackdropPress={() => {
                this.setState({
                  bottomVisible: false
                })
              }}
            >
              {
                bottomData.map((item, index) => {
                  return (
                    <TouchableOpacity
                      onPress={() => {
                        if (item.roomId) {
                          this.setState({
                            roomId: item.roomId,
                            roomselect: item.roomName
                          })
                        } else if (item.teamId) {
                          this.setState({
                            teamId: item.teamId,
                            teamselect: item.teamName
                          })
                        } else {
                          this.setState({
                            keeperId: item.keeperId,
                            keeperselect: item.keeperName
                          })
                        }
                        this.setState({
                          bottomVisible: false
                        })
                      }}
                    >
                      <ListItem
                        title={item.roomName ? item.roomName : (item.teamName ? item.teamName : item.keeperName)}
                        //titleStyle={styles.text}
                        //contentContainerStyle={styles.content}
                        // containerStyle={styles.container}
                        bottomDivider
                      />
                    </TouchableOpacity>
                  )

                })
              }
            </BottomSheet>
          </ScrollView>

        </View>

      )
    }
  }

}