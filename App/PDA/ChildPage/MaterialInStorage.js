import React from 'react';
import { View, StyleSheet, TouchableHighlight, Dimensions, ActivityIndicator, Alert, Image } from 'react-native';
import { Button, Text, Input, Icon, Card, ListItem, BottomSheet, Overlay, Header } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { ScrollView } from 'react-native-gesture-handler';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { NavigationEvents } from 'react-navigation';
import { styles } from '../Componment/PDAStyles';
import ListItemScan from '../Componment/ListItemScan';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import AvatarImg from '../Componment/AvatarImg';
import DatePicker from 'react-native-datepicker'
import ScanButton from '../Componment/ScanButton';

import { DeviceEventEmitter, NativeModules } from 'react-native';
import overlayImg from '../Componment/OverlayImg';
import OverlayImgZoomPicker from '../Componment/OverlayImgZoomPicker';
class BackIcon extends React.PureComponent {
    render() {
        return (
            <TouchableCustom
                onPress={this.props.onPress} >
                <Icon
                    name='arrow-back'
                    color='#419FFF' />
            </TouchableCustom>
        )
    }
}

let fileflag = "17"

let imageArr = [], imageFileArr = [];

let QRidArr = [] //扫码验重

const { height, width } = Dimensions.get('window') //获取宽高

export default class MaterialInStorage extends React.PureComponent {
    constructor() {
        super();
        this.state = {
            title: "",
            resData: [],
            rowguid: '',
            type: 'compid',
            inStorageNo: '', // 入库单号
            inStorageDate: '', // 入库日期
            supplier: '请选择', // 供应商名称
            supplierId: '',
            room: '请选择', // 库房
            roomId: '',
            totalMoney: '0', // 金额合计
            tax: '0', // 税额合计
            totalTax2money: '0', // 价税合计
            totalIncidental: '0', // 不含税运杂费
            freightTaxRate: '0', // 运杂费税额
            materConfirmCode: '', // 物资确认单号
            materTransCode: '', // 送货单号
            receivePeople: '', // 收料人
            remark: '', // 备注,

            materialInfos: [], // 材料明细
            roomList: [],

            roomVisible: false,

            //底栏控制
            bottomData: [],

            checked: true,

            focusIndex: 15,
            isLoading: false,

            imageArr: [],
            imageFileArr: [],
            pictureSelect: false,
            pictureUri: '',
            imageOverSize: false,
            fileid: "",
            recid: "",
            isPhotoUpdate: false,
            uriShow: '',

            //查询界面取消选择按钮
            isCheckPage: false,
            isCheckPageEdit: false,

            //控制app拍照，日期，相册选择功能
            isAppPhotoSetting: false,
            currShowImgIndex: 0,
            isAppDateSetting: false,
            isAppPhotoAlbumSetting: false
        }
    }

    componentDidMount() {
        const { navigation } = this.props;
        let guid = navigation.getParam('guid') || ''
        let pageType = navigation.getParam('pageType') || ''
        let title = navigation.getParam('title')
        let isAppPhotoSetting = navigation.getParam('isAppPhotoSetting')
        let isAppDateSetting = navigation.getParam('isAppDateSetting')
        let isAppPhotoAlbumSetting = navigation.getParam('isAppPhotoAlbumSetting')
        this.setState({
            isAppPhotoSetting: isAppPhotoSetting,
            isAppDateSetting: isAppDateSetting,
            isAppPhotoAlbumSetting: isAppPhotoAlbumSetting
        })
        this.setState({
            title: title
        })
        if (pageType == 'CheckPage') {
            let guid = navigation.getParam('guid') || ''
            this.checkResData(guid)
        } else {
            this.resData()

        }

        //通过使用DeviceEventEmitter模块来监听事件
        this.iDataScan = DeviceEventEmitter.addListener('iDataScan', (Event) => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 115 ~ SteelCage ~ Event.ScanResult", Event.ScanResult)
            if (typeof Event.ScanResult != undefined) {
                let data = Event.ScanResult
                let arr = data.split("=");
                let id = ''
                id = arr[arr.length - 1]
                console.log("🚀 ~ file: concrete_pouring.js ~ line 118 ~ SteelCage ~ id", id)
                if (this.state.type == 'compid') {
                    if (this.state.supplier == '请选择') {
                        this.toast.show('请选择供应商')
                        return
                    }
                    this.scanMaterial(id)
                }

            }
        });
    }

    componentWillUnmount() {
        imageArr = [], imageFileArr = [];
        QRidArr = []
        this.iDataScan.remove()
    }

    UpdateControl = () => {
        if (typeof this.props.navigation.getParam('QRid') != "undefined") {
            if (this.props.navigation.getParam('QRid') != "") {
                this.toast.show(Url.isLoadingView, 0)
            }
        }
        const { navigation } = this.props;
        const { } = this.state

        let type = navigation.getParam('type') || '';
        if (type == 'compid') {
            let QRid = navigation.getParam('QRid') || ''
            this.scanMaterial(QRid)
        }

        let mis = []
        mis = navigation.getParam('materialInfos') || []
        let state = navigation.getParam('state') || ""
        if (state == "selectSupplier") {
            let supplier = navigation.getParam('supplier') || ""
            let supplierId = navigation.getParam('supplierId') || ""
            this.setState({
                supplier: supplier,
                supplierId: supplierId
            })
        } else if (state == "addMaterial") {
            let totalMoney = this.state.totalMoney
            let tax = this.state.tax
            let totalTax2money = this.state.totalTax2money
            mis.map((item, index) => {
                totalMoney = (Math.round((Number.parseFloat(totalMoney) + Number.parseFloat(item.money)) * 100) / 100).toString()
                tax = (Math.round((Number.parseFloat(tax) + Number.parseFloat(item.tax)) * 100) / 100).toString()
                item._IsDel = "0"
            })
            this.state.materialInfos.map((item1, index1) => {
                let check = true
                mis.map((item2, index2) => {
                    if (item1.id == item2.id) {
                        check = false
                        return
                    }
                })
                if (check) mis.push(item1)
            })
            totalTax2money = (Math.round((Number.parseFloat(totalMoney) + Number.parseFloat(tax)) * 100) / 100).toString()
            this.setState({
                totalMoney: totalMoney,
                tax: tax,
                totalTax2money: totalTax2money,
                materialInfos: mis
            })
        } else if (state == "requisitionCount") {
            let pickingInfoId = navigation.getParam('pickingInfoId') || ""
            let inStorageAmount = navigation.getParam('inStorageAmount') || "0"
            let unitPrice = navigation.getParam('unitPrice') || "0"
            let taxratio = navigation.getParam('taxratio') || "0"
            let isTempPrice = navigation.getParam('isTempPrice')
            let money = navigation.getParam('price') || "0"
            let tax = navigation.getParam('tax') || "0"
            let notTaxUnitPrice = navigation.getParam('taxUnitPrice') || "0"
            let notTaxMoney = navigation.getParam('notTaxMoney') || "0"
            let remark = navigation.getParam('remark') || ""
            let totalMoney = '0'
            let taxCount = '0'
            let totalTax2money = '0'
            mis = this.state.materialInfos
            mis.map((item, index) => {
                if (item.id == pickingInfoId) {
                    item.inStorageAmount = inStorageAmount
                    item.unitPrice = unitPrice
                    item.taxratio = taxratio
                    item.isTempPrice = isTempPrice
                    item.money = money
                    item.tax = tax
                    item.notTaxUnitPrice = notTaxUnitPrice
                    item.notTaxMoney = notTaxMoney
                    item.remark = remark
                }
                totalMoney = (Math.round((Number.parseFloat(totalMoney) + Number.parseFloat(item.money)) * 100) / 100).toString()
                taxCount = (Math.round((Number.parseFloat(taxCount) + Number.parseFloat(item.tax)) * 100) / 100).toString()
                totalTax2money = (Math.round((Number.parseFloat(totalTax2money) + Number.parseFloat(item.notTaxMoney)) * 100) / 100).toString()
            })
            this.setState({
                materialInfos: mis,
                totalMoney: totalMoney,
                totalTax2money: totalTax2money,
                tax: taxCount
            })
        }

        this.props.navigation.setParams({
            type: "",
            state: "",
            materialInfos: []
        })

        this.forceUpdate()
    }

    //顶栏
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('title')
        }
    }

    resData = () => {
        let formData = new FormData();
        let data = {};
        data = {
            "action": "instorageinit",
            "servicetype": "pdamaterial",
            "express": "6DFD1C9B",
            "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
            "data": { "factoryId": Url.PDAFid }
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: MaterialInStorage.js ~ line 210 ~ MaterialInStorage ~ formData:", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res.statusText);
            //console.log(res);
            return res.json();
        }).then(resData => {
            console.log("🚀 ~ file: MaterialInStorage.js ~ line 222 ~ MaterialInStorage ~ resData:", resData)
            if (resData.status == '100') {
                //let dictionary = resData.result.defectTypes
                console.log("MaterialInStorage ---- 145 ----" + JSON.stringify(resData.result))
                this.setState({
                    //resData: resData.result,
                    rowguid: resData.result.rowguid,
                    inStorageNo: resData.result.inStorageNo,
                    inStorageDate: resData.result.inStorageDate,
                    roomList: resData.result.room,
                    receivePeople: Url.PDAEmployeeName,
                    isLoading: false
                })
            } else {
                Alert.alert("错误提示", resData.message, [{
                    text: '取消',
                    style: 'cancel'
                }, {
                    text: '返回上一级',
                    onPress: () => {
                        this.props.navigation.goBack()
                    }
                }])
            }

        }).catch((error) => {
            console.log("🚀 ~ file: MaterialInStorage.js ~ line 233 ~ MaterialInStorage ~ error:", error)
        });

    }

    checkResData = (guid) => {
        let formData = new FormData();
        let data = {};
        data = {
            "action": "instoragequerydetail",
            "servicetype": "pdamaterial",
            "express": "6DFD1C9B",
            "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
            "data": {
                "factoryId": Url.PDAFid,
                "guid": guid
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: MaterialInStorage.js ~ line 130 ~ MaterialInStorage ~ formData:", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res.statusText);
            //console.log(res);
            return res.json();
        }).then(resData => {
            if (resData.status == '100') {
                console.log("MaterialInStorage ---- 267 ----" + JSON.stringify(resData.result))
                imageArr = resData.result.fileAttach
                let fileAttach = resData.result.fileAttach
                if (fileAttach.length > 0) {
                    fileAttach.map((item, index) => {
                        let tmp = {}
                        tmp.fileid = ""
                        tmp.uri = item
                        tmp.url = item
                        imageFileArr.push(tmp)
                    })
                }
                this.setState({
                    imageFileArr: imageFileArr
                })
                let materialInfo = resData.result.materialInfo
                if (materialInfo != null && materialInfo != []) {
                    materialInfo.map((item, index) => {
                        item._IsDel = '0'
                    })
                }
                this.setState({
                    rowguid: resData.result.rowguid,
                    inStorageNo: resData.result.inStorageNo,
                    inStorageDate: resData.result.inStorageDate,
                    supplier: resData.result.supplierName,
                    supplierId: resData.result.supplierId,
                    room: resData.result.roomName,
                    roomId: resData.result.roomId,
                    totalMoney: resData.result.totalMoney,
                    tax: resData.result.tax,
                    totalTax2money: resData.result.totalTax2money,
                    totalIncidental: resData.result.totalIncidental,
                    freightTaxRate: resData.result.freightTaxRate,
                    materConfirmCode: resData.result.materConfirmCode,
                    materTransCode: resData.result.materTransCode,
                    receivePeople: resData.result.receivePeople,
                    remark: resData.result.remark,
                    materialInfos: materialInfo,
                    roomList: resData.result.room,
                    imageArr: resData.result.fileAttach,
                    pictureSelect: true,
                    isCheckPage: resData.result.state == '在编' ? false : true,
                    isCheckPageEdit: resData.result.state == '在编' ? true : false,
                    isLoading: false
                })
            } else {
                Alert.alert("错误提示", resData.message, [{
                    text: '取消',
                    style: 'cancel'
                }, {
                    text: '返回上一级',
                    onPress: () => {
                        this.props.navigation.goBack()
                    }
                }])
            }

        }).catch((error) => {
            console.log("🚀 ~ file: MaterialInStorage.js ~ line 233 ~ MaterialInStorage ~ error:", error)
        });
    }

    scanMaterial = (guid) => {
        let formData = new FormData();
        let data = {};
        let materialIds = ''
        if (QRidArr.indexOf(guid) != -1) {
            this.toast.show('材料重复')
            return
        }

        this.state.materialInfos.map((item, index) => {
            if (item._IsDel == '0') materialIds += index == this.state.materialInfos.length - 1 ? item.id : item.id + ','
        })
        data = {
            "action": "instoragescan",
            "servicetype": "pdamaterial",
            "express": "6DFD1C9B",
            "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
            "data": {
                "rowguid": guid,
                "materialIds": materialIds
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: MaterialInStorage.js ~ line 130 ~ MaterialInStorage ~ formData:", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res.statusText);
            //console.log(res);
            return res.json();
        }).then(resData => {
            this.toast.close()
            if (resData.status == '100') {
                console.log("MaterialInStorage ---- 267 ----" + JSON.stringify(resData.result))
                let result = resData.result
                let mis = this.state.materialInfos

                if (mis != []) {
                    result.map((item, index) => {
                        let check = true
                        this.state.materialInfos.map((item2, index2) => {
                            if (item.id == item2.id) {
                                check = false
                                return
                            }
                        })
                        item._IsDel = "0"
                        if (check) mis.push(item)
                    })
                } else {
                    result.map((item, index) => {
                        item._IsDel = "0"
                    })
                    mis = result
                }

                this.setState({
                    materialInfos: mis,
                }, () => {
                    this.forceUpdate()
                })
                QRidArr.push(guid)
            } else {
                Alert.alert("错误提示", resData.message)
            }

        }).catch((error) => {
            console.log("🚀 ~ file: MaterialInStorage.js ~ line 233 ~ MaterialInStorage ~ error:", error)
        });
    }

    PostData = (checkstate) => {
        const { supplier, room, materialInfos, isCheckPageEdit, recid } = this.state
        let formData = new FormData();
        let data = {};
        if (supplier == '请选择') {
            this.toast.show('请选择供应商')
            return
        } else if (room == '请选择') {
            this.toast.show('请选择库房')
            return
        } else if (materialInfos == []) {
            this.toast.show('请选择材料')
            return
        }

        if (this.state.isAppPhotoSetting) {
            if (imageArr.length == 0) {
                this.toast.show('请先拍摄照片');
                return
            }
        }

        let check = false
        materialInfos.map((item, index) => {
            if (item._IsDel == '0') {
                if (item.inStorageAmount == '') {
                    this.toast.show('材料' + item.code + '数量不能为空！')
                    check = true
                } else if (item.inStorageAmount == '0') {
                    this.toast.show('材料' + item.code + '数量不能为0！')
                    check = true
                } else if (item.unitPrice == '') {
                    this.toast.show('材料' + item.code + '单价不能为空！')
                    check = true
                } else if (item.unitPrice == '0') {
                    this.toast.show('材料' + item.code + '单价不能为0！')
                    check = true
                } else if (item.taxratio == '') {
                    this.toast.show('材料' + item.code + '税率不能为空！')
                    check = true
                }
            }
            if (check) return
        })
        if (check) return

        this.toast.show(saveLoading, 0)
        let MInStorage = this.MInStorageFormat(checkstate)
        let MInStorage_sub = this.MInStorageSubFormat()

        data = {
            "action": "instoragesave",
            "servicetype": "routematerial",
            "express": "6DFD1C9B",
            "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
            "userName": Url.PDAusername,
            "factoryId": Url.PDAFid,
            "data": {
                "MInStorage": MInStorage,
                "MInStorage_sub": MInStorage_sub,
                "DbActionType": isCheckPageEdit ? "Update" : "Add",
                "recid": recid
            },
        }
        if (Url.isAppNewUpload) {
            data.action = "instoragesavenew"
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: MaterialInStorage.js ~ line 476 ~ MaterialInStorage ~ formData", formData)
        if (!Url.isAppNewUpload) {
            imageArr.map((item, index) => {
                formData.append('img_' + index, {
                    uri: item,
                    type: 'image/jpeg',
                    name: 'img_' + index
                })
            })
        }
        fetch(Url.PDAurl, {
            method: 'post',
            headers: {
                'content-type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res);
            return res.json();
        }).then(resData => {
            //console.log("🚀 ~ file: MaterialInStorage.js ~ line 947 ~ MaterialInStorage ~ resdata", resData)
            if (resData.Success == true) {
                this.toast.show('保存成功');
                setTimeout(() => {
                    if (isCheckPageEdit) {
                        this.props.navigation.goBack()
                    } else {
                        this.props.navigation.replace(this.props.navigation.getParam("page"), {
                            title: this.props.navigation.getParam("title"),
                            pageType: this.props.navigation.getParam("pageType"),
                            page: this.props.navigation.getParam("page"),
                            isAppPhotoSetting: this.props.navigation.getParam("isAppPhotoSetting"),
                            isAppDateSetting: this.props.navigation.getParam("isAppDateSetting"),
                            isAppPhotoAlbumSetting: this.props.navigation.getParam("isAppPhotoAlbumSetting"),
                        })
                    }
                }, 1000)
            } else {
                Alert.alert('保存失败', resData.ErrMsg)
                //console.log('resdata', resData)
            }
        }).catch((error) => {
            console.log("error --- " + error)
        });

    }

    MInStorageFormat = (checkstate) => {
        const { rowguid, supplier, supplierId, room, roomId, totalMoney, totalIncidental, receivePeople, inStorageDate, totalTax2money, tax, remark, inStorageNo, materConfirmCode, freightTaxRate, materTransCode } = this.state
        let MInStorage = {}
        MInStorage._rowguid = rowguid
        MInStorage.factoryName = ''
        MInStorage.purchaseGuid = ''
        MInStorage.purchaseFormCode = ''
        MInStorage.supplierId = supplierId
        MInStorage.supplierName = supplier
        MInStorage.roomId = roomId
        MInStorage.roomName = room
        MInStorage.totalMoney = totalMoney
        MInStorage.totalIncidental = totalIncidental
        MInStorage.receivePeople = receivePeople
        MInStorage.inTime = inStorageDate
        MInStorage.receiverId = Url.PDAEmployeeId
        MInStorage.totalTax2money = totalTax2money
        MInStorage.rowGuidStorage = ''
        MInStorage.tax = tax
        MInStorage.isRawSet = ''
        MInStorage.isCommit = '0'
        MInStorage.remark = remark
        MInStorage.wbs = ''
        MInStorage.pwbs = ''
        MInStorage.isdisc = ''
        MInStorage.projectId = ''
        MInStorage.projectName = ''
        MInStorage.customerId = ''
        MInStorage.customerName = ''
        MInStorage.qcrkForm = ''
        MInStorage.discType = ''
        MInStorage.inType = 'none'
        MInStorage.formCode_M = ''
        MInStorage.formCode_G = ''
        MInStorage.formCode_H = ''
        MInStorage.formCode_PY = ''
        MInStorage.formCode = inStorageNo
        MInStorage.materConfirmCode = materConfirmCode
        MInStorage.materTransCode = materTransCode
        MInStorage.checkstate = checkstate
        MInStorage.formCode_PA = ''
        MInStorage.balanceGuid = ''
        MInStorage.balanceFormCode = ''
        MInStorage.appMsg = checkstate == '0' ? '' : '请审核'
        MInStorage.isClosed = '0'
        MInStorage.isPurchaseShare = ''
        MInStorage.CheckFormCode = ''
        MInStorage.CheckFormId = ''
        MInStorage.formCode_share = ''
        MInStorage.formCode_old = ''
        MInStorage.ShareMethod = ''
        MInStorage.totalIncidental_old = '.00'
        MInStorage.InStoGuid_old = ''
        MInStorage.FreightTaxRate = freightTaxRate
        MInStorage.isClosedToCurrentCycle = '1'
        MInStorage.formCode_shareAgain = ''
        MInStorage.formCode_shareOld = ''
        MInStorage.totalIncidentalAdd = '.00'
        MInStorage.differIncidental = '.00'
        MInStorage.InStoGuid_shareOld = ''
        MInStorage.supplierName2 = ''
        MInStorage.formCode_None = inStorageNo
        return MInStorage
    }

    MInStorageSubFormat = () => {
        const { rowguid, isCheckPageEdit, materialInfos, room, roomId } = this.state
        let MInStorageSub = []
        materialInfos.map((item, index) => {
            let MInStorageSubObj = {}
            let rowguid = item.rowguid;
            MInStorageSubObj._rowguid = isCheckPageEdit && rowguid != null ? item.rowguid : this.getRandom()
            MInStorageSubObj.mainid = '0'
            MInStorageSubObj.applyGuid = ''
            MInStorageSubObj.applyFormCode = ''
            MInStorageSubObj.applySubGuid = ''
            MInStorageSubObj.purchaseGuid = ''
            MInStorageSubObj.purchaseFormCode = ''
            MInStorageSubObj.purchaseSubGuid = ''
            MInStorageSubObj.projectId = ''
            MInStorageSubObj.projectName = ''
            MInStorageSubObj.roomId = roomId
            MInStorageSubObj.roomName = room
            MInStorageSubObj.disc = ''
            MInStorageSubObj.materialId = item.materialId
            MInStorageSubObj.materialCode = item.code
            MInStorageSubObj.materialName = item.name
            MInStorageSubObj.materialSize = item.size
            MInStorageSubObj.unit = item.unit
            MInStorageSubObj.unitPrice = item.unitPrice
            MInStorageSubObj.number = item.inStorageAmount
            MInStorageSubObj.money = item.money
            MInStorageSubObj.synumber = '.00'
            MInStorageSubObj.taxratio = item.taxratio
            MInStorageSubObj.tax = item.tax
            MInStorageSubObj.notTaxMoney = item.notTaxMoney
            MInStorageSubObj.remark = item.remark
            MInStorageSubObj.orgSubRowGuid = ''
            MInStorageSubObj.notTaxUnitPrice = item.notTaxUnitPrice
            MInStorageSubObj.needDate = ''
            MInStorageSubObj.applyTeam = ''
            MInStorageSubObj.applyTeamId = ''
            MInStorageSubObj.state = '0'
            MInStorageSubObj.IsTempPrice = item.isTempPrice == '否' ? '0' : '1'
            MInStorageSubObj.number_share = '.0000'
            MInStorageSubObj.unitPrice_share = '.000000'
            MInStorageSubObj.money_share = '.00'
            MInStorageSubObj.CheckSubGuid = ''
            MInStorageSubObj.IsRawSet = ''
            MInStorageSubObj.money_shareAdd = '.00'
            MInStorageSubObj._IsDel = item._IsDel

            MInStorageSub.push(MInStorageSubObj)
        })
        return MInStorageSub
    }

    getRandom = () => {
        var arr = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"],
            num = "";
        for (var i = 0; i < 32; i++) {
            num += arr[parseInt(Math.random() * 36)];
        }
        return num;
    }

    // 时间工具
    _DatePicker = (index) => {
        //const { ServerTime } = this.state
        return (
            <DatePicker
                customStyles={{
                    dateInput: styles.dateInput,
                    dateTouchBody: styles.dateTouchBody,
                    dateText: this.state.isAppDateSetting ? styles.dateText : styles.dateDisabled,
                }}
                iconComponent={<Icon name='caretdown' color={this.state.isAppDateSetting ? '#419fff' : "#666"} iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
                }
                showIcon={true}
                date={this.state.inStorageDate}
                onOpenModal={() => {
                    this.setState({ focusIndex: index })
                }}
                format="YYYY-MM-DD"
                mode="date"
                disabled={!this.state.isAppDateSetting}
                onDateChange={(value) => {
                    this.setState({
                        inStorageDate: value
                    })
                }}
            />
        )
    }

    // 正浮点数限制
    chkPrice(obj) { //方法1
        obj = obj.replace(/[^\d.]/g, "");
        //必须保证第一位为数字而不是. 
        obj = obj.replace(/^\./g, "");
        //保证只有出现一个.而没有多个. 
        obj = obj.replace(/\.{2,}/g, ".");
        //保证.只出现一次，而不能出现两次以上 
        obj = obj.replace(".", "$#$").replace(/\./g, "").replace("$#$", ".");
        return obj;
    }

    uploadImage = () => {
        const { pictureSelect, isCheckPage, pageType } = this.state
        return (
            <View style={{ flexDirection: 'row' }}>
                {/*{this.imageView()}*/}
                {/*{*/}
                {/*    isCheckPage ? <View /> :*/}
                {/*        <AvatarAdd*/}
                {/*            pictureSelect={pictureSelect}*/}
                {/*            backgroundColor='#4D8EF5'*/}
                {/*            color='white'*/}
                {/*            title="材"*/}
                {/*            onPress={() => {*/}
                {/*                if (Url.isUsePhotograph == "YES") {*/}
                {/*                    if (pageType == 'CheckPage') {*/}
                {/*                        this.toast.show('照片不可修改')*/}
                {/*                    } else {*/}
                {/*                        this.camandlibFunc()*/}
                {/*                    }*/}
                {/*                } else {*/}
                {/*                    this.cameraFunc()*/}
                {/*                }*/}
                {/*            }} />*/}
                {/*}*/}
                {
                    !isCheckPage ? <Icon name='camera-alt' color='#4D8EF5' iconProps={{ size: 32 }} onPress={() => {
                        if (this.state.isAppPhotoAlbumSetting) {
                            this.camandlibFunc()
                        } else {
                            this.cameraFunc()
                        }
                    }}
                    ></Icon> : <View />

                }
            </View>
        )

    }

    camandlibFunc = () => {
        Alert.alert(
            '提示信息',
            '选择拍照方式',
            [{
                text: '取消',
                style: 'cancel'
            }, {
                text: '拍照',
                onPress: () => {
                    this.cameraFunc()
                }
            }, {
                text: '相册',
                onPress: () => {
                    this.camLibraryFunc()
                }
            }])
    }

    cameraFunc = () => {
        launchCamera(
            {
                mediaType: 'photo',
                includeBase64: false,
                maxHeight: parseInt(Url.isResizePhoto),
                maxWidth: parseInt(Url.isResizePhoto),
                saveToPhotos: true,

            },
            (response) => {
                if (response.uri == undefined) {
                    return
                }
                imageArr.push(response.uri)
                this.setState({
                    pictureSelect: true,
                    pictureUri: response.uri,
                    imageArr: imageArr
                })
                if (Url.isAppNewUpload) {
                    this.cameraPost(response.uri)
                } else {
                    this.setState({
                        pictureSelect: true,
                    })
                }
            },
        )
    }

    camLibraryFunc = () => {
        launchImageLibrary(
            {
                mediaType: 'photo',
                includeBase64: false,
                maxHeight: parseInt(Url.isResizePhoto),
                maxWidth: parseInt(Url.isResizePhoto),
            },
            (response) => {
                if (response.uri == undefined) {
                    return
                }
                imageArr.push(response.uri)
                this.setState({

                    pictureUri: response.uri,
                    imageArr: imageArr
                })
                if (Url.isAppNewUpload) {
                    this.cameraPost(response.uri)
                } else {
                    this.setState({
                        pictureSelect: true,
                    })
                }
            },
        )
    }

    // 展示/隐藏 放大图片
    handleZoomPicture = (flag, index) => {
        this.setState({
            imageOverSize: false,
            currShowImgIndex: index || 0
        })
    }

    // 加载放大图片弹窗
    renderZoomPicture = () => {
        const { imageOverSize, currShowImgIndex, imageFileArr } = this.state
        return (
            <OverlayImgZoomPicker
                isShowImage={imageOverSize}
                currShowImgIndex={currShowImgIndex}
                zoomImages={imageFileArr}
                callBack={(flag) => this.handleZoomPicture(flag)}
            ></OverlayImgZoomPicker>
        )
    }

    cameraPost = (uri) => {
        const { recid } = this.state
        this.toast.show('图片上传中', 2000)

        let formData = new FormData();
        let data = {};
        data = {
            "action": "savephote",
            "servicetype": "photo_file",
            "express": "D2979AB2",
            "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
            "data": {
                "fileflag": fileflag,
                "username": Url.PDAusername,
                "recid": recid
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        formData.append('img', {
            uri: uri,
            type: 'image/jpeg',
            name: 'img'
        })
        console.log("🚀 ~ file: concrete_pouring.js ~ line 672 ~ SteelCage ~ formData", formData)

        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            console.log(res.statusText);
            console.log(res);
            return res.json();
        }).then(resData => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 690 ~ SteelCage ~ resData", resData)
            if (resData.status == '100') {
                let fileid = resData.result.fileid;
                let recid = resData.result.recid;

                let tmp = {}
                tmp.fileid = fileid
                tmp.uri = uri
                tmp.url = uri
                imageFileArr.push(tmp)
                this.setState({
                    fileid: fileid,
                    recid: recid,
                    pictureSelect: true,
                    imageFileArr: imageFileArr
                })
                console.log("🚀 ~ file: concrete_pouring.js ~ line 704 ~ SteelCage ~ imageFileArr", imageFileArr)
                this.toast.show('图片上传成功');
                this.setState({
                    isPhotoUpdate: false
                })
            } else {
                Alert.alert('图片上传失败', resData.message)
                this.toast.close(1)
            }
        }).catch((error) => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 692 ~ SteelCage ~ error", error)
        });
    }

    cameraDelete = (item) => {
        let i = imageArr.indexOf(item)

        let formData = new FormData();
        let data = {};
        data = {
            "action": "deletephote",
            "servicetype": "photo_file",
            "express": "D2979AB2",
            "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
            "data": {
                "fileid": imageFileArr[i].fileid,
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: concrete_pouring.js ~ line 733 ~ SteelCage ~ cameraDelete ~ formData", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            console.log(res.statusText);
            console.log(res);
            return res.json();
        }).then(resData => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 745 ~ SteelCage ~ cameraDelete ~ resData", resData)
            if (resData.status == '100') {
                if (i > -1) {
                    imageArr.splice(i, 1)
                    imageFileArr.splice(i, 1)
                    this.setState({
                        imageArr: imageArr,
                        imageFileArr: imageFileArr
                    }, () => {
                        this.forceUpdate()
                    })
                }
                if (imageArr.length == 0) {
                    this.setState({
                        pictureSelect: false
                    })
                }
                this.toast.show('图片删除成功');
            } else {
                Alert.alert('图片删除失败', resData.message)
                this.toast.close(1)
            }
        }).catch((error) => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 761 ~ SteelCage ~ cameraDelete ~ error", error)
        });
    }

    imageView = () => {
        return (
            <View style={{ flexDirection: 'row' }}>
                {
                    this.state.pictureSelect ?
                        this.state.imageArr.map((item, index) => {
                            return (
                                <AvatarImg
                                    item={item}
                                    index={index}
                                    onPress={() => {
                                        this.setState({
                                            imageOverSize: true,
                                            pictureUri: item
                                        })
                                    }}
                                    onLongPress={() => {
                                        Alert.alert(
                                            '提示信息',
                                            '是否删除构件照片',
                                            [{
                                                text: '取消',
                                                style: 'cancel'
                                            }, {
                                                text: '删除',
                                                onPress: () => {
                                                    this.cameraDelete(item)
                                                }
                                            }])
                                    }} />
                            )
                        }) : <View></View>
                }
            </View>
        )
    }

    // 下拉菜单
    downItem = (index) => {
        const { room, roomList } = this.state
        switch (index) {
            case '3': return (
                // 库房名称
                <View>{(
                    <TouchableCustom onPress={() => { this.isItemVisible(roomList, index) }} >
                        <IconDown text={room} />
                    </TouchableCustom>
                )}</View>
            ); break;

            default: return;
        }
    }

    isItemVisible = (data, focusIndex) => {
        //console.log("************** " + focusIndex + "-" + i)
        if (typeof (data) != "undefined") {
            if (data != [] && data != "") {
                switch (focusIndex) {
                    case '3': this.setState({ roomVisible: true, bottomData: data, focusIndex: focusIndex }); break;

                    default: return;
                }
            } else {
                this.toast.show("无数据")
            }
        } else {
            this.toast.show("无数据")
        }
    }

    jumpMaterialPickingInfo = (item) => {
        const { title } = this.state
        this.props.navigation.navigate('MaterialPickingInfo', {
            title: '入库信息',
            action: title,
            storageId: item.id,
            storageCode: item.code,
            storageName: item.name,
            size: item.size,
            unit: item.unit,
            inStorageAmount: item.inStorageAmount == '0' ? '' : item.inStorageAmount,
            //canInStorageAmount: item.canInStorageAmount,
            unitPrice: item.unitPrice == '0' ? '' : item.unitPrice,
            taxratio: item.taxratio == '0' ? '' : item.taxratio,
            isTempPrice: item.isTempPrice,
            price: item.money,
            tax: item.tax,
            taxUnitPrice: item.notTaxUnitPrice,
            notTaxMoney: item.notTaxMoney == '0' ? '' : item.notTaxMoney,
            remark: item.remark
        })
    }

    render() {
        const { navigation } = this.props;
        //从主页面传url, 未来集成到一起
        const QRurl = navigation.getParam('QRurl') || '';
        const QRid = navigation.getParam('QRid') || ''
        const type = navigation.getParam('type') || '';
        let pageType = navigation.getParam('pageType') || ''

        const { title, bottomData, isCheckPage, focusIndex, inStorageNo, inStorageDate, supplier, room, roomVisible, materialInfos, remark, supplierId, materialIds, totalMoney, tax, totalTax2money, totalIncidental, freightTaxRate, materConfirmCode, materTransCode, receivePeople, roomList } = this.state

        if (this.state.isLoading) {
            return (
                <View style={styles.loading}>
                    <ActivityIndicator
                        animating={true}
                        color='#419FFF'
                        size="large" />
                </View>
            )
            //渲染页面
        } else {
            return (
                <View style={{ flex: 1 }}>
                    <Toast ref={(ref) => { this.toast = ref; }} position="center" />
                    <NavigationEvents
                        onWillFocus={this.UpdateControl}
                        onWillBlur={() => {
                            if (QRid != '') {
                                navigation.setParams({ QRid: '', type: '' })
                            }
                        }} />
                    <ScrollView ref={ref => this.scoll = ref} style={styles.mainView} showsVerticalScrollIndicator={false} >
                        <View style={styles.listView}>

                            <ListItemScan focusStyle={focusIndex == '0' ? styles.focusColor : {}} title='入库单号:' rightElement={inStorageNo} />
                            <ListItemScan focusStyle={focusIndex == '1' ? styles.focusColor : {}} title='入库日期:' rightElement={() =>
                                isCheckPage ? <Text>{inStorageDate}</Text> :
                                    this._DatePicker('1')} />
                            {
                                isCheckPage ? <ListItemScan focusStyle={focusIndex == '2' ? styles.focusColor : {}} title='供应商名称:' rightElement={supplier} /> :
                                    <ListItemScan focusStyle={focusIndex == '2' ? styles.focusColor : {}} title='供应商名称:' rightElement={
                                        <View>{(
                                            <TouchableCustom onPress={() => {
                                                this.props.navigation.navigate('SelectSupplier', {
                                                    title: "选择供应商"
                                                })
                                            }} >
                                                <IconDown text={supplier} />
                                            </TouchableCustom>
                                        )}</View>
                                    } />
                            }
                            {
                                isCheckPage ? <ListItemScan focusStyle={focusIndex == '3' ? styles.focusColor : {}} title='库房名称:' rightElement={room} /> :
                                    <TouchableCustom underlayColor={'lightgray'} onPress={() => { this.isItemVisible(roomList, '3') }}>
                                        <ListItemScan focusStyle={focusIndex == '3' ? styles.focusColor : {}} title='库房名称:' rightElement={this.downItem('3')} />
                                    </TouchableCustom>
                            }
                            {
                                isCheckPage ? <View /> :
                                    //<View style={{ flexDirection: 'row', paddingHorizontal: 10, justifyContent: 'center', paddingVertical: 11 }}>
                                    //    <Button
                                    //        titleStyle={{ fontSize: 14 }}
                                    //        title='选择材料'
                                    //        //iconRight={true}
                                    //        //icon={<Icon type='antdesign' name='scan1' color='white' size={14} />}
                                    //        type='solid'
                                    //        buttonStyle={{ paddingVertical: 6, backgroundColor: '#17BC29', marginVertical: 0 }}
                                    //        onPress={() => {
                                    //            if (supplier == '请选择') {
                                    //                this.toast.show('请选择供应商')
                                    //                return
                                    //            }
                                    //            let materialIds = ''
                                    //            materialInfos.map((item, index) => {
                                    //                materialIds += index == materialInfos.length - 1 ? item.id : item.id + ','
                                    //            })
                                    //            this.props.navigation.navigate('SelectMaterial', {
                                    //                title: '选择材料',
                                    //                action: title,
                                    //                supplierId: supplierId,
                                    //                materialIds: materialIds
                                    //            })
                                    //        }}
                                    //    />
                                    //    <Text>   </Text>
                                    //    <Text>   </Text>
                                    //    <Icon type='material-community' name='qrcode-scan' color='#4D8EF5' style={{ marginTop: 4 }} onPress={() => {
                                    //        if (supplier == '请选择') {
                                    //            this.toast.show('请选择供应商')
                                    //            return
                                    //        }
                                    //        setTimeout(() => {
                                    //            this.setState({ GetError: false, buttondisable: false })
                                    //            varGetError = false
                                    //        }, 1500)
                                    //        this.props.navigation.navigate('QRCode', {
                                    //            type: 'compid',
                                    //            page: 'MaterialInStorage'
                                    //        })
                                    //    }} />
                                    //</View>

                                    <ListItemScan
                                        focusStyle={focusIndex == '15' ? styles.focusColor : {}}
                                        title='材料明细:'
                                        onPressIn={() => {
                                            if (supplier == '请选择') {
                                                this.toast.show('请选择供应商')
                                                return
                                            }
                                            this.setState({
                                                type: 'compid',
                                                focusIndex: 15
                                            })
                                            NativeModules.PDAScan.onScan();
                                        }}
                                        onPress={() => {
                                            if (supplier == '请选择') {
                                                this.toast.show('请选择供应商')
                                                return
                                            }
                                            this.setState({
                                                type: 'compid',
                                                focusIndex: 15
                                            })
                                            NativeModules.PDAScan.onScan();
                                        }}
                                        onPressOut={() => {
                                            NativeModules.PDAScan.offScan();
                                        }}
                                        rightElement={
                                            <View style={{ flexDirection: 'row' }}>

                                                <Button
                                                    titleStyle={{ fontSize: 14 }}
                                                    title='选择材料'
                                                    type='solid'
                                                    buttonStyle={{ paddingVertical: 6, backgroundColor: '#18BC28', marginVertical: 0 }}
                                                    onPress={() => {
                                                        if (supplier == '请选择') {
                                                            this.toast.show('请选择供应商')
                                                            return
                                                        }
                                                        this.setState({
                                                            type: 'compid',
                                                            focusIndex: 15
                                                        })
                                                        let materialIds = ''
                                                        materialInfos.map((item, index) => {
                                                            if (item._IsDel == '0') materialIds += index == materialInfos.length - 1 ? item.id : item.id + ','
                                                        })
                                                        this.props.navigation.navigate('SelectMaterial', {
                                                            title: '选择材料',
                                                            action: title,
                                                            supplierId: supplierId,
                                                            materialIds: materialIds
                                                        })
                                                    }}
                                                />
                                                <Text>   </Text>
                                                <ScanButton
                                                    onPress={() => {
                                                        if (supplier == '请选择') {
                                                            this.toast.show('请选择供应商')
                                                            return
                                                        }
                                                        this.setState({
                                                            type: 'compid',
                                                            focusIndex: 15
                                                        })
                                                        this.props.navigation.navigate('QRCode', {
                                                            type: 'compid',
                                                            page: 'MaterialInStorage'
                                                        })
                                                    }}
                                                    onLongPress={() => {
                                                        this.setState({
                                                            type: 'compid',
                                                            focusIndex: 15
                                                        })
                                                        NativeModules.PDAScan.onScan();
                                                    }}
                                                    onPressOut={() => {
                                                        NativeModules.PDAScan.offScan();
                                                    }}
                                                />
                                            </View>
                                        } />
                            }

                            {
                                materialInfos.map((item, index) => {
                                    return (
                                        item._IsDel == '1' ? <View /> : <TouchableCustom
                                            onLongPress={() => {
                                                !isCheckPage ?
                                                    Alert.alert(
                                                        '提示信息',
                                                        '是否删除材料信息',
                                                        [{
                                                            text: '取消',
                                                            style: 'cancel'
                                                        }, {
                                                            text: '删除',
                                                            onPress: () => {
                                                                let mis = this.state.materialInfos
                                                                //mis.splice(index, 1)
                                                                item._IsDel = "1"
                                                                let totalMoney = this.state.totalMoney
                                                                let tax = this.state.tax
                                                                let totalTax2money = this.state.totalTax2money
                                                                totalMoney = (Math.round((Number.parseFloat(totalMoney) - Number.parseFloat(item.money)) * 100) / 100).toString()
                                                                tax = (Math.round((Number.parseFloat(tax) - Number.parseFloat(item.tax)) * 100) / 100).toString()
                                                                totalTax2money = (Math.round((Number.parseFloat(totalMoney) + Number.parseFloat(tax)) * 100) / 100).toString()
                                                                this.setState({
                                                                    totalMoney: totalMoney,
                                                                    tax: tax,
                                                                    totalTax2money: totalTax2money,
                                                                    materialInfos: mis
                                                                })
                                                                this.forceUpdate()
                                                            }
                                                        }]) : ""
                                            }}
                                            onPress={() => { isCheckPage ? <View /> : this.jumpMaterialPickingInfo(item) }}
                                        >
                                            <View>
                                                <Card containerStyle={stylesMeterial.card_containerStyle}>
                                                    <View style={[{
                                                        flexDirection: 'row',
                                                        justifyContent: 'flex-start',
                                                        alignItems: 'center',
                                                    }]}>
                                                        <View style={{ flex: 1 }}>
                                                            <ListItem
                                                                containerStyle={stylesMeterial.list_container_style}
                                                                title={item.code}
                                                                rightTitle={'单价:' + item.unitPrice + '元'}
                                                                titleStyle={stylesMeterial.title}
                                                                rightTitleStyle={stylesMeterial.rightTitle}
                                                            />
                                                            <ListItem
                                                                containerStyle={stylesMeterial.list_container_style}
                                                                title={item.name}
                                                                rightTitle={'金额:' + item.money + '元'}
                                                                titleStyle={stylesMeterial.title}
                                                                rightTitleStyle={stylesMeterial.rightTitle}
                                                            />
                                                            <ListItem
                                                                containerStyle={stylesMeterial.list_container_style}
                                                                title={item.size}
                                                                rightTitle={'税率:' + item.taxratio + '%'}
                                                                titleStyle={stylesMeterial.title}
                                                                rightTitleStyle={stylesMeterial.rightTitle}
                                                            />
                                                            <ListItem
                                                                containerStyle={stylesMeterial.list_container_style}
                                                                title={'数量:' + item.inStorageAmount + "(" + item.unit + ")"}
                                                                rightTitle={'税额:' + item.tax}
                                                                titleStyle={stylesMeterial.title}
                                                                rightTitleStyle={stylesMeterial.rightTitle}
                                                            />
                                                            <ListItem
                                                                containerStyle={stylesMeterial.list_container_style}
                                                                title={'是否暂估:' + item.isTempPrice}
                                                                rightTitle={'含税金额:' + item.notTaxMoney}
                                                                titleStyle={stylesMeterial.title}
                                                                rightTitleStyle={stylesMeterial.rightTitle}
                                                            />
                                                        </View>
                                                        {
                                                            isCheckPage ? <View /> :
                                                                <Icon type='antdesign' name='right' color='#999' onPress={() => { this.jumpMaterialPickingInfo(item) }} />
                                                        }
                                                    </View>
                                                </Card>
                                                <Text />
                                            </View>
                                        </TouchableCustom>
                                    )
                                })
                            }

                            <ListItemScan focusStyle={focusIndex == '4' ? styles.focusColor : {}} title='金额合计:' rightElement={totalMoney} />
                            <ListItemScan focusStyle={focusIndex == '5' ? styles.focusColor : {}} title='税额合计:' rightElement={tax} />
                            <ListItemScan focusStyle={focusIndex == '6' ? styles.focusColor : {}} title='价税合计:' rightElement={totalTax2money} />
                            <ListItemScan focusStyle={focusIndex == '7' ? styles.focusColor : {}} title='不含税运杂费:' rightElement={
                                isCheckPage ? <Text>{totalIncidental}</Text> :
                                    <View>
                                        <Input
                                            containerStyle={styles.quality_input_container}
                                            inputContainerStyle={styles.inputContainerStyle}
                                            inputStyle={[styles.quality_input_, { top: 7 }]}
                                            placeholder='请填写'
                                            keyboardType='numeric'
                                            value={totalIncidental}
                                            onChangeText={(value) => {
                                                this.setState({
                                                    totalIncidental: this.chkPrice(value)
                                                })
                                            }} />
                                    </View>
                            } />
                            <ListItemScan focusStyle={focusIndex == '8' ? styles.focusColor : {}} title='运杂费税额:' rightElement={
                                isCheckPage ? <Text>{freightTaxRate}</Text> :
                                    <View>
                                        <Input
                                            containerStyle={styles.quality_input_container}
                                            inputContainerStyle={styles.inputContainerStyle}
                                            inputStyle={[styles.quality_input_, { top: 7 }]}
                                            placeholder='请填写'
                                            keyboardType='numeric'
                                            value={freightTaxRate}
                                            onChangeText={(value) => {
                                                this.setState({
                                                    freightTaxRate: this.chkPrice(value)
                                                })
                                            }} />
                                    </View>
                            } />
                            <ListItemScan focusStyle={focusIndex == '9' ? styles.focusColor : {}} title='物资确认单号:' rightElement={
                                isCheckPage ? <Text>{materConfirmCode}</Text> :
                                    <View>
                                        <Input
                                            containerStyle={styles.quality_input_container}
                                            inputContainerStyle={styles.inputContainerStyle}
                                            inputStyle={[styles.quality_input_, { top: 7 }]}
                                            placeholder='请填写'
                                            value={materConfirmCode}
                                            onChangeText={(value) => {
                                                this.setState({
                                                    materConfirmCode: value
                                                })
                                            }} />
                                    </View>
                            } />
                            <ListItemScan focusStyle={focusIndex == '10' ? styles.focusColor : {}} title='送货单号:' rightElement={
                                isCheckPage ? <Text>{materTransCode}</Text> :
                                    <View>
                                        <Input
                                            containerStyle={styles.quality_input_container}
                                            inputContainerStyle={styles.inputContainerStyle}
                                            inputStyle={[styles.quality_input_, { top: 7 }]}
                                            placeholder='请填写'
                                            value={materTransCode}
                                            onChangeText={(value) => {
                                                this.setState({
                                                    materTransCode: value
                                                })
                                            }} />
                                    </View>
                            } />
                            <ListItemScan focusStyle={focusIndex == '11' ? styles.focusColor : {}} title='收料人:' rightElement={receivePeople} />
                            <ListItemScan focusStyle={focusIndex == '12' ? styles.focusColor : {}} title='备注:' rightElement={
                                isCheckPage ? <Text>{remark}</Text> :
                                    <View>
                                        <Input
                                            containerStyle={styles.quality_input_container}
                                            inputContainerStyle={styles.inputContainerStyle}
                                            inputStyle={[styles.quality_input_, { top: 7 }]}
                                            placeholder='请填写'
                                            value={remark}
                                            onChangeText={(value) => {
                                                this.setState({
                                                    remark: value
                                                })
                                            }} />
                                    </View>} />
                            <ListItemScan focusStyle={focusIndex == '13' ? styles.focusColor : {}} title='附件:' rightElement={() =>
                                this.uploadImage()} />
                            {
                                this.state.pictureSelect && this.state.imageArr != [] && this.state.imageArr != "" ?
                                    <ListItemScan focusStyle={focusIndex == '14' ? styles.focusColor : {}} title={this.imageView()} />
                                    : <View />
                            }

                        </View>
                        {
                            isCheckPage ? <Text /> :
                                <View style={{ flexDirection: 'row' }}>
                                    <View style={{ flex: 1 }}><Button
                                        title='暂存'
                                        titleStyle={{ color: "white", fontSize: 17 }}
                                        type='solid'
                                        style={{ marginVertical: 10 }}
                                        containerStyle={{ marginVertical: 10, marginHorizontal: 8 }}
                                        buttonStyle={{ backgroundColor: '#17BC29', borderRadius: 10, paddingVertical: 16 }}
                                        onPress={() => {
                                            this.PostData("0")
                                        }}
                                        disabled={this.state.isPhotoUpdate}
                                    ></Button></View>
                                    <View style={{ flex: 1 }}><Button
                                        title='报审'
                                        titleStyle={{ color: "white", fontSize: 17 }}
                                        type='solid'
                                        style={{ marginVertical: 10 }}
                                        containerStyle={{ marginVertical: 10, marginHorizontal: 8 }}
                                        buttonStyle={{ backgroundColor: '#FF9E00', borderRadius: 10, paddingVertical: 16 }}
                                        onPress={() => {
                                            this.PostData("1")
                                        }}
                                        disabled={this.state.isPhotoUpdate}
                                    ></Button></View>
                                </View>
                        }

                        {/*库房名称底栏*/}
                        <BottomSheet
                            onBackdropPress={() => {
                                this.setState({
                                    roomVisible: false
                                })
                            }}
                            isVisible={roomVisible && !this.state.isCheckPage} onRequestClose={() => {
                                this.setState({
                                    roomVisible: false
                                })
                            }}
                        >
                            <ScrollView style={styles.bottomsheetScroll}>
                                {bottomData.map((item, index) => {
                                    return (
                                        <TouchableCustom
                                            onPress={() => {
                                                this.setState({
                                                    room: item.name,
                                                    roomVisible: false,
                                                    roomId: item.id,
                                                    materialInfos: []
                                                })
                                                QRidArr = []
                                            }}
                                        >
                                            <View style={{
                                                paddingVertical: 18,
                                                backgroundColor: 'white',
                                                //alignItems: 'center',
                                                borderBottomColor: '#DDDDDD',
                                                borderBottomWidth: 0.6,
                                                flexDirection: 'row',
                                                justifyContent: 'space-between',
                                                paddingLeft: 20,
                                                paddingRight: 20
                                            }}>
                                                <Text style={{ color: '#333', fontSize: 14 }}>{item.name}</Text>
                                                <Text style={{ color: '#333', fontSize: 14 }}>{item.code}</Text>
                                            </View>
                                        </TouchableCustom>
                                    )
                                })}
                            </ScrollView>
                            <TouchableHighlight
                                onPress={() => {
                                    this.setState({ roomVisible: false })
                                }}>
                                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
                            </TouchableHighlight>
                        </BottomSheet>

                        {this.renderZoomPicture()}

                        {/* {overlayImg(obj = {
                            onRequestClose: () => {
                                this.setState({ imageOverSize: !this.state.imageOverSize }, () => {
                                    console.log("🚀 ~ file: equipment_maintenance.js:1696 ~ render ~ imageOverSize:", this.state.imageOverSize)
                                })
                            },
                            onPress: () => {
                                this.setState({
                                    imageOverSize: !this.state.imageOverSize
                                })
                            },
                            uri: this.state.uriShow,
                            isVisible: this.state.imageOverSize
                        })
                        } */}


                    </ScrollView>
                </View>
            )
        }
    }
}

const stylesMeterial = StyleSheet.create({
    card_containerStyle: {
        borderRadius: 10,
        shadowOpacity: 0,
        backgroundColor: '#f8f8f8',
        borderWidth: 0,
        paddingVertical: 13,
        elevation: 0
    },
    title: {
        fontSize: 13,
        color: '#999'
    },
    rightTitle: {
        fontSize: 13,
        textAlign: 'right',
        lineHeight: 14,
        flexWrap: 'wrap',
        width: width / 2,
        color: '#333'
    },
    list_container_style: { marginVertical: 0, paddingVertical: 9, backgroundColor: 'transparent' }

})