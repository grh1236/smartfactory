import React from 'react';
import { View, TouchableOpacity, TouchableHighlight, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, Input, Icon, Badge, Card, ListItem, BottomSheet, CheckBox, Avatar, Overlay, Header } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { RFT } from '../../Url/Pixal';
import { ScrollView } from 'react-native-gesture-handler';
import CardList from "../Componment/CardList";
import DatePicker from 'react-native-datepicker'
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { Image } from 'react-native';
import { NavigationEvents } from 'react-navigation';
import { styles } from '../Componment/PDAStyles';
import ScanButton from '../Componment/ScanButton';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import ListItemScan_child from '../Componment/ListItemScan_child';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import CheckBoxScan from '../Componment/CheckBoxScan';
import AvatarAdd from '../Componment/AvatarAdd';
import AvatarImg from '../Componment/AvatarImg';
import DeviceStorage from '../../Url/DeviceStorage';
import Pdf from 'react-native-pdf';
import { DeviceEventEmitter, NativeModules } from 'react-native';
import overlayImg from '../Componment/OverlayImg';
import OverlayImgZoomPicker from '../Componment/OverlayImgZoomPicker';


let varGetError = false

let DeviceStorageData = {} //缓存数据

const { height, width } = Dimensions.get('window') //获取宽高

let inputComId = ''

let fileflag = "10"

let QRcompid = '', QRtaiwanId = '', QRacceptanceId = '', QRmonitorId = '', QRInspectorId = ''

let imageArr = [], imageFileArr = []

class BackIcon extends React.PureComponent {
  render() {
    return (
      <TouchableCustom
        onPress={this.props.onPress} >
        <Icon
          name='arrow-back'
          color='#419FFF' />
      </TouchableCustom>
    )
  }
}


export default class QualityInspection extends React.PureComponent {
  constructor() {
    super();
    this.state = {
      resData: [],
      rowguid: '',
      ServerTime: '2000-01-01 00:00',
      steelCageIDs: ' ',
      monitorSetup: '',
      monitors: [],
      monitorId: '',
      monitorName: '',
      monitorSelect: '请选择',
      monitorTeamId: '',
      monitorTeamName: '',
      Inspectors: [],
      InspectorId: '',
      InspectorName: '',
      InspectorSelect: '请选择',
      acceptanceId: '',
      acceptanceName: '',
      checkResult: '合格',
      taiwanId: '',
      type: 'compid',
      compData: {},
      compId: '',
      steelLabel: '',
      compCode: '',
      Tai_QRname: '',
      editEmployeeName: '',
      checked: true,
      //是否扫码成功
      isGetcomID: false,
      isGettaiwanId: false,
      isGetTeamPeopleInfo: false,
      isGetCommonMould: false,
      isGetInspector: false,
      //长按删除功能控制
      iscomIDdelete: false,
      istaiwanIddelete: false,
      isCommonMoulddelete: false,
      isTeamPeopledelete: false,
      isInspectordelete: false,
      //底栏控制
      bottomData: [],
      bottomVisible: false,
      datepickerVisible: false,
      CameraVisible: false,
      response: '',
      //照片控制
      pictureSelect: false,
      pictureUri: '',
      imageArr: [],
      imageFileArr: [],
      imageOverSize: false,
      fileid: "",
      recid: "",
      isPhotoUpdate: false,
      //
      GetError: false,
      buttondisable: false,
      //钢筋笼控制
      steelCageID: '',
      steelCageCode: '',
      isGetsteelCageInfoforPutMold: false,
      issteelCageIDdelete: false,
      focusIndex: 0,
      isLoading: true,
      //图纸
      isPaperTypeVisible: false,
      paperList: ["模版图", "配筋图", "预埋件装配图", "连接件排版图"],
      paperUrl: "",
      FileName: "",
      isPDFVisible: false,
      //查询界面取消选择按钮
      isCheck: false,
      isCheckPage: false,
      remark: '',
      //控制app拍照，日期，相册选择功能
      isAppPhotoSetting: false,
      currShowImgIndex: 0,
      currShowImgIndex: 0,
    
      isAppDateSetting: false,
      isAppPhotoAlbumSetting: false,
      
    }
    //this.requestCameraPermission = this.requestCameraPermission.bind(this)
  }

  componentDidMount() {
    const { navigation } = this.props;
    let guid = navigation.getParam('guid') || ''
    let pageType = navigation.getParam('pageType') || ''
    let isAppPhotoSetting = navigation.getParam('isAppPhotoSetting')
    let isAppDateSetting = navigation.getParam('isAppDateSetting')
    let isAppPhotoAlbumSetting = navigation.getParam('isAppPhotoAlbumSetting')
    this.setState({
      isAppPhotoSetting: isAppPhotoSetting,
      isAppDateSetting: isAppDateSetting,
      isAppPhotoAlbumSetting: isAppPhotoAlbumSetting
    })
    if (pageType == 'CheckPage') {
      this.checkResData(guid)
      this.setState({ isCheckPage: true })
    } else {
      this.resData()
      DeviceStorage.get('DeviceStorageDataQI')
        .then(res => {
          if (res) {
            console.log("🚀 ~ file: qualityinspection.js ~ line 133 ~ QualityInspection ~ componentDidMount ~ res", res)
            let DeviceStorageDataObj = JSON.parse(res)
            DeviceStorageData = DeviceStorageDataObj
            console.log("🚀 ~ file: qualityinspection.js ~ line 136 ~ QualityInspection ~ componentDidMount ~ DeviceStorageData", DeviceStorageData)
            if (res.length != 0) {
              this.setState({
                "isGetInspector": DeviceStorageDataObj.isGetInspector,
                "isGetTeamPeopleInfo": DeviceStorageDataObj.isGetTeamPeopleInfo,
                "InspectorId": DeviceStorageDataObj.InspectorId,
                "InspectorName": DeviceStorageDataObj.InspectorName,
                "InspectorSelect": DeviceStorageDataObj.InspectorName,
                "monitorId": DeviceStorageDataObj.monitorId,
                "monitorName": DeviceStorageDataObj.monitorName,
                "monitorSelect": DeviceStorageDataObj.monitorName,
                "monitorTeamName": DeviceStorageDataObj.monitorTeamName,
              })
            }
          }
        }).catch(err => {
          console.log("🚀 ~ file: loading.js ~ line 131 ~ SteelCage ~ componentDidMount ~ err", err)

        })
    }

    //通过使用DeviceEventEmitter模块来监听事件
    this.iDataScan = DeviceEventEmitter.addListener('iDataScan', (Event) => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 115 ~ SteelCage ~ Event.ScanResult", Event.ScanResult)
      if (typeof Event.ScanResult != undefined) {
        let data = Event.ScanResult
        let arr = data.split("=");
        let id = ''
        id = arr[arr.length - 1]
        console.log("🚀 ~ file: concrete_pouring.js ~ line 118 ~ SteelCage ~ id", id)
        if (this.state.type == 'compid') {
          this.GetcomID(id)
        } else if (this.state.type == 'taiwanId') {
          this.GettaiwanId(id)
        } else if (this.state.type == 'acceptanceId') {
          this.GetCommonMould(id)
        } else if (this.state.type == 'monitorId') {
          this.GetTeamPeopleInfo(id)
        } else if (this.state.type == 'InspectorId') {
          this.GetInspector(id)
        } else if (this.state.type == 'steelCageID') {
          this.GetsteelCageInfoforPutMold(id)
        }

      }
    });
  }

  componentWillUnmount() {
    imageArr = [], imageFileArr = []
    this.iDataScan.remove()
  }

  UpdateControl = () => {
    console.log("🚀 ~ file: qualityinspection.js:223 ~ QualityInspection ~ typeof this.props.navigation.getParam('QRid'):", typeof this.props.navigation.getParam('QRid'))
    console.log("🚀 ~ file: qualityinspection.js:225 ~ QualityInspection ~ this.props.navigation.getParam('QRid'):", this.props.navigation.getParam('QRid'))

    if (typeof this.props.navigation.getParam('QRid') != "undefined") {
      if (this.props.navigation.getParam('QRid') != "") {
        this.toast.show(Url.isLoadingView, 0)
      }
    }
    const { navigation } = this.props;
    const { isGetcomID, isGetCommonMould, isGetInspector, isGetTeamPeopleInfo, isGettaiwanId, isGetsteelCageInfoforPutMold } = this.state
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || ''
    console.log("🚀 ~ file: qualityinspection.js ~ line 11113 ~ SteelCage ~ componentDidUpdate ~ QRid", QRid)
    let type = navigation.getParam('type') || '';
    console.log("🚀 ~ file: qualityinspection.js ~ line 105 ~ SteelCage ~ componentDidUpdate ~ this.state.GetError", this.state.GetError)
    if (type == 'compid') {
      this.GetcomID(QRid)
    } else if (type == 'taiwanId') {
      this.GettaiwanId(QRid)
    } else if (type == 'acceptanceId') {
      this.GetCommonMould(QRid)
    } else if (type == 'monitorId') {
      this.GetTeamPeopleInfo(QRid)
    } else if (type == 'InspectorId') {
      this.GetInspector(QRid)
    } else if (type == 'steelCageID') {
      this.GetsteelCageInfoforPutMold(QRid)
    }
    console.log("🚀 ~ file: qualityinspection.js ~ line 11147 ~ SteelCage ~ componentDidUpdate ~ QRid", QRid)

  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  checkResData = (guid) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "LookHideCheck",
      "servicetype": "pda",
      "express": "8AC4C0B0",
      "ciphertext": "7df83f712027c63f147f6c2d4a43f08d",
      "data": {
        "guid": guid,
        "factoryId": Url.PDAFid,
        "merge": "1"
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: qualityinspection.js ~ line 189 ~ QualityInspection ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 204 ~ QualityInspection ~ resData", resData)
      let rowguid = resData.result.guid
      let ServerTime = resData.result.time
      let ImageUrls = resData.result.ImageUrls
      let compCode = resData.result.compCode
      let compId = resData.result.compId
      let taiWanName = resData.result.taiWanName
      let projectName = resData.result.projectName
      let acceptanceName = resData.result.acceptanceName
      let steelCageCode = resData.result.steelCageCode
      let compTypeName = resData.result.compTypeName
      let designType = resData.result.designType
      let floorName = resData.result.floorName
      let floorNoName = resData.result.floorNoName
      let volume = resData.result.volume
      let weight = resData.result.weight
      let Inspector = resData.result.Inspector
      let monitor = resData.result.monitor
      let editEmployeeName = resData.result.editEmployeeName
      let remark = resData.result.remark
      //图片数组
      let imageArr = [], imageFileArr = [];

      if (ImageUrls.length != 0) {
        ImageUrls.map((item, index) => {
          let tmp = {}
          tmp.fileid = ""
          tmp.uri = item.componentImageUrl
          tmp.url = item.componentImageUrl
          imageFileArr.push(tmp)
          imageArr.push(item.componentImageUrl)
        })
        this.setState({
          imageFileArr: imageFileArr
        })
        this.setState({ pictureSelect: false })
      } else {
        this.setState({ pictureSelect: false })
      }

      console.log("🚀 ~ file: qualityinspection.js ~ line 224 ~ QualityInspection ~ imageArr", imageArr)

      //产品子表
      let compData = {}, result = {}
      result.projectName = projectName
      result.compTypeName = compTypeName
      result.designType = designType
      result.floorNoName = floorNoName
      result.floorName = floorName
      result.volume = volume
      result.weight = weight
      compData.result = result
      this.setState({
        resData: resData,
        rowguid: rowguid,
        ServerTime: ServerTime,
        compCode: compCode,
        compId: compId,
        Tai_QRname: taiWanName,
        acceptanceName: acceptanceName,
        editEmployeeName: editEmployeeName,
        compData: compData,
        monitorName: monitor,
        monitorSelect: monitor,
        Inspector: Inspector,
        InspectorSelect: Inspector,
        steelCageCode: steelCageCode,
        remark: remark,
        imageArr: imageArr,
        pictureSelect: true,
        isGetcomID: true,
        isGettaiwanId: true,
        isGetTeamPeopleInfo: true,
        isGetCommonMould: true,
        isGetInspector: true,
        isCheck: true
      })
      this.setState({
        isLoading: false,
      })
    }).catch((error) => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 215 ~ QualityInspection ~ error", error)
    });
  }

  resData = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "complexhidecheckinit",
      "servicetype": "pda",
      "express": "8AC4C0B0",
      "ciphertext": "7df83f712027c63f147f6c2d4a43f08d",
      "data": {
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: qualityinspection.js ~ line 335 ~ QualityInspection ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 314 ~ QualityInspection ~ resData", resData)
      console.log("🚀 ~ file: qualityinspection.js ~ line 1002 ~ QualityInspection ~ Url.isUseTaiWanID ", Url.isUseTaiWanID)

      //初始化错误提示
      if (resData.status == 100) {
        let rowguid = resData.result.rowguid
        let ServerTime = resData.result.serverTime
        let monitorSetup = resData.result.monitorSetup
        if (monitorSetup != '扫码') {
          this.setState({
            isGetTeamPeopleInfo: true,
            isGetInspector: true,
          })
        }
        let monitors = resData.result.monitors
        let Inspectors = resData.result.Inspectors
        this.setState({
          resData: resData,
          rowguid: rowguid,
          ServerTime: ServerTime,
          monitorSetup: monitorSetup,
          monitors: monitors,
          Inspectors: Inspectors,
          isLoading: false
        })
      }
      else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }

    }).catch((error) => {
    });

  }

  GetcomID = (id) => {
    console.log("🚀 ~ file: qualityinspection.js ~ line 194 ~ SteelCage ~ GetcomID", id)
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getcompinfoandsteelcagesetup",
      "servicetype": "pda",
      "express": "0F51EF23",
      "ciphertext": "a91d6dc5c5a20f2a35939b6e2d91a0ef",
      "data": {
        "compId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: qualityinspection.js ~ line 367 ~ QualityInspection ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        compCode = resData.result.compCode
        let compId = resData.result.compId
        let steelLabel = resData.result.steelLabel
        this.setState({
          compData: resData,
          compCode: compCode,
          compId: compId,
          steelLabel: steelLabel,
          isGetcomID: true,
          iscomIDdelete: false
        })
        this.setState({
          focusIndex: 1,
          type: "taiwanId"
        })
        setTimeout(() => { this.scoll.scrollTo({ x: 0, y: 400, animated: true }) }, 300)
      } else {
        console.log('resData', resData)
        Alert.alert('扫码错误', resData.message)
        this.setState({
          compId: ""
        })
        //this.input1.focus()
        varGetError = true
        this.setState({
          GetError: true
        })
      }

    }).catch(err => {
      console.log("🚀 ~ file: qualityinspection.js:499 ~ QualityInspection ~ err:", err)
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  comIDItem = (index) => {
    const { isGetcomID, buttondisable, compCode, compId } = this.state
    return (
      <View>
        {
          !isGetcomID ?
            <View style={{ flexDirection: 'row' }}>
              <View>
                <Input
                  ref={ref => { this.input1 = ref }}
                  containerStyle={styles.scan_input_container}
                  inputContainerStyle={styles.scan_inputContainerStyle}
                  inputStyle={[styles.scan_input]}
                  placeholder='请输入'
                  keyboardType='numeric'
                  value={compId}
                  onFocus={() => {
                    this.setState({
                      focusIndex: 0,
                      type: 'compid',
                    })
                  }}
                  onChangeText={(value) => {
                    value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
                    value = value.replace(/^\./g, ""); //验证第一个字符是数字
                    value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
                    //value = value.replace(/\b(0+)/gi, ""); //清楚开头的0
                    this.setState({
                      compId: value
                    })
                  }}
                  //onSubmitEditing={() => { this.input1.focus() }}
                  //onFocus={() => { this.setState({ focusIndex: '5' }) }}
                  /*  */
                  //returnKeyType = 'previous'
                  onSubmitEditing={() => {
                    let inputComId = this.state.compId
                    console.log("🚀 ~ file: qualityinspection.js ~ line 468 ~ QualityInspection ~ inputComId", inputComId)
                    inputComId = inputComId.replace(/\b(0+)/gi, "")
                    this.GetcomID(inputComId)
                  }}
                />
              </View>
              <ScanButton
                onPress={() => {
                  this.setState({
                    iscomIDdelete: false,
                    buttondisable: true,
                    focusIndex: 0
                  })
                  this.props.navigation.navigate('QRCode', {
                    type: 'compid',
                    page: 'QualityInspection'
                  })
                }}
                onLongPress={() => {
                  this.setState({
                    type: 'compid',
                    focusIndex: 0
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}
              />
            </View>
            :
            <TouchableCustom
              onPress={() => {
                console.log('onPress')
              }}
              onLongPress={() => {
                console.log('onLongPress')
                Alert.alert(
                  '提示信息',
                  '是否删除构件信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      this.setState({
                        compData: [],
                        compCode: '',
                        compId: '',
                        iscomIDdelete: true,
                        isGetcomID: false,
                      })
                    }
                  }])
              }} >
              <Text>{compCode}</Text>
            </TouchableCustom>
        }
      </View>

    )
  }

  GetsteelCageInfoforPutMold = (id) => {
    console.log("🚀 ~ file: qualityinspection.js ~ line 346 ~ SteelCage ~ id", id)
    let formData = new FormData();
    let data = {};
    data = {
      "action": "GetsteelCageInfoforPutMold",
      "servicetype": "pda",
      "express": "6C8FAB14",
      "ciphertext": "96d2d1428051129167842f3b5eaca07f",
      "data": {
        "steelCageID": id,
        "compId": this.state.compId,
        "factoryId": Url.PDAFid
      }
    }
    console.log("🚀 ~ file: qualityinspection.js ~ line 349 ~ SteelCage ~ data", data)
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let steelCageID = resData.result.steelCageID
        let steelCageCode = resData.result.steelCageCode
        console.log("🚀 ~ file: qualityinspection.js ~ line 374 ~ SteelCage ~ steelCageCode", steelCageCode)
        this.setState({
          steelCageID: steelCageID,
          steelCageCode: steelCageCode,
          isGetsteelCageInfoforPutMold: true,
          issteelCageIDdelete: false
        })
        this.setState({
          focusIndex: 6,
          type: "monitorId"
        })
        setTimeout(() => { this.scoll.scrollToEnd() }, 300)
      } else {
        console.log('resData', resData)
        Alert.alert('扫码错误', resData.message)
        varGetError = true
        this.setState({
          GetError: true
        })
      }

    }).catch(err => {
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  GettaiwanId = (id) => {
    let Tai_QRname = ''
    let formData = new FormData();
    let data = {};
    data = {
      "action": "gettaiwanInfo",
      "servicetype": "pda",
      "express": "8FBA0BDC",
      "ciphertext": "591057f7d292870338e0fdffe9ef719f",
      "data": {
        "factoryId": Url.PDAFid,
        "taiwanId": id
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let taiwanId = resData.result.taiwanId
        Tai_QRname = resData.result.Tai_QRname
        this.setState({
          taiwanId: taiwanId,
          Tai_QRname: Tai_QRname,
          isGettaiwanId: true,
          istaiwanIddelete: false
        })
        this.setState({
          focusIndex: 2,
          type: "acceptanceId",
        })
      } else {
        Alert.alert('ERROR', resData.message)
        varGetError = true
        this.setState({
          GetError: true
        })
      }

    }).catch(err => {
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  taiwanItem = (index) => {
    const { isGettaiwanId, buttondisable, Tai_QRname } = this.state
    return (
      <View>
        {
          !isGettaiwanId ?
            <View>
              <ScanButton

                onPress={() => {
                  this.setState({
                    focusIndex: 1,
                    istaiwanIddelete: false,
                    buttondisable: true
                  })
                  setTimeout(() => {
                    this.setState({ GetError: false, buttondisable: false })
                    varGetError = false
                  }, 1500)
                  this.props.navigation.navigate('QRCode', {
                    type: 'taiwanId',
                    page: 'QualityInspection'
                  })
                }}
                onLongPress={() => {
                  this.setState({
                    type: 'taiwanId',
                    focusIndex: 1
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}
              />
            </View>
            :
            <TouchableCustom
              onPress={() => {
                console.log('onPress')
              }}
              onLongPress={() => {
                console.log('onLongPress')
                Alert.alert(
                  '提示信息',
                  '是否删除模台信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      this.setState({
                        taiwanId: '',
                        Tai_QRname: '',
                        isGettaiwanId: false,
                        istaiwanIddelete: true,
                      })
                    }
                  }])
              }} >
              <Text>{Tai_QRname}</Text>
            </TouchableCustom>
        }
      </View>
    )
  }

  GetCommonMould = (id) => {
    let acceptanceName = ''
    let formData = new FormData();
    let data = {};
    data = {
      "action": "GetCommonMould",
      "servicetype": "pda",
      "express": "50545047",
      "ciphertext": "f3eedf54128226aef62dd6023a1c1573",
      "data": {
        "compId": this.state.compId,
        "acceptanceId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let acceptanceId = resData.result.acceptanceId
        acceptanceName = resData.result.acceptanceName
        this.setState({
          acceptanceId: acceptanceId,
          acceptanceName: acceptanceName,
          isGetCommonMould: true,
          isCommonMoulddelete: false
        })
        if (this.state.steelLabel == '使用') {
          this.setState({
            focusIndex: 3,
            type: "steelCageID"
          })
          setTimeout(() => { this.scoll.scrollToEnd() }, 300)
        } else {
          this.setState({
            focusIndex: 6,
            type: "monitorId"
          })
          setTimeout(() => { this.scoll.scrollToEnd() }, 300)
        }

      } else {
        Alert.alert('ERROR', resData.message)
        varGetError = true
        this.setState({
          GetError: true
        })
      }


    }).catch(err => {
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  GetTeamPeopleInfo = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getTeamPeopleInfo",
      "servicetype": "pda",
      "express": "2B9B0E37",
      "ciphertext": "eb1a5891270a0ebdbb68d37e27f82b92",
      "data": {
        "factoryId": Url.PDAFid,
        "monitorId": id
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let monitorId = resData.result.monitorId
        let monitorName = resData.result.monitorName
        let teamId = resData.result.teamId
        let teamName = resData.result.teamName

        this.setState({
          monitorId: monitorId,
          monitorName: monitorName,
          monitorTeamId: teamId,
          monitorTeamName: teamName,
          isGetTeamPeopleInfo: true,
          isTeamPeopledelete: false
        }, () => {
          DeviceStorageData = {
            "isGetTeamPeopleInfo": true,
            "InspectorId": this.state.InspectorId,
            "InspectorName": this.state.InspectorName,
            "InspectorSelect": this.state.InspectorName,
            "monitorId": this.state.monitorId,
            "monitorName": this.state.monitorName,
            "monitorSelect": this.state.monitorName,
            "monitorTeamName": this.state.monitorTeamName,
          }

          DeviceStorage.save('DeviceStorageDataQI', JSON.stringify(DeviceStorageData))
        })

        this.setState({
          focusIndex: 7,
          type: "InspectorId",
        })
      } else {
        Alert.alert('ERROR', resData.message)
        varGetError = true
        this.setState({
          GetError: true
        })
      }
    }).catch(err => {
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  monitorNameItem = (index) => {
    const { isGetTeamPeopleInfo, buttondisable, monitorName, monitorSetup, monitors, monitorSelect } = this.state
    console.log("🚀 ~ file: qualityinspection.js ~ line 559 ~ SteelCage ~ monitors", monitors)
    return (
      <View>
        {
          monitorSetup == '扫码' ?
            (
              !isGetTeamPeopleInfo ?
                <View>
                  <ScanButton

                    onPress={() => {
                      this.setState({
                        isTeamPeopledelete: false,
                        focusIndex: index
                      })
                      setTimeout(() => {
                        this.setState({ GetError: false, buttondisable: false })
                        varGetError = false
                      }, 1500)
                      this.props.navigation.navigate('QRCode', {
                        type: 'monitorId',
                        page: 'QualityInspection'
                      })
                    }}
                    onLongPress={() => {
                      this.setState({
                        type: 'monitorId',
                        focusIndex: index
                      })
                      NativeModules.PDAScan.onScan();
                    }}
                    onPressOut={() => {
                      NativeModules.PDAScan.offScan();
                    }}
                  />
                </View>
                :
                <TouchableCustom
                  onPress={() => {
                    console.log('onPress')
                  }}
                  onLongPress={() => {
                    console.log('onLongPress')
                    Alert.alert(
                      '提示信息',
                      '是否删除班组信息',
                      [{
                        text: '取消',
                        style: 'cancel'
                      }, {
                        text: '删除',
                        onPress: () => {
                          this.setState({
                            monitorId: '',
                            monitorName: '',
                            monitorTeamId: '',
                            monitorTeamName: '',
                            isGetTeamPeopleInfo: false,
                            isTeamPeopledelete: true
                          })
                        }
                      }])
                  }} >
                  <Text>{monitorName}</Text>
                </TouchableCustom>
            )
            :
            (
              this.state.isCheck ? <Text >{monitorSelect}</Text> :
                <TouchableCustom onPress={() => { this.isBottomVisible(monitors, index) }} >
                  <IconDown text={monitorSelect} />
                </TouchableCustom>
            )

        }
      </View>
    )
  }

  GetInspector = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getInspector",
      "servicetype": "pda",
      "express": "CDAB7A2D",
      "ciphertext": "a590b603df9b5a4384c130c7c21befb7",
      "data": {
        "InspectorId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let InspectorId = resData.result.InspectorId
        let InspectorName = resData.result.InspectorName
        this.setState({
          InspectorId: InspectorId,
          InspectorName: InspectorName,
          isGetInspector: true,
          isInspectordelete: false
        }, () => {
          DeviceStorageData = {
            "isGetInspector": true,
            "InspectorId": this.state.InspectorId,
            "InspectorName": this.state.InspectorName,
            "InspectorSelect": this.state.InspectorName,
            "monitorId": this.state.monitorId,
            "monitorName": this.state.monitorName,
            "monitorSelect": this.state.monitorName,
            "monitorTeamName": this.state.monitorTeamName,
          }

          DeviceStorage.save('DeviceStorageDataQI', JSON.stringify(DeviceStorageData))
        })
      } else {
        Alert.alert('ERROR', resData.message)
        varGetError = true
        this.setState({
          GetError: true
        })
      }

    }).catch(err => {
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  inspectorItem = (index) => {
    const { isGetInspector, buttondisable, InspectorName, monitorSetup, InspectorSelect, Inspectors } = this.state
    return (
      <View>
        {
          monitorSetup == '扫码' ?
            (
              !isGetInspector ?
                <View>
                  <ScanButton
                    onPress={() => {
                      this.setState({
                        isInspectordelete: false,
                        buttondisable: true,
                        focusIndex: 6
                      })
                      this.props.navigation.navigate('QRCode', {
                        type: 'InspectorId',
                        page: 'QualityInspection'
                      })
                    }}
                    onLongPress={() => {
                      this.setState({
                        type: 'InspectorId',
                        focusIndex: index
                      })
                      NativeModules.PDAScan.onScan();
                    }}
                    onPressOut={() => {
                      NativeModules.PDAScan.offScan();
                    }}
                  />
                </View>
                :
                <TouchableCustom
                  onPress={() => {
                    console.log('onPress')

                  }}
                  onLongPress={() => {
                    console.log('onLongPress')
                    Alert.alert(
                      '提示信息',
                      '是否删除质检员信息',
                      [{
                        text: '取消',
                        style: 'cancel'
                      }, {
                        text: '删除',
                        onPress: () => {
                          this.setState({
                            InspectorId: '',
                            InspectorName: '',
                            isGetInspector: false,
                            isInspectordelete: true
                          })
                        }
                      }])
                  }} >
                  <Text>{InspectorName}</Text>
                </TouchableCustom>
            ) :
            (
              this.state.isCheck ? <Text >{InspectorSelect}</Text> :
                <TouchableCustom onPress={() => { this.isBottomVisible(Inspectors, index) }} >
                  <IconDown text={InspectorSelect} />
                </TouchableCustom>
            )
        }
      </View>
    )
  }

  PostData = () => {
    const { InspectorId, acceptanceId, steelLabel, monitorId, compId, ServerTime, checkResult, rowguid, taiwanId, steelCageIDs, steelCageID, remark } = this.state
    console.log("🚀 ~ file: qualityinspection.js:1102 ~ QualityInspection ~ ServerTime:", ServerTime)
    const { navigation } = this.props;
    const QRid = navigation.getParam('QRid') || '';

    if (compId == '') {
      this.toast.show('产品编号不能为空');
      return
    } else if (steelCageID == '' && steelLabel == '使用') {
      this.toast.show('钢筋笼编码不能为空');
      return
    } else if (monitorId == '') {
      this.toast.show('班组信息不能为空');
      return
    } else if (InspectorId == '') {
      this.toast.show('质检员信息不能为空');
      return
    }
    console.log("🚀 ~ file: qualityinspection.js ~ line 1002 ~ QualityInspection ~ Url.isUseTaiWanID ", Url.isUseTaiWanID)

    if (Url.isUseTaiWanID == 'YES') {
      if (taiwanId == '') {
        this.toast.show('模台不能为空');
        return
      }
    }

    if (this.state.isAppPhotoSetting) {
      if (imageArr.length == 0) {
        this.toast.show('请先拍摄构件照片');
        return
      }
    }

    this.toast.show(saveLoading, 0)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "SaveProduction",
      "servicetype": "pda",
      "express": "06C795EE",
      "ciphertext": "a1e3818d57d9bfc9437c29e7a558e32e",
      "data": {
        "InspectorId": InspectorId,
        "acceptanceId": acceptanceId,
        "monitorId": monitorId,
        "compId": compId,
        "Username": Url.PDAusername,
        "factoryId": Url.PDAFid,
        "hideCheckTime": ServerTime,
        "checkResult": checkResult,
        "rowguid": rowguid,
        "taiwanId": taiwanId,
        "steelCageIDs": steelCageID,
        "remark": remark,
        "recid": this.state.recid,
      },
    }
    formData.append('jsonParam', JSON.stringify(data))
    if (!Url.isAppNewUpload) {
      imageArr.map((item, index) => {
        formData.append('img_' + index, {
          uri: item,
          type: 'image/jpeg',
          name: 'img_' + index
        })
      })
    }
    console.log("🚀 ~ file: qualityinspection.js ~ line 793 ~ SteelCage ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 947 ~ QualityInspection ~ resData", resData)
      console.log("🚀 ~ file: qualityinspection.js ~ line 948 ~ QualityInspection ~ resData.status == '100'", resData.status == '100')

      if (resData.status == '100') {
        this.toast.show('保存成功');
        setTimeout(() => {
          this.props.navigation.replace(this.props.navigation.getParam("page"), {
            title: this.props.navigation.getParam("title"),
            pageType: this.props.navigation.getParam("pageType"),
            page: this.props.navigation.getParam("page"),
            isAppPhotoSetting: this.props.navigation.getParam("isAppPhotoSetting"),
            isAppDateSetting: this.props.navigation.getParam("isAppDateSetting"),
            isAppPhotoAlbumSetting: this.props.navigation.getParam("isAppPhotoAlbumSetting"),
          })
          //this.props.navigation.navigate('PDAMainStack')
        }, 400)
      } else {
        this.toast.close()
        Alert.alert('保存失败', resData.message)
      }
    }).catch((error) => {
      this.toast.show('保存失败')
      if (error.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('保存失败', "响应内容不是合法JSON格式")
        return
      }
      if (error.toString().indexOf('Network request faile') != -1) {
        Alert.alert('保存失败', "网络请求错误")
        return
      }
      Alert.alert('保存失败', error.toString())
    });
  }

  DeleteData = () => {
    const { navigation } = this.props;
    let guid = navigation.getParam('guid') || ''
    console.log('DeleteData')
    let formData = new FormData();
    let data = {};
    data = {
      "action": "DeleteComplexHideCheck",
      "servicetype": "pda",
      "express": "06C795EE",
      "ciphertext": "a1e3818d57d9bfc9437c29e7a558e32e",
      "data": {
        "guid": guid,
        "Username": Url.PDAusername,
        "factoryId": Url.PDAFid
      },
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: qualityinspection.js ~ line 953 ~ QualityInspection ~ DeleteData ~ formData", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('删除成功');
        setTimeout(() => {
          this.props.navigation.navigate('MainCheckPage', {
            backType: "revise"
          })
        }, 400)
      } else {
        Alert.alert(
          '删除失败',
          resData.message,
          [{
            text: '取消',
            style: 'cancel'
          }, {
            text: '返回上一级',
            onPress: () => {
              this.props.navigation.goBack()
            }
          }])
        console.log('resData', resData)
        console.log('resData', resData)
      }
    }).catch((error) => {
    });

  }

  cameraFunc = () => {
    launchCamera(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: parseInt(Url.isResizePhoto),
        maxWidth: parseInt(Url.isResizePhoto),
        saveToPhotos: true,

      },
      (response) => {
        if (response.uri == undefined) {
          return
        }
        imageArr.push(response.uri)
        this.setState({
          //pictureSelect: true,
          pictureUri: response.uri,
          imageArr: imageArr
        })
        if (Url.isAppNewUpload) {
          this.cameraPost(response.uri)
        } else {
          this.setState({
            pictureSelect: true,
          })
        }
        console.log(response)
      },
    )
  }

  camLibraryFunc = () => {
    launchImageLibrary(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: parseInt(Url.isResizePhoto),
        maxWidth: parseInt(Url.isResizePhoto),
      },
      (response) => {
        if (response.uri == undefined) {
          return
        }
        imageArr.push(response.uri)
        this.setState({
          //pictureSelect: true,
          pictureUri: response.uri,
          imageArr: imageArr
        })
        if (Url.isAppNewUpload) {
          this.cameraPost(response.uri)
        } else {
          this.setState({
            pictureSelect: true,
          })
        }
        console.log(response)
      },
    )
  }

  camandlibFunc = () => {
    Alert.alert(
      '提示信息',
      '选择拍照方式',
      [{
        text: '取消',
        style: 'cancel'
      }, {
        text: '拍照',
        onPress: () => {
          this.cameraFunc()
        }
      }, {
        text: '相册',
        onPress: () => {
          this.camLibraryFunc()
        }
      }])
  }

   // 展示/隐藏 放大图片
  handleZoomPicture = (flag, index) => {
    this.setState({
      imageOverSize: false,
      currShowImgIndex: index || 0
    })
  }

  // 加载放大图片弹窗
  renderZoomPicture = () => {
    const { imageOverSize, currShowImgIndex, imageFileArr } = this.state
    return (
      <OverlayImgZoomPicker
        isShowImage={imageOverSize}
        currShowImgIndex={currShowImgIndex}
        zoomImages={imageFileArr}
        callBack={(flag) => this.handleZoomPicture(flag)}
      ></OverlayImgZoomPicker>
    )
  }

  cameraPost = (uri) => {
    const { recid } = this.state
    this.setState({
      isPhotoUpdate: true
    })
    this.toast.show('图片上传中', 2000)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "savephote",
      "servicetype": "photo_file",
      "express": "D2979AB2",
      "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
      "data": {
        "fileflag": fileflag,
        "username": Url.PDAusername,
        "recid": recid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    formData.append('img', {
      uri: uri,
      type: 'image/jpeg',
      name: 'img'
    })
    console.log("🚀 ~ file: concrete_pouring.js ~ line 672 ~ SteelCage ~ formData", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 690 ~ SteelCage ~ resData", resData)
      this.toast.close()
      if (resData.status == '100') {
        let fileid = resData.result.fileid;
        let recid = resData.result.recid;

        let tmp = {}
        tmp.fileid = fileid
        tmp.uri = uri
        tmp.url = uri
        imageFileArr.push(tmp)
        this.setState({
          fileid: fileid,
          recid: recid,
          pictureSelect: true,
          imageFileArr: imageFileArr
        })
        console.log("🚀 ~ file: concrete_pouring.js ~ line 704 ~ SteelCage ~ imageFileArr", imageFileArr)
        this.toast.show('图片上传成功');
        this.setState({
          isPhotoUpdate: false
        })
      } else {
        Alert.alert('图片上传失败', resData.message)
        this.toast.close(1)
      }
    }).catch((error) => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 692 ~ SteelCage ~ error", error)
    });
  }

  cameraDelete = (item) => {
    let i = imageArr.indexOf(item)

    if (Url.isAppNewUpload) {
      let formData = new FormData();
      let data = {};
      data = {
        "action": "deletephote",
        "servicetype": "photo_file",
        "express": "D2979AB2",
        "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
        "data": {
          "fileid": imageFileArr[i].fileid,
        }
      }
      formData.append('jsonParam', JSON.stringify(data))
      fetch(Url.PDAurl, {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        body: formData
      }).then(res => {
        console.log(res.statusText);
        console.log(res);
        return res.json();
      }).then(resData => {
        if (resData.status == '100') {
          if (i > -1) {
            imageArr.splice(i, 1)
            imageFileArr.splice(i, 1)
            this.setState({
              imageArr: imageArr,
              imageFileArr: imageFileArr
            }, () => {
              this.forceUpdate()
            })
          }
          if (imageArr.length == 0) {
            this.setState({
              pictureSelect: false
            })
          }
          this.toast.show('图片删除成功');
        } else {
          Alert.alert('图片删除失败', resData.message)
          this.toast.close(1)
        }
      }).catch((error) => {
        console.log("🚀 ~ file: concrete_pouring.js ~ line 761 ~ SteelCage ~ cameraDelete ~ error", error)
      });
    } else {
      if (i > -1) {
        imageArr.splice(i, 1)
        this.setState({
          imageArr: imageArr,
        }, () => {
          this.forceUpdate()
        })
      }
      if (imageArr.length == 0) {
        this.setState({
          pictureSelect: false
        })
      }
    }
  }

  imageView = () => {
    return (
      <View style={{ flexDirection: 'row' }}>
        {
          this.state.pictureSelect ?
            this.state.imageArr.map((item, index) => {
              return (
                <AvatarImg
                  item={item}
                  index={index}
                  onPress={() => {
                    this.setState({
                      imageOverSize: true,
                      pictureUri: item
                    })
                  }}
                  onLongPress={() => {
                    Alert.alert(
                      '提示信息',
                      '是否删除构件照片',
                      [{
                        text: '取消',
                        style: 'cancel'
                      }, {
                        text: '删除',
                        onPress: () => {
                          this.cameraDelete(item)
                        }
                      }])
                  }} />
              )
            }) : <View></View>
        }
      </View>
    )
  }

  _DatePicker = () => {
    const { ServerTime } = this.state
    return (
      <DatePicker
        customStyles={{
          dateInput: styles.dateInput,
          dateTouchBody: styles.dateTouchBody,
          dateText: this.state.isAppDateSetting ? styles.dateText : styles.dateDisabled,
        }}
        iconComponent={<Icon name='caretdown' color={this.state.isAppDateSetting ? '#419fff' : "#666"} iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
        }
        showIcon={true}
        mode='datetime'
        date={ServerTime}
        onOpenModal={() => { this.setState({ focusIndex: 4 }) }}
        format="YYYY-MM-DD HH:mm"
        disabled={!this.state.isAppDateSetting}
        onDateChange={(value) => {
          this.setState({
            ServerTime: value
          })
        }}
      />
    )
  }

  isBottomVisible = (data, focusIndex) => {
    if (typeof (data) != "undefined") {
      if (data.length > 0) {
        this.setState({ bottomVisible: true, bottomData: data, focusIndex: focusIndex })
      }
    } else {
      this.toast.show("无数据")
    }
  }

  func = (state) => {
    console.log("🚀 ~ file: qualityinspection.js ~ line 1166 ~ QualityInspection ~ state", state)
    this.setState({
      isPaperTypeVisible: true
    })
  }

  paperResData = (type) => {
    let formData = new FormData();
    let data = {}
    data = {
      "action": "getDrawings",
      "servicetype": "pda",
      "express": "FCEF95AF",
      "ciphertext": "985e3620ae26a2f380c723b60ce9b525",
      "data": {
        "type": type,
        "compId": this.state.compId,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 314 ~ QualityInspection ~ resData", resData)
      if (resData.status == "100") {

        let paperUrl = resData.result.Url
        let FileName = resData.result.FileName

        this.setState({
          paperUrl: paperUrl,
          FileName: FileName,
          isPDFVisible: true,
          isPaperTypeVisible: false
        })
      } else {
        Alert.alert('错误', resData.message)
        this.setState({ isPaperTypeVisible: false })
      }
    }).catch((error) => {
    });
  }

  // 展示/隐藏 放大图片
  handleZoomPicture = (flag, index) => {
    this.setState({
      imageOverSize: false,
      currShowImgIndex: index || 0
    })
  }

  // 加载放大图片弹窗
  renderZoomPicture = () => {
    const { imageOverSize, currShowImgIndex, imageFileArr } = this.state
    return (
      <OverlayImgZoomPicker
        isShowImage={imageOverSize}
        currShowImgIndex={currShowImgIndex}
        zoomImages={imageFileArr}
        callBack={(flag) => this.handleZoomPicture(flag)}
      ></OverlayImgZoomPicker>
    )

  }


  render() {
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || ''
    const type = navigation.getParam('type') || '';
    let pageType = navigation.getParam('pageType') || ''

    console.log("🚀 ~ file: qualityinspection.js ~ line 1250 ~ QualityInspection ~ render ~ Url.isResizePhoto", Url.isResizePhoto)

    if (type == 'compid') {
      QRcompid = QRid
    }
    const { buttondisable, focusIndex, steelLabel, resData, ServerTime, isGetcomID, isGetCommonMould, isGetInspector, isGetTeamPeopleInfo, isGetsteelCageInfoforPutMold, isGettaiwanId, datepickerVisible, Tai_QRname, acceptanceName, monitorSetup, monitors, monitorid, monitorName, monitorTeamName, Inspectors, InspectorsId, InspectorsName, compData, compCode, bottomVisible, bottomData, isCheck, remark, isPaperTypeVisible, isPDFVisible, paperList, paperUrl, FileName } = this.state

    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          <NavigationEvents
            onWillFocus={this.UpdateControl}
            onWillBlur={() => {
              if (QRid != '') {
                navigation.setParams({ QRid: '', type: '' })
              }
            }} />
          <ScrollView ref={ref => this.scoll = ref} style={styles.mainView} showsVerticalScrollIndicator={false} >
            <View style={styles.listView}>
              <ListItem
                containerStyle={{ paddingBottom: 6 }}
                leftElement={
                  <View >
                    <View style={{ flexDirection: 'column' }} >
                      {
                        pageType == 'CheckPage' ? <View></View> :
                          <AvatarAdd
                            pictureSelect={this.state.pictureSelect}
                            backgroundColor='rgba(77,142,245,0.20)'
                            color='#4D8EF5'
                            title="构"
                            onPress={() => {
                              if (this.state.isAppPhotoAlbumSetting) {
                                this.camandlibFunc()
                              } else {
                                this.cameraFunc()
                              }
                            }} />
                      }
                      {this.imageView()}
                    </View>
                  </View>
                }
              />
              <ListItemScan
                isButton={!isGetcomID}
                focusStyle={focusIndex == '0' ? styles.focusColor : {}}
                title='产品编号'
                onPressIn={() => {
                  this.setState({
                    type: 'compid',
                    focusIndex: 0
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPress={() => {
                  this.setState({
                    type: 'compid',
                    focusIndex: 0
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}
                rightElement={
                  this.comIDItem()
                }
              />
              {
                this.state.isGetcomID ?
                  <CardList compData={compData} func={this.func.bind(this)} /> : <View></View>
              }
              <ListItemScan
                isButton={!isGettaiwanId}
                onPressIn={() => {
                  this.setState({
                    type: 'taiwanId',
                    focusIndex: 1
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPress={() => {
                  this.setState({
                    type: 'taiwanId',
                    focusIndex: 1
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}
                focusStyle={focusIndex == '1' ? styles.focusColor : {}}
                title='模台编码'
                rightElement={
                  this.taiwanItem()
                }
              />
              <ListItemScan
                isButton={!isGetCommonMould}
                onPressIn={() => {
                  this.setState({
                    type: 'acceptanceId',
                    focusIndex: 2
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPress={() => {
                  this.setState({
                    type: 'acceptanceId',
                    focusIndex: 2
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}
                focusStyle={focusIndex == '2' ? styles.focusColor : {}}
                title='专用模具'
                rightElement={
                  !isGetCommonMould ?
                    <View>
                      <ScanButton
                        onPress={() => {
                          this.setState({
                            isCommonMoulddelete: false,
                            buttondisable: false,
                            focusIndex: 2
                          })
                          setTimeout(() => {
                            this.setState({ GetError: false, buttondisable: false })
                            varGetError = false
                          }, 1500)
                          this.props.navigation.navigate('QRCode', {
                            type: 'acceptanceId',
                            page: 'QualityInspection'
                          })
                        }}
                        onLongPress={() => {
                          this.setState({
                            type: 'acceptanceId',
                            focusIndex: 2
                          })
                          NativeModules.PDAScan.onScan();
                        }}
                        onPressOut={() => {
                          NativeModules.PDAScan.offScan();
                        }}
                      />
                    </View>
                    :
                    <TouchableCustom
                      onPress={() => {
                        console.log('onPress')
                      }}
                      onLongPress={() => {
                        console.log('onLongPress')
                        Alert.alert(
                          '提示信息',
                          '是否删除模具信息',
                          [{
                            text: '取消',
                            style: 'cancel'
                          }, {
                            text: '删除',
                            onPress: () => {
                              this.setState({
                                acceptanceId: '',
                                acceptanceName: '',
                                isGetCommonMould: false,
                                isCommonMoulddelete: true
                              })
                            }
                          }])
                      }} >
                      <Text>{acceptanceName}</Text>
                    </TouchableCustom>
                }
              />
              {
                steelLabel == '使用' ?
                  <ListItemScan
                    isButton={!isGetsteelCageInfoforPutMold}
                    onPressIn={() => {
                      this.setState({
                        type: 'steelCageID',
                        focusIndex: 3
                      })
                      NativeModules.PDAScan.onScan();
                    }}
                    onPress={() => {
                      this.setState({
                        type: 'steelCageID',
                        focusIndex: 3
                      })
                      NativeModules.PDAScan.onScan();
                    }}
                    onPressOut={() => {
                      NativeModules.PDAScan.offScan();
                    }}
                    focusStyle={focusIndex == '3' ? styles.focusColor : {}}
                    title='钢筋笼编码'
                    rightTitle={
                      !isGetsteelCageInfoforPutMold ?
                        <View>
                          <ScanButton
                            onPress={() => {
                              this.setState({
                                focusIndex: 3,
                                issteelCageIDdelete: false,
                                buttondisable: false
                              })
                              setTimeout(() => {
                                this.setState({ GetError: false, buttondisable: false })
                                varGetError = false
                              }, 1500)
                              this.props.navigation.navigate('QRCode', {
                                type: 'steelCageID',
                                page: 'QualityInspection'
                              })
                            }}
                            onLongPress={() => {
                              this.setState({
                                type: 'steelCageID',
                                focusIndex: 3
                              })
                              NativeModules.PDAScan.onScan();
                            }}
                            onPressOut={() => {
                              NativeModules.PDAScan.offScan();
                            }}
                          />
                        </View>
                        :
                        <TouchableCustom
                          onPress={() => {
                            console.log('onPress')
                          }}
                          onLongPress={() => {
                            console.log('onLongPress')
                            Alert.alert(
                              '提示信息',
                              '是否删除钢筋笼编码',
                              [{
                                text: '取消',
                                style: 'cancel'
                              }, {
                                text: '删除',
                                onPress: () => {
                                  this.setState({
                                    steelCageID: '',
                                    steelCageCode: '',
                                    isGetsteelCageInfoforPutMold: false,
                                    issteelCageIDdelete: true
                                  })
                                }
                              }])
                          }} >
                          <Text numberOfLines={1}>{this.state.steelCageCode}</Text>
                        </TouchableCustom>
                    }
                  /> : <View></View>
              }

              <ListItemScan
                focusStyle={focusIndex == '4' ? styles.focusColor : {}}
                title='生产日期'
                rightElement={
                  this._DatePicker()
                }
              //rightTitle={ServerTime}
              />
              <ListItemScan
                title='操作人'
                rightTitleStyle={{ width: width / 1.5, textAlign: 'right', fontSize: 15 }}
                rightTitle={Url.PDAEmployeeName}
              />
              <ListItemScan
                isButton={true}
                focusStyle={focusIndex == '5' ? styles.focusColor : {}}
                bottomDivider
                title='检查结果'
                rightTitle={
                  <View style={{ flexDirection: "row", marginRight: -23, paddingVertical: 0 }}>
                    <CheckBoxScan
                      title='合格'
                      checked={this.state.checked}
                      onPress={() => {
                        this.setState({
                          checked: true,
                          checkResult: '合格',
                          focusIndex: 5
                        })
                      }}
                    />
                    <CheckBoxScan
                      title='不合格'
                      checked={false}
                      onPress={() => {
                        this.setState({
                          //checked: false,
                          //checkResult: '不合格'
                        })
                      }}
                    />
                  </View>
                }
              />
              <TouchableCustom underlayColor={'lightgray'} onPress={() => {
                if (monitorSetup != '扫码') {
                  this.isBottomVisible(monitors, 6)
                } else {
                  this.setState({
                    type: 'monitorId',
                    focusIndex: 6
                  })
                  NativeModules.PDAScan.onScan();
                }
              }}
                onPressIn={() => {
                  if (monitorSetup == '扫码') {
                    this.setState({
                      type: 'monitorId',
                      focusIndex: 6
                    })
                    NativeModules.PDAScan.onScan();
                  }
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}>
                <ListItemScan
                  isButton={monitorSetup == '扫码' && !isGetTeamPeopleInfo}
                  focusStyle={focusIndex == '6' ? styles.focusColor : {}}
                  title='班长确认'
                  rightElement={
                    this.monitorNameItem(6)
                  }
                />
              </TouchableCustom>
              <ListItemScan
                title='班组名称'
                rightTitleStyle={{ width: width / 1.5, textAlign: 'right', fontSize: 15 }}
                rightTitle={this.state.monitorTeamName}
              />
              <TouchableCustom underlayColor={'lightgray'} onPress={() => {
                if (monitorSetup != '扫码') {
                  this.isBottomVisible(Inspectors, 7)
                } else {
                  this.setState({
                    type: 'InspectorId',
                    focusIndex: 7
                  })
                  NativeModules.PDAScan.onScan();
                }
              }}
                onPressIn={() => {
                  if (monitorSetup == '扫码') {
                    this.setState({
                      type: 'InspectorId',
                      focusIndex: 7
                    })
                    NativeModules.PDAScan.onScan();
                  }
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}>
                <ListItemScan
                  isButton={monitorSetup == '扫码' && !isGetInspector}
                  focusStyle={focusIndex == '7' ? styles.focusColor : {}}
                  title='质检确认'
                  rightElement={
                    this.inspectorItem('7')
                  }
                />
              </TouchableCustom>
              <ListItemScan
                focusStyle={focusIndex == '8' ? styles.focusColor : {}}
                title='备注'
                rightElement={
                  <View>
                    <Input
                      containerStyle={styles.quality_input_container}
                      inputContainerStyle={styles.inputContainerStyle}
                      inputStyle={[styles.quality_input_, { top: 7 }]}
                      placeholder='请输入'
                      value={remark}
                      onChangeText={(value) => {
                        this.setState({
                          remark: value
                        })
                      }} />
                  </View>
                }
              />
            </View>
            {
              isCheck ? <FormButton
                backgroundColor='#EB5D20'
                onLongPress={() => {
                  Alert.alert(
                    '提示信息',
                    '是否删除构件信息',
                    [{
                      text: '取消',
                      style: 'cancel'
                    }, {
                      text: '删除',
                      onPress: () => {
                        this.DeleteData()
                      }
                    }])
                }}
                title='删除'
              /> :
                <FormButton
                  backgroundColor='#17BC29'
                  onPress={this.PostData}
                  title='保存'
                  disabled={this.state.isPhotoUpdate}
                />
            }

             {this.renderZoomPicture()}

            {/* {overlayImg(obj = {
              onRequestClose: () => {
                this.setState({ imageOverSize: !this.state.imageOverSize }, () => {
                  console.log("🚀 ~ file: equipment_maintenance.js:1696 ~ render ~ imageOverSize:", this.state.imageOverSize)
                })
              },
              onPress: () => {
                this.setState({
                  imageOverSize: !this.state.imageOverSize
                })
              },
              uri: this.state.pictureUri,
              isVisible: this.state.imageOverSize
            })
            }*/}

            <Overlay
              fullScreen={true}
              animationType='fade'
              isVisible={this.state.isPDFVisible}
              onRequestClose={() => {
                this.setState({ isPDFVisible: !this.state.isPDFVisible });
              }}
            >
              <Header
                containerStyle={{ height: 45, paddingTop: 0, top: -5 }}
                centerComponent={{ text: '图纸预览', style: { color: 'gray', fontSize: 20 } }}
                rightComponent={<BackIcon
                  onPress={() => {
                    this.setState({
                      isPDFVisible: !this.state.isPDFVisible
                    });
                  }} />}
                backgroundColor='white'
              />
              <View style={{ flex: 1 }}>
                <Pdf
                  source={{
                    uri: paperUrl,
                    //method: 'GET', //默认 'GET'，请求 url 的方式
                  }}
                  fitWidth={true} //默认 false，若为 true 则不能将 fitWidth = true 与 scale 一起使用
                  fitPolicy={0} // 0:宽度对齐，1：高度对齐，2：适合两者（默认）
                  page={1}
                  //scale={1}
                  onLoadComplete={(numberOfPages, filePath, width, height, tableContents) => {
                    console.log(`number of pages: ${numberOfPages}`); //总页数
                    console.log(`number of filePath: ${filePath}`); //本地返回的路径
                    console.log(`number of width: `, JSON.stringify(width));
                    console.log(`number of height: ${JSON.stringify(height)}`);
                    console.log(`number of tableContents: ${tableContents}`);
                  }}
                  onError={(error) => {
                    console.log(error);
                  }}
                  minScale={1} //最小模块
                  maxScale={3}
                  enablePaging={true} //在屏幕上只能显示一页
                  style={{
                    flex: 1,
                    width: width
                  }}
                />
              </View>

            </Overlay>

            <BottomSheet
              isVisible={this.state.CameraVisible}
              onRequestClose={() => {
                this.setState({ CameraVisible: false })
              }}
            >
              <TouchableCustom
                onPress={() => {
                  launchCamera(
                    {
                      mediaType: 'photo',
                      includeBase64: false,
                      //maxHeight: 600,
                      //maxWidth: 600,
                      saveToPhotos: true
                    },
                    (response) => {
                      if (response.uri == undefined) {
                        return
                      }
                      imageArr.push(response.uri)
                      this.setState({
                        pictureSelect: true,
                        pictureUri: response.uri,
                        imageArr: imageArr
                      })
                      console.log(response)
                    },
                  )
                  this.setState({ CameraVisible: false })
                }} >
                <ListItemScan
                  title='拍照' >
                </ListItemScan>
              </TouchableCustom>
              <TouchableCustom
                onPress={() => {
                  launchImageLibrary(
                    {
                      mediaType: 'photo',
                      includeBase64: false,
                      maxHeight: 600,
                      maxWidth: 600,

                    },
                    (response) => {
                      if (response.uri == undefined) {
                        return
                      }
                      imageArr.push(response.uri)
                      this.setState({
                        pictureSelect: true,
                        pictureUri: response.uri,
                        imageArr: imageArr
                      })
                      console.log(response)
                    },
                  )
                  this.setState({ CameraVisible: false })
                }} >
                <ListItemScan
                  title='相册' >
                </ListItemScan>
              </TouchableCustom>
              <ListItemScan
                title='关闭'
                containerStyle={{ backgroundColor: 'red' }}
                onPress={() => {
                  this.setState({ CameraVisible: false })
                }} >
              </ListItemScan>
            </BottomSheet>

            <BottomSheet
              isVisible={datepickerVisible}
              onRequestClose={() => {
                this.setState({ datepickerVisible: false })
              }}
            >

              <ListItemScan
                title='确定'
                onPress={() => {
                  this.setState({
                    datepickerVisible: false
                  })
                }}
              ></ListItemScan>
            </BottomSheet>

            <BottomSheet
              isVisible={bottomVisible && !this.state.isCheckPage}
              onRequestClose={() => {
                this.setState({
                  bottomVisible: false
                })
              }}
              onBackdropPress={() => {
                this.setState({
                  bottomVisible: false
                })
              }}
            >
              <ScrollView style={styles.bottomsheetScroll} >
                {
                  bottomData.map((item, index) => {
                    let title = ''
                    if (item.rowguid) {
                      title = item.name + ' ' + item.teamName
                    } else {
                      title = item.InspectorName
                    }
                    return (
                      <TouchableCustom
                        onPress={() => {
                          if (item.rowguid) {
                            this.setState({
                              monitorId: item.rowguid,
                              monitorSelect: item.name,
                              monitorName: item.name,
                              monitorTeamName: item.teamName,
                            }, () => {
                              DeviceStorageData = {
                                "InspectorId": this.state.InspectorId,
                                "InspectorName": this.state.InspectorName,
                                "InspectorSelect": this.state.InspectorName,
                                "monitorId": this.state.monitorId,
                                "monitorName": this.state.monitorName,
                                "monitorSelect": this.state.monitorName,
                                "monitorTeamName": this.state.monitorTeamName,
                              }

                              DeviceStorage.save('DeviceStorageDataQI', JSON.stringify(DeviceStorageData))
                            })
                          } else if (item.InspectorId) {
                            this.setState({
                              InspectorId: item.InspectorId,
                              InspectorName: item.InspectorName,
                              InspectorSelect: item.InspectorName,
                            }, () => {
                              DeviceStorageData = {
                                "InspectorId": this.state.InspectorId,
                                "InspectorName": this.state.InspectorName,
                                "InspectorSelect": this.state.InspectorName,
                                "monitorId": this.state.monitorId,
                                "monitorName": this.state.monitorName,
                                "monitorSelect": this.state.monitorName,
                                "monitorTeamName": this.state.monitorTeamName,
                              }

                              DeviceStorage.save('DeviceStorageDataQI', JSON.stringify(DeviceStorageData))
                            })
                          }
                          this.setState({
                            bottomVisible: false
                          })
                        }}
                      >
                        <BottomItem backgroundColor='white' color="#333" title={title} />
                      </TouchableCustom>
                    )

                  })
                }
              </ScrollView>
              <TouchableHighlight
                onPress={() => {
                  this.setState({ bottomVisible: false })
                }}>
                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
              </TouchableHighlight>

            </BottomSheet>

            <BottomSheet
              isVisible={isPaperTypeVisible}
              onRequestClose={() => {
                this.setState({
                  isPaperTypeVisible: false
                })
              }}
              onBackdropPress={() => {
                this.setState({
                  isPaperTypeVisible: false
                })
              }}
            >
              <ScrollView style={styles.bottomsheetScroll}>
                {
                  paperList.map((item, index) => {
                    return (
                      <TouchableCustom
                        onPress={() => {
                          this.paperResData(item)
                        }}
                      >
                        <BottomItem backgroundColor='white' color="#333" title={item} />
                      </TouchableCustom>
                    )

                  })
                }
              </ScrollView>
              <TouchableHighlight
                onPress={() => {
                  this.setState({ isPaperTypeVisible: false })
                }}>
                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
              </TouchableHighlight>

            </BottomSheet>
          </ScrollView>
        </View>
      )
    }
  }

}
