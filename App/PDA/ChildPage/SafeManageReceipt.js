import React from 'react';
import { View, TouchableHighlight, Dimensions, ActivityIndicator, Alert, Image } from 'react-native';
import { Button, Text, Input, Icon, Card, ListItem, Overlay, Header } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { ScrollView } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker'
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { NavigationEvents } from 'react-navigation';
import { styles } from '../Componment/PDAStyles';
import ListItemScan from '../Componment/ListItemScan';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import CheckBoxScan from '../Componment/CheckBoxScan';
import AvatarImg from '../Componment/AvatarImg';
import ListItemScan_child from '../Componment/ListItemScan_child';
import overlayImg from '../Componment/OverlayImg';
import OverlayImgZoomPicker from '../Componment/OverlayImgZoomPicker';
let letGetError = false

let DeviceStorageData = {} //缓存数据

const { height, width } = Dimensions.get('window') //获取宽高

let checkStandardListAll = [] //危险源列表

let fileflag = "11"

let imageArr = [], imageFileArr = [];

class BackIcon extends React.PureComponent {
    render() {
        return (
            <TouchableCustom
                onPress={this.props.onPress} >
                <Icon
                    name='arrow-back'
                    color='#419FFF' />
            </TouchableCustom>
        )
    }
}

export default class SafeManageReceipt extends React.PureComponent {
    constructor() {
        super();
        this.state = {
            proData: "",
            resData: [],
            ServerTime: "", // 时间工具用

            rectifyNoticeInfo: '', // 整改单号
            rectifyBy: '', // 整改签收人
            teamName: '', // 班组
            rechecker: '', // 复查人
            receiptDate: '', // 回执日期
            checkStandard: [], // 危险源
            opinion: '', // 复查意见

            //底栏控制
            CameraVisible: false,
            response: '',

            checked: true,
            pictureVisible: true, // 问题照片
            imageOverSize: false, // 照片放大
            pictureUri: "",
            imageArr: [],
            imageFileArr: [],
            fileid: "",
            recid: "",
            isPhotoUpdate: false,
            focusIndex: -1,
            isLoading: false,

            //查询界面取消选择按钮
            isCheck: false,
            isCheckPage: false,
            //控制app拍照，日期，相册选择功能
            isAppPhotoSetting: false,
      currShowImgIndex: 0,
            isAppDateSetting: false,
            isAppPhotoAlbumSetting: false
        }
        //this.requestCameraPermission = this.requestCameraPermission.bind(this)
    }

    componentDidMount() {

        const { navigation } = this.props;
        let guid = navigation.getParam('guid') || ''
        let pageType = navigation.getParam('pageType') || ''
        let dateNow = this.dateFormat("YYYY-mm-dd", new Date());
        let isAppPhotoSetting = navigation.getParam('isAppPhotoSetting')
        let isAppDateSetting = navigation.getParam('isAppDateSetting')
        let isAppPhotoAlbumSetting = navigation.getParam('isAppPhotoAlbumSetting')
        this.setState({
            isAppPhotoSetting: isAppPhotoSetting,
            isAppDateSetting: isAppDateSetting,
            isAppPhotoAlbumSetting: isAppPhotoAlbumSetting
        })
        this.checkResData(guid, pageType)
        this.setState({
            ServerTime: dateNow,
            isCheckPage: pageType == 'CheckPage' ? true : false
        })
    }

    checkResData = (guid, pageType) => {
        let formData = new FormData();
        let data = {};
        data = {
            "action": "safemanagenoticequerydetail",
            "servicetype": "pda",
            "express": "FA51025B",
            "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
            "data": {
                "guid": guid,
                "factoryId": Url.PDAFid
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: SafeManageReceipt.js:121 ~ SafeManageReceipt ~ formData:", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log("res ---- " + JSON.stringify(Url));
            return res.json();
        }).then(resData => {
            console.log("🚀 ~ file: qualityinspection.js ~ line 226 ~ QualityInspection ~ resData", resData)
            this.checkStandardListAllInit(resData.result.rectifyNoticeSub)
            this.setState({
                rectifyNoticeInfo: resData.result.rectifyNoticeInfo,
                teamName: resData.result.team,
                rechecker: pageType == 'CheckPage' ? resData.result.rechecker : Url.PDAEmployeeName,
                receiptDate: resData.result.receiptDate || dateNow,
                rectifyBy: resData.result.rectifyBy,
                checkStandard: resData.result.rectifyNoticeSub,
                opinion: resData.result.opinion,
            })
        }).catch((error) => {
            console.log("error --- " + error)
        });

    }

    componentWillUnmount() {
        checkStandardListAll = []
    }


    //顶栏
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('title')
        }
    }

    // 时间工具
    _DatePicker = (index, data) => {
        //const { ServerTime } = this.state
        data.rectify_date == "" ? data.rectify_date = this.dateFormat("YYYY-mm-dd", new Date()) : data.rectify_date
        return (
            <DatePicker
                customStyles={{
                    dateInput: styles.dateInput,
                    dateTouchBody: styles.dateTouchBody,
                    dateText: this.state.isAppDateSetting ? styles.dateText : styles.dateDisabled,
                }}
                iconComponent={<Icon name='caretdown' color={this.state.isAppDateSetting ? '#419fff' : "#666"} iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
                }
                showIcon={true}
                date={data.rectify_date}
                onOpenModal={() => {
                    this.setState({ focusIndex: index })
                }}
                format="YYYY-MM-DD"
                mode="date"
                disabled={!this.state.isAppDateSetting}
                onDateChange={(value) => {
                    data.rectify_date = value
                    this.setState({
                        ServerTime: value
                    })
                }}
            />
        )
    }

    PostData = (opt) => {
        const { rectifyBy, rectifyNoticeInfo, teamName, opinion, rechecker, recid } = this.state
        if (rectifyBy == '' || rectifyBy == '请填写姓名') {
            this.toast.show('整改签收人不能为空');
            return
        }
        let checkResult = true;
        checkStandardListAll.map((item, index) => {
            if (item.rectify_result == '完成') {
                if (item.rectify_info == '' || item.rectify_info == '请填写') {
                    this.toast.show('整改结果完成项：整改情况不能为空');
                    checkResult = false;
                    return
                }
            }
        })
        console.log("🚀 ~ file: SafeManageReceipt.js:208 ~ SafeManageReceipt ~ checkStandardListAll.map ~ checkStandardListAll:", checkStandardListAll)
        console.log("🚀 ~ file: SafeManageReceipt.js:211 ~ SafeManageReceipt ~ this.state.isAppPhotoSetting:", this.state.isAppPhotoSetting)

        if (this.state.isAppPhotoSetting) {
            let NotTakePhoto = false
            checkStandardListAll.map((item, index) => {
                if (item.rectify_result == '完成') {
                    if (item.image_arr == "") {
                        NotTakePhoto = true
                        return
                    }
                }
            })
            if (NotTakePhoto) {
                this.toast.show('整改结果完成项: 没有拍摄整改照片')
                return
            }
        }

        if (opt == 'save') {
        } else if (opt == 'commit') {
            checkStandardListAll.map((item, index) => {
                if (item.rectify_result == '未完成') {
                    this.toast.show('整改结果有未完成项，不能提交');
                    checkResult = false;
                    return
                }
            })
        }
        if (checkResult == false) return;

        this.toast.show(saveLoading, 0)

        let formData = new FormData();
        let data = {};

        data = {
            "action": "safemanagerectifysave",
            "servicetype": "pda",
            "express": "06C795EE",
            "ciphertext": "a1e3818d57d9bfc9437c29e7a558e32e",
            "data": {
                "action": opt,
                "userName": Url.PDAEmployeeName,
                "factoryId": Url.PDAFid,
                "rectifyNoticeInfo": rectifyNoticeInfo,
                "teamName": teamName,
                "rechecker": rechecker,
                "receiptDate": this.dateFormat("YYYY-mm-dd", new Date),
                "rectifyBy": rectifyBy,
                "checkStandardList": checkStandardListAll,
                "opinion": opinion,
                "editEmployeeId": Url.PDAEmployeeId,
                "editEmployeeName": rechecker,
            },
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: SafeManageReceipt.js:233 ~ SafeManageReceipt ~ formData:", formData)
        if (!Url.isAppNewUpload) {
            checkStandardListAll.map((item, index) => {
                if (item.image_arr != "") {
                    formData.append('img_' + index, {
                        uri: item.image_arr,
                        type: 'image/jpeg',
                        name: 'img_' + index
                    })
                }
            })
        }
        //console.log("--------------" + JSON.stringify(data))
        fetch(Url.PDAurl, {
            method: 'post',
            headers: {
                'content-type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res);
            return res.json();
        }).then(resData => {
            if (resData.status == '100') {
                this.toast.show('保存成功');
                this.props.navigation.goBack()
            } else {
                this.toast.show('保存失败')
                Alert.alert('保存失败', resData.message)
            }
        }).catch((error) => {
            console.log("error --- " + error)
            if (error.toString().indexOf('not valid JSON') != -1) {
                Alert.alert('保存失败', "响应内容不是合法JSON格式")
                return
            }
            if (error.toString().indexOf('Network request faile') != -1) {
                Alert.alert('保存失败', "网络请求错误")
                return
            }
            Alert.alert('保存失败', error.toString())
        });

    }

    // 获取登录时间
    getTodayDate() {
        let time = this.dateFormat("YYYY-mm-dd", new Date)
        return (
            <Text>{time}</Text>
        )
    }

    // 日期格式化
    dateFormat(fmt, date) {
        let ret;
        const opt = {
            "Y+": date.getFullYear().toString(),        // 年
            "m+": (date.getMonth() + 1).toString(),     // 月
            "d+": date.getDate().toString(),            // 日
            "H+": date.getHours().toString(),           // 时
            "M+": date.getMinutes().toString(),         // 分
            "S+": date.getSeconds().toString()          // 秒
            // 有其他格式化字符需求可以继续添加，必须转化成字符串
        };
        for (let k in opt) {
            ret = new RegExp("(" + k + ")").exec(fmt);
            if (ret) {
                fmt = fmt.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")))
            };
        };
        return fmt;
    }

    cameraFunc = (data) => {
        launchCamera(
            {
                mediaType: 'photo',
                includeBase64: false,
                maxHeight: parseInt(Url.isResizePhoto),
                maxWidth: parseInt(Url.isResizePhoto),
                saveToPhotos: true,

            },
            (response) => {
                if (response.uri == undefined) {
                    return
                }
                data.image_arr = response.uri

                data.picture_visible = false
                data.picture_select = true

                this.setState({ check: !this.state.check })
                if (Url.isAppNewUpload) {
                    this.cameraPost(response.uri, data)
                } else {
                    this.setState({
                        pictureSelect: true,
                    })
                }
                console.log(response)
            },
        )
    }

    camLibraryFunc = (data) => {
        launchImageLibrary(
            {
                mediaType: 'photo',
                includeBase64: false,
                maxHeight: parseInt(Url.isResizePhoto),
                maxWidth: parseInt(Url.isResizePhoto),
            },
            (response) => {
                if (response.uri == undefined) {
                    return
                }
                data.image_arr = response.uri
                data.picture_visible = false
                data.picture_select = true
                this.setState({ check: !this.state.check })
                if (Url.isAppNewUpload) {
                    this.cameraPost(response.uri, data)
                } else {
                    this.setState({
                        pictureSelect: true,
                    })
                }
                console.log(response)
            },
        )
    }

    camandlibFunc = (data) => {
        Alert.alert(
            '提示信息',
            '选择拍照方式',
            [{
                text: '取消',
                style: 'cancel'
            }, {
                text: '拍照',
                onPress: () => {
                    this.cameraFunc(data)
                }
            }, {
                text: '相册',
                onPress: () => {
                    this.camLibraryFunc(data)
                }
            }])
    }

     // 展示/隐藏 放大图片
  handleZoomPicture = (flag, index) => {
    this.setState({
      imageOverSize: false,
      currShowImgIndex: index || 0
    })
  }

  // 加载放大图片弹窗
  renderZoomPicture = () => {
    const { imageOverSize, currShowImgIndex, imageFileArr, pictureUri } = this.state
    let imgArr = [{
        url: pictureUri
    }]
    return (
      <OverlayImgZoomPicker
        isShowImage={imageOverSize}
        currShowImgIndex={currShowImgIndex}
        zoomImages={imgArr}
        callBack={(flag) => this.handleZoomPicture(flag)}
      ></OverlayImgZoomPicker>
    )
  }

  cameraPost = (uri, data) => {
        const { recid } = this.state
        this.setState({ isPhotoUpdate: true })
        this.toast.show('图片上传中', 2000)

        let formData = new FormData();
        let data1 = {};
        data1 = {
            "action": "savephote",
            "servicetype": "photo_file",
            "express": "D2979AB2",
            "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
            "data": {
                "fileflag": fileflag,
                "username": Url.PDAusername,
                "recid": recid
            }
        }
        formData.append('jsonParam', JSON.stringify(data1))
        formData.append('img', {
            uri: uri,
            type: 'image/jpeg',
            name: 'img'
        })
        console.log("🚀 ~ file: concrete_pouring.js ~ line 672 ~ SteelCage ~ formData", formData)

        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            console.log(res.statusText);
            console.log(res);
            return res.json();
        }).then(resData => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 690 ~ SteelCage ~ resData", resData)
            if (resData.status == '100') {
                let fileid = resData.result.fileid;
                let recid = resData.result.recid;

                let tmp = {}
                tmp.fileid = fileid
                tmp.uri = uri
                tmp.url = uri
                imageFileArr.push(tmp)
                data.fileid = fileid
                data.recid = recid
                console.log("🚀 ~ file: SafeManageReceipt.js:429 ~ SafeManageReceipt ~ data:", data)
                this.setState({
                    fileid: fileid,
                    recid: "",
                    pictureSelect: true,
                    imageFileArr: imageFileArr
                })
                console.log("🚀 ~ file: concrete_pouring.js ~ line 704 ~ SteelCage ~ imageFileArr", imageFileArr)
                this.toast.show('图片上传成功');
                this.setState({
                    isPhotoUpdate: false
                })
            } else {
                Alert.alert('图片上传失败', resData.message)
                this.toast.close(1)
            }
        }).catch((error) => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 692 ~ SteelCage ~ error", error)
        });
    }

    cameraDelete = (data) => {
        console.log("🚀 ~ file: SafeManageReceipt.js:451 ~ SafeManageReceipt ~ cameraDelete ~ data:", data)
        if (Url.isAppNewUpload) {
            let formData = new FormData();
            let data1 = {};
            data1 = {
                "action": "deletephote",
                "servicetype": "photo_file",
                "express": "D2979AB2",
                "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
                "data": {
                    "fileid": data.fileid,
                }
            }
            formData.append('jsonParam', JSON.stringify(data1))
            console.log("🚀 ~ file: concrete_pouring.js ~ line 733 ~ SteelCage ~ cameraDelete ~ formData", formData)
            fetch(Url.PDAurl, {
                method: 'POST',
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                body: formData
            }).then(res => {
                console.log(res.statusText);
                console.log(res);
                return res.json();
            }).then(resData => {
                console.log("🚀 ~ file: concrete_pouring.js ~ line 745 ~ SteelCage ~ cameraDelete ~ resData", resData)
                if (resData.status == '100') {
                    data.image_arr = ""
                    data.fileid = ""
                    data.picture_select = false
                    data.picture_visible = true
                    this.setState({ check: !this.state.check }, () => { this.forceUpdate() })
                    this.toast.show('图片删除成功');
                } else {
                    Alert.alert('图片删除失败', resData.message)
                    this.toast.close(1)
                }
            }).catch((error) => {
                console.log("🚀 ~ file: concrete_pouring.js ~ line 761 ~ SteelCage ~ cameraDelete ~ error", error)
            });
        } else {
            data.image_arr = ""
            data.picture_select = false
            data.picture_visible = true
            this.setState({ check: !this.state.check }, () => { this.forceUpdate() })
        }

    }

    // 问题照片
    getPicture = (data) => {
        return (
            <View>
                {
                    data.picture_visible ? <Icon name='camera-alt' color='#4D8EF5' iconProps={{ size: 32 }} onPress={() => {
                        if (this.state.isPhotoUpdate == false) {
                            if (this.state.isAppPhotoAlbumSetting) {
                                this.camandlibFunc(data)
                            } else {
                                this.cameraFunc(data)
                            }
                        }
                        /* this.setState({
                          CameraVisible: true
                        }) */
                    }}
                    ></Icon> : <View />

                }
                {this.imageView(data)}
            </View>
        )
    }

    imageView = (data) => {
        data.picture_select = this.state.isCheckPage ? true : data.picture_select
        data.image_arr = this.state.isCheckPage ? data.picture : data.image_arr
        return (
            <View style={{ flexDirection: 'row' }}>
                {
                    data.picture_select ?
                        <AvatarImg item={data.image_arr} isRounded="false" index='0'
                            onPress={() => {
                                this.setState({
                                    imageOverSize: true,
                                    pictureUri: data.image_arr
                                })
                            }}
                            onLongPress={() => {
                                Alert.alert(
                                    '提示信息',
                                    '是否删除照片',
                                    [{
                                        text: '取消',
                                        style: 'cancel'
                                    }, {
                                        text: '删除',
                                        onPress: () => {
                                            //console.log("🚀 ~ file: qualityinspection.js ~ line 649 ~ SteelCage ~ this.state.imageArr.map ~ i", data.image_arr)
                                            this.cameraDelete(data)
                                            //console.log("🚀 ~ file: qualityinspection.js ~ line 656 ~ SteelCage ~ this.state.imageArr.map ~ i", data.image_arr)
                                        }
                                    }])
                            }}
                        /> : <View></View>
                }
            </View>
        )
    }

    // 危险源列表数据初始化
    checkStandardListAllInit(check) {
        checkStandardListAll = []
        check.map((r, i) => {
            checkStandardListAll.push(
                {
                    "post": r.post,
                    "check_standard": r.check_standard,
                    "rectify_info": r.rectify_info || '',
                    "image_arr": r.rectify_picture || '',
                    "picture": r.rectify_picture || '',
                    "rectify_picture": r.rectify_picture || '',
                    "rectify_date": r.rectify_date || '',
                    "rectify_result": r.rectify_result || "未完成",
                    "picture_visible": r.rectify_picture == "" ? true : false,
                    "picture_select": r.rectify_picture == "" ? false : true,
                    "check_box_checked": r.rectify_result == "完成" ? true : false,
                    "recid": r.recid,
                }
            )
        })
    }

    render() {
        const { navigation } = this.props;
        //从主页面传url, 未来集成到一起
        const QRurl = navigation.getParam('QRurl') || '';
        const QRid = navigation.getParam('QRid') || ''
        const type = navigation.getParam('type') || '';
        let pageType = navigation.getParam('pageType') || ''

        if (type == 'compid') {
            QRcompid = QRid
        }
        const { focusIndex, pictureVisible, rectifyNoticeInfo, rectifyBy, opinion, teamName, rechecker, receiptDate, isCheckPage } = this.state

        if (this.state.isLoading) {
            return (
                <View style={styles.loading}>
                    <ActivityIndicator
                        animating={true}
                        color='#419FFF'
                        size="large" />
                </View>
            )
            //渲染页面
        } else {
            return (
                <View style={{ flex: 1 }}>
                    <Toast ref={(ref) => { this.toast = ref; }} position="center" />
                    <NavigationEvents
                        onWillFocus={this.UpdateControl}
                        onWillBlur={() => {
                            if (QRid != '') {
                                navigation.setParams({ QRid: '', type: '' })
                            }
                        }} />
                    <ScrollView ref={ref => this.scoll = ref} style={styles.mainView} showsVerticalScrollIndicator={false} >

                        <View style={styles.listView}>
                            <ListItemScan focusStyle={focusIndex == '0' ? styles.focusColor : {}} title='整改单号:' rightElement={rectifyNoticeInfo} />

                            <ListItemScan focusStyle={focusIndex == '1' ? styles.focusColor : {}} title='班组:' rightElement={teamName} />

                            <ListItemScan focusStyle={focusIndex == '2' ? styles.focusColor : {}} title='复查人:' rightElement={rechecker} />

                            <ListItemScan focusStyle={focusIndex == '3' ? styles.focusColor : {}} title='回执日期:' rightElement={receiptDate} />

                            <ListItemScan focusStyle={focusIndex == '4' ? styles.focusColor : {}} title='整改签收人:' rightElement={() =>
                                isCheckPage ? <Text>{rectifyBy}</Text> :
                                    <View>
                                        <Input
                                            containerStyle={styles.quality_input_container}
                                            inputContainerStyle={styles.inputContainerStyle}
                                            inputStyle={[styles.quality_input_, { top: 7 }]}
                                            placeholder='请填写姓名'
                                            value={rectifyBy}
                                            onChangeText={(value) => {
                                                this.setState({
                                                    rectifyBy: value
                                                })
                                            }} />
                                    </View>} />
                        </View>

                        {/*危险源列表*/}
                        {
                            checkStandardListAll.map((l, i) => {
                                return (
                                    <View style={styles.listView} key={i}>
                                        <Card containerStyle={styles.card_containerStyle}>
                                            {/*<View style={{marginBottom: 5,marginTop: 5,flexDirection: 'row' }}>*/}
                                            {/*    <Text style={[styles.text]} style={{ flex: 1, color: '#999' }} numberOfLines={1}> {'岗位：'}</Text>*/}
                                            {/*    <Text style={{ flex: 2 }}> {l.post}</Text>*/}
                                            {/*</View>*/}
                                            {/*<View style={{ marginBottom: 5, marginTop: 5, flexDirection: 'row' }}>*/}
                                            {/*    <Text style={[styles.text]} style={{ flex: 1, color: '#999' }} numberOfLines={1}> {'危险源：'}</Text>*/}
                                            {/*    <Text style={{ flex: 2 }}> {l.check_standard}</Text>*/}
                                            {/*</View>*/}

                                            <ListItem
                                                title="岗位："
                                                rightTitle={l.post}
                                                containerStyle={styles.list_container_style}
                                                titleStyle={styles.title_child}
                                                rightTitleStyle={{
                                                    color: '#333',
                                                    fontSize: 13,
                                                    lineHeight: 14,
                                                    flexWrap: 'wrap',
                                                    width: Dimensions.get('window').width / 2 - 10
                                                }}
                                            />
                                            <ListItem
                                                title="危险源："
                                                rightTitle={l.check_standard}
                                                containerStyle={styles.list_container_style}
                                                titleStyle={styles.title_child}
                                                rightTitleStyle={{
                                                    color: '#333',
                                                    fontSize: 13,
                                                    lineHeight: 14,
                                                    flexWrap: 'wrap',
                                                    width: Dimensions.get('window').width / 2 - 10
                                                }}
                                            />
                                        </Card>
                                        {
                                            isCheckPage ? <ListItemScan focusStyle={focusIndex == '5' + i ? styles.focusColor : {}} title='整改情况:' rightElement={l.rectify_info} /> :
                                                <ListItemScan focusStyle={focusIndex == '5' + i ? styles.focusColor : {}} title='整改情况:' rightElement={
                                                    <View>
                                                        <Input
                                                            containerStyle={styles.quality_input_container}
                                                            inputContainerStyle={styles.inputContainerStyle}
                                                            inputStyle={[styles.quality_input_, { top: 7 }]}
                                                            placeholder='请填写'
                                                            value={l.rectify_info}
                                                            onChangeText={(value) => {
                                                                l.rectify_info = value
                                                                this.setState({
                                                                    checked: !this.state.checked
                                                                })
                                                            }} />
                                                    </View>} />
                                        }
                                        <ListItemScan focusStyle={focusIndex == '6' + i ? styles.focusColor : {}} title='整改后照片:' rightElement={() =>
                                            isCheckPage ? this.imageView(l) : pictureVisible ? this.getPicture(l) : this.imageView()} />
                                        <ListItemScan focusStyle={focusIndex == '7' + i ? styles.focusColor : {}} title='整改完成日期:' rightElement={() =>
                                            isCheckPage ? <Text>{l.rectify_date}</Text> :
                                                this._DatePicker('10' + i, l)} />
                                        {
                                            isCheckPage ? <ListItemScan focusStyle={focusIndex == '8' + i ? styles.focusColor : {}} title='整改结果:' rightElement={l.rectify_result} /> :
                                                <ListItemScan focusStyle={focusIndex == '8' + i ? styles.focusColor : {}} title='整改结果:' rightTitle={
                                                    <View style={{ flexDirection: "row", marginRight: -23, paddingVertical: 0 }}>
                                                        <CheckBoxScan
                                                            title='完成'
                                                            checked={l.check_box_checked}
                                                            onPress={() => {
                                                                l.rectify_result = '完成'
                                                                l.check_box_checked = !l.check_box_checked
                                                                this.setState({
                                                                    checked: !this.state.checked
                                                                })
                                                            }}
                                                        />
                                                        <CheckBoxScan
                                                            title='未完成'
                                                            checked={!l.check_box_checked}
                                                            onPress={() => {
                                                                l.rectify_result = '未完成'
                                                                l.check_box_checked = !l.check_box_checked
                                                                this.setState({
                                                                    checked: !this.state.checked
                                                                })
                                                            }}
                                                        />
                                                    </View>
                                                } />
                                        }

                                    </View>
                                )
                            })
                        }

                        <View style={styles.listView}>
                            <ListItemScan focusStyle={focusIndex == '9' ? styles.focusColor : {}} title='复查意见:' rightElement={() =>
                                isCheckPage ? <Text>{opinion}</Text> :
                                    <View>
                                        <Input
                                            containerStyle={styles.quality_input_container}
                                            inputContainerStyle={styles.inputContainerStyle}
                                            inputStyle={[styles.quality_input_, { top: 7 }]}
                                            placeholder='请填写'
                                            value={opinion}
                                            onChangeText={(value) => {
                                                this.setState({
                                                    opinion: value
                                                })
                                            }} />
                                    </View>
                            } />
                        </View>
                        {
                            isCheckPage ? <Text /> :
                                <View style={{ flexDirection: 'row' }}>
                                    <View style={{ flex: 1 }}><Button
                                        title='保存'
                                        titleStyle={{ color: "white", fontSize: 17 }}
                                        type='solid'
                                        style={{ marginVertical: 10 }}
                                        containerStyle={{ marginVertical: 10, marginHorizontal: 8 }}
                                        buttonStyle={{ backgroundColor: '#17BC29', borderRadius: 10, paddingVertical: 16 }}
                                        onPress={() => {
                                            this.PostData('save')
                                        }}
                                        disabled={this.state.isPhotoUpdate}
                                    ></Button></View>
                                    <View style={{ flex: 1 }}><Button
                                        title='提交'
                                        titleStyle={{ color: "white", fontSize: 17 }}
                                        type='solid'
                                        style={{ marginVertical: 10 }}
                                        containerStyle={{ marginVertical: 10, marginHorizontal: 8 }}
                                        buttonStyle={{ backgroundColor: '#FF9E00', borderRadius: 10, paddingVertical: 16 }}
                                        onPress={() => { this.PostData('commit') }}
                                        disabled={this.state.isPhotoUpdate}
                                    ></Button></View>
                                </View>
                        }

                        {/*照片放大*/}
                        {this.renderZoomPicture()}
                        {/* {overlayImg(obj = {
                            onRequestClose: () => {
                                this.setState({ imageOverSize: !this.state.imageOverSize }, () => {
                                    console.log("🚀 ~ file: equipment_maintenance.js:1696 ~ render ~ imageOverSize:", this.state.imageOverSize)
                                })
                            },
                            onPress: () => {
                                this.setState({
                                    imageOverSize: !this.state.imageOverSize
                                })
                            },
                            uri: this.state.pictureUri,
                            isVisible: this.state.imageOverSize
                        })
                        } */}
                    </ScrollView>
                </View>
            )
        }
    }

}


