import React from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, ListItem, BottomSheet, CheckBox } from 'react-native-elements';
import Url from '../../Url/Url';
import Toast from 'react-native-easy-toast';
import { ToDay, YesterDay, BeforeYesterDay } from '../../CommonComponents/Data';
import { RFT } from '../../Url/Pixal';
import { ScrollView } from 'react-native-gesture-handler';
import { styles } from "../Componment/PDAStyles";
import { NavigationEvents } from 'react-navigation';
import ScanButton from '../Componment/ScanButton';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import ListItemScan_child from '../Componment/ListItemScan_child';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import CheckBoxScan from '../Componment/CheckBoxScan';
import AvatarAdd from '../Componment/AvatarAdd';
import AvatarImg from '../Componment/AvatarImg';


let varGetError = false

const { height, width } = Dimensions.get('window') //获取宽高

class CardList extends React.Component {
  constructor() {
    super();
    this.state = {

    }
  }

  render() {
    const { compData, compCode } = this.props
    let result = compData
    console.log("🚀 ~ file: CardList.js ~ line 15 ~ CardList ~ render ~ compData", compData)
    console.log("🚀 ~ file: CardList.js ~ line 16 ~ CardList ~ render ~ compData.result", compData.result)
    return (
      <View>
        <Card containerStyle={styles.card_containerStyle}>
          <ListItemScan_child
            title='产品编号'
            rightTitle={compCode}
          />
          <ListItemScan_child
            title='项目名称'
            rightTitle={result.projectName || result.ProjectName}
          />
          <ListItemScan_child
            title='构件类型'
            rightTitle={result.compTypeName}
          />
          <ListItemScan_child
            title='设计型号'
            rightTitle={result.designType}
          />
          <ListItemScan_child
            title='楼号'
            rightTitle={result.floorNoName}
          />
          <ListItemScan_child
            title='层号'
            rightTitle={result.floorName}
          />
          <ListItemScan_child
            title='砼方量'
            rightTitle={result.concretrVolume}
          />
          <ListItemScan_child
            title='重量'
            rightTitle={result.weight}
          />
          <ListItemScan_child
            title='库房'
            rightTitle={result.roomName}
          />
          <ListItemScan_child
            title='库位'
            rightTitle={result.libraryName}
          />
          <ListItemScan_child
            title='构件状态'
            rightTitle={result.bigState}
          />
        </Card>
      </View>
    )
  }
}

export default class InventoryCheck extends React.Component {
  constructor() {
    super();
    this.state = {
      buttondisable: false,
      bottomData: [],
      bottomVisible: false,
      resData: [],
      rowguid: '',
      ServerTime: '',
      compData: {},
      compId: '',
      compCode: '',
      checked: true,
      isGetcomID: false,
      iscomIDdelete: false,
      buttondisable: false,
      GetError: false
    }
    //this.requestCameraPermission = this.requestCameraPermission.bind(this)
  }

  componentDidMount() {
  }

  UpdateControl = () => {
    if (typeof this.props.navigation.getParam('QRid') != "undefined") {
      if (this.props.navigation.getParam('QRid') != "") {
        this.toast.show(Url.isLoadingView, 0)
      }
    }
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || ''
    const type = navigation.getParam('type') || '';
    if (type == 'compid') {
      this.GetcomID(QRid)
    }

  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  GetcomID = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "StockTaking",
      "servicetype": "pda",
      "express": "2934F95F",
      "ciphertext": "1bb0c4f99cbd1d1d56267152cb954ab6",
      "data": {
        "compId": id,
        "factoryId": Url.PDAFid,
        "userName": Url.PDAusername
      }
    }
    console.log("🚀 ~ file: inventory_check.js ~ line 141 ~ InventoryCheck ~ data", data)

    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
    console.log("🚀 ~ file: inventory_check.js ~ line 161 ~ InventoryCheck ~ resData", resData)
       this.toast.close()
      if (resData.status == '100') {
        let compdata = resData.result
        let compCode = compdata.compCode
        this.setState({
          compData: compdata,
          compCode: compCode,
          isGetcomID: true,
          iscomIDdelete: false
        }, () => {
          this.forceUpdate()
        })
      } else {
        console.log('resData', resData)
        this.toast.show(resData.message, 600)
       this.setState({
          compId: ""
        })
        //this.input1.focus()
        varGetError = true
        this.setState({
          GetError: true
        })
      }

    }).catch(err => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 210 ~ SteelCage ~ err", err)
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }


  comIDItem = () => {
    const { isGetcomID, buttondisable, compCode, compId } = this.state
    return (
      <View style={{ flexDirection: 'row' }}>
        <View>
          <Input
            ref={ref => { this.input1 = ref }}
            containerStyle={styles.scan_input_container}
            inputContainerStyle={styles.scan_inputContainerStyle}
            inputStyle={[styles.scan_input]}
            placeholder='请输入'
            keyboardType='numeric'
            value={compId}
            onChangeText={(value) => {
              value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
              value = value.replace(/^\./g, ""); //验证第一个字符是数字
              value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
              //value = value.replace(/\b(0+)/gi, ""); //清楚开头的0
              this.setState({
                compId: value
              })
            }}
            //onSubmitEditing={() => { this.input1.focus() }}
            //onFocus={() => { this.setState({ focusIndex: '5' }) }}
            /*  */
            //returnKeyType = 'previous'
            onSubmitEditing={() => {
              let inputComId = this.state.compId
              console.log("🚀 ~ file: qualityinspection.js ~ line 468 ~ QualityInspection ~ inputComId", inputComId)
              inputComId = inputComId.replace(/\b(0+)/gi, "")
              this.GetcomID(inputComId)
            }}
          />
        </View>
        <ScanButton
           
          onPress={() => {
            this.setState({
              iscomIDdelete: false,
              buttondisable: true
            })
            setTimeout(() => {
              this.setState({ GetError: false, buttondisable: false })
              varGetError = false
            }, 1500)
            this.props.navigation.navigate('QRCode', {
              type: 'compid',
              page: 'InventoryCheck'
            })
          }}
        />
      </View>

    )
  }

  render() {
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起

    const { resData, isGetcomID, compData, compCode, bottomVisible, bottomData } = this.state

    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (
        <View style={{ flex: 1 }}>
          <NavigationEvents
            onWillFocus={this.UpdateControl} />
          <Toast ref={(ref) => { this.toast = ref; }} position="bottom" />
          <ScrollView ref={ref => this.scoll = ref} style={styles.mainView}showsVerticalScrollIndicator={false} >
            <View style={styles.listView}>

              <ListItemScan
                isButton={!isGetcomID}
                title='产品编号'
                rightElement={
                  this.comIDItem()
                }
              />
              {
                JSON.stringify(compData) != "{}" ?
                  <CardList compData={compData} compCode={compCode} /> : <View></View>
              }
            </View>

          </ScrollView>
        </View>


      )
    }
  }

}