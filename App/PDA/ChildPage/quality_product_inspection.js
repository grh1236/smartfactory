import React from 'react';

import { View, TouchableOpacity, TouchableHighlight, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, ListItem, BottomSheet, CheckBox, Avatar, Overlay, Header } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { ToDay, YesterDay, BeforeYesterDay } from '../../CommonComponents/Data';
import { RFT } from '../../Url/Pixal';
import CardList from "../Componment/CardList";
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import DatePicker from 'react-native-datepicker'
import { ScrollView } from 'react-native-gesture-handler';
import { NavigationEvents } from 'react-navigation';
import { styles } from '../Componment/PDAStyles';
import ScanButton from '../Componment/ScanButton';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import ListItemScan_child from '../Componment/ListItemScan_child';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import CheckBoxScan from '../Componment/CheckBoxScan';
import AvatarAdd from '../Componment/AvatarAdd';
import AvatarImg from '../Componment/AvatarImg';
import DeviceStorage from '../../Url/DeviceStorage';
import Pdf from 'react-native-pdf';
import { DeviceEventEmitter, NativeModules } from 'react-native';
import overlayImg from '../Componment/OverlayImg';
import OverlayImgZoomPicker from '../Componment/OverlayImgZoomPicker';


class BackIcon extends React.PureComponent {
  render() {
    return (
      <TouchableCustom
        onPress={this.props.onPress} >
        <Icon
          name='arrow-back'
          color='#419FFF' />
      </TouchableCustom>
    )
  }
}


const { height, width } = Dimensions.get('window') //获取宽高

let varGetError = false

let DeviceStorageData = {} //缓存数据

let fileflag = "15"

let imageArr = [], imageFileArr = [],//照片数组
  checkimageArr = [], checkimageFileArr = [], //检查照片数组
  questionChickIdArr = [], questionChickArr = [], //子表问题数组
  isCheckSecondList = [], //是否展开二级
  isCheckedAllList = [] //二级菜单是否被全选

export default class QualityProductInspection extends React.Component {
  constructor() {
    super();
    this.state = {
      checkstr: '',
      bottomData: [],
      bottomVisible: false,
      resData: [],
      checkFormCode: '',
      type: 'compid',
      rowguid: '',
      ServerTime: '',
      monitorSetup: '',
      compId: '',
      compCode: '',
      isGetcomID: false,
      iscomIDdelete: false,
      monitors: [],
      //问题缺陷
      dictionary: [],
      problemDefect: '',
      measure: '',
      problemId: '',
      questionChick: -1,
      questionChickIdArr: [],
      questionChickArr: [],
      problemDefects: [],
      isCheckSecondList: [],
      isCheckedAllList: [],
      isQuestionOverlay: false,
      //质检员
      inspectors: [],
      InspectorId: '',
      InspectorName: '',
      InspectorSelect: '请选择',
      isGetInspector: false,
      isInspectordelete: false,
      acceptanceId: '',
      acceptanceName: '',
      //合格
      checked: true,
      checkResult: '合格',
      problemIdStr: '',
      record: '',
      steelCageIDs: ' ',
      buttondisable: false,
      //照片控制
      pictureSelect: false,
      pictureUri: '',
      imageArr: [],
      imageFileArr: [],
      fileid: "",
      recid: "",
      isPhotoUpdate: false,
      imageOverSize: false,
      CameraVisible: false,
      //check照片控制
      checkpictureSelect: false,
      checkpictureUri: '',
      checkimageArr: [],
      checkimageFileArr: [],
      checkfileid: "",
      checkrecid: "",
      checkimageOverSize: false,
      checkCameraVisible: false,
      GetError: false,
      //图纸
      isPaperTypeVisible: false,
      paperList: ["模版图", "配筋图", "预埋件装配图", "连接件排版图"],
      paperUrl: "",
      FileName: "",
      isPDFVisible: false,
      isLoading: true,
      focusIndex: 0,
      isCheckPage: false,
      remark: '',
      isQualityProductInspectionReview: false,
      //控制app拍照，日期，相册选择功能
      isAppPhotoSetting: false,
      currShowImgIndex: 0,
      isAppPhotoSetting1: false,
      isAppDateSetting: false,
      isAppPhotoAlbumSetting: false,
      isAppPhotoAlbumSetting1: false,
      checkCurrShowImgIndex: 0
    }
    //this.requestCameraPermission = this.requestCameraPermission.bind(this)
  }

  componentDidMount() {
    const { navigation } = this.props;
    let guid = navigation.getParam('guid') || ''
    let pageType = navigation.getParam('pageType') || ''
    let page = navigation.getParam('page') || ''
    let isAppPhotoSetting = navigation.getParam('isAppPhotoSetting')
    let isAppPhotoSetting1 = navigation.getParam('isAppPhotoSetting1')
    let isAppDateSetting = navigation.getParam('isAppDateSetting')
    let isAppPhotoAlbumSetting = navigation.getParam('isAppPhotoAlbumSetting')
    let isAppPhotoAlbumSetting1 = navigation.getParam('isAppPhotoAlbumSetting1')
    this.setState({
      isAppPhotoSetting: isAppPhotoSetting,
      isAppPhotoSetting1: isAppPhotoSetting1,
      isAppDateSetting: isAppDateSetting,
      isAppPhotoAlbumSetting: isAppPhotoAlbumSetting,
      isAppPhotoAlbumSetting1: isAppPhotoAlbumSetting1
    })
    if (pageType == 'CheckPage') {
      this.checkResData(guid)
      this.setState({
        isCheckPage: true,
        isQualityProductInspectionReview: page == '成品检复检' ? true : false
      })
    } else {
      this.resData()
      DeviceStorage.get('DeviceStorageDataQPI')
        .then(res => {
          if (res) {
            let DeviceStorageDataObj = JSON.parse(res)
            DeviceStorageData = DeviceStorageDataObj
            if (res.length != 0) {
              this.setState({
                "isGetInspector": DeviceStorageDataObj.isGetInspector,
                "InspectorId": DeviceStorageDataObj.InspectorId,
                "InspectorName": DeviceStorageDataObj.InspectorName,
                "InspectorSelect": DeviceStorageDataObj.InspectorName,
              })
            }
          }
        }).catch(err => {
          console.log("🚀 ~ file: loading.js ~ line 131 ~ SteelCage ~ componentDidMount ~ err", err)
        })
    }

    //通过使用DeviceEventEmitter模块来监听事件
    this.iDataScan = DeviceEventEmitter.addListener('iDataScan', (Event) => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 115 ~ SteelCage ~ Event.ScanResult", Event.ScanResult)
      if (typeof Event.ScanResult != undefined) {
        let data = Event.ScanResult
        let arr = data.split("=");
        console.log("🚀 ~ file: quality_product_inspection.js:195 ~ this.iDataScan=DeviceEventEmitter.addListener ~ arr:", arr)
        let id = ''
        id = arr[arr.length - 1]
        console.log("🚀 ~ file: concrete_pouring.js ~ line 118 ~ SteelCage ~ id", id)
        if (this.state.type == 'compid') {
          this.GetcomID(id)
        }
        if (this.state.type == 'InspectorId') {
          this.GetInspector(id)
        }
      }
    });
  }

  UpdateControl = () => {
    if (typeof this.props.navigation.getParam('QRid') != "undefined") {
      if (this.props.navigation.getParam('QRid') != "") {
        this.toast.show(Url.isLoadingView, 0)
      }
    }
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || ''
    const type = navigation.getParam('type') || '';

    if (type == 'compid') {
      this.GetcomID(QRid)
    } else if (type == 'InspectorId') {
      this.GetInspector(QRid)
    }


  }

  componentWillUnmount() {
    imageArr = [], imageFileArr = [], //照片数组
      checkimageArr = [], checkimageFileArr = [], //检查照片数组
      questionChickIdArr = [], questionChickArr = [], //子表问题数组
      isCheckSecondList = [], //是否展开二级
      isCheckedAllList = [] //二级菜单是否被全选
    this.iDataScan.remove()
  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  checkResData = (guid) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "ObtainQualityCheck",
      "servicetype": "pda",
      "express": "8AC4C0B0",
      "ciphertext": "7df83f712027c63f147f6c2d4a43f08d",
      "data": {
        "guid": guid,
        "factoryId": Url.PDAFid,
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: qualityinspection.js ~ line 189 ~ QualityInspection ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {

      console.log("🚀 ~ file: quality_product_inspection.js ~ line 192 ~ QualityProductInspection ~ resData", resData)

      let rowguid = resData.result.guid
      let ServerTime = resData.result.time
      let checkResult = resData.result.result
      let componentImage = resData.result.componentImage
      let reportImage = resData.result.reportImage
      let compCode = resData.result.compCode
      let compId = resData.result.compId
      let projectName = resData.result.projectName
      let compTypeName = resData.result.compTypeName
      let designType = resData.result.designType
      let floorName = resData.result.floorName
      let floorNoName = resData.result.floorNoName
      let volume = resData.result.volume
      let weight = resData.result.weight
      let Inspector = resData.result.InspectorName
      let record = resData.result.record
      let dictionary = resData.result.dictionary
      let remark = resData.result.remark
      let editEmployeeName = resData.result.editEmployeeName
      //图片数组
      let imageArr = [], imageFileArr = [];
      let checkimageArr = [], checkimageFileArr = [];

      componentImage.map((item, index) => {
        let tmp = {}
        tmp.fileid = ""
        tmp.uri = item.componentImageUrl
        tmp.url = item.componentImageUrl
        imageFileArr.push(tmp)
        imageArr.push(item.componentImageUrl)
      })
      this.setState({
        imageFileArr: imageFileArr
      })
      reportImage.map((item, index) => {
        let tmp = {}
        tmp.fileid = ""
        tmp.uri = item.qualityCheckImageUrl
        tmp.url = item.qualityCheckImageUrl
        checkimageFileArr.push(tmp)
        checkimageArr.push(item.qualityCheckImageUrl)
      })
      this.setState({
        checkimageFileArr: checkimageFileArr
      })
      let reviewDictionary = []
      if (checkResult == '不合格') {
        this.setState({ checked: false })
        dictionary.map((item, index) => {
          // 成品检复检查看成品检信息，展示二级菜单
          let check = false
          let reviewDictionarySecond = item
          let problemDefects = []
          item.problemDefects.map((defectitem, index) => {
            if (defectitem.ischecked == true) {
              check = true
              problemDefects.push(defectitem)
              isCheckSecondList.push(item.defectTypeId);
              questionChickArr.push(defectitem)
              questionChickIdArr.push(defectitem.rowguid)
            }
          })
          reviewDictionarySecond.problemDefects = problemDefects
          if (check) reviewDictionary.push(reviewDictionarySecond)
        })
      }
      //产品子表
      let compData = {}, result = {}
      result.projectName = projectName
      result.compTypeName = compTypeName
      result.designType = designType
      result.floorNoName = floorNoName
      result.floorName = floorName
      result.volume = volume
      result.weight = weight
      compData.result = result
      this.setState({
        resData: resData,
        rowguid: rowguid,
        ServerTime: ServerTime,
        compCode: compCode,
        compId: compId,
        compData: compData,
        Inspector: Inspector,
        InspectorSelect: Inspector,
        record: record,
        checkResult: checkResult,
        imageArr: imageArr,
        checkimageArr: checkimageArr,
        questionChickArr: questionChickArr,
        questionChickIdArr: questionChickIdArr,
        dictionary: this.state.isQualityProductInspectionReview ? reviewDictionary : dictionary,
        remark: remark,
        pictureSelect: true,
        checkpictureSelect: true,
        isGetcomID: true,
        isGetInspector: false,
        isCheckSecondList: isCheckSecondList
      })
      this.setState({
        isLoading: false,
      })
    }).catch((error) => {
      console.log("🚀 ~ file: quality_product_inspection.js ~ line 215 ~ quality_product_inspection ~ error", error)
    });
  }

  resData = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "gettimeandproblemdefect",
      "servicetype": "pda",
      "express": "6DFD1C9B",
      "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
      "data": { "factoryId": Url.PDAFid }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: steel_cage_storage.js ~ line 35 ~ SteelCage ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        let rowguid = resData.result.rowguid
        let ServerTime = resData.result.time
        let monitorSetup = resData.result.monitorSetup
        if (monitorSetup != '扫码') {
          this.setState({
            isGetInspector: true,
          })
        }
        let dictionary = resData.result.dictionary
        let inspectors = resData.result.inspectors
        let checkFormCode = ''
        if (typeof resData.result.checkFormCode != 'undefined') {
          checkFormCode = resData.result.checkFormCode
        }
        console.log("🚀 ~ file: quality_product_inspection.js ~ line 156 ~ SteelCage ~ resData", resData)
        this.setState({
          resData: resData,
          rowguid: rowguid,
          ServerTime: ServerTime,
          monitorSetup: monitorSetup,
          dictionary: dictionary,
          inspectors: inspectors,
          checkFormCode: checkFormCode,
          isLoading: false
        })
      } else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }

    }).catch((error) => {
      console.log("🚀 ~ file: steel_cage_storage.js ~ line 75 ~ SteelCage ~ error", error)
    });

  }

  DeleteData = () => {
    console.log('DeleteData')
    let formData = new FormData();
    let data = {};
    let problemIdStr1 = this.state.problemIdStr.toString() || ''
    data = {
      "action": "ReviseQualityCheck",
      "servicetype": "pda",
      "express": "06C795EE",
      "ciphertext": "a1e3818d57d9bfc9437c29e7a558e32e",
      "data": {
        "result": this.state.checkResult,
        "PDAuserid": Url.PDAEmployeeId,
        "problemDefects": this.state.questionChickArr,
        "factoryId": Url.PDAFid,
        "guid": this.state.rowguid,
        "time": this.state.ServerTime,
        "PDAuser": Url.PDAEmployeeName
      },
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: qualityinspection.js ~ line 953 ~ QualityInspection ~ DeleteData ~ formData", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('修改成功');
        this.props.navigation.navigate('MainCheckPage')
      } else {
        Alert.alert(
          '保存失败',
          resData.message,
          [{
            text: '取消',
            style: 'cancel'
          }, {
            text: '返回上一级',
            onPress: () => {
              this.props.navigation.goBack()
            }
          }])
        console.log('resData', resData)
        console.log('resData', resData)
      }
    }).catch((error) => {
    });

  }

  PostData = () => {
    const { InspectorId, compId, checkFormCode, ServerTime, checkResult, rowguid, record, questionChickIdArr, questionChickArr, remark } = this.state
    const { navigation } = this.props;
    const QRid = navigation.getParam('QRid') || '';

    if (compId == '') {
      this.toast.show('产品编号不能为空');
      return
    }
    if (InspectorId == '') {
      this.toast.show('质检员信息不能为空');
      return
    }
    if (this.state.isAppPhotoSetting) {
      if (imageArr.length == 0) {
        this.toast.show('请先拍摄构件照片');
        return
      }
    }
    if (this.state.isAppPhotoSetting1) {
      if (checkimageArr.length == 0) {
        this.toast.show('请先拍摄构件检查照片');
        return
      }
    }

    if (checkResult == '不合格') {
      if (imageArr.length == 0) {
        this.toast.show('请先拍摄构件照片');
        return
      } else if (checkimageArr.length == 0) {
        this.toast.show('请先拍摄构件检查照片');
        return
      } else if (questionChickArr.length == 0) {
        this.toast.show('请先选择问题缺陷');
        return
      }

    }

    this.toast.show(saveLoading, 0)

    let problemIdStr1 = questionChickIdArr.toString()

    let formData = new FormData();
    let data = {};
    data = {
      "action": "saveProductCheck",
      "servicetype": "pda",
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        "InspectorId": InspectorId,
        "compId": compId,
        "problemDefects": questionChickArr,
        "factoryId": Url.PDAFid,
        "productCheckTime": ServerTime,
        "operatorId": Url.PDAEmployeeId,
        "checkResult": checkResult,
        "operator": Url.PDAEmployeeName,
        "rowguid": rowguid,
        "record": record,
        "remark": remark,
        "checkFormCode": checkFormCode,
        "recid": this.state.recid,
        "recid2": this.state.checkrecid
      }
    }

    if (!Url.isAppNewUpload) {
      imageArr.map((item, index) => {
        formData.append('img_' + index, {
          uri: item,
          type: 'image/jpeg',
          name: 'img_' + index
        })
      })
      checkimageArr.map((item, index) => {
        formData.append('check_' + index, {
          uri: item,
          type: 'image/jpeg',
          name: 'check_' + index
        })
      })
    }

    console.log("🚀 ~ file: steel_cage_storage.js ~ line 191 ~ SteelCage ~ data", data)
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: steel_cage_storage.js ~ line 193 ~ SteelCage ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('保存成功');
        setTimeout(() => {
          this.props.navigation.replace(this.props.navigation.getParam("page"), {
            title: this.props.navigation.getParam("title"),
            pageType: this.props.navigation.getParam("pageType"),
            page: this.props.navigation.getParam("page"),
            isAppPhotoSetting: this.props.navigation.getParam("isAppPhotoSetting"),
            isAppPhotoSetting1: this.props.navigation.getParam("isAppPhotoSetting1"),
            isAppDateSetting: this.props.navigation.getParam("isAppDateSetting"),
            isAppPhotoAlbumSetting: this.props.navigation.getParam("isAppPhotoAlbumSetting"),
            isAppPhotoAlbumSetting1: this.props.navigation.getParam("isAppPhotoAlbumSetting1"),
          })
          //this.props.navigation.navigate('PDAMainStack')
        }, 400)
      } else {
        this.toast.show('保存失败')
        Alert.alert('保存失败', resData.message)
      }
      console.log("🚀 ~ file: steel_cage_storage.js ~ line 195 ~ SteelCage ~ resData", resData)
    }).catch((error) => {
      this.toast.show('保存失败')
      if (error.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('保存失败', "响应内容不是合法JSON格式")
        return
      }
      if (error.toString().indexOf('Network request faile') != -1) {
        Alert.alert('保存失败', "网络请求错误")
        return
      }
      Alert.alert('保存失败', error.toString())
      console.log("🚀 ~ file: steel_cage_storage.js ~ line 75 ~ SteelCage ~ error", error)
    });
  }

  GetcomID = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "GetCompInfoForProductCheck",
      "servicetype": "pda",
      "express": "4287BAEA",
      "ciphertext": "bbbaad34794eef7a726f6b3832a24924",
      "data": {
        "compId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: quality_product_inspection.js:600 ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: quality_product_inspection.js:610 ~ resData", resData)
      this.toast.close()
      if (resData.status == '100') {
        let compCode = resData.result.compCode
        let compId = resData.result.compId
        this.setState({
          compData: resData,
          compCode: compCode,
          compId: compId,
          isGetcomID: true,
          iscomIDdelete: false
        })
        this.setState({
          focusIndex: 3,
          type: "InspectorId"
        })
        setTimeout(() => { this.scoll.scrollToEnd() }, 300)
      } else {
        console.log('resData', resData)
        Alert.alert('错误', resData.message)
        this.setState({
          compId: ""
        })
        //this.input1.focus()
      }

    }).catch(err => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 210 ~ SteelCage ~ err", err)
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  comIDItem = (index) => {
    const { isGetcomID, buttondisable, compCode, compId } = this.state
    return (
      <View>

        {
          !isGetcomID ?
            <View style={{ flexDirection: 'row' }}>
              <View>
                <Input
                  ref={ref => { this.input1 = ref }}
                  containerStyle={styles.scan_input_container}
                  inputContainerStyle={styles.scan_inputContainerStyle}
                  inputStyle={[styles.scan_input]}
                  placeholder='请输入'
                  keyboardType='numeric'
                  value={compId}
                  onChangeText={(value) => {
                    value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
                    value = value.replace(/^\./g, ""); //验证第一个字符是数字
                    value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
                    //value = value.replace(/\b(0+)/gi, ""); //清楚开头的0
                    this.setState({
                      compId: value
                    })
                  }}
                  onFocus={() => {
                    this.setState({
                      focusIndex: 0,
                      type: 'compid',
                    })
                  }}
                  onSubmitEditing={() => {
                    let inputComId = this.state.compId
                    inputComId = inputComId.replace(/\b(0+)/gi, "")
                    console.log("🚀 ~ file: quality_product_inspection.js ~ line 614 ~ QualityProductInspection ~ inputComId", inputComId)
                    this.GetcomID(inputComId)
                  }}
                />
              </View>
              <ScanButton
                onPress={() => {
                  this.setState({
                    iscomIDdelete: false,
                    buttondisable: true,
                    focusIndex: index
                  })
                  setTimeout(() => {
                    this.setState({ GetError: false, buttondisable: false })
                    varGetError = false
                  }, 1500)
                  this.props.navigation.navigate('QRCode', {
                    type: 'compid',
                    page: 'QualityProductInspection'
                  })
                }}
                onLongPress={() => {
                  this.setState({
                    type: 'compid',
                    focusIndex: 0
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}
              />
            </View>
            :
            <TouchableCustom
              onPress={() => {
                console.log('onPress')
              }}
              onLongPress={() => {
                console.log('onLongPress')
                Alert.alert(
                  '提示信息',
                  '是否删除构件信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      this.setState({
                        compData: [],
                        compCode: '',
                        compId: '',
                        iscomIDdelete: true,
                        isGetcomID: false,
                      })
                    }
                  }])
              }} >
              <Text>{compCode}</Text>
            </TouchableCustom>
        }
      </View>

    )
  }

  GetInspector = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getInspector",
      "servicetype": "pda",
      "express": "CDAB7A2D",
      "ciphertext": "a590b603df9b5a4384c130c7c21befb7",
      "data": {
        "InspectorId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let InspectorId = resData.result.InspectorId
        let InspectorName = resData.result.InspectorName
        this.setState({
          InspectorId: InspectorId,
          InspectorName: InspectorName,
          isGetInspector: true,
          isInspectordelete: false
        }, () => {
          DeviceStorageData = {
            "isGetInspector": true,
            "InspectorId": this.state.InspectorId,
            "InspectorName": this.state.InspectorName,
            "InspectorSelect": this.state.InspectorName,
          }

          DeviceStorage.save('DeviceStorageDataQPI', JSON.stringify(DeviceStorageData))
        })
      } else {
        Alert.alert('ERROR', resData.message)
        varGetError = true
        this.setState({
          GetError: true
        })
      }

    }).catch(err => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 210 ~ SteelCage ~ err", err)
    })
  }

  inspectorItem = (index) => {
    const { isGetInspector, buttondisable, InspectorName, monitorSetup, InspectorSelect, inspectors } = this.state
    return (
      <View>
        {
          monitorSetup == '扫码' ?
            (
              !isGetInspector ?
                <View>
                  <ScanButton
                    onPress={() => {
                      this.setState({
                        isInspectordelete: false,
                        buttondisable: true,
                        focusIndex: index
                      })
                      setTimeout(() => {
                        this.setState({ GetError: false, buttondisable: false })
                        varGetError = false
                      }, 1500)
                      this.props.navigation.navigate('QRCode', {
                        type: 'InspectorId',
                        page: 'QualityProductInspection'
                      })
                    }}
                    onLongPress={() => {
                      this.setState({
                        type: 'InspectorId',
                        focusIndex: index
                      })
                      NativeModules.PDAScan.onScan();
                    }}
                    onPressOut={() => {
                      NativeModules.PDAScan.offScan();
                    }}
                  />
                </View>
                :
                <TouchableCustom
                  onPress={() => {
                    console.log('onPress')
                  }}
                  onLongPress={() => {
                    console.log('onLongPress')
                    Alert.alert(
                      '提示信息',
                      '是否删除质检员信息',
                      [{
                        text: '取消',
                        style: 'cancel'
                      }, {
                        text: '删除',
                        onPress: () => {
                          this.setState({
                            InspectorId: '',
                            InspectorName: '',
                            isGetInspector: false,
                            isInspectordelete: true
                          })
                        }
                      }])
                  }} >
                  <Text>{InspectorName}</Text>
                </TouchableCustom>
            ) :
            (
              <TouchableCustom onPress={() => { this.isBottomVisible(inspectors, index) }} >
                <IconDown text={InspectorSelect}></IconDown>
              </TouchableCustom>
            )
        }
      </View>
    )
  }

  _DatePicker = (index) => {
    const { ServerTime } = this.state
    return (
      <DatePicker
        customStyles={{
          dateInput: styles.dateInput,
          dateTouchBody: styles.dateTouchBody,
          dateText: this.state.isAppDateSetting ? styles.dateText : styles.dateDisabled,
        }}
        iconComponent={<Icon name='caretdown' color={this.state.isAppDateSetting ? '#419fff' : "#666"} iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
        }
        disabled={!this.state.isAppDateSetting}
        onOpenModal={() => { this.setState({ focusIndex: index }) }}
        showIcon={true}
        mode='datetime'
        date={ServerTime}
        format="YYYY-MM-DD HH:mm"
        onDateChange={(value) => {
          this.setState({
            ServerTime: value
          })
        }}
      />
    )
  }

  cameraFunc = () => {
    launchCamera(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: parseInt(Url.isResizePhoto),
        maxWidth: parseInt(Url.isResizePhoto),
        saveToPhotos: true,

      },
      (response) => {
        if (response.uri == undefined) {
          return
        }
        imageArr.push(response.uri)
        this.setState({
          ////pictureSelect: true,
          pictureUri: response.uri,
          imageArr: imageArr
        })
        if (Url.isAppNewUpload) {
          this.cameraPost(response.uri)
        } else {
          this.setState({
            pictureSelect: true,
          })
        }
        console.log(response)
      },
    )
  }

  camLibraryFunc = () => {
    launchImageLibrary(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: parseInt(Url.isResizePhoto),
        maxWidth: parseInt(Url.isResizePhoto),
      },
      (response) => {
        if (response.uri == undefined) {
          return
        }
        imageArr.push(response.uri)
        this.setState({
          ////pictureSelect: true,
          pictureUri: response.uri,
          imageArr: imageArr
        })
        if (Url.isAppNewUpload) {
          this.cameraPost(response.uri)
        } else {
          this.setState({
            pictureSelect: true,
          })
        }
        console.log(response)
      },
    )
  }

  camandlibFunc = () => {
    Alert.alert(
      '提示信息',
      '选择拍照方式',
      [{
        text: '取消',
        style: 'cancel'
      }, {
        text: '拍照',
        onPress: () => {
          this.cameraFunc()
        }
      }, {
        text: '相册',
        onPress: () => {
          this.camLibraryFunc()
        }
      }])
  }

  // 展示/隐藏 放大图片
  handleZoomPicture = (flag, index) => {
    this.setState({
      imageOverSize: false,
      currShowImgIndex: index || 0
    })
  }

  // 加载放大图片弹窗
  renderZoomPicture = () => {
    const { imageOverSize, currShowImgIndex, imageFileArr } = this.state
    return (
      <OverlayImgZoomPicker
        isShowImage={imageOverSize}
        currShowImgIndex={currShowImgIndex}
        zoomImages={imageFileArr}
        callBack={(flag) => this.handleZoomPicture(flag)}
      ></OverlayImgZoomPicker>
    )
  }

  // 展示/隐藏 放大图片
  checkHandleZoomPicture = (flag, index) => {
    this.setState({
      checkimageOverSize: false,
      checkCurrShowImgIndex: index || 0
    })
  }

  // 加载放大图片弹窗
  checkRenderZoomPicture = () => {
    const { checkimageOverSize, checkCurrShowImgIndex, checkimageFileArr } = this.state
    return (
      <OverlayImgZoomPicker
        isShowImage={checkimageOverSize}
        currShowImgIndex={checkCurrShowImgIndex}
        zoomImages={checkimageFileArr}
        callBack={(flag) => this.checkHandleZoomPicture(flag)}
      ></OverlayImgZoomPicker>
    )

  }

  cameraPost = (uri) => {
    const { recid } = this.state
    this.setState({
      isPhotoUpdate: true
    })
    this.toast.show('图片上传中', 2000)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "savephote",
      "servicetype": "photo_file",
      "express": "D2979AB2",
      "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
      "data": {
        "fileflag": fileflag,
        "username": Url.PDAusername,
        "recid": recid,
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    formData.append('img', {
      uri: uri,
      type: 'image/jpeg',
      name: 'img'
    })
    console.log("🚀 ~ file: concrete_pouring.js ~ line 672 ~ SteelCage ~ formData", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 690 ~ SteelCage ~ resData", resData)
      this.toast.close()
      if (resData.status == '100') {
        let fileid = resData.result.fileid;
        let recid = resData.result.recid;

        let tmp = {}
        tmp.fileid = fileid
        tmp.uri = uri
        tmp.url = uri
        imageFileArr.push(tmp)
        this.setState({
          fileid: fileid,
          recid: recid,
          pictureSelect: true,
          imageFileArr: imageFileArr
        })
        console.log("🚀 ~ file: concrete_pouring.js ~ line 704 ~ SteelCage ~ imageFileArr", imageFileArr)
        this.toast.show('图片上传成功');
        this.setState({
          isPhotoUpdate: false
        })
      } else {
        Alert.alert('图片上传失败', resData.message)
        this.toast.close(1)
      }
    }).catch((error) => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 692 ~ SteelCage ~ error", error)
    });
  }

  cameraDelete = (item) => {
    let i = imageArr.indexOf(item)

    if (Url.isAppNewUpload) {
      let formData = new FormData();
      let data = {};
      data = {
        "action": "deletephote",
        "servicetype": "photo_file",
        "express": "D2979AB2",
        "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
        "data": {
          "fileid": imageFileArr[i].fileid,
        }
      }
      formData.append('jsonParam', JSON.stringify(data))
      fetch(Url.PDAurl, {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        body: formData
      }).then(res => {
        console.log(res.statusText);
        console.log(res);
        return res.json();
      }).then(resData => {
        if (resData.status == '100') {
          if (i > -1) {
            imageArr.splice(i, 1)
            imageFileArr.splice(i, 1)
            this.setState({
              imageArr: imageArr,
              imageFileArr: imageFileArr
            }, () => {
              this.forceUpdate()
            })
          }
          if (imageArr.length == 0) {
            this.setState({
              pictureSelect: false
            })
          }
          this.toast.show('图片删除成功');
        } else {
          Alert.alert('图片删除失败', resData.message)
          this.toast.close(1)
        }
      }).catch((error) => {
        console.log("🚀 ~ file: concrete_pouring.js ~ line 761 ~ SteelCage ~ cameraDelete ~ error", error)
      });
    } else {
      if (i > -1) {
        imageArr.splice(i, 1)
        this.setState({
          imageArr: imageArr,
        }, () => {
          this.forceUpdate()
        })
      }
      if (imageArr.length == 0) {
        this.setState({
          pictureSelect: false
        })
      }
    }
  }

  checkCameraFunc = () => {
    launchCamera(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: parseInt(Url.isResizePhoto),
        maxWidth: parseInt(Url.isResizePhoto),
        saveToPhotos: true,

      },
      (response) => {
        if (response.uri == undefined) {
          return
        }
        checkimageArr.push(response.uri)
        this.setState({
          //checkpictureSelect: true,
          checkpictureUri: response.uri,
          checkimageArr: checkimageArr
        })
        if (Url.isAppNewUpload) {
          this.checkcameraPost(response.uri)
        } else {
          this.setState({
            checkpictureSelect: true,
          })
        }
        console.log(response)
      },
    )
  }

  checkCamLibraryFunc = () => {
    launchImageLibrary(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: parseInt(Url.isResizePhoto),
        maxWidth: parseInt(Url.isResizePhoto),
      },
      (response) => {
        if (response.uri == undefined) {
          return
        }
        checkimageArr.push(response.uri)
        this.setState({
          checkpictureSelect: true,
          checkpictureUri: response.uri,
          checkimageArr: checkimageArr
        })
        if (Url.isAppNewUpload) {
          this.checkcameraPost(response.uri)
        } else {
          this.setState({
            checkpictureSelect: true,
          })
        }
        console.log(response)
      },
    )
  }

  checkCamAndLibFunc = () => {
    Alert.alert(
      '提示信息',
      '选择拍照方式',
      [{
        text: '取消',
        style: 'cancel'
      }, {
        text: '拍照',
        onPress: () => {
          this.checkCameraFunc()
        }
      }, {
        text: '相册',
        onPress: () => {
          this.checkCamLibraryFunc()
        }
      }])
  }

  // 展示/隐藏 放大图片
  handleZoomPicture = (flag, index) => {
    this.setState({
      imageOverSize: false,
      currShowImgIndex: index || 0
    })
  }

  // 加载放大图片弹窗
  renderZoomPicture = () => {
    const { imageOverSize, currShowImgIndex, imageFileArr } = this.state
    return (
      <OverlayImgZoomPicker
        isShowImage={imageOverSize}
        currShowImgIndex={currShowImgIndex}
        zoomImages={imageFileArr}
        callBack={(flag) => this.handleZoomPicture(flag)}
      ></OverlayImgZoomPicker>
    )
  }

  checkcameraPost = (uri) => {
    const { checkrecid } = this.state
    this.setState({
      isPhotoUpdate: true
    })
    this.toast.show('图片上传中', 2000)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "savephote",
      "servicetype": "photo_file",
      "express": "D2979AB2",
      "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
      "data": {
        "fileflag": fileflag,
        "username": Url.PDAusername,
        "recid": checkrecid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    formData.append('img', {
      uri: uri,
      type: 'image/jpeg',
      name: 'img'
    })
    console.log("🚀 ~ file: concrete_pouring.js ~ line 672 ~ SteelCage ~ formData", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 690 ~ SteelCage ~ resData", resData)
      this.toast.close()
      if (resData.status == '100') {
        let fileid = resData.result.fileid;
        let recid = resData.result.recid;

        let tmp = {}
        tmp.fileid = fileid
        tmp.uri = uri
        tmp.url = uri
        checkimageFileArr.push(tmp)
        this.setState({
          checkfileid: fileid,
          checkrecid: recid,
          checkpictureSelect: true,
          checkimageFileArr: checkimageFileArr
        })
        console.log("🚀 ~ file: concrete_pouring.js ~ line 704 ~ SteelCage ~ imageFileArr", imageFileArr)
        this.toast.show('图片上传成功');
        this.setState({
          isPhotoUpdate: false
        })
      } else {
        Alert.alert('图片上传失败', resData.message)
        this.toast.close(1)
      }
    }).catch((error) => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 692 ~ SteelCage ~ error", error)
    });
  }

  checkcameraDelete = (item) => {
    let i = checkimageArr.indexOf(item)

    if (Url.isAppNewUpload) {
      let formData = new FormData();
      let data = {};
      data = {
        "action": "deletephote",
        "servicetype": "photo_file",
        "express": "D2979AB2",
        "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
        "data": {
          "fileid": checkimageFileArr[i].fileid,
        }
      }
      formData.append('jsonParam', JSON.stringify(data))
      console.log("🚀 ~ file: concrete_pouring.js ~ line 733 ~ SteelCage ~ cameraDelete ~ formData", formData)
      fetch(Url.PDAurl, {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        body: formData
      }).then(res => {
        console.log(res.statusText);
        console.log(res);
        return res.json();
      }).then(resData => {
        console.log("🚀 ~ file: concrete_pouring.js ~ line 745 ~ SteelCage ~ cameraDelete ~ resData", resData)
        if (resData.status == '100') {
          if (i > -1) {
            checkimageArr.splice(i, 1)
            checkimageFileArr.splice(i, 1)
            this.setState({
              checkimageArr: checkimageArr,
              checkimageFileArr: checkimageFileArr
            }, () => {
              this.forceUpdate()
            })
          }
          if (checkimageArr.length == 0) {
            this.setState({
              checkpictureSelect: false
            })
          }
          this.toast.show('图片删除成功');
        } else {
          Alert.alert('图片删除失败', resData.message)
          this.toast.close(1)
        }
      }).catch((error) => {
        console.log("🚀 ~ file: concrete_pouring.js ~ line 761 ~ SteelCage ~ cameraDelete ~ error", error)
      });
    }

  }

  imageView = () => {
    return (
      <View style={{ flexDirection: 'row' }}>
        {
          this.state.pictureSelect ?
            this.state.imageArr.map((item, index) => {
              //if (index == 0) {
              return (
                <AvatarImg
                  item={item}
                  index={index}
                  onPress={() => {
                    this.setState({
                      imageOverSize: true,
                      pictureUri: item
                    })
                  }}
                  onLongPress={() => {
                    Alert.alert(
                      '提示信息',
                      '是否删除构件照片',
                      [{
                        text: '取消',
                        style: 'cancel'
                      }, {
                        text: '删除',
                        onPress: () => {
                          this.cameraDelete(item)
                        }
                      }])
                  }} />
              )
            }) : <View></View>
        }
      </View>
    )
  }

  checkimgView = () => {
    return (
      <View style={{ flexDirection: 'row' }}>
        {
          this.state.checkpictureSelect ?
            this.state.checkimageArr.map((item, index) => {
              return (
                <AvatarImg
                  item={item}
                  index={index}
                  onPress={() => {
                    this.setState({
                      checkimageOverSize: true,
                      checkpictureUri: item
                    })
                  }}
                  onLongPress={() => {
                    Alert.alert(
                      '提示信息',
                      '是否删除构件照片',
                      [{
                        text: '取消',
                        style: 'cancel'
                      }, {
                        text: '删除',
                        onPress: () => {
                          this.checkcameraDelete(item)
                        }
                      }])
                  }}
                />
              )
            }) : <View></View>
        }
      </View>
    )
  }

  checkRefuseItem_old = (index) => {
    const { focusIndex, checkResult } = this.state
    console.log("🚀 ~ file: quality_product_inspection.js ~ line 614 ~ SteelCage ~ this.state.checkResult", this.state.checkResult)
    if (checkResult == '不合格') {
      return (
        <View>
          <ListItem
            containerStyle={focusIndex == index ? { backgroundColor: '#A9E2F3' } : {}}
            title='检查记录'
            rightElement={
              <Input
                containerStyle={styles.quality_input_container}
                inputContainerStyle={styles.inputContainerStyle}
                inputStyle={styles.quality_input_}
                placeholder='请输入'
                value={this.state.record}
                //onFocus={() => { this.setState({ focusIndex: index }) }}
                onChangeText={(value) => {
                  this.setState({
                    record: value
                  })
                }} />
            }
            bottomDivider
          />
          <ListItem
            containerStyle={focusIndex == (index + 1) ? { backgroundColor: '#A9E2F3' } : {}}
            title='问题缺陷/改进措施'
            rightElement={
              <TouchableCustom onPress={() => { this.setState({ isQuestionOverlay: true, focusIndex: (index + 1) }) }} >
                <View style={{ flexDirection: "row" }}>
                  <Text >请选择</Text>
                  <Icon name='downsquare' color='#419fff' iconStyle={{ fontSize: 17, marginLeft: 2 }} type='antdesign' ></Icon>
                </View>
              </TouchableCustom>
            } />
          {
            this.state.questionChickArr.length != 0 ?
              <View style={{ width: width * 0.8, marginLeft: width * 0.1, backgroundColor: 'lightgray' }}>
                {
                  this.state.questionChickArr.map((item, index) => {
                    return (
                      <View style={styles.problemList_container_style} >
                        <Text style={styles.ProblemTitle} numberOfLines={1}>{item.problemDefect}</Text>
                        <Text style={styles.rightProblemTitle} numberOfLines={1}>{item.measure}</Text>
                      </View>
                    )
                  })
                }
              </View> : <View></View>
          }
        </View>

      )
    }
  }

  checkRefuseItem = (index) => {
    const { focusIndex, checkResult } = this.state
    console.log("🚀 ~ file: quality_product_inspection.js ~ line 614 ~ SteelCage ~ this.state.checkResult", this.state.checkResult)
    if (checkResult == '不合格') {
      return (
        <View >
          <ListItemScan
            focusColor={focusIndex == index ? styles.focusColor : {}}
            title='检查记录'
            rightElement={
              this.state.isQualityProductInspectionReview ? this.state.record :
                <Input
                  containerStyle={styles.quality_input_container}
                  inputContainerStyle={styles.inputContainerStyle}
                  inputStyle={styles.quality_input_}
                  placeholder='请输入'
                  value={this.state.record}
                  //onFocus={() => { this.setState({ focusIndex: index }) }}
                  onChangeText={(value) => {
                    this.setState({
                      record: value
                    })
                  }} />
            }
            bottomDivider
          />
          <ListItemScan
            focusColor={focusIndex == (index + 1) ? styles.focusColor : {}}
            title='问题缺陷/改进措施'
            rightElement={
              this.state.isQualityProductInspectionReview ? <View /> :
                <TouchableCustom onPress={() => { this.setState({ isQuestionOverlay: false, focusIndex: (index + 1) }) }} >
                  <IconDown text={'请选择'} />
                </TouchableCustom>
            } />
          <View style={{ marginHorizontal: width * 0.05, backgroundColor: "#f8f8f8", borderRadius: 5, marginTop: 10 }}>
            <FlatList
              data={this.state.dictionary}
              renderItem={this._renderItem.bind(this)}
              extraData={this.state} />
          </View>

        </View>

      )
    }
  }

  isBottomVisible = (data, focusIndex) => {
    if (typeof (data) != "undefined") {
      if (data.length > 0) {
        this.setState({ bottomVisible: true, bottomData: data, focusIndex: focusIndex })
      }
    } else {
      this.toast.show("无数据")
    }
  }

  func = (state) => {
    console.log("🚀 ~ file: qualityinspection.js ~ line 1166 ~ QualityInspection ~ state", state)
    this.setState({
      isPaperTypeVisible: true
    })
  }

  paperResData = (type) => {
    let formData = new FormData();
    let data = {}
    data = {
      "action": "getDrawings",
      "servicetype": "pda",
      "express": "FCEF95AF",
      "ciphertext": "985e3620ae26a2f380c723b60ce9b525",
      "data": {
        "type": type,
        "compId": this.state.compId,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 314 ~ QualityInspection ~ resData", resData)
      if (resData.status == "100") {

        let paperUrl = resData.result.Url
        let FileName = resData.result.FileName

        this.setState({
          paperUrl: paperUrl,
          FileName: FileName,
          isPDFVisible: true,
          isPaperTypeVisible: false
        })
      } else {
        Alert.alert('错误', resData.message)
        this.setState({ isPaperTypeVisible: false })
      }
    }).catch((error) => {
    });
  }

  render() {
    const { navigation } = this.props;
    let pageType = navigation.getParam('pageType') || ''
    const { resData, focusIndex, compData, compId, compCode, InspectorId, InspectorName, bottomVisible, bottomData, isGetcomID, remark, isPaperTypeVisible, isPDFVisible, paperList, paperUrl, FileName, ServerTime, isQualityProductInspectionReview } = this.state

    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          <NavigationEvents
            onWillFocus={this.UpdateControl} />
          <ScrollView ref={ref => this.scoll = ref} style={styles.mainView} showsVerticalScrollIndicator={false} >
            <View style={styles.listView}>
              <ListItem
                containerStyle={{ paddingBottom: 6 }}
                leftElement={
                  <View style={{ width: width / 2.7 }}>
                    <View >
                      {
                        this.state.isCheckPage ? <View></View> :
                          <AvatarAdd
                            pictureSelect={this.state.pictureSelect}
                            backgroundColor='#4D8EF5'
                            color='white'
                            title="构"
                            onPress={() => {
                              if (this.state.isAppPhotoAlbumSetting) {
                                if (pageType == 'CheckPage') {
                                  this.toast.show('构件照片不可修改')
                                } else {
                                  this.camandlibFunc()
                                }
                              } else {
                                this.cameraFunc()
                              }
                            }} />
                      }
                      {this.imageView()}
                    </View>
                  </View>
                }
                rightElement={
                  <View >
                    <View>
                      {
                        this.state.isCheckPage ? <View></View> :
                          <AvatarAdd
                            pictureSelect={this.state.checkpictureSelect}
                            backgroundColor='#17BC29'
                            color='white'
                            title="检"
                            onPress={() => {
                              if (this.state.isAppPhotoAlbumSetting1) {
                                if (pageType == 'CheckPage') {
                                  this.toast.show('检查照片不可修改')
                                } else {
                                  this.checkCamAndLibFunc()
                                }
                              }
                              else {
                                this.checkCameraFunc()
                              }

                            }} />
                      }
                      {this.checkimgView()}
                    </View>
                  </View>
                }
              />

              <ListItemScan
                isButton={!isGetcomID}
                focusStyle={focusIndex == '0' ? styles.focusColor : {}}
                onPressIn={() => {
                  this.setState({
                    type: 'compid',
                    focusIndex: 0
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPress={() => {
                  this.setState({
                    type: 'compid',
                    focusIndex: 0
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}
                title='产品编号'
                rightElement={this.comIDItem(0)}
              />
              {
                isGetcomID ? <CardList compData={compData} func={this.func.bind(this)} /> : <View></View>
              }
              <ListItemScan
                focusStyle={focusIndex == '1' ? styles.focusColor : {}}
                title='成检日期'
                rightElement={isQualityProductInspectionReview ? ServerTime : this._DatePicker(1)}
              />
              <ListItemScan
                title='操作人'
                rightTitle={Url.PDAEmployeeName}
              />
              <ListItemScan
                isButton={true}
                focusStyle={focusIndex == '2' ? styles.focusColor : {}}
                title='检查结果'
                rightElement={
                  isQualityProductInspectionReview ? this.state.checkResult :
                    <View style={{ flexDirection: "row", marginRight: -23, paddingVertical: 0 }}>
                      <CheckBoxScan
                        title='合格'
                        checked={this.state.checked}
                        onPress={() => {
                          if (pageType != 'CheckPage') {
                            this.setState({
                              checked: true,
                              checkResult: '合格',
                              focusIndex: 2
                            })
                          }

                        }}
                      />
                      <CheckBoxScan
                        title='不合格'
                        checked={!this.state.checked}
                        onPress={() => {
                          if (pageType != 'CheckPage') {
                            this.setState({
                              checked: false,
                              checkResult: '不合格',
                              focusIndex: 2
                            })
                          }
                        }}
                      />
                    </View>
                }
              />
              {this.checkRefuseItem(4)}
              <TouchableCustom underlayColor={'lightgray'} onPress={() => {
                if (this.state.monitorSetup != '扫码') {
                  this.isBottomVisible(this.state.inspectors, 3)
                } else {
                  this.setState({
                    type: 'InspectorId',
                    focusIndex: 3
                  })
                  NativeModules.PDAScan.onScan();
                }
              }}
                onPressIn={() => {
                  if (this.state.monitorSetup == '扫码') {
                    this.setState({
                      type: 'InspectorId',
                      focusIndex: 3
                    })
                    NativeModules.PDAScan.onScan();
                  }
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}>
                <ListItemScan
                  isButton={this.state.monitorSetup == '扫码' && !this.state.isGetInspector}
                  focusStyle={focusIndex == 3 ? styles.focusColor : {}}
                  title='质检确认'
                  rightElement={isQualityProductInspectionReview ? this.state.InspectorSelect : this.inspectorItem(3)}
                />
              </TouchableCustom>

              <ListItemScan
                focusStyle={focusIndex == '8' ? styles.focusColor : {}}
                title='备注'
                rightElement={
                  isQualityProductInspectionReview ? remark :
                    <View>
                      <Input
                        containerStyle={styles.quality_input_container}
                        inputContainerStyle={styles.inputContainerStyle}
                        inputStyle={[styles.quality_input_, { top: 7 }]}
                        placeholder='请输入'
                        value={remark}
                        onChangeText={(value) => {
                          this.setState({
                            remark: value
                          })
                        }} />
                    </View>
                }
              />

            </View>

            {
              isQualityProductInspectionReview ? <View /> :
                pageType == 'CheckPage' ? <FormButton
                  backgroundColor='#EB5D20'
                  title='修改'
                  onPress={() => {
                    this.DeleteData()
                  }}
                /> :
                  <FormButton
                    title='保存'
                    onPress={() => {
                      this.PostData()
                    }}
                    backgroundColor='#17BC29'
                    disabled={this.state.isPhotoUpdate}
                  />
            }



            {/* 照片 */}
            <BottomSheet
              isVisible={this.state.CameraVisible}
              onRequestClose={() => {
                this.setState({ CameraVisible: false })
              }}
            >
              <TouchableCustom
                onPress={() => {
                  launchCamera(
                    {
                      mediaType: 'photo',
                      includeBase64: false,
                      //maxHeight: 600,
                      //maxWidth: 600,
                      saveToPhotos: true
                    },
                    (response) => {
                      if (response.uri == undefined) {
                        return
                      }
                      imageArr.push(response.uri)
                      this.setState({
                        pictureSelect: true,
                        pictureUri: response.uri,
                        imageArr: imageArr
                      })
                      console.log(response)
                    },
                  )
                  this.setState({ CameraVisible: false })
                }} >
                <ListItem
                  title='拍照' >
                </ListItem>
              </TouchableCustom>
              <TouchableCustom
                onPress={() => {
                  launchImageLibrary(
                    {
                      mediaType: 'photo',
                      includeBase64: false,
                      maxHeight: 800,
                      maxWidth: 800,

                    },
                    (response) => {
                      if (response.uri == undefined) {
                        return
                      }
                      imageArr.push(response.uri)
                      this.setState({
                        pictureSelect: true,
                        pictureUri: response.uri,
                        imageArr: imageArr
                      })
                      console.log(response)
                    },
                  )
                  this.setState({ CameraVisible: false })
                }} >
                <ListItem
                  title='相册' >
                </ListItem>
              </TouchableCustom>
              <ListItem
                title='关闭'
                containerStyle={{ backgroundColor: 'red' }}
                onPress={() => {
                  this.setState({ CameraVisible: false })
                }} >
              </ListItem>
            </BottomSheet>

            {/* check照片 */}
            <BottomSheet
              isVisible={this.state.checkCameraVisible}
              onRequestClose={() => {
                this.setState({ checkCameraVisible: false })
              }}
            >
              <TouchableCustom
                onPress={() => {
                  launchCamera(
                    {
                      mediaType: 'photo',
                      includeBase64: false,
                      //maxHeight: 600,
                      //maxWidth: 600,
                      saveToPhotos: true
                    },
                    (response) => {
                      if (response.uri == undefined) {
                        return
                      }
                      checkimageArr.push(response.uri)
                      this.setState({
                        checkpictureSelect: true,
                        checkpictureUri: response.uri,
                        checkimageArr: checkimageArr
                      })
                      console.log(response)
                    },
                  )
                  this.setState({ checkCameraVisible: false })
                }} >
                <ListItem
                  title='拍照' >
                </ListItem>
              </TouchableCustom>
              <TouchableCustom
                onPress={() => {
                  launchImageLibrary(
                    {
                      mediaType: 'photo',
                      includeBase64: false,
                      maxHeight: 600,
                      maxWidth: 600,

                    },
                    (response) => {
                      if (response.uri == undefined) {
                        return
                      }
                      checkimageArr.push(response.uri)
                      this.setState({
                        checkpictureSelect: true,
                        checkpictureUri: response.uri,
                        checkimageArr: checkimageArr
                      })
                      console.log(response)
                    },
                  )
                  this.setState({ checkCameraVisible: false })
                }} >
                <ListItem
                  title='相册' >
                </ListItem>
              </TouchableCustom>
              <ListItem
                title='关闭'
                containerStyle={{ backgroundColor: 'red' }}
                onPress={() => {
                  this.setState({ checkCameraVisible: false })
                }} >
              </ListItem>
            </BottomSheet>

            {this.renderZoomPicture()}

            {this.checkRenderZoomPicture()}

            {/* {overlayImg(obj = {
              onRequestClose: () => {
                this.setState({ imageOverSize: !this.state.imageOverSize }, () => {
                  console.log("🚀 ~ file: equipment_maintenance.js:1696 ~ render ~ imageOverSize:", this.state.imageOverSize)
                })
              },
              onPress: () => {
                this.setState({
                  imageOverSize: !this.state.imageOverSize
                })
              },
              uri: this.state.pictureUri,
              isVisible: this.state.imageOverSize
            })
            }*/}

            {/* {overlayImg(obj = {
              onRequestClose: () => {
                this.setState({ checkimageOverSize: !this.state.checkimageOverSize }, () => {
                  console.log("🚀 ~ file: equipment_maintenance.js:1696 ~ render ~ imageOverSize:", this.state.imageOverSize)
                })
              },
              onPress: () => {
                this.setState({
                  checkimageOverSize: !this.state.checkimageOverSize
                })
              },
              uri: this.state.checkpictureUri,
              isVisible: this.state.checkimageOverSize
            })
            } */}

            <Overlay
              fullScreen={true}
              animationType='fade'
              isVisible={this.state.isPDFVisible}
              onRequestClose={() => {
                this.setState({ isPDFVisible: !this.state.isPDFVisible });
              }}
            >
              <Header
                containerStyle={{ height: 45, paddingTop: 0, top: -5 }}
                centerComponent={{ text: '图纸预览', style: { color: 'gray', fontSize: 20 } }}
                rightComponent={<BackIcon
                  onPress={() => {
                    this.setState({
                      isPDFVisible: !this.state.isPDFVisible
                    });
                  }} />}
                backgroundColor='white'
              />
              <View style={{ flex: 1 }}>
                <Pdf
                  source={{
                    uri: paperUrl,
                    //method: 'GET', //默认 'GET'，请求 url 的方式
                  }}
                  fitWidth={true} //默认 false，若为 true 则不能将 fitWidth = true 与 scale 一起使用
                  fitPolicy={0} // 0:宽度对齐，1：高度对齐，2：适合两者（默认）
                  page={1}
                  //scale={1}
                  onLoadComplete={(numberOfPages, filePath, width, height, tableContents) => {
                    console.log(`number of pages: ${numberOfPages}`); //总页数
                    console.log(`number of filePath: ${filePath}`); //本地返回的路径
                    console.log(`number of width: `, JSON.stringify(width));
                    console.log(`number of height: ${JSON.stringify(height)}`);
                    console.log(`number of tableContents: ${tableContents}`);
                  }}
                  onError={(error) => {
                    console.log(error);
                  }}
                  minScale={1} //最小模块
                  maxScale={3}
                  enablePaging={true} //在屏幕上只能显示一页
                  style={{
                    flex: 1,
                    width: width
                  }}
                />
              </View>

            </Overlay>

            <BottomSheet
              isVisible={bottomVisible && !this.state.isCheckPage}
              onRequestClose={() => {
                this.setState({
                  bottomVisible: false
                })
              }}
              onBackdropPress={() => {
                this.setState({
                  bottomVisible: false
                })
              }}
            >
              <ScrollView style={styles.bottomsheetScroll}>
                {
                  bottomData.map((item, index) => {
                    let title = ''
                    if (item.rowguid) {
                      title = item.name + ' ' + item.teamName
                    } else {
                      title = item.InspectorName
                    }
                    return (
                      <TouchableCustom
                        onPress={() => {
                          if (item.rowguid) {
                            this.setState({
                              monitorId: item.rowguid,
                              monitorSelect: item.name,
                              monitorName: item.name,
                              monitorTeamName: item.teamName,
                            })
                          } else if (item.InspectorId) {
                            this.setState({
                              InspectorId: item.InspectorId,
                              InspectorName: item.InspectorName,
                              InspectorSelect: item.InspectorName,
                            }, () => {
                              DeviceStorageData = {
                                "InspectorId": this.state.InspectorId,
                                "InspectorName": this.state.InspectorName,
                                "InspectorSelect": this.state.InspectorName,
                              }

                              DeviceStorage.save('DeviceStorageDataQPI', JSON.stringify(DeviceStorageData))

                            })
                          }
                          this.setState({
                            bottomVisible: false
                          })
                        }}
                      >
                        <BottomItem backgroundColor='white' color="#333" title={title} />
                      </TouchableCustom>
                    )

                  })
                }
              </ScrollView>
              <TouchableHighlight
                onPress={() => {
                  this.setState({ bottomVisible: false })
                }}>
                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
              </TouchableHighlight>
            </BottomSheet>


          </ScrollView>

        </View>
      )
    }
  }

  // 展示/隐藏 放大图片
  handleZoomPicture = (flag, index) => {
    this.setState({
      isShowImage: flag,
      currShowImgIndex: index || 0
    })
  }

  // 加载放大图片弹窗
  renderZoomPicture() {
    let { imageOverSize, currShowImgIndex } = this.state
    // 测试
    let zoomImages = [{
      url: 'https://tse2-mm.cn.bing.net/th/id/OIP.yPEBi69raqV5QW3aTmEQsgHaFj?pid=Api&rs=1',
      props: {
        // headers: ...
      }
    }, {
      url: 'https://img.locationscout.net/images/2017-01/alpe-di-siusi-italy_l.jpeg',
    }, {
      url: 'https://tse2-mm.cn.bing.net/th/id/OIP.yPEBi69raqV5QW3aTmEQsgHaFj?pid=Api&rs=1'
    }]


    return (
      <OverlayImgZoomPicker
        isShowImage={isShowImage}
        currShowImgIndex={currShowImgIndex}
        zoomImages={zoomImages}
        callBack={(flag) => {
          console.log("🚀 ~ file: quality_product_inspection.js:2203 ~ renderZoomPicture ~ flag:", flag)
          this.handleZoomPicture(flag)
        }}
      ></OverlayImgZoomPicker>
    )

  }


  checkListSecondisVisible = (item) => {
    if (isCheckSecondList.indexOf(item.defectTypeId) == -1) {
      isCheckSecondList.push(item.defectTypeId);
      this.setState({ isCheckSecondList: isCheckSecondList });
    } else {
      if (isCheckSecondList.length == 1) {
        isCheckSecondList.splice(0, 1);
        this.setState({ isCheckSecondList: isCheckSecondList }, () => {
          this.forceUpdate;
        });
      } else {
        isCheckSecondList.map((item1, index) => {
          if (item1 == item.defectTypeId) {
            isCheckSecondList.splice(index, 1);
            this.setState({ isCheckSecondList: isCheckSecondList }, () => {
              this.forceUpdate;
            });
          }
        });
      }
    }
  }

  //主列全选功能
  allMainCheck = (item) => {
    //主列全选
    if (isCheckedAllList.indexOf(item.defectTypeId) == -1) {
      //遍历子列，全部勾选
      item.problemDefects.map((defectitem, defectindex) => {
        if (questionChickIdArr.indexOf(defectitem.rowguid) == -1) {
          questionChickArr.push(defectitem)
          questionChickIdArr.push(defectitem.rowguid)
          this.setState({
            questionChickArr: questionChickArr,
            questionChickIdArr: questionChickIdArr,
          })
        }
      })
      //子列全部勾选就勾选上主列
      isCheckedAllList.push(item.defectTypeId)
      this.setState({ isCheckedAllList: isCheckedAllList })
    }
    //主列取消全选
    else {
      //遍历子列，依次取消勾选
      item.problemDefects.map((defectitem, defectindex) => {
        if (questionChickIdArr.indexOf(defectitem.rowguid) != -1) {
          let num = questionChickIdArr.indexOf(defectitem.rowguid)
          questionChickIdArr.splice(num, 1)
          questionChickArr.splice(num, 1)
          this.setState({
            questionChickArr: questionChickArr,
            questionChickIdArr: questionChickIdArr,
          })
        }
      })
      //把主列数据取消勾选
      let frinum = isCheckedAllList.indexOf(item.defectTypeId)
      isCheckedAllList.splice(frinum, 1)
      this.setState({ isCheckedAllList: isCheckedAllList })
    }
  }

  sonCheck = (item, defectitem) => {
    //点击勾选☑️
    if (questionChickIdArr.indexOf(defectitem.rowguid) == -1) {
      //数组中添加勾选的子列数据
      questionChickArr.push(defectitem)
      questionChickIdArr.push(defectitem.rowguid)
      this.setState({
        questionChickArr: questionChickArr,
        questionChickIdArr: questionChickIdArr,
      })
      //记录子列是否全部勾选，用数量对比
      let count = 0
      item.problemDefects.map((judgeitem) => {
        if (questionChickIdArr.indexOf(judgeitem.rowguid) != -1) {
          count += 1
        }
      })
      //子列全部勾选，就勾选主列
      if (count == item.problemDefects.length) {
        isCheckedAllList.push(item.defectTypeId)
        this.setState({ isCheckedAllList: isCheckedAllList })
      }

    }
    //反勾选 
    else {
      //查出取消勾选项的角标
      let num = questionChickIdArr.indexOf(defectitem.rowguid)
      //删掉！
      questionChickIdArr.splice(num, 1)
      questionChickArr.splice(num, 1)
      this.setState({
        questionChickArr: questionChickArr,
        questionChickIdArr: questionChickIdArr
      })
    }
    //如果子列取消全部勾选，主列取消勾选
    if (questionChickIdArr.indexOf(defectitem.rowguid) == -1) {
      let frinum = isCheckedAllList.indexOf(item.defectTypeId)
      isCheckedAllList.splice(frinum, 1)
      this.setState({ isCheckedAllList: isCheckedAllList })
    }
  }

  _renderItem = ({ item, index }) => {
    const { isCheckSecondListVisible, isCheckSecondList } = this.state
    return (
      <View style={{}}>
        <ListItem
          title={
            <View style={{ flexDirection: 'row' }}>
              {/* <Icon name={isCheckSecondList.indexOf(item.defectTypeId) != -1 ? 'downcircle' : 'rightcircle'} size={18} type='antdesign' color='#419FFF' /> */}
              <Text style={{ fontSize: 14, marginLeft: 4 }}>{item.defectTypeName}</Text>
            </View>
          }
          underlayColor={'lightgray'}
          containerStyle={{ marginVertical: 0, paddingVertical: 10, borderColor: 'transparent', borderWidth: 0, backgroundColor: "transparent" }}
          onPress={() => {
            this.checkListSecondisVisible(item)
          }}
          rightElement={
            this.state.isQualityProductInspectionReview ? <View /> :
              <CheckBoxScan
                key={index}
                //checkedColor='red'
                checked={this.state.isCheckedAllList.indexOf(item.defectTypeId) != -1}
                onPress={() => {
                  this.allMainCheck(item)
                }
                } />
          } />
        {
          isCheckSecondList.indexOf(item.defectTypeId) != -1 ?
            (
              <View>
                {item.problemDefects.map((defectitem, defectindex) => {
                  return (
                    <View>
                      <ListItem
                        title={defectitem.defect + ": " + defectitem.measure}
                        titleStyle={{ fontSize: 13 }}
                        containerStyle={{ marginVertical: 0, paddingVertical: 10, borderRadius: 0, borderColor: 'lightgray', borderWidth: 0, backgroundColor: "rgba(77,142,245,0.10)" }}
                        onPress={() => {
                          this.sonCheck(item, defectitem)
                        }
                        }
                        underlayColor='lightgray'
                        rightElement={
                          this.state.isQualityProductInspectionReview ? <View /> :
                            <CheckBoxScan
                              key={index}
                              title=''
                              //checkedColor='red'
                              checked={this.state.questionChickIdArr.indexOf(defectitem.rowguid) != -1}
                            />
                        } />
                    </View>

                  )
                })}
                <View style={{ height: 5, backgroundColor: "rgba(77,142,245,0.10)", borderBottomLeftRadius: 10, borderBottomRightRadius: 10 }}></View>
              </View>
            ) : <View></View>
        }

      </View>
    )
  }

}
