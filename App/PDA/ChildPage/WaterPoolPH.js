import React, { Component } from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, Icon, Card, ListItem, BottomSheet, CheckBox, Avatar, Overlay, Header, Input } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { RFT } from '../../Url/Pixal';
import { saveLoading } from '../../Url/Pixal';
import { ScrollView } from 'react-native-gesture-handler';
import CardList from "../Componment/CardList";
import DatePicker from 'react-native-datepicker'
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { Image } from 'react-native';
import { NavigationEvents } from 'react-navigation';
import ScanButton from '../Componment/ScanButton';
import { styles } from '../Componment/PDAStyles';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import ListItemScan_child from '../Componment/ListItemScan_child';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import TouchableCustom from '../Componment/TouchableCustom';
import CheckBoxScan from '../Componment/CheckBoxScan';
import AvatarAdd from '../Componment/AvatarAdd';
import AvatarImg from '../Componment/AvatarImg';
import CheckBoxchild from '../Componment/CheckBoxchild';
import DeviceStorage from '../../Url/DeviceStorage';

const { height, width } = Dimensions.get('window') //获取宽高


export default class WaterPoolPH extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      rowguid: '',
      ServerTime: '2000-01-01 00:00',
      recordDate: '',
      poolList: [],
      formcode: '',
      poolid: '',
      poolname: '',
      poolselect: '',
      isLoading: true,
      isGetPoolID: false,
      morningTemperature: '',
      afternoonTemperature: '',
      eveningTemperature: '',
      morningPH: '',
      afternoonPH: '',
      eveningPH: '',
      editEmployeeName: Url.PDAEmployeeName,
      //底栏控制
      bottomData: [],
      bottomVisible: false,
      focusIndex: 1,
      isCheckPage: false,
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    let guid = navigation.getParam('guid') || ''
    let pageType = navigation.getParam('pageType') || ''
    if (pageType == 'CheckPage') {
      this.checkResData(guid)
      this.setState({ isCheckPage: true })
    } else {
      this.resData()
      //this.GetcomID()
    }
  }

  componentWillUnmount() {

  }

  UpdateControl = () => {
    if (typeof this.props.navigation.getParam('QRid') != "undefined") {
      if (this.props.navigation.getParam('QRid') != "") {
        this.toast.show(Url.isLoadingView, 0)
      }
    }
    const { navigation } = this.props;
    const QRid = navigation.getParam('QRid') || ''
    let type = navigation.getParam('type') || '';
    if (type == 'compid') {
      this.GetcomID(QRid)
    }
  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  DateToStr = (date) => {
    var year = date.getFullYear();//年
    var month = date.getMonth();//月
    var day = date.getDate();//日
    var hours = date.getHours();//时
    var min = date.getMinutes();//分
    var second = date.getSeconds();//秒
    return year + "-" +
      ((month + 1) > 9 ? (month + 1) : "0" + (month + 1)) + "-" +
      (day > 9 ? day : ("0" + day)) + " " +
      (hours > 9 ? hours : ("0" + hours)) + ":" +
      (min > 9 ? min : ("0" + min)) + ":" +
      (second > 9 ? second : ("0" + second));
  }

  checkResData = (guid) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getwaterpoolphinfo",
      "servicetype": "pda",
      "express": "8AC4C0B0",
      "ciphertext": "7df83f712027c63f147f6c2d4a43f08d",
      "data": {
        "_rowguid": guid,
        //"guid": guid,
        "factoryId": Url.PDAFid,
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: WaterPoolPH.js:110 ~ WaterPoolPH ~ formData:", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: WaterPoolPH.js:123 ~ WaterPoolPH ~ resData:", resData)

      let rowguid = resData.result._rowguid
      let ServerTime = resData.result.editDate
      let recordDate = resData.result.recordDate
      let poolname = resData.result.poolName
      let poolid = resData.result.poolId
      let remaincapacity = resData.result.remaincapacity
      let formCode = resData.result.formCode
      let morningTemperature = resData.result.morningTemperature
      let afternoonTemperature = resData.result.afternoonTemperature
      let eveningTemperature = resData.result.eveningTemperature
      let morningPH = resData.result.morningPH
      let afternoonPH = resData.result.afternoonPH
      let eveningPH = resData.result.eveningPH
      let editEmployeeName = resData.result.editEmployeeName

      this.setState({
        resData: resData,
        rowguid: rowguid,
        ServerTime: ServerTime,
        recordDate: recordDate,
        poolname: poolname,
        poolselect: poolname,
        poolid: poolid,
        remaincapacity: remaincapacity,
        formcode: formCode,
        morningTemperature: morningTemperature,
        afternoonTemperature: afternoonTemperature,
        eveningTemperature: eveningTemperature,
        morningPH: morningPH,
        afternoonPH: afternoonPH,
        eveningPH: eveningPH,
        editEmployeeName: editEmployeeName,
        isGetPoolID: true,
      })
      this.setState({
        isLoading: false,
      })
    }).catch((error) => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 215 ~ QualityInspection ~ error", error)
    });
  }

  resData = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "initwaterpoollist",
      "servicetype": "pda",
      "express": "8AC4C0B0",
      "ciphertext": "7df83f712027c63f147f6c2d4a43f08d",
      "data": {
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      //初始化错误提示
      if (resData.status == 100) {
        let ServerTime = resData.result.dateTime
        let recordDate = resData.result.dateTime
        let rowguid = resData.result._rowguid
        let poolList = resData.result.poolList
        this.setState({
          resData: resData,
          rowguid: rowguid,
          ServerTime: ServerTime,
          recordDate: recordDate,
          poolList: poolList,
          isLoading: false,
        })
      }
      else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }

    }).catch((error) => {
      Alert.alert("错误提示", '服务器查询错误', [{
        text: '取消',
        style: 'cancel'
      }, {
        text: '返回上一级',
        onPress: () => {
          this.props.navigation.goBack()
        }
      }])
    });

  }

  PostData = () => {
    const { rowguid, ServerTime, recordDate, poolid, poolname, formcode, morningTemperature, afternoonTemperature, eveningTemperature, morningPH, afternoonPH, eveningPH } = this.state

    if (poolid.length == 0) {
      this.toast.show('水养池信息不能为空')
      return
    }

    this.toast.show(saveLoading, 0)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "savewaterpoolphinfo",
      "servicetype": "pda",
      "express": "54FA70C0",
      "ciphertext": "042b311dd63cfff370a4230b6412a775",
      "data": {
        "saveType": "add",// add-新建保存;update-修改保存
        "_rowguid": rowguid,
        "_bizid": Url.PDAFid,
        "poolId": poolid,
        "poolName": poolname,
        "formCode": formcode,
        "userName": Url.PDAusername,
        "editDate": ServerTime,
        "recordDate": recordDate,
        "morningTemperature": morningTemperature,
        "afternoonTemperature": afternoonTemperature,
        "eveningTemperature": eveningTemperature,
        "morningPH": morningPH,
        "afternoonPH": afternoonPH,
        "eveningPH": eveningPH
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: WaterPoolPH.js:232 ~ WaterPoolPH ~ formData:", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('保存成功');
        setTimeout(() => {
          this.props.navigation.replace(this.props.navigation.getParam("page"), {
            title: this.props.navigation.getParam("title"),
            pageType: this.props.navigation.getParam("pageType"),
            page: this.props.navigation.getParam("page"),
          })
          //this.props.navigation.navigate('PDAMainStack')
        }, 400)
      } else {
        this.toast.close()
        Alert.alert('保存失败', resData.message)

      }
      console.log("🚀 ~ file: steel_cage_storage.js ~ line 195 ~ SteelCage ~ resData", resData)
    }).catch((error) => {
      this.toast.show('保存失败')
      console.log("🚀 ~ file: steel_cage_storage.js ~ line 75 ~ SteelCage ~ error", error)
    });
  }

  ReviseData = () => {
    const { rowguid, ServerTime, recordDate, poolid, poolname, formcode, morningTemperature, afternoonTemperature, eveningTemperature, morningPH, afternoonPH, eveningPH } = this.state

    if (poolid.length == 0) {
      this.toast.show('水养池信息不能为空')
      return
    }

    let date1 = new Date()
    let ndate1 = this.DateToStr(date1)
    console.log("🚀 ~ file: WaterPoolPH.js:321 ~ WaterPoolPH ~ ndate1:", ndate1)

    this.toast.show(saveLoading, 0)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "savewaterpoolphinfo",
      "servicetype": "pda",
      "express": "54FA70C0",
      "ciphertext": "042b311dd63cfff370a4230b6412a775",
      "data": {
        "saveType": "update",// add-新建保存;update-修改保存
        "_rowguid": rowguid,
        "_bizid": Url.PDAFid,
        "poolId": poolid,
        "poolName": poolname,
        "formCode": formcode,
        "userName": Url.PDAusername,
        "editDate": ServerTime,
        "recordDate": recordDate,
        "morningTemperature": morningTemperature,
        "afternoonTemperature": afternoonTemperature,
        "eveningTemperature": eveningTemperature,
        "morningPH": morningPH,
        "afternoonPH": afternoonPH,
        "eveningPH": eveningPH
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: WaterPoolPH.js:232 ~ WaterPoolPH ~ formData:", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('保存成功');
        this.props.navigation.goBack()
      } else {
        this.toast.close()
        Alert.alert('保存失败', resData.message)

      }
      console.log("🚀 ~ file: steel_cage_storage.js ~ line 195 ~ SteelCage ~ resData", resData)
    }).catch((error) => {
      this.toast.show('保存失败')
      console.log("🚀 ~ file: steel_cage_storage.js ~ line 75 ~ SteelCage ~ error", error)
    });
  }

  DeleteData = () => {

    this.toast.show('删除中', 10000);

    let formData = new FormData();
    let data = {};
    data = {
      "action": "deletewaterpoolph",
      "servicetype": "projectsystem",
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        "_rowguid": guid,
        "factoryId": Url.PDAFid
      }
    }

    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: main_compreceive.js ~ line 358 ~ miain_compreceive ~ DeleteData ~ formData", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('删除成功');
        this.props.navigation.goBack()
      } else {
        Alert.alert('删除失败', resData.message)
      }
    }).catch((error) => {
    });
  }

  GetcomID = (id) => {
    const { poolid } = this.state
    let formData = new FormData();
    let data = {};
    data = {
      "action": "initwaterpoolinfo",
      "servicetype": "pda",
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        //"poolId": "FDC76BA329B7401B97C43D382EE6A056",
        "poolId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: WaterPoolPH.js:286 ~ WaterPoolPH ~ formData:", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        console.log("🚀 ~ file: equipment_maintenance.js ~ line 360 ~ EquipmentMaintenance ~ resData", resData)
        let rowguid = resData.result._rowguid
        let ServerTime = resData.result.dateTime
        let poolInfo = resData.result.poolInfo
        let poolId = poolInfo.poolid
        let formCode = poolInfo.formcode
        let poolName = poolInfo.poolname
        console.log("🚀 ~ file: WaterPoolPH.js:367 ~ WaterPoolPH ~ poolName:", poolName)
        this.setState({
          rowguid: rowguid,
          ServerTime: ServerTime,
          poolid: poolId,
          formcode: formCode,
          poolname: poolName,
          poolselect: poolName,
          isGetPoolID: true
        }, () => {
          this.forceUpdate
        })
      } else {
        console.log('resData', resData)
        Alert.alert('ERROR', resData.message)
      }

    }).catch(err => {
      console.log("🚀 ~ file: equipment_maintenance.js ~ line 400 ~ EquipmentMaintenance ~ err", err)

    })
  }

  comIDItem = () => {
    const { isGetPoolID, poolList, poolselect, isCheckPage } = this.state
    return (
      <View>
        <View style={{ flexDirection: 'row' }}>
          {
            !isCheckPage ? <TouchableCustom
              style={{ height: 30 }}
              onPress={() => {
                this.setState({ bottomVisible: true, bottomData: poolList, focusIndex: 1 })
              }} >
              <View style={{ marginRight: 16, top: 9 }}>
                <IconDown text={'请选择'} />
              </View>
            </TouchableCustom> : <View></View>
          }

          {
            !isGetPoolID ?
              <ScanButton
                onPress={() => {
                  this.setState({ focusIndex: 1, isGetStoreroom: false })
                  this.props.navigation.navigate('QRCode', {
                    type: 'compid',
                    page: 'WaterPoolPH'
                  })

                }}
              /> :
              <View style={{ top: 8 }}>
                <TouchableOpacity
                  style={{ height: 30 }}
                  onLongPress={() => {
                    Alert.alert(
                      '提示信息',
                      '是否删除水养池信息',
                      [{
                        text: '取消',
                        style: 'cancel'
                      }, {
                        text: '删除',
                        onPress: () => {
                          this.setState({
                            isGetPoolID: false,
                          })
                        }
                      }])
                  }} >
                  <Text allowFontScaling={false}>{poolselect}</Text>
                </TouchableOpacity>
              </View>
          }
        </View>
      </View>

    )
  }

  temSet = () => {
    const { morningTemperature, afternoonTemperature, eveningTemperature } = this.state

    return (
      <View>
        <Card containerStyle={styles.card_containerStyle}>

          <ListItemScan_child
            title='上午'
            rightTitle={() => {
              return (
                <View style={{ flexDirection: 'row' }}>
                  <Input
                    containerStyle={{
                      width: width * 0.18,
                      height: 18,
                      marginVertical: 0,
                      paddingVertical: 0
                    }}

                    inputContainerStyle={{
                      height: 18
                    }}
                    inputStyle={{
                      fontSize: 14,
                      textAlign: "center"
                    }}
                    onFocus={() => {
                      if (morningTemperature == '0.0') {
                        this.setState({
                          morningTemperature: ""
                        })
                      }

                    }}
                    keyboardType='number-pad'
                    placeholder='请输入'
                    allowFontScaling={false}
                    value={morningTemperature}
                    //onFocus={() => { this.setState({ focusIndex: '5' }) }}
                    onChangeText={(value) => {
                      value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
                      value = value.replace(/^\./g, ""); //验证第一个字符是数字
                      value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
                      ////value = value.replace(/\b(0+)/gi, ""); //清楚开头的0
                      this.setState({
                        morningTemperature: value
                      })
                    }} />
                  <Text>°C</Text>
                </View>
              )
            }}
          />
          <ListItemScan_child
            title='下午'
            rightTitle={() => {
              return (
                <View style={{ flexDirection: 'row' }}>
                  <Input
                    containerStyle={{
                      width: width * 0.18,
                      height: 18,
                      marginVertical: 0,
                      paddingVertical: 0
                    }}
                    inputContainerStyle={{
                      height: 18
                    }}
                    inputStyle={{
                      fontSize: 14,
                      textAlign: "center"
                    }}
                    onFocus={() => {
                      if (afternoonTemperature == '0.0') {
                        this.setState({
                          afternoonTemperature: ""
                        })
                      }
                    }}
                    keyboardType='number-pad'
                    placeholder='请输入'
                    allowFontScaling={false}
                    value={afternoonTemperature}
                    //onFocus={() => { this.setState({ focusIndex: '5' }) }}
                    onChangeText={(value) => {
                      value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
                      value = value.replace(/^\./g, ""); //验证第一个字符是数字
                      value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
                      //value = value.replace(/\b(0+)/gi, ""); //清楚开头的0
                      this.setState({
                        afternoonTemperature: value
                      })
                    }} />
                  <Text>°C</Text>
                </View>

              )
            }}
          />
          <ListItemScan_child
            title='晚上'
            rightTitle={() => {
              return (
                <View style={{ flexDirection: 'row' }}>
                  <Input
                    containerStyle={{
                      width: width * 0.18,
                      height: 18,
                      marginVertical: 0,
                      paddingVertical: 0
                    }}
                    inputContainerStyle={{
                      height: 18
                    }}
                    inputStyle={{
                      fontSize: 14,
                      textAlign: "center"
                    }}
                    onFocus={() => {
                      if (eveningTemperature == '0.0') {
                        this.setState({
                          eveningTemperature: ""
                        })
                      }
                    }}
                    keyboardType='number-pad'
                    placeholder='请输入'
                    allowFontScaling={false}
                    value={eveningTemperature}
                    //onFocus={() => { this.setState({ focusIndex: '5' }) }}
                    onChangeText={(value) => {
                      value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
                      value = value.replace(/^\./g, ""); //验证第一个字符是数字
                      value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
                      //value = value.replace(/\b(0+)/gi, ""); //清楚开头的0
                      this.setState({
                        eveningTemperature: value
                      })
                    }} />
                  <Text>°C</Text>
                </View>
              )
            }}
          />
        </Card>
      </View>
    )
  }

  PHSet = () => {
    const { morningPH, afternoonPH, eveningPH } = this.state
    return (
      <View>
        <Card containerStyle={styles.card_containerStyle}>
          <ListItemScan_child
            title='上午'
            rightTitle={() => {
              return (
                <Input
                  containerStyle={{
                    width: width * 0.18,
                    height: 18,
                    marginVertical: 0,
                    paddingVertical: 0
                  }}
                  inputContainerStyle={{
                    height: 18
                  }}
                  inputStyle={{
                    fontSize: 14,
                    textAlign: "center"
                  }}
                  onFocus={() => {
                    if (morningPH == '0.0') {
                      this.setState({
                        morningPH: ""
                      })
                    }
                  }}
                  keyboardType='number-pad'
                  placeholder='请输入'
                  allowFontScaling={false}
                  value={morningPH}
                  //onFocus={() => { this.setState({ focusIndex: '5' }) }}
                  onChangeText={(value) => {
                    value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
                    value = value.replace(/^\./g, ""); //验证第一个字符是数字
                    value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
                    //value = value.replace(/\b(0+)/gi, ""); //清楚开头的0
                    this.setState({
                      morningPH: value
                    })
                  }} />

              )
            }}
          />
          <ListItemScan_child
            title='下午'
            rightTitle={() => {
              return (
                <Input
                  containerStyle={{
                    width: width * 0.18,
                    height: 18,
                    marginVertical: 0,
                    paddingVertical: 0
                  }}
                  inputContainerStyle={{
                    height: 18
                  }}
                  inputStyle={{
                    fontSize: 14,
                    textAlign: "center"
                  }}
                  keyboardType='number-pad'
                  placeholder='请输入'
                  allowFontScaling={false}
                  value={afternoonPH}
                  onFocus={() => {
                    if (afternoonPH == '0.0') {
                      this.setState({
                        afternoonPH: ""
                      })
                    }
                  }}
                  onChangeText={(value) => {
                    value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
                    value = value.replace(/^\./g, ""); //验证第一个字符是数字
                    value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
                    //value = value.replace(/\b(0+)/gi, ""); //清楚开头的0
                    this.setState({
                      afternoonPH: value
                    })
                  }} />
              )
            }}
          />
          <ListItemScan_child
            title='晚上'
            rightTitle={() => {
              return (
                <Input
                  containerStyle={{
                    width: width * 0.18,
                    height: 18,
                    marginVertical: 0,
                    paddingVertical: 0
                  }}
                  inputContainerStyle={{
                    height: 18
                  }}
                  inputStyle={{
                    fontSize: 14,
                    textAlign: "center"
                  }}
                  keyboardType='number-pad'
                  placeholder='请输入'
                  allowFontScaling={false}
                  value={eveningPH}
                  onFocus={() => {
                    if (eveningPH == '0.0') {
                      this.setState({
                        eveningPH: ""
                      })
                    }
                  }}
                  onChangeText={(value) => {
                    value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
                    value = value.replace(/^\./g, ""); //验证第一个字符是数字
                    value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
                    //value = value.replace(/\b(0+)/gi, ""); //清楚开头的0
                    this.setState({
                      eveningPH: value
                    })
                  }} />
              )
            }}
          />
        </Card>
      </View>
    )
  }

  _DatePicker = (index) => {
    const { recordDate } = this.state
    return (
      <DatePicker
        customStyles={{
          dateInput: styles.dateInput,
          dateTouchBody: styles.dateTouchBody,
          dateText: styles.dateText
        }}
        allowFontScaling={false}
        onOpenModal={() => {
          this.setState({ focusIndex: index })
        }}
        iconComponent={<Icon name='caretdown' color={'#419fff'} iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
        }
        showIcon={true}
        mode='datetime'
        date={recordDate}
        format="YYYY-MM-DD HH:mm"
        onDateChange={(value) => {
          this.setState({
            recordDate: value
          })
        }}
      />
    )
  }

  render() {
    const { isLoading, focusIndex, isCheckPage, bottomVisible, bottomData, editEmployeeName } = this.state
    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          <NavigationEvents
            onWillFocus={this.UpdateControl} />
          <ScrollView style={styles.mainView} showsVerticalScrollIndicator={false} >
            <View style={styles.listView}>
              <ListItemScan
                isButton={true}
                focusStyle={focusIndex == '1' ? styles.focusColor : {}}
                title='水养池名称'
                rightElement={
                  this.comIDItem()
                }
              />
              <ListItemScan
                focusStyle={focusIndex == '2' ? styles.focusColor : {}}
                title='记录时间'
                rightElement={
                  this._DatePicker('2')
                }
              />
              <ListItemScan
                title='温度'
              //rightTitle='请输入'
              />
              {this.temSet()}
              <ListItemScan
                title='PH值'
              //rightTitle='请输入'
              />
              {this.PHSet()}
              <ListItemScan
                title='编制人'
                rightTitle={editEmployeeName}
              />
            </View>

            <FormButton
              onPress={isCheckPage ? this.ReviseData : this.PostData}
              title={isCheckPage ? '修改' : '保存'}
              backgroundColor={isCheckPage ? '#EB5D20' : '#17BC29'}
            />

            <BottomSheet
              isVisible={bottomVisible}
              onRequestClose
            >
              <ScrollView style={styles.bottomsheetScroll}>
                {
                  bottomData.map((item, index) => {
                    let title = ''
                    if (item.poolid) {
                      title = item.poolname
                    }
                    return (
                      <TouchableCustom
                        onPress={() => {
                          if (item.poolid) {
                            this.setState({
                              poolid: item.poolid,
                              poolselect: item.poolname,
                              poolname: item.poolname,
                              formcode: item.formcode,
                              isGetPoolID: true
                            })
                          }
                          this.setState({
                            bottomVisible: false
                          })
                        }}
                      >
                        <BottomItem backgroundColor='white' color="#333" title={title} />

                      </TouchableCustom>
                    )

                  })
                }
              </ScrollView>
              <TouchableCustom
                onPress={() => {
                  this.setState({ bottomVisible: false })
                }}>
                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
              </TouchableCustom>

            </BottomSheet>
          </ScrollView>
        </View>
      );
    }
  }
}
