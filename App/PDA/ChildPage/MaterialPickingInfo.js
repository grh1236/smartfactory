import React from 'react';
import { View, StyleSheet, TouchableHighlight, Dimensions, ActivityIndicator, Picker, TextInput } from 'react-native';
import { Button, Text, Input, Icon, Card, ListItem, BottomSheet, Overlay, Header } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { ScrollView } from 'react-native-gesture-handler';
import { NavigationEvents } from 'react-navigation';
import { styles } from '../Componment/PDAStyles';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import DatePicker from 'react-native-datepicker'
import { Alert } from 'react-native';

class BackIcon extends React.PureComponent {
    render() {
        return (
            <TouchableCustom
                onPress={this.props.onPress} >
                <Icon
                    name='arrow-back'
                    color='#419FFF' />
            </TouchableCustom>
        )
    }
}

const { height, width } = Dimensions.get('window') //获取宽高
let profitAndLossOver = ""
let listData = []

export default class MaterialPickingInfo extends React.PureComponent {
    constructor() {
        super();
        this.state = {
            action: '',
            resData: [],
            storageId: '', // 材料id
            storageCode: '', // 材料编码
            storageName: '', // 材料名称
            size: '', // 规格型号
            unit: '', // 单位
            requisitionCount: '', // 本次申领数量
            accuactualcount: '', // 项目累计领用数量
            stockCount2: '', // 可用库存

            orderNo: '', // 订单编号
            signDate: '', // 签订日期
            purchaseCount: '', // 采购数量
            unitPrice: '', // 单价
            taxRate: '', // 税率
            tax: '0', // 税额
            taxUnitPrice: '', // 含税单价
            notTaxMoney: '', // 含税金额
            remark: '',

            requisitionNo: '', // 请购单号
            requireDate: '', // 需用日期
            projectName: '', // 项目名称
            requisitionDep: '', // 请购部门

            stockCount: '', // 账面数量
            inventoryCount: '', // 盘存数量
            profitAndLoss: '', // 盘盈/盘亏

            count: '', // 库存数量
            accout: '', // 库存金额

            inStorageAmount: '', // 入库数量
            canInStorageAmount: '', // 可入库数量
            taxratio: '', // 税率
            isTempPrice: '', // 是否暂估

            outNumber: '', // 退库量
            canOutNumber: '', // 可退库量
            price: '', // 金额

            // 选择确认
            supplierSelect: false,
            roomSelect: false,

            //底栏控制
            bottomData: [],
            isTempPriceList: ['是','否'],
            teamVisible: false,
            cycleVisible: false,
            inspectorVisible: false,
            CameraVisible: false,
            isTempPriceVisible: false,
            response: '',

            checked: true,
            pictureVisible: true, // 问题照片
            imageOverSize: false, // 照片放大
            pictureUri: "",

            focusIndex: -1,
            isLoading: false,

            //查询界面取消选择按钮
            isCheckPage: false,

            focusDescriptionInput: true
        }
    }

    componentDidMount() {
        const { navigation } = this.props;
        let guid = navigation.getParam('guid') || ''
        let action = navigation.getParam('action') || ''
        let pageType = navigation.getParam('pageType') || ''
        let storageId = navigation.getParam('storageId') || ''
        let storageCode = navigation.getParam('storageCode')
        let storageName = navigation.getParam('storageName')
        let size = navigation.getParam('size')
        let unit = navigation.getParam('unit')
        let price = navigation.getParam('price') || '0'

        // 入库
        let inStorageAmount = navigation.getParam('inStorageAmount') || ''
        //let canInStorageAmount = navigation.getParam('canInStorageAmount') || ''
        let taxratio = navigation.getParam('taxratio') || ''
        let isTempPrice = navigation.getParam('isTempPrice') || ''
        let tax = navigation.getParam('tax') || '0'
        let taxUnitPrice = navigation.getParam('taxUnitPrice') || ''
        let notTaxMoney = navigation.getParam('notTaxMoney') || ''
        let remark = navigation.getParam('remark') || ''

        // 材料盘点
        let stockCount = navigation.getParam('stockCount') || ''
        let inventoryCount = navigation.getParam('inventoryCount') || ''
        let profitAndLoss = navigation.getParam('profitAndLoss') || ''

        // 库存查询
        let count = navigation.getParam('count') || '0'
        let unitPrice = navigation.getParam('unitPrice') || ''
        let accout = navigation.getParam('accout') || '0'
        if (action == '库存查询') this.resData(storageId)

        // 退库
        let outNumber = navigation.getParam('outNumber') || '0'
        let canOutNumber = navigation.getParam('canOutNumber') || '0'
        if (action == '材料退库') {
            price = (Math.round((Number.parseFloat(outNumber) * Number.parseFloat(unitPrice)) * 100) / 100).toString()
        }

        let requisitionCount = navigation.getParam('requisitionCount') == '0' ? '' : navigation.getParam('requisitionCount')
        let accuactualcount = navigation.getParam('accuactualcount')
        let stockCount2 = navigation.getParam('stockCount2')
        if (pageType == 'CheckPage') {

        } else {
            this.setState({
                action: action,
                storageId: storageId,
                storageCode: storageCode,
                storageName: storageName,
                size: size,
                unit: unit,
                requisitionCount: requisitionCount,
                accuactualcount: accuactualcount,
                stockCount2: stockCount2,
                stockCount: stockCount,
                inventoryCount: inventoryCount,
                profitAndLoss: profitAndLoss,
                count: count, 
                unitPrice: unitPrice,
                accout: accout,
                inStorageAmount: inStorageAmount,
                //canInStorageAmount: canInStorageAmount,
                taxratio: taxratio,
                isTempPrice: isTempPrice,
                outNumber: outNumber, 
                canOutNumber: canOutNumber,
                price: price,
                tax: tax,
                taxUnitPrice: taxUnitPrice,
                notTaxMoney: notTaxMoney,
                remark: remark
            })
        }
    }

    componentWillUnmount() {

        this.setState({

        })
    }

    resData = (guid) => {
        let formData = new FormData();
        let data = {};
        data = {
            "action": "stockquerydetail",
            "servicetype": "pdamaterial",
            "express": "6DFD1C9B",
            "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
            "data": {
                "factoryId": Url.PDAFid,
                "materialId": guid
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: MaterialInStorage.js ~ line 130 ~ MaterialInStorage ~ formData:", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res.statusText);
            //console.log(res);
            return res.json();
        }).then(resData => {
            if (resData.status == '100') {
                listData = resData.result
                this.setState({
                    resData: resData.result
                })
            } else {
                Alert.alert("错误提示", resData.message, [{
                    text: '取消',
                    style: 'cancel'
                }, {
                    text: '返回上一级',
                    onPress: () => {
                        this.props.navigation.goBack()
                    }
                }])
            }

        }).catch((error) => {
            console.log("🚀 ~ file: MaterialInStorage.js ~ line 233 ~ MaterialInStorage ~ error:", error)
        });
    }

    //顶栏
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('title')
        }
    }

    PostData = () => {

    }

    // 时间工具
    _DatePicker = (index) => {
        //const { ServerTime } = this.state
        return (
            <DatePicker
                customStyles={{
                    dateInput: styles.dateInput,
                    dateTouchBody: styles.dateTouchBody,
                    dateText: styles.dateText,
                }}
                iconComponent={<Icon name='caretdown' color='#419fff' iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
                }
                showIcon={true}
                date={this.state.storageDate}
                onOpenModal={() => {
                    this.setState({ focusIndex: index })
                }}
                format="YYYY-MM-DD"
                mode="date"
                onDateChange={(value) => {
                    this.setState({
                        storageDate: value
                    })
                }}
            />
        )
    }

    // 下拉菜单
    downItem = (index) => {
        const { isTempPrice, isTempPriceList } = this.state
        switch (index) {
            case '17': return (
                // 是否暂估
                <View>{(
                    <TouchableCustom onPress={() => { this.isItemVisible(isTempPriceList, index) }} >
                        <IconDown text={isTempPrice} />
                    </TouchableCustom>
                )}</View>
            ); break;

            default: return;
        }
    }

    isItemVisible = (data, focusIndex) => {
        //console.log("************** " + focusIndex + "-" + i)
        if (typeof (data) != "undefined") {
            if (data != [] && data != "") {
                switch (focusIndex) {
                    case '17': this.setState({ isTempPriceVisible: true, bottomData: data, focusIndex: focusIndex }); break;

                    default: return;
                }
            } else {
                this.toast.show("无数据")
            }
        } else {
            this.toast.show("无数据")
        }
    }

    // 正浮点数限制
    chkPrice(obj) { //方法1
        obj = obj.replace(/[^\d.]/g, "");
        //必须保证第一位为数字而不是. 
        obj = obj.replace(/^\./g, "");
        //保证只有出现一个.而没有多个. 
        obj = obj.replace(/\.{2,}/g, ".");
        //保证.只出现一次，而不能出现两次以上 
        obj = obj.replace(".", "$#$").replace(/\./g, "").replace("$#$", ".");
        return obj;
    }

    materialInventoryView = () => {
        const { isCheckPage, focusIndex, stockCount, inventoryCount, profitAndLoss } = this.state
        return (
            <View>
                <ListItemScan focusStyle={focusIndex == '8' ? styles.focusColor : {}} title='账面数量:' rightElement={stockCount} />
                <ListItemScan focusStyle={focusIndex == '9' ? styles.focusColor : {}} title='盘存数量:' rightElement={isCheckPage ? <Text>{inventoryCount}</Text> :
                    <Input
                        containerStyle={styles.quality_input_container}
                        inputContainerStyle={styles.inputContainerStyle}
                        inputStyle={[styles.quality_input_, { top: 7 }]}
                        placeholder='请填写'
                        value={inventoryCount}
                        onChangeText={(value) => {
                            let str = []
                            let count = (Math.round((Number.parseFloat(value) - Number.parseFloat(stockCount)) * 10000) / 10000).toString()
                            str = count.split('.')
                            if (str.length == 2 && str[1].length > 4) {
                                count = str[0] + '.' + str[1].substring(0, 4)
                            }
                            let result = this.chkPrice(value)
                            if (result == '') count = stockCount

                            this.setState({
                                profitAndLoss: count,
                                inventoryCount: result,
                            })
                        }} />
                } />
                <ListItemScan focusStyle={focusIndex == '10' ? styles.focusColor : {}} title='盘盈/盘亏:' rightElement={profitAndLoss} />
            </View>
        )
    }

    materialOutStorageReturnView = () => {
        const { focusIndex, isCheckPage, outNumber, canOutNumber, unitPrice, price } = this.state
        return (
            <View>
                <ListItemScan focusStyle={focusIndex == '18' ? styles.focusColor : {}} title='退库数量:' rightElement={
                    () =>
                        isCheckPage ? <Text>{outNumber}</Text> :
                            <View>
                                <Input
                                    containerStyle={styles.quality_input_container}
                                    inputContainerStyle={styles.inputContainerStyle}
                                    inputStyle={[styles.quality_input_, { top: 7 }]}
                                    placeholder='请填写'
                                    keyboardType='numeric'
                                    value={outNumber}
                                    onChangeText={(value) => {
                                        let result = this.chkPrice(value)
                                        let price = (Math.round((Number.parseFloat(result) * Number.parseFloat(unitPrice)) * 100) / 100).toString()
                                        if (Number.parseFloat(result) > canOutNumber) {
                                            Alert.alert('退库数量应小于可退库数量，请重新填写')
                                        } else {
                                            this.setState({
                                                outNumber: result,
                                                price: price
                                            })
                                        }
                                    }} />
                            </View>
                } />
                <ListItemScan focusStyle={focusIndex == '19' ? styles.focusColor : {}} title='单价:' rightElement={unitPrice} />
                <ListItemScan focusStyle={focusIndex == '20' ? styles.focusColor : {}} title='金额:' rightElement={price} />
            </View>
        )
    }

    materialStockView = () => {
        const { focusIndex, count,  unitPrice, accout } = this.state
        return (
            <View>
                <ListItemScan focusStyle={focusIndex == '11' ? styles.focusColor : {}} title='库存数量:' rightElement={count} />
                <ListItemScan focusStyle={focusIndex == '12' ? styles.focusColor : {}} title='库存单价:' rightElement={unitPrice} />
                <ListItemScan focusStyle={focusIndex == '13' ? styles.focusColor : {}} title='库存金额:' rightElement={accout} />
            </View>
        )
    }

    materialInStorageView = () => {
        const { isCheckPage, focusIndex, inStorageAmount, canInStorageAmount, unitPrice, taxratio, isTempPrice, isTempPriceList, price, tax, notTaxMoney, remark, taxUnitPrice } = this.state
       
        return (
            <View>
                <ListItemScan focusStyle={focusIndex == '14' ? styles.focusColor : {}} title='数量:' rightElement={
                    () =>
                        isCheckPage ? <Text>{inStorageAmount}</Text> :
                            <View>
                                <Input
                                    containerStyle={styles.quality_input_container}
                                    inputContainerStyle={styles.inputContainerStyle}
                                    inputStyle={[styles.quality_input_, { top: 7 }]}
                                    placeholder='请填写'
                                    keyboardType= 'numeric'
                                    //returnKeyType='next'
                                    value={inStorageAmount}
                                    onChangeText={(value) => {
                                        let result = this.chkPrice(value)
                                        //if (Number.parseFloat(result) > canInStorageAmount) {
                                        //    Alert.alert('入库数量应小于可入库数量，请重新填写')
                                        //} else {
                                        //    this.setState({
                                        //        inStorageAmount: result
                                        //    })
                                        //}
                                        let price = unitPrice != '' ? (Math.round((Number.parseFloat(result) * Number.parseFloat(unitPrice)) * 100) / 100).toString() : '0'
                                        let tax = unitPrice != '' && taxratio != '' ? (Math.round((Number.parseFloat(result) * Number.parseFloat(unitPrice) * Number.parseFloat(taxratio) * 0.01) * 100) / 100).toString() : '0'
                                        let notTaxMoney = price != '' && tax != '' ? (Math.round((Number.parseFloat(price) + Number.parseFloat(tax)) * 100) / 100).toString() : '0'
                                        this.setState({
                                            inStorageAmount: result,
                                            price: price,
                                            tax: tax,
                                            notTaxMoney: notTaxMoney
                                        })
                                    }} />
                            </View>
                } />
                <ListItemScan focusStyle={focusIndex == '15' ? styles.focusColor : {}} title='单价:' rightElement={
                    () =>
                        isCheckPage ? <Text>{unitPrice}</Text> :
                            <View>
                                <Input
                                    containerStyle={styles.quality_input_container}
                                    inputContainerStyle={styles.inputContainerStyle}
                                    inputStyle={[styles.quality_input_, { top: 7 }]}
                                    placeholder='请填写'
                                    keyboardType='numeric'
                                    //returnKeyType='next'
                                    value={unitPrice}
                                    onChangeText={(value) => {
                                        let result = this.chkPrice(value)
                                        let price = inStorageAmount != '' ? (Math.round((Number.parseFloat(result) * Number.parseFloat(inStorageAmount)) * 1000000) / 1000000).toString() : '0'
                                        let taxUnitPrice = taxratio != '' ? (Math.round((Number.parseFloat(result) * Number.parseFloat(taxratio) * 0.01 + Number.parseFloat(result)) * 1000000) / 1000000).toString() : '0'
                                        let tax = inStorageAmount != '' && taxratio != '' ? (Math.round((Number.parseFloat(result) * Number.parseFloat(inStorageAmount) * Number.parseFloat(taxratio) * 0.01) * 100) / 100).toString() : '0'
                                        let notTaxMoney = price != '' && tax != '' ? (Math.round((Number.parseFloat(price) + Number.parseFloat(tax)) * 100) / 100).toString() : '0'
                                        this.setState({
                                            unitPrice: result,
                                            price: price,
                                            taxUnitPrice: taxUnitPrice,
                                            tax: tax,
                                            notTaxMoney: notTaxMoney
                                        })
                                    }} />
                            </View>
                } />
                {/*<ListItemScan focusStyle={focusIndex == '21' ? styles.focusColor : {}} title='金额:' rightElement={price} />*/}
                <ListItemScan focusStyle={focusIndex == '16' ? styles.focusColor : {}} title='税率%:' rightElement={
                    () =>
                        isCheckPage ? <Text>{taxratio}</Text> :
                            <View>
                                <Input
                                    containerStyle={styles.quality_input_container}
                                    inputContainerStyle={styles.inputContainerStyle}
                                    inputStyle={[styles.quality_input_, { top: 7 }]}
                                    placeholder='请填写'
                                    keyboardType='numeric'
                                    value={taxratio}
                                    onChangeText={(value) => {
                                        let result = this.chkPrice(value)
                                        let tax = price != "" && result != "" ? (Math.round((Number.parseFloat(result) * 0.01 * Number.parseFloat(price)) * 100) / 100).toString() : '0'
                                        let taxUnitPrice = unitPrice != '' && result != "" ? (Math.round((Number.parseFloat(result) * Number.parseFloat(unitPrice) * 0.01 + Number.parseFloat(unitPrice)) * 1000000) / 1000000).toString() : '0'
                                        let notTaxMoney = price != '' && tax != '' ? (Math.round((Number.parseFloat(price) + Number.parseFloat(tax)) * 100) / 100).toString() : '0'
                                        this.setState({
                                            taxratio: result,
                                            tax: tax,
                                            taxUnitPrice: taxUnitPrice,
                                            notTaxMoney: notTaxMoney
                                        })
                                    }} />
                            </View>
                } />
                {/*<ListItemScan focusStyle={focusIndex == '22' ? styles.focusColor : {}} title='税额:' rightElement={tax} />*/}
                {/*<ListItemScan focusStyle={focusIndex == '23' ? styles.focusColor : {}} title='含税单价:' rightElement={taxUnitPrice} />*/}
                <ListItemScan focusStyle={focusIndex == '24' ? styles.focusColor : {}} title='含税金额:' rightElement={
                    () =>
                        isCheckPage ? <Text>{notTaxMoney}</Text> :
                            <View>
                                <Input
                                    containerStyle={styles.quality_input_container}
                                    inputContainerStyle={styles.inputContainerStyle}
                                    inputStyle={[styles.quality_input_, { top: 7 }]}
                                    placeholder='请填写'
                                    keyboardType='numeric'
                                    value={notTaxMoney}
                                    onChangeText={(value) => {
                                        let result = this.chkPrice(value)
                                        let price = taxratio != '' && result != "" ? (Math.round((Number.parseFloat(result) / (1 + (Number.parseFloat(taxratio) / 100))) * 100) / 100).toString() : '0'
                                        let tax = (Math.round((Number.parseFloat(result) - Number.parseFloat(price)) * 100) / 100).toString()
                                        
                                        if (inStorageAmount == '') {
                                            Alert.alert('请先填写数量')
                                            return
                                        }
                                        let unitPrice = (Math.round((Number.parseFloat(price) / Number.parseFloat(inStorageAmount)) * 1000000) / 1000000).toString()
                                        let taxUnitPrice = (Math.round((Number.parseFloat(taxratio) * Number.parseFloat(unitPrice) * 0.01 + Number.parseFloat(unitPrice)) * 1000000) / 1000000).toString()

                                        this.setState({
                                            notTaxMoney: result,
                                            unitPrice: unitPrice,
                                            price: price,
                                            tax: tax,
                                            taxUnitPrice: taxUnitPrice
                                        })
                                    }} />
                </View>
                } />

                {
                    isCheckPage ? <ListItemScan focusStyle={focusIndex == '17' ? styles.focusColor : {}} title='是否暂估:' rightElement={isTempPrice} /> :
                        <TouchableCustom underlayColor={'lightgray'} onPress={() => { this.isItemVisible(isTempPriceList, '17') }}>
                            <ListItemScan focusStyle={focusIndex == '17' ? styles.focusColor : {}} title='是否暂估:' rightElement={this.downItem('17')} />
                        </TouchableCustom>
                }
                <ListItemScan focusStyle={focusIndex == '25' ? styles.focusColor : {}} title='备注:' rightElement={
                    () =>
                        isCheckPage ? <Text>{remark}</Text> :
                            <View>
                                <Input
                                    containerStyle={styles.quality_input_container}
                                    inputContainerStyle={styles.inputContainerStyle}
                                    inputStyle={[styles.quality_input_, { top: 7 }]}
                                    placeholder='请填写'
                                    value={remark}
                                    onChangeText={(value) => {
                                        this.setState({
                                            remark: value
                                        })
                                    }} />
                            </View>
                } />
            </View>
        )
    }
    
    render() {
        const { navigation } = this.props;
        //从主页面传url, 未来集成到一起
        const QRurl = navigation.getParam('QRurl') || '';
        const QRid = navigation.getParam('QRid') || ''
        const type = navigation.getParam('type') || '';
        let pageType = navigation.getParam('pageType') || ''

        if (type == 'compid') {
            QRcompid = QRid
        }
        const { isCheckPage, bottomData, focusIndex, storageId, storageCode, storageName, size, unit, requisitionCount, accuactualcount, stockCount2, action, inventoryCount, profitAndLoss, stockCount, isTempPrice, isTempPriceVisible, inStorageAmount, unitPrice, taxratio, outNumber, price, tax, taxUnitPrice, notTaxMoney, remark, resData } = this.state

        if (this.state.isLoading) {
            return (
                <View style={styles.loading}>
                    <ActivityIndicator
                        animating={true}
                        color='#419FFF'
                        size="large" />
                </View>
            )
            //渲染页面
        } else {
            return (
                <View style={{ flex: 1 }}>
                    <Toast ref={(ref) => { this.toast = ref; }} position="center" />
                    <NavigationEvents
                        onWillFocus={this.UpdateControl}
                        onWillBlur={() => {
                            if (QRid != '') {
                                navigation.setParams({ QRid: '', type: '' })
                            }
                        }} />
                    <ScrollView ref={ref => this.scoll = ref} style={styles.mainView}showsVerticalScrollIndicator={false} >
                        {
                            action != '库存查询' ?
                                <View style={styles.listView}>
                                    {/*<ListItem*/}
                                    {/*    containerStyle={[{ paddingVertical: 18 }, { backgroundColor: 'transparent' }]}*/}
                                    {/*    title="领料信息"*/}
                                    {/*    style={{ paddingHorizontal: 10 }}*/}
                                    {/*    titleStyle={{ fontSize: 16 }}*/}
                                    {/*    bottomDivider*/}
                                    {/*/>*/}
                                    <ListItemScan focusStyle={focusIndex == '1' ? styles.focusColor : {}} title='材料编码:' rightElement={storageCode} />
                                    <ListItemScan focusStyle={focusIndex == '2' ? styles.focusColor : {}} title='材料名称:' rightElement={storageName} />
                                    <ListItemScan focusStyle={focusIndex == '3' ? styles.focusColor : {}} title='规格型号:' rightElement={size} />
                                    <ListItemScan focusStyle={focusIndex == '4' ? styles.focusColor : {}} title='单位:' rightElement={unit} />

                                    {
                                        action == '材料盘点' ? this.materialInventoryView() :
                                            action == '材料退库' ? this.materialOutStorageReturnView() :
                                                action == '采购入库' || action == '库存查询' ? <View /> :
                                                    <ListItemScan focusStyle={focusIndex == '5' ? styles.focusColor : {}} title='本次申领数量:' rightElement={
                                                        isCheckPage ? <Text>{requisitionCount}</Text> :
                                                            <View>
                                                                <Input
                                                                    containerStyle={styles.quality_input_container}
                                                                    inputContainerStyle={styles.inputContainerStyle}
                                                                    inputStyle={[styles.quality_input_, { top: 7 }]}
                                                                    placeholder='请填写'
                                                                    keyboardType='numeric'
                                                                    value={requisitionCount}
                                                                    onChangeText={(value) => {
                                                                        if (Number.parseFloat(value) > stockCount2) {
                                                                            if (action == '设备点检' || action == '设备保养' || action == '设备维修') {
                                                                                Alert.alert('申领数量应小于可用库存，请重新填写')
                                                                            } else {
                                                                                Alert.alert('领料数量应小于可用库存，请重新填写')
                                                                            }
                                                                        } else {
                                                                            this.setState({
                                                                                requisitionCount: this.chkPrice(value)
                                                                            })
                                                                        }
                                                                    }} />
                                                            </View>
                                                    } />
                                    }

                                    {
                                        action == '材料盘点' ? <View /> :
                                            action == '项目领料' ?
                                                <ListItemScan focusStyle={focusIndex == '6' ? styles.focusColor : {}} title='项目累计领用数量:' rightElement={accuactualcount} />
                                                : <View />
                                    }
                                    {
                                        action == '采购入库' || action == '材料退库' || action == '材料盘点' || action == '库存查询' ? <View /> :
                                            <ListItemScan focusStyle={focusIndex == '7' ? styles.focusColor : {}} title='可用库存:' rightElement={stockCount2} />
                                    }
                                    {
                                        action == '库存查询' ? this.materialStockView() : <View />
                                    }
                                    {
                                        action == '采购入库' ? this.materialInStorageView() : <View />
                                    }
                                    {
                                        action == '设备点检' || action == '设备保养' || action == '设备维修' ? <ListItemScan focusStyle={focusIndex == '26' ? styles.focusColor : {}} title='备注:' rightElement={
                                            isCheckPage ? <Text>{remark}</Text> :
                                                <View>
                                                    <Input
                                                        containerStyle={styles.quality_input_container}
                                                        inputContainerStyle={styles.inputContainerStyle}
                                                        inputStyle={[styles.quality_input_, { top: 7 }]}
                                                        placeholder='请填写'
                                                        value={remark}
                                                        onChangeText={(value) => {
                                                            this.setState({
                                                                remark: value
                                                            })
                                                        }} />
                                                </View>
                                        } /> : <View />
                                    }
                                </View>
                                :
                                resData.map((item, index) => {
                                    return (
                                        <Card containerStyle={{ borderRadius: 10, elevation: 0 }}>
                                            <View style={[{
                                                flexDirection: 'row',
                                                justifyContent: 'flex-start',
                                                alignItems: 'center'
                                            }]}>
                                                <View style={{ flex: 1 }}>

                                                    <ListItem
                                                        containerStyle={stylesMaterialPickingInfo.list_container_style}
                                                        title={item.roomName || ' '}
                                                        titleStyle={stylesMaterialPickingInfo.title}
                                                        rightTitleStyle={stylesMaterialPickingInfo.rightTitle}
                                                    />
                                                    <ListItem
                                                        containerStyle={stylesMaterialPickingInfo.list_container_style}
                                                        title={"库存:" + item.count}
                                                        rightElement={
                                                            <View style={{
                                                                backgroundColor:
                                                                    item.type == '直接材料' || item.type == '1' ? '#31C731' :
                                                                        item.type == '小五金' || item.type == '2' ? '#DAC512' :
                                                                            item.type == '甲供材料' || item.type == '3' ? '#4D8EF5' : 'transparent'
                                                            }}>
                                                                <Text style={{ color: 'white', fontSize: 13 }}> {
                                                                    item.type == '1' ? '直接材料' : item.type == '2' ? '小五金' : item.type == '3' ? '甲供材料' : item.type
                                                                } </Text>
                                                            </View>
                                                        }
                                                        titleStyle={stylesMaterialPickingInfo.title}
                                                        rightTitleStyle={stylesMaterialPickingInfo.rightTitle}
                                                    />
                                                    <ListItem
                                                        containerStyle={stylesMaterialPickingInfo.list_container_style}
                                                        title={item.materialCode || ' '}
                                                        rightElement={item.materialName}
                                                        titleStyle={stylesMaterialPickingInfo.title}
                                                        rightTitleStyle={stylesMaterialPickingInfo.rightTitle}
                                                    />
                                                </View>
                                            </View>
                                        </Card>
                                    )
                                })

                        }

                        <FormButton
                            backgroundColor='#17BC29'
                            onPress={() => {
                                if (action == '项目领料' || action == '部门领料') {
                                    this.props.navigation.navigate('MaterialProjectPicking', {
                                        state: "requisitionCount",
                                        pickingInfoId: storageId,
                                        requisitionCount: requisitionCount == '请填写' ? '0' : requisitionCount
                                    })
                                } else if (action == '材料退库') {
                                    this.props.navigation.navigate('MaterialOutStorageReturn', {
                                        state: "requisitionCount",
                                        pickingInfoId: storageId,
                                        outNumber: outNumber,
                                        price: price
                                    })
                                } else if (action == '材料盘点') {
                                    let count = inventoryCount
                                    let str = []
                                    str = count.split('.')
                                    if (str.length == 2 && str[1].length > 4) {
                                        count = str[0] + '.' + str[1].substring(0, 4)
                                    }
                                    this.props.navigation.navigate('MaterialInventory', {
                                        state: "requisitionCount",
                                        pickingInfoId: storageId,
                                        inventoryCount: inventoryCount == '' ? '0' : count,
                                        profitAndLoss: profitAndLoss
                                    })
                                } else if (action == '采购入库') {
                                    this.props.navigation.navigate('MaterialInStorage', {
                                        state: "requisitionCount",
                                        pickingInfoId: storageId,
                                        inStorageAmount: inStorageAmount == '' ? '0' : inStorageAmount,
                                        unitPrice: unitPrice == '' ? '0' : unitPrice,
                                        taxratio: taxratio == '' ? '0' : taxratio,
                                        isTempPrice: isTempPrice,
                                        price: price,
                                        tax: tax,
                                        taxUnitPrice: taxUnitPrice,
                                        notTaxMoney: notTaxMoney,
                                        remark: remark
                                    })
                                } else if (action == '设备点检') {
                                    this.props.navigation.navigate('EquipmentInspection', {
                                        state: "requisitionCount",
                                        pickingInfoId: storageId,
                                        requisitionCount: requisitionCount == '请填写' ? '0' : requisitionCount,
                                        remark: remark
                                    })
                                } else if (action == '设备保养') {
                                    this.props.navigation.navigate('EquipmentMaintenance', {
                                        state: "requisitionCount",
                                        pickingInfoId: storageId,
                                        requisitionCount: requisitionCount == '请填写' ? '0' : requisitionCount,
                                        remark: remark
                                    })
                                } else if (action == '设备维修') {
                                    this.props.navigation.navigate('EquipmentService', {
                                        state: "requisitionCount",
                                        pickingInfoId: storageId,
                                        requisitionCount: requisitionCount == '请填写' ? '0' : requisitionCount,
                                        remark: remark
                                    })
                                } else {
                                    this.props.navigation.goBack()
                                }
                            }}
                            title='确定'
                        />

                        {/*是否暂估底栏*/}
                        <BottomSheet
                            onBackdropPress={() => {
                                this.setState({
                                    isTempPriceVisible: false
                                })
                            }}
                            isVisible={isTempPriceVisible && !this.state.isCheckPage} onRequestClose={() => {
                                this.setState({
                                    isTempPriceVisible: false
                                })
                            }}
                        >
                            <ScrollView style={styles.bottomsheetScroll}>
                                {bottomData.map((item, index) => {
                                    let title = item
                                    return (
                                        <TouchableCustom
                                            onPress={() => {
                                                this.setState({
                                                    isTempPrice: item,
                                                    isTempPriceVisible: false
                                                })
                                            }}
                                        >
                                            <BottomItem backgroundColor='white' color="#333" title={title} />
                                        </TouchableCustom>
                                    )
                                })}
                            </ScrollView>
                            <TouchableHighlight
                                onPress={() => {
                                    this.setState({ isTempPriceVisible: false })
                                }}>
                                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
                            </TouchableHighlight>
                        </BottomSheet>
                    </ScrollView>
                </View>
            )
        }
    }
}

const stylesMaterialPickingInfo = StyleSheet.create({
    list_container_style: { marginVertical: 0, paddingVertical: 2, backgroundColor: 'transparent' },
    title: {
        fontSize: 13,
        //color: '#999'
    },
    rightTitle: {
        fontSize: 13,
        textAlign: 'right',
        lineHeight: 14,
        flexWrap: 'wrap',
        width: width / 2,
        color: '#333'
    },
});