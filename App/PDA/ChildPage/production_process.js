import React from 'react';
import { View, StatusBar, TouchableOpacity, TouchableHighlight, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, Input, Icon, Badge, Card, ListItem, BottomSheet, CheckBox, Avatar, Overlay, Header } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { RFT } from '../../Url/Pixal';
import { ScrollView } from 'react-native-gesture-handler';
import CardList from "../Componment/CardList";
import DatePicker from 'react-native-datepicker'
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { Image } from 'react-native';
import { NavigationEvents } from 'react-navigation';
import { styles } from '../Componment/PDAStyles';
import ScanButton from '../Componment/ScanButton';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import ListItemScan_child from '../Componment/ListItemScan_child';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import CheckBoxScan from '../Componment/CheckBoxScan';
import AvatarAdd from '../Componment/AvatarAdd';
import AvatarImg from '../Componment/AvatarImg';
import TimeLine from '../Componment/TimeLine';
import DeviceStorage from '../../Url/DeviceStorage';
import { DeviceEventEmitter, NativeModules } from 'react-native';
import overlayImg from '../Componment/OverlayImg';
import OverlayImgZoomPicker from '../Componment/OverlayImgZoomPicker';
let varGetError = false

let DeviceStorageData = {} //缓存数据

const { height, width } = Dimensions.get('window') //获取宽高

let QRcompid = '', QRtaiwanId = '', QRacceptanceId = '', QRmonitorId = '', QRInspectorId = ''

let fileflag = "7"

let imageArr = [], imageFileArr = [], //照片数组
  checkimageArr = [], //检查照片数组
  questionChickIdArr = [], questionChickArr = [], //子表问题数组
  isCheckSecondList = [], //是否展开二级
  isCheckedAllList = [] //二级菜单是否被全选

let guid = ''
let subguid = ''



class BackIcon extends React.PureComponent {
  render() {
    return (
      <TouchableCustom
        onPress={this.props.onPress} >
        <Icon
          name='arrow-back'
          color='#419FFF' />
      </TouchableCustom>
    )
  }
}


export default class ProductionProcess extends React.PureComponent {
  constructor() {
    super();
    this.state = {
      resData: [],
      type: 'compid',
      rowguid: '',
      ServerTime: '',
      steelCageIDs: ' ',
      monitorSetup: '',
      monitors: [],
      monitorId: '',
      monitorName: '',
      monitorSelect: '请选择',
      monitorTeamId: '',
      monitorTeamName: '',
      Inspectors: [],
      InspectorId: '',
      InspectorName: '',
      InspectorSelect: '请选择',
      acceptanceId: '',
      acceptanceName: '',
      checkResult: '合格',
      taiwanId: '',
      compData: {},
      compId: '',
      steelLabel: '',
      compCode: '',
      procedid: '',
      iskey: '',
      procedname: '',
      CurrentProcedure: '',
      devicename: '',
      equipment: '',
      Tai_QRname: '',
      editEmployeeName: '',
      checked: true,
      stopTime: "",
      splitTime: "",
      endTime: "",
      repair: "",
      reason: "",
      //问题缺陷
      dictionary: [],
      problemDefect: '',
      measure: '',
      problemId: '',
      questionChick: -1,
      questionChickIdArr: [],
      questionChickArr: [],
      problemDefects: [],
      isCheckSecondList: [],
      isCheckedAllList: [],
      isQuestionOverlay: false,
      //工序流程卡子页面
      ProcessOverLay: false,
      technologyproductState: '',
      technologycompCode: '',
      technologycompTypeName: '',
      technologycompList: [],
      //是否扫码成功
      isGetcomID: false,
      isGettaiwanId: false,
      isGetTeamPeopleInfo: false,
      isGetCommonMould: false,
      isGetInspector: false,
      //长按删除功能控制
      iscomIDdelete: false,
      istaiwanIddelete: false,
      isCommonMoulddelete: false,
      isTeamPeopledelete: false,
      isInspectordelete: false,
      //底栏控制
      bottomData: [],
      bottomVisible: false,
      datepickerVisible: false,
      CameraVisible: false,
      response: '',
      //照片控制
      pictureSelect: false,
      pictureUri: '',
      imageArr: [],
      imageFileArr: [],
      imageOverSize: false,
      fileid: "",
      recid: "",
      isPhotoUpdate: false,
      //
      GetError: false,
      buttondisable: false,
      //钢筋笼控制
      steelCageID: '',
      steelCageCode: '',
      isGetsteelCageInfoforPutMold: false,
      issteelCageIDdelete: false,
      focusIndex: 0,
      isLoading: true,
      lineList: [],
      //查询界面取消选择按钮
      isCheck: false,
      isCheckPage: false,
      remark: '',
      //控制app拍照，日期，相册选择功能
      isAppPhotoSetting: false,
      currShowImgIndex: 0,
      isAppDateSetting: false,
      isAppPhotoAlbumSetting: false
    }
    this.checkFunc = this.checkFunc.bind(this)
    //this.requestCameraPermission = this.requestCameraPermission.bind(this)
  }

  componentDidMount() {
    StatusBar.setBarStyle('dark-content')
    const { navigation } = this.props;
    guid = navigation.getParam('guid') || ''
    subguid = navigation.getParam('subguid') || ''
    let pageType = navigation.getParam('pageType') || ''
    let isAppPhotoSetting = navigation.getParam('isAppPhotoSetting')
    let isAppDateSetting = navigation.getParam('isAppDateSetting')
    let isAppPhotoAlbumSetting = navigation.getParam('isAppPhotoAlbumSetting')
    this.setState({
      isAppPhotoSetting: isAppPhotoSetting,
      isAppDateSetting: isAppDateSetting,
      isAppPhotoAlbumSetting: isAppPhotoAlbumSetting
    })
    if (pageType == 'CheckPage') {
      this.checkResData(guid, subguid)

      this.setState({ isCheckPage: true })
    } else {
      this.resData()
      DeviceStorage.get('DeviceStorageDataQI')
        .then(res => {
          if (res) {
            console.log("🚀 ~ file: qualityinspection.js ~ line 133 ~ ProductionProcess ~ componentDidMount ~ res", res)
            let DeviceStorageDataObj = JSON.parse(res)
            DeviceStorageData = DeviceStorageDataObj
            console.log("🚀 ~ file: qualityinspection.js ~ line 136 ~ ProductionProcess ~ componentDidMount ~ DeviceStorageData", DeviceStorageData)
            if (res.length != 0) {
              this.setState({
                "isGetInspector": DeviceStorageDataObj.isGetInspector,
                "isGetTeamPeopleInfo": DeviceStorageDataObj.isGetTeamPeopleInfo,
                "InspectorId": DeviceStorageDataObj.InspectorId,
                "InspectorName": DeviceStorageDataObj.InspectorName,
                "InspectorSelect": DeviceStorageDataObj.InspectorName,
                "monitorId": DeviceStorageDataObj.monitorId,
                "monitorName": DeviceStorageDataObj.monitorName,
                "monitorSelect": DeviceStorageDataObj.monitorName,
                "monitorTeamId": DeviceStorageDataObj.monitorTeamId,
                "monitorTeamName": DeviceStorageDataObj.monitorTeamName,
              })
            }
          }
        }).catch(err => {
          console.log("🚀 ~ file: loading.js ~ line 131 ~ SteelCage ~ componentDidMount ~ err", err)

        })
    }
    //通过使用DeviceEventEmitter模块来监听事件
    this.iDataScan = DeviceEventEmitter.addListener('iDataScan', (Event) => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 115 ~ SteelCage ~ Event.ScanResult", Event.ScanResult)
      if (typeof Event.ScanResult != undefined) {
        let data = Event.ScanResult
        let arr = data.split("=");
        let id = ''
        id = arr[arr.length - 1]
        console.log("🚀 ~ file: concrete_pouring.js ~ line 118 ~ SteelCage ~ id", id)
        if (this.state.type == 'compid') {
          this.GetcomID(id)
        }
        if (this.state.type == 'taiwanId') {
          {
            this.GettaiwanId(id)
          }
        }
        if (this.state.type == 'acceptanceId') {
          {
            this.GetCommonMould(id)
          }
        }
        if (this.state.type == 'monitorId') {
          {
            this.GetTeamPeopleInfo(id)
          }
        }
        if (this.state.type == 'InspectorId') {
          {
            this.GetInspector(id)
          }
        }
        if (this.state.type == 'steelCageID') {
          {
            this.GetsteelCageInfoforPutMold(id)
          }
        }

      }
    });
  }

  componentWillUnmount() {
    imageArr = [], imageFileArr = [],
      checkimageArr = [], //检查照片数组
      questionChickIdArr = [], questionChickArr = [], //子表问题数组
      isCheckSecondList = [], //是否展开二级
      isCheckedAllList = [] //二级菜单是否被全选
    QRcompid = '', guid = '', subguid = ''
    this.iDataScan.remove()

  }

  UpdateControl = () => {
    if (typeof this.props.navigation.getParam('QRid') != "undefined") {
      if (this.props.navigation.getParam('QRid') != "") {
        this.toast.show(Url.isLoadingView, 0)
      }
    }
    const { navigation } = this.props;
    const { isGetcomID, isGetCommonMould, isGetInspector, isGetTeamPeopleInfo, isGettaiwanId, isGetsteelCageInfoforPutMold } = this.state
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || ''
    console.log("🚀 ~ file: qualityinspection.js ~ line 11113 ~ SteelCage ~ componentDidUpdate ~ QRid", QRid)
    let type = navigation.getParam('type') || '';
    console.log("🚀 ~ file: qualityinspection.js ~ line 105 ~ SteelCage ~ componentDidUpdate ~ this.state.GetError", this.state.GetError)

    if (type == 'compid') {
      QRcompid = QRid
      this.GetcomID(QRid)
    } else if (type == 'taiwanId') {
      {
        this.GettaiwanId(QRid)
      }
    } else if (type == 'acceptanceId') {
      {
        this.GetCommonMould(QRid)
      }
    } else if (type == 'monitorId') {
      {
        this.GetTeamPeopleInfo(QRid)
      }
    } else if (type == 'InspectorId') {
      {
        this.GetInspector(QRid)
      }
    } else if (type == 'steelCageID') {
      {
        this.GetsteelCageInfoforPutMold(QRid)
      }
    }
    if (QRid != '') {
      navigation.setParams({ QRid: '', type: '' })
    }
    console.log("🚀 ~ file: qualityinspection.js ~ line 11147 ~ SteelCage ~ componentDidUpdate ~ QRid", QRid)

  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  checkResData = (guid, subguid) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "ObtainProcedure",
      "servicetype": "pda",
      "express": "A65B2C5B",
      "ciphertext": "73d2ddd465b4e66618f7610846e6919f",
      "data": {
        "subguid": subguid,
        "guid": guid,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: qualityinspection.js ~ line 189 ~ ProductionProcess ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 204 ~ ProductionProcess ~ resData", resData)
      let rowguid = resData.result.guid
      let ServerTime = resData.result.time
      let iskey = resData.result.iskey
      let compCode = resData.result.compCode
      let compId = resData.result.compId
      QRcompid = compId
      let taiWanName = resData.result.taiWanName
      let teamName = resData.result.teamName
      let proced = resData.result.proced
      let devicename = resData.result.equipment
      let repair = resData.result.repair
      let reason = resData.result.Reason
      let remark = resData.result.Remark
      let stopTime = resData.result.Stop_time
      let splitTime = resData.result.Split_time
      let endTime = resData.result.end_time


      let projectName = resData.result.projectName
      let componentImageUrl = resData.result.componentImageUrl
      let steelCage = resData.result.steelCage
      let compTypeName = resData.result.compTypeName
      let designType = resData.result.designType
      let floorName = resData.result.floorName
      let floorNoName = resData.result.floorNoName
      let volume = resData.result.volume
      let weight = resData.result.weight
      let Inspector = resData.result.qualityconfirm
      let monitor = resData.result.Monitorconfirm
      let checkResult = resData.result.isQualified
      let dictionary = resData.result.dictionary
      let monitors = resData.result.monitors
      let Inspectors = resData.result.Inspectors
      let defects = resData.result.defects

      monitors.map((item) => {
        if (item.name == monitor) {
          this.setState({ monitorId: item.rowguid })
        }
      })

      Inspectors.map((item) => {
        if (item.InspectorName == Inspector) {
          this.setState({ InspectorId: item.InspectorId })
        }
      })
      if (checkResult == '合格') {
        this.setState({
          checked: true
        })
      } else {
        this.setState({
          checked: false
        })
        defects.map((defectitem) => {
          questionChickArr.push(defectitem)
          questionChickIdArr.push(defectitem.rowguid)
        })
        this.setState({
          questionChickArr: questionChickArr,
          questionChickIdArr: questionChickIdArr
        })
      }
      let editEmployeeName = resData.result.editEmployeeName

      //图片数组
      let imageArr = [], imageFileArr = [];
      let tmp = {}
      tmp.fileid = ""
      tmp.uri = componentImageUrl
      tmp.url = componentImageUrl
      imageFileArr.push(tmp)
      imageArr.push(componentImageUrl)
      this.setState({
        imageFileArr: imageFileArr
      })
      console.log("🚀 ~ file: qualityinspection.js ~ line 224 ~ ProductionProcess ~ imageArr", imageArr)

      //产品子表
      let compData = {}, result = {}
      result.taiWanName = taiWanName
      result.projectName = projectName
      result.compTypeName = compTypeName
      result.designType = designType
      result.floorNoName = floorNoName
      result.floorName = floorName
      result.volume = volume
      result.weight = weight
      compData.result = result
      this.setState({
        resData: resData,
        rowguid: rowguid,
        ServerTime: ServerTime,
        compCode: compCode,
        compId: compId,
        taiWanName: taiWanName,
        procedname: proced,
        devicename: devicename,
        iskey: iskey,
        editEmployeeName: editEmployeeName,
        compData: compData,
        monitorName: monitor,
        monitorTeamName: teamName,
        monitorSelect: monitor,
        Inspector: Inspector,
        InspectorSelect: Inspector,
        steelCageCode: steelCage,
        checkResult: checkResult,
        monitors: monitors,
        Inspectors: Inspectors,
        dictionary: dictionary,
        repair: repair,
        reason: reason,
        remark: remark,
        stopTime: stopTime,
        splitTime: splitTime,
        endTime: endTime,
        imageArr: imageArr,
        pictureSelect: true,
        isGetcomID: true,
        isGettaiwanId: true,
        isGetTeamPeopleInfo: true,
        isGetCommonMould: true,
        isGetInspector: true,
        isCheck: true
      })
      this.setState({
        isLoading: false,
      })
    }).catch((error) => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 215 ~ ProductionProcess ~ error", error)
    });
  }

  resData = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "complexhidecheckinit",
      "servicetype": "pda",
      "express": "8AC4C0B0",
      "ciphertext": "7df83f712027c63f147f6c2d4a43f08d",
      "data": {
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      //初始化错误提示
      if (resData.status == 100) {
        let rowguid = resData.result.rowguid
        let ServerTime = resData.result.serverTime
        let monitorSetup = resData.result.monitorSetup
        if (monitorSetup != '扫码') {
          this.setState({
            isGetTeamPeopleInfo: true,
            isGetInspector: true,
          })
        }
        let monitors = resData.result.monitors
        let Inspectors = resData.result.Inspectors
        this.setState({
          resData: resData,
          rowguid: rowguid,
          ServerTime: ServerTime,
          splitTime: ServerTime,
          endTime: ServerTime,
          monitorSetup: monitorSetup,
          monitors: monitors,
          Inspectors: Inspectors,
          isLoading: false
        })
      }
      else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }
      console.log("🚀 ~ file: qualityinspection.js ~ line 314 ~ ProductionProcess ~ resData", resData)


    }).catch((error) => {
    });

  }

  GetcomID = (id) => {
    console.log("🚀 ~ file: qualityinspection.js ~ line 194 ~ SteelCage ~ GetcomID", id)
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getTechnologyInfo",
      "servicetype": "pda",
      "express": "0F51EF23",
      "ciphertext": "a91d6dc5c5a20f2a35939b6e2d91a0ef",
      "data": {
        "compId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: production_process.js ~ line 379 ~ ProductionProcess ~ data", data)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: production_process.js ~ line 419 ~ ProductionProcess ~ resData", resData)
      this.toast.close()
      if (resData.status == '100') {
        let result = resData.result
        let compId = result.compId
        let compCode = result.compCode
        let taiWanName = result.taiWanName
        let compTypeName = result.compTypeName
        let designType = result.designType
        let floorName = result.floorName
        let floorNoName = result.floorNoName
        let projectName = result.projectName
        let volume = result.volume
        let weight = result.weight
        let CurrentProcedure = result.CurrentProcedure
        let procedid = result.procedid
        let iskey = result.iskey
        let steelLabel = result.steelLabel
        let procedname = result.procedname
        let devicename = result.devicename
        let dictionary = result.dictionary

        /* let compData = {}
        compData.taiWanName = taiWanName
        compData.projectName = projectName
        compData.compTypeName = compTypeName
        compData.designType = designType
        compData.floorNoName = floorNoName
        compData.floorName = floorName
        compData.CurrentProcedure = CurrentProcedure
        compData.weight = weight
        compData.volume = volume */

        this.setState({
          compData: resData,
          compCode: compCode,
          compId: compId,
          steelLabel: steelLabel,
          procedid: procedid,
          iskey: iskey,
          CurrentProcedure: CurrentProcedure,
          procedname: procedname,
          devicename: devicename,
          dictionary: dictionary,
          isGetcomID: true,
          iscomIDdelete: false
        }, () => {
          this.forceUpdate();
        })
        if (this.state.steelLabel == '使用' && this.state.iskey == '钢筋笼') {
          this.setState({
            focusIndex: 3,
            type: "steelCageID"
          })
          setTimeout(() => { this.scoll.scrollTo({ x: 0, y: 400, animated: true }) }, 300)
        } else {
          this.setState({
            focusIndex: 6,
            type: "monitorId"
          })
          setTimeout(() => { this.scoll.scrollToEnd() }, 300)
        }


      } else {
        console.log('resData', resData)
        this.setState({ compId: '' })
        Alert.alert('错误', resData.message)
      }

    }).catch(err => {
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  comIDItem = (index) => {
    const { isGetcomID, buttondisable, compCode, compId } = this.state
    return (
      <View>
        {
          !isGetcomID ?
            <View style={{ flexDirection: 'row' }}>
              <View>
                <Input
                  ref={ref => { this.input1 = ref }}
                  containerStyle={styles.scan_input_container}
                  inputContainerStyle={styles.scan_inputContainerStyle}
                  inputStyle={[styles.scan_input]}
                  placeholder='请输入'
                  keyboardType='numeric'
                  value={compId}
                  onChangeText={(value) => {
                    value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
                    value = value.replace(/^\./g, ""); //验证第一个字符是数字
                    value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
                    //value = value.replace(/\b(0+)/gi, ""); //清楚开头的0
                    this.setState({
                      compId: value
                    })
                  }}
                  onFocus={() => {
                    this.setState({
                      focusIndex: index,
                      type: 'compid',
                    })
                  }}
                  onSubmitEditing={() => {
                    let inputComId = this.state.compId
                    console.log("🚀 ~ file: qualityinspection.js ~ line 468 ~ QualityInspection ~ inputComId", inputComId)
                    inputComId = inputComId.replace(/\b(0+)/gi, "")
                    this.GetcomID(inputComId)
                  }}
                />
              </View>
              <ScanButton

                onPress={() => {
                  this.setState({
                    iscomIDdelete: false,
                    buttondisable: true,
                    focusIndex: 0
                  })
                  setTimeout(() => {
                    this.setState({ GetError: false, buttondisable: false })
                    varGetError = false
                  }, 1500)
                  this.props.navigation.navigate('QRCode', {
                    type: 'compid',
                    page: 'ProductionProcess'
                  })
                }}
                onLongPress={() => {
                  this.setState({
                    type: 'compid',
                    focusIndex: index
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }} F
              />
            </View>
            :
            <TouchableCustom
              onPress={() => {
                console.log('onPress')
              }}
              onLongPress={() => {
                console.log('onLongPress')
                Alert.alert(
                  '提示信息',
                  '是否删除构件信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      this.setState({
                        compData: [],
                        compCode: '',
                        compId: '',
                        iscomIDdelete: true,
                        isGetcomID: false,
                      })
                    }
                  }])
              }} >
              <Text>{compCode}</Text>
            </TouchableCustom>
        }
      </View>

    )
  }

  GetsteelCageInfoforPutMold = (id) => {
    console.log("🚀 ~ file: qualityinspection.js ~ line 346 ~ SteelCage ~ id", id)
    let formData = new FormData();
    let data = {};
    data = {
      "action": "GetsteelCageInfoforPutMold",
      "servicetype": "pda",
      "express": "6C8FAB14",
      "ciphertext": "96d2d1428051129167842f3b5eaca07f",
      "data": {
        "steelCageID": id,
        "compId": this.state.compId,
        "factoryId": Url.PDAFid
      }
    }
    console.log("🚀 ~ file: qualityinspection.js ~ line 349 ~ SteelCage ~ data", data)
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let steelCageID = resData.result.steelCageID
        let steelCageCode = resData.result.steelCageCode
        console.log("🚀 ~ file: qualityinspection.js ~ line 374 ~ SteelCage ~ steelCageCode", steelCageCode)
        this.setState({
          steelCageID: steelCageID,
          steelCageCode: steelCageCode,
          isGetsteelCageInfoforPutMold: true,
          issteelCageIDdelete: false
        })
        this.setState({
          focusIndex: 6,
          type: "monitorId"
        })
        setTimeout(() => { this.scoll.scrollToEnd() }, 300)
      } else {
        console.log('resData', resData)
        Alert.alert('ERROR', resData.message)
        varGetError = true
        this.setState({
          GetError: true
        })
      }

    }).catch(err => {
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  GettaiwanId = (id) => {
    let Tai_QRname = '111'
    let formData = new FormData();
    let data = {};
    data = {
      "action": "gettaiwanInfo",
      "servicetype": "pda",
      "express": "8FBA0BDC",
      "ciphertext": "591057f7d292870338e0fdffe9ef719f",
      "data": {
        "factoryId": Url.PDAFid,
        "taiwanId": id
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let taiwanId = resData.result.taiwanId
        Tai_QRname = resData.result.Tai_QRname
        this.setState({
          taiwanId: taiwanId,
          Tai_QRname: Tai_QRname,
          isGettaiwanId: true,
          istaiwanIddelete: false
        })
      } else {
        Alert.alert('ERROR', resData.message)
        varGetError = true
        this.setState({
          GetError: true
        })
      }

    }).catch(err => {
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  taiwanItem = (index) => {
    const { isGettaiwanId, buttondisable, Tai_QRname } = this.state
    return (
      <View>
        {
          !isGettaiwanId ?
            <View>
              <ScanButton

                onPress={() => {
                  this.setState({
                    focusIndex: 1,
                    istaiwanIddelete: false,
                    buttondisable: true
                  })
                  setTimeout(() => {
                    this.setState({ GetError: false, buttondisable: false })
                    varGetError = false
                  }, 1500)
                  this.props.navigation.navigate('QRCode', {
                    type: 'taiwanId',
                    page: 'ProductionProcess'
                  })
                }}
              />
            </View>
            :
            <TouchableCustom
              onPress={() => {
                console.log('onPress')
              }}
              onLongPress={() => {
                console.log('onLongPress')
                Alert.alert(
                  '提示信息',
                  '是否删除模台信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      this.setState({
                        taiwanId: '',
                        Tai_QRname: '',
                        isGettaiwanId: false,
                        istaiwanIddelete: true,
                      })
                    }
                  }])
              }} >
              <Text>{Tai_QRname}</Text>
            </TouchableCustom>
        }
      </View>
    )
  }

  GetCommonMould = (id) => {
    let acceptanceName = '111'
    let formData = new FormData();
    let data = {};
    data = {
      "action": "GetCommonMould",
      "servicetype": "pda",
      "express": "50545047",
      "ciphertext": "f3eedf54128226aef62dd6023a1c1573",
      "data": {
        "compId": this.state.compId,
        "acceptanceId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let acceptanceId = resData.result.acceptanceId
        acceptanceName = resData.result.acceptanceName
        this.setState({
          acceptanceId: acceptanceId,
          acceptanceName: acceptanceName,
          isGetCommonMould: true,
          isCommonMoulddelete: false
        })
      } else {
        Alert.alert('ERROR', resData.message)
        varGetError = true
        this.setState({
          GetError: true
        })
      }


    }).catch(err => {
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  GetTeamPeopleInfo = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getTeamPeopleInfo",
      "servicetype": "pda",
      "express": "2B9B0E37",
      "ciphertext": "eb1a5891270a0ebdbb68d37e27f82b92",
      "data": {
        "factoryId": Url.PDAFid,
        "monitorId": id
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let monitorId = resData.result.monitorId
        let monitorName = resData.result.monitorName
        let teamId = resData.result.teamId
        let teamName = resData.result.teamName

        this.setState({
          monitorId: monitorId,
          monitorName: monitorName,
          monitorTeamId: teamId,
          monitorTeamName: teamName,
          isGetTeamPeopleInfo: true,
          isTeamPeopledelete: false
        }, () => {
          DeviceStorageData = {
            "isGetTeamPeopleInfo": true,
            "InspectorId": this.state.InspectorId,
            "InspectorName": this.state.InspectorName,
            "InspectorSelect": this.state.InspectorName,
            "monitorId": this.state.monitorId,
            "monitorName": this.state.monitorName,
            "monitorSelect": this.state.monitorName,
            "monitorTeamId": this.state.monitorTeamId,
            "monitorTeamName": this.state.monitorTeamName,
          }

          DeviceStorage.save('DeviceStorageDataQI', JSON.stringify(DeviceStorageData))
        })
        this.setState({
          focusIndex: 7,
          type: "InspectorId"
        })
        setTimeout(() => { this.scoll.scrollToEnd() }, 300)

      } else {
        Alert.alert('ERROR', resData.message)
        varGetError = true
        this.setState({
          GetError: true
        })
      }
    }).catch(err => {
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  monitorNameItem = (index) => {
    const { isGetTeamPeopleInfo, buttondisable, monitorName, monitorSetup, monitors, monitorSelect } = this.state
    console.log("🚀 ~ file: qualityinspection.js ~ line 559 ~ SteelCage ~ monitors", monitors)
    return (
      <View>
        {
          monitorSetup == '扫码' ?
            (
              !isGetTeamPeopleInfo ?
                <View>
                  <ScanButton
                    onPress={() => {
                      this.setState({
                        isTeamPeopledelete: false,
                        focusIndex: index
                      })
                      setTimeout(() => {
                        this.setState({ GetError: false, buttondisable: false })
                        varGetError = false
                      }, 1500)
                      this.props.navigation.navigate('QRCode', {
                        type: 'monitorId',
                        page: 'ProductionProcess'
                      })
                    }}
                    onLongPress={() => {
                      this.setState({
                        type: 'monitorId',
                        focusIndex: index
                      })
                      NativeModules.PDAScan.onScan();
                    }}
                    onPressOut={() => {
                      NativeModules.PDAScan.offScan();
                    }}
                  />
                </View>
                :
                <TouchableCustom
                  onPress={() => {
                    console.log('onPress')
                  }}
                  onLongPress={() => {
                    console.log('onLongPress')
                    Alert.alert(
                      '提示信息',
                      '是否删除班组信息',
                      [{
                        text: '取消',
                        style: 'cancel'
                      }, {
                        text: '删除',
                        onPress: () => {
                          this.setState({
                            monitorId: '',
                            monitorName: '',
                            monitorTeamId: '',
                            monitorTeamName: '',
                            isGetTeamPeopleInfo: false,
                            isTeamPeopledelete: true
                          })
                        }
                      }])
                  }} >
                  <Text>{monitorName}</Text>
                </TouchableCustom>
            )
            :
            (
              <TouchableCustom onPress={() => { this.isBottomVisible(monitors, index) }} >
                <IconDown text={monitorSelect} />
              </TouchableCustom>
            )

        }
      </View>
    )
  }

  GetInspector = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getInspector",
      "servicetype": "pda",
      "express": "CDAB7A2D",
      "ciphertext": "a590b603df9b5a4384c130c7c21befb7",
      "data": {
        "InspectorId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let InspectorId = resData.result.InspectorId
        let InspectorName = resData.result.InspectorName
        this.setState({
          InspectorId: InspectorId,
          InspectorName: InspectorName,
          isGetInspector: true,
          isInspectordelete: false
        }, () => {
          DeviceStorageData = {
            "isGetInspector": true,
            "InspectorId": this.state.InspectorId,
            "InspectorName": this.state.InspectorName,
            "InspectorSelect": this.state.InspectorName,
            "monitorId": this.state.monitorId,
            "monitorName": this.state.monitorName,
            "monitorSelect": this.state.monitorName,
            "monitorTeamId": this.state.monitorTeamId,
            "monitorTeamName": this.state.monitorTeamName,
          }

          DeviceStorage.save('DeviceStorageDataQI', JSON.stringify(DeviceStorageData))
        })
      } else {
        Alert.alert('ERROR', resData.message)
        varGetError = true
        this.setState({
          GetError: true
        })
      }

    }).catch(err => {
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  inspectorItem = (index) => {
    const { isGetInspector, buttondisable, InspectorName, monitorSetup, InspectorSelect, Inspectors } = this.state
    return (
      <View>
        {
          monitorSetup == '扫码' ?
            (
              !isGetInspector ?
                <View>
                  <ScanButton
                    onPress={() => {
                      this.setState({
                        isInspectordelete: false,
                        buttondisable: true,
                        focusIndex: 6
                      })
                      this.props.navigation.navigate('QRCode', {
                        type: 'InspectorId',
                        page: 'ProductionProcess'
                      })
                    }}
                    onLongPress={() => {
                      this.setState({
                        type: 'InspectorId',
                        focusIndex: index
                      })
                      NativeModules.PDAScan.onScan();
                    }}
                    onPressOut={() => {
                      NativeModules.PDAScan.offScan();
                    }}
                  />
                </View>
                :
                <TouchableCustom
                  onPress={() => {
                    console.log('onPress')

                  }}
                  onLongPress={() => {
                    console.log('onLongPress')
                    Alert.alert(
                      '提示信息',
                      '是否删除质检员信息',
                      [{
                        text: '取消',
                        style: 'cancel'
                      }, {
                        text: '删除',
                        onPress: () => {
                          this.setState({
                            InspectorId: '',
                            InspectorName: '',
                            isGetInspector: false,
                            isInspectordelete: true
                          })
                        }
                      }])
                  }} >
                  <Text>{InspectorName}</Text>
                </TouchableCustom>
            ) :
            (
              <TouchableCustom onPress={() => { this.isBottomVisible(Inspectors, index) }} >
                <IconDown text={InspectorSelect} />
              </TouchableCustom>
            )
        }
      </View>
    )
  }

  LookUpTechnology = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "LookUpTechnology",
      "servicetype": "pda",
      "express": "512DA1E6",
      "ciphertext": "3d7e1a5d8bdae9edb4d45953081162a8",
      "data": {
        "compId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: production_process.js ~ line 379 ~ ProductionProcess ~ data", data)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log("🚀 ~ file: production_process.js ~ line 1013 ~ ProductionProcess ~ res", res)
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: production_process.js ~ line 1014 ~ ProductionProcess ~ resData", resData)

      this.toast.close()
      if (resData.status == '100') {
        let result = resData.result
        let productState = result.productState
        let compCode = result.compCode
        let compTypeName = result.compTypeName
        let compList = result.compList

        let lineList = []
        compList.map((item, index) => {
          let lineItem = {}
          lineItem.title = item.procedName
          lineItem.description1 = "开始时间：" + " " + item.Split_time
          lineItem.description2 = "停止时间：" + " " + item.end_time
          lineList.push(lineItem)

        })

        console.log("🚀 ~ file: production_process.js ~ line 1066 ~ ProductionProcess ~ compList.map ~ lineList", lineList)

        this.setState({
          technologyproductState: productState,
          technologycompCode: compCode,
          technologycompTypeName: compTypeName,
          technologycompList: compList,
          lineList: lineList
        })
        this.setState({ ProcessOverLay: true })
      } else {
        console.log('resData', resData)
        Alert.alert('错误', resData.message)
      }

    }).catch(err => {
      console.log("🚀 ~ file: production_process.js ~ line 1034 ~ ProductionProcess ~ err", err)
    })
  }

  checkRefuseItem = (index) => {
    const { focusIndex, checkResult, dictionary } = this.state
    console.log("🚀 ~ file: quality_product_inspection.js ~ line 614 ~ SteelCage ~ this.state.checkResult", this.state.checkResult)
    if (checkResult == '不合格') {
      return (
        <View >
          <ListItemScan
            focusColor={focusIndex == (index) ? styles.focusColor : {}}
            title='问题缺陷/改进措施'
            rightElement={
              <TouchableCustom onPress={() => { this.setState({ isQuestionOverlay: false, focusIndex: (index + 1) }) }} >
                <IconDown text={'请选择'} />
              </TouchableCustom>
            } />
          <View style={{ marginHorizontal: width * 0.05, backgroundColor: "#f8f8f8", borderRadius: 5, marginTop: 10 }}>
            <FlatList
              data={this.state.dictionary}
              renderItem={this._renderItem.bind(this)}
              extraData={this.state} />
          </View>

        </View>

      )
    }
  }

  PostData = () => {
    const { InspectorId, acceptanceId, steelLabel, monitorId, compId, ServerTime, checkResult, rowguid, taiwanId, steelCageIDs, steelCageID, remark, monitorTeamName, iskey, monitorName, technologyproductState, monitorTeamId, procedid, compData, InspectorName, equipment, questionChickArr, procedname, CurrentProcedure, stopTime, splitTime, endTime, repair, reason, devicename, recid } = this.state
    const { navigation } = this.props;
    const QRid = navigation.getParam('QRid') || '';

    if (compId == '') {
      this.toast.show('产品编号不能为空');
      return
    } else if (steelCageID == '' && steelLabel == '使用' && iskey == '钢筋笼') {
      this.toast.show('钢筋笼编码不能为空');
      return
    } else if (monitorId == '') {
      this.toast.show('班组信息不能为空');
      return
    } else if (InspectorId == '') {
      this.toast.show('质检员信息不能为空');
      return
    } else if (checkResult == '不合格') {
      if (questionChickArr.length == 0) {
        this.toast.show('请选择问题缺陷')
        return
      }
    }
    if (this.state.isAppPhotoSetting) {
      if (imageArr.length == 0) {
        this.toast.show('请先拍摄构件照片');
        return
      }
    }

    this.toast.show(saveLoading, 0)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "saveTechWorkProcedure",
      "servicetype": "pda",
      "express": "FA353910",
      "ciphertext": "8854f72a1b81682b7beee8c374cf10f8",
      "data": {
        "rowguid": rowguid,
        "teamName": monitorTeamName,
        "repair": repair,
        "reason": reason,
        "split_Time": splitTime,
        "monitorConfirm": monitorName,
        "iskey": iskey,
        "isQualified": checkResult,
        "people_Name": Url.PDAEmployeeName,
        "myConfirm": Url.PDAEmployeeName,
        "Remark": remark,
        "stop_Time": stopTime,
        "procedname": procedname,
        "steelCageID": steelCageID,
        "Monitor_Id": monitorId,
        "end_Time": endTime,
        "normal": "",
        "CurrentProcedure": CurrentProcedure,
        "procedId": procedid,
        "factoryId": Url.PDAFid,
        "equipment": devicename,
        "userName": Url.PDAusername,
        "quality": InspectorId,
        "people_Id": Url.PDAEmployeeId,
        "qualityconfirm": InspectorName,
        "compId": compId,
        "teamId": monitorTeamId,
        "defects": questionChickArr,
        "steelLabel": steelLabel,
        "recid": recid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    if (!Url.isAppNewUpload) {
      imageArr.map((item, index) => {
        formData.append('img_' + index, {
          uri: item,
          type: 'image/jpeg',
          name: 'img_' + index
        })
      })
    }
    // imageArr.map((item, index) => {
    //   formData.append('img_' + index, {
    //     uri: item,
    //     type: 'image/jpeg',
    //     name: 'img_' + index
    //   })
    // })
    console.log("🚀 ~ file: qualityinspection.js ~ line 793 ~ SteelCage ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('保存成功');
        setTimeout(() => {
          this.props.navigation.replace(this.props.navigation.getParam("page"), {
            title: this.props.navigation.getParam("title"),
            pageType: this.props.navigation.getParam("pageType"),
            page: this.props.navigation.getParam("page"),
            isAppPhotoSetting: this.props.navigation.getParam("isAppPhotoSetting"),
            isAppDateSetting: this.props.navigation.getParam("isAppDateSetting"),
            isAppPhotoAlbumSetting: this.props.navigation.getParam("isAppPhotoAlbumSetting"),
          })
          //this.props.navigation.navigate('PDAMainStack')
        }, 400)
      } else {
        this.toast.close();
        Alert.alert('保存失败', resData.message)
        console.log('resData', resData)
      }
    }).catch((error) => {
      if (error.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('保存失败', "响应内容不是合法JSON格式")
        return
      }
      if (error.toString().indexOf('Network request faile') != -1) {
        Alert.alert('保存失败', "网络请求错误")
        return
      }
      Alert.alert('保存失败', error.toString())
    });
  }

  ReviseData = () => {
    const { splitTime, monitorId, endTime, checkResult, stopTime, InspectorId, steelCageID, questionChickArr, remark, repair, reason } = this.state
    const { navigation } = this.props;
    let guid = navigation.getParam('guid') || ''
    console.log('ReviseData')
    let formData = new FormData();
    let data = {};
    data = {
      "action": "ReviseProcedure",
      "servicetype": "pda",
      "express": "06C795EE",
      "ciphertext": "a1e3818d57d9bfc9437c29e7a558e32e",
      "data": {
        "subguid": subguid,
        "repair": repair,
        "PDAuserid": Url.PDAEmployeeId,
        "Split_time": splitTime,
        "monitorId": monitorId,
        "factoryId": Url.PDAFid,
        "componentImageId": "",
        "end_time": endTime,
        "isQualified": checkResult,
        "Reason": reason,
        "Remark": remark || "",
        "Stop_time": stopTime,
        "InspectorId": InspectorId,
        "steelCageId": steelCageID,
        "myconfirm": Url.PDAusername,
        "defects": questionChickArr,
        "PDAuser": Url.PDAusername
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: qualityinspection.js ~ line 953 ~ ProductionProcess ~ ReviseData ~ formData", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('修改成功');
        this.props.navigation.navigate('MainCheckPage')
      } else {
        Alert.alert(
          '修改失败',
          resData.message,
          [{
            text: '取消',
            style: 'cancel'
          }, {
            text: '返回上一级',
            onPress: () => {
              this.props.navigation.goBack()
            }
          }])
      }
    }).catch((error) => {
      Alert.alert(
        '修改失败',
        resData.message,
        [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      console.log("🚀 ~ file: production_process.js ~ line 1328 ~ ProductionProcess ~ error", error)
    });
  }

  cameraFunc = () => {
    launchCamera(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: parseInt(Url.isResizePhoto),
        maxWidth: parseInt(Url.isResizePhoto),
        saveToPhotos: true,

      },
      (response) => {
        if (response.uri == undefined) {
          return
        }
        imageArr.push(response.uri)
        this.setState({
          //pictureSelect: true,
          pictureUri: response.uri,
          imageArr: imageArr
        })
        if (Url.isAppNewUpload) {
          this.cameraPost(response.uri)
        } else {
          this.setState({
            pictureSelect: true,
          })
        }
        console.log(response)
      },
    )
  }

  camLibraryFunc = () => {
    launchImageLibrary(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: parseInt(Url.isResizePhoto),
        maxWidth: parseInt(Url.isResizePhoto),
      },
      (response) => {
        if (response.uri == undefined) {
          return
        }
        imageArr.push(response.uri)
        this.setState({
          //pictureSelect: true,
          pictureUri: response.uri,
          imageArr: imageArr
        })
        if (Url.isAppNewUpload) {
          this.cameraPost(response.uri)
        } else {
          this.setState({
            pictureSelect: true,
          })
        }
        console.log(response)
      },
    )
  }

  camandlibFunc = () => {
    Alert.alert(
      '提示信息',
      '选择拍照方式',
      [{
        text: '取消',
        style: 'cancel'
      }, {
        text: '拍照',
        onPress: () => {
          this.cameraFunc()
        }
      }, {
        text: '相册',
        onPress: () => {
          this.camLibraryFunc()
        }
      }])
  }

   // 展示/隐藏 放大图片
  handleZoomPicture = (flag, index) => {
    this.setState({
      imageOverSize: false,
      currShowImgIndex: index || 0
    })
  }

  // 加载放大图片弹窗
  renderZoomPicture = () => {
    const { imageOverSize, currShowImgIndex, imageFileArr } = this.state
    return (
      <OverlayImgZoomPicker
        isShowImage={imageOverSize}
        currShowImgIndex={currShowImgIndex}
        zoomImages={imageFileArr}
        callBack={(flag) => this.handleZoomPicture(flag)}
      ></OverlayImgZoomPicker>
    )
  }

  cameraPost = (uri) => {
    const { recid } = this.state
    this.setState({
      isPhotoUpdate: true
    })
    this.toast.show('图片上传中', 2000)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "savephote",
      "servicetype": "photo_file",
      "express": "D2979AB2",
      "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
      "data": {
        "fileflag": fileflag,
        "username": Url.PDAusername,
        "recid": recid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    formData.append('img', {
      uri: uri,
      type: 'image/jpeg',
      name: 'img'
    })
    console.log("🚀 ~ file: concrete_pouring.js ~ line 672 ~ SteelCage ~ formData", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 690 ~ SteelCage ~ resData", resData)
      this.toast.close()
      if (resData.status == '100') {
        let fileid = resData.result.fileid;
        let recid = resData.result.recid;

        let tmp = {}
        tmp.fileid = fileid
        tmp.uri = uri
        tmp.url = uri
        imageFileArr.push(tmp)
        this.setState({
          fileid: fileid,
          recid: recid,
          pictureSelect: true,
          imageFileArr: imageFileArr
        })
        console.log("🚀 ~ file: concrete_pouring.js ~ line 704 ~ SteelCage ~ imageFileArr", imageFileArr)
        this.toast.show('图片上传成功');
        this.setState({
          isPhotoUpdate: false
        })
      } else {
        Alert.alert('图片上传失败', resData.message)
        this.toast.close(1)
      }
    }).catch((error) => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 692 ~ SteelCage ~ error", error)
    });
  }

  cameraDelete = (item) => {
    let i = imageArr.indexOf(item)

    if (Url.isAppNewUpload) {
      let formData = new FormData();
      let data = {};
      data = {
        "action": "deletephote",
        "servicetype": "photo_file",
        "express": "D2979AB2",
        "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
        "data": {
          "fileid": imageFileArr[i].fileid,
        }
      }
      formData.append('jsonParam', JSON.stringify(data))
      fetch(Url.PDAurl, {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        body: formData
      }).then(res => {
        console.log(res.statusText);
        console.log(res);
        return res.json();
      }).then(resData => {
        if (resData.status == '100') {
          if (i > -1) {
            imageArr.splice(i, 1)
            imageFileArr.splice(i, 1)
            this.setState({
              imageArr: imageArr,
              imageFileArr: imageFileArr
            }, () => {
              this.forceUpdate()
            })
          }
          if (imageArr.length == 0) {
            this.setState({
              pictureSelect: false
            })
          }
          this.toast.show('图片删除成功');
        } else {
          Alert.alert('图片删除失败', resData.message)
          this.toast.close(1)
        }
      }).catch((error) => {
        console.log("🚀 ~ file: concrete_pouring.js ~ line 761 ~ SteelCage ~ cameraDelete ~ error", error)
      });
    } else {
      if (i > -1) {
        imageArr.splice(i, 1)
        this.setState({
          imageArr: imageArr,
        }, () => {
          this.forceUpdate()
        })
      }
      if (imageArr.length == 0) {
        this.setState({
          pictureSelect: false
        })
      }
    }
  }

  imageView = () => {
    return (
      <View style={{ flexDirection: 'row' }}>
        {
          this.state.pictureSelect ?
            this.state.imageArr.map((item, index) => {
              return (
                <AvatarImg
                  item={item}
                  index={index}
                  onPress={() => {
                    this.setState({
                      imageOverSize: true,
                      pictureUri: item
                    })
                  }}
                  onLongPress={() => {
                    Alert.alert(
                      '提示信息',
                      '是否删除构件照片',
                      [{
                        text: '取消',
                        style: 'cancel'
                      }, {
                        text: '删除',
                        onPress: () => {
                          this.cameraDelete(item)
                        }
                      }])
                  }} />
              )
            }) : <View></View>
        }
      </View>
    )
  }

  _DatePicker = (Time, type, focusIndex) => {
    const { ServerTime } = this.state
    return (
      <DatePicker
        customStyles={{
          dateInput: styles.dateInput,
          dateTouchBody: styles.dateTouchBody,
          dateText: this.state.isAppDateSetting ? styles.dateText : styles.dateDisabled,
        }}
        iconComponent={<Icon name='caretdown' color={this.state.isAppDateSetting ? '#419fff' : "#666"} iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
        }
        showIcon={true}
        onOpenModal={() => {
          this.setState({ focusIndex: focusIndex })
        }}
        mode='datetime'
        date={Time}
        format="YYYY-MM-DD HH:mm"
        disabled={!this.state.isAppDateSetting}
        onDateChange={(value) => {
          if (type == 'splitTime') {
            this.setState({
              splitTime: value
            })
          } else {
            this.setState({
              endTime: value
            })
          }

        }}
      />
    )
  }

  isBottomVisible = (data, focusIndex) => {
    if (typeof (data) != "undefined") {
      if (data.length > 0) {
        this.setState({ bottomVisible: true, bottomData: data, focusIndex: focusIndex })
      }
    } else {
      this.toast.show("无数据")
    }

  }

  render() {
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || ''
    const type = navigation.getParam('type') || '';
    let pageType = navigation.getParam('pageType') || ''

    if (type == 'compid') {
      QRcompid = QRid
    }
    const { buttondisable, focusIndex, steelLabel, resData, ServerTime, isGetcomID, isGetCommonMould, isGetInspector, isGetTeamPeopleInfo, isGetsteelCageInfoforPutMold, isGettaiwanId, datepickerVisible, Tai_QRname, acceptanceName, monitorSetup, monitors, monitorid, monitorName, monitorTeamName, Inspectors, InspectorsId, InspectorsName, compData, compCode, bottomVisible, bottomData, isCheck, remark, procedid, iskey, procedname, devicename, dictionary, technologycompCode, technologycompList, technologycompTypeName, technologyproductState, equipment, lineList, stopTime, splitTime, endTime, checkResult, reason, repair } = this.state

    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          <NavigationEvents
            onWillFocus={this.UpdateControl}
            onWillBlur={() => {
              if (QRid != '') {
                navigation.setParams({ QRid: '', type: '' })
              }
            }} />
          <ScrollView ref={ref => this.scoll = ref} style={styles.mainView} showsVerticalScrollIndicator={false} >
            <View style={styles.listView}>
              {
                <ListItem
                  containerStyle={{ paddingBottom: 6 }}
                  leftAvatar={
                    <View >
                      <View style={{ flexDirection: 'column' }} >
                        {
                          pageType == 'CheckPage' ? <Avatar size="large"></Avatar> :
                            <AvatarAdd
                              pictureSelect={this.state.pictureSelect}
                              backgroundColor='rgba(77,142,245,0.20)'
                              color='#4D8EF5'
                              title="构"
                              onPress={() => {
                                if (this.state.isAppPhotoAlbumSetting) {
                                  this.camandlibFunc()
                                } else {
                                  this.cameraFunc()
                                }
                              }} />
                        }
                        {this.imageView()}
                      </View>
                    </View>
                  }
                />
              }

              <ListItemScan
                isButton={true}
                focusStyle={focusIndex == '10' ? styles.focusColor : {}}
                title={<View style={{ flexDirection: 'row' }}>
                  <Text>当前工序：</Text>
                  <Text style={{ color: 'red' }}>{procedname}</Text>
                </View>}
                //title={'当前工序:' + ' ' + procedname}
                rightElement={
                  <View>
                    <Button
                      titleStyle={{ fontSize: 14 }}
                      title='查看完成工艺'
                      type='solid'
                      buttonStyle={{ paddingVertical: 6, backgroundColor: '#4D8EF5', marginVertical: 0 }}
                      onPress={() => {
                        if (QRcompid != '') {
                          this.LookUpTechnology(QRcompid)
                        } else {
                          this.toast.show('请扫描构件编码')
                        }
                      }} />
                  </View>
                }
              />
              <ListItemScan
                isButton={!isGetcomID}
                focusStyle={focusIndex == '0' ? styles.focusColor : {}}
                title='产品编号'
                onPressIn={() => {
                  this.setState({
                    type: 'compid',
                    focusIndex: 0
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onLongPress={() => {
                  this.setState({
                    type: 'compid',
                    focusIndex: 0
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}
                rightElement={
                  this.comIDItem(0)
                }
              />
              {
                this.state.isGetcomID ?
                  <CardList compData={compData} /> : <View></View>
              }
              {
                steelLabel == '使用' && iskey == '钢筋笼' ?
                  <ListItemScan
                    isButton={!isGetsteelCageInfoforPutMold}
                    onPressIn={() => {
                      this.setState({
                        type: 'steelCageID',
                        focusIndex: 3
                      })
                      NativeModules.PDAScan.onScan();
                    }}
                    onLongPress={() => {
                      this.setState({
                        type: 'steelCageID',
                        focusIndex: 3
                      })
                      NativeModules.PDAScan.onScan();
                    }}
                    onPressOut={() => {
                      NativeModules.PDAScan.offScan();
                    }}
                    focusStyle={focusIndex == '3' ? styles.focusColor : {}}
                    title='钢筋笼编码'
                    rightTitle={
                      !isGetsteelCageInfoforPutMold ?
                        <View>
                          <ScanButton

                            onPress={() => {
                              this.setState({
                                focusIndex: 3,
                                issteelCageIDdelete: false,
                                buttondisable: false
                              })
                              this.props.navigation.navigate('QRCode', {
                                type: 'steelCageID',
                                page: 'QualityInspection'
                              })
                            }}
                            onLongPress={() => {
                              this.setState({
                                type: 'steelCageID',
                                focusIndex: 3
                              })
                              NativeModules.PDAScan.onScan();
                            }}
                            onPressOut={() => {
                              NativeModules.PDAScan.offScan();
                            }}
                          />
                        </View>
                        :
                        <TouchableCustom
                          onPress={() => {
                            console.log('onPress')
                          }}
                          onLongPress={() => {
                            console.log('onLongPress')
                            Alert.alert(
                              '提示信息',
                              '是否删除钢筋笼编码',
                              [{
                                text: '取消',
                                style: 'cancel'
                              }, {
                                text: '删除',
                                onPress: () => {
                                  this.setState({
                                    steelCageID: '',
                                    steelCageCode: '',
                                    isGetsteelCageInfoforPutMold: false,
                                    issteelCageIDdelete: true
                                  })
                                }
                              }])
                          }} >
                          <Text numberOfLines={1}>{this.state.steelCageCode}</Text>
                        </TouchableCustom>
                    }
                  /> : <View></View>
              }
              <ListItemScan
                title='作业班组'
                rightTitle={this.state.monitorTeamName}
              />
              <ListItemScan
                title='作业人员'
                rightTitle={Url.PDAEmployeeName}
              />
              <ListItemScan
                focusStyle={focusIndex == '4' ? styles.focusColor : {}}
                title='开始时间'
                rightElement={
                  this._DatePicker(splitTime, "splitTime", 4)
                }
              //rightTitle={ServerTime}
              />
              <ListItemScan
                focusStyle={focusIndex == '9' ? styles.focusColor : {}}
                title='结束时间'
                rightElement={
                  this._DatePicker(endTime, "endTime", 9)
                }
              //rightTitle={ServerTime}
              />
              <ListItemScan
                title='正常作业时间'
                rightElement={
                  <View></View>
                }
              />
              <ListItemScan
                title='修改作业时间'
                rightElement={
                  <View>
                    <Input
                      containerStyle={styles.quality_input_container}
                      inputContainerStyle={styles.inputContainerStyle}
                      inputStyle={[styles.quality_input_, { top: 7 }]}
                      placeholder='选填'
                      defaultValue={repair}
                      value={repair}
                      onChangeText={(value) => {
                        this.setState({
                          repair: value
                        })
                      }} />
                  </View>
                }
              />
              <ListItemScan
                title='设备名称'
                rightTitle={devicename}
              />
              <ListItemScan
                title='停机时间'
                rightElement={
                  <View>
                    <Input
                      containerStyle={styles.quality_input_container}
                      inputContainerStyle={styles.inputContainerStyle}
                      inputStyle={[styles.quality_input_, { top: 7 }]}
                      placeholder='选填'
                      defaultValue={stopTime}
                      value={stopTime}
                      onChangeText={(value) => {
                        this.setState({
                          stopTime: value
                        })
                      }} />
                  </View>
                }
              />
              <ListItemScan
                title='停机原因'
                rightElement={
                  <View>
                    <Input
                      containerStyle={styles.quality_input_container}
                      inputContainerStyle={styles.inputContainerStyle}
                      inputStyle={[styles.quality_input_, { top: 7 }]}
                      placeholder='选填'
                      defaultValue={reason}
                      value={reason}
                      onChangeText={(value) => {
                        this.setState({
                          reason: value
                        })
                      }} />
                  </View>
                }
              />
              <ListItemScan
                focusStyle={focusIndex == '8' ? styles.focusColor : {}}
                title='备注'
                rightElement={
                  <View>
                    <Input
                      containerStyle={styles.quality_input_container}
                      inputContainerStyle={styles.inputContainerStyle}
                      inputStyle={[styles.quality_input_, { top: 7 }]}
                      placeholder='选填'
                      defaultValue={remark}
                      value={remark}
                      onChangeText={(value) => {
                        this.setState({
                          remark: value
                        })
                      }} />
                  </View>
                }
              />
              <ListItemScan
                title='自检确认'
                rightTitleStyle={{ width: width / 1.5, textAlign: 'right', fontSize: 15 }}
                rightTitle={Url.PDAEmployeeName}
              />
              <ListItemScan
                isButton={true}
                focusStyle={focusIndex == '5' ? styles.focusColor : {}}
                bottomDivider
                title='检查结果'
                rightTitle={
                  <View style={{ flexDirection: "row", marginRight: -23, paddingVertical: 0 }}>
                    <CheckBoxScan
                      title='合格'
                      checked={this.state.checked}
                      onPress={() => {
                        if (!this.state.isCheck) {
                          this.setState({
                            checked: true,
                            checkResult: '合格',
                            focusIndex: 5
                          })
                        }
                      }}
                    />
                    <CheckBoxScan
                      title='不合格'
                      checked={!this.state.checked}
                      onPress={() => {
                        if (!this.state.isCheck) {
                          this.setState({
                            checked: false,
                            checkResult: '不合格',
                            focusIndex: 5
                          })
                        }
                      }}
                    />
                  </View>
                }
              />
              {
                checkResult == '不合格' ?
                  <View >
                    <ListItemScan
                      focusColor={focusIndex == (12) ? styles.focusColor : {}}
                      title='问题缺陷/改进措施'
                      rightElement={
                        <TouchableCustom onPress={() => { this.setState({ isQuestionOverlay: false, focusIndex: (11) }) }} >
                          <IconDown text={'请选择'} />
                        </TouchableCustom>
                      } />
                    <View style={{ marginHorizontal: width * 0.05, backgroundColor: "#f8f8f8", borderRadius: 5, marginTop: 10 }}>
                      <FlatList
                        data={this.state.dictionary}
                        renderItem={this._renderItem.bind(this)}
                        extraData={this.state}
                      />
                    </View>
                  </View> : <View></View>
              }
              {/* this.checkRefuseItem(4) */}
              <TouchableCustom underlayColor={'lightgray'} onPress={() => {
                if (monitorSetup != '扫码') {
                  this.isBottomVisible(monitors, 6)
                } else {
                  this.setState({
                    type: 'monitorId',
                    focusIndex: 6
                  })
                  NativeModules.PDAScan.onScan();
                }
              }}
                onPressIn={() => {
                  if (monitorSetup == '扫码') {
                    this.setState({
                      type: 'monitorId',
                      focusIndex: 6
                    })
                    NativeModules.PDAScan.onScan();
                  }
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}>

                <ListItemScan
                  isButton={monitorSetup == '扫码' && !isGetTeamPeopleInfo}
                  focusStyle={focusIndex == '6' ? styles.focusColor : {}}
                  title='班长确认'
                  rightElement={
                    this.monitorNameItem(6)
                  }
                />
              </TouchableCustom>
              <ListItemScan
                title='班组名称'
                rightTitleStyle={{ width: width / 1.5, textAlign: 'right', fontSize: 15 }}
                rightTitle={this.state.monitorTeamName}
              />

              <TouchableCustom underlayColor={'lightgray'} onPress={() => {
                if (monitorSetup != '扫码') {
                  this.isBottomVisible(Inspectors, 7)
                } else {
                  this.setState({
                    type: 'InspectorId',
                    focusIndex: 7
                  })
                  NativeModules.PDAScan.onScan();
                }
              }}
                onPressIn={() => {
                  if (monitorSetup == '扫码') {
                    this.setState({
                      type: 'InspectorId',
                      focusIndex: 7
                    })
                    NativeModules.PDAScan.onScan();
                  }
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}>
                <ListItemScan
                  isButton={monitorSetup == '扫码' && !isGetInspector}
                  focusStyle={focusIndex == '7' ? styles.focusColor : {}}
                  title='质检确认'
                  rightElement={
                    this.inspectorItem('7')
                  }
                />
              </TouchableCustom>

            </View>

            {
              isCheck ? <FormButton
                backgroundColor='#EB5D20'
                onPress={() => {
                  this.ReviseData()
                }}
                title='修改'
              /> :
                <FormButton
                  backgroundColor='#17BC29'
                  onPress={this.PostData}
                  title='保存'
                  disabled={this.state.isPhotoUpdate}
                />
            }

             {this.renderZoomPicture()}

            {/* {overlayImg(obj = {
              onRequestClose: () => {
                this.setState({ imageOverSize: !this.state.imageOverSize }, () => {
                  console.log("🚀 ~ file: equipment_maintenance.js:1696 ~ render ~ imageOverSize:", this.state.imageOverSize)
                })
              },
              onPress: () => {
                this.setState({
                  imageOverSize: !this.state.imageOverSize
                })
              },
              uri: this.state.pictureUri,
              isVisible: this.state.imageOverSize
            })
            }*/}

            <Overlay
              fullScreen={true}
              animationType='fade'
              isVisible={this.state.ProcessOverLay}
              onRequestClose={() => {
                this.setState({ ProcessOverLay: !this.state.ProcessOverLay });
              }}
            >
              <Header
                containerStyle={{ height: 45, paddingTop: 0, top: -5 }}
                centerComponent={{ text: '已完成生产工序', style: { color: 'gray', fontSize: 20 } }}
                rightComponent={<BackIcon
                  onPress={() => {
                    this.setState({
                      ProcessOverLay: !this.state.ProcessOverLay
                    });
                  }} />}
                backgroundColor='white'
              />
              <ScrollView ref={ref => this.scoll = ref} style={styles.mainView} showsVerticalScrollIndicator={false} >
                <View style={styles.listView}>
                  <ListItemScan
                    title='生产状态'
                    rightTitleStyle={{ width: width / 1.5, textAlign: 'right', fontSize: 15, color: 'red' }}
                    rightTitle={technologyproductState}

                  />
                  <ListItemScan
                    title='产品编号'
                    rightTitleStyle={{ width: width / 1.5, textAlign: 'right', fontSize: 15 }}
                    rightTitle={technologycompCode}
                  />
                  <ListItemScan
                    title='构件类型'
                    rightTitleStyle={{ width: width / 1.5, textAlign: 'right', fontSize: 15 }}
                    rightTitle={technologycompTypeName}
                  />
                  <View>
                    <TimeLine
                      style={{ flex: 1, marginTop: 20, }}
                      data={lineList}
                      showTime={false}
                    />
                  </View>
                </View>
              </ScrollView>

            </Overlay>

            <BottomSheet
              isVisible={this.state.CameraVisible}
              onRequestClose={() => {
                this.setState({ CameraVisible: false })
              }}
            >
              <TouchableCustom
                onPress={() => {
                  launchCamera(
                    {
                      mediaType: 'photo',
                      includeBase64: false,
                      maxHeight: 600,
                      maxWidth: 600,
                      saveToPhotos: true
                    },
                    (response) => {
                      if (response.uri == undefined) {
                        return
                      }
                      imageArr.push(response.uri)
                      this.setState({
                        pictureSelect: true,
                        pictureUri: response.uri,
                        imageArr: imageArr
                      })
                      console.log(response)
                    },
                  )
                  this.setState({ CameraVisible: false })
                }} >
                <ListItemScan
                  title='拍照' >
                </ListItemScan>
              </TouchableCustom>
              <TouchableCustom
                onPress={() => {
                  launchImageLibrary(
                    {
                      mediaType: 'photo',
                      includeBase64: false,
                      maxHeight: 600,
                      maxWidth: 600,

                    },
                    (response) => {
                      if (response.uri == undefined) {
                        return
                      }
                      imageArr.push(response.uri)
                      this.setState({
                        pictureSelect: true,
                        pictureUri: response.uri,
                        imageArr: imageArr
                      })
                      console.log(response)
                    },
                  )
                  this.setState({ CameraVisible: false })
                }} >
                <ListItemScan
                  title='相册' >
                </ListItemScan>
              </TouchableCustom>
              <ListItemScan
                title='关闭'
                containerStyle={{ backgroundColor: 'red' }}
                onPress={() => {
                  this.setState({ CameraVisible: false })
                }} >
              </ListItemScan>
            </BottomSheet>

            <BottomSheet
              isVisible={datepickerVisible}
              onRequestClose={() => {
                this.setState({ datepickerVisible: false })
              }}
            >

              <ListItemScan
                title='确定'
                onPress={() => {
                  this.setState({
                    datepickerVisible: false
                  })
                }}
              ></ListItemScan>
            </BottomSheet>

            <BottomSheet
              isVisible={bottomVisible}
              onRequestClose={() => {
                this.setState({
                  bottomVisible: false
                })
              }}
              onBackdropPress={() => {
                this.setState({
                  bottomVisible: false
                })
              }}
            >
              <ScrollView style={styles.bottomsheetScroll}>
                {
                  bottomData.map((item, index) => {
                    let title = ''
                    if (item.rowguid) {
                      title = item.name + ' ' + item.teamName
                    } else {
                      title = item.InspectorName
                    }
                    return (
                      <TouchableCustom
                        onPress={() => {
                          if (item.rowguid) {
                            this.setState({
                              monitorId: item.rowguid,
                              monitorSelect: item.name,
                              monitorName: item.name,
                              monitorTeamId: item.teamId,
                              monitorTeamName: item.teamName,
                            }, () => {
                              DeviceStorageData = {
                                "InspectorId": this.state.InspectorId,
                                "InspectorName": this.state.InspectorName,
                                "InspectorSelect": this.state.InspectorName,
                                "monitorId": this.state.monitorId,
                                "monitorName": this.state.monitorName,
                                "monitorSelect": this.state.monitorName,
                                "monitorTeamId": this.state.monitorTeamId,
                                "monitorTeamName": this.state.monitorTeamName,
                              }

                              DeviceStorage.save('DeviceStorageDataQI', JSON.stringify(DeviceStorageData))
                            })
                          } else if (item.InspectorId) {
                            this.setState({
                              InspectorId: item.InspectorId,
                              InspectorName: item.InspectorName,
                              InspectorSelect: item.InspectorName,
                            }, () => {
                              DeviceStorageData = {
                                "InspectorId": this.state.InspectorId,
                                "InspectorName": this.state.InspectorName,
                                "InspectorSelect": this.state.InspectorName,
                                "monitorId": this.state.monitorId,
                                "monitorName": this.state.monitorName,
                                "monitorSelect": this.state.monitorName,
                                "monitorTeamId": this.state.monitorTeamId,
                                "monitorTeamName": this.state.monitorTeamName,
                              }

                              DeviceStorage.save('DeviceStorageDataQI', JSON.stringify(DeviceStorageData))
                            })
                          }
                          this.setState({
                            bottomVisible: false
                          })
                        }}
                      >
                        <BottomItem backgroundColor='white' color="#333" title={title} />
                      </TouchableCustom>
                    )

                  })
                }
              </ScrollView>
              <TouchableHighlight
                onPress={() => {
                  this.setState({ bottomVisible: false })
                }}>
                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
              </TouchableHighlight>

            </BottomSheet>
          </ScrollView>
        </View>
      )
    }
  }

  checkFunc = (item) => {
    if (questionChickIdArr.indexOf(item.rowguid) == -1) {
      questionChickArr.push(item)
      questionChickIdArr.push(item.rowguid)
    } else {
      //查出取消勾选项的角标
      let num = questionChickIdArr.indexOf(item.rowguid)
      //删掉！
      questionChickIdArr.splice(num, 1)
      questionChickArr.splice(num, 1)
    }
    this.setState({
      questionChickArr: questionChickArr,
      questionChickIdArr: questionChickIdArr
    })
    console.log("🚀 ~ file: production_process.js ~ line 1200 ~ ProductionProcess ~ this.state.questionChickIdArr", this.state.questionChickIdArr)
    console.log("🚀 ~ file: production_process.js ~ line 1200 ~ ProductionProcess ~ this.state.questionChickArr", this.state.questionChickArr)
  }

  _renderItem = ({ item, index }) => {
    console.log("🚀 ~ file: production_process.js ~ line 1068 ~ ProductionProcess ~ item", item)
    return (
      <View>
        <ListItem
          title={
            <View style={{ flexDirection: 'row' }}>
              <Text style={{ fontSize: 14, marginLeft: 4 }}>{item.defect}</Text>
            </View>
          }
          underlayColor={'lightgray'}
          containerStyle={{ marginVertical: 0, paddingVertical: 10, borderColor: 'transparent', borderWidth: 0, backgroundColor: "transparent" }}
          onPress={() => {
            this.checkFunc(item)
            this.forceUpdate()
          }}
          rightElement={
            <CheckBoxScan
              key={index}
              //checkedColor='red'
              checked={this.state.questionChickIdArr.indexOf(item.rowguid) != -1}
              onPress={() => {
                this.checkFunc(item)
                this.forceUpdate()
              }
              } />
          } />

      </View>
    )
  }

}
