import React from 'react';
import { View, StyleSheet,TouchableHighlight, Dimensions, ActivityIndicator, Alert, Image } from 'react-native';
import { Button, Text, Input, Icon, Card, ListItem, BottomSheet, Overlay, Header } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { ScrollView } from 'react-native-gesture-handler';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { NavigationEvents } from 'react-navigation';
import { styles } from '../Componment/PDAStyles';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import CheckBoxScan from '../Componment/CheckBoxScan';
import AvatarImg from '../Componment/AvatarImg';
import DeviceStorage from '../../Url/DeviceStorage';
import ListItemScan_child from '../Componment/ListItemScan_child';
import DatePicker from 'react-native-datepicker'
import ScanButton from '../Componment/ScanButton';

class BackIcon extends React.PureComponent {
    render() {
        return (
            <TouchableCustom
                onPress={this.props.onPress} >
                <Icon
                    name='arrow-back'
                    color='#419FFF' />
            </TouchableCustom>
        )
    }
}

const { height, width } = Dimensions.get('window') //获取宽高

export default class MaterialInventory extends React.PureComponent {
    constructor() {
        super();
        this.state = {
            title: "",
            resData: [],
            rowguid: '',
            formNo: '', // 盘点单号
            inventoryType: '请选择', // 盘点类型
            room: '请选择', // 库房名称
            roomId: '', 
            inventoryDate: '', // 盘库日期
            accountCount: '0', // 账面数量
            inventoryCount: '0', // 实盘数量
            inventoryPerson: '', // 盘库人
            remark: '', // 备注,



            department: '请选择', // 部门
            departmentId: '', 
            shop: '请选择', // 车间
            shopId: '', 
            totalAmount: '0', // 金额
            tabulator: '', // 编制人

            materialInfos: [],
            //materialIds: '', // 已选材料id
            inventoryTypeList: [],
            departmentList: [], 
            shopList: [], 
            roomList: [], 

            // 选择确认


            //底栏控制
            bottomData: [],
            inventoryTypeVisible: false,
            departmentVisible: false,
            shopVisible: false,
            roomVisible: false,

            checked: true,

            focusIndex: -1,
            isLoading: false,

            //查询界面取消选择按钮
            isCheckPage: false
        }
    }

    componentDidMount() {
        const { navigation } = this.props;
        let guid = navigation.getParam('guid') || ''
        let pageType = navigation.getParam('pageType') || ''
        let title = navigation.getParam('title')
        this.setState({
            title: title
        })
        if (pageType == 'CheckPage') {
            let guid = navigation.getParam('guid') || ''
            this.checkResData(guid)
        } else {
            this.resData()
        }
    }

    componentWillUnmount() {

        this.setState({

        })
    }

    UpdateControl = () => {
    if (typeof this.props.navigation.getParam('QRid') != "undefined") {
      if (this.props.navigation.getParam('QRid') != "") {
        this.toast.show(Url.isLoadingView, 0)
      }
    }
        const { navigation } = this.props;
        const { } = this.state

        let mis = []
        mis = navigation.getParam('materialInfos') || []
        let pickingInfoId = navigation.getParam('pickingInfoId') || ""
        let state = navigation.getParam('state') || ""
        if (state == "requisitionCount") {
            let inventoryCount = navigation.getParam('inventoryCount') || "0"
            let profitAndLoss = navigation.getParam('profitAndLoss') || "0"
            let count2 = (Math.round((Number.parseFloat(this.state.inventoryCount) + Number.parseFloat(profitAndLoss)) * 10000) / 10000).toString()

            mis = this.state.materialInfos
            mis.map((item, index) => {
                if (item.materialId == pickingInfoId) {
                    item.inventoryCount = inventoryCount
                    return
                }
            })
            this.setState({
                inventoryCount: count2,
                materialInfos: mis
            })
        } else if (state == "addMaterial") {
            //this.state.materialInfos.map((item, index) => {
            //    mis.push(item)
            //})
            //this.setState({
            //    materialInfos: mis
            //})
        }
        this.props.navigation.setParams({
            state: "",
            materialInfos: []
        })
        
        this.forceUpdate()
    }

    //顶栏
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('title')
        }
    }

    resData = () => {
        let formData = new FormData();
        let data = {};
        data = {
            "action": "inventoryinit",
            "servicetype": "pdamaterial",
            "express": "6DFD1C9B",
            "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
            "data": { "factoryId": Url.PDAFid }
        }
        formData.append('jsonParam', JSON.stringify(data))
        //console.log("🚀 ~ file: MaterialProjectPicking.js ~ line 130 ~ SampleManage ~ formData:", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res.statusText);
            //console.log(res);
            return res.json();
        }).then(resData => {
            if (resData.status == '100') {
                
                //let dictionary = resData.result.defectTypes
                //console.log("MaterialProjectPicking ---- 145 ----" + JSON.stringify(resData.result))
                    this.setState({
                        //resData: resData.result,
                        rowguid: resData.result.rowguid,
                        formNo: resData.result.formNo,
                        inventoryTypeList: resData.result.inventoryType,
                        roomList: resData.result.room,
                        inventoryDate: resData.result.inventoryDate,
                        inventoryPerson: Url.PDAEmployeeName,
                        isLoading: false
                    })
            } else {
                Alert.alert("错误提示", resData.message, [{
                    text: '取消',
                    style: 'cancel'
                }, {
                    text: '返回上一级',
                    onPress: () => {
                        this.props.navigation.goBack()
                    }
                }])
            }

        }).catch((error) => {
            console.log("🚀 ~ file: MaterialProjectPicking.js ~ line 233 ~ MaterialProjectPicking ~ error:", error)
        });

    }

    checkResData = (guid) => {
        let formData = new FormData();
        let data = {};
        data = {
            "action": "inventoryquerydetail",
            "servicetype": "pdamaterial",
            "express": "6DFD1C9B",
            "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
            "data": {
                "factoryId": Url.PDAFid,
                "guid": guid
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: MaterialProjectPicking.js ~ line 130 ~ SampleManage ~ formData:", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res.statusText);
            //console.log(res);
            return res.json();
        }).then(resData => {
            if (resData.status == '100') {
                console.log("MaterialProjectPicking ---- 267 ----" + JSON.stringify(resData.result))
                if (resData.result.chkstatus == '在编') {
                    this.setState({
                        inventoryTypeList: resData.result.inventoryTypeList,
                        roomList: resData.result.roomList,
                    })
                }
                this.setState({
                    rowguid: resData.result.rowguid,
                    formNo: resData.result.formNo,
                    room: resData.result.room,
                    roomId: resData.result.roomId,
                    inventoryType: resData.result.inventoryType,
                    inventoryDate: resData.result.inventoryDate,
                    accountCount: resData.result.accountCount,
                    inventoryCount: resData.result.inventoryCount,
                    inventoryPerson: resData.result.inventoryPerson,
                    remark: resData.result.remark,
                    chkstatus: resData.result.chkstatus,
                    materialInfos: resData.result.materialInfo,
                    isCheckPage: resData.result.chkstatus == '在编' ? false : true,
                    isLoading: false
                })
            } else {
                Alert.alert("错误提示", resData.message, [{
                    text: '取消',
                    style: 'cancel'
                }, {
                    text: '返回上一级',
                    onPress: () => {
                        this.props.navigation.goBack()
                    }
                }])
            }

        }).catch((error) => {
            console.log("🚀 ~ file: MaterialProjectPicking.js ~ line 233 ~ MaterialProjectPicking ~ error:", error)
        });
    }

    //材料盘点账面库存
    inventoryBookData = () => {
        let formData = new FormData();
        let data = {};
        data = {
            "action": "inventorybook",
            "servicetype": "pdamaterial",
            "express": "6DFD1C9B",
            "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
            "data": {
                "factoryId": Url.PDAFid,
                "inventoryType": this.state.inventoryType == '非钢筋盘点' ? "1" : "2",
                "roomId": this.state.roomId,
                "inventoryDate": this.state.inventoryDate
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        //console.log("🚀 ~ file: MaterialProjectPicking.js ~ line 130 ~ SampleManage ~ formData:", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res.statusText);
            //console.log(res);
            return res.json();
        }).then(resData => {
            if (resData.status == '100') {
               
                //console.log("MaterialProjectPicking ---- 145 ----" + JSON.stringify(resData.result))
                let data = []
                let count = '0'
                if (resData.result != []) {
                    data = resData.result
                    data.map((item, index) => {
                        item.inventoryCount = item.stockCount
                        count = (Number.parseFloat(count) + Number.parseFloat(item.stockCount)).toString()
                    })
                }
                let str = []
                str = count.split('.')
                if (str.length == 2 && str[1].length > 4) {
                    count = str[0] + '.' + str[1].substring(0,4)
                }
                this.setState({
                    //resData: resData.result,
                    materialInfos: data,
                    accountCount: count,
                    inventoryCount: count
                })
            } else {
                Alert.alert("错误提示", resData.message)
            }

        }).catch((error) => {
            console.log("🚀 ~ file: MaterialProjectPicking.js ~ line 233 ~ MaterialProjectPicking ~ error:", error)
        });

    }

    PostData = () => { }

    // 时间工具
    _DatePicker = (index) => {
        //const { ServerTime } = this.state
        return (
            <DatePicker
                customStyles={{
                    dateInput: styles.dateInput,
                    dateTouchBody: styles.dateTouchBody,
                    dateText: styles.dateText,
                }}
                iconComponent={<Icon name='caretdown' color='#419fff' iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
                }
                showIcon={true}
                date={this.state.inventoryDate}
                onOpenModal={() => {
                    this.setState({ focusIndex: index })
                }}
                format="YYYY-MM-DD"
                mode="date"
                onDateChange={(value) => {
                    this.setState({
                        inventoryDate: value
                    })
                }}
            />
        )
    }

    // 下拉菜单
    downItem = (index) => {
        const { inventoryType, inventoryTypeList, shop, shopList, department, departmentList, room, roomList } = this.state
        switch (index) {
            case '1': return (
                // 盘点类型
                <View>{(
                    <TouchableCustom onPress={() => { this.isItemVisible(inventoryTypeList, index) }} >
                        <IconDown text={inventoryType} />
                    </TouchableCustom>
                )}</View>
            ); break;
            case '2': return (
                // 库房名称
                <View>{(
                    <TouchableCustom onPress={() => { this.isItemVisible(roomList, index) }} >
                        <IconDown text={room} />
                    </TouchableCustom>
                )}</View>
            ); break;
            default: return;
        }
    }

    isItemVisible = (data, focusIndex) => {
        //console.log("************** " + focusIndex + "-" + i)
        if (typeof (data) != "undefined") {
            if (data != [] && data != "") {
                switch (focusIndex) {
                    case '1': this.setState({ inventoryTypeVisible: true, bottomData: data, focusIndex: focusIndex }); break;
                    case '2': this.setState({ roomVisible: true, bottomData: data, focusIndex: focusIndex }); break;
                    default: return;
                }
            } else {
                this.toast.show("无数据")
            }
        } else {
            this.toast.show("无数据")
        }
    }


    render() {
        const { navigation } = this.props;
        //从主页面传url, 未来集成到一起
        const QRurl = navigation.getParam('QRurl') || '';
        const QRid = navigation.getParam('QRid') || ''
        const type = navigation.getParam('type') || '';
        let pageType = navigation.getParam('pageType') || ''

        if (type == 'compid') {
            QRcompid = QRid
        }
        const { title, formNo, isCheckPage, focusIndex, inventoryTypeList, inventoryType, inventoryTypeVisible, inventoryDate, bottomData, shop, shopId, shopList, team, tabulationDate, tabulator, supplyUse, remark, materialInfos, department, departmentId, departmentList, shopVisible, departmentVisible, room, roomList, roomVisible, accountCount, inventoryCount, inventoryPerson } = this.state

        if (this.state.isLoading) {
            return (
                <View style={styles.loading}>
                    <ActivityIndicator
                        animating={true}
                        color='#419FFF'
                        size="large" />
                </View>
            )
            //渲染页面
        } else {
            return (
                <View style={{ flex: 1 }}>
                    <Toast ref={(ref) => { this.toast = ref; }} position="center" />
                    <NavigationEvents
                        onWillFocus={this.UpdateControl}
                        onWillBlur={() => {
                            if (QRid != '') {
                                navigation.setParams({ QRid: '', type: '' })
                            }
                        }} />
                    <ScrollView ref={ref => this.scoll = ref} style={styles.mainView}showsVerticalScrollIndicator={false} >
                        <View style={styles.listView}>
                            <ListItemScan focusStyle={focusIndex == '0' ? styles.focusColor : {}} title='盘点编号:' rightElement={formNo} />
                            {
                                isCheckPage ? <ListItemScan focusStyle={focusIndex == '1' ? styles.focusColor : {}} title='盘点类型:' rightElement={inventoryType} /> :
                                    <TouchableCustom underlayColor={'lightgray'} onPress={() => { this.isItemVisible(inventoryTypeList, '1') }}>
                                        <ListItemScan focusStyle={focusIndex == '1' ? styles.focusColor : {}} title='盘点类型:' rightElement={this.downItem('1')} />
                                    </TouchableCustom>
                            }
                            {
                                inventoryType == '非钢筋盘点' ?
                                    isCheckPage ? <ListItemScan focusStyle={focusIndex == '2' ? styles.focusColor : {}} title='库房名称:' rightElement={room} /> :
                                        <TouchableCustom underlayColor={'lightgray'} onPress={() => { this.isItemVisible(roomList, '2') }}>
                                            <ListItemScan focusStyle={focusIndex == '2' ? styles.focusColor : {}} title='库房名称:' rightElement={this.downItem('2')} />
                                        </TouchableCustom>
                                    : <View />
                            }
                            <ListItemScan focusStyle={focusIndex == '3' ? styles.focusColor : {}} title='盘库日期:' rightElement={isCheckPage ? inventoryDate : this._DatePicker(3)} />
                            {
                                isCheckPage ? <View /> :
                                    <View style={{ flexDirection: 'row', paddingHorizontal: 10, justifyContent: 'center', paddingVertical: 11 }}>
                                        <Button
                                            titleStyle={{ fontSize: 14 }}
                                            title='账面库存'
                                            //iconRight={true}
                                            //icon={<Icon type='antdesign' name='scan1' color='white' size={14} />}
                                            type='solid'
                                            buttonStyle={{ paddingVertical: 6, backgroundColor: '#17BC29', marginVertical: 0 }}
                                            onPress={() => {
                                                if (inventoryType == '请选择') {
                                                    this.toast.show('请选择盘点类型')
                                                    return
                                                } else if (room == '请选择' && inventoryType == '非钢筋盘点') {
                                                    this.toast.show('请选择库房名称')
                                                    return
                                                }
                                                this.inventoryBookData()
                                            }}
                                        />
                                        <Text>   </Text>
                                        {
                                            <Button
                                                titleStyle={{ fontSize: 14 }}
                                                title={inventoryType == '非钢筋盘点' ? '选择材料' : '损耗计算'}
                                                //iconRight={true}
                                                //icon={<Icon type='antdesign' name='scan1' color='white' size={14} />}
                                                type='solid'
                                                buttonStyle={{ paddingVertical: 6, backgroundColor: '#17BC29', marginVertical: 0 }}
                                                onPress={() => {
                                                    if (inventoryType == '请选择') {
                                                        this.toast.show('请选择盘点类型')
                                                        return
                                                    } else if (room == '请选择' && inventoryType == '非钢筋盘点') {
                                                        this.toast.show('请选择库房名称')
                                                        return
                                                    }

                                                    //let materialIds = ''
                                                    //materialInfos.map((item, index) => {
                                                    //    materialIds += index == materialInfos.length - 1 ? item.id : item.id + ','
                                                    //})
                                                    //this.props.navigation.navigate('SelectMaterial', {
                                                    //    title: '选择材料',
                                                    //    action: title,
                                                    //    returnType: returnType,
                                                    //    materialIds: materialIds,
                                                    //})
                                                }}
                                            />
                                        }
                                        <Text>   </Text>
                                        <Icon type='material-community' name='qrcode-scan' color='#4D8EF5' style={{ marginTop: 4 }} />
                                    </View>
                            }
                            
                            {
                                materialInfos.map((item, index) => {
                                    return (
                                        <TouchableCustom onLongPress={() => {
                                            !isCheckPage ?
                                                Alert.alert(
                                                    '提示信息',
                                                    '是否删除材料信息',
                                                    [{
                                                        text: '取消',
                                                        style: 'cancel'
                                                    }, {
                                                        text: '删除',
                                                        onPress: () => {
                                                            let mis = this.state.materialInfos
                                                            let count1 = (Math.round((Number.parseFloat(accountCount) - Number.parseFloat(item.stockCount)) * 10000) / 10000).toString()
                                                            let count2 = (Math.round((Number.parseFloat(inventoryCount) - Number.parseFloat(item.inventoryCount)) * 10000) / 10000).toString()
                                                            mis.splice(index, 1)
                                                            this.setState({
                                                                materialInfos: mis,
                                                                accountCount: count1,
                                                                inventoryCount: count2
                                                            })
                                                            this.forceUpdate()
                                                        }
                                                    }]) : ""
                                        }} >
                                            <View>
                                                <Card containerStyle={stylesMeterial.card_containerStyle}>
                                                    <View style={[{
                                                        flexDirection: 'row',
                                                        justifyContent: 'flex-start',
                                                        alignItems: 'center',
                                                    }]}>
                                                        <View style={{ flex: 1 }}>
                                                            <ListItem
                                                                containerStyle={stylesMeterial.list_container_style}
                                                                title={item.code}
                                                                titleStyle={stylesMeterial.title}
                                                                rightTitle={'账面数量:' + item.stockCount}
                                                                rightTitleStyle={stylesMeterial.rightTitle}
                                                            />
                                                            <ListItem
                                                                containerStyle={stylesMeterial.list_container_style}
                                                                title={item.name}
                                                                rightTitle={'盘存数量:' + item.inventoryCount}
                                                                titleStyle={stylesMeterial.title}
                                                                rightTitleStyle={stylesMeterial.rightTitle}
                                                            />
                                                            <ListItem
                                                                containerStyle={stylesMeterial.list_container_style}
                                                                title={item.size || ' '}
                                                                rightTitle={'盘盈/盘亏:' + (Math.round((Number.parseFloat(item.inventoryCount) - Number.parseFloat(item.stockCount)) * 10000) / 10000).toString()}
                                                                titleStyle={stylesMeterial.title}
                                                                rightTitleStyle={stylesMeterial.rightTitle}
                                                            />
                                                            <ListItem
                                                                containerStyle={stylesMeterial.list_container_style}
                                                                title={item.unit}
                                                                //rightTitle={'是否暂估:  是'}
                                                                titleStyle={stylesMeterial.title}
                                                                rightTitleStyle={stylesMeterial.rightTitle}
                                                            />
                                                        </View>
                                                        {
                                                            isCheckPage ? <View /> :
                                                                <Icon type='antdesign' name='right' color='#999' onPress={() => {
                                                                    this.props.navigation.navigate('MaterialPickingInfo', {
                                                                        title: '盘点信息',
                                                                        action: '材料盘点',
                                                                        storageId: item.materialId,
                                                                        storageCode: item.code,
                                                                        storageName: item.name,
                                                                        size: item.size,
                                                                        unit: item.unit,
                                                                        stockCount: item.stockCount,
                                                                        inventoryCount: item.inventoryCount,
                                                                        profitAndLoss: (Math.round((Number.parseFloat(item.inventoryCount) - Number.parseFloat(item.stockCount)) * 10000) / 10000).toString()
                                                                    })
                                                                }} />
                                                        }
                                                    </View>
                                                </Card>
                                                <Text />
                                            </View>
                                        </TouchableCustom>
                                    )
                                })
                            }

                            <ListItemScan focusStyle={focusIndex == '4' ? styles.focusColor : {}} title='账面数量:' rightElement={accountCount} />
                            <ListItemScan focusStyle={focusIndex == '5' ? styles.focusColor : {}} title='实盘数量:' rightElement={inventoryCount} />
                            <ListItemScan focusStyle={focusIndex == '6' ? styles.focusColor : {}} title='盘库人:' rightElement={inventoryPerson} />
                            <ListItemScan focusStyle={focusIndex == '7' ? styles.focusColor : {}} title='备注:' rightElement={isCheckPage ? <Text>{remark}</Text> :
                                <View>
                                    <Input
                                        containerStyle={styles.quality_input_container}
                                        inputContainerStyle={styles.inputContainerStyle}
                                        inputStyle={[styles.quality_input_, { top: 7 }]}
                                        placeholder='请填写'
                                        value={remark}
                                        onChangeText={(value) => {
                                            this.setState({
                                                remark: value
                                            })
                                        }} />
                                </View>
                            } />

                        </View>
                        
                        {
                            isCheckPage ? <Text /> :
                                <View style={{ flexDirection: 'row' }}>
                                    <View style={{ flex: 1 }}><Button
                                        title='暂存'
                                        titleStyle={{ color: "white", fontSize: 17 }}
                                        type='solid'
                                        style={{ marginVertical: 10 }}
                                        containerStyle={{ marginVertical: 10, marginHorizontal: 8 }}
                                        buttonStyle={{ backgroundColor: '#17BC29', borderRadius: 10, paddingVertical: 16 }}
                                        onPress={() => {
                                            
                                        }}
                                    ></Button></View>
                                    <View style={{ flex: 1 }}><Button
                                        title='报审'
                                        titleStyle={{ color: "white", fontSize: 17 }}
                                        type='solid'
                                        style={{ marginVertical: 10 }}
                                        containerStyle={{ marginVertical: 10, marginHorizontal: 8 }}
                                        buttonStyle={{ backgroundColor: '#FF9E00', borderRadius: 10, paddingVertical: 16 }}
                                        onPress={() => {  }}
                                    ></Button></View>
                                </View>
                        }

                        {/*盘点类型底栏*/}
                        <BottomSheet
                            onBackdropPress={() => {
                                this.setState({
                                    inventoryTypeVisible: false
                                })
                            }}
                            isVisible={inventoryTypeVisible && !this.state.isCheckPage} onRequestClose={() => {
                                this.setState({
                                    inventoryTypeVisible: false
                                })
                            }}
                        >
                            <ScrollView style={styles.bottomsheetScroll}>
                                {bottomData.map((item, index) => {
                                    return (
                                        <TouchableCustom
                                            onPress={() => {
                                                this.setState({
                                                    inventoryType: item,
                                                    inventoryTypeVisible: false,
                                                    materialInfos: []
                                                })
                                            }}
                                        >
                                            <BottomItem backgroundColor='white' color="#333" title={item} />
                                        </TouchableCustom>
                                    )
                                })}
                            </ScrollView>
                            <TouchableHighlight
                                onPress={() => {
                                    this.setState({ inventoryTypeVisible: false })
                                }}>
                                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
                            </TouchableHighlight>
                        </BottomSheet>
                        {/*库房名称底栏*/}
                        <BottomSheet
                            onBackdropPress={() => {
                                this.setState({
                                    roomVisible: false
                                })
                            }}
                            isVisible={roomVisible && !this.state.isCheckPage} onRequestClose={() => {
                                this.setState({
                                    roomVisible: false
                                })
                            }}
                        >
                            <ScrollView style={styles.bottomsheetScroll}>
                                {bottomData.map((item, index) => {
                                    return (
                                        <TouchableCustom
                                            onPress={() => {
                                                this.setState({
                                                    room: item.name,
                                                    roomId: item.id,
                                                    roomVisible: false,
                                                })
                                            }}
                                        >
                                            <View style={{
                                                paddingVertical: 18,
                                                backgroundColor: 'white',
                                                //alignItems: 'center',
                                                borderBottomColor: '#DDDDDD',
                                                borderBottomWidth: 0.6,
                                                flexDirection: 'row',
                                                justifyContent: 'space-between',
                                                paddingLeft: 20,
                                                paddingRight: 20
                                            }}>
                                                <Text style={{ color: '#333', fontSize: 14 }}>{item.name}</Text>
                                                <Text style={{ color: '#333', fontSize: 14 }}>{item.code}</Text>
                                            </View>
                                        </TouchableCustom>
                                    )
                                })}
                            </ScrollView>
                            <TouchableHighlight
                                onPress={() => {
                                    this.setState({ roomVisible: false })
                                }}>
                                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
                            </TouchableHighlight>
                        </BottomSheet>
                    </ScrollView>
                </View>
            )
        }
    }
}

const stylesMeterial = StyleSheet.create({
    card_containerStyle: {
        borderRadius: 10,
        shadowOpacity: 0,
        backgroundColor: '#f8f8f8',
        borderWidth: 0,
        paddingVertical: 13,
        elevation: 0
    },
    title: {
        fontSize: 13,
        color: '#999'
    },
    rightTitle: {
        fontSize: 13,
        textAlign: 'right',
        lineHeight: 14,
        flexWrap: 'wrap',
        width: width / 2,
        color: '#333'
    },
    list_container_style: { marginVertical: 0, paddingVertical: 9, backgroundColor: 'transparent' }

})