/*
 * @Author: 高睿豪 1375308739@qq.com
 * @Date: 2021-11-03 15:52:18
 * @LastEditors: 高睿豪 1375308739@qq.com
 * @LastEditTime: 2024-02-20 14:57:06
 * @FilePath: /SmartFactory/App/PDA/ChildPage/Web.js
 * @Description: 
 * 
 * Copyright (c) 2024 by ${git_name_email}, All Rights Reserved. 
 */
import React from 'react'
import { View, Text, Dimensions } from 'react-native'
import { Icon } from "react-native-elements";
import { WebView } from 'react-native-webview';
import { ScrollView, TouchableHighlight, TouchableOpacity } from 'react-native-gesture-handler';
import { RFT } from '../../Url/Pixal';

const { height, width } = Dimensions.get('window')

export default class webView extends React.Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title'),
    };
  };
  render() {
    let url = this.props.navigation.getParam('url')  //"http://smart.pkpm.cn:9100/PCIS/Production/ShowCompInfo.aspx?compid=E45E65EB439549C5842DAFBCE55EF63B"
    //let url =  "http://smart.pkpm.cn:9100/PCIS/Production/ShowCompInfo.aspx?compid=E45E65EB439549C5842DAFBCE55EF63B"
    this.props.navigation.goBack('PDAMainStack')
    console.log("render -> url", url)
    return (
      <ScrollView scrollEnabled={false}  overScrollMode = 'never'>
        <View style={{ height: height * 0.92 }}>
          <WebView
            source={{ uri: url }}
          ></WebView>
        </View>
      </ScrollView>
    )
  }
}