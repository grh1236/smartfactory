import React from 'react';
import { View, StyleSheet, FlatList, Dimensions, ActivityIndicator, Picker, TextInput } from 'react-native';
import { Button, Text, Card, Icon, Image, ListItem, BottomSheet, Overlay, Header, SearchBar } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { NavigationEvents } from 'react-navigation';
import { styles } from '../Componment/PDAStyles';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import { deviceWidth } from '../../Url/Pixal';
import ModalDropdown from 'react-native-modal-dropdown';

class BackIcon extends React.PureComponent {
    render() {
        return (
            <TouchableCustom
                onPress={this.props.onPress} >
                <Icon
                    name='arrow-back'
                    color='#419FFF' />
            </TouchableCustom>
        )
    }
}

const { height, width } = Dimensions.get('window') //获取宽高
let listData = []
let pageNum = 1
let action = ""
let returnType = ""

export default class MaterialStock extends React.PureComponent {
    constructor() {
        super();
        this.state = {
            action: '',
            resData: [],

            searchValue: '',
            selectType: '材料编码',
            searchType: 'materialCode',

            roomId: '',

            focusIndex: -1,
            isLoading: false,

            //查询界面取消选择按钮
            isCheckPage: false,
            isScan: false,
            isChecked: false,

            dataCount: 0,
            focusButton: 0,
            resDataButton: []
        }
    }

    componentDidMount() {
        const { navigation } = this.props;
        let guid = navigation.getParam('guid') || ''
        let pageType = navigation.getParam('pageType') || ''
        let roomId = ''
        let projectId = ''
        let title = navigation.getParam('title') || ''

        if (pageType == 'CheckPage') {

        } else {
            this.resData("", "")
            this.setState({

            })
        }
    }

    componentWillUnmount() {
        listData = []
        pageNum = 1
        action = ""
        returnType = ""
        this.setState({

        })
    }


    //顶栏
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('title')
        }
    }

    resData = (searchType, searchValue) => {
        let formData = new FormData();
        let data = {};
        data = {
            "action": "stockquery",
            "servicetype": "pdamaterial",
            "express": "6DFD1C9B",
            "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
            "data": {
                "factoryId": Url.PDAFid,
                "searchType": searchType,
                "keyStr": searchValue,
                "pageNum": "1",
                "viewRowCt": "10"
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: SelectMaterial.js ~ line 130 ~ SampleManage ~ formData:", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res.statusText);
            //console.log(res);
            return res.json();
        }).then(resData => {
            if (resData.status == '100') {
                console.log("SelectMaterial ---- 146 ----" + JSON.stringify(resData.result))
                let res = resData.result
                res.map((item, index) => {
                    item.requisitionCount = "0",
                        item.select = false
                })
                listData = res
                this.setState({
                    resData: res,
                    isLoading: false
                })
                this.forceUpdate()
            } else {
                Alert.alert("错误提示", resData.message, [{
                    text: '取消',
                    style: 'cancel'
                }, {
                    text: '返回上一级',
                    onPress: () => {
                        this.props.navigation.goBack()
                    }
                }])
            }

        }).catch((error) => {
            console.log("🚀 ~ file: SelectMaterial.js ~ line 184 ~ SelectMaterial ~ error:", error)
        });

    }

    UpdateControl = () => {
        const { navigation } = this.props;
        const { } = this.state

        let type = navigation.getParam('type') || '';
        if (type == 'compid') {
            let QRid = navigation.getParam('QRid') || ''
            this.scanMaterial(QRid)
        }
    }

    PostData = () => { }

    // 分类选择
    _selectType = (index, value) => {
        //console.log(index + '--' + value)
        const { roomId, projectId, materialIds, searchType, searchValue } = this.state;
        let type = ""
        switch (value) {
            case "材料编码": type = "materialCode"; break;
            case "材料名称": type = "storageName"; break;
            case "库房名称": type = "roomName"; break;
            default: type = "";
        }
        this.setState({
            searchType: type,
            selectType: value,
            isScan: false
        })
        this.resData(roomId, projectId, materialIds, type, searchValue)
    }
    // 下拉列表分隔符
    _separator = () => {
        return (
            <Text style={{ height: 0 }}></Text>
        )
    }

    renderSearchBar = () => {
        const { selectType, roomId, projectId, materialIds } = this.state;
        let type = ['材料编码', '材料名称', '库房名称']
        return (
            <View style={{
                flexDirection: 'row',
                alignItems: 'center',
                paddingTop: 10,
                paddingBottom: 10,
                backgroundColor: '#fff',
                borderBottomWidth: 1,
                borderBottomColor: '#efefef',
            }}>
                <View style={{ flex: 1 }}>
                    <View style={stylesSelectStock.searchContainerStyle}>

                        <View style={{ flexDirection: 'row', backgroundColor: '#f9f9f9', width: deviceWidth * 0.94, marginLeft: deviceWidth * 0.03, borderRadius: 45 }}>
                            <Text>   </Text>
                            <ModalDropdown
                                options={type}    //下拉内容数组
                                style={{ marginTop: 12 }}    //按钮样式
                                dropdownStyle={{ height: 36 * 3, width: 80 }}    //下拉框样式
                                dropdownTextStyle={{ fontSize: 13 }}    //下拉框文本样式
                                renderSeparator={this._separator}    //下拉框文本分隔样式
                                adjustFrame={this._adjustType}    //下拉框位置
                                dropdownTextHighlightStyle={{ color: 'rgba(42, 130, 228, 1)' }}    //下拉框选中颜色
                                onDropdownWillShow={() => { this.setState({ typeShow: true }) }}      //按下按钮显示按钮时触发
                                onDropdownWillHide={() => this.setState({ typeShow: false })}    //当下拉按钮通过触摸按钮隐藏时触发
                                onSelect={this._selectType}    //当选项行与选定的index 和 value 接触时触发
                                defaultValue={'材料编码'}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text>{this.state.selectType}</Text>
                                    <Icon name='caretdown' color='#419fff' iconStyle={{ fontSize: 13, marginTop: 1, marginLeft: 8, marginRight: -1 }} type='antdesign' ></Icon>
                                </View>
                            </ModalDropdown>

                            <Icon type='material-community' name='power-on' color='#999' style={{ marginTop: 10 }} />
                            <Icon name='search' color='#999' type='material' iconStyle={{ fontSize: 22, marginTop: 12 }}></Icon>

                            <TextInput style={stylesSelectStock.inputText}
                                keyboardType='web-search'
                                placeholder='搜索关键词'
                                onChangeText={(value) => {
                                    let type = ""
                                    switch (selectType) {
                                        case "材料编码": type = "materialCode"; break;
                                        case "材料名称": type = "storageName"; break;
                                        case "库房名称": type = "roomName"; break;
                                        default: type = ""; break;
                                    }
                                    this.setState({
                                        searchType: type,
                                        searchValue: value,
                                        dataCount: 0,
                                        isScan: false
                                    })
                                    this.resData(type, value)
                                }}
                            />

                            <Icon type='material-community' name='qrcode-scan' color='#4D8EF5' iconStyle={{ marginTop: 12 }} onPress={() => {
                                this.props.navigation.navigate('QRCode', {
                                    type: 'compid',
                                    page: 'MaterialStock'
                                })
                            }} />
                            {/*<Icon type='material-community' name='microphone' color='#4D8EF5' style={{ marginTop: 12 }} />*/}
                            <Text>   </Text>
                        </View>



                    </View>
                </View>
            </View>
        );
    };

    ButtonTopScreen = () => {
        const { focusButton, resDataButton } = this.state;

        return (
            resDataButton.map((item, index) => {
                if (true) {
                    return (
                        <Button
                            //key={index}
                            buttonStyle={{
                                borderRadius: 20,
                                flex: 1,
                                backgroundColor: focusButton == index ? '#419FFF' : 'white',
                                width: 100
                            }}
                            containerStyle={{
                                marginHorizontal: 6,
                                borderRadius: 20,
                                marginTop: 20,
                            }}
                            title={item.name}
                            titleStyle={{ fontSize: 15, color: focusButton == index ? 'white' : '#419FFF' }}
                            //titleStyle={{ fontSize: 15, color: '#999' }}
                            //type='outline'
                            onPress={() => {
                                this.setState({
                                    focusButton: index,
                                });
                            }}
                        />
                    )
                }
            })
        )
    }


    render() {
        const { navigation } = this.props;
        //从主页面传url, 未来集成到一起
        const QRurl = navigation.getParam('QRurl') || '';
        const QRid = navigation.getParam('QRid') || ''
        const type = navigation.getParam('type') || '';
        let pageType = navigation.getParam('pageType') || ''

        if (type == 'compid') {
            QRcompid = QRid
        }
        const { isCheckPage, focusIndex, resData, isChecked, dataCount } = this.state

        if (this.state.isLoading) {
            return (
                <View style={styles.loading}>
                    <ActivityIndicator
                        animating={true}
                        color='#419FFF'
                        size="large" />
                </View>
            )
            //渲染页面
        } else {
            return (
                <View style={{ flex: 1, backgroundColor: '#f8f8f8' }}>
                    <Toast ref={(ref) => { this.toast = ref; }} position="center" />
                    <NavigationEvents
                        onWillFocus={this.UpdateControl}
                        onWillBlur={() => {
                            if (QRid != '') {
                                navigation.setParams({ QRid: '', type: '' })
                            }
                        }} />

                    {
                        this.renderSearchBar()
                    }

                    <FlatList
                        style={{ flex: 1, borderRadius: 10, backgroundColor: '#f8f8f8' }}
                        ref={(flatList) => this._flatList1 = flatList}
                        //ItemSeparatorComponent={this._separator}
                        renderItem={this._renderItem}
                        onRefresh={this.refreshing}
                        refreshing={false}
                        //控制上拉加载，两个平台需要区分开
                        onEndReachedThreshold={(Platform.OS === 'ios') ? -0.3 : 0.1}
                        onEndReached={() => {
                            this._onload()
                        }
                        }
                        numColumns={1}
                        //ListFooterComponent={this._footer}
                        //columnWrapperStyle={{borderWidth:2,borderColor:'black',paddingLeft:20}}
                        //horizontal={true}
                        data={listData}
                        scrollEnabled={true}
                        extraData={this.state}
                    />

                    <View style={{
                        marginHorizontal: width * 0.06,
                        marginTop: width * 0.04,
                        backgroundColor: 'white',
                        borderRadius: 10,
                        flexDirection: 'row',
                        justifyContent: 'flex-start',
                        alignItems: 'center',
                    }}>
                        {/*<CheckBoxScan*/}
                        {/*    //title='全选'*/}
                        {/*    checked={isChecked}*/}
                        {/*    onPress={() => {*/}
                        {/*        listData.map((item, index) => { item.select = !isChecked })*/}
                        {/*        this.setState({*/}
                        {/*            dataCount: !isChecked ? listData.length : 0,*/}
                        {/*            isChecked: !isChecked*/}
                        {/*        })*/}
                        {/*    }}*/}
                        {/*/>*/}
                        {/*<View style={{ flex: 1 }}>*/}
                        {/*    <ListItemScan focusStyle={focusIndex == '3' ? styles.focusColor : {}} title={"全选"} rightElement={"共" + dataCount + "条"} />*/}
                        {/*</View>*/}
                    </View>

                    {/*<FormButton*/}
                    {/*    backgroundColor='#17BC29'*/}
                    {/*    onPress={() => {*/}
                    {/*        let res = []*/}
                    {/*        listData.map((item, idex) => {*/}
                    {/*            if (item.select) {*/}
                    {/*                res.push(item)*/}
                    {/*            }*/}
                    {/*        })*/}
                    {/*        let navAction = ''*/}
                    {/*        if (returnType == '') navAction = 'MaterialProjectPicking'*/}
                    {/*        else navAction = 'MaterialOutStorageReturn'*/}
                    {/*        this.props.navigation.navigate(navAction, {*/}
                    {/*            state: "addMaterial",*/}
                    {/*            materialInfos: res*/}
                    {/*        })*/}
                    {/*    }}*/}
                    {/*    title='确定'*/}
                    {/*/>*/}

                </View>
            )
        }
    }

    _onload = (user) => {
        const { search, roomId, projectId, materialIds, searchType, searchValue, isScan } = this.state
        if (isScan) return
        let strhazy = search
        this.toast.show('加载中...', 10000)
        pageNum += 1

        let formData = new FormData();
        let data = {};
        data = {
            "action": "stockquery",
            "servicetype": "pdamaterial",
            "express": "6DFD1C9B",
            "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
            "data": {
                "factoryId": Url.PDAFid,
                "searchType": searchType,
                "keyStr": searchValue,
                "pageNum": pageNum,
                "viewRowCt": "10"
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        //console.log("🚀 ~ file: SelectMaterial.js ~ line 130 ~ SampleManage ~ formData:", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res.statusText);
            //console.log(res);
            return res.json();
        }).then(resData => {
            if (resData.status == '100') {
                //console.log("SelectMaterial ---- 146 ----" + JSON.stringify(resData.result))
                let res = resData.result
                res.map((item, index) => {
                    item.requisitionCount = "0",
                        item.select = false
                })
                listData.push.apply(listData, res)
                //listData = res
                this.setState({
                    resData: listData,
                    isLoading: false
                })
                if (res.length > 0 || res.length > 0) {
                    this.toast.show('加载完成');
                } else {
                    this.toast.show('无更多数据');
                }
            } else if (resData.status != '100') {
                Alert.alert('错误', resData.message)
            } else {
                this.toast.show('加载失败')
            }

        }).catch((error) => {
            console.log("🚀 ~ file: MaterialStock.js ~ line 471 ~ MaterialStock ~ error:", error)
        });
    }

    refreshing = () => {
        this.toast.show('刷新中...', 10000)
        this.setState({
            pageLoading: true,
            isScan: false
        })
        const { roomId, projectId, materialIds, searchType, searchValue } = this.state
        let formData = new FormData();
        let data = {};
        data = {
            "action": "stockquery",
            "servicetype": "pdamaterial",
            "express": "6DFD1C9B",
            "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
            "data": {
                "factoryId": Url.PDAFid,
                "searchType": searchType,
                "keyStr": searchValue,
                "pageNum": "1",
                "viewRowCt": "10"
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        //console.log("🚀 ~ file: SelectMaterial.js ~ line 130 ~ SampleManage ~ formData:", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res.statusText);
            //console.log(res);
            return res.json();
        }).then(resData => {
            if (resData.status == '100') {
                //console.log("SelectMaterial ---- 146 ----" + JSON.stringify(resData.result))
                let res = resData.result
                res.map((item, index) => {
                    item.requisitionCount = "0",
                        item.select = false
                })
                //listData.push.apply(listData, res)
                listData = res
                this.setState({
                    resData: listData,
                    isLoading: false
                })
            } else {
                this.toast.show('刷新失败', 1600)
            }

        }).catch((error) => {
            console.log("🚀 ~ file: SelectMaterial.js ~ line 184 ~ SelectMaterial ~ error:", error)
        });
    }

    jumpPageInfo = (item) => {
        this.props.navigation.navigate('MaterialPickingInfo', {
            title: '材料信息',
            action: '库存查询',
            storageCode: item.FormCode,
            storageName: item.EditName,
            size: item.ProjectName,
            unit: item.ConcreteType,
            count: item.EditDate,
            storageId: item._Rowguid,

            //unitPrice: item.unitPrice,
            //accout: item.accout
        })
    }

    _renderItem = ({ item, index }) => {
        const { focusIndex, dataCount, action } = this.state
        let count = dataCount
        return (
            <Card containerStyle={{ borderRadius: 10, elevation: 0 }}>
                <TouchableCustom onPress={() => { this.jumpPageInfo(item) }}>
                    <View style={[{
                        flexDirection: 'row',
                        justifyContent: 'flex-start',
                        alignItems: 'center'
                    }]}>
                        <View style={{ flex: 1 }}>
                            <ListItem
                                containerStyle={stylesSelectStock.list_container_style}
                                title={item.FormCode}
                                rightElement={item.EditName}
                                titleStyle={stylesSelectStock.title}
                                rightTitleStyle={stylesSelectStock.rightTitle}
                            />
                            <ListItem
                                containerStyle={stylesSelectStock.list_container_style}
                                title={item.ProjectName}
                                rightElement={item.ConcreteType}
                                titleStyle={stylesSelectStock.title}
                                rightTitleStyle={stylesSelectStock.rightTitle}
                            />
                            <ListItem
                                containerStyle={stylesSelectStock.list_container_style}
                                title={"库存: " + item.EditDate}
                                //rightTitle={'单价:' + item.unitPrice + '元'}
                                rightElement={
                                    <View style={{
                                        backgroundColor:
                                            item.ConcreteGrade == '补货' ? '#F27070' :
                                                item.ConcreteGrade == '警告' ? '#DAC512' :
                                                    item.ConcreteGrade == '正常' ? '#31C731' : 'transparent'
                                    }}>
                                        <Text style={{ color: 'white', fontSize: 13 }}> {item.ConcreteGrade} </Text>
                                    </View>
                                }
                                titleStyle={stylesSelectStock.title}
                                rightTitleStyle={stylesSelectStock.rightTitle}
                            />
                        </View>
                        <Icon type='antdesign' name='right' color='#999' onPress={() => { this.jumpPageInfo(item) }} />
                    </View>
                </TouchableCustom>
            </Card>
        )
    }

    scanMaterial = (guid) => {
        if (typeof this.props.navigation.getParam('QRid') != "undefined") {
            if (this.props.navigation.getParam('QRid') != "") {
                this.toast.show(Url.isLoadingView, 0)
            }
        }
        let formData = new FormData();
        let data = {};
        data = {
            "action": "stockscan",
            "servicetype": "pdamaterial",
            "express": "6DFD1C9B",
            "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
            "data": {
                "factoryId": Url.PDAFid,
                "rowguid": guid,
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: MaterialStock.js ~ line 612 ~ MaterialStock ~ formData:", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res.statusText);
            //console.log(res);
            return res.json();
        }).then(resData => {
            this.toast.close()
            if (resData.status == '100') {
                console.log("MaterialStock ---- 267 ----" + JSON.stringify(resData.result))
                listData = resData.result
                this.setState({
                    resData: resData.result,
                    isScan: true
                })
            } else {
                Alert.alert("错误提示", resData.message)
            }

        }).catch((err) => {
            this.toast.show("扫码错误")
            if (err.toString().indexOf('not valid JSON') != -1) {
                Alert.alert('扫码失败', "响应内容不是合法JSON格式")
                return
            }
            if (err.toString().indexOf('Network request faile') != -1) {
                Alert.alert('扫码失败', "网络请求错误")
                return
            }
            Alert.alert('扫码失败', err.toString())
        });
    }
}

const stylesSelectStock = StyleSheet.create({
    searchContainerStyle: {
        shadowColor: '#999', shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.3,
        shadowRadius: 6
    },
    inputText: {
        flex: 1,
        backgroundColor: 'transparent',
        fontSize: 15,
    },
    listView: {
        marginHorizontal: width * 0.06,
        marginTop: width * 0.04,
        paddingHorizontal: 0,
        paddingTop: 5,
        paddingBottom: 15,
        backgroundColor: 'white',
        borderRadius: 10
    },
    list_container_style: { marginVertical: 0, paddingVertical: 2, backgroundColor: 'transparent' },
    title: {
        fontSize: 13,
        //color: '#999'
    },
    rightTitle: {
        fontSize: 13,
        textAlign: 'right',
        lineHeight: 14,
        flexWrap: 'wrap',
        width: width / 2,
        color: '#333'
    },
});
