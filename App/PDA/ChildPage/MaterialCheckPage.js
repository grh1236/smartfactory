import React from 'react';
import { View, StyleSheet, FlatList, Dimensions, ActivityIndicator, Picker, TextInput } from 'react-native';
import { Button, Text, Card, Icon, Image, ListItem } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { ScrollView } from 'react-native-gesture-handler';
import { NavigationEvents } from 'react-navigation';
import { styles } from '../Componment/PDAStyles';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import { deviceWidth } from '../../Url/Pixal';
import ModalDropdown from 'react-native-modal-dropdown';

class BackIcon extends React.PureComponent {
    render() {
        return (
            <TouchableCustom
                onPress={this.props.onPress} >
                <Icon
                    name='arrow-back'
                    color='#419FFF' />
            </TouchableCustom>
        )
    }
}

const { height, width } = Dimensions.get('window') //获取宽高
let listData = []
let pageNum = 1
let action = ""
let title = ""
let returnType = ""

export default class MaterialCheckPage extends React.PureComponent {
    constructor() {
        super();
        this.state = {
            action: '',
            resData: [],
            storageCode: '', // 材料编码
            storageName: '', // 材料名称
            size: '', // 规格型号
            unit: '', // 单位
            stock: '', // 库存
            type: '', // 材料性质
            stockCount: '', // 库存数量
            stockCount2: '', // 可用库存
            accuactualcount: '', // 项目累计领用量
            totalAmount: '0', // 合计金额 
            searchValue: '',
            selectType: '表单编号',
            searchType: 'materialCode',

            roomId: '',
            projectId: '',
            materialIds: '',

            // 选择确认
            supplierSelect: false,
            roomSelect: false,

            //底栏控制
            bottomData: [],
            teamVisible: false,
            cycleVisible: false,
            inspectorVisible: false,
            CameraVisible: false,
            response: '',

            checked: true,
            pictureVisible: true, // 问题照片
            imageOverSize: false, // 照片放大
            pictureUri: "",

            focusIndex: -1,
            isLoading: false,

            //查询界面取消选择按钮
            isCheckPage: false,

            isChecked: false,

            dataCount: 0,
            focusButton: 0,
            resDataButton: []
        }
    }

    componentDidMount() {
        const { navigation } = this.props;
        let guid = navigation.getParam('guid') || ''
        let pageType = navigation.getParam('pageType') || ''
        let roomId = ''
        let projectId = ''
        let titleParam = navigation.getParam('title') || ''
        title = titleParam
        action = titleParam
        this.resData("", "")
        if (title == '材料退库') {
            this.setState({
                resDataButton: [{ "name": "全部" }, { "name": "部门退库" }, { "name": "车间退库" }]
            })
        } else if (title == '材料盘点') {
            this.setState({
                resDataButton: [{ "name": "全部" }, { "name": "钢筋盘点" }, { "name": "材料盘点" }]
            })
        } else {
            this.setState({
                resDataButton: [{ "name": "全部" }, { "name": "项目领料" }, { "name": "部门领料" }, { "name": "车间领料" }]
            })
        }
        
        
    }

    componentWillUnmount() {
        listData = []
        pageNum = 1
        title = ""
        action = ""
        returnType = ""
        this.setState({

        })
    }


    //顶栏
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('title')
        }
    }

    resData = (searchType, searchValue) => {
        let formData = new FormData();
        let data = {};
        let dataAction = ""
        if (title == '项目领料' || title == '部门领料') {
            if (action == "项目领料") dataAction = "projectpickingquery"
            else if (action == "部门领料") dataAction = "deptpickingquery"
            else if (action == "车间领料") dataAction = "shoppickingquery"
            else dataAction = "allpickingquery"
        } else if (title == '材料出库') {
            if (action == "项目领料") dataAction = "projectoutstoragequery"
            else if (action == "部门领料") dataAction = "deptoutstoragequery"
            else if (action == "车间领料") dataAction = "shopoutstoragequery"
            else dataAction = "alloutstoragequery" 
        } else if (title == '材料退库') {
            if (action == "部门退库") dataAction = "deptoutstoragereturnquery"
            else if (action == "车间退库") dataAction = "shopoutstoragereturnquery"
            else dataAction = "alloutstoragereturnquery"
        } else if (title == '材料盘点') {
            if (action == "钢筋盘点") dataAction = "booktrueinventoryquery"
            else if (action == "车间退库") dataAction = "bookfalseinventoryquery"
            else dataAction = "allinventoryquery"
        } else if (title == '采购入库') {
            dataAction = "instoragequery"
        }
       
        data = {
            "action": dataAction,
            "servicetype": "pdamaterial",
            "express": "6DFD1C9B",
            "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
            "data": {
                "factoryId": Url.PDAFid,
                "searchType": searchType,
                "keyStr": searchValue,
                "pageNum": "1",
                "viewRowCt": "10"
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: MaterialCheckPage.js ~ line 130 ~ MaterialCheckPage ~ formData:", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res.statusText);
            //console.log(res);
            return res.json();
        }).then(resData => {
            if (resData.status == '100') {
                console.log("MaterialCheckPage ---- 146 ----" + JSON.stringify(resData.result))
                let res = resData.result
                //res.map((item, index) => {
                //    item.requisitionCount = "0",
                //    item.select = false
                //})
                listData = res
                this.setState({
                    resData: resData.result,
                    focusButton: action == "项目领料" || action == "部门退库" || action == "钢筋盘点" ? '1' :
                        action == "部门领料" || action == "车间退库" || action == "材料盘点" ? '2'  :
                            action == "车间领料" ? '3' :
                                '0',
                    isLoading: false
                })
                this.forceUpdate()
            } else {
                Alert.alert("错误提示", resData.message, [{
                    text: '取消',
                    style: 'cancel'
                }, {
                    text: '返回上一级',
                    onPress: () => {
                        this.props.navigation.goBack()
                    }
                }])
            }

        }).catch((error) => {
            console.log("🚀 ~ file: MaterialCheckPage.js ~ line 184 ~ MaterialCheckPage ~ error:", error)
        });

    }

    checkResData = () => {

    }

    PostData = () => { }

    // 分类选择
    _selectType = (index, value) => {
        //console.log(index + '--' + value)
        const { roomId, projectId, materialIds, searchType, searchValue } = this.state;
        let type = ""
        if (title == '采购入库') {
            switch (value) {
                case "表单编号": type = "formCode"; break;
                case "库房名称": type = "roomName"; break;
                case "供应商": type = "supplierName"; break;
                case "日期": type = "inTime"; break;
                default: type = ""; break;
            }
        } else if (title == '材料退库') {
            switch (value) {
                case "表单编号": type = "formCode"; break;
                case "状态": type = "chkstatusName"; break;
                case "库房名称": type = "roomName"; break;
                case "车间/部门": type = "deptname"; break;
                case "日期": type = "editDate"; break;
                default: type = ""; break;
            }
        } else {
            switch (value) {
                case "表单编号": type = "formCode"; break;
                case "库房名称": type = "roomName"; break;
                case "日期": type = "editDate"; break;
                default: type = ""; break;
            }
        }
        this.setState({
            searchType: type,
            selectType: value
        })
        this.resData(type, searchValue)
    }
    // 下拉列表分隔符
    _separator = () => {
        return (
            <Text style={{ height: 0 }}></Text>
        )
    }
  
    renderSearchBar = () => {
        const { selectType, roomId, projectId, materialIds } = this.state;
        let type = []
        if (title == '采购入库') {
            type = ['表单编号', '库房名称', '供应商', '日期']
        } else if (title == '材料退库') {
            type = ['表单编号', '状态', '库房名称', '车间/部门', '日期']
        } else {
            type = ['表单编号', '库房名称', '日期']
        }
        return (
            <View style={{
                flexDirection: 'row',
                alignItems: 'center',
                paddingTop: 10,
                paddingBottom: 10,
                backgroundColor: '#fff',
                borderBottomWidth: 1,
                borderBottomColor: '#efefef',
            }}>
                <View style={{ flex: 1 }}>
                    <View style={stylesSelectStock.searchContainerStyle}>

                        <View style={{ flexDirection: 'row', backgroundColor: '#f9f9f9', width: deviceWidth * 0.94, marginLeft: deviceWidth * 0.03, borderRadius: 45 }}>
                            <Text>   </Text>
                            <ModalDropdown
                                options={type}    //下拉内容数组
                                style={{ marginTop: 12 }}    //按钮样式
                                dropdownStyle={{ height: 36 * 3, width: 80 }}    //下拉框样式
                                dropdownTextStyle={{ fontSize: 13 }}    //下拉框文本样式
                                renderSeparator={this._separator}    //下拉框文本分隔样式
                                adjustFrame={this._adjustType}    //下拉框位置
                                dropdownTextHighlightStyle={{ color: 'rgba(42, 130, 228, 1)' }}    //下拉框选中颜色
                                onDropdownWillShow={() => { this.setState({ typeShow: true }) }}      //按下按钮显示按钮时触发
                                onDropdownWillHide={() => this.setState({ typeShow: false })}    //当下拉按钮通过触摸按钮隐藏时触发
                                onSelect={this._selectType}    //当选项行与选定的index 和 value 接触时触发
                                defaultValue={'表单编号'}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text>{this.state.selectType}</Text>
                                    <Icon name='caretdown' color='#419fff' iconStyle={{ fontSize: 13, marginTop: 1, marginLeft: 8, marginRight: -1 }} type='antdesign' ></Icon>
                                </View>
                            </ModalDropdown>

                            <Icon type='material-community' name='power-on' color='#999' style={{ marginTop: 10 }} />
                            <Icon name='search' color='#999' type='material' iconStyle={{ fontSize: 22, marginTop: 12 }}></Icon>

                            <TextInput style={stylesSelectStock.inputText}
                                keyboardType='web-search'
                                placeholder='搜索关键词'
                                onChangeText={(value) => {
                                    let type = ""
                                    if (title == '采购入库') {
                                        switch (selectType) {
                                            case "表单编号": type = "formCode"; break;
                                            case "库房名称": type = "roomName"; break;
                                            case "供应商": type = "supplierName"; break;
                                            case "日期": type = "inTime"; break;
                                            default: type = ""; break;
                                        }
                                    } else if (title == '材料退库') {
                                        switch (selectType) {
                                            case "表单编号": type = "formCode"; break;
                                            case "状态": type = "chkstatusName"; break;
                                            case "库房名称": type = "roomName"; break;
                                            case "车间/部门": type = "deptname"; break;
                                            case "日期": type = "editDate"; break;
                                            default: type = ""; break;
                                        }
                                    } else {
                                        switch (selectType) {
                                            case "表单编号": type = "formCode"; break;
                                            case "库房名称": type = "roomName"; break;
                                            case "日期": type = "editDate"; break;
                                            default: type = ""; break;
                                        }
                                    }
                                    this.setState({
                                        searchType: type,
                                        searchValue: value,
                                        dataCount: 0
                                    })
                                    this.resData(type, value)
                                }}
                            />

                            {/*<Icon type='material-community' name='qrcode-scan' color='#4D8EF5' style={{ marginTop: 12 }} />*/}
                            {/*<Icon type='material-community' name='microphone' color='#4D8EF5' style={{ marginTop: 12 }} />*/}
                            <Text>   </Text>
                        </View>



                    </View>
                </View>
            </View>
        );
    };

    ButtonTopScreen = () => {
        const { focusButton, resDataButton, searchType, searchValue } = this.state;
       
        return (
            resDataButton.map((item, index) => {
                if (true) {
                    return (
                        <Button
                            //key={index}
                            buttonStyle={{
                                borderRadius: 20,
                                flex: 1,
                                backgroundColor: focusButton == index ? '#419FFF' : 'white',
                                width: 80,
                            }}
                            containerStyle={{
                                marginHorizontal: 6,
                                borderRadius: 20,
                                marginTop: 20,
                            }}
                            title={item.name}
                            titleStyle={{ fontSize: 15, color: focusButton == index ? 'white' : '#419FFF' }}
                            //titleStyle={{ fontSize: 15, color: '#999' }}
                            //type='outline'
                            onPress={() => {
                                if (title == '材料退库') {
                                    switch (index) {
                                        case 0: action = '全部'; break;
                                        case 1: action = '部门退库'; break;
                                        case 2: action = '车间退库'; break;
                                        default: break;
                                    }
                                } else if (title == '材料盘点') {
                                    switch (index) {
                                        case 0: action = '全部'; break;
                                        case 1: action = '钢筋盘点'; break;
                                        case 2: action = '材料盘点'; break;
                                        default: break;
                                    }
                                } else {
                                    switch (index) {
                                        case 0: action = '全部'; break;
                                        case 1: action = '项目领料'; break;
                                        case 2: action = '部门领料'; break;
                                        case 3: action = '车间领料'; break;
                                        default: break;
                                    }
                                }
                                pageNum = 1
                                this.resData(searchType, searchValue)
                                this.setState({
                                    focusButton: index,
                                });
                            }}
                        />
                    )
                }
            })
        )
    }


    render() {
        const { navigation } = this.props;
        //从主页面传url, 未来集成到一起
        const QRurl = navigation.getParam('QRurl') || '';
        const QRid = navigation.getParam('QRid') || ''
        const type = navigation.getParam('type') || '';
        let pageType = navigation.getParam('pageType') || ''

        if (type == 'compid') {
            QRcompid = QRid
        }
        const { isCheckPage } = this.state

        if (this.state.isLoading) {
            return (
                <View style={styles.loading}>
                    <ActivityIndicator
                        animating={true}
                        color='#419FFF'
                        size="large" />
                </View>
            )
            //渲染页面
        } else {
            return (
                <View style={{ flex: 1, backgroundColor: '#f8f8f8' }}>
                    <Toast ref={(ref) => { this.toast = ref; }} position="center" />
                    <NavigationEvents
                        onWillFocus={this.UpdateControl}
                        onWillBlur={() => {
                            if (QRid != '') {
                                navigation.setParams({ QRid: '', type: '' })
                            }
                        }} />
                    {
                        this.renderSearchBar()
                    }
                    {
                        title == '采购入库' ? <View /> :
                            <View style={{ flexDirection: 'row', height: 60, backgroundColor: '#f8f8f8' }}>

                                <ScrollView scrollEnabled={true} horizontal={true} showsHorizontalScrollIndicator={false}>
                                    {this.ButtonTopScreen()}
                                </ScrollView>


                                {/*<Button*/}
                                {/*    type='outline'*/}
                                {/*    title='更多'*/}
                                {/*    titleStyle={{ fontSize: 15, }}*/}
                                {/*    buttonStyle={{*/}
                                {/*        borderRadius: 100,*/}
                                {/*        flex: 1*/}
                                {/*    }}*/}
                                {/*    containerStyle={{*/}
                                {/*        //marginHorizontal: 6,*/}
                                {/*        borderRadius: 100,*/}
                                {/*        marginTop: 20,*/}
                                {/*    }}*/}
                                {/*    onPress={() => {*/}
                                {/*        this.setState({*/}

                                {/*        })*/}
                                {/*    }} />*/}
                            </View>
                    }
                    <FlatList
                        style={{ flex: 1, borderRadius: 10, backgroundColor: '#f8f8f8' }}
                        ref={(flatList) => this._flatList1 = flatList}
                        //ItemSeparatorComponent={this._separator}
                        renderItem={this._renderItem}
                        onRefresh={this.refreshing}
                        refreshing={false}
                        //控制上拉加载，两个平台需要区分开
                        onEndReachedThreshold={(Platform.OS === 'ios') ? -0.3 : 0.1}
                        onEndReached={() => {
                            this._onload()
                        }
                        }
                        numColumns={1}
                        //ListFooterComponent={this._footer}
                        //columnWrapperStyle={{borderWidth:2,borderColor:'black',paddingLeft:20}}
                        //horizontal={true}
                        data={listData}
                        scrollEnabled={true}
                        extraData={this.state}
                    />
                </View>
            )
        }
    }

    _onload = (user) => {
        const { search, roomId, projectId, materialIds, searchType, searchValue } = this.state
        let strhazy = search
        this.toast.show('加载中...', 10000)
        pageNum += 1

        let formData = new FormData();
        let data = {};
        let dataAction = ""
        if (title == '项目领料' || title == '部门领料') {
            if (action == "项目领料") dataAction = "projectpickingquery"
            else if (action == "部门领料") dataAction = "deptpickingquery"
            else if (action == "车间领料") dataAction = "shoppickingquery"
            else dataAction = "allpickingquery"
        } else if (title == '材料出库') {
            if (action == "项目领料") dataAction = "projectoutstoragequery"
            else if (action == "部门领料") dataAction = "deptoutstoragequery"
            else if (action == "车间领料") dataAction = "shopoutstoragequery"
            else dataAction = "alloutstoragequery"
        } else if (title == '材料退库') {
            if (action == "部门退库") dataAction = "deptoutstoragereturnquery"
            else if (action == "车间退库") dataAction = "shopoutstoragereturnquery"
            else dataAction = "alloutstoragereturnquery"
        } else if (title == '材料盘点') {
            if (action == "钢筋盘点") dataAction = "booktrueinventoryquery"
            else if (action == "车间退库") dataAction = "bookfalseinventoryquery"
            else dataAction = "allinventoryquery"
        } else if (title == '采购入库') {
            dataAction = "instoragequery"
        }
        data = {
            "action": dataAction,
            "servicetype": "pdamaterial",
            "express": "6DFD1C9B",
            "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
            "data": {
                "factoryId": Url.PDAFid,
                "searchType": searchType,
                "keyStr": searchValue,
                "pageNum": pageNum,
                "viewRowCt": "10"
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        //console.log("🚀 ~ file: MaterialCheckPage.js ~ line 130 ~ MaterialCheckPage ~ formData:", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res.statusText);
            //console.log(res);
            return res.json();
        }).then(resData => {
            if (resData.status == '100') {
                //console.log("MaterialCheckPage ---- 146 ----" + JSON.stringify(resData.result))
                let res = resData.result
                //res.map((item, index) => {
                //    item.requisitionCount = "0",
                //        item.select = false
                //})
                listData.push.apply(listData, res)
                //listData = res
                this.setState({
                    resData: resData.result,
                    isLoading: false
                })
                if (res.length > 0 || res.length > 0) {
                    this.toast.show('加载完成');
                } else {
                    this.toast.show('无更多数据');
                }
            } else if (resData.status != '100') {
                Alert.alert('错误', resData.message)
            } else {
                this.toast.show('加载失败')
            }

        }).catch((error) => {
            console.log("🚀 ~ file: MaterialCheckPage.js ~ line 184 ~ MaterialCheckPage ~ error:", error)
        });
    }

    refreshing = () => {
        this.toast.show('刷新中...', 10000)
        this.setState({
            pageLoading: true
        })
        const { roomId, projectId, materialIds, searchType, searchValue } = this.state
        let formData = new FormData();
        let data = {};
        let dataAction = ""
        if (title == '项目领料' || title == '部门领料') {
            if (action == "项目领料") dataAction = "projectpickingquery"
            else if (action == "部门领料") dataAction = "deptpickingquery"
            else if (action == "车间领料") dataAction = "shoppickingquery"
            else dataAction = "allpickingquery"
        } else if (title == '材料出库') {
            if (action == "项目领料") dataAction = "projectoutstoragequery"
            else if (action == "部门领料") dataAction = "deptoutstoragequery"
            else if (action == "车间领料") dataAction = "shopoutstoragequery"
            else dataAction = "alloutstoragequery"
        } else if (title == '材料退库') {
            if (action == "部门退库") dataAction = "deptoutstoragereturnquery"
            else if (action == "车间退库") dataAction = "shopoutstoragereturnquery"
            else dataAction = "alloutstoragereturnquery"
        } else if (title == '材料盘点') {
            if (action == "钢筋盘点") dataAction = "booktrueinventoryquery"
            else if (action == "车间退库") dataAction = "bookfalseinventoryquery"
            else dataAction = "allinventoryquery"
        } else if (title == '采购入库') {
            dataAction = "instoragequery"
        }
        data = {
            "action": dataAction,
            "servicetype": "pdamaterial",
            "express": "6DFD1C9B",
            "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
            "data": {
                "factoryId": Url.PDAFid,
                "searchType": searchType,
                "keyStr": searchValue,
                "pageNum": "1",
                "viewRowCt": "10"
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        //console.log("🚀 ~ file: MaterialCheckPage.js ~ line 130 ~ MaterialCheckPage ~ formData:", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res.statusText);
            //console.log(res);
            return res.json();
        }).then(resData => {
            if (resData.status == '100') {
                //console.log("MaterialCheckPage ---- 146 ----" + JSON.stringify(resData.result))
                let res = resData.result
                //res.map((item, index) => {
                //    item.requisitionCount = "0",
                //        item.select = false
                //})
                //listData.push.apply(listData, res)
                listData = res
                this.setState({
                    resData: resData.result,
                    isLoading: false
                })
            } else {
                this.toast.show('刷新失败', 1600)
            }

        }).catch((error) => {
            console.log("🚀 ~ file: MaterialCheckPage.js ~ line 184 ~ MaterialCheckPage ~ error:", error)
        });
    }

    jumpPageInfo = (item) => {
        let formCodeFour = item.FormCode.substring(0, 4)
        if (title == '项目领料' || title == '部门领料') {
            if (formCodeFour == 'LLXM') {
                this.props.navigation.navigate('MaterialProjectPicking', {
                    title: '项目领料',
                    pageType: 'CheckPage',
                    guid: item._Rowguid
                })
            } else if (formCodeFour == 'LLBM') {
                this.props.navigation.navigate('MaterialProjectPicking', {
                    title: '部门领料',
                    pageType: 'CheckPage',
                    guid: item._Rowguid
                })
            }
        } else if (title == '材料出库') {
            this.props.navigation.navigate('MaterialOutStorage', {
                title: '材料出库',
                pageType: 'CheckPage',
                guid: item._Rowguid
            })
        } else if (title == '材料退库') {
            this.props.navigation.navigate('MaterialOutStorageReturn', {
                title: '材料退库',
                pageType: 'CheckPage',
                guid: item._Rowguid
            })
        } else if (title == '材料盘点') {
            this.props.navigation.navigate('MaterialInventory', {
                title: '材料退库',
                pageType: 'CheckPage',
                guid: item._Rowguid
            })
        } else if (title == '采购入库') {
            this.props.navigation.navigate('MaterialInStorage', {
                title: '采购入库',
                pageType: 'CheckPage',
                guid: item._Rowguid
            })
        }
    }

    _renderItem = ({ item, index }) => {
        const { focusIndex, dataCount } = this.state
        let count = dataCount
        return (
            <Card containerStyle={{ borderRadius: 10, elevation: 0 }}>
                <TouchableCustom onPress={() => { this.jumpPageInfo(item) }}>
                    <View style={[{
                        flexDirection: 'row',
                        justifyContent: 'flex-start',
                        alignItems: 'center'
                    }]}>
                        <View style={{ flex: 1 }}>
                            <ListItem
                                containerStyle={stylesSelectStock.list_container_style}
                                title={item.FormCode || ' '}
                                rightElement={
                                    <View style={{
                                        backgroundColor:
                                            item.EditName == '未出库' || item.EditName == '在编' ? '#5B5B5B' :
                                                item.EditName == '已审' ? '#DAC512' :
                                                    item.EditName == '待审' ? '#F27070' :
                                                        item.EditName == '已出库' || item.EditName == '已结' ? '#31C731' : 'transparent'
                                    }}>
                                        <Text style={{ color: 'white', fontSize: 13 }}>  {item.EditName}  </Text>
                                    </View>
                                }
                                titleStyle={stylesSelectStock.title}
                                rightTitleStyle={stylesSelectStock.rightTitle}
                            />
                            <ListItem
                                containerStyle={stylesSelectStock.list_container_style}
                                title={item.ProjectName || ' '}
                                rightElement={item.ConcreteType}
                                titleStyle={stylesSelectStock.title}
                                rightTitleStyle={stylesSelectStock.rightTitle}
                            />
                            <ListItem
                                containerStyle={stylesSelectStock.list_container_style}
                                title={item.EditDate || ' '}
                                rightElement={title == '材料退库' ? '收料人:' : title == '采购入库' ? '￥' + item.ConcreteGrade : '领料人:' + item.ConcreteGrade}
                                titleStyle={stylesSelectStock.title}
                                rightTitleStyle={stylesSelectStock.rightTitle}
                            />
                        </View>
                        <Icon type='antdesign' name='right' color='#999' onPress={() => { this.jumpPageInfo(item) }} />
                    </View>
                </TouchableCustom>
            </Card>
            )
    }
}

const stylesSelectStock = StyleSheet.create({
    searchContainerStyle:{
        shadowColor: '#999', shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.3,
        shadowRadius: 6
    },
    inputText: {
        flex: 1,
        backgroundColor: 'transparent',
        fontSize: 15,
    },
    listView: {
        marginHorizontal: width * 0.06,
        marginTop: width * 0.04,
        paddingHorizontal: 0,
        paddingTop: 5,
        paddingBottom: 15,
        backgroundColor: 'white',
        borderRadius: 10
    },
    list_container_style: { marginVertical: 0, paddingVertical: 2, backgroundColor: 'transparent' },
    title: {
        fontSize: 13,
        //color: '#999'
    },
    rightTitle: {
        fontSize: 13,
        textAlign: 'right',
        lineHeight: 14,
        flexWrap: 'wrap',
        width: width / 2,
        color: '#333'
    },
});
