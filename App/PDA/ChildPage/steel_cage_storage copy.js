import React from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, ListItem, BottomSheet } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import CardList from "../Componment/CardList";
import Url from '../../Url/Url';
import { RFT } from '../../Url/Pixal';
import { ScrollView } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker'
import { NavigationEvents } from 'react-navigation';
import ScanButton from '../Componment/ScanButton';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import ListItemScan_child from '../Componment/ListItemScan_child';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import BatchListItem from '../Componment/BatchBottomItem';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import { styles } from '../Componment/PDAStyles';




const { height, width } = Dimensions.get('window') //获取宽高

let QRkeeperName = ''
let QRStock = ''
let QRkeeperID = ''

let compDataArr = [], compIdArr = [], tmpstr = ''

export default class SteelCage extends React.Component {
  constructor() {
    super();
    this.state = {
      monitorSetup: '',
      isroom: false,
      isteam: false,
      iskeeper: false,
      editEmployeeName: Url.PDAEmployeeName,
      bottomData: [],
      bottomVisible: false,
      resData: [],
      formCode: '',
      rowguid: '',
      steelCageIDs: '',
      room: [],
      roomselect: '请选择',
      roomtext: '',
      roomId: '',
      team: [],
      teamselect: '请选择',
      teamId: '',
      labourName: '',
      QRkeeperName: '',
      QRkeeperID: '',
      keeper: [],
      keeperselect: '请选择',
      keeperId: '',
      ServerTime: '',
      QRid: '',
      QRStock: '',
      rowguid: '',
      compDataArr: [],
      compIdArr: [],
      compData: {},
      steelCage: {},
      steelCageCode: '',
      steelCageID: '',
      focusIndex: -1,
      compId: '',
      isGetcomID: false,
      isLoading: true,
      isCheckPage: false
    }
    //this.requestCameraPermission = this.requestCameraPermission.bind(this)
  }

  componentDidMount() {
    const { navigation } = this.props;
    let guid = navigation.getParam('guid') || ''
    let pageType = navigation.getParam('pageType') || ''
    if (pageType == 'CheckPage') {
      this.checkResData(guid)
      this.setState({ isCheckPage: true })
    } else {
      this.resData()
    }

  }

  componentWillUnmount() {
    compDataArr = [], compIdArr = [], tmpstr = ''
  }

  UpdateControl = () => {
    if (typeof this.props.navigation.getParam('QRid') != "undefined") {
      if (this.props.navigation.getParam('QRid') != "") {
        this.toast.show(Url.isLoadingView, 0)
      }
    }
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || ''
    const type = navigation.getParam('type') || '';

    if (type == 'Stockid') {
      this.GetcomID(QRid)
    }

    if (QRid != '') {
      navigation.setParams({ QRid: '', type: '' })
    }

  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  checkResData = (guid) => {
    let formData = new FormData();
    let data = {};
    data1 = { "action": "ObtainsteelCageStorage", "servicetype": "pda", "express": "249E955B", "ciphertext": "ddd23c2aae6f7b7e6e591356efe0edd2", "data": { "guid": "684C02CE429643EF9E4E8F8EE715EFFD", "factoryId": "74e1cbc6-2cfc-4d6f-bcd1-739d28e4c74d" } }
    data = {
      "action": "ObtainsteelCageStorage",
      "servicetype": "pda",
      "express": "249E955B",
      "ciphertext": "ddd23c2aae6f7b7e6e591356efe0edd2",
      "data": {
        "guid": guid,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: steel_cage_storage.js ~ line 132 ~ SteelCage ~ JSON.stringify(data1)", JSON.stringify(data1))

    /* fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json()
  }).catch(err => {
    }) */

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.setState({
        isLoading: false
      })
      let formCode = resData.result.formCode
      let team = resData.result.team
      let teamName = resData.result.teamName
      let labourName = resData.result.labourName
      let room = resData.result.room
      let roomName = resData.result.roomName
      let keeperName = resData.result.keeper
      let ServerTime = resData.result.time
      let editEmployeeName = resData.result.editEmployeeName
      let guid = resData.result.guid
      let steelCageCode = '', steelCage = ''
      compDataArr = resData.result.component
      compDataArr.map((item, index) => {
        steelCageCode = item.steelCageCode
        steelCage = item.steelCage[0]
        compIdArr.push(steelCage.steelCageID)
      })
      this.setState({
        resData: resData,
        formCode: formCode,
        ServerTime: ServerTime,
        room: room,
        roomselect: roomName,
        team: team,
        teamselect: teamName,
        keeperselect: keeperName,
        labourName: labourName,
        compDataArr: compDataArr,
        compIdArr: compIdArr,
        steelCageCode: steelCageCode,
        steelCage: steelCage,
        editEmployeeName: editEmployeeName,
        rowguid: guid,
      })
    }).catch((error) => {
    });
  }

  resData = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "GetFormAndTimeAndStoreroomAndTeamAndLabourforsteelCageStorage",
      "servicetype": "pda",
      "express": "249E955B",
      "ciphertext": "ddd23c2aae6f7b7e6e591356efe0edd2",
      "data": {
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      let formCode = resData.result.formCode
      let monitorSetup = resData.result.monitorSetup
      let team = resData.result.team
      let room = resData.result.room
      let keeper = resData.result.keeper
      let ServerTime = resData.result.ServerTime
      let rowguid = resData.result.rowguid
      this.setState({
        resData: resData,
        formCode: formCode,
        ServerTime: ServerTime,
        room: room,
        team: team,
        keeper: keeper,
        rowguid: rowguid,
        monitorSetup: monitorSetup,
        isLoading: false
      })
    }).catch((error) => {
    });
  }

  QRIdtrans = (type) => {
    const { navigation } = this.props;
    const QRid = navigation.getParam('QRid') || '';
    if (type == 'keeper') {
      this.state.keeper.map((item, index) => {
        if (QRid == item.keeperId) {
          this.setState({
            keeperselect: item.keeperName
          })
        }
      })
    }
  }

  GetcomID = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "GetSteelCageInfoForStorage",
      "servicetype": "pda",
      "express": "227EB695",
      "ciphertext": "de110251c4c1faf0038a3416d728a830",
      "data": {
        "steelCageID": id || '',
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
       this.toast.close()
      if (resData.status == '100') {
        let compData = resData.result
        let steelCageCode = compData.steelCageCode
        let steelCage = compData.steelCage[0]
        if (compDataArr.length == 0) {
          compDataArr.push(compData)
          compIdArr.push(steelCage.steelCageID)
          this.setState({
            compDataArr: compDataArr,
            compIdArr: compIdArr,
            compData: compData,
            steelCage: steelCage,
            steelCageCode: steelCageCode,
            isGetcomID: true
          })
          tmpstr = tmpstr + compIdArr.toString() + ' '
        } else {
          if (tmpstr.indexOf(steelCage.steelCageID) == -1) {
            compDataArr.push(compData)
            compIdArr.push(steelCage.steelCageID)
            this.setState({
              compDataArr: compDataArr,
              compIdArr: compIdArr,
              compData: compData,
              steelCage: steelCage,
              steelCageCode: steelCageCode,
              isGetcomID: true
            })
            tmpstr = tmpstr + compIdArr.toString() + ' '
          }

        }

      } else {
        Alert.alert('ERROR', resData.message)
      }

    }).catch(err => {
    })
  }

  comIDItem = () => {
    const { isGetcomID, buttondisable, compCode } = this.state
    return (
      <View>
        {
          !isGetcomID ?
            <View>
              <Button
                 
                title='扫码'
                type='solid'
                buttonStyle={{ height: 24 }}
                onPress={() => {
                  this.setState({
                    iscomIDdelete: false,
                    buttondisable: true
                  })
                  setTimeout(() => {
                    this.setState({ GetError: false, buttondisable: false })
                    varGetError = false
                  }, 1500)
                  this.props.navigation.navigate('QRCode', {
                    type: 'compid',
                    page: 'PDAStorage'
                  })
                }}
              />
            </View>
            :
            <TouchableOpacity
              onPress={() => {
                console.log('onPress')
              }}
              onLongPress={() => {
                console.log('onLongPress')
                Alert.alert(
                  '提示信息',
                  '是否删除构件信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      this.setState({
                        compData: [],
                        compCode: '',
                        compId: '',
                        iscomIDdelete: true,
                        isGetcomID: false,
                      })
                    }
                  }])
              }} >
              <Text>{compCode}</Text>
            </TouchableOpacity>
        }
      </View>

    )
  }

  _DatePicker = (index) => {
    const { ServerTime } = this.state
    return (
      <DatePicker
        customStyles={{
          dateInput: { borderColor: 'transparent', height: 16, width: width, marginLeft: 10 },
          dateTouchBody: { height: 14, marginLeft: -15, width: width / 2 },
          dateText: { width: width / 3, textAlign: 'right', marginRight: 20 }
        }}
        showIcon={false}
        mode='datetime'
        date={ServerTime}
        format="YYYY-MM-DD HH:mm"
        onOpenModal={() => { this.setState({ focusIndex: index }) }}
        onDateChange={(value) => {
          this.setState({
            ServerTime: value
          })
        }}
      />
    )
  }

  PostData = () => {
    console.log('QRStock', QRStock)
    console.log('QRkeeperID', QRkeeperID)
    this.setState({
      QRkeeperID: QRkeeperID,
      QRStock: QRStock
    })
    const { formCode, teamId, keeperId, ServerTime, roomId, rowguid, compIdArr } = this.state
    const { navigation } = this.props;
    const QRid = navigation.getParam('QRid') || '';

    console.log("🚀 ~ file: steel_cage_storage.js 411 ~ Post")

    if (roomId == '') {
      this.toast.show('库房信息不能为空');
      return
    } else if (teamId == '') {
      this.toast.show('班组信息不能为空');
      return
    } else if (keeperId == '' && QRkeeperID == '') {
      this.toast.show('库管员信息不能为空');
      return
    } else if (compIdArr.length == 0) {
      this.toast.show('入库明细信息不能为空');
      return
    }
    console.log("🚀 ~ file: steel_cage_storage.js 426 ~ Post")

    this.toast.show(saveLoading, 0)

    let steelCageIDs = compIdArr.toString()

    let formData = new FormData();
    let data = {};
    data = {
      "action": "SaveSteelCageStorage",
      "servicetype": "pda",
      "express": "4ED13340",
      "ciphertext": "3221ac6498a0e8b94ef042a37a728588",
      "data": {
        "formCode": formCode,
        "Username": Url.username,
        "factoryId": Url.PDAFid,
        "teamId": teamId,
        "steelCageIDs": steelCageIDs,
        "keeperId": keeperId || QRkeeperID,
        "ServerTime": ServerTime,
        "roomId": roomId,
        "rowguid": rowguid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: steel_cage_storage.js ~ line 450 ~ SteelCage ~ formData", formData)
    console.log("🚀 ~ file: steel_cage_storage.js ~ line 452 ~ SteelCage ~ Url.PDAurl", Url.PDAurl)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('保存成功');
        setTimeout(() => {
          this.props.navigation.replace(this.props.navigation.getParam("page"), {
            title: this.props.navigation.getParam("title"),
            pageType: this.props.navigation.getParam("pageType"),
            page: this.props.navigation.getParam("page"),
            isAppPhotoSetting: this.props.navigation.getParam("isAppPhotoSetting"),
            isAppDateSetting: this.props.navigation.getParam("isAppDateSetting"),
            isAppPhotoAlbumSetting: this.props.navigation.getParam("isAppPhotoAlbumSetting"),
          })
          //this.props.navigation.navigate('PDAMainStack')
        }, 400)
      } else {
        this.toast.show('保存失败')
        Alert.alert('保存失败', resData.message)
      }
    }).catch((error) => {
      console.log("🚀 ~ file: steel_cage_storage.js ~ line 474 ~ SteelCage ~ error", error)
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    });
  }

  CardList = (compData) => {
    return (
      compData.map((item, index) => {
        let comp = item.steelCage[0]
        return (
          <View style={{ width: width * 0.9, marginLeft: width * 0.05, }}>
            <ListItem
              key={index}
              onPress={() => {
                this.setState({
                  hidden: index,
                  isVisible: !this.state.isVisible
                }, () => {
                  console.log("🚀 ~ file: storage.js ~ line 126 ~ CardList2 ~ compData.map ~ this.state.isVisible", this.state.isVisible)
                })
              }}
              onLongPress={() => {
                Alert.alert(
                  '提示信息',
                  '是否删除钢筋笼信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      compDataArr.splice(index, 1)
                      compIdArr.splice(index, 1)
                      tmpstr = compIdArr.toString()

                      this.setState({
                        compDataArr: compDataArr,
                        compIdArr: compIdArr
                      })
                    }
                  }])

              }}
              containerStyle={[{ backgroundColor: '#36A7FF' }]}
              title={(index + 1) + '      ' + '钢筋笼编码' + item.steelCageCode}
              titleStyle={{ color: 'white' }}
              rightElement={
                <View style={{ flexDirection: 'row' }}>
                  {/* <Text>1个</Text> */}
                  <Icon name={this.state.hidden == index ? 'down' : 'up'} type='antdesign' color='white' />
                </View>
              }
              bottomDivider >
            </ListItem>
            {
              this.state.hidden == index ?
                <View key={index} style={{ backgroundColor: '#F9F9F9' }} >
                  <ListItem
                    containerStyle={styles.list_container_style}
                    title='钢筋笼编码'
                    rightTitle={comp.steelCageCode}
                    titleStyle={styles.title}
                    rightTitleProps={{ numberOfLines: 1 }}
                    rightTitleStyle={styles.rightTitle}
                  />
                  <ListItem
                    containerStyle={styles.list_container_style}
                    title='项目名称'
                    rightTitle={comp.projectName}
                    rightTitleProps={{ numberOfLines: 1 }}
                    titleStyle={styles.title}
                    rightTitleStyle={styles.rightTitle}
                  />
                  <ListItem
                    rightTitleProps={{ numberOfLines: 1 }}
                    containerStyle={styles.list_container_style}
                    title='构件类型'
                    rightTitle={comp.compTypeName}
                    titleStyle={styles.title}
                    rightTitleStyle={styles.rightTitle}
                  />
                  <ListItem
                    containerStyle={styles.list_container_style}
                    title='设计型号'
                    rightTitle={comp.designType}
                    titleStyle={styles.title}
                    rightTitleProps={{ numberOfLines: 1 }}
                    rightTitleStyle={styles.rightTitle}
                  />
                  <ListItem
                    containerStyle={styles.list_container_style}
                    title='钢筋笼重量'
                    rightTitle={comp.steelCageWeight}
                    titleStyle={styles.title}
                    rightTitleProps={{ numberOfLines: 1 }}
                    rightTitleStyle={styles.rightTitle}
                  />
                </View> : <View></View>
            }

          </View>
        )
      })
    )
  }

  render() {
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || '';
    const type = navigation.getParam('type') || '';

    const { resData, compDataArr, focusIndex, monitorSetup, formCode, ServerTime, team, teamselect, keeper, keeperselect, room, roomselect, labourName, bottomVisible, bottomData } = this.state

    if (type == 'keeper') {
      keeper.map((item, index) => {
        if (QRid == item.keeperId) {
          QRkeeperID = item.keeperId
          QRkeeperName = item.keeperName
        }
      })
    } else {
      QRStock = QRid
    }

    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (
        <ScrollView style={styles.mainView}>
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          <NavigationEvents
            onWillFocus={this.UpdateControl} />
          <ListItem
            title='表单编号'
            rightTitle={formCode}
            rightTitleProps={{ numberOfLines: 1 }}
            rightTitleStyle={{ width: width / 1.5, textAlign: 'right', fontSize: 15 }}
            bottomDivider
          />
          <ListItem
            containerStyle={focusIndex == '0' ? { backgroundColor: '#A9E2F3' } : {}}
            title='库房'
            rightElement={
              <TouchableOpacity onPress={() => { this.setState({ bottomVisible: true, bottomData: room, focusIndex: 0 }) }} >
                <View style={{ flexDirection: "row" }}>
                  <Text>{roomselect}</Text>
                  <Icon name='downsquare' color='#419fff' iconStyle={{ fontSize: 17, marginLeft: 2 }} type='antdesign' ></Icon>
                </View>
              </TouchableOpacity>
            }
            bottomDivider
          />
          <ListItem
            containerStyle={focusIndex == '1' ? { backgroundColor: '#A9E2F3' } : {}}
            title='加工班组'
            rightElement={
              <TouchableOpacity onPress={() => { this.setState({ bottomVisible: true, bottomData: team, focusIndex: 1 }) }} >
                <View style={{ flexDirection: "row" }}>
                  <Text>{teamselect}</Text>
                  <Icon name='downsquare' color='#419fff' iconStyle={{ fontSize: 17, marginLeft: 2 }} type='antdesign' ></Icon>
                </View>
              </TouchableOpacity>

            }
            bottomDivider
          />
          <ListItem
            title='劳务队'
            rightTitle={labourName}
            rightTitleStyle={{ width: width / 1.5, textAlign: 'right', fontSize: 15 }}
            rightTitleProps={{ numberOfLines: 1 }}
            bottomDivider
          />
          <ListItem
            title='操作人'
            rightTitle={this.state.editEmployeeName}
            rightTitleStyle={{ width: width / 1.5, textAlign: 'right', fontSize: 15 }}
            bottomDivider
          />
          <ListItem
            containerStyle={focusIndex == '2' ? { backgroundColor: '#A9E2F3' } : {}}
            title='库管员'
            rightElement={
              monitorSetup == '扫码' ?
                <View style={{ flexDirection: 'row' }}>
                  {
                    keeperselect == '请选择' && QRkeeperName == '' ?
                      <ScanButton
                        onPress={() => {
                          this.setState({ focusIndex: 2 })
                          this.props.navigation.navigate('QRCode', {
                            type: 'keeper',
                            page: 'SteelCageStorage'
                          })
                        }}
                      /> : <Text>{keeperselect == '请选择' ? QRkeeperName : keeperselect}</Text>
                  }
                </View> :
                <TouchableOpacity onPress={() => { this.setState({ bottomVisible: true, bottomData: keeper, focusIndex: 2 }) }} >
                  <View style={{ flexDirection: "row" }}>
                    <Text >{keeperselect}</Text>
                    <Icon name='downsquare' color='#419fff' iconStyle={{ fontSize: 17, marginLeft: 2 }} type='antdesign' ></Icon>
                  </View>
                </TouchableOpacity>

            }
            bottomDivider
          />
          <ListItem
            containerStyle={focusIndex == 4 ? { backgroundColor: '#A9E2F3' } : {}}
            title='入库日期'
            rightElement={this._DatePicker(4)}
            bottomDivider
          />
          <ListItem
            containerStyle={focusIndex == '3' ? { backgroundColor: '#A9E2F3' } : {}}
            title='入库明细'
            rightElement={
              <View style={{ flexDirection: 'row' }}>
                <ScanButton
                  onPress={() => {
                    this.setState({
                      isGetcomID: false,
                      focusIndex: 3
                    })
                    this.props.navigation.navigate('QRCode', {
                      type: 'Stockid',
                      page: 'SteelCageStorage'
                    })
                  }}
                />
              </View>
            }
          />

          {
            compDataArr.length != 0 ? this.CardList(compDataArr) : <View></View>
          }

          {
            this.state.isCheckPage ? <View></View> :
              <Button
                //disabled={!username || !password}

                //onPress={this.login}
                title='保存'
                type='outline'
                onPress={() => {
                  this.PostData()
                }}
                style={{ marginVertical: 10 }}
                containerStyle={{ marginVertical: 10, marginHorizontal: 20 }}
                buttonStyle={{ backgroundColor: '#FFFFFF', borderRadius: 60 }}
              ></Button>
          }



          <BottomSheet
            isVisible={bottomVisible && !this.state.isCheckPage}
            onRequestClose={() => {
              this.setState({
                bottomVisible: false
              })
            }}
            onBackdropPress={() => {
              this.setState({
                bottomVisible: false
              })
            }}
          >
            {
              bottomData.map((item, index) => {
                return (
                  <TouchableOpacity
                    onPress={() => {
                      if (item.roomId) {
                        this.setState({
                          roomId: item.roomId,
                          roomselect: item.roomName
                        })
                      } else if (item.teamId) {
                        this.setState({
                          teamId: item.teamId,
                          teamselect: item.teamName,
                          labourName: item.labourName
                        })
                      } else {
                        this.setState({
                          keeperId: item.keeperId,
                          keeperselect: item.keeperName
                        })
                      }
                      this.setState({
                        bottomVisible: false
                      })
                    }}
                  >
                    <ListItem
                      title={item.roomName ? item.roomName : (item.teamName ? item.teamName : item.keeperName)}
                      //titleStyle={styles.text}
                      //contentContainerStyle={styles.content}
                      // containerStyle={styles.container}
                      bottomDivider
                    />
                  </TouchableOpacity>
                )

              })
            }
          </BottomSheet>
        </ScrollView>
      )
    }
  }

}