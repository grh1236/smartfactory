import React from 'react';
import { View, TouchableHighlight, Dimensions, ActivityIndicator, Alert, Image } from 'react-native';
import { Button, Text, Input, Icon, Card, ListItem, BottomSheet, Overlay, Header } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { ScrollView } from 'react-native-gesture-handler';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { NavigationEvents } from 'react-navigation';
import { styles } from '../Componment/PDAStyles';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import CheckBoxScan from '../Componment/CheckBoxScan';
import AvatarImg from '../Componment/AvatarImg';
import DeviceStorage from '../../Url/DeviceStorage';
import ListItemScan_child from '../Componment/ListItemScan_child';
import DatePicker from 'react-native-datepicker';
import overlayImg from '../Componment/OverlayImg';
import OverlayImgZoomPicker from '../Componment/OverlayImgZoomPicker';
let letGetError = false

let DeviceStorageData = {} //缓存数据

const { height, width } = Dimensions.get('window') //获取宽高

let checkStandardListAll = [] //危险源列表

let fileflag = "6"

let imageArr = [], imageFileArr = [];

let postNameAndCheckStandardInfoAll = [] //所有岗位对应风险源

class BackIcon extends React.PureComponent {
    render() {
        return (
            <TouchableCustom
                onPress={this.props.onPress} >
                <Icon
                    name='arrow-back'
                    color='#419FFF' />
            </TouchableCustom>
        )
    }
}

export default class PostRiskInspection extends React.PureComponent {
    constructor() {
        super();
        this.state = {
            resData: [],
            formNo: '', // 表单编号
            team: "请选择", // 班组
            teamList: "",
            inspector: "请选择", // 排查人
            inspectorList: "",
            post: "", // 岗位
            cycle: "请选择", // 检查周期
            cycleList: "",
            checkDate: "", //检查时间
            teamLeader: "",// 班组长,
            rowguid: "",
            checkStandardList: [],

            //底栏控制
            bottomData: [],
            teamVisible: false,
            cycleVisible: false,
            inspectorVisible: false,
            CameraVisible: false,
            response: '',

            checked: true,
            pictureVisible: true, // 问题照片
            imageOverSize: false, // 照片放大
            pictureUri: "",
            imageArr: [],
            imageFileArr: [],
            fileid: "",
            recid: "",
            isPhotoUpdate: false,
            focusIndex: -1,
            isLoading: false,

            //查询界面取消选择按钮
            isCheck: false,
            isCheckPage: false,
            //控制app拍照，日期，相册选择功能
            isAppPhotoSetting: false,
            currShowImgIndex: 0,
            isAppDateSetting: false,
            isAppPhotoAlbumSetting: false
        }
        //this.requestCameraPermission = this.requestCameraPermission.bind(this)
    }

    componentDidMount() {
        const { navigation } = this.props;
        let guid = navigation.getParam('guid') || ''
        let pageType = navigation.getParam('pageType') || ''
        let isAppPhotoSetting = navigation.getParam('isAppPhotoSetting')
        let isAppDateSetting = navigation.getParam('isAppDateSetting')
        let isAppPhotoAlbumSetting = navigation.getParam('isAppPhotoAlbumSetting')
        this.setState({
            isAppPhotoSetting: isAppPhotoSetting,
            isAppDateSetting: isAppDateSetting,
            isAppPhotoAlbumSetting: isAppPhotoAlbumSetting
        })
        if (pageType == 'CheckPage') {
            this.setState({
                isCheckPage: true
            })
            this.checkResData(guid)
        } else {
            this.setState({
                isLoading: true,
                //inspector: Url.PDAEmployeeName,
            })
            this.resData()
        }

    }

    componentWillUnmount() {
        checkStandardListAll = []
        postNameAndCheckStandardInfoAll = []
        this.setState({
            rectifyNoticeInfo: ''
        })
    }

    resData = () => {
        let formData = new FormData();
        let data = {};
        data = {
            "action": "postriskinspectioninit",
            "servicetype": "pda",
            "express": "8AC4C0B0",
            "ciphertext": "7df83f712027c63f147f6c2d4a43f08d",
            "data": {
                "factoryId": Url.PDAFid
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log("res ---- " + res);
            return res.json();
        }).then(resData => {
            console.log("🚀 ~ file: PostRiskInspection.js ~ line 143 ~ PostRiskInspection ~ resData", resData)
            //console.log("🚀 ~ file: qualityinspection.js ~ line 226 ~ QualityInspection ~ resData", JSON.stringify(resData))
            this.setState({
                resData: resData,
            })

            let teamList = resData.result.team
            let cycleList = resData.result.cycle
            let formNo = resData.result.formNo
            let rowguid = resData.result.rowguid
            let postAndCheck = resData.result.postAndCheck

            let checkDate = this.dateFormat("YYYY-mm-dd", new Date());
            if (typeof resData.result.checkDate != 'undefined') {
                checkDate = resData.result.checkDate
            }

            DeviceStorage.get('priTeamId').then(res => {
                if (res != null) {
                    let bottomData = teamList
                    bottomData.filter(i => i.teamId == res).map((item, index) => {
                        this.setState({
                            team: item.teamName,
                            inspector: '请选择',
                            post: ''
                        })
                        if (item.teamPeople == null || item.teamPeople == '' || item.teamPeople == []) this.setState({ inspector: '无班组人员', post: '无对应岗位', inspectorList: '' })
                        else this.setState({ inspectorList: item.teamPeople, teamLeader: item.leader })
                    })
                }
            }).catch(err => { console.log("priTeam error: " + error) });

            DeviceStorage.get('priInspectorId').then(res => {
                if (res != null) {
                    let bottomData = this.state.inspectorList
                    bottomData.filter(i => i.id == res).map((item, index) => {
                        this.setState({
                            inspector: item.name,
                            post: item.post
                        })
                        if (item.post == null || item.post == '') {
                            checkStandardListAll = []
                            this.setState({ post: '无对应岗位' })
                        }
                        else {
                            postNameAndCheckStandardInfoAll.filter(j => j.postName == item.post).map((l, i) => {
                                this.checkStandardListAllInit(l, 'inspector')
                            })
                        }
                    })
                }
            })

            // 获取新添加表单单号
            //let formNoSplit = ""
            //if (formNo != null) {
            //    formNoSplit = formNo.split("-")
            //}
            //let time = this.dateFormat("YYYYmmdd", new Date)
            //if (formNoSplit == null || formNoSplit == "" || formNoSplit[1] != time) {
            //    formNo = "GWJC-" + time + "-001"
            //} else {
            //    formNo = formNoSplit[0] + "-" + formNoSplit[1] + "-" + ("00" + (Number(formNoSplit[2]) + 1 )).slice(-3)
            //}

            postNameAndCheckStandardInfoAll = postAndCheck
            this.setState({
                resData: resData,
                teamList: teamList,
                cycleList: cycleList,
                rowguid: rowguid,
                formNo: formNo,
                checkDate: checkDate,
                isLoading: false
            })
        }).catch((error) => {
            console.log("error --- " + error)
        });

    }

    checkResData = (guid, pageType) => {
        let formData = new FormData();
        let data = {};
        data = {
            "action": "postriskinspectionquerydetail",
            "servicetype": "pda",
            "express": "FA51025B",
            "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
            "data": {
                "guid": guid,
                "factoryId": Url.PDAFid
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: qualityinspection.js ~ line 226 ~ QualityInspection ~ formData", guid + '----' + Url.PDAFid)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log("res ---- " + JSON.stringify(Url));
            return res.json();
        }).then(resData => {
            //console.log("🚀 ~ file: qualityinspection.js ~ line 226 ~ QualityInspection ~ resData", resData)
            checkStandardListAll = resData.result.checkStandard
            this.setState({
                formNo: resData.result.formNo,
                team: resData.result.team,
                inspector: resData.result.inspector,
                post: resData.result.post,
                cycle: resData.result.cycle,
                checkDate: resData.result.checkDate,
                teamLeader: resData.result.teamLeader
            })
        }).catch((error) => {
            console.log("error --- " + error)
        });

    }

    //顶栏
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('title')
        }
    }

    PostData = () => {

        const { team, post, inspector, cycle, formNo, checkDate, teamLeader, rowguid, recid } = this.state
        if (team == '请选择' || team == '') {
            this.toast.show('请选择班组信息');
            return
        } else if (inspector == '请输入' || inspector == '') {
            this.toast.show('请选择排查人');
            return
        } else if (cycle == '请选择' || cycle == '') {
            this.toast.show('请选择检查周期');
            return
        }
        console.log("🚀 ~ file: PostRiskInspection.js:305 ~ PostRiskInspection ~ checkStandardListAll.map ~ checkStandardListAll:", checkStandardListAll)

        if (this.state.isAppPhotoSetting) {
            let NotTakePhoto = false
            checkStandardListAll.map((item, index) => {
                if (item.image_arr == "") {
                    NotTakePhoto = true
                    return
                }
            })
            if (NotTakePhoto) {
                this.toast.show('没有拍摄检查照片')
                return
            }
        }

        this.toast.show(saveLoading, 0)

        let formData = new FormData();
        let data = {};

        data = {
            "action": "postriskinspectionsave",
            "servicetype": "pda",
            "express": "06C795EE",
            "ciphertext": "a1e3818d57d9bfc9437c29e7a558e32e",
            "data": {
                "userName": Url.PDAEmployeeName,
                "factoryId": Url.PDAFid,
                "formNo": formNo,
                "team": team,
                "inspector": inspector,
                "post": post,
                "cycle": cycle,
                "rowguid": rowguid,
                "checkDate": checkDate,
                "checkStandardList": checkStandardListAll,
                "teamLeader": teamLeader,
                "editEmployeeId": Url.PDAEmployeeId,
                "editEmployeeName": Url.PDAEmployeeName,
                "recid": recid
            },
        }
        formData.append('jsonParam', JSON.stringify(data))
        if (!Url.isAppNewUpload) {
            checkStandardListAll.map((item, index) => {
                if (item.image_arr != "") {
                    formData.append('img_' + index, {
                        uri: item.image_arr,
                        type: 'image/jpeg',
                        name: 'img_' + index
                    })
                }
            })
        }
        console.log("🚀 ~ file: qualityinspection.js ~ line 217 ~ steelcage ~ formData", JSON.stringify(formData))
        fetch(Url.PDAurl, {
            method: 'post',
            headers: {
                'content-type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            console.log("🚀 ~ file: PostRiskInspection.js ~ line 318 ~ PostRiskInspection ~ res", res)
            console.log("🚀 ~ file: PostRiskInspection.js ~ line 318 ~ PostRiskInspection ~ res", res.status)

            return res.json();
        }).then(resData => {
            console.log("🚀 ~ file: PostRiskInspection.js ~ line 319 ~ PostRiskInspection ~ resData", resData)
            //console.log("🚀 ~ file: qualityinspection.js ~ line 228 ~ qualityinspection ~ resdata", resData)
            if (resData.status == '100') {
                this.toast.show('保存成功');
                setTimeout(() => {
                    this.props.navigation.replace(this.props.navigation.getParam("page"), {
                        title: this.props.navigation.getParam("title"),
                        pageType: this.props.navigation.getParam("pageType"),
                        page: this.props.navigation.getParam("page"),
                        isAppPhotoSetting: this.props.navigation.getParam("isAppPhotoSetting"),
                        isAppDateSetting: this.props.navigation.getParam("isAppDateSetting"),
                        isAppPhotoAlbumSetting: this.props.navigation.getParam("isAppPhotoAlbumSetting"),
                    })
                }, 1000)
            } else {
                this.toast.show("保存失败")
                Alert.alert('保存失败', resData.message)
                //console.log('resdata', resData)
            }
        }).catch((error) => {
            this.toast.show("保存失败")
            console.log("error --- " + error)
        });

    }

    // 获取登录时间
    getTodayDate() {
        let time = this.dateFormat("YYYY-mm-dd", new Date)
        return (
            <Text>{time}</Text>
        )
    }

    // 日期格式化
    dateFormat(fmt, date) {
        let ret;
        const opt = {
            "Y+": date.getFullYear().toString(),        // 年
            "m+": (date.getMonth() + 1).toString(),     // 月
            "d+": date.getDate().toString(),            // 日
            "H+": date.getHours().toString(),           // 时
            "M+": date.getMinutes().toString(),         // 分
            "S+": date.getSeconds().toString()          // 秒
            // 有其他格式化字符需求可以继续添加，必须转化成字符串
        };
        for (let k in opt) {
            ret = new RegExp("(" + k + ")").exec(fmt);
            if (ret) {
                fmt = fmt.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")))
            };
        };
        return fmt;
    }

    _DatePicker = (index) => {
        const { checkDate } = this.state
        return (
            <DatePicker
                customStyles={{
                    dateInput: styles.dateInput,
                    dateTouchBody: styles.dateTouchBody,
                    dateText: this.state.isAppDateSetting ? styles.dateText : styles.dateDisabled,
                }}
                iconComponent={<Icon name='caretdown' color={this.state.isAppDateSetting ? '#419fff' : "#666"} iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
                }
                showIcon={true}
                mode='date'
                date={checkDate}
                onOpenModal={() => { this.setState({ focusIndex: index }) }}
                format="YYYY-MM-DD"
                disabled={!this.state.isAppDateSetting}
                onDateChange={(value) => {
                    this.setState({
                        checkDate: value
                    })
                }}
            />
        )
    }

    // 下拉菜单
    downItem = (index, i, data) => {
        const { teamList, team, cycle, cycleList, inspector, inspectorList } = this.state
        switch (index) {
            case '1': return (
                // 班组
                <View>{(
                    this.state.isCheck ? <Text >{team}</Text> :
                        <TouchableCustom onPress={() => { this.isItemVisible(teamList, index, i) }} >
                            <IconDown text={team} />
                        </TouchableCustom>
                )}</View>
            ); break;
            case '2': return (
                // 排查人
                <View>{(
                    this.state.isCheck ? <Text >{inspector}</Text> :
                        <TouchableCustom onPress={() => { this.isItemVisible(inspectorList, index, i) }} >
                            <IconDown text={inspector} />
                        </TouchableCustom>
                )}</View>
            ); break;
            case '4': return (
                // 检查周期
                <View>{(
                    this.state.isCheck ? <Text >{cycle}</Text> :
                        <TouchableCustom onPress={() => { this.isItemVisible(cycleList, index, i) }} >
                            <IconDown text={cycle} />
                        </TouchableCustom>
                )}</View>
            ); break;
            default: return;
        }
    }

    isItemVisible = (data, focusIndex, i) => {
        //console.log("************** " + focusIndex + "-" + JSON.stringify(data))
        if (typeof (data) != "undefined") {
            if (data != [] && data != "") {
                switch (focusIndex) {
                    case '1': this.setState({ teamVisible: true, bottomData: data, focusIndex: focusIndex }); break;
                    case '2': this.setState({ inspectorVisible: true, bottomData: data, focusIndex: focusIndex }); break;
                    case '4': this.setState({ cycleVisible: true, bottomData: data, focusIndex: focusIndex }); break;
                    default: return;
                }
            }
        } else {
            this.toast.show("无数据")
        }
    }

    cameraFunc = (data) => {
        launchCamera(
            {
                mediaType: 'photo',
                includeBase64: false,
                maxHeight: parseInt(Url.isResizePhoto),
                maxWidth: parseInt(Url.isResizePhoto),
                saveToPhotos: true,

            },
            (response) => {
                if (response.uri == undefined) {
                    return
                }
                data.image_arr = response.uri

                data.picture_visible = false
                data.picture_select = true

                this.setState({ check: !this.state.check })
                if (Url.isAppNewUpload) {
                    this.cameraPost(response.uri, data)
                } else {
                    this.setState({
                        pictureSelect: true,
                    })
                }
                console.log(response)
            },
        )
    }

    camLibraryFunc = (data) => {
        launchImageLibrary(
            {
                mediaType: 'photo',
                includeBase64: false,
                maxHeight: parseInt(Url.isResizePhoto),
                maxWidth: parseInt(Url.isResizePhoto),
            },
            (response) => {
                if (response.uri == undefined) {
                    return
                }
                data.image_arr = response.uri
                data.picture_visible = false
                data.picture_select = true
                this.setState({ check: !this.state.check })
                if (Url.isAppNewUpload) {
                    this.cameraPost(response.uri, data)
                } else {
                    this.setState({
                        pictureSelect: true,
                    })
                }
                console.log(response)
            },
        )
    }

    camandlibFunc = (data) => {
        Alert.alert(
            '提示信息',
            '选择拍照方式',
            [{
                text: '取消',
                style: 'cancel'
            }, {
                text: '拍照',
                onPress: () => {
                    this.cameraFunc(data)
                }
            }, {
                text: '相册',
                onPress: () => {
                    this.camLibraryFunc(data)
                }
            }])
    }

    // 展示/隐藏 放大图片
    handleZoomPicture = (flag, index) => {
        this.setState({
            imageOverSize: false,
            currShowImgIndex: index || 0
        })
    }

    // 加载放大图片弹窗
    renderZoomPicture = () => {
        const { imageOverSize, currShowImgIndex, imageFileArr, pictureUri } = this.state
        let imgArr = [{
            url: pictureUri
        }]
        return (
            <OverlayImgZoomPicker
                isShowImage={imageOverSize}
                currShowImgIndex={currShowImgIndex}
                zoomImages={imgArr}
                callBack={(flag) => this.handleZoomPicture(flag)}
            ></OverlayImgZoomPicker>
        )
    }

    cameraPost = (uri, data) => {
        const { recid } = this.state
        this.toast.show('图片上传中', 2000)

        let formData = new FormData();
        let data1 = {};
        data1 = {
            "action": "savephote",
            "servicetype": "photo_file",
            "express": "D2979AB2",
            "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
            "data": {
                "fileflag": fileflag,
                "username": Url.PDAusername,
                "recid": recid
            }
        }
        formData.append('jsonParam', JSON.stringify(data1))
        formData.append('img', {
            uri: uri,
            type: 'image/jpeg',
            name: 'img'
        })
        console.log("🚀 ~ file: concrete_pouring.js ~ line 672 ~ SteelCage ~ formData", formData)

        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            console.log(res.statusText);
            console.log(res);
            return res.json();
        }).then(resData => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 690 ~ SteelCage ~ resData", resData)
            if (resData.status == '100') {
                let fileid = resData.result.fileid;
                let recid = resData.result.recid;

                let tmp = {}
                tmp.fileid = fileid
                tmp.uri = uri
                tmp.url = uri
                imageFileArr.push(tmp)
                data.fileid = fileid
                data.recid = recid
                this.setState({
                    fileid: fileid,
                    recid: "",
                    pictureSelect: true,
                    imageFileArr: imageFileArr
                })
                console.log("🚀 ~ file: concrete_pouring.js ~ line 704 ~ SteelCage ~ imageFileArr", imageFileArr)
                this.toast.show('图片上传成功');
                this.setState({
                    isPhotoUpdate: false
                })
            } else {
                Alert.alert('图片上传失败', resData.message)
                this.toast.close(1)
            }
        }).catch((error) => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 692 ~ SteelCage ~ error", error)
        });
    }

    cameraDelete = (data) => {
        if (Url.isAppNewUpload) {
            let formData = new FormData();
            let data1 = {};
            data1 = {
                "action": "deletephote",
                "servicetype": "photo_file",
                "express": "D2979AB2",
                "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
                "data": {
                    "fileid": data.fileid,
                }
            }
            formData.append('jsonParam', JSON.stringify(data1))
            console.log("🚀 ~ file: concrete_pouring.js ~ line 733 ~ SteelCage ~ cameraDelete ~ formData", formData)
            fetch(Url.PDAurl, {
                method: 'POST',
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                body: formData
            }).then(res => {
                console.log(res.statusText);
                console.log(res);
                return res.json();
            }).then(resData => {
                console.log("🚀 ~ file: concrete_pouring.js ~ line 745 ~ SteelCage ~ cameraDelete ~ resData", resData)
                if (resData.status == '100') {
                    data.image_arr = ""
                    data.fileid = ""
                    data.picture_select = false
                    data.picture_visible = true
                    this.setState({ check: !this.state.check }, () => { this.forceUpdate() })
                    this.toast.show('图片删除成功');
                } else {
                    Alert.alert('图片删除失败', resData.message)
                    this.toast.close(1)
                }
            }).catch((error) => {
                console.log("🚀 ~ file: concrete_pouring.js ~ line 761 ~ SteelCage ~ cameraDelete ~ error", error)
            });
        } else {
            data.image_arr = ""
            data.picture_select = false
            data.picture_visible = true
            this.setState({ check: !this.state.check }, () => { this.forceUpdate() })
        }

    }

    // 问题照片
    getPicture = (data) => {
        return (
            <View>
                {
                    data.picture_visible ? <Icon name='camera-alt' color='#4D8EF5' iconProps={{ size: 32 }} onPress={() => {
                        if (this.state.isPhotoUpdate == false) {
                            if (this.state.isAppPhotoAlbumSetting) {
                                this.camandlibFunc(data)
                            } else {
                                this.cameraFunc(data)
                            }
                        }
                        /* this.setState({
                          CameraVisible: true
                        }) */
                    }}
                    ></Icon> : <View />

                }
                {this.imageView(data)}
            </View>
        )
    }

    imageView = (data) => {
        data.picture_select = this.state.isCheckPage ? true : data.picture_select
        data.image_arr = this.state.isCheckPage ? data.picture : data.image_arr
        return (
            <View style={{ flexDirection: 'row' }}>
                {
                    data.picture_select ?
                        <AvatarImg item={data.image_arr} isRounded="false" index='0'
                            onPress={() => {
                                this.setState({
                                    imageOverSize: true,
                                    pictureUri: data.image_arr
                                })
                            }}
                            onLongPress={() => {
                                Alert.alert(
                                    '提示信息',
                                    '是否删除照片',
                                    [{
                                        text: '取消',
                                        style: 'cancel'
                                    }, {
                                        text: '删除',
                                        onPress: () => {
                                            //console.log("🚀 ~ file: qualityinspection.js ~ line 649 ~ SteelCage ~ this.state.imageArr.map ~ i", data.image_arr)
                                            this.cameraDelete(data)
                                            //console.log("🚀 ~ file: qualityinspection.js ~ line 656 ~ SteelCage ~ this.state.imageArr.map ~ i", data.image_arr)
                                        }
                                    }])
                            }}
                        /> : <View></View>
                }
            </View>
        )
    }

    // 危险源列表数据初始化
    checkStandardListAllInit(check, changeItem, param) {
        let standrad = check.checkStandard
        checkStandardListAll = []
        if (changeItem == 'inspector') {
            standrad.map((r, i) => {
                if (this.state.cycle != "" || this.state.cycle != "请选择") {
                    if (this.state.cycle == r.cycle) {
                        checkStandardListAll.push(
                            {
                                "check_standard": r.check_standard,
                                "check_method": r.check_method,
                                "cycle": r.cycle,
                                "risk_level": r.risk_level,
                                "check_result": "不符合",
                                "image_arr": "",
                                "picture_visible": true,
                                "picture_select": false,
                                "check_result_checked": false,
                                "recid": ""
                            }
                        )
                    }
                }
            })
        } else if (changeItem == 'cycle') {
            standrad.map((r, i) => {
                if (param == r.cycle) {
                    checkStandardListAll.push(
                        {
                            "check_standard": r.check_standard,
                            "check_method": r.check_method,
                            "cycle": r.cycle,
                            "risk_level": r.risk_level,
                            "check_result": "不符合",
                            "image_arr": "",
                            "picture_visible": true,
                            "picture_select": false,
                            "check_result_checked": false,
                            "recid": ""
                        }
                    )
                }
            })
        }
    }

    render() {
        const { navigation } = this.props;
        //从主页面传url, 未来集成到一起
        const QRurl = navigation.getParam('QRurl') || '';
        const QRid = navigation.getParam('QRid') || ''
        const type = navigation.getParam('type') || '';
        let pageType = navigation.getParam('pageType') || ''

        if (type == 'compid') {
            QRcompid = QRid
        }
        const { isCheckPage, focusIndex, formNo, team, teamList, inspector, inspectorList, post, cycle, cycleList, checkDate, teamLeader, bottomData, teamVisible, cycleVisible, inspectorVisible, pictureVisible } = this.state

        if (this.state.isLoading) {
            return (
                <View style={styles.loading}>
                    <ActivityIndicator
                        animating={true}
                        color='#419FFF'
                        size="large" />
                </View>
            )
            //渲染页面
        } else {
            return (
                <View style={{ flex: 1 }}>
                    <Toast ref={(ref) => { this.toast = ref; }} position="center" />
                    <NavigationEvents
                        onWillFocus={this.UpdateControl}
                        onWillBlur={() => {
                            if (QRid != '') {
                                navigation.setParams({ QRid: '', type: '' })
                            }
                        }} />
                    <ScrollView ref={ref => this.scoll = ref} style={styles.mainView} showsVerticalScrollIndicator={false} >

                        <View style={styles.listView}>
                            <ListItemScan focusStyle={focusIndex == '0' ? styles.focusColor : {}} title='表单单号:' rightElement={formNo} />

                            {
                                isCheckPage ? <ListItemScan focusStyle={focusIndex == '1' ? styles.focusColor : {}} title='班组:' rightElement={team} /> :
                                    <TouchableCustom underlayColor={'lightgray'} onPress={() => { this.isItemVisible(teamList, '1', '') }}>
                                        <ListItemScan focusStyle={focusIndex == '1' ? styles.focusColor : {}} title='班组:' rightElement={this.downItem('1', '')} />
                                    </TouchableCustom>
                            }

                            {
                                isCheckPage ? <ListItemScan focusStyle={focusIndex == '2' ? styles.focusColor : {}} title='排查人:' rightElement={inspector} /> :
                                    <TouchableCustom underlayColor={'lightgray'} onPress={() => { this.isItemVisible(inspectorList, '2', '') }}>
                                        <ListItemScan focusStyle={focusIndex == '2' ? styles.focusColor : {}} title='排查人:' rightElement={this.downItem('2', '')} />
                                    </TouchableCustom>
                            }

                            <ListItemScan focusStyle={focusIndex == '3' ? styles.focusColor : {}} title='岗位:' rightElement={post} />

                            {
                                isCheckPage ? <ListItemScan focusStyle={focusIndex == '4' ? styles.focusColor : {}} title='检查周期:' rightElement={cycle} /> :
                                    <TouchableCustom underlayColor={'lightgray'} onPress={() => { this.isItemVisible(cycleList, '4', '') }}>
                                        <ListItemScan focusStyle={focusIndex == '4' ? styles.focusColor : {}} title='检查周期:' rightElement={this.downItem('4', '')} />
                                    </TouchableCustom>
                            }

                            <ListItemScan focusStyle={focusIndex == '5' ? styles.focusColor : {}} title='检查时间:' rightElement={this._DatePicker(5)} />

                            {/*<Text style={{ paddingHorizontal: 10, textAlign: 'center', fontSize: 14, paddingVertical: 11 }}>危险源/检查标准（排查项目）</Text>*/}


                        </View>


                        {/*危险源列表*/}
                        {
                            checkStandardListAll.map((l, i) => {
                                return (

                                    <View style={styles.listView} key={i}>
                                        <Card containerStyle={styles.card_containerStyle} >
                                            <ListItemScan_child><Text>{l.check_standard}</Text></ListItemScan_child>
                                            <ListItemScan_child><Text>排查方法：{l.check_method}</Text></ListItemScan_child>
                                            <ListItemScan_child><Text>风险等级：{l.risk_level}</Text></ListItemScan_child>
                                            {/*<ListItem containerStyle={{ backgroundColor: '#F8F8F8' }} title={"周期：" + l.cycle}></ListItem>*/}
                                        </Card>
                                        {
                                            isCheckPage ? <ListItemScan focusStyle={focusIndex == '7' + i ? styles.focusColor : {}} title='检查结果:' rightElement={l.check_result} /> :
                                                <ListItemScan focusStyle={focusIndex == '7' + i ? styles.focusColor : {}} title='检查结果:' rightTitle={
                                                    <View style={{ flexDirection: "row", marginRight: -23, paddingVertical: 0 }}>
                                                        <CheckBoxScan
                                                            title='符合'
                                                            checked={l.check_result_checked}
                                                            onPress={() => {
                                                                l.check_result = '符合'
                                                                l.check_result_checked = !l.check_result_checked
                                                                this.setState({
                                                                    checked: !this.state.checked
                                                                })
                                                            }}
                                                        />
                                                        <CheckBoxScan
                                                            title='不符合'
                                                            checked={!l.check_result_checked}
                                                            onPress={() => {
                                                                l.check_result = '不符合'
                                                                l.check_result_checked = !l.check_result_checked
                                                                this.setState({
                                                                    checked: !this.state.checked
                                                                })
                                                            }}
                                                        />
                                                    </View>
                                                } />
                                        }

                                        <ListItemScan focusStyle={focusIndex == '8' + i ? styles.focusColor : {}} title='拍照:' rightElement={() =>
                                            isCheckPage ? this.imageView(l) : pictureVisible ? this.getPicture(l) : this.imageView()}
                                        />

                                    </View>
                                )
                            })

                        }

                        <View style={styles.listView}>
                            <ListItemScan focusStyle={focusIndex == '6' ? styles.focusColor : {}} title='班组长:' rightElement={teamLeader} />
                        </View>
                        {
                            isCheckPage ? <Text /> :
                                <FormButton
                                    backgroundColor='#17BC29'
                                    onPress={() => {
                                        this.PostData()
                                    }}
                                    title='保存'
                                    disabled={this.state.isPhotoUpdate}
                                />
                        }

                        {/*班组底栏*/}
                        <BottomSheet
                            onBackdropPress={() => {
                                this.setState({
                                    teamVisible: false
                                })
                            }}
                            isVisible={teamVisible && !this.state.isCheckPage} onRequestClose={() => {
                                this.setState({
                                    teamVisible: false
                                })
                            }}
                        >
                            <ScrollView style={styles.bottomsheetScroll}>
                                {bottomData.map((item, index) => {
                                    let title = item.teamName
                                    return (
                                        <TouchableCustom
                                            onPress={() => {
                                                //console.log("------------------ " + JSON.stringify(item.teamPeople))
                                                this.setState({
                                                    team: item.teamName,
                                                    teamVisible: false,
                                                    inspector: '请选择',
                                                    post: ''
                                                })
                                                checkStandardListAll = []
                                                DeviceStorage.save('priTeamId', item.teamId);
                                                DeviceStorage.save('priInspectorId', null);
                                                if (item.teamPeople == null || item.teamPeople == '' || item.teamPeople == []) this.setState({ inspector: '无班组人员', post: '无对应岗位', inspectorList: '' })
                                                else this.setState({ inspectorList: item.teamPeople, teamLeader: item.leader })
                                            }}
                                        >
                                            <BottomItem backgroundColor='white' color="#333" title={title} />
                                        </TouchableCustom>
                                    )
                                })}
                            </ScrollView>
                            <TouchableHighlight
                                onPress={() => {
                                    this.setState({ teamVisible: false })
                                }}>
                                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
                            </TouchableHighlight>
                        </BottomSheet>

                        {/*排查人底栏*/}
                        <BottomSheet
                            onBackdropPress={() => {
                                this.setState({
                                    inspectorVisible: false
                                })
                            }}
                            isVisible={inspectorVisible && !this.state.isCheckPage} onRequestClose={() => {
                                this.setState({
                                    inspectorVisible: false
                                })
                            }}
                        >
                            <ScrollView style={styles.bottomsheetScroll}>
                                {bottomData.map((item, index) => {
                                    let title = item.name
                                    return (
                                        <TouchableCustom
                                            onPress={() => {
                                                this.setState({
                                                    inspector: item.name,
                                                    post: item.post,
                                                    inspectorVisible: false
                                                })
                                                DeviceStorage.save('priInspectorId', item.id);
                                                if (item.post == null || item.post == '') {
                                                    checkStandardListAll = []
                                                    this.setState({ post: '无对应岗位' })
                                                }
                                                else {
                                                    postNameAndCheckStandardInfoAll.filter(j => j.postName == item.post).map((l, i) => {
                                                        this.checkStandardListAllInit(l, 'inspector')
                                                    })
                                                }
                                                //console.log("------------------ " + JSON.stringify(checkStandardListAll))
                                            }}
                                        >
                                            <BottomItem backgroundColor='white' color="#333" title={title} />
                                        </TouchableCustom>
                                    )
                                })}
                            </ScrollView>
                            <TouchableHighlight
                                onPress={() => {
                                    this.setState({ inspectorVisible: false })
                                }}>
                                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
                            </TouchableHighlight>
                        </BottomSheet>

                        {/*检查周期底栏*/}
                        <BottomSheet
                            onBackdropPress={() => {
                                this.setState({
                                    cycleVisible: false
                                })
                            }}
                            isVisible={cycleVisible && !this.state.isCheckPage} onRequestClose={() => {
                                this.setState({
                                    cycleVisible: false
                                })
                            }}
                        >
                            <ScrollView style={styles.bottomsheetScroll}>
                                {bottomData.map((item, index) => {
                                    let title = item
                                    return (
                                        <TouchableCustom
                                            onPress={() => {
                                                this.setState({
                                                    cycle: item,
                                                    cycleVisible: false
                                                })
                                                postNameAndCheckStandardInfoAll.filter(j => j.postName == post).map((l, i) => {
                                                    this.checkStandardListAllInit(l, 'cycle', item)
                                                })
                                            }}
                                        >
                                            <BottomItem backgroundColor='white' color="#333" title={title} />
                                        </TouchableCustom>
                                    )
                                })}
                            </ScrollView>
                            <TouchableHighlight
                                onPress={() => {
                                    this.setState({ cycleVisible: false })
                                }}>
                                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
                            </TouchableHighlight>
                        </BottomSheet>

                        {/*照片放大*/}
                        {this.renderZoomPicture()}
                        {/* {overlayImg(obj = {
                            onRequestClose: () => {
                                this.setState({ imageOverSize: !this.state.imageOverSize }, () => {
                                    console.log("🚀 ~ file: equipment_maintenance.js:1696 ~ render ~ imageOverSize:", this.state.imageOverSize)
                                })
                            },
                            onPress: () => {
                                this.setState({
                                    imageOverSize: !this.state.imageOverSize
                                })
                            },
                            uri: this.state.pictureUri,
                            isVisible: this.state.imageOverSize
                        })
                        } */}
                    </ScrollView>
                </View>
            )
        }
    }

}


