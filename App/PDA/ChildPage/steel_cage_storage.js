import React from 'react';
import { View, TouchableHighlight, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, ListItem, BottomSheet } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import CardList from "../Componment/CardList";
import Url from '../../Url/Url';
import { RFT } from '../../Url/Pixal';
import { ScrollView } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker'
import { NavigationEvents } from 'react-navigation';
import ScanButton from '../Componment/ScanButton';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import ListItemScan_child from '../Componment/ListItemScan_child';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import BatchListItem from '../Componment/BatchBottomItem';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import { styles } from '../Componment/PDAStyles';
import DeviceStorage from '../../Url/DeviceStorage';
import { DeviceEventEmitter, NativeModules } from 'react-native';

const { height, width } = Dimensions.get('window') //获取宽高

let QRkeeperName = ''
let QRStock = ''
let QRkeeperID = ''

let compDataArr = [], compIdArr = [], tmpstr = '', visibleArr = []

export default class SteelCage extends React.Component {
  constructor() {
    super();
    this.state = {
      monitorSetup: '',
      editEmployeeName: Url.PDAEmployeeName,
      bottomData: [],
      bottomVisible: false,
      resData: [],
      type: 'Stockid',
      formCode: '',
      rowguid: '',
      steelCageIDs: '',
      room: [],
      roomselect: '请选择',
      roomtext: '',
      roomId: '',
      team: [],
      teamselect: '请选择',
      teamId: '',
      labourName: '',
      QRkeeperName: '',
      QRkeeperID: '',
      keeper: [],
      keeperselect: '请选择',
      keeperId: '',
      isGetkeepID: false,
      ServerTime: '',
      QRid: '',
      QRStock: '',
      rowguid: '',
      compDataArr: [],
      compIdArr: [],
      compData: {},
      visibleArr: [],
      steelCage: {},
      steelCageCode: '',
      steelCageID: '',
      focusIndex: 3,
      compId: '',
      isGetcomID: false,
      isLoading: true,
      isCheckPage: false,
      //批量
      isBatch: false,
      listBatchNoInfo: [],
      batchCompCode: '',
      batchCompCodeselect: '请选择',
      number: 0,
      numberEdit: 0,
    }
    //this.requestCameraPermission = this.requestCameraPermission.bind(this)
  }

  componentDidMount() {
    const { navigation } = this.props;
    let guid = navigation.getParam('guid') || ''
    let pageType = navigation.getParam('pageType') || ''
    if (pageType == 'CheckPage') {
      this.checkResData(guid)
      this.setState({ isCheckPage: true })
    } else if (pageType == 'BatchScan') {
      this.batchResData()
    } else {
      this.resData()
      /* DeviceStorage.get('DeviceStorageDataQI')
        .then(res => {
          if (res) {
            let DeviceStorageDataObj = JSON.parse(res)
            DeviceStorageData = DeviceStorageDataObj
            if (res.length != 0) {
              this.setState({
                "isGetInspector": DeviceStorageDataObj.isGetInspector,
                "isGetTeamPeopleInfo": DeviceStorageDataObj.isGetTeamPeopleInfo,
                "InspectorId": DeviceStorageDataObj.InspectorId,
                "InspectorName": DeviceStorageDataObj.InspectorName,
                "InspectorSelect": DeviceStorageDataObj.InspectorName,
                "monitorId": DeviceStorageDataObj.monitorId,
                "monitorName": DeviceStorageDataObj.monitorName,
                "monitorSelect": DeviceStorageDataObj.monitorName,
                "monitorTeamName": DeviceStorageDataObj.monitorTeamName,
              })
            }
          }
        }).catch(err => {
          console.log("🚀 ~ file: loading.js ~ line 131 ~ SteelCage ~ componentDidMount ~ err", err)
        }) */
    }

    //通过使用DeviceEventEmitter模块来监听事件
    this.iDataScan = DeviceEventEmitter.addListener('iDataScan', (Event) => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 115 ~ SteelCage ~ Event.ScanResult", Event.ScanResult)
      if (typeof Event.ScanResult != undefined) {
        let data = Event.ScanResult
        let arr = data.split("=");
        let id = ''
        id = arr[arr.length - 1]
        if (this.state.type == 'Stockid') {
          this.GetcomID(id)
        } else if (this.state.type == "Keeperid") {
          this.GetkeepID(id);
        }
      }
    });
  }

  componentWillUnmount() {
    compDataArr = [], compIdArr = [], tmpstr = '', visibleArr = []
    this.iDataScan.remove()
  }

  UpdateControl = () => {
    if (typeof this.props.navigation.getParam('QRid') != "undefined") {
      if (this.props.navigation.getParam('QRid') != "") {
        this.toast.show(Url.isLoadingView, 0)
      }
    }
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || ''
    const type = navigation.getParam('type') || '';

    if (type == 'Stockid') {
      this.GetcomID(QRid)
    } else if (type == "Keeperid") {
      this.GetkeepID(QRid);
    }
  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  checkResData = (guid) => {
    let formData = new FormData();
    let data = {};
    data1 = { "action": "ObtainsteelCageStorage", "servicetype": "pda", "express": "249E955B", "ciphertext": "ddd23c2aae6f7b7e6e591356efe0edd2", "data": { "guid": "684C02CE429643EF9E4E8F8EE715EFFD", "factoryId": "74e1cbc6-2cfc-4d6f-bcd1-739d28e4c74d" } }
    data = {
      "action": "ObtainsteelCageStorage",
      "servicetype": "pda",
      "express": "249E955B",
      "ciphertext": "ddd23c2aae6f7b7e6e591356efe0edd2",
      "data": {
        "guid": guid,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: steel_cage_storage.js:184 ~ SteelCage ~ formData:", formData)

    /* fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json()
  }).catch(err => {
    }) */

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.setState({
        isLoading: false
      })
      let formCode = resData.result.formCode
      let team = resData.result.team
      let teamName = resData.result.teamName
      let labourName = resData.result.labourName
      let room = resData.result.room
      let roomName = resData.result.roomName
      let keeperName = resData.result.keeper
      let ServerTime = resData.result.time
      let editEmployeeName = resData.result.editEmployeeName
      let guid = resData.result.guid
      let steelCageCode = '', steelCage = ''
      compDataArr = resData.result.component
      compDataArr.map((item, index) => {
        steelCageCode = item.steelCageCode
        steelCage = item.steelCage[0]
        compIdArr.push(steelCage.steelCageID)
      })
      this.setState({
        resData: resData,
        formCode: formCode,
        ServerTime: ServerTime,
        room: room,
        roomselect: roomName,
        team: team,
        teamselect: teamName,
        keeperselect: keeperName,
        labourName: labourName,
        compDataArr: compDataArr,
        compIdArr: compIdArr,
        steelCageCode: steelCageCode,
        steelCage: steelCage,
        editEmployeeName: editEmployeeName,
        rowguid: guid,
      })
    }).catch((error) => {
    });
  }

  resData = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "GetFormAndTimeAndStoreroomAndTeamAndLabourforsteelCageStorage",
      "servicetype": "pda",
      "express": "249E955B",
      "ciphertext": "ddd23c2aae6f7b7e6e591356efe0edd2",
      "data": {
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: steel_cage_storage.js:260 ~ SteelCage ~ formData:", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      //初始化错误提示
      if (resData.status == 100) {
        let formCode = resData.result.formCode
        let monitorSetup = resData.result.monitorSetup
        let team = resData.result.team
        let room = resData.result.room
        let keeper = resData.result.keeper
        let ServerTime = resData.result.ServerTime
        let rowguid = resData.result.rowguid
        this.setState({
          resData: resData,
          formCode: formCode,
          ServerTime: ServerTime,
          room: room,
          team: team,
          keeper: keeper,
          rowguid: rowguid,
          monitorSetup: monitorSetup,
          isLoading: false
        })
        if (monitorSetup == '扫码') {
          this.setState({
            focusIndex: 2,
            type: "Keeperid"
          })
        }

      }
      else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }

    }).catch((error) => {
    });
  }

  batchResData = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "batchcageinstorageinit",
      "servicetype": "batchsteelcage",
      "express": "588DF83A",
      "ciphertext": "ba3f26d8fd1c527a0bc4362f761eab2e",
      "data": { "_bizid": Url.PDAFid }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: steel_cage_storage.js ~ line 35 ~ SteelCage ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      //初始化错误提示
      if (resData.status == 100) {
        let rowguid = resData.result.rowguid
        let formCode = resData.result.formCode
        let ServerTime = resData.result.inStorageTime
        let room = resData.result.storageRooms
        let team = resData.result.teams
        let keeper = resData.result.Keepers
        let steelCages = resData.result.steelCages
        let listBatchNoInfo = resData.result.steelCages
        this.setState({
          resData: resData,
          formCode: formCode,
          rowguid: rowguid,
          ServerTime: ServerTime,
          room: room,
          team: team,
          keeper: keeper,
          listBatchNoInfo: listBatchNoInfo,
          isBatch: true,
          isLoading: false
        })
      }
      else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }

    }).catch((error) => {
      console.log("🚀 ~ file: steel_cage_storage.js ~ line 75 ~ SteelCage ~ error", error)
    });

  }

  QRIdtrans = (type) => {
    const { navigation } = this.props;
    const QRid = navigation.getParam('QRid') || '';
    if (type == 'keeper') {
      this.state.keeper.map((item, index) => {
        if (QRid == item.keeperId) {
          this.setState({
            keeperselect: item.keeperName
          })
        }
      })
    }
  }

  GetcomID = (id) => {
    let formData = new FormData();
    let data = {};
    if (this.state.isBatch) {
      data = {
        "action": "getcagebatchnoinfo",
        "servicetype": "batchsteelcage",
        "express": "B1BED500",
        "ciphertext": "16d1066a518469711de179e1f79db4d3",
        "data": {
          "steelCageId": id,
          "_bizid": Url.PDAFid
        }
      }
    } else {
      data = {
        "action": "GetSteelCageInfoForStorage",
        "servicetype": "pda",
        "express": "227EB695",
        "ciphertext": "de110251c4c1faf0038a3416d728a830",
        "data": {
          "steelCageID": id || '',
          "factoryId": Url.PDAFid
        }
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: steel_cage_storage.js:420 ~ SteelCage ~ formData:", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
       this.toast.close()
      if (resData.status == '100') {
        if (this.state.isBatch) {
          compDataArr = [], compIdArr = [], tmpstr = ''
        }
        let compData = resData.result
        let steelCageCode = compData.steelCageCode
        if (this.state.isBatch) {
          compData.steelCage = []
          compData.steelCage.push(resData.result)
        }
        let steelCage = compData.steelCage[0]
        if (compDataArr.length == 0) {
          compDataArr.push(compData)
          compIdArr.push(steelCage.steelCageID)
          this.setState({
            compDataArr: compDataArr,
            compIdArr: compIdArr,
            compData: compData,
            steelCage: steelCage,
            steelCageCode: steelCageCode,
            isGetcomID: true
          })
          if (this.state.isBatch) {
            this.setState({
              number: compData.number,
              numberEdit: compData.number
            })
          }
          tmpstr = tmpstr + compIdArr.toString() + ' '
        } else {
          if (tmpstr.indexOf(steelCage.steelCageID) == -1) {
            compDataArr.push(compData)
            compIdArr.push(steelCage.steelCageID)
            this.setState({
              compDataArr: compDataArr,
              compIdArr: compIdArr,
              compData: compData,
              steelCage: steelCage,
              steelCageCode: steelCageCode,
              isGetcomID: true
            })
            tmpstr = tmpstr + compIdArr.toString() + ' '
          } else {
            this.toast.show('已经扫瞄过此钢筋笼')
          }

        }
        this.setState({
          compId: ""
        })
      } else {
        Alert.alert('错误', resData.message)
        this.setState({
          compId: ""
        })
        //this.input1.focus()
      }

    }).catch(err => {
      this.toast.show('二维码获取错误')
    })
  }

  comIDItem = () => {
    const { isGetcomID, buttondisable, compCode } = this.state
    return (
      <View>
        {
          !isGetcomID ?
            <View>
              <View>
                <Input
                  ref={ref => { this.input1 = ref }}
                  containerStyle={styles.scan_input_container}
                  inputContainerStyle={styles.scan_inputContainerStyle}
                  inputStyle={[styles.scan_input]}
                  placeholder='请输入'
                  keyboardType='numeric'
                  value={compId}
                  onChangeText={(value) => {
                    value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
                    value = value.replace(/^\./g, ""); //验证第一个字符是数字
                    value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
                    //value = value.replace(/\b(0+)/gi, ""); //清楚开头的0
                    this.setState({
                      compId: value
                    })
                  }}
                  //onSubmitEditing={() => { this.input1.focus() }}
                  //onFocus={() => { this.setState({ focusIndex: '5' }) }}
                  /*  */
                  //returnKeyType = 'previous'
                  onSubmitEditing={() => {
                    let inputComId = this.state.compId
                    console.log("🚀 ~ file: qualityinspection.js ~ line 468 ~ QualityInspection ~ inputComId", inputComId)
                    inputComId = inputComId.replace(/\b(0+)/gi, "")
                    this.GetcomID(inputComId)

                  }}
                />
              </View>
              <Button

                title='扫码'
                type='solid'
                buttonStyle={{ height: 24 }}
                onPress={() => {
                  this.setState({
                    iscomIDdelete: false,
                    buttondisable: true
                  })
                  setTimeout(() => {
                    this.setState({ GetError: false, buttondisable: false })
                    varGetError = false
                  }, 1500)
                  this.props.navigation.navigate('QRCode', {
                    type: 'compid',
                    page: 'PDAStorage'
                  })
                }}
              />
            </View>
            :
            <TouchableCustom
              onPress={() => {
                console.log('onPress')
              }}
              onLongPress={() => {
                console.log('onLongPress')
                Alert.alert(
                  '提示信息',
                  '是否删除构件信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      this.setState({
                        compData: [],
                        compCode: '',
                        compId: '',
                        iscomIDdelete: true,
                        isGetcomID: false,
                      })
                    }
                  }])
              }} >
              <Text>{compCode}</Text>
            </TouchableCustom>
        }
      </View>

    )
  }

  GetkeepID = id => {
    let formData = new FormData();
    let data = {};
    data = {
      action: "Getkeeper",
      servicetype: "pda",
      express: "227EB695",
      ciphertext: "de110251c4c1faf0038a3416d728a830",
      data: {
        keeperId: id,
        factoryId: Url.PDAFid
      }
    };
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == "100") {
        let compData = resData.result;
        console.log(
          "🚀 ~ file: steelCageStorage.jsx ~ line 455 ~ steelCageStorage ~ compData",
          compData
        );
        let keeperId = compData.keeperId;
        let keeperName = compData.keeperName;
        this.setState({
          keeperId: keeperId,
          keeperselect: keeperName,
          isGetkeepID: true
        });
        this.setState({
          focusIndex: 3,
          type: "Stockid"
        })
        setTimeout(() => { this.scoll.scrollToEnd() }, 300)
      } else {
        Alert.alert('ERROR', resData.message)
      }

    }).catch(err => {
    })

  };

  _DatePicker = (index) => {
    const { ServerTime } = this.state
    return (
      <DatePicker
        customStyles={{
          dateInput: styles.dateInput,
          dateTouchBody: styles.dateTouchBody,
          dateText: styles.dateText,

        }}
        iconComponent={<Icon name='caretdown' color='#419fff' iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
        }
        showIcon={true}
        mode='datetime'
        date={ServerTime}
        format="YYYY-MM-DD HH:mm"
        onOpenModal={() => { this.setState({ focusIndex: index }) }}
        onDateChange={(value) => {
          this.setState({
            ServerTime: value
          })
        }}
      />
    )
  }

  PostData = () => {
    console.log('QRStock', QRStock)
    console.log('QRkeeperID', QRkeeperID)
    this.setState({
      QRkeeperID: QRkeeperID,
      QRStock: QRStock
    })
    const { isBatch, number, numberEdit, formCode, teamId, keeperId, ServerTime, roomId, rowguid, compIdArr } = this.state
    const { navigation } = this.props;
    const QRid = navigation.getParam('QRid') || '';

    let steelCageArray = []

    if (roomId == '') {
      this.toast.show('库房信息不能为空');
      return
    } else if (teamId == '') {
      this.toast.show('班组信息不能为空');
      return
    } else if (keeperId == '' && QRkeeperID == '') {
      this.toast.show('库管员信息不能为空');
      return
    } else if (compIdArr.length == 0) {
      this.toast.show('入库明细信息不能为空');
      return
    }

    if (isBatch) {

      compDataArr.map((item, index) => {
        let tmp = {}
        tmp.number = numberEdit
        tmp.steelCageId = item.steelCageId
        tmp.steelCageWeight = 0
        steelCageArray.push(tmp)
      })
    }
    this.toast.show('保存中...', 0)

    let steelCageIDs = compIdArr.toString()

    let formData = new FormData();
    let data = {};
    if (isBatch) {
      data = {
        "action": "savebatchcageinstorage",
        "servicetype": "batchsteelcage",
        "express": "A8FE84DA",
        "ciphertext": "02f5aae7b467821ea96c2c34a07798af",
        "data": {
          "teamId": teamId,
          "inStorageTime": ServerTime,
          "keeperId": keeperId,
          "userName": Url.PDAusername,
          "_bizid": Url.PDAFid,
          "rowguid": rowguid,
          "roomId": roomId,
          "steelCageArray": steelCageArray
        }
      }
    } else {
      data = {
        "action": "SaveSteelCageStorage",
        "servicetype": "pda",
        "express": "4ED13340",
        "ciphertext": "3221ac6498a0e8b94ef042a37a728588",
        "data": {
          "formCode": formCode,
          "Username": Url.PDAusername,
          "factoryId": Url.PDAFid,
          "teamId": teamId,
          "steelCageIDs": steelCageIDs,
          "keeperId": keeperId || QRkeeperID,
          "ServerTime": ServerTime,
          "roomId": roomId,
          "rowguid": rowguid
        }
      }
    }

    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: steel_cage_storage.js:743 ~ SteelCage ~ formData:", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('保存成功');
        setTimeout(() => {
          this.props.navigation.replace(this.props.navigation.getParam("page"), {
            title: this.props.navigation.getParam("title"),
            pageType: this.props.navigation.getParam("pageType"),
            page: this.props.navigation.getParam("page"),
            isAppPhotoSetting: this.props.navigation.getParam("isAppPhotoSetting"),
            isAppDateSetting: this.props.navigation.getParam("isAppDateSetting"),
            isAppPhotoAlbumSetting: this.props.navigation.getParam("isAppPhotoAlbumSetting"),
          })
          //this.props.navigation.navigate('PDAMainStack')
        }, 400)
      } else {
        this.toast.show('保存失败')
        Alert.alert('保存失败', resData.message)
      }
    }).catch((error) => {
      this.toast.show('保存失败')
      if (error.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('保存失败', "响应内容不是合法JSON格式")
        return
      }
      if (error.toString().indexOf('Network request faile') != -1) {
        Alert.alert('保存失败', "网络请求错误")
        return
      }
      Alert.alert('保存失败', error.toString())
    });
  }

  CardList = (compData) => {
    console.log("🚀 ~ file: steel_cage_storage.js:790 ~ SteelCage ~ compData:", compData)
    return (
      compData.map((item, index) => {
        let comp = {}
        comp = item.steelCage[0]
        return (
          <View style={styles.CardList_main}>
            <TouchableCustom
              onPress={() => {
                this.cardListContont(comp);
              }}
              onLongPress={() => {
                Alert.alert(
                  '提示信息',
                  '是否删除钢筋笼信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      compDataArr.splice(index, 1)
                      compIdArr.splice(index, 1)
                      tmpstr = compIdArr.toString()

                      this.setState({
                        compDataArr: compDataArr,
                        compIdArr: compIdArr
                      })
                    }
                  }])

              }}>
              <View style={styles.CardList_title_main}>
                <View style={styles.title_num}>
                  <Text >{index + 1}</Text>
                </View>
                <View style={styles.title_title}>
                  <Text >{item.steelCageCode}</Text>
                </View>
                <View style={styles.CardList_at_icon}>
                  <IconDown />
                </View>
              </View>
            </TouchableCustom>

            {/* <ListItem
              key={index}
              onPress={() => {
                this.cardListContont(comp);
              }}

              containerStyle={[{ backgroundColor: '#36A7FF' }]}
              title={(index + 1) + '      ' + '钢筋笼编码' + item.steelCageCode}
              titleStyle={{ color: 'white' }}
              rightElement={
                <View style={{ flexDirection: 'row' }}>
                  <Icon name={this.state.hidden == index ? 'down' : 'up'} type='antdesign' color='white' />
                </View>
              }
              bottomDivider >
            </ListItem> */}
            {
              visibleArr.indexOf(comp.steelCageCode) != -1 ?
                <View key={index} style={{ backgroundColor: 'transparent' }} >
                  <ListItemScan_child
                    title='钢筋笼编码'
                    rightTitle={comp.steelCageCode}
                  />
                  <ListItemScan_child
                    title='项目名称'
                    rightTitle={comp.projectName}
                  />
                  <ListItemScan_child
                    title='构件类型'
                    rightTitle={comp.compTypeName}
                  />
                  <ListItemScan_child
                    title='设计型号'
                    rightTitle={comp.designType}
                  />
                  <ListItemScan_child
                    title='钢筋笼重量'
                    rightTitle={comp.steelCageWeight}
                  />
                  <View style={{ height: 10 }}></View>
                </View> : <View></View>
            }

          </View>
        )
      })
    )
  }

  isBottomVisible = (data, focusIndex) => {
    if (typeof (data) != "undefined") {
      if (data.length > 0) {
        this.setState({ bottomVisible: true, bottomData: data, focusIndex: focusIndex })
      } else {
        this.toast.show("数据为空")
      }
    } else {
      this.toast.show("无数据")
    }
  }

  cardListContont = comp => {
    if (visibleArr.indexOf(comp.steelCageCode) == -1) {
      visibleArr.push(comp.steelCageCode);
      this.setState({ visibleArr: visibleArr });
    } else {
      if (visibleArr.length == 1) {
        visibleArr.splice(0, 1);
        this.setState({ visibleArr: visibleArr }, () => {
          this.forceUpdate;
        });
      } else {
        visibleArr.map((item, index) => {
          if (item == comp.steelCageCode) {
            visibleArr.splice(index, 1);
            this.setState({ visibleArr: visibleArr }, () => {
              this.forceUpdate;
            });
          }
        });
      }
    }
  };

  batchListView = () => {
    return (
      <TouchableCustom underlayColor={'lightgray'} onPress={() => {
        this.isBottomVisible(this.state.listBatchNoInfo, "11")
      }}>

        <ListItemScan
          isButton={false}
          focusStyle={this.state.focusIndex == '11' ? styles.focusColor : {}}
          title='生产批次'
          rightElement={
            <IconDown text={this.state.batchCompCodeselect}></IconDown>
          }
        />
      </TouchableCustom>
    )
  }


  BottomSheetBatchMain = () => {
    const { bottomVisible, bottomData, isCheckPage } = this.state;
    return (
      <BottomSheet
        isVisible={bottomVisible && !isCheckPage}
        onRequestClose={() => {
          this.setState({
            bottomVisible: false
          })
        }}
        onBackdropPress={() => {
          this.setState({
            bottomVisible: false
          })
        }}
      >
        <ScrollView style={styles.bottomsheetScroll}>
          {
            bottomData.map((item, index) => {
              let title = ''
              let dataItem = {}
              if (item.steelCageId) {
                dataItem.Title = item.steelCageCode
                dataItem.rightTitle = item.number
                dataItem.subTitle = ""
                dataItem.subRightTitle = item.projectName
              } else if (item.roomName) {
                title = item.roomName
              } else if (item.teamName) {
                title = item.teamName
              } else if (item.keeperName) {
                title = item.keeperName
              } else if (item.keeper) {
                title = item.keeper
              }

              return (
                <TouchableOpacity
                  onPress={() => {
                    if (item.steelCageId) {
                      this.setState({
                        steelCageCode: item.steelCageCode,
                        batchCompCode: item.steelCageCode,
                        batchCompCodeselect: item.steelCageCode,
                        steelCageID: item.steelCageId,
                      })
                      this.GetcomID(item.steelCageId)
                    } else if (item.roomId) {
                      this.setState({
                        roomId: item.roomId,
                        roomselect: item.roomName
                      })
                    } else if (item.teamId) {
                      this.setState({
                        teamId: item.teamId,
                        teamselect: item.teamName,
                        labourName: item.labourName
                      })
                    } else if (item.keeperName) {
                      this.setState({
                        keeperId: item.keeperId,
                        keeperName: item.keeperName,
                        keeperselect: item.keeperName
                      })
                    } else {
                      this.setState({
                        keeperId: item.keeperId,
                        keeperName: item.keeper,
                        keeperselect: item.keeper
                      })
                    }

                    this.setState({
                      bottomVisible: false
                    })
                  }}
                >
                  {
                    !item.steelCageId ? <BottomItem backgroundColor='white' color="#333" title={title} /> :
                      <BatchListItem backgroundColor='white' color="#333" dataItem={dataItem} />
                  }
                </TouchableOpacity>
              )

            })
          }
        </ScrollView>
        <TouchableCustom
          onPress={() => {
            this.setState({ bottomVisible: false })
          }}>
          <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
        </TouchableCustom>
      </BottomSheet>
    )
  }

  render() {
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || '';
    const type = navigation.getParam('type') || '';

    const { isBatch, number, numberEdit, resData, isGetkeepID, compDataArr, focusIndex, monitorSetup, formCode, ServerTime, team, teamselect, keeper, keeperselect, room, roomselect, labourName, bottomVisible, bottomData, compId } = this.state

    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (

        <ScrollView ref={ref => this.scoll = ref} style={styles.mainView} showsVerticalScrollIndicator={false} >
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          <NavigationEvents
            onWillFocus={this.UpdateControl} />
          <View style={styles.listView}>
            <ListItemScan
              title='表单编号'
              rightTitle={formCode}
            />
            <TouchableCustom onPress={() => { this.isBottomVisible(room, '0') }} >
              <ListItemScan
                focusStyle={focusIndex == '0' ? styles.focusColor : {}}
                title='库房'
                rightTitle={
                  <IconDown text={roomselect} />
                }
              />
            </TouchableCustom>
            <TouchableCustom onPress={() => { this.isBottomVisible(team, '1') }} >
              <ListItemScan
                focusStyle={focusIndex == '1' ? styles.focusColor : {}}
                title='加工班组'
                rightTitle={
                  <IconDown text={teamselect} />
                }
              />
            </TouchableCustom>
            <ListItemScan
              title='劳务队'
              rightTitle={labourName}
            />
            <ListItemScan
              title='操作人'
              rightTitle={this.state.editEmployeeName}
            />
            <TouchableCustom underlayColor={'lightgray'} onPress={() => {
              if (monitorSetup != '扫码') {
                this.isBottomVisible(keeper, '2')
              }
            }}>
              <ListItemScan
                isButton={monitorSetup == '扫码' && !isGetkeepID}
                onPressIn={() => {
                  console.log("🚀 ~ file: steel_cage_storage.js ~ line 1111 ~ SteelCage ~ render ~ onPressIn", "onPressIn")
                  this.setState({
                    type: 'Keeperid',
                    focusIndex: 2
                  })
                  if (monitorSetup == '扫码') {
                    NativeModules.PDAScan.onScan();
                  } else {

                  }
                }}
                onPress={() => {
                  console.log("🚀 ~ file: steel_cage_storage.js ~ line 1111 ~ SteelCage ~ render ~ onPressIn", "onPressIn")
                  this.setState({
                    type: 'Keeperid',
                    focusIndex: 2
                  })
                  if (monitorSetup == '扫码') {
                    NativeModules.PDAScan.onScan();
                  }
                }}
                onPressOut={() => {
                  if (monitorSetup == '扫码') {
                    NativeModules.PDAScan.offScan();
                  }
                }}
                focusStyle={focusIndex == '2' ? styles.focusColor : {}}
                title='库管员'
                rightTitle={
                  monitorSetup == '扫码' ?
                    !isGetkeepID ?
                      < ScanButton
                        onPress={() => {
                          this.setState({ focusIndex: 2 })
                          this.props.navigation.navigate('QRCode', {
                            type: 'Keeperid',
                            page: 'SteelCageStorage'
                          })
                        }}
                        onLongPress={() => {
                          console.log("🚀 ~ file: steel_cage_storage.js ~ line 1111 ~ SteelCage ~ render ~ onPressIn", "onPressIn")
                          this.setState({
                            type: 'Keeperid',
                            focusIndex: 2
                          })
                          NativeModules.PDAScan.onScan();
                        }}
                        onPressOut={() => {
                          NativeModules.PDAScan.offScan();
                        }}
                      />
                      : <TouchableCustom
                        onPress={() => {
                          console.log('onPress')
                        }}
                        onLongPress={() => {
                          console.log('onLongPress')
                          Alert.alert(
                            '提示信息',
                            '是否删除库管员信息',
                            [{
                              text: '取消',
                              style: 'cancel'
                            }, {
                              text: '删除',
                              onPress: () => {
                                this.setState({
                                  keeperId: "",
                                  keeperselect: "",
                                  isGetkeepID: false
                                });
                              }
                            }])
                        }} >
                        <Text>{keeperselect}</Text>
                      </TouchableCustom>
                    :
                    <TouchableOpacity onPress={() => { this.setState({ bottomVisible: true, bottomData: keeper, focusIndex: 2 }) }} >
                      <IconDown text={keeperselect} />
                    </TouchableOpacity>
                }
              />
            </TouchableCustom>

            <ListItemScan
              focusStyle={focusIndex == '4' ? styles.focusColor : {}}
              title='入库日期'
              rightTitle={this._DatePicker(4)}
              rightTitleStyle={{ backgroundColor: 'red' }}
            />

            {
              isBatch ? this.batchListView() :
                <ListItemScan
                  isButton={true}
                  onPress={() => {
                    console.log("🚀 ~ file: steel_cage_storage.js ~ line 1111 ~ SteelCage ~ render ~ onPressIn", "onPressIn")
                    this.setState({
                      type: 'Stockid',
                      focusIndex: 3
                    })
                    NativeModules.PDAScan.onScan();
                  }}
                  onPressIn={() => {
                    console.log("🚀 ~ file: steel_cage_storage.js ~ line 1111 ~ SteelCage ~ render ~ onPressIn", "onPressIn")
                    this.setState({
                      type: 'Stockid',
                      focusIndex: 3
                    })
                    NativeModules.PDAScan.onScan();
                  }}
                  onPressOut={() => {
                    NativeModules.PDAScan.offScan();
                  }}
                  focusStyle={focusIndex == '3' ? styles.focusColor : {}}
                  title='入库明细'
                  rightTitle={
                    <View style={{ flexDirection: 'row' }}>
                      <View>
                        <Input
                          ref={ref => { this.input1 = ref }}
                          containerStyle={styles.scan_input_container}
                          inputContainerStyle={styles.scan_inputContainerStyle}
                          inputStyle={[styles.scan_input]}
                          placeholder='请输入'
                          keyboardType='numeric'
                          value={compId}
                          onChangeText={(value) => {
                            value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
                            value = value.replace(/^\./g, ""); //验证第一个字符是数字
                            value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
                            //value = value.replace(/\b(0+)/gi, ""); //清楚开头的0
                            this.setState({
                              compId: value
                            })
                          }}
                          onFocus={() => {
                            this.setState({
                              focusIndex: 3,
                              type: 'Stockid',
                            })
                          }}
                          onSubmitEditing={() => {
                            let inputComId = this.state.compId
                            console.log("🚀 ~ file: qualityinspection.js ~ line 468 ~ QualityInspection ~ inputComId", inputComId)
                            inputComId = inputComId.replace(/\b(0+)/gi, "")
                            this.GetcomID(inputComId)

                          }}
                        />
                      </View>
                      <ScanButton
                        onPress={() => {
                          this.setState({
                            isGetcomID: false,
                            focusIndex: 3
                          })
                          this.props.navigation.navigate('QRCode', {
                            type: 'Stockid',
                            page: 'SteelCageStorage'
                          })
                        }}
                        onLongPress={() => {
                          this.setState({
                            type: 'Stockid',
                            focusIndex: 3
                          })
                          NativeModules.PDAScan.onScan();
                        }}
                        onPressOut={() => {
                          NativeModules.PDAScan.offScan();
                        }}
                      />
                    </View>
                  }
                />
            }

            {
              compDataArr.length != 0 ? this.CardList(compDataArr) : <View></View>
            }

            {
              isBatch ? <View>
                <ListItemScan
                  title='可入库数量'
                  rightTitle={number}
                />
                <ListItemScan
                  title='入库数量'
                  rightElement={
                    <View>
                      <Input
                        containerStyle={styles.quality_input_container}
                        inputContainerStyle={styles.inputContainerStyle}
                        inputStyle={[styles.quality_input_, { top: 7 }]}
                        placeholder={number == '0' ? '可编辑' : number.toString()}
                        value={numberEdit}
                        onChangeText={(value) => {
                          if (value > number) {
                            this.toast.show('不可超过可入库数量')
                            return
                          } else {
                            this.setState({
                              numberEdit: value
                            })
                          }
                        }} />
                    </View>
                  }
                />
              </View> : <View></View>
            }
          </View>


          {
            this.state.isCheckPage ? <View></View> :
              <FormButton
                backgroundColor='#17BC29'
                title='保存'
                onPress={() => {
                  this.PostData()
                }}
              />
          }

          {this.BottomSheetBatchMain()}
        </ScrollView>
      )
    }
  }

}