import React, { useEffect } from 'react';
import { View, StatusBar, TouchableOpacity, TouchableHighlight, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert, Keyboard, KeyboardAvoidingView } from 'react-native';
import { Button, Text, Input, Icon, Badge, Card, ListItem, BottomSheet, CheckBox, Avatar, Overlay, Header } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { deviceHeight, RFT } from '../../Url/Pixal';
import { ScrollView } from 'react-native-gesture-handler';
import CardList from "../Componment/CardList";
import DatePicker from 'react-native-datepicker'
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { Image } from 'react-native';
import { NavigationEvents } from 'react-navigation';
import { styles } from '../Componment/PDAStyles';
import ScanButton from '../Componment/ScanButton';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import ListItemScan_child from '../Componment/ListItemScan_child';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import CheckBoxScan from '../Componment/CheckBoxScan';
import AvatarAdd from '../Componment/AvatarAdd';
import AvatarImg from '../Componment/AvatarImg';
import DeviceStorage from '../../Url/DeviceStorage';
import Pdf from 'react-native-pdf';
import { DeviceEventEmitter, NativeModules } from 'react-native';
import reactNativeKeyevent from 'react-native-keyevent';
import overlayImg from '../Componment/OverlayImg';
import OverlayImgZoomPicker from '../Componment/OverlayImgZoomPicker';

let varGetError = false

let DeviceStorageData = {} //缓存数据

let fileflag = "8"

const { height, width } = Dimensions.get('window') //获取宽高

let QRcompid = '', QRtaiwanId = '', QRacceptanceId = '', QRmonitorId = '', QRInspectorId = ''

let imageArr = [], imageFileArr = [];

let showSoftInputOnFocus = false

class BackIcon extends React.PureComponent {
  render() {
    return (
      <TouchableCustom
        onPress={this.props.onPress} >
        <Icon
          name='arrow-back'
          color='#419FFF' />
      </TouchableCustom>
    )
  }
}

export default class ProductionStart extends React.PureComponent {
  constructor() {
    super();
    this.state = {
      resData: [],
      rowguid: '',
      ServerTime: '2000-01-01 00:00',
      steelCageIDs: ' ',
      type: 'compid',
      //班组信息
      teaminfos: [],
      teamId: '',
      teamName: '',
      teamNameSelect: '请选择',
      //专用模具
      acceptanceId: '',
      acceptanceName: '',
      //模台编码
      taiwanId: '',
      Tai_QRname: '',
      compData: {},
      compId: '',
      compCode: '',
      editEmployeeName: '',
      checked: true,
      //是否扫码成功
      isGetcomID: false,
      isGettaiwanId: false,
      isGetTeamPeopleInfo: false,
      isGetCommonMould: false,
      isGetInspector: false,
      //长按删除功能控制
      iscomIDdelete: false,
      istaiwanIddelete: false,
      isCommonMoulddelete: false,
      isTeamPeopledelete: false,
      isInspectordelete: false,
      //底栏控制
      bottomData: [],
      bottomVisible: false,
      datepickerVisible: false,
      CameraVisible: false,
      response: '',
      //照片控制
      pictureSelect: false,
      pictureUri: '',
      imageArr: [],
      imageFileArr: [],
      imageOverSize: false,
      fileid: "",
      recid: "",
      isPhotoUpdate: false,
      //
      GetError: false,
      buttondisable: false,
      //钢筋笼控制
      steelCageID: '',
      steelCageCode: '',
      isGetsteelCageInfoforPutMold: false,
      issteelCageIDdelete: false,
      focusIndex: 0,
      isLoading: true,
      //图纸
      isPaperTypeVisible: false,
      paperList: ["模版图", "配筋图", "预埋件装配图", "连接件排版图"],
      paperUrl: "",
      FileName: "",
      isPDFVisible: false,
      //查询界面取消选择按钮
      isCheck: false,
      isCheckPage: false,
      remark: '',
      //控制app拍照，日期，相册选择功能
      isAppPhotoSetting: false,
      currShowImgIndex: 0,
      isAppDateSetting: false,
      isAppPhotoAlbumSetting: false
    }
    //this.requestCameraPermission = this.requestCameraPermission.bind(this)
  }

  componentDidMount() {
    StatusBar.setBarStyle('dark-content')
    const { navigation } = this.props;
    let guid = navigation.getParam('guid') || ''
    let pageType = navigation.getParam('pageType') || ''
    let isAppPhotoSetting = navigation.getParam('isAppPhotoSetting')
    let isAppDateSetting = navigation.getParam('isAppDateSetting')
    let isAppPhotoAlbumSetting = navigation.getParam('isAppPhotoAlbumSetting')
    this.setState({
      isAppPhotoSetting: isAppPhotoSetting,
      isAppDateSetting: isAppDateSetting,
      isAppPhotoAlbumSetting: isAppPhotoAlbumSetting
    })
    if (pageType == 'CheckPage') {
      this.checkResData(guid)
      this.setState({ isCheckPage: true })
    } else {
      this.resData()
      DeviceStorage.get('DeviceStorageDataPS')
        .then(res => {
          if (res) {
            console.log("🚀 ~ file: qualityinspection.js ~ line 133 ~ QualityInspection ~ componentDidMount ~ res", res)
            let DeviceStorageDataObj = JSON.parse(res)
            DeviceStorageData = DeviceStorageDataObj
            console.log("🚀 ~ file: qualityinspection.js ~ line 136 ~ QualityInspection ~ componentDidMount ~ DeviceStorageData", DeviceStorageData)
            if (res.length != 0) {
              this.setState({
                "teamId": DeviceStorageDataObj.teamId,
                "teamName": DeviceStorageDataObj.teamName,
                "teamNameSelect": DeviceStorageDataObj.teamName,
              })
            }
          }
        }).catch(err => {
          console.log("🚀 ~ file: loading.js ~ line 131 ~ SteelCage ~ componentDidMount ~ err", err)

        })
    }

    //通过使用DeviceEventEmitter模块来监听事件
    this.iDataScan = DeviceEventEmitter.addListener('iDataScan', (Event) => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 115 ~ SteelCage ~ Event.ScanResult", Event.ScanResult)
      if (typeof Event.ScanResult != undefined) {
        let data = Event.ScanResult
        let arr = data.split("=");
        let id = ''
        id = arr[arr.length - 1]
        console.log("🚀 ~ file: concrete_pouring.js ~ line 118 ~ SteelCage ~ id", id)
        if (this.state.type == 'compid') {
          this.GetcomID(id)
        }
        if (this.state.type == 'taiwanId') {
          this.GettaiwanId(id)
        }
        if (this.state.type == 'acceptanceId') {
          this.GetCommonMould(id)
        }
        if (this.state.type == 'monitorId') {
          this.GetTeamPeopleInfo(id)
        }
        if (this.state.type == 'InspectorId') {
          this.GetInspector(id)
        }

      }
    });

  }

  componentWillUnmount() {
    imageArr = [], imageFileArr = [];
    this.iDataScan.remove()
  }

  UpdateControl = () => {
    if (typeof this.props.navigation.getParam('QRid') != "undefined") {
      if (this.props.navigation.getParam('QRid') != "") {
        this.toast.show(Url.isLoadingView, 0)
      }
    }
    const { navigation } = this.props;
    const { isGetcomID, isGetCommonMould, isGetInspector, isGetTeamPeopleInfo, isGettaiwanId, isGetsteelCageInfoforPutMold } = this.state
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || ''
    console.log("🚀 ~ file: qualityinspection.js ~ line 11113 ~ SteelCage ~ componentDidUpdate ~ QRid", QRid)
    let type = navigation.getParam('type') || '';
    console.log("🚀 ~ file: qualityinspection.js ~ line 105 ~ SteelCage ~ componentDidUpdate ~ this.state.GetError", this.state.GetError)
    {
      if (type == 'compid') {
        this.GetcomID(QRid)
      } else if (type == 'taiwanId') {
        this.GettaiwanId(QRid)
      } else if (type == 'acceptanceId') {
        this.GetCommonMould(QRid)
      } else if (type == 'monitorId') {
        this.GetTeamPeopleInfo(QRid)
      } else if (type == 'InspectorId') {
        this.GetInspector(QRid)
      }
    }
    console.log("🚀 ~ file: qualityinspection.js ~ line 11147 ~ SteelCage ~ componentDidUpdate ~ QRid", QRid)

  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  checkResData = (guid) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "ObtainProductStart",
      "servicetype": "pda",
      "express": "8AC4C0B0",
      "ciphertext": "7df83f712027c63f147f6c2d4a43f08d",
      "data": {
        "guid": guid,
        "factoryId": Url.PDAFid,
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: qualityinspection.js ~ line 189 ~ ProductionStart ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 204 ~ ProductionStart ~ resData", resData)
      let rowguid = resData.result.guid
      let ServerTime = resData.result.sjtime
      let compCode = resData.result.compCode
      let compId = resData.result.compId
      let taiWanName = resData.result.taiWanName
      let projectName = resData.result.projectName
      let acceptanceName = resData.result.acceptanceName
      let compTypeName = resData.result.compTypeName
      let designType = resData.result.designType
      let floorName = resData.result.floorName
      let floorNoName = resData.result.floorNoName
      let volume = resData.result.volume
      let weight = resData.result.weight
      let remark = resData.result.remark
      let componentImageUrl = resData.result.componentImageUrl
      let imageArr = [], imageFileArr = [];
      let tmp = {}
      tmp.fileid = ""
      tmp.uri = componentImageUrl
      tmp.url = componentImageUrl
      imageFileArr.push(tmp)
      this.setState({
        imageFileArr: imageFileArr
      })
      imageArr.push(componentImageUrl)

      //产品子表
      let compData = {}, result = {}
      result.projectName = projectName
      result.compTypeName = compTypeName
      result.designType = designType
      result.floorNoName = floorNoName
      result.floorName = floorName
      result.volume = volume
      result.weight = weight
      compData.result = result
      this.setState({
        resData: resData,
        rowguid: rowguid,
        ServerTime: ServerTime,
        compCode: compCode,
        compId: compId,
        Tai_QRname: taiWanName,
        acceptanceName: acceptanceName,
        compData: compData,
        remark: remark,
        pictureSelect: true,
        imageArr: imageArr,
        isGetcomID: true,
        isGettaiwanId: true,
        isGetTeamPeopleInfo: true,
        isGetCommonMould: true,
        isGetInspector: true,
        isCheck: true
      })
      this.setState({
        isLoading: false,
      })
    }).catch((error) => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 215 ~ ProductionStart ~ error", error)
    });
  }

  resData = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "GetTeamInfo",
      "servicetype": "pda",
      "express": "8AC4C0B0",
      "ciphertext": "7df83f712027c63f147f6c2d4a43f08d",
      "data": {
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      //初始化错误提示
      if (resData.status == 100) {
        let rowguid = resData.result.rowguid
        let ServerTime = resData.result.serverTime
        let teaminfos = resData.result.teaminfos
        this.setState({
          resData: resData,
          rowguid: rowguid,
          ServerTime: ServerTime,
          teaminfos: teaminfos,
          isLoading: false
        })
      }
      else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }
      console.log("🚀 ~ file: qualityinspection.js ~ line 314 ~ ProductionStart ~ resData", resData)


    }).catch((error) => {
    });

  }

  GetcomID = (id) => {
    console.log("🚀 ~ file: qualityinspection.js ~ line 194 ~ SteelCage ~ GetcomID", id)
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getCompInfo",
      "servicetype": "pda",
      "express": "0F51EF23",
      "ciphertext": "a91d6dc5c5a20f2a35939b6e2d91a0ef",
      "data": {
        "compId": id,
        "factoryId": Url.PDAFid
      }
    }
    console.log("🚀 ~ file: production_start.js ~ line 349 ~ ProductionStart ~ data", data)
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        compCode = resData.result.compCode
        let compId = resData.result.compId
        let steelLabel = resData.result.steelLabel
        this.setState({
          compData: resData,
          compCode: compCode,
          compId: compId,
          steelLabel: steelLabel,
          isGetcomID: true,
          iscomIDdelete: false
        })
        this.setState({
          focusIndex: 1,
          type: "taiwanId"
        })
        setTimeout(() => { this.scoll.scrollToEnd() }, 300)

      } else {
        console.log('resData', resData)
        Alert.alert('错误', resData.message)
        this.setState({
          compId: ""
        })
      }

    }).catch(err => {
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  comIDItem = (index) => {
    const { isGetcomID, buttondisable, compCode, compId } = this.state
    return (
      <View>
        {
          !isGetcomID ?
            <View style={{ flexDirection: 'row' }}>
              <View>
                <Input
                  ref={ref => { this.input1 = ref }}
                  containerStyle={styles.scan_input_container}
                  inputContainerStyle={styles.scan_inputContainerStyle}
                  inputStyle={[styles.scan_input]}
                  placeholder='请输入'
                  keyboardType='numeric'
                  value={compId}
                  onChangeText={(value) => {
                    value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
                    value = value.replace(/^\./g, ""); //验证第一个字符是数字
                    value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
                    //value = value.replace(/\b(0+)/gi, ""); //清楚开头的0
                    this.setState({
                      compId: value
                    })
                  }}
                  onSubmitEditing={() => {
                    let inputComId = this.state.compId
                    console.log("🚀 ~ file: qualityinspection.js ~ line 468 ~ QualityInspection ~ inputComId", inputComId)
                    if (inputComId.length == 16) {
                      inputComId = inputComId.replace(/\b(0+)/gi, "")
                      this.GetcomID(inputComId)
                    } else {
                      this.GetcomID(inputComId)
                    }
                  }}
                  onFocus={() => {
                    this.setState({
                      focusIndex: index,
                      type: 'compid',
                    })
                  }}
                />
              </View>
              <ScanButton

                onPress={() => {
                  this.setState({
                    iscomIDdelete: false,
                    buttondisable: true,
                    focusIndex: 0
                  })
                  setTimeout(() => {
                    this.setState({ GetError: false, buttondisable: false })
                    varGetError = false
                  }, 1500)
                  this.props.navigation.navigate('QRCode', {
                    type: 'compid',
                    page: 'ProductionStart'
                  })
                }}
                onLongPress={() => {
                  this.setState({
                    type: 'compid',
                    focusIndex: index
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}
              />
            </View>
            :
            <TouchableCustom
              onPress={() => {
                console.log('onPress')
              }}
              onLongPress={() => {
                console.log('onLongPress')
                Alert.alert(
                  '提示信息',
                  '是否删除构件信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      this.setState({
                        compData: [],
                        compCode: '',
                        compId: '',
                        iscomIDdelete: true,
                        isGetcomID: false,
                      })
                    }
                  }])
              }} >
              <Text>{compCode}</Text>
            </TouchableCustom>
        }
      </View>

    )
  }

  GetsteelCageInfoforPutMold = (id) => {
    console.log("🚀 ~ file: qualityinspection.js ~ line 346 ~ SteelCage ~ id", id)
    let formData = new FormData();
    let data = {};
    data = {
      "action": "GetsteelCageInfoforPutMold",
      "servicetype": "pda",
      "express": "6C8FAB14",
      "ciphertext": "96d2d1428051129167842f3b5eaca07f",
      "data": {
        "steelCageID": id,
        "compId": this.state.compId,
        "factoryId": Url.PDAFid
      }
    }
    console.log("🚀 ~ file: qualityinspection.js ~ line 349 ~ SteelCage ~ data", data)
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let steelCageID = resData.result.steelCageID
        let steelCageCode = resData.result.steelCageCode
        console.log("🚀 ~ file: qualityinspection.js ~ line 374 ~ SteelCage ~ steelCageCode", steelCageCode)
        this.setState({
          steelCageID: steelCageID,
          steelCageCode: steelCageCode,
          isGetsteelCageInfoforPutMold: true,
          issteelCageIDdelete: false
        })
      } else {
        console.log('resData', resData)
        Alert.alert('ERROR', resData.message)
        varGetError = true
        this.setState({
          GetError: true
        })
      }

    }).catch(err => {
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  GettaiwanId = (id) => {
    let Tai_QRname = '111'
    let formData = new FormData();
    let data = {};
    data = {
      "action": "gettaiwanInfo",
      "servicetype": "pda",
      "express": "8FBA0BDC",
      "ciphertext": "591057f7d292870338e0fdffe9ef719f",
      "data": {
        "factoryId": Url.PDAFid,
        "taiwanId": id
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let taiwanId = resData.result.taiwanId
        Tai_QRname = resData.result.Tai_QRname
        this.setState({
          taiwanId: taiwanId,
          Tai_QRname: Tai_QRname,
          isGettaiwanId: true,
          istaiwanIddelete: false
        })
        this.scoll.scrollToEnd()
        this.setState({
          focusIndex: 2,
          type: "acceptanceId",
        })
      } else {
        Alert.alert('ERROR', resData.message)
      }

    }).catch(err => {
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  taiwanItem = (index) => {
    const { isGettaiwanId, buttondisable, Tai_QRname, taiwanId } = this.state
    return (
      <View>
        {
          !isGettaiwanId ?
            <View>
              <ScanButton
                onPress={() => {
                  this.setState({
                    focusIndex: 1,
                    istaiwanIddelete: false,
                    buttondisable: true
                  })
                  this.props.navigation.navigate('QRCode', {
                    type: 'taiwanId',
                    page: 'ProductionStart'
                  })
                }}
                onLongPress={() => {
                  this.setState({
                    type: 'taiwanId',
                    focusIndex: index
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}
              />
            </View>
            :
            <TouchableCustom
              onPress={() => {
                console.log('onPress')
              }}
              onLongPress={() => {
                console.log('onLongPress')
                Alert.alert(
                  '提示信息',
                  '是否删除模台信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      this.setState({
                        taiwanId: '',
                        Tai_QRname: '',
                        isGettaiwanId: false,
                        istaiwanIddelete: true,
                      })
                    }
                  }])
              }} >
              <Text>{Tai_QRname}</Text>
            </TouchableCustom>
        }
      </View>
    )
  }

  GetCommonMould = (id) => {
    let acceptanceName = ''
    let formData = new FormData();
    let data = {};
    data = {
      "action": "GetCommonMould",
      "servicetype": "pda",
      "express": "50545047",
      "ciphertext": "f3eedf54128226aef62dd6023a1c1573",
      "data": {
        "compId": this.state.compId,
        "acceptanceId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let acceptanceId = resData.result.acceptanceId
        acceptanceName = resData.result.acceptanceName
        this.setState({
          acceptanceId: acceptanceId,
          acceptanceName: acceptanceName,
          isGetCommonMould: true,
          isCommonMoulddelete: false
        })
        this.scoll.scrollToEnd()
        if (this.state.steelLabel == '使用') {
          this.setState({
            focusIndex: 3,
            type: "steelCageID",
          })
        }
      } else {
        Alert.alert('ERROR', resData.message)
      }


    }).catch(err => {
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  GetTeamPeopleInfo = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getTeamPeopleInfo",
      "servicetype": "pda",
      "express": "2B9B0E37",
      "ciphertext": "eb1a5891270a0ebdbb68d37e27f82b92",
      "data": {
        "factoryId": Url.PDAFid,
        "monitorId": id
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let monitorId = resData.result.monitorId
        let monitorName = resData.result.monitorName
        let teamId = resData.result.teamId
        let teamName = resData.result.teamName
        this.setState({
          monitorId: monitorId,
          monitorName: monitorName,
          monitorTeamId: teamId,
          monitorTeamName: teamName,
          isGetTeamPeopleInfo: true,
          isTeamPeopledelete: false
        })
      } else {
        Alert.alert('ERROR', resData.message)
        varGetError = true
        this.setState({
          GetError: true
        })
      }
    }).catch(err => {
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  monitorNameItem = (index) => {
    const { isGetTeamPeopleInfo, buttondisable, monitorName, monitorSetup, monitors, monitorSelect } = this.state
    console.log("🚀 ~ file: qualityinspection.js ~ line 559 ~ SteelCage ~ monitors", monitors)
    return (
      <View>
        {
          monitorSetup == '扫码' ?
            (
              !isGetTeamPeopleInfo ?
                <View>
                  <ScanButton

                    onPress={() => {
                      this.setState({
                        isTeamPeopledelete: false,
                        focusIndex: index
                      })
                      setTimeout(() => {
                        this.setState({ GetError: false, buttondisable: false })
                        varGetError = false
                      }, 1500)
                      this.props.navigation.navigate('QRCode', {
                        type: 'monitorId',
                        page: 'ProductionStart'
                      })
                    }}
                  />
                </View>
                :
                <TouchableCustom
                  onPress={() => {
                    console.log('onPress')
                  }}
                  onLongPress={() => {
                    console.log('onLongPress')
                    Alert.alert(
                      '提示信息',
                      '是否删除班组信息',
                      [{
                        text: '取消',
                        style: 'cancel'
                      }, {
                        text: '删除',
                        onPress: () => {
                          this.setState({
                            monitorId: '',
                            monitorName: '',
                            monitorTeamId: '',
                            monitorTeamName: '',
                            isGetTeamPeopleInfo: false,
                            isTeamPeopledelete: true
                          })
                        }
                      }])
                  }} >
                  <Text>{monitorName}</Text>
                </TouchableCustom>
            )
            :
            (
              this.state.isCheck ? <Text >{monitorSelect}</Text> :
                <TouchableCustom onPress={() => { this.isBottomVisible(monitors, index) }} >
                  <IconDown text={monitorSelect} />
                </TouchableCustom>
            )

        }
      </View>
    )
  }

  GetInspector = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getInspector",
      "servicetype": "pda",
      "express": "CDAB7A2D",
      "ciphertext": "a590b603df9b5a4384c130c7c21befb7",
      "data": {
        "InspectorId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let InspectorId = resData.result.InspectorId
        let InspectorName = resData.result.InspectorName
        this.setState({
          InspectorId: InspectorId,
          InspectorName: InspectorName,
          isGetInspector: true,
          isInspectordelete: false
        })
      } else {
        Alert.alert('ERROR', resData.message)
        varGetError = true
        this.setState({
          GetError: true
        })
      }

    }).catch(err => {
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  inspectorItem = (index) => {
    const { isGetInspector, buttondisable, InspectorName, monitorSetup, InspectorSelect, Inspectors } = this.state
    return (
      <View>
        {
          monitorSetup == '扫码' ?
            (
              !isGetInspector ?
                <View>
                  <ScanButton


                    onPress={() => {
                      this.setState({
                        isInspectordelete: false,
                        buttondisable: true,
                        focusIndex: 6
                      })
                      setTimeout(() => {
                        this.setState({ GetError: false, buttondisable: false })
                        varGetError = false
                      }, 1500)
                      this.props.navigation.navigate('QRCode', {
                        type: 'InspectorId',
                        page: 'ProductionStart'
                      })
                    }}
                  />
                </View>
                :
                <TouchableCustom
                  onPress={() => {
                    console.log('onPress')

                  }}
                  onLongPress={() => {
                    console.log('onLongPress')
                    Alert.alert(
                      '提示信息',
                      '是否删除质检员信息',
                      [{
                        text: '取消',
                        style: 'cancel'
                      }, {
                        text: '删除',
                        onPress: () => {
                          this.setState({
                            InspectorId: '',
                            InspectorName: '',
                            isGetInspector: false,
                            isInspectordelete: true
                          })
                        }
                      }])
                  }} >
                  <Text>{InspectorName}</Text>
                </TouchableCustom>
            ) :
            (
              this.state.isCheck ? <Text >{InspectorSelect}</Text> :
                <TouchableCustom onPress={() => { this.isBottomVisible(Inspectors, index) }} >
                  <IconDown text={InspectorSelect} />
                </TouchableCustom>
            )
        }
      </View>
    )
  }

  PostData = () => {
    const { InspectorId, acceptanceId, teamId, steelLabel, monitorId, compId, ServerTime, checkResult, rowguid, taiwanId, steelCageIDs, steelCageID, remark, imageArr, recid } = this.state
    const { navigation } = this.props;
    const QRid = navigation.getParam('QRid') || '';

    if (compId == '') {
      this.toast.show('产品编号不能为空');
      return
    } else if (teamId == '') {
      this.toast.show('班组信息不能为空');
      return
    }

    if (Url.isUseTaiWanID == 'YES') {
      if (taiwanId == '' && acceptanceId == '') {
        this.toast.show('模台编码或专用模具不能同时为空');
        return
      }
    }

    if (this.state.isAppPhotoSetting) {
      if (imageArr.length == 0) {
        this.toast.show('请先拍摄构件照片');
        return
      }
    }

    this.toast.show(saveLoading, 0)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "saveProductStart",
      "servicetype": "pda",
      "express": "CD4A180C",
      "ciphertext": "483793ea6fcbc18ae2fda0b908563f56",
      "data": {
        "PDAuserid": Url.PDAEmployeeId,
        "acceptanceId": acceptanceId,
        "compId": compId,
        "factoryId": Url.PDAFid,
        "teamId": teamId,
        "ProductTime": ServerTime,
        "PDAuser": Url.PDAEmployeeName,
        "rowguid": rowguid,
        "taiwanId": taiwanId,
        "remark": remark,
        "recid": recid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    if (!Url.isAppNewUpload) {
      imageArr.map((item, index) => {
        formData.append('img_' + index, {
          uri: item,
          type: 'image/jpeg',
          name: 'img_' + index
        })
      })
    }
    console.log("🚀 ~ file: production_start.js ~ line 862 ~ ProductionStart ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('保存成功');
        setTimeout(() => {
          this.props.navigation.replace(this.props.navigation.getParam("page"), {
            title: this.props.navigation.getParam("title"),
            pageType: this.props.navigation.getParam("pageType"),
            page: this.props.navigation.getParam("page"),
            isAppPhotoSetting: this.props.navigation.getParam("isAppPhotoSetting"),
            isAppDateSetting: this.props.navigation.getParam("isAppDateSetting"),
            isAppPhotoAlbumSetting: this.props.navigation.getParam("isAppPhotoAlbumSetting"),
          })
          //this.props.navigation.navigate('PDAMainStack')
        }, 400)
      } else {
        this.toast.show('保存失败')
        Alert.alert('保存失败', resData.message)
        console.log('resData', resData)
      }
    }).catch((error) => {
      this.toast.show('保存失败')
      if (error.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('保存失败', "响应内容不是合法JSON格式")
        return
      }
      if (error.toString().indexOf('Network request faile') != -1) {
        Alert.alert('保存失败', "网络请求错误")
        return
      }
      Alert.alert('保存失败', error.toString())
    });
  }

  DeleteData = () => {
    const { navigation } = this.props;
    let guid = navigation.getParam('guid') || ''
    console.log('DeleteData')
    let formData = new FormData();
    let data = {};
    data = {
      "action": "DeleteProductStart",
      "servicetype": "pda",
      "express": "06C795EE",
      "ciphertext": "a1e3818d57d9bfc9437c29e7a558e32e",
      "data": {
        "guid": guid,
        "userName": Url.username,
        "factoryId": Url.PDAFid
      },
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: qualityinspection.js ~ line 953 ~ ProductionStart ~ DeleteData ~ formData", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('删除成功');
        this.props.navigation.navigate('MainCheckPage')
      } else {
        Alert.alert(
          '删除失败',
          resData.message,
          [{
            text: '取消',
            style: 'cancel'
          }, {
            text: '返回上一级',
            onPress: () => {
              this.props.navigation.goBack()
            }
          }])
        console.log('resData', resData)
        console.log('resData', resData)
      }
    }).catch((error) => {
    });

  }

  cameraFunc = () => {
    launchCamera(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: parseInt(Url.isResizePhoto),
        maxWidth: parseInt(Url.isResizePhoto),
        saveToPhotos: true,

      },
      (response) => {
        if (response.uri == undefined) {
          return
        }
        imageArr.push(response.uri)
        this.setState({
          //pictureSelect: true,
          pictureUri: response.uri,
          imageArr: imageArr
        })
        if (Url.isAppNewUpload) {
          this.cameraPost(response.uri)
        } else {
          this.setState({
            pictureSelect: true,
          })
        }
        console.log(response)
      },
    )
  }

  camLibraryFunc = () => {
    launchImageLibrary(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: parseInt(Url.isResizePhoto),
        maxWidth: parseInt(Url.isResizePhoto),
      },
      (response) => {
        if (response.uri == undefined) {
          return
        }
        imageArr.push(response.uri)
        this.setState({
          //pictureSelect: true,
          pictureUri: response.uri,
          imageArr: imageArr
        })
        if (Url.isAppNewUpload) {
          this.cameraPost(response.uri)
        } else {
          this.setState({
            pictureSelect: true,
          })
        }
        console.log(response)
      },
    )
  }

  camandlibFunc = () => {
    Alert.alert(
      '提示信息',
      '选择拍照方式',
      [{
        text: '取消',
        style: 'cancel'
      }, {
        text: '拍照',
        onPress: () => {
          this.cameraFunc()
        }
      }, {
        text: '相册',
        onPress: () => {
          this.camLibraryFunc()
        }
      }])
  }

   // 展示/隐藏 放大图片
  handleZoomPicture = (flag, index) => {
    this.setState({
      imageOverSize: false,
      currShowImgIndex: index || 0
    })
  }

  // 加载放大图片弹窗
  renderZoomPicture = () => {
    const { imageOverSize, currShowImgIndex, imageFileArr } = this.state
    return (
      <OverlayImgZoomPicker
        isShowImage={imageOverSize}
        currShowImgIndex={currShowImgIndex}
        zoomImages={imageFileArr}
        callBack={(flag) => this.handleZoomPicture(flag)}
      ></OverlayImgZoomPicker>
    )
  }

  cameraPost = (uri) => {
    const { recid } = this.state
    this.setState({
      isPhotoUpdate: true
    })
    this.toast.show('图片上传中', 2000)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "savephote",
      "servicetype": "photo_file",
      "express": "D2979AB2",
      "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
      "data": {
        "fileflag": fileflag,
        "username": Url.PDAusername,
        "recid": recid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    formData.append('img', {
      uri: uri,
      type: 'image/jpeg',
      name: 'img'
    })
    console.log("🚀 ~ file: concrete_pouring.js ~ line 672 ~ SteelCage ~ formData", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 690 ~ SteelCage ~ resData", resData)
      this.toast.close()
      if (resData.status == '100') {
        let fileid = resData.result.fileid;
        let recid = resData.result.recid;

        let tmp = {}
        tmp.fileid = fileid
        tmp.uri = uri
        tmp.url = uri
        imageFileArr.push(tmp)
        this.setState({
          fileid: fileid,
          recid: recid,
          pictureSelect: true,
          imageFileArr: imageFileArr
        })
        this.toast.show('图片上传成功');
        this.setState({
          isPhotoUpdate: false
        })
      } else {
        Alert.alert('图片上传失败', resData.message)
        this.toast.close(1)
      }
    }).catch((error) => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 692 ~ SteelCage ~ error", error)
    });
  }

  cameraDelete = (item) => {
    let i = imageArr.indexOf(item)

    if (Url.isAppNewUpload) {
      let formData = new FormData();
      let data = {};
      data = {
        "action": "deletephote",
        "servicetype": "photo_file",
        "express": "D2979AB2",
        "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
        "data": {
          "fileid": imageFileArr[i].fileid,
        }
      }
      formData.append('jsonParam', JSON.stringify(data))
      fetch(Url.PDAurl, {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        body: formData
      }).then(res => {
        console.log(res.statusText);
        console.log(res);
        return res.json();
      }).then(resData => {
        if (resData.status == '100') {
          if (i > -1) {
            imageArr.splice(i, 1)
            imageFileArr.splice(i, 1)
            this.setState({
              imageArr: imageArr,
              imageFileArr: imageFileArr
            }, () => {
              this.forceUpdate()
            })
          }
          if (imageArr.length == 0) {
            this.setState({
              pictureSelect: false
            })
          }
          this.toast.show('图片删除成功');
        } else {
          Alert.alert('图片删除失败', resData.message)
          this.toast.close(1)
        }
      }).catch((error) => {
        console.log("🚀 ~ file: concrete_pouring.js ~ line 761 ~ SteelCage ~ cameraDelete ~ error", error)
      });
    } else {
      if (i > -1) {
        imageArr.splice(i, 1)
        this.setState({
          imageArr: imageArr,
        }, () => {
          this.forceUpdate()
        })
      }
      if (imageArr.length == 0) {
        this.setState({
          pictureSelect: false
        })
      }
    }
  }

  imageView = () => {
    return (
      <View style={{ flexDirection: 'row' }}>
        {
          this.state.pictureSelect ?
            this.state.imageArr.map((item, index) => {
              return (
                <AvatarImg
                  item={item}
                  index={index}
                  onPress={() => {
                    this.setState({
                      imageOverSize: true,
                      pictureUri: item
                    })
                  }}
                  onLongPress={() => {
                    Alert.alert(
                      '提示信息',
                      '是否删除构件照片',
                      [{
                        text: '取消',
                        style: 'cancel'
                      }, {
                        text: '删除',
                        onPress: () => {
                          this.cameraDelete(item)
                        }
                      }])
                  }} />
              )
            }) : <View></View>
        }
      </View>
    )
  }

  paperResData = (type) => {
    let formData = new FormData();
    let data = {}
    data = {
      "action": "getDrawings",
      "servicetype": "pda",
      "express": "FCEF95AF",
      "ciphertext": "985e3620ae26a2f380c723b60ce9b525",
      "data": {
        "type": type,
        "compId": this.state.compId,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 314 ~ QualityInspection ~ resData", resData)
      if (resData.status == "100") {

        let paperUrl = resData.result.Url
        let FileName = resData.result.FileName

        this.setState({
          paperUrl: paperUrl,
          FileName: FileName,
          isPDFVisible: true,
          isPaperTypeVisible: false
        })
      } else {
        Alert.alert('错误', resData.message)
        this.setState({ isPaperTypeVisible: false })
      }
    }).catch((error) => {
    });
  }

  func = (state) => {
    console.log("🚀 ~ file: qualityinspection.js ~ line 1166 ~ QualityInspection ~ state", state)
    this.setState({
      isPaperTypeVisible: true
    })
  }

  _DatePicker = () => {
    const { ServerTime } = this.state
    return (
      <DatePicker
        customStyles={{
          dateInput: styles.dateInput,
          dateTouchBody: styles.dateTouchBody,
          dateText: this.state.isAppDateSetting ? styles.dateText : styles.dateDisabled,
        }}
        iconComponent={<Icon name='caretdown' color={this.state.isAppDateSetting ? '#419fff' : "#666"} iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
        }
        showIcon={true}
        mode='datetime'
        date={ServerTime}
        onOpenModal={() => { this.setState({ focusIndex: 4 }) }}
        format="YYYY-MM-DD HH:mm"
        disabled={!this.state.isAppDateSetting}
        onDateChange={(value) => {
          this.setState({
            ServerTime: value
          })
        }}
      />
    )
  }

  isBottomVisible = (data, focusIndex) => {
    if (typeof (data) != "undefined") {
      if (data.length > 0) {
        this.setState({ bottomVisible: true, bottomData: data, focusIndex: focusIndex })
      }
    } else {
      this.toast.show("无数据")
    }
  }

  render() {
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || ''
    const type = navigation.getParam('type') || '';
    let pageType = navigation.getParam('pageType') || ''

    if (type == 'compid') {
      QRcompid = QRid
    }
    const { buttondisable, focusIndex, steelLabel, taiwanId, acceptanceId, steelCageID, resData, ServerTime, isGetcomID, isGetCommonMould, isGetInspector, isGetTeamPeopleInfo, isGetsteelCageInfoforPutMold, isGettaiwanId, datepickerVisible, Tai_QRname, acceptanceName, teaminfos, teamNameSelect, compData, compCode, bottomVisible, bottomData, isCheck, isCheckPage, remark, isPDFVisible, paperList, paperUrl, FileName } = this.state

    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          <NavigationEvents
            onWillFocus={this.UpdateControl}
            onWillBlur={() => {
              if (QRid != '') {
                navigation.setParams({ QRid: '', type: '' })
              }
            }} />
          <ScrollView ref={ref => this.scoll = ref} style={styles.mainView} showsVerticalScrollIndicator={false} >
            <KeyboardAvoidingView>
              <View style={styles.listView}>
                <ListItem
                  containerStyle={{ paddingBottom: 6 }}
                  leftAvatar={
                    <View >
                      <View style={{ flexDirection: 'column' }} >
                        {
                          isCheckPage ? <View></View> :
                            <AvatarAdd
                              pictureSelect={this.state.pictureSelect}
                              backgroundColor='rgba(77,142,245,0.20)'
                              color='#4D8EF5'
                              title="构"
                              onPress={() => {
                                if (this.state.isAppPhotoAlbumSetting) {
                                  this.camandlibFunc()
                                } else {
                                  this.cameraFunc()
                                }
                              }} />
                        }
                        {this.imageView()}
                      </View>
                    </View>
                  }
                />
                <ListItemScan
                  isButton={!isGetcomID}
                  focusStyle={focusIndex == '0' ? styles.focusColor : {}}
                  title='产品编号'
                  onPressIn={() => {
                    this.setState({
                      type: 'compid',
                      focusIndex: 0
                    })
                    NativeModules.PDAScan.onScan();
                  }}
                  onPress={() => {
                    this.setState({
                      type: 'compid',
                      focusIndex: 0
                    })
                    NativeModules.PDAScan.onScan();
                  }}
                  onPressOut={() => {
                    NativeModules.PDAScan.offScan();
                  }}
                  rightElement={
                    this.comIDItem(0)
                  }
                />
                {
                  this.state.isGetcomID ?
                    <CardList compData={compData} func={this.func.bind(this)} /> : <View></View>
                }
                <ListItemScan
                  isButton={!isGettaiwanId}
                  focusStyle={focusIndex == '1' ? styles.focusColor : {}}
                  title='模台编码'
                  onPressIn={() => {
                    this.setState({
                      type: 'taiwanId',
                      focusIndex: 1
                    })
                    NativeModules.PDAScan.onScan();
                  }}
                  onPress={() => {
                    this.setState({
                      type: 'taiwanId',
                      focusIndex: 1
                    })
                    NativeModules.PDAScan.onScan();
                  }}
                  onPressOut={() => {
                    NativeModules.PDAScan.offScan();
                  }}
                  rightElement={
                    this.taiwanItem(1)
                  }
                />
                <ListItemScan
                  isButton={!isGetCommonMould}
                  focusStyle={focusIndex == '2' ? styles.focusColor : {}}
                  onPressIn={() => {
                    this.setState({
                      type: 'acceptanceId',
                      focusIndex: 2
                    })
                    NativeModules.PDAScan.onScan();
                  }}
                  onPress={() => {
                    this.setState({
                      type: 'acceptanceId',
                      focusIndex: 2
                    })
                    NativeModules.PDAScan.onScan();
                  }}
                  onPressOut={() => {
                    NativeModules.PDAScan.offScan();
                  }}
                  title='专用模具'
                  rightElement={
                    !isGetCommonMould ?
                      <View>
                        <ScanButton
                          onPress={() => {
                            this.setState({
                              isCommonMoulddelete: false,
                              buttondisable: false,
                              focusIndex: 2
                            })
                            this.props.navigation.navigate('QRCode', {
                              type: 'acceptanceId',
                              page: 'ProductionStart'
                            })
                          }}
                          onLongPress={() => {
                            this.setState({
                              type: 'acceptanceId',
                              focusIndex: 2
                            })
                            NativeModules.PDAScan.onScan();
                          }}
                          onPressOut={() => {
                            NativeModules.PDAScan.offScan();
                          }}
                        />
                      </View>
                      :
                      <TouchableCustom
                        onPress={() => {
                          console.log('onPress')
                        }}
                        onLongPress={() => {
                          console.log('onLongPress')
                          Alert.alert(
                            '提示信息',
                            '是否删除模具信息',
                            [{
                              text: '取消',
                              style: 'cancel'
                            }, {
                              text: '删除',
                              onPress: () => {
                                this.setState({
                                  acceptanceId: '',
                                  acceptanceName: '',
                                  isGetCommonMould: false,
                                  isCommonMoulddelete: true
                                })
                              }
                            }])
                        }} >
                        <Text>{acceptanceName}</Text>
                      </TouchableCustom>
                  }
                />
                {
                  steelLabel == '使用' ?
                    <ListItemScan
                      isButton={!isGetsteelCageInfoforPutMold}
                      onPressIn={() => {
                        this.setState({
                          type: 'compid',
                          focusIndex: 0
                        })
                        NativeModules.PDAScan.onScan();
                      }}
                      onPress={() => {
                        this.setState({
                          type: 'compid',
                          focusIndex: 0
                        })
                        NativeModules.PDAScan.onScan();
                      }}
                      onPressOut={() => {
                        NativeModules.PDAScan.offScan();
                      }}
                      focusStyle={focusIndex == '3' ? styles.focusColor : {}}
                      title='钢筋笼编码'
                      rightTitle={
                        !isGetsteelCageInfoforPutMold ?
                          <View >
                            <ScanButton

                              onPress={() => {
                                this.setState({
                                  focusIndex: 3,
                                  issteelCageIDdelete: false,
                                  buttondisable: false
                                })
                                setTimeout(() => {
                                  this.setState({ GetError: false, buttondisable: false })
                                  varGetError = false
                                }, 1500)
                                this.props.navigation.navigate('QRCode', {
                                  type: 'steelCageID',
                                  page: 'ProductionStart'
                                })
                              }}
                              onLongPress={() => {
                                this.setState({
                                  type: 'steelCageID',
                                  focusIndex: 3
                                })
                                NativeModules.PDAScan.onScan();
                              }}
                              onPressOut={() => {
                                NativeModules.PDAScan.offScan();
                              }}
                            />
                          </View>
                          :
                          <TouchableCustom
                            onPress={() => {
                              console.log('onPress')
                            }}
                            onLongPress={() => {
                              console.log('onLongPress')
                              Alert.alert(
                                '提示信息',
                                '是否删除钢筋笼编码',
                                [{
                                  text: '取消',
                                  style: 'cancel'
                                }, {
                                  text: '删除',
                                  onPress: () => {
                                    this.setState({
                                      steelCageID: '',
                                      steelCageCode: '',
                                      isGetsteelCageInfoforPutMold: false,
                                      issteelCageIDdelete: true
                                    })
                                  }
                                }])
                            }} >
                            <Text numberOfLines={1}>{this.state.steelCageCode}</Text>
                          </TouchableCustom>
                      }
                    /> : <View></View>
                }

                <TouchableCustom underlayColor={'lightgray'} onPress={() => {
                  this.isBottomVisible(teaminfos, '6')
                }} >
                  <ListItemScan
                    focusStyle={focusIndex == '6' ? styles.focusColor : {}}
                    title='班组名称'
                    rightElement={
                      <IconDown text={teamNameSelect} />
                    }
                  />
                </TouchableCustom>

                <ListItemScan
                  focusStyle={focusIndex == '4' ? styles.focusColor : {}}
                  title='生产日期'
                  rightElement={
                    this._DatePicker()
                  }
                //rightTitle={ServerTime}
                />
                <ListItemScan
                  title='操作人'
                  rightTitleStyle={{ width: width / 1.5, textAlign: 'right', fontSize: 15 }}
                  rightTitle={Url.PDAEmployeeName}
                />

                <ListItemScan
                  focusStyle={focusIndex == '5' ? styles.focusColor : {}}
                  title='备注'
                  rightElement={
                    <View>
                      <Input
                        containerStyle={styles.quality_input_container}
                        inputContainerStyle={styles.inputContainerStyle}
                        inputStyle={[styles.quality_input_, { top: 7 }]}
                        placeholder='请输入'
                        value={remark}
                        onChangeText={(value) => {
                          this.setState({
                            remark: value
                          })
                        }} />
                    </View>
                  }
                />
              </View>
            </KeyboardAvoidingView>


            {
              isCheck ? <FormButton
                backgroundColor='#EB5D20'
                onLongPress={() => {
                  Alert.alert(
                    '提示信息',
                    '是否删除',
                    [{
                      text: '取消',
                      style: 'cancel'
                    }, {
                      text: '删除',
                      onPress: () => {
                        this.DeleteData()
                      }
                    }])
                }}
                title='删除'
              /> :
                <FormButton
                  backgroundColor='#17BC29'
                  onPress={this.PostData}
                  title='保存'
                  disabled={this.state.isPhotoUpdate}
                />
            }

             {this.renderZoomPicture()}

            {/* {overlayImg(obj = {
              onRequestClose: () => {
                this.setState({ imageOverSize: !this.state.imageOverSize }, () => {
                  console.log("🚀 ~ file: equipment_maintenance.js:1696 ~ render ~ imageOverSize:", this.state.imageOverSize)
                })
              },
              onPress: () => {
                this.setState({
                  imageOverSize: !this.state.imageOverSize
                })
              },
              uri: this.state.pictureUri,
              isVisible: this.state.imageOverSize
            })
            }*/}

            <Overlay
              fullScreen={true}
              animationType='fade'
              isVisible={this.state.isPDFVisible}
              onRequestClose={() => {
                this.setState({ isPDFVisible: !this.state.isPDFVisible });
              }}
            >
              <Header
                containerStyle={{ height: 45, paddingTop: 0, top: -5 }}
                centerComponent={{ text: '图纸预览', style: { color: 'gray', fontSize: 20 } }}
                rightComponent={<BackIcon
                  onPress={() => {
                    this.setState({
                      isPDFVisible: !this.state.isPDFVisible
                    });
                  }} />}
                backgroundColor='white'
              />
              <View style={{ flex: 1 }}>
                <Pdf
                  source={{
                    uri: paperUrl,
                    //method: 'GET', //默认 'GET'，请求 url 的方式
                  }}
                  fitWidth={true} //默认 false，若为 true 则不能将 fitWidth = true 与 scale 一起使用
                  fitPolicy={0} // 0:宽度对齐，1：高度对齐，2：适合两者（默认）
                  page={1}
                  //scale={1}
                  onLoadComplete={(numberOfPages, filePath, width, height, tableContents) => {
                    console.log(`number of pages: ${numberOfPages}`); //总页数
                    console.log(`number of filePath: ${filePath}`); //本地返回的路径
                    console.log(`number of width: `, JSON.stringify(width));
                    console.log(`number of height: ${JSON.stringify(height)}`);
                    console.log(`number of tableContents: ${tableContents}`);
                  }}
                  onError={(error) => {
                    console.log(error);
                  }}
                  minScale={1} //最小模块
                  maxScale={3}
                  enablePaging={true} //在屏幕上只能显示一页
                  style={{
                    flex: 1,
                    width: width
                  }}
                />
              </View>

            </Overlay>

            <BottomSheet
              isVisible={this.state.CameraVisible}
              onRequestClose={() => {
                this.setState({ CameraVisible: false })
              }}
            >
              <TouchableCustom
                onPress={() => {
                  launchCamera(
                    {
                      mediaType: 'photo',
                      includeBase64: false,
                      maxHeight: 600,
                      maxWidth: 600,
                      saveToPhotos: true
                    },
                    (response) => {
                      if (response.uri == undefined) {
                        return
                      }
                      imageArr.push(response.uri)
                      this.setState({
                        pictureSelect: true,
                        pictureUri: response.uri,
                        imageArr: imageArr
                      })
                      console.log(response)
                    },
                  )
                  this.setState({ CameraVisible: false })
                }} >
                <ListItemScan
                  title='拍照' >
                </ListItemScan>
              </TouchableCustom>
              <TouchableCustom
                onPress={() => {
                  launchImageLibrary(
                    {
                      mediaType: 'photo',
                      includeBase64: false,
                      maxHeight: 600,
                      maxWidth: 600,

                    },
                    (response) => {
                      if (response.uri == undefined) {
                        return
                      }
                      imageArr.push(response.uri)
                      this.setState({
                        pictureSelect: true,
                        pictureUri: response.uri,
                        imageArr: imageArr
                      })
                      console.log(response)
                    },
                  )
                  this.setState({ CameraVisible: false })
                }} >
                <ListItemScan
                  title='相册' >
                </ListItemScan>
              </TouchableCustom>
              <ListItemScan
                title='关闭'
                containerStyle={{ backgroundColor: 'red' }}
                onPress={() => {
                  this.setState({ CameraVisible: false })
                }} >
              </ListItemScan>
            </BottomSheet>

            <BottomSheet
              isVisible={datepickerVisible}
              onRequestClose={() => {
                this.setState({ datepickerVisible: false })
              }}
            >

              <ListItemScan
                title='确定'
                onPress={() => {
                  this.setState({
                    datepickerVisible: false
                  })
                }}
              ></ListItemScan>
            </BottomSheet>

            <BottomSheet
              isVisible={bottomVisible && !this.state.isCheckPage}
              onRequestClose={() => {
                this.setState({
                  bottomVisible: false
                })
              }}
              onBackdropPress={() => {
                this.setState({
                  bottomVisible: false
                })
              }}
            >
              <ScrollView style={styles.bottomsheetScroll}>
                {
                  bottomData.map((item, index) => {
                    let title = ''
                    if (item.teamId) {
                      title = item.teamName
                    }
                    return (
                      <TouchableCustom
                        onPress={() => {
                          if (item.teamId) {
                            this.setState({
                              teamId: item.teamId,
                              teamName: item.teamName,
                              teamNameSelect: item.teamName,
                            }, () => {
                              DeviceStorageData = {
                                "teamId": this.state.teamId,
                                "teamName": this.state.teamName,
                                "teamNameSelect": this.state.teamName,
                              }

                              DeviceStorage.save('DeviceStorageDataPS', JSON.stringify(DeviceStorageData))
                            })
                          }
                          this.setState({
                            bottomVisible: false
                          })
                        }}
                      >
                        <BottomItem backgroundColor='white' color="#333" title={title} />
                      </TouchableCustom>
                    )

                  })
                }
              </ScrollView>
              <TouchableHighlight
                onPress={() => {
                  this.setState({ bottomVisible: false })
                }}>
                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
              </TouchableHighlight>

            </BottomSheet>

            <BottomSheet
              isVisible={this.state.isPaperTypeVisible}
              onRequestClose={() => {
                this.setState({
                  isPaperTypeVisible: false
                })
              }}
            >
              <ScrollView style={styles.bottomsheetScroll}>
                {
                  paperList.map((item, index) => {
                    return (
                      <TouchableCustom
                        onPress={() => {
                          this.paperResData(item)
                        }}
                      >
                        <BottomItem backgroundColor='white' color="#333" title={item} />
                      </TouchableCustom>
                    )

                  })
                }
              </ScrollView>
              <TouchableHighlight
                onPress={() => {
                  this.setState({ isPaperTypeVisible: false })
                }}>
                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
              </TouchableHighlight>

            </BottomSheet>
          </ScrollView>
        </View>
      )
    }
  }

}
