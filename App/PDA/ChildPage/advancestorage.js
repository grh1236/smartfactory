import React from 'react';
import { View, StatusBar, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, ListItem, BottomSheet } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { ToDay, YesterDay, BeforeYesterDay } from '../../CommonComponents/Data';
import { RFT } from '../../Url/Pixal';
import { ScrollView } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker'
import { NavigationEvents } from 'react-navigation';
import { styles } from '../Componment/PDAStyles';
import ScanButton from '../Componment/ScanButton';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import ListItemScan_child from '../Componment/ListItemScan_child';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import CheckBoxScan from '../Componment/CheckBoxScan';
import AvatarAdd from '../Componment/AvatarAdd';
import AvatarImg from '../Componment/AvatarImg';

import { DeviceEventEmitter, NativeModules } from 'react-native';

const { height, width } = Dimensions.get('window') //获取宽高

let QRkeeperName = ''
let QRStock = ''
let QRkeeperID = ''

let compDataArr = [], compIdArr = [], QRid2 = '', tmpstr = '', visibleArr = [];

class CardList extends React.Component {
  render() {
    const { compData } = this.props
    let comp = compData.comp[0]
    return (
      <View>
        <Card
          title={compData.compCode}
          containerStyle={{ borderRadius: 10, shadowOpacity: 0 }}>
          <ListItem
            containerStyle={styles.list_container_style}
            title='项目名称'
            rightTitle={comp.projectName}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='项目名称'
            rightTitle={comp.projectName}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='构件类型'
            rightTitle={comp.compTypeName}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='设计型号'
            rightTitle={comp.designType}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='楼号'
            rightTitle={comp.floorNoName}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='层号'
            rightTitle={comp.floorName}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='砼方量'
            rightTitle={comp.volume}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='重量'
            rightTitle={comp.weight}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='生产单位'
            rightTitle={Url.PDAFname}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
        </Card>
      </View>
    )
  }
}

class CardList2 extends React.Component {
  constructor() {
    super();
    this.state = {
      hidden: -1,
      isVisible: false
    }
  }
  render() {
    const { compData } = this.props
    return (
      compData.map((item, index) => {
        let comp = item.comp[0]
        return (
          <View style={{ width: width * 0.9, marginLeft: width * 0.05, }}>
            <ListItem
              key={index}
              onPress={() => {
                this.setState({
                  hidden: index,
                  isVisible: !this.state.isVisible
                }, () => {
                  console.log("🚀 ~ file: storage.js ~ line 126 ~ CardList2 ~ compData.map ~ this.state.isVisible", this.state.isVisible)

                })
              }}
              containerStyle={{ backgroundColor: '#419fff' }}
              title={item.compCode} >
            </ListItem>
            {
              this.state.hidden == index && this.state.isVisible ?
                <View key={index} >
                  <ListItem
                    containerStyle={styles.list_container_style}
                    title='项目名称'
                    rightTitle={comp.projectName}
                    titleStyle={styles.title}
                    rightTitleStyle={styles.rightTitle}
                  />
                  <ListItem
                    containerStyle={styles.list_container_style}
                    title='项目名称'
                    rightTitle={comp.projectName}
                    titleStyle={styles.title}
                    rightTitleStyle={styles.rightTitle}
                  />
                  <ListItem
                    containerStyle={styles.list_container_style}
                    title='构件类型'
                    rightTitle={comp.compTypeName}
                    titleStyle={styles.title}
                    rightTitleStyle={styles.rightTitle}
                  />
                  <ListItem
                    containerStyle={styles.list_container_style}
                    title='设计型号'
                    rightTitle={comp.designType}
                    titleStyle={styles.title}
                    rightTitleStyle={styles.rightTitle}
                  />
                  <ListItem
                    containerStyle={styles.list_container_style}
                    title='楼号'
                    rightTitle={comp.floorNoName}
                    titleStyle={styles.title}
                    rightTitleStyle={styles.rightTitle}
                  />
                  <ListItem
                    containerStyle={styles.list_container_style}
                    title='层号'
                    rightTitle={comp.floorName}
                    titleStyle={styles.title}
                    rightTitleStyle={styles.rightTitle}
                  />
                  <ListItem
                    containerStyle={styles.list_container_style}
                    title='砼方量'
                    rightTitle={comp.volume}
                    titleStyle={styles.title}
                    rightTitleStyle={styles.rightTitle}
                  />
                  <ListItem
                    containerStyle={styles.list_container_style}
                    title='重量'
                    rightTitle={comp.weight}
                    titleStyle={styles.title}
                    rightTitleStyle={styles.rightTitle}
                  />
                  <ListItem
                    containerStyle={styles.list_container_style}
                    title='生产单位'
                    rightTitle={Url.PDAFname}
                    titleStyle={styles.title}
                    rightTitleStyle={styles.rightTitle}
                  />
                </View> : <View></View>
            }

          </View>
        )
      })
    )

  }
}

export default class AdvanceStorage extends React.Component {
  constructor() {
    super();
    this.state = {
      monitorSetup: '',
      isroom: false,
      isteam: false,
      iskeeper: false,
      type: 'Stockid',
      bottomData: [],
      bottomVisible: false,
      resData: [],
      formCode: '',
      rowguid: '',
      steelCageIDs: '',
      ServerTime: '',
      QRid: '',
      news: '',
      room: [],
      roomId: '',
      roomselect: '请选择',
      roomName: '',
      library: [],
      libraryId: '',
      libraryselect: '请选择',
      libraryName: '',
      compDataArr: [],
      compData: '',
      compId: '',
      compIdArr: [],
      compCode: '',
      isGetStoreComponentInfo: false,
      isGetStoreroom: false,
      //工装是否使用
      isUsedPRacking: '',
      handingToolName: '',
      handingUniqueCode: '',
      handlingToolId: '',
      isGettrackingID: false,
      istrackingIDdelete: false,
      //CardList
      visibleArr: [],
      hidden: -1,
      isVisible: false,
      focusIndex: 3,
      isLoading: true,
      isCheckPage: false
    }
    //this.requestCameraPermission = this.requestCameraPermission.bind(this)
  }

  componentDidMount() {
    StatusBar.setBarStyle('dark-content')
    const { navigation } = this.props;
    let guid = navigation.getParam('guid') || ''
    let pageType = navigation.getParam('pageType') || ''
    if (pageType == 'CheckPage') {
      this.checkResData(guid)
      this.setState({ isCheckPage: true })
    } else {
      this.resData()
    }

    //通过使用DeviceEventEmitter模块来监听事件
    this.iDataScan = DeviceEventEmitter.addListener('iDataScan', (Event) => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 115 ~ SteelCage ~ Event.ScanResult", Event.ScanResult)
      if (typeof Event.ScanResult != undefined) {
        let data = Event.ScanResult
        let arr = data.split("=");
        let id = ''
        id = arr[arr.length - 1]
        console.log("🚀 ~ file: concrete_pouring.js ~ line 118 ~ SteelCage ~ id", id)
        if (this.state.type == 'Stockid') {
          this.GetStoreComponentInfo(id)
        }
        if (this.state.type == 'libraryid') {
          this.GetStoreroom(id)
        }
        if (this.state.type == 'trackingid') {
          this.GetPalleTracking(id)
        }
      }
    });
  }

  UpdateControl = () => {
    if (typeof this.props.navigation.getParam('QRid') != "undefined") {
      if (this.props.navigation.getParam('QRid') != "") {
        this.toast.show(Url.isLoadingView, 0)
      }
    }
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || ''
    console.log("🚀 ~ file: storage.js ~ line 162 ~ AdvanceStorage ~ componentDidUpdate ~ QRid", QRid)
    const type = navigation.getParam('type') || '';

    navigation.setParams({ QRid: '', type: '' })


    if (type == 'libraryid') {
      this.GetStoreroom(QRid)

    } else if (type == 'Stockid') {
      this.GetStoreComponentInfo(QRid)
    } else if (type == 'trackingid') {
      this.GetPalleTracking(QRid)
    }

    if (QRid != '') {
      navigation.setParams({ QRid: '', type: '' })
    }
    console.log("🚀 ~ file: storage.js ~ line 1621 ~ AdvanceStorage ~ componentDidUpdate ~ QRid", QRid)

  }

  componentWillUnmount() {
    compDataArr = [], QRid2 = '', tmpstr = '', compIdArr = [], visibleArr = []
    this.iDataScan.remove()
  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  checkResData = (guid) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getpreinstodetail",
      "servicetype": "pdapreinsto",
      "express": "8AC4C0B0",
      "ciphertext": "7df83f712027c63f147f6c2d4a43f08d",
      "data": {
        "_Rowguid": guid,
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: storage.js ~ line 313 ~ AdvanceStorage ~ formData", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 204 ~ QualityInspection ~ resData", resData)
      let rowguid = resData.result._Rowguid
      let ServerTime = resData.result.proStoDate
      let formCode = resData.result.formCode
      let libraryName = resData.result.libraryName
      let roomName = resData.result.roomName
      let component = resData.result.preInStoComponets
      let compCode = ""
      let compId = ''
      component.map((item, index) => {
        compDataArr.push(item)
        compIdArr.push(item.compId)
        compCode = item.compCode
        //compId = item.compId
      })
      this.setState({
        resData: resData,
        rowguid: rowguid,
        ServerTime: ServerTime,
        formCode: formCode,
        libraryName: libraryName,
        libraryselect: libraryName,
        roomName: roomName,
        roomselect: roomName,
        compDataArr: compDataArr,
        compIdArr: compIdArr,
        compId: compId,
        compCode: compCode,
        isGetStoreroom: true,
        isCheck: true
      })
      this.setState({
        isLoading: false,
      })
    }).catch((error) => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 215 ~ QualityInspection ~ error", error)
    });
  }

  resData = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "preinstoinitial",
      "servicetype": "pdapreinsto",
      "express": "B34B2B21",
      "ciphertext": "219527459fa29fa23c7d7ef001165d2b",
      "data": { "factoryId": Url.PDAFid }
    }

    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == 100) {
        let formCode = resData.result.formCode
        let ServerTime = resData.result.proStoDate
        let rowguid = resData.result.rowguid
        this.setState({
          resData: resData,
          formCode: formCode,
          ServerTime: ServerTime,
          rowguid: rowguid,
          isLoading: false
        })
      } else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }
    }).catch((error) => {
      console.log("🚀 ~ file: steel_cage_storage.js ~ line 75 ~ SteelCage ~ error", error)
    });
  }

  GetPalleTracking = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getpalletracking",
      "servicetype": "pdaservice",
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        "_bizid": Url.PDAFid,
        "handlingToolId": id
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
       this.toast.close()
      if (resData.status == '100') {
        let handlingToolId = resData.result.handlingToolId
        let handingToolName = resData.result.handingToolName
        let handingUniqueCode = resData.result.handingUniqueCode
        this.setState({
          handlingToolId: handlingToolId,
          handingToolName: handingToolName,
          handingUniqueCode: handingUniqueCode,
          isGettrackingID: true
        })
      } else {
        Alert.alert('ERROR', resData.message)
      }

    }).catch(err => {
    })
  }

  GetStoreroom = (id) => {
    let compCode = '111'
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getStoreroom",
      "servicetype": "pda",
      "express": "162C33CC",
      "ciphertext": "3b81e5182d128db9333f0fec4d3703a5",
      "data": {
        "LibraryId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
       this.toast.close()
      if (resData.status == '100') {
        let libraryName = resData.result.libraryName
        let libraryId = resData.result.libraryId
        let roomName = resData.result.roomName
        this.setState({
          libraryName: libraryName,
          libraryId: libraryId,
          roomName: roomName,
          libraryselect: libraryName,
          roomselect: roomName,
          isGetStoreroom: true
        })
      } else {
        Alert.alert('ERROR', resData.message)
      }

    }).catch(err => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 210 ~ SteelCage ~ err", err)
    })
  }

  GetStoreComponentInfo = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getcompinfo",
      "servicetype": "pdapreinsto",
      "express": "227EB695",
      "ciphertext": "de110251c4c1faf0038a3416d728a830",
      "data": {
        "compId": id,
        "factoryId": Url.PDAFid
      }
    }
    console.log("🚀 ~ file: advancestorage.js ~ line 525 ~ AdvanceStorage ~ data", data)

    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: storage.js ~ line 241 ~ AdvanceStorage ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
       this.toast.close()
      if (resData.status == '100') {
        let compData = resData.result
        console.log("🚀 ~ file: storage.js ~ line 301 ~ AdvanceStorage ~ compData", compData)
        let compId = compData.compId
        let compCode = compData.compCode
        console.log("🚀 ~ file: storage.js ~ line 395 ~ AdvanceStorage ~ compDataArr", compDataArr)
        if (compDataArr.length == 0) {
          compDataArr.push(compData)
          compIdArr.push(compId)
          this.setState({
            compDataArr: compDataArr,
            compIdArr: compIdArr,
            compData: compData,
            compId: "",
            compCode: compCode,
            isGetStoreComponentInfo: true
          })
          tmpstr = tmpstr + JSON.stringify(compId) + ','
        } else {
          if (tmpstr.indexOf(compId) == -1) {
            compIdArr.push(compId)
            compId = this.state.compId + ',' + compId
            console.log("🚀 ~ file: storage.js ~ line 406 ~ AdvanceStorage ~ compId", compId)
            compDataArr.push(compData)
            this.setState({
              compDataArr: compDataArr,
              compIdArr: compIdArr,
              compData: compData,
              compId: "",
              compCode: compCode,
              isGetStoreComponentInfo: true
            })
            tmpstr = tmpstr + JSON.stringify(compId) + ','
            console.log("🚀 ~ file: storage.js ~ line 307 ~ AdvanceStorage ~ tmpstr", tmpstr)
          } else {
            this.toast.show('已经扫瞄过此构件')
          }
        }
        this.setState({
          compId: ""
        })
        console.log("🚀 ~ file: storage.js ~ line 292 ~ AdvanceStorage ~ compDataArr", compDataArr)

      } else {
        Alert.alert('错误', resData.message)
        this.setState({
          compId: ""
        })
        //this.input1.focus()
      }

    }).catch(err => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 210 ~ SteelCage ~ err", err)
    })
  }

  comIDItem = (index) => {
    const { isGetcomID, buttondisable, compCode } = this.state
    return (
      <View>
        {
          !isGetcomID ?
            <View>
              <ScanButton
                onPress={() => {
                  this.setState({
                    iscomIDdelete: false,
                    buttondisable: true,
                    focusIndex: index
                  })
                  setTimeout(() => {
                    this.setState({ GetError: false, buttondisable: false })
                    varGetError = false
                  }, 1500)
                  this.props.navigation.navigate('QRCode', {
                    type: 'compid',
                    page: 'AdvanceStorage'
                  })
                }}
              />
            </View>
            :
            <TouchableCustom
              onPress={() => {
                console.log('onPress')
              }}
              onLongPress={() => {
                console.log('onLongPress')
                Alert.alert(
                  '提示信息',
                  '是否删除构件信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      this.setState({
                        compData: [],
                        compCode: '',
                        compId: '',
                        iscomIDdelete: true,
                        isGetcomID: false,
                      })
                    }
                  }])
              }} >
              <Text>{compCode}</Text>
            </TouchableCustom>
        }
      </View>

    )
  }

  trackingIDItem = (index) => {
    const { isGettrackingID, buttondisable } = this.state
    return (
      <View>
        {
          !isGettrackingID ?
            <View>
              <ScanButton
                onPress={() => {
                  this.setState({
                    istrackingIDdelete: false,
                    buttondisable: true,
                    isUpdate: true
                  })
                  this.setState({
                    type: 'trackingid',
                    focusIndex: index
                  })
                  this.props.navigation.navigate('QRCode', {
                    type: 'trackingid',
                    page: 'AdvanceStorage'
                  })
                }}
                onLongPress={() => {
                  this.setState({
                    type: 'trackingid',
                    focusIndex: index
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}
              />
            </View>
            :
            <TouchableCustom
              onPress={() => {
                console.log('onPress')
              }}
              onLongPress={() => {
                console.log('onLongPress')
                Alert.alert(
                  '提示信息',
                  '是否删除工装编码',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      this.setState({
                        handingToolName: '',
                        handingUniqueCode: '',
                        handlingToolId: '',
                        istrackingIDdelete: true,
                        buttondisable: false,
                        isGettrackingID: false,
                      })
                    }
                  }])
              }} >
              <Text>{this.state.handingUniqueCode}</Text>
            </TouchableCustom>
        }
      </View>

    )
  }

  PostData = () => {
    const { formCode, ServerTime, handlingToolId, rowguid, compId, compIdArr, libraryId, libraryName, roomName, } = this.state
    const { navigation } = this.props;
    const QRid = navigation.getParam('QRid') || '';

    console.log("🚀 ~ file: storage.js ~ line 511 ~ AdvanceStorage ~ compIdArr", compIdArr)
    let compIdstr = compIdArr.toString()
    console.log("🚀 ~ file: storage.js ~ line 506 ~ AdvanceStorage ~ compIdstr", compIdstr)

    if (libraryId.length == 0) {
      this.toast.show('库位不能为空')
      return
    } else if (compIdstr.length == 0) {
      this.toast.show('入库集合不能为空')
      return
    } else if (roomName.length == 0) {
      this.toast.show('库房不能为空')
      return
    }

    this.toast.show(saveLoading, 0)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "savepreinsto",
      "servicetype": "pdapreinsto",
      "express": "DE6E7C94",
      "ciphertext": "7bf6de0535b9746346937423067db4b2",
      "data": {
        "userName": Url.PDAusername,
        "proStoDate": ServerTime,
        "factoryId": Url.PDAFid,
        "rowguid": rowguid,
        "libraryId": libraryId,
        "compIds": compIdstr
      }
    }
    console.log("🚀 ~ file: steel_cage_storage.js ~ line 191 ~ SteelCage ~ data", data)
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: steel_cage_storage.js ~ line 193 ~ SteelCage ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('保存成功');
        setTimeout(() => {
          this.props.navigation.replace(this.props.navigation.getParam("page"), {
            title: this.props.navigation.getParam("title"),
            pageType: this.props.navigation.getParam("pageType"),
            page: this.props.navigation.getParam("page"),
            isAppPhotoSetting: this.props.navigation.getParam("isAppPhotoSetting"),
            isAppDateSetting: this.props.navigation.getParam("isAppDateSetting"),
            isAppPhotoAlbumSetting: this.props.navigation.getParam("isAppPhotoAlbumSetting"),
          })
          //this.props.navigation.navigate('PDAMainStack')
        }, 400)
      } else {
        this.toast.show('保存失败')
        Alert.alert('保存失败', resData.message)
      }
      console.log("🚀 ~ file: steel_cage_storage.js ~ line 195 ~ SteelCage ~ resData", resData)
    }).catch((error) => {
      this.toast.show('保存失败')
      if (error.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('保存失败', "响应内容不是合法JSON格式")
        return
      }
      if (error.toString().indexOf('Network request faile') != -1) {
        Alert.alert('保存失败', "网络请求错误")
        return
      }
      Alert.alert('保存失败', error.toString())
      console.log("🚀 ~ file: steel_cage_storage.js ~ line 75 ~ SteelCage ~ error", error)
    });
  }

  CardList = (compData) => {
    return (
      compData.map((item, index) => {
        let comp = item
        return (
          <View style={styles.CardList_main}>
            <TouchableCustom
              onPress={() => {
                this.cardListContont(comp);
              }}
              onLongPress={() => {
                Alert.alert(
                  '提示信息',
                  '是否删除构件信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      compDataArr.splice(index, 1)
                      compIdArr.splice(index, 1)
                      tmpstr = compIdArr.toString()

                      this.setState({
                        compDataArr: compDataArr,
                        compIdArr: compIdArr
                      })
                    }
                  }])

              }}>
              <View style={styles.CardList_title_main}>
                <View style={styles.title_num}>
                  <Text >{index + 1}</Text>
                </View>
                <View style={styles.title_title}>
                  <Text >{item.designType}</Text>
                </View>
                <View style={styles.CardList_at_icon}>
                  <IconDown />
                </View>
              </View>
            </TouchableCustom>
            {
              visibleArr.indexOf(item.compCode) != -1 ?
                <View key={index} style={{ backgroundColor: 'transparent' }} >
                  <ListItemScan_child
                    title='产品编号'
                    rightTitle={comp.compCode}
                  />
                  <ListItemScan_child
                    title='项目名称'
                    rightTitle={comp.projectName}
                  />
                  <ListItemScan_child
                    title='构件类型'
                    rightTitle={comp.compTypeName}
                  />
                  <ListItemScan_child
                    title='设计型号'
                    rightTitle={comp.designType}
                  />
                  <ListItemScan_child
                    title='楼号'
                    rightTitle={comp.floorNoName}
                  />
                  <ListItemScan_child
                    title='层号'
                    rightTitle={comp.floorName}
                  />
                  <ListItemScan_child
                    title='砼方量'
                    rightTitle={comp.volume}
                  />
                  <ListItemScan_child
                    title='重量'
                    rightTitle={comp.weight}
                  />
                  <ListItemScan_child
                    title='生产单位'
                    rightTitle={Url.PDAFname}
                  />
                  <View style={{ height: 10 }}></View>
                </View> : <View></View>
            }

          </View>
        )
      })
    )
  }

  isBottomVisible = (data, focusIndex) => {
    if (typeof (data) != "undefined") {
      if (data.length > 0) {
        this.setState({ bottomVisible: true, bottomData: data, focusIndex: focusIndex })
      }
    } else {
      this.toast.show("无数据")
    }
  }

  cardListContont = comp => {
    if (visibleArr.indexOf(comp.compCode) == -1) {
      visibleArr.push(comp.compCode);
      this.setState({ visibleArr: visibleArr });
    } else {
      if (visibleArr.length == 1) {
        visibleArr.splice(0, 1);
        this.setState({ visibleArr: visibleArr }, () => {
          this.forceUpdate;
        });
      } else {
        visibleArr.map((item, index) => {
          if (item == comp.compCode) {
            visibleArr.splice(index, 1);
            this.setState({ visibleArr: visibleArr }, () => {
              this.forceUpdate;
            });
          }
        });
      }
    }
  };

  _DatePicker = (index) => {
    const { ServerTime } = this.state
    return (
      <DatePicker
        customStyles={{
          dateInput: styles.dateInput,
          dateTouchBody: styles.dateTouchBody,
          dateText: styles.dateText,
        }}
        iconComponent={<Icon name='caretdown' color='#419fff' iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
        }
        showIcon={true}
        mode='datetime'
        onOpenModal={() => {
          this.setState({ focusIndex: index })
        }}
        date={ServerTime}
        format="YYYY-MM-DD HH:mm"
        onDateChange={(value) => {
          this.setState({
            ServerTime: value
          })
        }}
      />
    )
  }

  render() {
    const { navigation } = this.props;
    const QRid = navigation.getParam('QRid') || '';
    const type = navigation.getParam('type') || '';
    let pageType = navigation.getParam('pageType') || ''

    const { resData, isGettrackingID, isUsedPRacking, formCode, focusIndex, ServerTime, teamselect, roomselect, library, libraryId, libraryselect, libraryName, bottomVisible, bottomData, compData, compId, compCode } = this.state

    if (type == 'keeper') {
      keeper.map((item, index) => {
        if (QRid == item.keeperId) {
          QRkeeperID = item.keeperId
          QRkeeperName = item.keeperName
        }
      })
    } else {
      QRStock = QRid
    }
    console.log("🚀 ~ file: steel_cage_storage.js ~ line 237 ~ SteelCage ~ keeper.map ~ QRkeeperID", QRkeeperID)
    console.log("🚀 ~ file: steel_cage_storage.js ~ line 238 ~ SteelCage ~ keeper.map ~ QRkeeperName", QRkeeperName)

    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          <NavigationEvents
            onWillFocus={this.UpdateControl} />
          <ScrollView ref={ref => this.scoll = ref} style={styles.mainView} showsVerticalScrollIndicator={false} >

            <View style={styles.listView}>
              <ListItemScan
                title='表单编号'
                rightTitle={formCode}
              />
              <TouchableCustom underlayColor={'lightgray'} onPress={() => { this.isBottomVisible(Url.PDAnews, '0') }} >
                <ListItemScan
                  focusStyle={focusIndex == '0' ? styles.focusColor : {}}
                  title='库房'
                  rightElement={
                    <IconDown text={roomselect} />
                  }
                  bottomDivider
                />
              </TouchableCustom>
              <ListItemScan
                isButton={true}
                focusStyle={focusIndex == '1' ? styles.focusColor : {}}
                title='库位'
                onPress={() => {
                  this.setState({
                    type: 'libraryid',
                    focusIndex: 1
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressIn={() => {
                  this.setState({
                    type: 'libraryid',
                    focusIndex: 1
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}
                rightElement={
                  <View style={{ flexDirection: 'row' }}>
                    <TouchableCustom
                      style={{ height: 30 }}
                      onPress={() => {
                        if (library.length != 0) {
                          this.setState({ bottomVisible: true, bottomData: library, focusIndex: 1, type: 'libraryid' })
                        } else {
                          this.toast.show('请先选库房')
                        }
                      }} >
                      <View style={{ marginRight: 16, top: 9 }}>
                        <IconDown text={libraryselect} />
                      </View>
                    </TouchableCustom>
                    <ScanButton
                      onPress={() => {
                        this.setState({ focusIndex: 1, type: 'libraryid', isGetStoreroom: false })
                        this.props.navigation.navigate('QRCode', {
                          type: 'libraryid',
                          page: 'AdvanceStorage'
                        })
                      }}
                      onLongPress={() => {
                        this.setState({
                          type: 'libraryid',
                          focusIndex: 1
                        })
                        NativeModules.PDAScan.onScan();
                      }}
                      onPressOut={() => {
                        NativeModules.PDAScan.offScan();
                      }}
                    />
                  </View>
                }
              />
              {
                isUsedPRacking == '使用' ?
                  <View>
                    <ListItemScan
                      focusStyle={focusIndex == '4' ? styles.focusColor : {}}
                      isButton={!isGettrackingID}
                      title='工装唯一码'
                      onPressIn={() => {
                        this.setState({
                          type: 'trackingid',
                          focusIndex: 4
                        })
                        NativeModules.PDAScan.onScan();
                      }}
                      onPress={() => {
                        this.setState({
                          type: 'trackingid',
                          focusIndex: 4
                        })
                        NativeModules.PDAScan.onScan();
                      }}
                      onPressOut={() => {
                        NativeModules.PDAScan.offScan();
                      }}
                      rightElement={this.trackingIDItem(4)}
                    />
                    <ListItemScan
                      title='托盘/货架'
                      rightTitle={this.state.handingToolName}
                    />
                  </View> : <View></View>
              }
              <ListItemScan
                focusStyle={focusIndex == '2' ? styles.focusColor : {}}
                title='入库日期'
                rightTitle={this._DatePicker(2)}
              />
              <ListItemScan
                title='入库人'
                rightTitle={Url.PDAEmployeeName}
              />
              <ListItemScan
                isButton={true}
                focusStyle={focusIndex == '3' ? styles.focusColor : {}}
                title='入库明细'
                onPress={() => {
                  this.setState({
                    type: 'Stockid',
                    focusIndex: 3
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressIn={() => {
                  this.setState({
                    type: 'Stockid',
                    focusIndex: 3
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}
                rightElement={
                  <View style={{ flexDirection: 'row' }}>
                    <View>
                      <Input
                        ref={ref => { this.input1 = ref }}
                        containerStyle={styles.scan_input_container}
                        inputContainerStyle={styles.scan_inputContainerStyle}
                        inputStyle={[styles.scan_input]}
                        placeholder='请输入'
                        keyboardType='numeric'
                        value={compId}
                        onChangeText={(value) => {
                          value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
                          value = value.replace(/^\./g, ""); //验证第一个字符是数字
                          value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
                          //value = value.replace(/\b(0+)/gi, ""); //清楚开头的0
                          this.setState({
                            compId: value
                          })
                        }}
                        onFocus={() => {
                          this.setState({
                            type: 'Stockid',
                            focusIndex: 3
                          })
                        }}
                        onSubmitEditing={() => {
                          let inputComId = this.state.compId
                          console.log("🚀 ~ file: qualityinspection.js ~ line 468 ~ QualityInspection ~ inputComId", inputComId)
                          inputComId = inputComId.replace(/\b(0+)/gi, "")
                          this.GetStoreComponentInfo(inputComId)
                        }}
                      />
                    </View>
                    <ScanButton
                      //disabled={pageType == 'CheckPage'}
                      onPress={() => {
                        this.setState({
                          isGetStoreComponentInfo: false,
                          focusIndex: 3
                        })
                        this.props.navigation.navigate('QRCode', {
                          type: 'Stockid',
                          page: 'AdvanceStorage'
                        })
                      }}
                      onLongPress={() => {
                        this.setState({
                          type: 'Stockid',
                          focusIndex: 3
                        })
                        NativeModules.PDAScan.onScan();
                      }}
                      onPressOut={() => {
                        NativeModules.PDAScan.offScan();
                      }}
                    />
                  </View>
                }
              />

              {
                compCode ? this.CardList(compDataArr) : <View></View>
              }

            </View>

            {
              pageType == 'CheckPage' ? <View></View> :
                <FormButton
                  backgroundColor='#17BC29'
                  title='保存'
                  onPress={() => {
                    this.PostData()
                  }}
                />
            }



            <BottomSheet
              isVisible={bottomVisible}
              onRequestClose={() => {
                this.setState({
                  bottomVisible: false
                })
              }}
              onBackdropPress={() => {
                this.setState({
                  bottomVisible: false
                })
              }}
            >
              <ScrollView style={styles.bottomsheetScroll}>
                {
                  bottomData.map((item, index) => {
                    let title = ''
                    if (item.roomName) {
                      title = item.roomName
                    } else if (item.libraryName) {
                      title = item.libraryName
                    }
                    return (
                      <TouchableCustom
                        onPress={() => {
                          if (item.roomId) {
                            this.setState({
                              roomId: item.roomId,
                              roomselect: item.roomName,
                              roomName: item.roomName,
                              library: item.library
                            })
                          } else if (item.libraryId) {
                            this.setState({
                              libraryId: item.libraryId,
                              libraryName: item.libraryName,
                              libraryselect: item.libraryName
                            })
                          }
                          this.setState({
                            bottomVisible: false
                          })
                        }}
                      >
                        <BottomItem backgroundColor='white' color="#333" title={title} />

                      </TouchableCustom>
                    )

                  })
                }
              </ScrollView>
              <TouchableCustom
                onPress={() => {
                  this.setState({ bottomVisible: false })
                }}>
                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
              </TouchableCustom>

            </BottomSheet>
          </ScrollView>
        </View>
      )
    }
  }

}