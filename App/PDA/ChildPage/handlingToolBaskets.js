import React, { Component } from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert, DeviceEventEmitter, NativeModules } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, ListItem, BottomSheet, Tooltip } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { ToDay, YesterDay, BeforeYesterDay } from '../../CommonComponents/Data';
import { RFT } from '../../Url/Pixal';
import { ScrollView } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker'
import { NavigationEvents } from 'react-navigation';
import { styles } from '../Componment/PDAStyles';
import ScanButton from '../Componment/ScanButton';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import ListItemScan_child from '../Componment/ListItemScan_child';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';

let compDataArr = [], compIdArr = [], QRid2 = '', tmpstr = '', visibleArr = [];
let weightCount = 0, compNum = 0
let pageType = ''

export default class handlingToolBaskets extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bottomData: [],
      bottomVisible: false,
      resData: [],
      type: 'Stockid',
      formCode: '',
      rowguid: '',
      ServerTime: '',
      projectId: '',
      projectName: '',
      compTypeId: '',
      compTypeName: '',
      compDataArr: [],
      compData: '',
      compId: '',
      compIdArr: [],
      compCode: '',
      isGetStoreComponentInfo: false,
      isGetStoreroom: false,
      //工装是否使用
      Handlingtool: [],
      isUsedPRacking: '',
      handingToolName: '',
      handingUniqueCode: '',
      handingSelect: '请选择',
      handlingToolId: '',
      size: '',
      isGettrackingID: false,
      istrackingIDdelete: false,
      weight: '',
      compNum: '',
      //CardList
      visibleArr: [],
      hidden: -1,
      isVisible: false,
      focusIndex: 0,
      isLoading: true,
      isCheckPage: false,
      pageType: '',
      remark: ''
    };
  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  componentDidMount() {
    const { navigation } = this.props;
    let guid = navigation.getParam('guid') || ''
    pageType = navigation.getParam('pageType') || ''
    if (pageType == "CheckPage") {
      this.checkResData(guid)
      this.setState({ isCheckPage: true })
    } else {
      this.resData()
      this.setState({
        type: 'trackingid',
        focusIndex: 0
      })
    }

    //通过使用DeviceEventEmitter模块来监听事件
    this.iDataScan = DeviceEventEmitter.addListener('iDataScan', (Event) => {
      if (typeof Event.ScanResult != undefined) {
        let data = Event.ScanResult
        let arr = data.split("=");
        let id = ''
        id = arr[arr.length - 1]
        if (this.state.type == 'Stockid') {
          this.GetStoreComponentInfo(id)
        }
        if (this.state.type == 'libraryid') {
          this.GetStoreroom(id)
        }
        if (this.state.type == 'trackingid') {
          this.GetPalleTracking(id)
        }
      }
    });
  }

  UpdateControl = () => {
    if (typeof this.props.navigation.getParam('QRid') != "undefined") {
      if (this.props.navigation.getParam('QRid') != "") {
        this.toast.show(Url.isLoadingView, 0)
      }
    }
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || ''
    console.log("🚀 ~ file: storage.js ~ line 162 ~ PDAStorage ~ componentDidUpdate ~ QRid", QRid)
    const type = navigation.getParam('type') || '';

    if (type == 'Stockid') {
      this.GetStoreComponentInfo(QRid)
    } else if (type == 'trackingid') {
      this.GetPalleTracking(QRid)
    }

    console.log("🚀 ~ file: storage.js ~ line 1621 ~ PDAStorage ~ componentDidUpdate ~ QRid", QRid)

  }

  componentWillUnmount() {
    compDataArr = [], QRid2 = '', tmpstr = '', compIdArr = [], visibleArr = []
    weightCount = 0, compNum = 0
    pageType = ''
    this.iDataScan.remove()
  }

  checkResData = (guid) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "obtaihandlingtoolinfo",
      "servicetype": "pda",
      "express": "1369DBD0",
      "ciphertext": "4a11531d09901ddbe059ae9b1352e240",
      "data": {
        "factoryId": Url.PDAFid,
        "guid": guid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: handlingToolBaskets.js:148 ~ handlingToolBaskets ~ formData", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 204 ~ QualityInspection ~ resData", resData)
      let rowguid = resData.result.guid
      let ServerTime = resData.result.editdate
      let formCode = resData.result.formCode
      let handingUniqueCode = resData.result.PRacking
      let handlingToolId = resData.result.PRackingInfoGuid
      let compTypeName = resData.result.comptype
      let projectName = resData.result.projectname
      let summain = resData.result.summain
      let size = resData.result.size
      let weightmain = resData.result.weightmain
      let component = resData.result.component
      compNum = summain
      weightCount = weightmain
      let compCode = ""
      let compId = ''
      let remark = resData.result.remark

      component.map((item, index) => {
        item.comp[0].proeductcode = item.compCode
        compDataArr.push(item.comp[0])
        compIdArr.push(item.comp[0].compId)
        compCode = item.compCode
      })
      this.setState({
        resData: resData,
        rowguid: rowguid,
        ServerTime: ServerTime,
        formCode: formCode,
        handlingToolId: handlingToolId,
        handingUniqueCode: handingUniqueCode,
        handingSelect: handingUniqueCode,
        projectName: projectName,
        compTypeName: compTypeName,
        compNum: compNum,
        weightCount: weightCount,
        compDataArr: compDataArr,
        compIdArr: compIdArr,
        compId: compId,
        compCode: compCode,
        size: size,
        remark: remark,
        isGetStoreroom: true,
        isCheck: true
      })
      this.setState({
        isLoading: false,
      })
    }).catch((error) => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 215 ~ QualityInspection ~ error", error)
    });
  }

  resData = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "gethandlingtool_basketsservertime",
      "servicetype": "pda",
      "express": "B34B2B21",
      "ciphertext": "219527459fa29fa23c7d7ef001165d2b",
      "data": { "factoryId": Url.PDAFid }
    }

    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: steel_cage_storage.js ~ line 35 ~ SteelCage ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: steel_cage_storage.js ~ line 44 ~ SteelCage ~ resData", resData)
      //初始化错误提示
      if (resData.status == 100) {
        let formCode = resData.result.formCode
        let ServerTime = resData.result.ServerTime
        let rowguid = resData.result.rowguid
        let Handlingtool = resData.result.Handlingtool
        this.setState({
          resData: resData,
          formCode: formCode,
          ServerTime: ServerTime,
          Handlingtool: Handlingtool,
          rowguid: rowguid,
          isLoading: false
        })
      }
      else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }

    }).catch((error) => {
      console.log("🚀 ~ file: steel_cage_storage.js ~ line 75 ~ SteelCage ~ error", error)
    });
  }

  GetPalleTracking = (id) => {
    const { Handlingtool } = this.state
    console.log("🚀 ~ handlingToolBaskets ~ Handlingtool:", Handlingtool)
    for (let index = 0; index < Handlingtool.length; index++) {
      const item = Handlingtool[index];
      if (id == item.Handlingtoolid) {
        this.setState({
          handlingToolId: item.Handlingtoolid,
          handingToolName: item.uniqueCode,
          handingUniqueCode: item.uniqueCode,
          handingSelect: item.uniqueCode,
          size: item.size,
        })
        this.setState({
          type: 'Stockid',
          focusIndex: 2
        })
        // this.toast.show("加载成功")

        setTimeout(() => { this.toast.close(); this.scoll.scrollToEnd() }, 300)
        break;
      } else {
        if (index == (Handlingtool.length - 1)) {
          this.toast.show('没有此托盘')
        }
      }
    }

    // if (this.state.handlingToolId.length != 0) {
    //   return
    // } else {
    //   this.toast.show('没有此托盘')
    // }
  }

  GetStoreComponentInfo = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "handlingtool_basketsdetail",
      "servicetype": "pda",
      "express": "227EB695",
      "ciphertext": "de110251c4c1faf0038a3416d728a830",
      "data": {
        "compId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: handlingToolBaskets.js:217 ~ handlingToolBaskets ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: handlingToolBaskets.js:232 ~ handlingToolBaskets ~ resData", resData)
      this.toast.close()
      if (resData.status == '100') {
        let compData = resData.result[0]
        console.log("🚀 ~ file: handlingToolBaskets.js:237 ~ handlingToolBaskets ~ compData", compData)
        let compId = compData.compid
        console.log("🚀 ~ file: handlingToolBaskets.js:239 ~ handlingToolBaskets ~ compData.compid", compData.compid)
        console.log("🚀 ~ file: handlingToolBaskets.js:238 ~ handlingToolBaskets ~ compId", compId)
        let compCode = compData.proeductcode
        let projectId = compData.projectId
        let projectName = compData.projectName
        let compTypeId = compData.compTypeId
        let compTypeName = compData.compTypeName
        let weight = compData.weight
        if (compDataArr.length == 0) {
          compDataArr.push(compData)
          compIdArr.push(compId)
          weightCount += Number(weight)
          weightCount = Number(weightCount.toFixed(3))
          compNum += 1
          this.setState({
            compDataArr: compDataArr,
            compIdArr: compIdArr,
            compData: compData,
            compId: "",
            compCode: compCode,
            projectId: projectId,
            projectName: projectName,
            compTypeId: compTypeId,
            compTypeName: compTypeName,
            compNum: compNum,
            isGetStoreComponentInfo: true
          })
          tmpstr = tmpstr + compId + ','
          console.log("🚀 ~ file: handlingToolBaskets.js:257 ~ handlingToolBaskets ~ tmpstr", tmpstr)
        } else {
          if (tmpstr.indexOf(compId) == -1) {
            if (this.state.projectName == projectName && this.state.compTypeName == compTypeName) {
              compIdArr.push(compId)
              compDataArr.push(compData)
              weightCount += Number(weight)
              weightCount = Number(weightCount.toFixed(3))
              compNum += 1
              this.setState({
                compDataArr: compDataArr,
                compIdArr: compIdArr,
                compData: compData,
                compId: "",
                compCode: compCode,
                isGetStoreComponentInfo: true
              })
              tmpstr = tmpstr + JSON.stringify(compId) + ','
              console.log("🚀 ~ file: handlingToolBaskets.js:274 ~ handlingToolBaskets ~ tmpstr", tmpstr)
            } else {
              if (this.state.projectName != projectName) {
                this.toast.show('项目名称不符')
              }
              if (this.state.compTypeName != compTypeName) {
                this.toast.show('构件类型不符')
              }
            }
          } else {
            this.toast.show('已经扫瞄过此构件')
          }

        }
        console.log("🚀 ~ file: storage.js ~ line 292 ~ PDAStorage ~ compDataArr", compDataArr)
        setTimeout(() => { this.scoll.scrollToEnd() }, 300)
      } else {
        this.toast.close()
        Alert.alert('错误', resData.message)
        this.setState({
          compId: ""
        })
        //this.input1.focus()
      }

    }).catch(err => {
      this.toast.close()
      console.log("🚀 ~ file: qualityinspection.js ~ line 210 ~ SteelCage ~ err", err)
    })
  }

  CardList = (compData) => {
    return (
      compData.map((item, index) => {
        let comp = item
        return (
          <View style={styles.CardList_main}>
            <TouchableCustom
              onPress={() => {
                this.cardListContont(comp);
              }}
              onLongPress={() => {
                Alert.alert(
                  '提示信息',
                  '是否删除构件信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      compDataArr.splice(index, 1)
                      compIdArr.splice(index, 1)
                      tmpstr = compIdArr.toString()
                      weightCount = weightCount - comp.weight
                      compNum = compNum - 1

                      this.setState({
                        compDataArr: compDataArr,
                        compIdArr: compIdArr
                      })
                    }
                  }])

              }}>
              <View style={styles.CardList_title_main}>
                <View style={styles.title_num}>
                  <Text >{index + 1}</Text>
                </View>
                <View style={styles.title_title}>
                  <Text numberOfLines={1}>{item.proeductcode}</Text>
                </View>
                <View style={styles.CardList_at_icon}>
                  <IconDown />
                </View>
              </View>
            </TouchableCustom>
            {
              visibleArr.indexOf(item.proeductcode) != -1 ?
                <View key={index} style={{ backgroundColor: 'transparent' }} >
                  <ListItemScan_child
                    title='产品编号'
                    rightTitle={comp.proeductcode}
                  />
                  <ListItemScan_child
                    title='设计型号'
                    rightTitle={comp.designType}
                  />
                  <ListItemScan_child
                    title='楼号'
                    rightTitle={comp.floorNoName}
                  />
                  <ListItemScan_child
                    title='层号'
                    rightTitle={comp.floorName}
                  />
                  <ListItemScan_child
                    title='砼方量'
                    rightTitle={comp.volume}
                  />
                  <ListItemScan_child
                    title='重量'
                    rightTitle={comp.weight}
                  />
                  <ListItemScan_child
                    title='生产单位'
                    rightTitle={Url.PDAFname}
                  />
                  <View style={{ height: 10 }}></View>
                </View> : <View></View>
            }

          </View>
        )
      })
    )
  }

  cardListContont = comp => {
    if (visibleArr.indexOf(comp.proeductcode) == -1) {
      visibleArr.push(comp.proeductcode);
      this.setState({ visibleArr: visibleArr });
    } else {
      if (visibleArr.length == 1) {
        visibleArr.splice(0, 1);
        this.setState({ visibleArr: visibleArr }, () => {
          this.forceUpdate;
        });
      } else {
        visibleArr.map((item, index) => {
          if (item == comp.proeductcode) {
            visibleArr.splice(index, 1);
            this.setState({ visibleArr: visibleArr }, () => {
              this.forceUpdate;
            });
          }
        });
      }
    }
  };

  PostData = () => {
    const { formCode, ServerTime, handlingToolId, rowguid, compId, compTypeId, compTypeName, compIdArr, projectId, projectName, handingUniqueCode, size, remark } = this.state
    const { navigation } = this.props;

    console.log("🚀 ~ file: storage.js ~ line 511 ~ PDAStorage ~ compIdArr", compIdArr)
    let compIdstr = compIdArr.toString()
    console.log("🚀 ~ file: storage.js ~ line 506 ~ PDAStorage ~ compIdstr", compIdstr)

    if (handlingToolId.length == 0) {
      this.toast.show('没有选择托盘/货架')
      return
    } else if (compIdstr.length == 0) {
      this.toast.show('没有扫描构件')
      return
    }

    this.toast.show(saveLoading, 0)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "savehandlingtool_baskets",
      "servicetype": "pda",
      "express": "54FA70C0",
      "ciphertext": "042b311dd63cfff370a4230b6412a775",
      "data": {
        "projectid": projectId,
        "projectname": projectName,
        "formcode": formCode,
        "comptype": compTypeName,
        "comptypeid": compTypeId,
        "editdate": ServerTime,
        "PRackingInfoGuid": handlingToolId,
        "PRacking": handingUniqueCode,
        "size": size,
        "weightmain": weightCount,
        "summain": compNum,
        "editman": Url.PDAusername,
        "editmanid": Url.PDAEmployeeId,
        "rowguid": rowguid,
        "factoryId": Url.PDAFid,
        "remark": remark,
        "compid": compIdstr
      }
    }
    console.log("🚀 ~ file: steel_cage_storage.js ~ line 191 ~ SteelCage ~ data", data)
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: steel_cage_storage.js ~ line 193 ~ SteelCage ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('保存成功');
        setTimeout(() => {
          this.props.navigation.replace(this.props.navigation.getParam("page"), {
            title: this.props.navigation.getParam("title"),
            pageType: this.props.navigation.getParam("pageType"),
            page: this.props.navigation.getParam("page"),
            isAppPhotoSetting: this.props.navigation.getParam("isAppPhotoSetting"),
            isAppDateSetting: this.props.navigation.getParam("isAppDateSetting"),
            isAppPhotoAlbumSetting: this.props.navigation.getParam("isAppPhotoAlbumSetting"),
          })
          //this.props.navigation.navigate('PDAMainStack')
        }, 400)
      } else {
        this.toast.show('保存失败')
        Alert.alert('保存失败', resData.message)
      }
      console.log("🚀 ~ file: steel_cage_storage.js ~ line 195 ~ SteelCage ~ resData", resData)
    }).catch((error) => {
      this.toast.show('保存失败')
      if (error.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('保存失败', "响应内容不是合法JSON格式")
        return
      }
      if (error.toString().indexOf('Network request faile') != -1) {
        Alert.alert('保存失败', "网络请求错误")
        return
      }
      Alert.alert('保存失败', error.toString())
      console.log("🚀 ~ file: steel_cage_storage.js ~ line 75 ~ SteelCage ~ error", error)
    });
  }

  render() {
    const { formCode, projectName, compTypeName, ServerTime, Handlingtool, handlingToolId, handingToolName, handingUniqueCode, handingSelect, size, weight, focusIndex, remark, compId, compCode, compDataArr, bottomData, bottomVisible } = this.state
    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          <NavigationEvents
            onWillFocus={this.UpdateControl} />
          <ScrollView ref={ref => this.scoll = ref} style={styles.mainView} showsVerticalScrollIndicator={false} >
            <View style={styles.listView}>
              <ListItemScan
                title='表单编号'
                rightTitle={formCode}
              />
              <ListItemScan
                title='项目名称'
                rightTitle={projectName}
              />
              <ListItemScan
                title='构件类型'
                rightTitle={compTypeName}
              />
              <ListItemScan
                isButton={true}
                focusStyle={focusIndex == '0' ? styles.focusColor : {}}
                onPress={() => {
                  this.setState({
                    type: 'trackingid',
                    focusIndex: 0
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressIn={() => {
                  this.setState({
                    type: 'trackingid',
                    focusIndex: 0
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}
                title='托盘/货架'
                onFocus={() => { this.setState({ focusIndex: 0 }) }}
                rightElement={
                  <View style={{ flexDirection: 'row' }}>
                    <TouchableCustom
                      style={{ height: 30 }}
                      onPress={() => {
                        {
                          this.setState({ bottomVisible: true, bottomData: Handlingtool, focusIndex: 0, type: 'trackingid' })
                        }
                      }} >
                      <View style={{ marginRight: 16, top: 9 }}>
                        <IconDown text={handingSelect} />
                      </View>
                    </TouchableCustom>
                    <ScanButton
                      //disabled={pageType == 'CheckPage'}
                      onPress={() => {
                        this.setState({ focusIndex: 0, isGetStoreroom: false })
                        this.props.navigation.navigate('QRCode', {
                          type: 'trackingid',
                          page: 'HandlingToolBaskets'
                        })
                      }}
                      onLongPress={() => {
                        this.setState({
                          type: 'trackingid',
                          focusIndex: 0
                        })
                        NativeModules.PDAScan.onScan();
                      }}
                      onPressOut={() => {
                        NativeModules.PDAScan.offScan();
                      }}
                    />
                  </View>
                }
              />
              <ListItemScan
                title='尺寸'
                rightTitle={size}
              />
              <ListItemScan
                title='重量合计'
                rightTitle={weightCount}
              />
              <ListItemScan
                title='构件数量'
                rightTitle={compNum}
              />
              <ListItemScan
                title='操作日期'
                rightTitle={ServerTime}
              />
              <ListItemScan
                title='操作人'
                rightTitle={Url.PDAEmployeeName}
              />
              <ListItemScan
                focusStyle={focusIndex == '1' ? styles.focusColor : {}}
                title='备注'
                rightElement={
                  <View>
                    <Input
                      containerStyle={styles.quality_input_container}
                      inputContainerStyle={styles.inputContainerStyle}
                      inputStyle={[styles.quality_input_, { top: 7 }]}
                      placeholder='请输入'
                      value={remark}
                      onChangeText={(value) => {
                        this.setState({
                          remark: value
                        })
                      }} />
                  </View>
                }
              />
              <ListItemScan
                isButton={true}
                onPress={() => {
                  this.setState({
                    type: 'Stockid',
                    focusIndex: 2
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressIn={() => {
                  this.setState({
                    type: 'Stockid',
                    focusIndex: 2
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}
                focusStyle={focusIndex == '2' ? styles.focusColor : {}}
                title='装筐明细'
                rightElement={
                  <View style={{ flexDirection: 'row' }}>
                    <View>
                      <Input
                        ref={ref => { this.input1 = ref }}
                        containerStyle={styles.scan_input_container}
                        inputContainerStyle={styles.scan_inputContainerStyle}
                        inputStyle={[styles.scan_input]}
                        placeholder='请输入'
                        keyboardType='numeric'
                        value={compId}
                        onChangeText={(value) => {
                          value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
                          value = value.replace(/^\./g, ""); //验证第一个字符是数字
                          value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
                          //value = value.replace(/\b(0+)/gi, ""); //清楚开头的0
                          this.setState({
                            compId: value
                          })
                        }}
                        onFocus={() => {
                          this.setState({
                            type: 'Stockid',
                            focusIndex: 2
                          })
                        }}
                        onSubmitEditing={() => {
                          let inputComId = this.state.compId
                          inputComId = inputComId.replace(/\b(0+)/gi, "")
                          console.log("🚀 ~ file: storage.js ~ line 1147 ~ PDAStorage ~ render ~ inputComId", inputComId)
                          this.GetStoreComponentInfo(inputComId)
                        }}
                      />
                    </View>
                    <ScanButton
                      //disabled={pageType == 'CheckPage'}
                      onPress={() => {
                        this.setState({
                          isGetStoreComponentInfo: false,
                          focusIndex: 2
                        })
                        this.props.navigation.navigate('QRCode', {
                          type: 'Stockid',
                          page: 'HandlingToolBaskets'
                        })
                      }}
                      onLongPress={() => {
                        this.setState({
                          type: 'Stockid',
                          focusIndex: 2
                        })
                        NativeModules.PDAScan.onScan();
                      }}
                      onPressOut={() => {
                        NativeModules.PDAScan.offScan();
                      }}
                    />
                  </View>
                }

              />
              {
                compCode ? this.CardList(compDataArr) : <View></View>
              }


            </View>

            {
              pageType == 'CheckPage' ? <View></View> :
                <FormButton
                  backgroundColor='#17BC29'
                  title='保存'
                  onPress={() => {
                    this.PostData()
                  }}
                />
            }

            <BottomSheet
              isVisible={bottomVisible}
              onRequestClose={() => {
                this.setState({
                  bottomVisible: false
                })
              }}
              onBackdropPress={() => {
                this.setState({
                  bottomVisible: false
                })
              }}
            >
              <ScrollView style={styles.bottomsheetScroll}>
                {
                  bottomData.map((item, index) => {
                    let title = ''
                    if (item.Handlingtoolid) {
                      title = item.uniqueCode
                    }
                    return (
                      <TouchableCustom
                        onPress={() => {
                          if (item.Handlingtoolid) {
                            this.setState({
                              handlingToolId: item.Handlingtoolid,
                              handingToolName: item.uniqueCode,
                              handingUniqueCode: item.uniqueCode,
                              handingSelect: item.uniqueCode,
                              size: item.size,
                            })
                            this.setState({
                              type: 'Stockid',
                              focusIndex: 2
                            })
                            setTimeout(() => { this.scoll.scrollToEnd() }, 300)
                          }
                          this.setState({
                            bottomVisible: false
                          })
                        }}
                      >
                        <BottomItem backgroundColor='white' color="#333" title={title} />

                      </TouchableCustom>
                    )

                  })
                }
              </ScrollView>
              <TouchableCustom
                onPress={() => {
                  this.setState({ bottomVisible: false })
                }}>
                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
              </TouchableCustom>

            </BottomSheet>
          </ScrollView>
        </View>
      );
    }
  }
}
