import React from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert, Keyboard } from 'react-native';
import { Button, Text, Icon, Card, ListItem, BottomSheet, CheckBox, Input, Overlay, Header, SearchBar } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { RFT, deviceHeight, deviceWidth } from '../../Url/Pixal';
import { ScrollView } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker'
import { NavigationEvents } from 'react-navigation';
import ScanButton from '../Componment/ScanButton';
import { styles } from '../Componment/PDAStyles';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import ListItemScan_child from '../Componment/ListItemScan_child';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import CheckBoxScan from '../Componment/CheckBoxScan';
import AvatarAdd from '../Componment/AvatarAdd';
import AvatarImg from '../Componment/AvatarImg';
import DeviceStorage from '../../Url/DeviceStorage';

import { DeviceEventEmitter, NativeModules } from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';
import LinearGradient from 'react-native-linear-gradient';
import BatchListItem from '../Componment/BatchBottomItem';

const input1 = React.createRef();
const input2 = React.createRef();
const input3 = React.createRef();
const input4 = React.createRef();
const input5 = React.createRef();
const input6 = React.createRef();
const input7 = React.createRef();

const { height, width } = Dimensions.get('window') //获取宽高

let varGetError = false

let compDataArr = [], compIdArr = [], compIdArrTmp = [], QRid2 = '', tmpstr = '', weightAccount = 0, visibleArr = [];

let loadingData = {}

let driversSelectArr = [], driverHisSelectArr = [], driverTelHisSelectArr = [], carnumHisSelectArr = [], truckHisSelectArr = [], bottomData = [1, 2, 3, 4, 5, 6, 7, 8, 9, 9]

let driveButtonList = [
  { title: "历史记录", type: "driverHis" },
  { title: "司机信息", type: "driverLib" },
]

export default class SteelCage extends React.Component {
  constructor() {
    super();
    this.state = {
      deleteflag: '',
      bottomData: [1, 2, 3, 4, 5, 6, 7, 8, 9, 9],
      bottomVisible: false,
      bottomDriverVisible: false,
      isDropDownModalOpen: false,
      buttomTitle: '',
      resData: [],
      type: 'compid',
      formCodeSetup: '',
      formCode: '',
      formCodePlan: '',
      rowguid: '',
      ServerTime: '',
      loadOutTime: '',
      arrivalTime: '',
      //发货单号
      plans: [],
      planselect: '请选择',
      planId: '',
      planprojectName: '',
      isPlanVisible: false,
      PlanData: {},
      carrier: '',
      customer: '',
      cartrains: '',
      formCode_jh: '',
      projectName: '',
      //发货引导
      deliveryguide: [],
      isDeliveryGuideVisible: false,
      //车辆型号
      truckModels: [],
      truckHisSelectArr: [],
      truckModel: '',
      truckModelselect: '',
      driverTelephoneSerchText: '',
      //
      loadCarMethods: [],
      loadCarMethod: '',
      //司机
      drivers: [],
      driverinfo: [],
      driversSelectArr: [],
      driverHisSelectArr: [],
      driver: '',
      driverselect: '',
      driverSerchText: '',
      driverLibSelect: 'driverHis',
      driverFocusButton: 1,
      isShowDriverInfo: false,
      //司机电话
      isShowDriverTel: false,
      driverTelephoneArr: [' '],
      driverTelHisSelectArr: [],
      driverTelephone: '',
      driverTelephoneSelect: '请输入或选择',
      driverTelephoneSerchText: '',
      //车牌号
      carnums: [],
      carnumHisSelectArr: [],
      carnum: '',
      carnumselect: '',
      carnumTelephoneSerchText: '',
      //车板号
      sweepnums: [],
      sweepnum: '',
      sweepselect: '',
      //承运单位
      SupplierInfo: [],
      companyid: '',
      companyname: '',
      companyselct: '请选择',
      //产品编号
      compId: '',
      compCode: '',
      compData: [],
      compDataArr: [],
      compIdArr: [],
      isGetcomID: false,
      //木方
      WoodBeam: '0',
      //托架
      Bracket: '0',
      //装车方式
      loadCarMethods: [],
      loadCarMethod: '',
      loadCarMethodselect: '请选择',
      remark: '',
      //工装
      isUsedPRacking: '',
      handlingToolId: '',
      hidden: -1,
      GetError: false,
      QRID: '',
      focusIndex: '15',
      visibleArr: [],
      isLoading: true,
      buttonDisabled: false,
    }
    this.resData = this.resData.bind(this)
    //this.requestCameraPermission = this.requestCameraPermission.bind(this)
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    const { navigation } = this.props;
    let guid = navigation.getParam('guid') || ''
    let pageType = navigation.getParam('pageType') || ''
    if (pageType == 'CheckPage') {
      this.checkResData(guid)
    } else {
      this.resData()
      //DeviceStorage.delete('loadingData')
      DeviceStorage.get('loadingData')
        .then(res => {
          if (res) {
            console.log("🚀 ~ file: loading.js ~ line 133 ~ SteelCage ~ componentDidMount ~ res", res)
            let loadingDataObj = JSON.parse(res)
            loadingData = loadingDataObj
            console.log("🚀 ~ file: loading.js ~ line 128 ~ SteelCage ~ componentDidMount ~ loadingDataObj", loadingDataObj)
            if (res.length != 0) {
              Alert.alert(
                '提示信息',
                '是否使用挂单出库数据',
                [{
                  text: '取消',
                  style: 'cancel'
                }, {
                  text: '确定',
                  onPress: () => {
                    compDataArr = loadingDataObj.compDataArr
                    compIdArr = loadingDataObj.compIdArr
                    compIdArrTmp = loadingDataObj.compIdArrTmp
                    this.setState({
                      "Bracket": loadingDataObj.Bracket,
                      "formCode": loadingDataObj.formCode,
                      "carnum": loadingDataObj.carNum,
                      "carnumselect": loadingDataObj.carNum,
                      "remark": loadingDataObj.remark,
                      "sweepnum": loadingDataObj.sweepnum,
                      "sweepselect": loadingDataObj.sweepnum,
                      "compDataArr": loadingDataObj.compDataArr,
                      "compIdArr": loadingDataObj.compIdArr,
                      "truckModel": loadingDataObj.truckModel,
                      "truckModelselect": loadingDataObj.truckModel,
                      "companyid": loadingDataObj.companyid,
                      "companyname": loadingDataObj.companyname,
                      "companyselct": loadingDataObj.companyname,
                      "driver": loadingDataObj.driver,
                      "driverselect": loadingDataObj.driver,
                      "WoodBeam": loadingDataObj.WoodBeam,
                      "planId": loadingDataObj.planId,
                      "planselect": loadingDataObj.planselect,
                      "projectName": loadingDataObj.projectName,
                      "LoadCarMethod": loadingDataObj.loadCarMethod
                    })
                    if (typeof loadingDataObj.driverTelephone != 'undefined') {
                      this.setState({
                        "driverTelephone": loadingDataObj.driverTelephone,
                        "driverTelephoneSelect": loadingDataObj.driverTelephone,
                      })
                    }
                    if (loadingDataObj.planId) {
                      this.GetInvoice(loadingDataObj.planId)
                    }

                  }
                }])
            }
          }
        }).catch(err => {
          console.log("🚀 ~ file: loading.js ~ line 131 ~ SteelCage ~ componentDidMount ~ err", err)

        })
    }

    //通过使用DeviceEventEmitter模块来监听事件
    this.iDataScan = DeviceEventEmitter.addListener('iDataScan', (Event) => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 115 ~ SteelCage ~ Event.ScanResult", Event.ScanResult)
      if (typeof Event.ScanResult != undefined) {
        let data = Event.ScanResult
        let arr = data.split("=");
        let id = ''
        id = arr[arr.length - 1]
        console.log("🚀 ~ file: concrete_pouring.js ~ line 118 ~ SteelCage ~ id", id)
        if (this.state.type == 'compid') {
          this.GetcomID(id)
        } else if (this.state.type == 'trackingid') {
          this.GetManyComponentbyHandlingtool(id)
        }

      }
    });
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    compDataArr = [], compIdArr = [], compIdArrTmp = [], QRid2 = '', tmpstr = '', weightAccount = 0, visibleArr = [];
    this.iDataScan.remove()
  }

  _keyboardDidShow() {

  }

  UpdateControl = () => {
    if (typeof this.props.navigation.getParam('QRid') != "undefined") {
      if (this.props.navigation.getParam('QRid') != "") {
        this.toast.show(Url.isLoadingView, 0)
      }
    }
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || ''
    navigation.setParams({ QRid: '' })
    const type = navigation.getParam('type') || '';
    console.log("🚀 ~ file: loading.js:245 ~ SteelCage ~ navigation.getParam('type'):", navigation.getParam('type'))
    console.log("🚀 ~ file: loading.js:246 ~ SteelCage ~ type:", type)
    navigation.setParams({ type: '' })

    navigation.setParams({ QRid: '' })
    navigation.setParams({ type: '' })
    console.log("🚀 ~ file: loading.js:2222 ~ SteelCage ~ QRid", navigation.getParam('QRid'))
    console.log("🚀 ~ file: loading.js:2223 ~ SteelCage ~ type", navigation.getParam('type'))

    if (QRid) {
      if (type == 'compid') {
        this.GetcomID(QRid)
      } else if (type == 'trackingid') {
        this.GetManyComponentbyHandlingtool(QRid)
      }
      return
    }

    DeviceStorage.get('isLoadingDataTmpUse')
      .then(res => {
        if (res == true) {
          DeviceStorage.get('loadingDataTmp')
            .then(res => {
              if (res) {
                console.log("🚀 ~ file: loading.js ~ line 133 ~ SteelCage ~ componentDidMount ~ res", res)
                let loadingDataObj = JSON.parse(res)
                loadingData = loadingDataObj
                console.log("🚀 ~ file: loading.js ~ line 128 ~ SteelCage ~ componentDidMount ~ loadingDataObj", loadingDataObj)
                if (res.length != 0) {
                  compDataArr = loadingDataObj.compDataArr
                  compIdArr = loadingDataObj.compIdArr
                  compIdArrTmp = loadingDataObj.compIdArrTmp
                  this.setState({
                    "Bracket": loadingDataObj.Bracket,
                    "formCode": loadingDataObj.formCode,
                    "planformCode": loadingDataObj.planformCode,
                    "formCodePlan": loadingDataObj.formCode,
                    "carnum": loadingDataObj.carNum,
                    "carnumselect": loadingDataObj.carNum,
                    "cartrains": loadingDataObj.cartrains,
                    "remark": loadingDataObj.remark,
                    "sweepnum": loadingDataObj.sweepnum,
                    "sweepselect": loadingDataObj.sweepnum,
                    "compDataArr": loadingDataObj.compDataArr,
                    "compIdArr": loadingDataObj.compIdArr,
                    "customer": loadingDataObj.customer,
                    "truckModel": loadingDataObj.truckModel,
                    "truckModelselect": loadingDataObj.truckModel,
                    "companyid": loadingDataObj.companyid,
                    "companyname": loadingDataObj.companyname,
                    "companyselct": loadingDataObj.companyname,
                    "driver": loadingDataObj.driver,
                    "driverselect": loadingDataObj.driver,
                    "driverTelephone": loadingDataObj.driverTelephone,
                    "driverTelephoneSelect": loadingDataObj.driverTelephone,
                    "WoodBeam": loadingDataObj.WoodBeam,
                    "planId": loadingDataObj.planId,
                    "planselect": loadingDataObj.planselect,
                    "projectName": loadingDataObj.projectName,
                    "LoadCarMethod": loadingDataObj.loadCarMethod,
                    "rowguid": loadingDataObj.rowguid
                  })
                  if (loadingDataObj.planId) {
                    this.GetInvoiceTmp(loadingDataObj.planId)
                    driversSelectArr = []
                    this.state.driverinfo.map((driveItem, driveIndex) => {
                      if (driveItem.companyname == loadingDataObj.carrier) {
                        driversSelectArr.push(driveItem)
                      }
                    })
                    this.setState({
                      driversSelectArr: driversSelectArr
                    })
                  }
                }
              }
            }).catch(err => {
              console.log("🚀 ~ file: loading.js:282 ~ SteelCage ~ err:", err)
            })
          DeviceStorage.save('isLoadingDataTmpUse', false)
          DeviceStorage.delete('loadingDataTmp')
        }
      }).catch(err => {
        console.log("🚀 ~ file: loading.js:285 ~ SteelCage ~ err:", err)

      })

  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: '装车出库',
      headerRight: (
        <View>
          {
            navigation.getParam("isShowDriverInfo") == true ?
              <Button
                type='outline'
                title='暂存出库'
                containerStyle={{ right: 10 }}
                buttonStyle={{ borderRadius: 10 }}
                titleStyle={{ fontSize: 13 }}
                onPress={() => {
                  navigation.navigate("PDALoadingTemData", {
                    title: '暂存出库单选择',
                    type: 'TmpData'
                  })
                }}></Button> : <View></View>
          }
        </View>


      )
    }
  }

  checkResData = (guid) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "ObtainLoadCarOut",
      "servicetype": "pda",
      "express": "8AC4C0B0",
      "ciphertext": "7df83f712027c63f147f6c2d4a43f08d",
      "data": {
        "guid": guid,
        "factoryId": Url.PDAFid,
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: loading.js ~ line 159 ~ SteelCage ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: loading.js ~ line 189 ~ SteelCage ~ resData", resData)

      let rowguid = resData.result.guid
      let formCodePlan = resData.result.wayBill
      let formCode = resData.result.formCode
      let loadOutTime = resData.result.loadOutTime
      let arrivalTime = resData.result.arrivalTime
      let carNum = resData.result.carNum
      let carBoardNum = resData.result.carBoardNum
      let carrier = resData.result.carrier
      let cartrains = resData.result.cartrains
      let customer = resData.result.customer
      let driver = resData.result.driver
      let planformCode = resData.result.planformCode
      let projectName = resData.result.projectName
      let loadPeople = resData.result.loadPeople
      let WoodBeam = resData.result.WoodBeam
      let Bracket = resData.result.Bracket
      let loadCarMethod = resData.result.LoadCarMethod
      let truckModel = resData.result.truckModel
      let deleteflag = '', driverTelephone = ''
      if (typeof resData.result.deleteflag != "undefined") {
        deleteflag = resData.result.deleteflag
        driverTelephone = resData.result.driverTelephone
        this.setState({
          isShowDriverTel: true,
          driverTelephone: driverTelephone,
          driverTelephoneSelect: driverTelephone,
        })
      }

      let component = resData.result.component
      let compDataArr = []
      component.map((item, index) => {
        if (item.comp.length == 1) {
          compDataArr.push(item.comp[0])
          compIdArr.push(item.comp[0].compId)
        } else {
          item.comp.map((compItem, index) => {
            compDataArr.push(compItem)
            compIdArr.push(compItem.compId)
          })
        }
      })

      let PlanData = {}, planResult = {}
      planResult.projectName = projectName
      planResult.carrier = carrier
      planResult.customer = customer
      planResult.cartrains = cartrains
      PlanData.result = planResult

      this.setState({
        resData: resData,
        formCode: formCode,
        formCodePlan: formCodePlan,
        formCode: formCodePlan,
        rowguid: rowguid,
        loadOutTime: loadOutTime,
        arrivalTime: arrivalTime,
        carnum: carNum,
        carnumselect: carNum,
        sweepnum: carBoardNum,
        sweepselect: carBoardNum,
        carrier: carrier,
        companyselct: carrier,
        cartrains: cartrains,
        driver: driver,
        driverselect: driver,
        planselect: planformCode,
        planprojectName: projectName,
        customer: customer,
        WoodBeam: WoodBeam,
        Bracket: Bracket,
        loadCarMethod: loadCarMethod,
        loadCarMethodselect: loadCarMethod,
        truckModel: truckModel,
        truckModelselect: truckModel,
        deleteflag: deleteflag,
        compData: component,
        compDataArr: compDataArr,
        PlanData: PlanData,
        isPlanVisible: true,
        isGetcomID: true,

      })
      this.setState({
        isLoading: false,
      })
    }).catch((error) => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 215 ~ QualityInspection ~ error", error)
    });
  }

  resData = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "LoadOutInitial",
      "servicetype": "pda",
      "express": "2066CAB8",
      "ciphertext": "36a65b6b6fd779dc9e0143147f65ae13",
      "data": { "factoryId": Url.PDAFid }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: loading.js ~ line 254 ~ SteelCage ~ formData", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      //return JSON.parse('<!DOCTYPE html>')
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: loading.js ~ line 345 ~ SteelCage ~ resData", resData)

      if (resData.status == 100) {
        let formCodeSetup = resData.result.formCodeSetup
        let formCode = resData.result.formCode
        let isUsedPRacking = resData.result.isUsedPRacking
        let rowguid = resData.result.rowguid
        let ServerTime = resData.result.ServerTime
        let plans = resData.result.plans
        let truckModels = resData.result.truckModels
        let loadCarMethods = resData.result.loadCarMethods
        let drivers = resData.result.drivers
        let driverinfo = [], driverTelephoneArr = []
        this.props.navigation.setParams({ isShowDriverInfo: false })
        if (typeof resData.result.driverinfo != 'undefined') {
          driverinfo = resData.result.driverinfo
          driverTelephoneArr = resData.result.driverTelephone
          driversSelectArr = driverinfo
          this.setState({ driverselect: '请输入或选择', carnumselect: '请输入或选择', truckModelselect: '请输入或选择' })
          this.setState({ isShowDriverInfo: true, driverLibSelect: "driverLib" })
          this.props.navigation.setParams({ isShowDriverInfo: true })
        }
        console.log("🚀 ~ file: loading.js:400 ~ SteelCage ~ driversSelectArr:", driversSelectArr)
        let carnums = resData.result.carnums
        let sweepnums = resData.result.sweepnums
        let SupplierInfo = resData.result.SupplierInfo
        this.setState({
          resData: resData,
          rowguid: rowguid,
          ServerTime: ServerTime,
          arrivalTime: ServerTime,
          loadOutTime: ServerTime,
          formCodeSetup: formCodeSetup,
          formCode: formCode,
          formCodePlan: formCode,
          plans: plans,
          truckModels: truckModels,
          loadCarMethods: loadCarMethods,
          isUsedPRacking: isUsedPRacking,
          loadCarMethod: '单张吊装',
          loadCarMethodselect: '单张吊装',
          drivers: drivers,
          driverinfo: driverinfo,
          driversSelectArr: driverinfo,
          driverTelephoneArr: driverTelephoneArr,
          carnums: carnums,
          sweepnums: sweepnums,
          SupplierInfo: SupplierInfo,
          driverHisSelectArr: drivers,
          driverTelHisSelectArr: driverTelephoneArr,
          carnumHisSelectArr: carnums,
          truckHisSelectArr: truckModels,
        })
        this.setState({
          isLoading: false,
        })
      } else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }

    }).catch((error) => {
      console.log("🚀 ~ file: loading.js:602 ~ SteelCage ~ error:", error)
      if (error.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('初始化失败', "响应内容不是合法JSON格式")
        return
      }
      if (error.toString().indexOf('Network request faile') != -1) {
        Alert.alert('初始化失败', "网络请求错误")
        return
      }
      Alert.alert('初始化失败', error.toString())
    });

  }

  DeleteData = () => {
    const { navigation } = this.props;
    let guid = navigation.getParam('guid') || ''
    console.log('DeleteData')
    let formData = new FormData();
    let data = {};
    data = {
      "action": "deleteLoadcar",
      "servicetype": "pda",
      "express": "06C795EE",
      "ciphertext": "a1e3818d57d9bfc9437c29e7a558e32e",
      "data": {
        "guid": guid,
        "factoryId": Url.PDAFid,
        "deleteflag": this.state.deleteflag
      },
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: loading.js ~ line 314 ~ SteelCage ~ DeleteData ~ formData", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('删除成功');
        this.props.navigation.navigate('MainCheckPage', {
          backType: "revise",
          test: '123'
        })
        // setTimeout(() => {
        //   
        // }, 400)
      } else {
        this.toast.show('删除失败', 3000)
      }
    }).catch((error) => {
      if (error.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('删除失败', "响应内容不是合法JSON格式")
        return
      }
      if (error.toString().indexOf('Network request faile') != -1) {
        Alert.alert('删除失败', "网络请求错误")
        return
      }
      Alert.alert('删除失败', error.toString())

    });

  }

  TmpPostData = () => {
    this.setState({ buttonDisabled: true })
    const { ServerTime, loadOutTime, arrivalTime, Bracket, formCode, carnum, remark, sweepnum, rowguid, compId, truckModel, companyid, driver, WoodBeam, planId, loadCarMethod, monitorId, compIdArr, driverTelephone } = this.state
    const { navigation } = this.props;
    const QRid = navigation.getParam('QRid') || '';
    let Bracket1 = Bracket, WoodBeam1 = WoodBeam

    if (planId.length == 0) {
      this.toast.show('发货单号不能为空');
      this.setState({ buttonDisabled: false })
      return
    } else if (companyid == '') {
      this.toast.show('承运单位不能为空');
      this.setState({ buttonDisabled: false })
      return
    } else if (driver == '') {
      this.toast.show('司机姓名不能为空');
      this.setState({ buttonDisabled: false })
      return
    } else if (carnum == '') {
      this.toast.show('车牌号不能为空');
      this.setState({ buttonDisabled: false })
      return
    } else if (carnum.length > 10) {
      this.toast.show('车牌号名字过长');
      this.setState({ buttonDisabled: false })
      return
    } else if (sweepnum.length > 10) {
      this.toast.show('车板号名字过长');
      this.setState({ buttonDisabled: false })
      return
    } else if (truckModel == '') {
      this.toast.show('车辆型号不能为空');
      this.setState({ buttonDisabled: false })
      return
    } else if (truckModel.length > 10) {
      this.toast.show('车辆型号名字过长');
      this.setState({ buttonDisabled: false })
      return
    } else if (loadCarMethod.length == 0) {
      this.toast.show('装车方式不能为空');
      this.setState({ buttonDisabled: false })
      return
    } else if (compIdArr.length == 0) {
      this.toast.show('装车明细不能为空');
      this.setState({ buttonDisabled: false })
      return
    }
    if (Bracket1.length == 0) {
      Bracket1 = "0"
    }
    if (WoodBeam1.length == 0) {
      WoodBeam1 = "0"
    }/* else if (InspectorId == '') {
      this.toast.show('质检员信息不能为空');
      return
    } */

    this.toast.show(saveLoading, 0)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "TemporarySaveLoad",
      "servicetype": "pda",
      "express": "173B0BD0",
      "ciphertext": "554ae676049cd672b7bb193a10fab046",
      "data": {
        "loadOutTime": loadOutTime,
        "Bracket": Bracket1,
        "formCode": formCode,
        "factoryId": Url.PDAFid,
        "carNum": carnum,
        "remark": remark,
        "userName": Url.PDAusername,
        "carBoardNum": sweepnum,
        "rowguid": rowguid,
        "compInfo": compIdArrTmp,
        "truckModel": truckModel,
        "companyid": companyid,
        "driver": driver,
        "driverTelephone": driverTelephone,
        "arrivalTime": arrivalTime,
        "WoodBeam": WoodBeam1,
        "planId": planId,
        "LoadCarMethod": loadCarMethod
      }
    }

    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: loading.js ~ line 461 ~ SteelCage ~ formData", formData)

    fetch(Url.PDAurl, {
      method: 'POST',

      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      this.setState({ buttonDisabled: false })
      if (resData.status == '100') {
        this.toast.show('暂存成功');
        loadingData = {}
        DeviceStorage.delete('loadingData')
        setTimeout(() => {
          this.props.navigation.replace(this.props.navigation.getParam("page"), {
            title: this.props.navigation.getParam("title"),
            pageType: this.props.navigation.getParam("pageType"),
            page: this.props.navigation.getParam("page"),
            isAppPhotoSetting: this.props.navigation.getParam("isAppPhotoSetting"),
            isAppDateSetting: this.props.navigation.getParam("isAppDateSetting"),
            isAppPhotoAlbumSetting: this.props.navigation.getParam("isAppPhotoAlbumSetting"),
          })
          //this.props.navigation.navigate('PDAMainStack')
        }, 400)
      } else {

        Alert.alert('暂存失败', resData.message)
        this.toast.show('暂存失败')

        //this.toast.show('保存失败')
      }
    }).catch((error) => {
      console.log("🚀 ~ file: loading.js:577 ~ SteelCage ~ error", error)
      this.toast.show('暂存失败')
      Alert.alert('暂存失败', error.toString())
      this.setState({ buttonDisabled: false })
    });
  }

  PostData = () => {
    this.setState({ buttonDisabled: true })
    const { ServerTime, loadOutTime, arrivalTime, Bracket, formCode, carnum, remark, sweepnum, rowguid, compId, truckModel, companyid, driver, WoodBeam, planId, loadCarMethod, monitorId, compIdArr, driverTelephone } = this.state
    const { navigation } = this.props;
    const QRid = navigation.getParam('QRid') || '';
    let Bracket1 = Bracket, WoodBeam1 = WoodBeam

    if (planId.length == 0) {
      this.toast.show('发货单号不能为空');
      this.setState({ buttonDisabled: false })
      return
    } else if (companyid == '') {
      this.toast.show('承运单位不能为空');
      this.setState({ buttonDisabled: false })
      return
    } else if (driver == '') {
      this.toast.show('司机姓名不能为空');
      this.setState({ buttonDisabled: false })
      return
    } else if (carnum == '') {
      this.toast.show('车牌号不能为空');
      this.setState({ buttonDisabled: false })
      return
    } else if (carnum.length > 10) {
      this.toast.show('车牌号名字过长');
      this.setState({ buttonDisabled: false })
      return
    } else if (sweepnum.length > 10) {
      this.toast.show('车板号名字过长');
      this.setState({ buttonDisabled: false })
      return
    } else if (truckModel == '') {
      this.toast.show('车辆型号不能为空');
      this.setState({ buttonDisabled: false })
      return
    } else if (truckModel.length > 10) {
      this.toast.show('车辆型号名字过长');
      this.setState({ buttonDisabled: false })
      return
    } else if (loadCarMethod.length == 0) {
      this.toast.show('装车方式不能为空');
      this.setState({ buttonDisabled: false })
      return
    } else if (compIdArr.length == 0) {
      this.toast.show('装车明细不能为空');
      this.setState({ buttonDisabled: false })
      return
    }
    if (Bracket1.length == 0) {
      Bracket1 = "0"
    }
    if (WoodBeam1.length == 0) {
      WoodBeam1 = "0"
    }/* else if (InspectorId == '') {
      this.toast.show('质检员信息不能为空');
      return
    } */

    this.toast.show(saveLoading, 0)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "saveLoadAndSwap",
      "servicetype": "pda",
      "express": "173B0BD0",
      "ciphertext": "554ae676049cd672b7bb193a10fab046",
      "data": {
        "loadOutTime": loadOutTime,
        "Bracket": Bracket1,
        "formCode": formCode,
        "factoryId": Url.PDAFid,
        "carNum": carnum,
        "remark": remark,
        "userName": Url.PDAusername,
        "carBoardNum": sweepnum,
        "rowguid": rowguid,
        "compIds": compIdArr,
        "truckModel": truckModel,
        "companyid": companyid,
        "driver": driver,
        "driverTelephone": driverTelephone,
        "arrivalTime": arrivalTime,
        "WoodBeam": WoodBeam1,
        "planId": planId,
        "LoadCarMethod": loadCarMethod
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: loading.js ~ line 461 ~ SteelCage ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      this.setState({ buttonDisabled: false })
      if (resData.status == '100') {
        this.toast.show('保存成功');
        loadingData = {}
        DeviceStorage.delete('loadingData')
        setTimeout(() => {
          this.props.navigation.replace(this.props.navigation.getParam("page"), {
            title: this.props.navigation.getParam("title"),
            pageType: this.props.navigation.getParam("pageType"),
            page: this.props.navigation.getParam("page"),
            isAppPhotoSetting: this.props.navigation.getParam("isAppPhotoSetting"),
            isAppDateSetting: this.props.navigation.getParam("isAppDateSetting"),
            isAppPhotoAlbumSetting: this.props.navigation.getParam("isAppPhotoAlbumSetting"),
          })
          //this.props.navigation.navigate('PDAMainStack')
        }, 400)
      } else {

        Alert.alert('保存失败', resData.message)
        this.toast.show('保存失败')

        //this.toast.show('保存失败')
      }
    }).catch((error) => {
      this.toast.show('保存失败')
      if (error.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('保存失败', "响应内容不是合法JSON格式")
        return
      }
      if (error.toString().indexOf('Network request faile') != -1) {
        Alert.alert('保存失败', "网络请求错误")
        return
      }
      Alert.alert('保存失败', error.toString())

    });
    //this.setState({ buttonDisabled: false })
  }

  GetcomID = (id) => {
    let formData = new FormData();
    let data = {};
    const { planId, } = this.state
    if (planId == '') {
      this.toast.show('请先选择发货单号')
      return
    }
    if (tmpstr.indexOf(id) != -1) {
      this.toast.show('已经扫瞄过此构件')
      return
    }
    Keyboard.dismiss()

    let correspondingArr = []
    compDataArr.map((item, index) => {
      correspondingArr.push(item)
    })

    data = {
      "action": "GetComponentCorresponding",
      "servicetype": "pda",
      "express": "905DFC2D",
      "ciphertext": "96b3b3b57fff0e8170754b7b58036e09",
      "data": {
        "planId": planId,
        "compId": [
          { "compId": id }
        ],
        "corresponding": correspondingArr,
        "factoryId": Url.PDAFid
      }
    }
    console.log("🚀 ~ file: loading.js:633 ~ SteelCage ~ data:", data)

    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: loading.js:636 ~ SteelCage ~ formData:", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let compId = resData.result[0].compId_source
        let compData = resData.result[0]
        let compCode = resData.result[0].compCode
        let tmpobj = {}, tmpobjTmp = {}
        if (compDataArr.length == 0) {
          compDataArr.push(compData)
          tmpobj.compId = compId
          compIdArr.push(tmpobj)
          tmpobjTmp.compId_source = resData.result[0].compId_source
          tmpobjTmp.compId_target = resData.result[0].compId_target
          compIdArrTmp.push(tmpobjTmp)
          this.setState({
            compData: compData,
            compIdArr: compIdArr,
            compDataArr: compDataArr,
            compCode: compCode,
            compId: "",
            isGetcomID: true,
          })
          tmpstr = tmpstr + JSON.stringify(compId) + ','
          weightAccount += compData.weight
        } else {
          if (tmpstr.indexOf(compId) == -1) {
            tmpobj.compId = compId
            compIdArr.push(tmpobj)
            console.log("🚀 ~ file: loading.js ~ line 546 ~ SteelCage ~ compIdArr", compIdArr)
            tmpobjTmp.compId_source = resData.result[0].compId_source
            tmpobjTmp.compId_target = resData.result[0].compId_target
            compIdArrTmp.push(tmpobjTmp)
            console.log("🚀 ~ file: loading.js:960 ~ SteelCage ~ compIdArrTmp:", compIdArrTmp)
            compId = this.state.compId + ',' + compId
            compDataArr.push(compData)
            this.setState({
              compData: compData,
              compIdArr: compIdArr,
              compDataArr: compDataArr,
              compCode: compCode,
              compId: "",
              isGetcomID: true,
            })
            tmpstr = tmpstr + tmpobj.compId + ','
            console.log("🚀 ~ file: loading.js ~ line 554 ~ SteelCage ~ tmpstr", tmpstr)
            weightAccount += compData.weight
          } else {
            this.toast.show('已经扫瞄过此构件')
          }
          this.setState({
            compId: ""
          })
        }

        const { loadOutTime, planselect, formCode_jh, projectName, Bracket, formCode, carnum, remark, sweepnum, rowguid, truckModel, companyid, driver, arrivalTime, WoodBeam, companyname, loadCarMethod, driverTelephone } = this.state

        loadingData = {
          "loadOutTime": loadOutTime,
          "Bracket": Bracket,
          "formCode": formCode,
          "carNum": carnum,
          "remark": remark,
          "sweepnum": sweepnum,
          "compDataArr": compDataArr,
          "compIdArr": compIdArr,
          "compIdArrTmp": compIdArrTmp,
          "truckModel": truckModel,
          "companyid": companyid,
          "companyname": companyname,
          "driver": driver,
          "driverTelephone": driverTelephone,
          "arrivalTime": arrivalTime,
          "WoodBeam": WoodBeam,
          "planId": planId,
          "planselect": planselect,
          "projectName": projectName,
          "LoadCarMethod": loadCarMethod,
        }

        DeviceStorage.save('loadingData', JSON.stringify(loadingData))


      } else {
        Alert.alert('错误', resData.message)
        this.setState({
          compId: ""
        })
      }

    }).catch(err => {
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  GetManyComponentbyHandlingtool = (id) => {
    this.props.navigation.setParams({ QRid: '' })
    let formData = new FormData();
    let data = {};
    const { planId } = this.state
    if (planId == '') {
      this.toast.show('请先选择发货单号')
      return
    }
    data = {
      "action": "GetManyComponentbyHandlingtool",
      "servicetype": "pda",
      "express": "905DFC2D",
      "ciphertext": "96b3b3b57fff0e8170754b7b58036e09",
      "data": {
        "planId": planId,
        "handlingToolId": id,
        "corresponding": [

        ],
        "factoryId": Url.PDAFid
      }
    }

    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let compData = resData.result.correspondingComponents
        console.log("🚀 ~ file: loading.js ~ line 581 ~ SteelCage ~ compData", compData)
        compData.map((item, index) => {
          console.log("🚀 ~ file: loading.js ~ line 583 ~ SteelCage ~ compData.map ~ item", item)
          let compId = item.compId_source
          let compCode = item.compCode
          let tmpobj = {}
          if (compDataArr.length == 0) {
            compDataArr.push(item)
            tmpobj.compId = compId
            compIdArr.push(tmpobj)
            this.setState({
              compData: compData,
              compIdArr: compIdArr,
              compDataArr: compDataArr,
              compCode: compCode,
              compId: "",
              isGetcomID: true,
            })
            tmpstr = tmpstr + JSON.stringify(tmpobj) + ' '
            weightAccount += item.weight
          } else {
            console.log("🚀 ~ file: loading.js ~ line 584 ~ SteelCage ~ compData.map ~ tmpstr", tmpstr)
            if (tmpstr.indexOf(compId) == -1) {
              tmpobj.compId = compId
              compIdArr.push(tmpobj)
              compId = this.state.compId + ',' + compId
              compDataArr.push(item)
              this.setState({
                compData: compData,
                compIdArr: compIdArr,
                compDataArr: compDataArr,
                compCode: compCode,
                compId: "",
                isGetcomID: true,
              })
              tmpstr = tmpstr + JSON.stringify(tmpobj) + ' '
              weightAccount += item.weight
            }
          }
        })
      } else {
        Alert.alert('错误', resData.message)
        this.setState({
          handlingToolId: ""
        })

      }

    }).catch(err => {
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  GetInvoice = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "GetInvoice",
      "servicetype": "pda",
      "express": "1A352485",
      "ciphertext": "ab5b14cd3652990e568ab2b747b23b2f",
      "data": {
        "planId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: loading.js ~ line 550 ~ SteelCage ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: loading.js:842 ~ SteelCage ~ resData:", resData)
      this.toast.close()
      if (resData.status == '100') {
        let planprojectName = resData.result.projectName
        let carrier = resData.result.carrier
        let customer = resData.result.customer
        let cartrains = resData.result.cartrains
        driversSelectArr = []
        this.state.driverinfo.map((driveItem, driveIndex) => {
          if (driveItem.companyname == carrier) {
            driversSelectArr.push(driveItem)
          }
        })
        this.setState({
          driversSelectArr: driversSelectArr
        })
        this.setState({
          PlanData: resData,
          planprojectName: planprojectName,
          carrier: carrier,
          customer: customer,
          cartrains: cartrains,
          isPlanVisible: true,
          companyselct: carrier,
          companyname: carrier
        })
        this.state.SupplierInfo.map((item, index) => {
          if (item.companyname == carrier) {
            this.setState({ companyid: item.companyid })
          }
        })
        const { loadOutTime, planId, planselect, formCode_jh, projectName, Bracket, formCode, carnum, remark, sweepnum, rowguid, truckModel, companyid, driver, arrivalTime, WoodBeam, companyname, loadCarMethod, driverTelephone } = this.state

        loadingData = {
          "loadOutTime": loadOutTime,
          "Bracket": Bracket,
          "formCode": formCode,
          "carNum": carnum,
          "remark": remark,
          "sweepnum": sweepnum,
          "compDataArr": compDataArr,
          "compIdArr": compIdArr,
          "truckModel": truckModel,
          "companyid": companyid,
          "companyname": companyname,
          "driver": driver,
          "driverTelephone": driverTelephone,
          "arrivalTime": arrivalTime,
          "WoodBeam": WoodBeam,
          "planId": planId,
          "planselect": planselect,
          "projectName": projectName,
          "LoadCarMethod": loadCarMethod,
        }

        DeviceStorage.save('loadingData', JSON.stringify(loadingData))
      } else {
        Alert.alert('ERROR', resData.message)
      }

    }).catch(err => {
    })
  }

  GetInvoiceTmp = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "GetInvoice",
      "servicetype": "pda",
      "express": "1A352485",
      "ciphertext": "ab5b14cd3652990e568ab2b747b23b2f",
      "data": {
        "planId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: loading.js ~ line 550 ~ SteelCage ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: loading.js:842 ~ SteelCage ~ resData:", resData)
      this.toast.close()
      if (resData.status == '100') {
        let planprojectName = resData.result.projectName
        let carrier = resData.result.carrier
        let customer = resData.result.customer
        let cartrains = resData.result.cartrains
        this.setState({
          PlanData: resData,
          planprojectName: planprojectName,
          carrier: carrier,
          customer: customer,
          cartrains: cartrains,
          isPlanVisible: true,
        })
      } else {
        Alert.alert('ERROR', resData.message)
      }

    }).catch(err => {
    })
  }

  GetDeliveryGuide = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getdeliveryguide",
      "servicetype": "pda",
      "express": "1A352485",
      "ciphertext": "ab5b14cd3652990e568ab2b747b23b2f",
      "data": {
        "planId": this.state.planId,
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let result = resData.result
        let deliveryguide = result
        this.setState({
          deliveryguide: deliveryguide
        })
        this.setState({ isDeliveryGuideVisible: true })
      } else {
        Alert.alert('ERROR', resData.message)
      }

    }).catch(err => {
    })
  }

  GetCommonMould = (id) => {
    let acceptanceName = '111'
    let formData = new FormData();
    let data = {};
    data = {
      "action": "GetCommonMould",
      "servicetype": "pda",
      "express": "50545047",
      "ciphertext": "f3eedf54128226aef62dd6023a1c1573",
      "data": {
        "compId": this.state.compId,
        "acceptanceId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let acceptanceId = resData.result.acceptanceId
        acceptanceName = resData.result.acceptanceName
        this.setState({
          acceptanceId: acceptanceId,
          acceptanceName: acceptanceName
        })
      } else {
        Alert.alert('ERROR', resData.message)
      }


    }).catch(err => {
    })
  }

  GetTeamPeopleInfo = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getTeamPeopleInfo",
      "servicetype": "pda",
      "express": "2B9B0E37",
      "ciphertext": "eb1a5891270a0ebdbb68d37e27f82b92",
      "data": {
        "factoryId": Url.PDAFid,
        "monitorId": id
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let monitorId = resData.result.monitorId
        let monitorName = resData.result.monitorName
        let teamId = resData.result.teamId
        let teamName = resData.result.teamName
        this.setState({
          monitorId: monitorId,
          monitorName: monitorName,
          monitorTeamId: teamId,
          monitorTeamName: teamName
        })
      } else {
        Alert.alert('ERROR', resData.message)
      }
    }).catch(err => {
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  GetInspector = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getInspector",
      "servicetype": "pda",
      "express": "CDAB7A2D",
      "ciphertext": "a590b603df9b5a4384c130c7c21befb7",
      "data": {
        "InspectorId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let InspectorId = resData.result.InspectorId
        let InspectorName = resData.result.InspectorName
        this.setState({
          InspectorId: InspectorId,
          InspectorName: InspectorName,
        })
      } else {
        Alert.alert('ERROR', resData.message)
      }

    }).catch(err => {
    })
  }

  _DatePicker = (Time, focusIndex, TimeString) => {
    const { ServerTime, loadOutTime, arrivalTime } = this.state
    return (
      <View >
        <DatePicker
          customStyles={{
            dateInput: styles.dateInput,
            dateTouchBody: styles.dateTouchBody,
            dateText: styles.dateText,
          }}
          iconComponent={<Icon name='caretdown' color='#419fff' iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
          }
          showIcon={true}
          onOpenModal={() => {
            this.setState({ focusIndex: focusIndex })
          }}
          mode='datetime'
          date={Time}
          format="YYYY-MM-DD HH:mm"
          onDateChange={(value) => {
            if (TimeString == 'loadOutTime') {
              this.setState({
                loadOutTime: value
              })
            } else {
              this.setState({
                arrivalTime: value
              })
            }

          }}
        />
      </View>
    )
  }

  CardList = (compData) => {
    console.log("🚀 ~ file: loading.js ~ line 886 ~ SteelCage ~ compData", compData)
    return (
      compData.map((item, index) => {
        let comp = item
        return (
          <View style={styles.CardList_main}>
            <TouchableCustom
              onPress={() => {
                this.cardListContont(comp);
              }}
              onLongPress={() => {
                Alert.alert(
                  '提示信息',
                  '是否删除构件信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      compDataArr.splice(index, 1)
                      console.log("🚀 ~ file: loading.js:1417 ~ SteelCage ~ compData.map ~ compDataArr:", compDataArr)
                      compIdArr.splice(index, 1)
                      compIdArrTmp.splice(index, 1)
                      weightAccount = 0;
                      compDataArr.map(item1 => {
                        let weight = parseFloat(item1.weight)
                        weightAccount += weight;
                      });
                      console.log("🚀 ~ file: loading.js:1423 ~ SteelCage ~ compData.map ~ weightAccount:", weightAccount)

                      tmpstr = "";
                      compIdArr.map(itemID => {
                        tmpstr += itemID.compId + ",";
                      });
                      console.log("🚀 ~ file: loading.js ~ line 929 ~ SteelCage ~ compData.map ~ tmpstr", tmpstr)
                      loadingData.compDataArr = compDataArr
                      loadingData.compIdArr = compIdArr
                      loadingData.compIdArrTmp = compIdArrTmp
                      this.setState({
                        compDataArr: compDataArr,
                        compIdArr: compIdArr
                      }, () => {
                        this.forceUpdate()
                      })
                    }
                  }])

              }}
            >
              <View style={styles.CardList_title_main}>
                <View style={styles.title_num}>
                  <Text >{index + 1}</Text>
                </View>
                <View style={styles.title_title}>
                  <Text >{comp.designType}</Text>
                </View>
                <View style={styles.CardList_at_icon}>
                  <IconDown text={'1个'} />
                </View>
              </View>
            </TouchableCustom>

            {
              visibleArr.indexOf(item.compCode) != -1 ?
                <View key={index} >
                  <ListItemScan_child
                    title='产品编号'
                    rightTitle={comp.compCode}
                  />
                  {/* <ListItem
                      title='项目名称'
                      rightTitle={comp.projectName}
                    /> */}
                  <ListItemScan_child
                    title='构件类型'
                    rightTitle={comp.compTypeName}
                  />
                  <ListItemScan_child
                    title='设计型号'
                    rightTitle={comp.designType}
                  />
                  {
                    typeof comp.buildingUnit != 'undefined' ?
                      <ListItemScan_child
                        title='单元号'
                        rightTitle={comp.buildingUnit}
                      /> : <View></View>
                  }
                  <ListItemScan_child
                    title='楼号'
                    rightTitle={comp.floorNoName}
                  />
                  <ListItemScan_child
                    title='层号'
                    rightTitle={comp.floorName}
                  />
                  <ListItemScan_child
                    title='砼方量'
                    rightTitle={comp.volume}
                  />
                  <ListItemScan_child
                    title='重量'
                    rightTitle={comp.weight}
                  />
                  <ListItemScan_child
                    title='库房'
                    rightTitle={comp.roomName}
                  />
                  <ListItemScan_child
                    title='库位'
                    rightTitle={comp.libraryName}
                  />
                  <View style={{ height: 8 }}></View>
                </View> : <View></View>
            }

          </View>
        )

      })
    )
  }

  BottomList = () => {
    const { bottomVisible, bottomData } = this.state
    const { navigation } = this.props;
    let pageType = navigation.getParam('pageType') || ''
    return (
      <BottomSheet
        isVisible={bottomVisible && pageType != 'CheckPage'}
        MAX_HEIGHT_CUS={385}
        onRequestClose={() => {
          this.setState({
            bottomVisible: false
          })
        }}
        onBackdropPress={() => {
          this.setState({
            bottomVisible: false
          })
        }}
      //modalProps={{ style: { height: 100 }, }}
      >
        <ScrollView style={styles.bottomsheetScroll}>
          {
            bottomData.map((item, index) => {
              let title = ''
              let dataItem = {}

              if (item.planId) {
                title = item.formCode_jh + ' ' + item.projectName
                dataItem.Title = item.formCode_jh + ' ' + item.projectName
                dataItem.subTitle = item.floorNamefloorNoName
              } else if (item.companyid) {
                title = item.companyname
              } else if (item.driver) {
                title = item.driver
              } else if (item.carnum) {
                title = item.carnum
              } else if (item.sweepnum) {
                title = item.sweepnum
              } else if (item.truckModel) {
                title = item.truckModel
              } else if (item.loadCarMethod) {
                title = item.loadCarMethod
              }

              return (
                <TouchableCustom
                  onPress={() => {
                    if (item.planId) {
                      this.setState({
                        planId: item.planId,
                        formCode_jh: item.formCode_jh,
                        projectName: item.projectName,
                        planselect: item.formCode_jh,
                      })
                      this.GetInvoice(item.planId)
                    } else if (item.companyid) {
                      this.setState({
                        companyid: item.companyid,
                        companyname: item.companyname,
                        companyselct: item.companyname,
                      })
                      driversSelectArr = []
                      this.state.driverinfo.map((driveItem, driveIndex) => {
                        if (driveItem.companyname == item.companyname) {
                          driversSelectArr.push(driveItem)
                        }
                      })
                      this.setState({
                        driversSelectArr: driversSelectArr
                      })
                    } else if (item.driver) {
                      this.setState({
                        driver: item.driver,
                        driverselect: item.driver,
                      })
                    } else if (item.carnum) {
                      this.setState({
                        carnum: item.carnum,
                        carnumselect: item.carnum,
                      })
                    } else if (item.sweepnum) {
                      this.setState({
                        sweepnum: item.sweepnum,
                        sweepselect: item.sweepnum,
                      })
                    } else if (item.truckModel) {
                      this.setState({
                        truckModel: item.truckModel,
                        truckModelselect: item.truckModel,
                      })
                    } else if (item.loadCarMethod) {
                      this.setState({
                        loadCarMethod: item.loadCarMethod,
                        loadCarMethodselect: item.loadCarMethod,
                      })
                    }

                    const { loadOutTime, planId, planselect, formCode_jh, projectName, Bracket, formCode, carnum, remark, sweepnum, rowguid, truckModel, companyid, driver, arrivalTime, WoodBeam, companyname, loadCarMethod, driverTelephone } = this.state

                    loadingData = {
                      "loadOutTime": loadOutTime,
                      "Bracket": Bracket,
                      "formCode": formCode,
                      "carNum": carnum,
                      "remark": remark,
                      "sweepnum": sweepnum,
                      "compDataArr": compDataArr,
                      "compIdArr": compIdArr,
                      "truckModel": truckModel,
                      "companyid": companyid,
                      "companyname": companyname,
                      "driver": driver,
                      "driverTelephone": driverTelephone,
                      "arrivalTime": arrivalTime,
                      "WoodBeam": WoodBeam,
                      "planId": planId,
                      "planselect": planselect,
                      "projectName": projectName,
                      "LoadCarMethod": loadCarMethod,
                    }

                    DeviceStorage.save('loadingData', JSON.stringify(loadingData))

                    this.setState({
                      bottomVisible: false
                    })
                  }}
                >
                  {
                    item.planId && this.state.isShowDriverInfo ?
                      <BatchListItem backgroundColor='white' color="#333" dataItem={dataItem} /> :
                      <BottomItem backgroundColor='white' color="#333" title={title} />
                  }
                </TouchableCustom>
              )
            })
          }
        </ScrollView>
        <TouchableCustom
          onPress={() => {
            this.setState({ bottomVisible: false })
          }}>
          <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
        </TouchableCustom>
      </BottomSheet>

    )
  }

  BottomListDriver = () => {
    const { bottomDriverVisible, isShowDriverInfo, bottomData } = this.state
    const { navigation } = this.props;
    let pageType = navigation.getParam('pageType') || ''
    return (
      <BottomSheet
        isVisible={bottomDriverVisible && pageType != 'CheckPage'}
        MAX_HEIGHT_CUS={400}
        onRequestClose={() => {
          this.setState({
            bottomDriverVisible: false
          })
        }}
        onBackdropPress={() => {
          this.setState({
            bottomDriverVisible: false
          })
        }}
      //modalProps={{ style: { height: 100 }, }}
      >
        {
          isShowDriverInfo == true ? <View style={{ flexDirection: 'row', backgroundColor: '#efefef' }}>
            {
              driveButtonList.map((item, index) => {
                return (
                  <Button
                    buttonStyle={{
                      //borderRadius: 20,
                      paddingHorizontal: 10,
                      width: 80,
                      height: 45,
                      //flex: 1,
                      backgroundColor: this.state.driverFocusButton == index ? 'white' : 'transparent'
                    }}
                    containerStyle={{
                      //flex: 1,
                      //borderRadius: 20,
                      marginTop: 6,
                      marginHorizontal: 6,
                    }}
                    titleStyle={{ fontSize: RFT * 3.5, color: this.state.driverFocusButton == index ? '#419FFF' : '#333' }}
                    type='clear'
                    title={item.title}
                    onPress={() => {
                      this.setState({ driverLibSelect: item.type, driverFocusButton: index })
                    }}
                  />

                )
              })
            }
          </View> : <View></View>
        }

        {
          this.state.driverLibSelect == "driverHis" ?
            <ScrollView style={styles.bottomsheetScroll}>
              {
                bottomData.map((item, index) => {
                  let title = ''
                  if (item.driver) {
                    title = item.driver
                  } else if (item.carnum) {
                    title = item.carnum
                  } else if (item.truckModel) {
                    title = item.truckModel
                  } else if (item.driverTelephone) {
                    title = item.driverTelephone
                  }
                  return (
                    <TouchableCustom
                      onPress={() => {
                        if (item.driver) {
                          this.setState({
                            driver: item.driver,
                            driverselect: item.driver,
                          })
                        } else if (item.carnum) {
                          this.setState({
                            carnum: item.carnum,
                            carnumselect: item.carnum,
                          })
                        } else if (item.truckModel) {
                          this.setState({
                            truckModel: item.truckModel,
                            truckModelselect: item.truckModel,
                          })
                        } else if (item.driverTelephone) {
                          this.setState({
                            driverTelephone: item.driverTelephone,
                            driverTelephoneSelect: item.driverTelephone,
                          })
                        }
                        const { loadOutTime, planId, planselect, formCode_jh, projectName, Bracket, formCode, carnum, remark, sweepnum, rowguid, truckModel, companyid, driver, arrivalTime, WoodBeam, companyname, loadCarMethod, driverTelephone } = this.state

                        loadingData = {
                          "loadOutTime": loadOutTime,
                          "Bracket": Bracket,
                          "formCode": formCode,
                          "carNum": carnum,
                          "remark": remark,
                          "sweepnum": sweepnum,
                          "compDataArr": compDataArr,
                          "compIdArr": compIdArr,
                          "truckModel": truckModel,
                          "companyid": companyid,
                          "companyname": companyname,
                          "driver": driver,
                          "driverTelephone": driverTelephone,
                          "arrivalTime": arrivalTime,
                          "WoodBeam": WoodBeam,
                          "planId": planId,
                          "planselect": planselect,
                          "projectName": projectName,
                          "LoadCarMethod": loadCarMethod,
                        }

                        DeviceStorage.save('loadingData', JSON.stringify(loadingData))

                        this.setState({
                          bottomDriverVisible: false
                        })
                      }}
                    >
                      <BottomItem backgroundColor='white' color="#333" title={title} />
                    </TouchableCustom>
                  )
                })
              }
            </ScrollView> : <ScrollView style={styles.bottomsheetScroll}>
              {
                this.state.driversSelectArr.map((item, index) => {
                  let dataItem = {}
                  dataItem.Title = item.driver
                  dataItem.rightTitle = item.driverTelephone
                  dataItem.subTitle = item.carnum
                  dataItem.subRightTitle = item.modelofCar
                  //console.log("🚀 ~ file: loading.js:1703 ~ SteelCage ~ this.state.driverinfo.map ~ dataItem:", dataItem)
                  return (
                    <TouchableCustom
                      onPress={() => {
                        if (item.driver) {
                          this.setState({
                            driver: item.driver,
                            driverselect: item.driver,
                            driverTelephone: item.driverTelephone,
                            driverTelephoneSelect: item.driverTelephone,
                            carnum: item.carnum,
                            carnumselect: item.carnum,
                            companyname: item.companyname,
                            companyid: item.companyid,
                            companyselct: item.companyname,
                            truckModel: item.modelofCar,
                            truckModelselect: item.modelofCar
                          })
                        }
                        const { loadOutTime, planId, planselect, formCode_jh, projectName, Bracket, formCode, carnum, remark, sweepnum, rowguid, truckModel, companyid, driver, arrivalTime, WoodBeam, companyname, loadCarMethod } = this.state

                        loadingData = {
                          "loadOutTime": loadOutTime,
                          "Bracket": Bracket,
                          "formCode": formCode,
                          "carNum": carnum,
                          "remark": remark,
                          "sweepnum": sweepnum,
                          "compDataArr": compDataArr,
                          "compIdArr": compIdArr,
                          "truckModel": truckModel,
                          "companyid": companyid,
                          "companyname": companyname,
                          "driver": driver,
                          "driverTelephone": item.driverTelephone,
                          "arrivalTime": arrivalTime,
                          "WoodBeam": WoodBeam,
                          "planId": planId,
                          "planselect": planselect,
                          "projectName": projectName,
                          "LoadCarMethod": loadCarMethod,
                        }

                        DeviceStorage.save('loadingData', JSON.stringify(loadingData))

                        this.setState({
                          bottomDriverVisible: false
                        })
                      }}
                    >
                      <BatchListItem backgroundColor='white' color="#333" dataItem={dataItem} />
                    </TouchableCustom>
                  )
                })
              }
            </ScrollView>
        }
        <TouchableCustom
          onPress={() => {
            this.setState({ bottomDriverVisible: false })
          }}>
          <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
        </TouchableCustom>
      </BottomSheet>

    )
  }

  cardListContont = comp => {
    if (visibleArr.indexOf(comp.compCode) == -1) {
      visibleArr.push(comp.compCode);
      this.setState({ visibleArr: visibleArr });
    } else {
      if (visibleArr.length == 1) {
        visibleArr.splice(0, 1);
        this.setState({ visibleArr: visibleArr }, () => {
          this.forceUpdate;
        });
      } else {
        visibleArr.map((item, index) => {
          if (item == comp.compCode) {
            visibleArr.splice(index, 1);
            this.setState({ visibleArr: visibleArr }, () => {
              this.forceUpdate;
            });
          }
        });
      }
    }
  };

  isBottomVisible = (data, focusIndex) => {
    console.log("🚀 ~ file: loading.js ~ line 1262 ~ SteelCage ~ data", data)
    if (typeof (data) != "undefined") {
      if (data.length > 0) {
        this.setState({ bottomVisible: true, bottomData: data, focusIndex: focusIndex })
      } else {
        this.toast.show("无数据")
      }
    } else {
      this.toast.show("无数据")
    }
  }

  isBottomVisibleDriver = (data, focusIndex) => {
    this.setState({ bottomData: data, focusIndex: focusIndex })
    // if (typeof (data) != "undefined") {
    //   if (data.length > 0) {
    //     this.setState({ bottomDriverVisible: true, bottomData: data, focusIndex: focusIndex })
    //   } else {
    //     this.toast.show("无数据")
    //   }
    // } else {
    //   this.toast.show("无数据")
    // }
  }

  PlanCardList = (PlanData) => {
    return (
      <View>
        <Card containerStyle={styles.card_containerStyle}>
          <ListItemScan_child
            title='项目名称'
            rightTitle={PlanData.result.projectName}
          />
          <ListItemScan_child
            title='承运单位'
            rightTitle={PlanData.result.carrier}
          />
          <ListItemScan_child
            title='客户名称'
            rightTitle={PlanData.result.customer}
          />
          <ListItemScan_child
            title='组合车次'
            rightTitle={PlanData.result.cartrains}
          />
          <ListItemScan_child
            title='发货引导'
            rightElement={
              <View>
                <Text onPress={() => { this.GetDeliveryGuide() }} style={[styles.rightTitle, { color: '#419FFF', fontSize: 13, textAlign: 'right' }]} numberOfLines={1}>
                  查看
                </Text>
              </View>}
          />
        </Card>
      </View>
    )
  }

  setFocus = (isfocus) => {
    if (isfocus) {
      return styles.focusColor
    } else {
      return
    }
  }

  onSearchDriver = (text) => {
    const { driverinfo, focusIndex, drivers, driverTelephoneArr, truckModels, carnums } = this.state
    let re = /[\u4E00-\u9FA5]/g; //测试中文字符的正则
    let textArr = []
    let searchArr = [[]]
    let reasonArr = [], reasonHisArr = [], reasonTelArr = [], reasonCarArr = [], reasonTruckkArr = []
    driversSelectArr = []
    if (text.length > 0) {
      textArr = text.split("")
      for (let i = 0; i < textArr.length; i++) {
        const tempsearchArr = searchArr.map(subset => {
          const one = subset.concat([]);
          one.push(textArr[i]);
          return one;
        })
        searchArr = searchArr.concat(tempsearchArr);
      }

      searchArr.map((item, index) => {
        if (item.length > 0) {
          //司机信息
          driverinfo.map((driverItem) => {
            if (this.state.companyname.length > 0) {
              if (driverItem.companyname == this.state.companyname) {
                let arr = Object.keys(driverItem).map((i, n) => {
                  console.log("🚀 ~ file: loading.js:1926 ~ SteelCage ~ arr ~ i:", i)
                  if (i == 'driver') {
                    return driverItem[i]
                  }
                  if (i == 'driverTelephone') {
                    return driverItem[i]
                  }
                  if (i == 'carnum') {
                    return driverItem[i]
                  }
                  if (i == 'modelofCar') {
                    return driverItem[i]
                  }
                });
                // arr = 
                let arrString = arr.join(" ")
                console.log("🚀 ~ file: loading.js:1885 ~ SteelCage ~ driverinfo.map ~ arrString:", arrString)
                if (arrString.indexOf(item) != -1) {
                  let tmp = driverItem
                  reasonArr.push(tmp)
                }
              }
            } else {
              let arr = Object.keys(driverItem).map((i, n) => {
                console.log("🚀 ~ file: loading.js:1926 ~ SteelCage ~ arr ~ i:", i)
                if (i == 'driver') {
                  return driverItem[i]
                }
                if (i == 'driverTelephone') {
                  return driverItem[i]
                }
                if (i == 'carnum') {
                  return driverItem[i]
                }
                if (i == 'modelofCar') {
                  return driverItem[i]
                }
              });
              // arr = 
              let arrString = arr.join(" ")
              console.log("🚀 ~ file: loading.js:1885 ~ SteelCage ~ driverinfo.map ~ arrString:", arrString)
              if (arrString.indexOf(item) != -1) {
                let tmp = driverItem
                reasonArr.push(tmp)
              }
            }
          })
          //历史记录
          if (focusIndex == 5) {
            drivers.map((driverhisitem) => {
              let driverhisStr = driverhisitem.driver
              if (driverhisStr.indexOf(item) != -1) {
                let tmp = driverhisitem
                reasonHisArr.push(tmp)
              }
            })
          }
          if (focusIndex == 17) {
            driverTelephoneArr.map((drivertelitem) => {
              let drivertelStr = drivertelitem.driverTelephone
              if (drivertelStr.indexOf(item) != -1) {
                let tmp = drivertelitem
                reasonTelArr.push(tmp)
              }
            })
          }
          if (focusIndex == 6) {
            carnums.map((carnumitem) => {
              let carnumStr = carnumitem.carnum
              if (carnumStr.indexOf(item) != -1) {
                let tmp = carnumitem
                reasonCarArr.push(tmp)
              }
            })
          }
          if (focusIndex == 8) {
            truckModels.map((truckitem) => {
              let truckStr = truckitem.truckModel
              if (truckStr.indexOf(item) != -1) {
                let tmp = truckitem
                reasonCarArr.push(tmp)
              }
            })
          }
        }
      })
      driversSelectArr = [...new Set(reasonArr)]
      driverHisSelectArr = [...new Set(reasonHisArr)]
      driverTelHisSelectArr = [...new Set(reasonTelArr)]
      carnumHisSelectArr = [...new Set(reasonCarArr)]
      truckHisSelectArr = [...new Set(reasonTruckkArr)]
      if (this.state.driversSelectArr != driversSelectArr) {
        this.setState({
          driversSelectArr: driversSelectArr,
          driverHisSelectArr: driverHisSelectArr,
          driverTelHisSelectArr: driverTelHisSelectArr,
          carnumHisSelectArr: carnumHisSelectArr,
          truckHisSelectArr: truckHisSelectArr,
        })
        if (focusIndex == 5) {
          bottomData = driverHisSelectArr
          this.setState({ bottomData: driverHisSelectArr })
        }
        if (focusIndex == 17) {
          bottomData = driverTelHisSelectArr
          this.setState({ bottomData: driverTelHisSelectArr })
        }
        if (focusIndex == 6) {
          bottomData = carnumHisSelectArr
          this.setState({ bottomData: carnumHisSelectArr })
        }
        if (focusIndex == 8) {
          bottomData = truckHisSelectArr
          this.setState({ bottomData: truckHisSelectArr })
        }
      }
      //this.setState({driversSelectArr: driversSelectArr})
      console.log("🚀 ~ file: loading.js:2207 ~ SteelCage ~ render ~ driversSelectArr:", driversSelectArr)
    } else {
      this.driverSelectSet()
      this.driverHisSet()
    }
  }

  onInputDrive = () => {
    const { focusIndex, driverSerchText } = this.state
    if (driverSerchText.length > 0) {
      if (focusIndex == 5) {
        this.setState({
          driver: driverSerchText,
          driverselect: driverSerchText
        })
      }
      if (focusIndex == 17) {
        this.setState({
          driverTelephone: driverSerchText,
          driverTelephoneSelect: driverSerchText
        })
      }
      if (focusIndex == 6) {
        this.setState({
          carnum: driverSerchText,
          carnumselect: driverSerchText
        })
      }
      if (focusIndex == 8) {
        this.setState({
          truckModel: driverSerchText,
          truckModelselect: driverSerchText
        })
      }
    }
    this.setState({
      driverSerchText: '',
      isDropDownModalOpen: false
    })
    this.driverSelectSet()
  }

  dropDownBottomView = () => {
    const { focusIndex, bottomData } = this.state
    return (
      <BottomSheet
        isVisible={this.state.isDropDownModalOpen}
        MAX_HEIGHT_CUS={460}
        onBackdropPress={() => {
          this.setState({ isDropDownModalOpen: !this.state.isDropDownModalOpen, bottomData: [] });
        }}
        onRequestClose={() => {
          this.setState({ isDropDownModalOpen: !this.state.isDropDownModalOpen, bottomData: [] });
        }}>
        <View style={{
        }}>
          <View style={{ flexDirection: 'row', backgroundColor: '#efefef' }}>
            {
              driveButtonList.map((item, index) => {
                return (
                  <Button
                    buttonStyle={{
                      //borderRadius: 20,
                      paddingHorizontal: 10,
                      width: 80,
                      height: 45,
                      //flex: 1,
                      backgroundColor: this.state.driverFocusButton == index ? 'white' : 'transparent'
                    }}
                    containerStyle={{
                      //flex: 1,
                      //borderRadius: 20,
                      marginTop: 6,
                      marginHorizontal: 6,
                    }}
                    titleStyle={{ fontSize: RFT * 3.5, color: this.state.driverFocusButton == index ? '#419FFF' : '#333' }}
                    type='clear'
                    title={item.title}
                    onPress={() => {
                      this.setState({
                        driverLibSelect: item.type,
                        driverFocusButton: index
                      })
                    }}
                  />

                )
              })
            }
          </View>
          <View style={{ flexDirection: 'row' }}>
            <SearchBar
              platform='android'
              placeholder={'请输入或搜索内容...'}
              containerStyle={{ backgroundColor: 'white', shadowColor: '#999', }}
              inputContainerStyle={{ backgroundColor: '#f9f9f9', borderRadius: 45, width: deviceWidth * 0.76 }}
              inputStyle={[{ color: '#333' }, deviceWidth <= 330 && { fontSize: 14 }]}
              round={true}
              keyboardType={focusIndex == 17 ? 'number-pad' : 'default'}
              cancelIcon={() => ({
                type: 'material',
                size: 25,
                color: 'rgba(0, 0, 0, 0.54)',
                name: 'search',
              })}
              value={this.state.driverSerchText}
              onChangeText={(text) => {
                text = text.replace(/\s+/g, "")
                text = text.replace(/<\/?.+?>/g, "")
                text = text.replace(/[\r\n]/g, "")
                this.setState({
                  driverSerchText: text
                })
              }}
              returnKeyType='search'
              onClear={() => {
                this.driverSelectSet()
                this.driverHisSet()
              }}
              onSubmitEditing={() => {
                this.onSearchDriver(this.state.driverSerchText)
              }} />
            <Button
              containerStyle={{ width: deviceWidth * 0.2 }}
              buttonStyle={{ borderRadius: 20, width: deviceWidth * 0.16, top: 12, left: 10 }}
              title='确定'
              onPress={() => { this.onInputDrive() }} />
          </View>
          {
            this.flatListSet()
          }
        </View>
      </BottomSheet>
    )
  }

  dropDownModalOpenFunc = (data, focusIndex) => {
    let dataBottom = []
    bottomData = data
    if (typeof data != 'undefined' && data.length > 0) {
      data.map((item) => {
        item.index = Math.floor(Math.random() * 10000)
        dataBottom.push(item)
      })
    }
    console.log("🚀 ~ file: loading.js:2211 ~ SteelCage ~ bottomData:", bottomData)
    this.setState({
      focusIndex: focusIndex,
      bottomData: dataBottom,
      isDropDownModalOpen: true
    }, () => {
      console.log("🚀 ~ file: loading.js:2225 ~ SteelCage ~ bottomData:", this.state.bottomData)
      this.forceUpdate()
    })

    this.driverSelectSet()
  }

  driverSelectSet = () => {
    const { driverinfo, drivers, driverTelephoneArr, truckModels, carnums, focusIndex } = this.state
    driversSelectArr = []
    driverinfo.map((driveItem, driveIndex) => {
      if (this.state.companyname.length > 0) {
        if (driveItem.companyname == this.state.companyname) {
          driversSelectArr.push(driveItem)
        }
      } else {
        driversSelectArr.push(driveItem)
      }
    })
    this.setState({
      driversSelectArr: driversSelectArr,
      driverHisSelectArr: drivers,
      driverTelHisSelectArr: driverTelephoneArr,
      carnumHisSelectArr: carnums,
      truckHisSelectArr: truckModels,
    })

  }

  driverHisSet = () => {
    const { driverinfo, drivers, driverTelephoneArr, truckModels, carnums, focusIndex } = this.state
    if (focusIndex == 5) {
      bottomData = drivers
      this.setState({ bottomData: drivers })
    }
    if (focusIndex == 17) {
      bottomData = driverTelephoneArr
      this.setState({ bottomData: driverTelephoneArr })
    }
    if (focusIndex == 6) {
      bottomData = carnums
      this.setState({ bottomData: carnums })
    }
    if (focusIndex == 8) {
      bottomData = truckModels
      this.setState({ bottomData: truckModels })
    }
    console.log("🚀 ~ file: loading.js:2226 ~ SteelCage ~ bottomData:", this.state.bottomData)
  }

  render() {
    const { navigation } = this.props;
    let pageType = navigation.getParam('pageType') || ''

    const { resData, ServerTime, isUsedPRacking, focusIndex, loadOutTime, arrivalTime, formCode, formCodePlan, plans, compData, planId, projectName, planselect, isPlanVisible, planprojectName, customer, carrier, cartrains, SupplierInfo, companyid, companyname, companyselct, drivers, driver, driverselect, driverTelephone, driverTelephoneSerchText, driverTelephoneSelect, carnums, carnum, carnumselect, sweepnums, sweepnum, sweepselect, truckModels, truckModel, truckModelselect, WoodBeam, Bracket, loadCarMethods, loadCarMethod, loadCarMethodselect, compId, PlanData, compCode, remark, bottomVisible, compDataArr, handlingToolId, driverTelephoneArr, driverHisSelectArr, driverTelHisSelectArr, carnumHisSelectArr, truckHisSelectArr } = this.state

    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          <NavigationEvents
            onWillFocus={this.UpdateControl} />
          <ScrollView ref={ref => this.scoll = ref} style={styles.mainView} keyboardShouldPersistTaps={'always'} >
            <View style={styles.listView}>
              <ListItemScan
                title='表单编号'
                rightTitle={formCodePlan}
                focusStyle={focusIndex == '1' ? styles.focusColor : {}}
              />
              <TouchableCustom underlayColor={'lightgray'} onPress={() => { this.isBottomVisible(plans, '2') }} >
                <ListItemScan
                  title='发货单号'
                  focusStyle={focusIndex == '2' ? styles.focusColor : {}}
                  rightElement={
                    <TouchableOpacity onPress={() => {
                      this.setState({ bottomVisible: true, bottomData: plans, focusIndex: 2 })
                    }}>
                      <IconDown text={planselect} />
                      {/* <View style={{ flexDirection: "row" }}>
                      <Text numberOfLines={1} style={{ width: width / 2, textAlign: 'right' }} >{planselect}</Text>
                      <Icon name='downsquare' color='#419fff' iconStyle={{ fontSize: 17, marginLeft: 1 }} type='antdesign' ></Icon>
                    </View> */}
                    </TouchableOpacity>

                  }
                />
              </TouchableCustom>

              {
                isPlanVisible ?
                  this.PlanCardList(PlanData) : <View></View>
              }
              <ListItemScan
                title='运单编号'
                //rightTitle={formCode}
                rightElement={
                  <Input
                    ref={ref => { this.input0 = ref }}
                    containerStyle={styles.quality_input_container}
                    inputContainerStyle={styles.inputContainerStyle}
                    inputStyle={[styles.quality_input_, { top: 2 }]}
                    placeholder={formCode.toString()}
                    placeholderTextColor={"#333"}
                    defaultValue={formCode.toString()}
                    value={formCode.toString()}
                    //onFocus={() => { this.setState({ focusIndex: 4 }) }}
                    onChangeText={(value) => {
                      this.setState({
                        formCode: value
                      })
                    }} />
                }
                focusStyle={focusIndex == '3' ? styles.focusColor : {}}
              />
              <TouchableCustom underlayColor={'lightgray'} onPress={() => { this.isBottomVisible(SupplierInfo, '4') }} >
                <ListItemScan
                  title='承运单位'
                  focusStyle={focusIndex == '4' ? styles.focusColor : {}}
                  rightElement={
                    <TouchableOpacity onPress={() => { this.setState({ bottomVisible: true, bottomData: SupplierInfo, focusIndex: '4' }) }} >
                      <IconDown text={companyselct} />
                    </TouchableOpacity>
                  }
                />
              </TouchableCustom>

              {
                this.state.isShowDriverInfo == true ?
                  <View>
                    <ListItemScan
                      title='司机姓名'
                      focusStyle={focusIndex == '5' ? styles.focusColor : {}}
                      rightElement={
                        <TouchableOpacity
                          onPress={() => {
                            this.dropDownModalOpenFunc(drivers, '5')
                          }}>
                          <View style={[styles.loading_input_view]}>
                            <Text numberOfLines={1} style={[{ left: -11, textAlign: 'right', width: deviceWidth * 0.4 }, driver.length == 0 ? { color: "#86939e" } : { color: "#242424" }]} >{driverselect}</Text>
                            <Icon
                              name='caretdown'
                              type='antdesign'
                              color='#419fff'
                              iconStyle={{ fontSize: 13, marginLeft: 8, marginRight: -1 }}
                            />
                          </View>
                        </TouchableOpacity>
                      } />

                    <ListItemScan
                      title='司机电话'
                      focusStyle={focusIndex == '17' ? styles.focusColor : {}}
                      rightElement={
                        <TouchableOpacity onPress={() => { this.dropDownModalOpenFunc(driverTelephoneArr, 17) }}>
                          <View style={[styles.loading_input_view]}>
                            <Text numberOfLines={1} style={[{ left: -11, textAlign: 'right', width: deviceWidth * 0.4 }, driverTelephone.length == 0 ? { color: "#86939e" } : { color: "#242424" }]} >{driverTelephoneSelect}</Text>
                            <Icon
                              name='caretdown'
                              type='antdesign'
                              color='#419fff'
                              iconStyle={{ fontSize: 13, marginLeft: 8, marginRight: -1 }}
                            />
                          </View>
                        </TouchableOpacity>
                      } />

                    <ListItemScan
                      title='车牌号'
                      focusStyle={focusIndex == '6' ? styles.focusColor : {}}
                      rightElement={
                        <TouchableOpacity onPress={() => { this.dropDownModalOpenFunc(carnums, 6) }}>
                          <View style={[styles.loading_input_view]}>
                            <Text numberOfLines={1} style={[{ left: -11, textAlign: 'right', width: deviceWidth * 0.4 }, carnum.length == 0 ? { color: "#86939e" } : { color: "#242424" }]} >{carnumselect}</Text>
                            <Icon
                              name='caretdown'
                              type='antdesign'
                              color='#419fff'
                              iconStyle={{ fontSize: 13, marginLeft: 8, marginRight: -1 }}
                            //containerStyle = {{backgroundColor:'red', width: 20, height: 20,}}
                            />
                          </View>
                        </TouchableOpacity>
                      }
                    />
                    <ListItemScan
                      title='车辆型号'
                      focusStyle={focusIndex == '8' ? styles.focusColor : {}}
                      rightElement={
                        <TouchableOpacity onPress={() => { this.dropDownModalOpenFunc(truckModels, 8) }}>
                          <View style={[styles.loading_input_view]}>
                            <Text numberOfLines={1} style={[{ left: -11, textAlign: 'right', width: deviceWidth * 0.4 }, truckModel.length == 0 ? { color: "#86939e" } : { color: "#242424" }]}>{truckModelselect}</Text>
                            <Icon
                              name='caretdown'
                              type='antdesign'
                              color='#419fff'
                              iconStyle={{ fontSize: 13, marginLeft: 8, marginRight: -1 }}
                            //containerStyle = {{backgroundColor:'red', width: 20, height: 20,}}
                            />
                          </View>
                        </TouchableOpacity>
                      }
                    />
                  </View> :
                  <View>
                    <ListItemScan
                      title='司机姓名'
                      focusStyle={focusIndex == '5' ? styles.focusColor : {}}
                      rightElement={
                        <View style={styles.loading_input_view}>
                          <Input
                            ref={input1}
                            containerStyle={styles.input_container}
                            inputContainerStyle={styles.inputContainerStyle}
                            inputStyle={styles.input_}
                            placeholder='请输入或选择'
                            value={driverselect}
                            onChangeText={(value) => {
                              value = value.replace(/\s+/g, "")
                              value = value.replace(/<\/?.+?>/g, "")
                              value = value.replace(/[\r\n]/g, "")
                              this.setState({
                                driverselect: value,
                                driver: value
                              })
                            }}
                            //onFocus={() => { this.setState({ focusIndex: 7 }) }}
                            //onSubmitEditing={() => { input2.current.focus() }}
                            underlineColorAndroid='transparent'
                          ></Input>
                          <Icon onPress={() => { this.isBottomVisible(drivers, 7) }}
                            name='caretdown'
                            type='antdesign'
                            color='#419fff'
                            iconStyle={{ fontSize: 13, marginLeft: 8, marginRight: -1 }}
                          />
                        </View>
                      } />
                    {
                      this.state.isShowDriverTel == true ? <ListItemScan
                        title='司机电话'
                        focusStyle={focusIndex == '17' ? styles.focusColor : {}}
                        rightElement={
                          <View style={[styles.loading_input_view]}>
                            <Text numberOfLines={1} style={[{ left: -11 }, driver.length == 0 ? { color: "#86939e" } : { color: "#242424" }]}>{driverTelephoneSelect}</Text>
                            <TouchableOpacity >
                              <Icon
                                name='caretdown'
                                type='antdesign'
                                color='#419fff'
                                iconStyle={{ fontSize: 13, marginLeft: 8, marginRight: -1 }}
                              //containerStyle = {{backgroundColor:'red', width: 20, height: 20,}}
                              />
                            </TouchableOpacity>
                          </View>
                        } /> : <View></View>
                    }
                    <ListItemScan
                      title='车牌号'
                      focusStyle={focusIndex == '6' ? styles.focusColor : {}}
                      rightElement={
                        <View style={styles.loading_input_view}>
                          <Input
                            ref={input2}
                            containerStyle={styles.input_container}
                            inputContainerStyle={styles.inputContainerStyle}
                            inputStyle={styles.input_}
                            placeholder='请输入或选择'
                            value={carnumselect}
                            onChangeText={(value) => {
                              value = value.replace(/\s+/g, "")
                              value = value.replace(/<\/?.+?>/g, "")
                              value = value.replace(/[\r\n]/g, "")
                              this.setState({
                                carnum: value,
                                carnumselect: value
                              })
                            }}
                            //onFocus={() => { this.setState({ focusIndex: 7 }) }}
                            //onSubmitEditing={() => { input3.current.focus() }}
                            underlineColorAndroid='transparent'
                          ></Input>
                          <Icon onPress={() => { this.isBottomVisible(carnums, 7) }}
                            name='caretdown'
                            type='antdesign'
                            color='#419fff'
                            iconStyle={{ fontSize: 13, marginLeft: 8, marginRight: -1 }}
                          />
                        </View>
                      }
                    />
                    <ListItemScan
                      title='车辆型号'
                      focusStyle={focusIndex == '8' ? styles.focusColor : {}}
                      rightElement={
                        <View style={styles.loading_input_view}>
                          <Input
                            ref={input3}
                            containerStyle={styles.input_container}
                            inputContainerStyle={styles.inputContainerStyle}
                            inputStyle={styles.input_}
                            placeholder='请输入或选择'
                            value={truckModelselect}
                            onChangeText={(value) => {
                              value = value.replace(/\s+/g, "")
                              value = value.replace(/<\/?.+?>/g, "")
                              value = value.replace(/[\r\n]/g, "")
                              this.setState({
                                truckModelselect: value,
                                truckModel: value
                              })
                            }}
                            //onFocus={() => { this.setState({ focusIndex: 8 }) }}
                            //onSubmitEditing={() => { input4.current.focus() }}
                            underlineColorAndroid='transparent'
                          ></Input>
                          <Icon onPress={() => { this.isBottomVisible(truckModels, 8) }}
                            name='caretdown'
                            type='antdesign'
                            color='#419fff'
                            iconStyle={{ fontSize: 13, marginLeft: 8, marginRight: -1 }}
                          />
                        </View>
                      }
                    />
                  </View>
              }



              <ListItemScan
                title='车板号'
                focusStyle={focusIndex == '7' ? styles.focusColor : {}}
                rightElement={
                  <View style={styles.loading_input_view}>
                    <Input
                      ref={input4}
                      containerStyle={styles.input_container}
                      inputContainerStyle={styles.inputContainerStyle}
                      inputStyle={styles.input_}
                      placeholder='请输入或选择'
                      value={sweepselect}
                      onChangeText={(value) => {
                        value = value.replace(/\s+/g, "")
                        value = value.replace(/<\/?.+?>/g, "")
                        value = value.replace(/[\r\n]/g, "")
                        this.setState({
                          sweepselect: value,
                          sweepnum: value
                        })
                      }}
                      //onFocus={() => { this.setState({ focusIndex: 7 }) }}
                      //onSubmitEditing={() => { input5.current.focus() }}
                      underlineColorAndroid='transparent'
                    ></Input>
                    <Icon onPress={() => { this.isBottomVisible(sweepnums, 7) }}
                      name='caretdown'
                      type='antdesign'
                      color='#419fff'
                      iconStyle={{ fontSize: 13, marginLeft: 8, marginRight: -1 }}
                    />
                  </View>
                }
              />

              <ListItemScan
                focusStyle={focusIndex == '9' ? styles.focusColor : {}}
                leftElement={
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.text_beforeinput} >木方</Text>
                    <Input
                      ref={input5}
                      keyboardType='number-pad'
                      containerStyle={[styles.input_container, { width: width / 4, }]}
                      inputContainerStyle={styles.inputContainerStyle}
                      inputStyle={[styles.input_, { textAlign: 'center', top: 2 }]}
                      placeholder='请输入'
                      value={WoodBeam}
                      onChangeText={(value) => {
                        value = value.replace(/\s+/g, "")
                        value = value.replace(/<\/?.+?>/g, "")
                        value = value.replace(/[\r\n]/g, "")
                        value = value.replace(/\D/g, '')
                        this.setState({
                          WoodBeam: value
                        })
                      }}
                    //onFocus={() => { this.setState({ focusIndex: 9 }) }}
                    //onSubmitEditing={() => { input6.current.focus()}}
                    />
                  </View>
                }
                rightElement={
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.text_beforeinput} >托架</Text>
                    <Input
                      ref={input6}
                      keyboardType='number-pad'
                      containerStyle={[styles.input_container, { width: width / 4, }]}
                      inputContainerStyle={styles.inputContainerStyle}
                      inputStyle={[styles.input_, { textAlign: 'center' }]}
                      placeholder='请输入'
                      //onFocus={() => { this.setState({ focusIndex: 9 }) }}
                      value={Bracket}
                      onChangeText={(value) => {
                        value = value.replace(/\s+/g, "")
                        value = value.replace(/<\/?.+?>/g, "")
                        value = value.replace(/[\r\n]/g, "")
                        value = value.replace(/\D/g, '')
                        this.setState({
                          Bracket: value
                        })
                      }}
                    />
                  </View>
                }
              />
              <TouchableCustom underlayColor={'lightgray'} onPress={() => { this.isBottomVisible(loadCarMethods, '10') }} >
                <ListItemScan
                  focusStyle={focusIndex == '10' ? styles.focusColor : {}}
                  title='装车方式'
                  rightElement={
                    <IconDown text={loadCarMethodselect} />
                  }
                />
              </TouchableCustom>

              <ListItemScan
                focusStyle={focusIndex == '11' ? styles.focusColor : {}}
                title='出库日期'
                rightElement={this._DatePicker(loadOutTime, '11', "loadOutTime")}
              />
              <ListItemScan
                focusStyle={focusIndex == '12' ? styles.focusColor : {}}
                title='到货日期'
                rightElement={this._DatePicker(arrivalTime, '12', "arrivalTime")}
              />
              <ListItemScan
                focusStyle={focusIndex == '13' ? styles.focusColor : {}}
                title='出库人'
                rightTitle={Url.PDAEmployeeName}
              />
              <ListItemScan
                focusStyle={focusIndex == '14' ? styles.focusColor : {}}
                title='备注'
                rightElement={
                  <Input
                    containerStyle={[styles.quality_input_container, { top: 2 }]}
                    inputContainerStyle={styles.inputContainerStyle}
                    inputStyle={[styles.quality_input_]}
                    placeholder='请输入'
                    value={remark}
                    onFocus={() => { this.setState({ focusIndex: 14 }) }}
                    onChangeText={(value) => {
                      this.setState({
                        remark: value
                      })
                    }} />
                  /* <Input
                    containerStyle={styles.input_container}
                    inputStyle={[styles.input_, { marginTop: -11 }]}
                    inputContainerStyle={styles.inputContainerStyle}
                    placeholder='请输入'
                    value={remark}
                    onFocus={() => { this.setState({ focusIndex: 14 }) }}
                    onChangeText={(value) => {
                      this.setState({
                        remark: value
                      })
                    }}
                  /> */
                }
              />
              <ListItemScan
                title='重量合计'
                rightTitle={weightAccount.toFixed(2)}
              />

              {
                isUsedPRacking == '使用' ?
                  <ListItemScan
                    isButton={true}
                    focusStyle={focusIndex == '16' ? styles.focusColor : {}}
                    title='工装构件出库'
                    onPressIn={() => {
                      this.setState({
                        type: 'trackingid',
                        focusIndex: 16
                      })
                      NativeModules.PDAScan.onScan();
                    }}
                    onPress={() => {
                      this.setState({
                        type: 'trackingid',
                        focusIndex: 16
                      })
                      NativeModules.PDAScan.onScan();
                    }}
                    onPressOut={() => {
                      NativeModules.PDAScan.offScan();
                    }}
                    rightElement={
                      <View style={{ flexDirection: 'row' }}>
                        <View>
                          <Input
                            ref={ref => { this.input12 = ref }}
                            containerStyle={styles.scan_input_container}
                            inputContainerStyle={styles.scan_inputContainerStyle}
                            inputStyle={[styles.scan_input]}
                            placeholder='请输入'
                            keyboardType='numeric'
                            value={handlingToolId}
                            onChangeText={(value) => {
                              value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
                              value = value.replace(/^\./g, ""); //验证第一个字符是数字
                              value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
                              //value = value.replace(/\b(0+)/gi, ""); //清楚开头的0
                              this.setState({
                                handlingToolId: value
                              })
                            }}
                            onFocus={() => {
                              this.setState({
                                type: 'trackingid',
                                focusIndex: 16
                              })
                            }}
                            onSubmitEditing={() => {
                              let inputComId = this.state.handlingToolId
                              console.log("🚀 ~ file: qualityinspection.js ~ line 468 ~ QualityInspection ~ inputComId", inputComId)
                              inputComId = inputComId.replace(/\b(0+)/gi, "")
                              this.GetManyComponentbyHandlingtool(inputComId)
                            }}
                          />
                        </View>
                        <ScanButton
                          onPress={() => {
                            if (planId == '') {
                              this.toast.show('请先选择发货单号')
                              return
                            }
                            this.setState({
                              isGetcomID: false,
                              focusIndex: 16
                            })
                            this.props.navigation.navigate('QRCode', {
                              type: 'trackingid',
                              page: 'PDALoading'
                            })
                          }}
                          onLongPress={() => {
                            this.setState({
                              type: 'trackingid',
                              focusIndex: 16
                            })
                            NativeModules.PDAScan.onScan();
                          }}
                          onPressOut={() => {
                            NativeModules.PDAScan.offScan();
                          }}
                        />
                      </View>
                    }
                  /> : <View></View>
              }

              <ListItemScan
                isButton={true}
                focusStyle={focusIndex == '15' ? styles.focusColor : {}}
                title='装车明细'
                onPressIn={() => {
                  this.setState({
                    type: 'compid',
                    focusIndex: 15
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPress={() => {
                  this.setState({
                    type: 'compid',
                    focusIndex: 15
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}
                rightElement={
                  <View style={{ flexDirection: 'row' }}>
                    <View>
                      <Input
                        ref={ref => { this.input11 = ref }}
                        containerStyle={styles.scan_input_container}
                        inputContainerStyle={styles.scan_inputContainerStyle}
                        inputStyle={[styles.scan_input]}
                        placeholder='请输入'
                        keyboardType='numeric'
                        value={compId}
                        onChangeText={(value) => {
                          value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
                          value = value.replace(/^\./g, ""); //验证第一个字符是数字
                          value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
                          //value = value.replace(/\b(0+)/gi, ""); //清楚开头的0
                          this.setState({
                            compId: value
                          })
                        }}
                        onFocus={() => {
                          this.setState({
                            focusIndex: 15,
                            type: 'compid',
                          })
                        }}
                        onSubmitEditing={() => {
                          let inputComId = this.state.compId
                          console.log("🚀 ~ file: qualityinspection.js ~ line 468 ~ QualityInspection ~ inputComId", inputComId)
                          inputComId = inputComId.replace(/\b(0+)/gi, "")
                          this.GetcomID(inputComId)
                        }}
                      />
                    </View>
                    <ScanButton
                      onPress={() => {
                        if (planId == '') {
                          this.toast.show('请先选择发货单号')
                          return
                        }
                        this.setState({
                          isGetcomID: false,
                          focusIndex: 15
                        })
                        this.props.navigation.navigate('QRCode', {
                          type: 'compid',
                          page: 'PDALoading'
                        })
                      }}
                      onLongPress={() => {
                        this.setState({
                          type: 'compid',
                          focusIndex: 15
                        })
                        NativeModules.PDAScan.onScan();
                      }}
                      onPressOut={() => {
                        NativeModules.PDAScan.offScan();
                      }}
                    />
                  </View>
                }
              />
              {
                compDataArr.length != 0 ? this.CardList(compDataArr) : <View></View>
              }

            </View>

            {
              pageType == 'CheckPage' ?
                <FormButton
                  backgroundColor='#EB5D20'
                  title='删除'
                  onLongPress={() => {
                    Alert.alert(
                      '提示信息',
                      '是否删除',
                      [{
                        text: '取消',
                        style: 'cancel'
                      }, {
                        text: '删除',
                        onPress: () => {
                          this.DeleteData()
                        }
                      }])
                  }}
                /> :
                <View>
                  {
                    this.state.isShowDriverInfo == true ? <View style={{ flexDirection: 'row' }}>

                      <Button
                        disabled={this.state.buttonDisabled}
                        style={{ marginVertical: 10, flex: 1 }}
                        containerStyle={{ marginVertical: 10, marginLeft: 22, width: width * 0.4 }}
                        buttonStyle={{ backgroundColor: '#EB5D20', borderRadius: 10, paddingVertical: 16 }}
                        title='暂存'
                        onPress={() => {
                          this.TmpPostData()
                        }}
                      />
                      <Button
                        disabled={this.state.buttonDisabled}
                        style={{ marginVertical: 10 }}
                        containerStyle={{ marginVertical: 10, marginLeft: 32, width: width * 0.4 }}
                        buttonStyle={{ backgroundColor: '#17BC29', borderRadius: 10, paddingVertical: 16 }}
                        title='出库'
                        onPress={() => {
                          this.PostData()
                        }}
                      />
                    </View> :
                      <FormButton
                        backgroundColor='#17BC29'
                        title='保存'
                        onPress={() => {
                          this.PostData()
                        }}
                      />
                  }
                </View>


            }

            {this.BottomList()}

            {this.BottomListDriver()}

            {this.dropDownBottomView()}

          </ScrollView >

          <Overlay
            fullScreen={true}
            animationType='fade'
            isVisible={this.state.isDeliveryGuideVisible}
            onRequestClose={() => {
              this.setState({ isDeliveryGuideVisible: !this.state.isDeliveryGuideVisible });
            }}>
            <Header
              centerComponent={{ text: '发货引导', style: { color: 'gray', fontSize: 20 } }}
              rightComponent={<BackIcon
                onPress={() => {
                  this.setState({
                    isDeliveryGuideVisible: !this.state.isDeliveryGuideVisible
                  });
                }} />}
              backgroundColor='white'
            />
            <ScrollView>
              {
                this.state.deliveryguide.map((item, index) => {
                  return (
                    <Card containerStyle={{ borderRadius: 10 }}>
                      <TouchableOpacity >
                        <View style={{ flexDirection: 'row', }}>
                          <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 1, }}>
                            <View style={{ flexDirection: 'row' }}>
                              <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.compCode}</Text>
                              <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.diffSourceType}</Text>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                              <Text style={[styles.text]} numberOfLines={1}> {item.designType}</Text>
                              <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.roomName}</Text>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                              <Text style={[styles.text, styles.textthi]} numberOfLines={1}> {item.compTypeName}</Text>
                              <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.libraryName}</Text>
                            </View>
                          </View>
                        </View>
                      </TouchableOpacity>
                    </Card>
                  )
                })
              }
            </ScrollView>

          </Overlay>

        </View >
      )
    }
  }

  flatListSet = () => {
    const { bottomData, driversSelectArr } = this.state
    if (this.state.driverLibSelect == 'driverHis') {
      return (
        <FlatList
          style={{ height: 345 }}
          ref={(flatList) => this._flatList1 = flatList}
          keyExtractor={(item) => item.index}
          renderItem={this._renderItemHis.bind(this)}
          refreshing={false}
          numColumns={1}
          data={bottomData}
          extraData={this.state}
        />
      )

    } else {
      return (
        <FlatList
          style={{ height: 345 }}
          ref={(flatList) => this._flatList2 = flatList}
          renderItem={this._renderItemLib.bind(this)}
          keyboardShouldPersistTaps='always'
          refreshing={false}
          numColumns={1}
          data={driversSelectArr}
        />
      )

    }
  }

  _renderItemLib({ item, index }) {
    let dataItem = {}
    dataItem.Title = item.driver
    dataItem.rightTitle = item.driverTelephone
    dataItem.subTitle = item.carnum
    dataItem.subRightTitle = item.modelofCar
    return (
      <TouchableOpacity onPress={() => {
        this.setState({
          driver: item.driver,
          driverselect: item.driver,
          driverTelephone: item.driverTelephone,
          driverTelephoneSelect: item.driverTelephone,
          carnum: item.carnum,
          carnumselect: item.carnum,
          companyname: item.companyname,
          truckModel: item.modelofCar,
          truckModelselect: item.modelofCar,
          companyid: item.companyid,
          companyselct: item.companyname,
          isDropDownModalOpen: false,
          driverSerchText: '',
        }, () => {
          const { loadOutTime, planId, planselect, formCode_jh, projectName, Bracket, formCode, carnum, remark, sweepnum, rowguid, truckModel, companyid, driver, arrivalTime, WoodBeam, companyname, loadCarMethod, driverTelephone } = this.state

          loadingData = {
            "loadOutTime": loadOutTime,
            "Bracket": Bracket,
            "formCode": formCode,
            "carNum": carnum,
            "remark": remark,
            "sweepnum": sweepnum,
            "compDataArr": compDataArr,
            "compIdArr": compIdArr,
            "truckModel": truckModel,
            "companyid": companyid,
            "companyname": companyname,
            "driver": driver,
            "driverTelephone": driverTelephone,
            "arrivalTime": arrivalTime,
            "WoodBeam": WoodBeam,
            "planId": planId,
            "planselect": planselect,
            "projectName": projectName,
            "LoadCarMethod": loadCarMethod,
          }
          console.log("🚀 ~ file: loading.js:3170 ~ SteelCage ~ _renderItemLib ~ loadingData:", loadingData)

          DeviceStorage.save('loadingData', JSON.stringify(loadingData))
        })

      }}>
        <BatchListItem backgroundColor='white' color="#333" dataItem={dataItem} />
      </TouchableOpacity>
    )
  }

  _renderItemHis({ item, index }) {
    let title = ''
    if (item.driver) {
      title = item.driver
    } else if (item.carnum) {
      title = item.carnum
    } else if (item.truckModel) {
      title = item.truckModel
    } else if (item.driverTelephone) {
      title = item.driverTelephone
    }
    return (
      <TouchableOpacity key={'histouch_' + index} onPress={() => {
        if (item.driver) {
          this.setState({
            driver: item.driver,
            driverselect: item.driver,
          })
        } else if (item.carnum) {
          this.setState({
            carnum: item.carnum,
            carnumselect: item.carnum,
          })
        } else if (item.truckModel) {
          this.setState({
            truckModel: item.truckModel,
            truckModelselect: item.truckModel,
          })
        } else if (item.driverTelephone) {
          this.setState({
            driverTelephone: item.driverTelephone,
            driverTelephoneSelect: item.driverTelephone,
          })
        }
        this.setState({
          isDropDownModalOpen: false,
          driverSerchText: '',
          bottomData: []
        })
        const { loadOutTime, planId, planselect, formCode_jh, projectName, Bracket, formCode, carnum, remark, sweepnum, rowguid, truckModel, companyid, driver, arrivalTime, WoodBeam, companyname, loadCarMethod, driverTelephone } = this.state

        loadingData = {
          "loadOutTime": loadOutTime,
          "Bracket": Bracket,
          "formCode": formCode,
          "carNum": carnum,
          "remark": remark,
          "sweepnum": sweepnum,
          "compDataArr": compDataArr,
          "compIdArr": compIdArr,
          "truckModel": truckModel,
          "companyid": companyid,
          "companyname": companyname,
          "driver": driver,
          "driverTelephone": driverTelephone,
          "arrivalTime": arrivalTime,
          "WoodBeam": WoodBeam,
          "planId": planId,
          "planselect": planselect,
          "projectName": projectName,
          "LoadCarMethod": loadCarMethod,
        }

        DeviceStorage.save('loadingData', JSON.stringify(loadingData))
      }}>
        <BottomItem key={'hisBottom_' + index} backgroundColor='white' color="#333" title={title} />
      </TouchableOpacity>
    )
  }

}

class InputView extends React.Component {
  render() {
    const { ref, value, onChangeText, onFocus, onSubmitEditing, onPress } = this.props
    return (
      <View style={styles.loading_input_view}>
        <Input
          {...this.props}
          containerStyle={styles.input_container}
          inputContainerStyle={styles.inputContainerStyle}
          inputStyle={styles.input_}
          placeholder='请输入或选择'
          value={value}
          onChangeText={onChangeText}
          onFocus={onFocus}
          onSubmitEditing={onSubmitEditing}
          underlineColorAndroid='transparent'
        ></Input>
        <Icon onPress={onPress}
          name='caretdown'
          type='antdesign'
          color='#419fff'
          iconStyle={{ fontSize: 13, marginLeft: 8, marginRight: -1 }}
        />

      </View>
    )
  }
}

class PlanCardList extends React.Component {
  render() {
    const { PlanData } = this.props
    return (
      <View>
        <Card containerStyle={{ borderRadius: 10, shadowOpacity: 0 }}>
          <ListItem
            containerStyle={styles.list_container_style}
            title='项目名称'
            rightTitle={PlanData.result.projectName}
            titleStyle={styles.title}
            rightTitleProps={{ numberOfLines: 1 }}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='承运单位'
            rightTitle={PlanData.result.carrier}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='客户名称'
            rightTitle={PlanData.result.customer}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='组合车次'
            rightTitle={PlanData.result.cartrains}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='发货引导'
            rightElement={<View>
              <Text onPress={() => { this.setState({}) }} style={[styles.rightTitle, { color: '#419FFF', fontSize: 13, textAlign: 'right' }]} numberOfLines={1}>
                查看
              </Text>
            </View>}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
        </Card>
      </View>
    )
  }
}

class BackIcon extends React.PureComponent {
  render() {
    return (
      <TouchableOpacity
        onPress={this.props.onPress} >
        <Icon
          name='arrow-back'
          color='#419FFF' />
      </TouchableOpacity>
    )
  }
}

/* const styles = StyleSheet.create({
  title: {
    fontSize: 13,
  },
  rightTitle: {
    fontSize: 13,
    textAlign: 'right',
    lineHeight: 14,
    flexWrap: 'wrap',
    width: width / 2
  },
  list_container_style: { height: 14 },
  input_container: {
    width: width / 4,
    height: 14,
    marginTop: -8,
  },
  input_: {
    fontSize: 14,
    textAlign: 'right'
  },
  inputContainerStyle: { backgroundColor: 'transparent', borderColor: 'transparent' },
  text_beforeinput: { width: width / 5, textAlign: 'center', fontSize: RFT * 3 }
}) */