import React from 'react';
import {
    Dimensions,
    FlatList,
    SectionList,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    SafeAreaView
} from 'react-native';
import { deviceWidth } from '../../Url/Pixal';
import { SearchBar, Avatar, Icon } from 'react-native-elements';
import Url from '../../Url/Url';



class BackIcon extends React.PureComponent {
    render() {
        return (
            <TouchableCustom
                onPress={this.props.onPress} >
                <Icon
                    name='arrow-back'
                    color='#419FFF' />
            </TouchableCustom>
        )
    }
}

const { height, width } = Dimensions.get('window') //获取宽高
let list = []; //接口请求存放的数据
let sectionsOld = [] 
let letterArrOld = []

//const selectedFieldName = 'id';

//搜索栏搜索图标
const defaultSearchIcon = () => ({
    //type: 'antdesign',
    size: 24,
    name: 'search',
    color: '#999',
});

//搜索栏清理图标
const defaultClearIcon = () => ({
    //type: '',
    size: 20,
    name: 'close',
    color: '#999',
});

export default class SelectSupplier extends React.PureComponent {
    //isLoading = false;
    constructor(props) {
        super(props);

        this.state = {
            searchValue: null, //搜索框里的值

            dataList: list,
            sections: [],       //section数组
            letterArr: [],      //首字母数组
            //letterArr: [],      //首字母数组
            activeLetterIndex: 0,
            selectedItemSet: new Set(),

            // 是否开启批量选择模式
            batchSelected: false,
            refreshCount: 0,

        };
    }

    componentDidMount() {
        const { navigation } = this.props;
        let guid = navigation.getParam('guid') || ''
        let pageType = navigation.getParam('pageType') || ''
        let title = navigation.getParam('title') || ''
        let resData = navigation.getParam('resData') || ''
        if (pageType == 'CheckPage') {

        } else {
            if (title == '选择项目名称') {
                sectionsOld = resData
                sectionsOld.forEach((item, index) => {
                    letterArrOld.push(item.title)
                })
                this.setState({
                    sections: resData,
                    letterArr: letterArrOld
                }, () => { this.forceUpdate() })
            } else {
                this.resData()
            }
            
            this.setState({
                title: title
            })
        }
    }

    componentWillUnmount() {
        sectionsOld = []
        letterArrOld = []
        this.setState({

        })
    }

    resData = () => {
        let formData = new FormData();
        let data = {};
        data = {
            "action": "instoragegetsupplier",
            "servicetype": "pdamaterial",
            "express": "6DFD1C9B",
            "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
            "data": { "factoryId": Url.PDAFid }
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: SelectSupplier.js ~ line 123 ~ SelectSupplier ~ formData:", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res.statusText);
            //console.log(res);
            return res.json();
        }).then(resData => {
            console.log("🚀 ~ file: SelectSupplier.js ~ line 135 ~ SelectSupplier ~ resData:", resData.result)
            if (resData.status == '100') {
                sectionsOld = resData.result
                sectionsOld.forEach((item, index) => {
                    letterArrOld.push(item.title)
                })
                this.setState({
                    sections: resData.result,
                    letterArr: letterArrOld
                }, () => { this.forceUpdate() })
            } else {
                Alert.alert("错误提示", resData.message, [{
                    text: '取消',
                    style: 'cancel'
                }, {
                    text: '返回上一级',
                    onPress: () => {
                        this.props.navigation.goBack()
                    }
                }])
            }
        }).catch((error) => {
            console.log("🚀 ~ file: SelectSupplier.js ~ line 233 ~ SelectSupplier ~ error:", error)
        });

    }

    //顶栏
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('title')
        }
    }

    // 分组列表的头部
    _renderSectionHeader(sectionItem) {
        const { section } = sectionItem;
        return (
            <View style={{
                height: 40,
                backgroundColor: '#e7f0f9',
                paddingHorizontal: 10,
                flexDirection: 'row',
                alignItems: 'center',
            }}>
                <Text style={{ fontSize: 16 }}>{section.title.toUpperCase()}</Text>
            </View>
        );
    }

    // 联系人列表处理
    _renderItem(item, index) {
        let title = '供'
        if (this.state.title == '选择项目名称') title = '项'
        
        return (
            <TouchableOpacity
                style={{
                    //paddingLeft: pxToDp(20),
                    //paddingRight: pxToDp(30),
                    height: 80,
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                    borderBottomWidth: 1,
                    backgroundColor: "#FFFFFF",
                    borderBottomColor: '#efefef',
                }}
                activeOpacity={.75}
                
                onPress={() => {
                    if (this.state.title == '选择供应商') {
                        this.props.navigation.navigate('MaterialInStorage', {
                            state: "selectSupplier",
                            supplier: item.name,
                            supplierId: item.id
                        })
                    } else if (this.state.title == '选择项目名称') {
                        this.props.navigation.navigate('MaterialProjectPicking', {
                            state: "selectSupplier",
                            projectName: item.name,
                            projectId: item.id
                        })
                    }
                    
                }}
            >
                <Avatar
                    size={56}
                    rounded
                    containerStyle={[{ marginLeft: 20, marginRight: 10 }]}
                    overlayContainerStyle={{ backgroundColor: '#4D8EF5', }}
                    title={title}
                    titleStyle={[{ fontSize: 24 }, { color: 'white' }]}
                    activeOpacity={0.7} />

                <View style={{
                    marginLeft: 20,
                }}>
                    <View>
                        <View style={{ flexDirection: "row", alignItems: "center" }}>
                            <Text style={{ fontSize: 18 }}>{item.name} </Text>
                        </View>
                        <Text style={{ /*fontSize: pxToDp(8),*/ color: "#555" }}>{item.code}        {item.contactsnumber}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }

    // 字母关联分组跳转
    _onSectionselect = (key) => {
        this.setState({
            activeLetterIndex: key,
        }, () => {

        });
        this.refs._sectionList.scrollToLocation({
            itemIndex: 0,
            sectionIndex: key,
            viewOffset: 20,
        });

    };

    setSearchValue = (searchValue, callback) => {
        this.setState({
            searchValue: searchValue,
        }, () => {
            callback && callback();
        });
    };

    search = () => {
        const { searchValue } = this.state;
        this.transferToSectionsData(searchValue);
    };

    /**
    * 转化数据列表
    */
    transferToSectionsData = (searchValue) => {
        //获取联系人列表
        let sections = [], letterArr = [];
        if (searchValue == "") {
            sections = sectionsOld
            letterArr = letterArrOld
        } else {
            sectionsOld.forEach((item, index) => {
                item.data.forEach((item2, index2) => {
                    if (item2.name.includes(searchValue) || item2.code.includes(searchValue)) {
                        let check = false
                        sections.forEach((item3, index3) => {
                            if (item3.title == item.title) {
                                check = true
                                let obj2 = { "code": "", "name": "", "id": "", "contactsnumber": "" }
                                obj2.code = item2.code
                                obj2.name = item2.name
                                obj2.id = item2.id
                                obj2.contactsnumber = item2.contactsnumber
                                item3.data.push(obj2)

                                if (!letterArr.includes(item.title)) letterArr.push(item.title)
                                return
                            }
                        })
                        if (!check) {
                            let obj = { "title": "", "data": [] }
                            obj.title = item.title
                            let arr = []
                            let obj2 = { "code": "", "name": "", "id": "", "contactsnumber": "" }
                            obj2.code = item2.code
                            obj2.name = item2.name
                            obj2.id = item2.id
                            obj2.contactsnumber = item2.contactsnumber
                            arr.push(obj2)
                            obj.data = arr
                            sections.push(obj)
                            if (!letterArr.includes(item.title)) letterArr.push(item.title)
                        }
                    }
                })
            })
        }

        this.setState({ letterArr: letterArr });
        this.setState({ sections: sections });
    };

    render = () => {
        const { letterArr, sections, activeLetterIndex, batchSelected } = this.state;
        //偏移量 = （设备高度 - 字母索引高度 - 底部导航栏 - 顶部标题栏 - 24）/ 2
        let top_offset = (Dimensions.get('window').height - letterArr.length * 16 - 10 - 10 - 24) / 2;
        //if (isAndroid) {
        //    top_offset = top_offset + StatusBar.currentHeight + 45;
        //}
        return (
            <SafeAreaView style={{
                flex: 1,
            }}>
                {
                    this.renderSearchBar()
                }

                <SectionList
                    ref="_sectionList"
                    renderItem={({ item, index }) => this._renderItem(item, index)}
                    renderSectionHeader={this._renderSectionHeader.bind(this)}
                    sections={sections}
                    keyExtractor={(item, index) => item + index}
                    ItemSeparatorComponent={() => <View />}
                    ListFooterComponent={<View style={{ height: 50 }} />}
                    initialNumToRender={1000}
                />

                {/*右侧字母栏*/}
                <View style={{ position: 'absolute', width: 26, right: 0, top: top_offset }}>
                    <FlatList
                        data={letterArr}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item, index }) => {
                            let isActive = index === activeLetterIndex;
                            // let textStyle = isActive ? styles.activeIndicatorText : styles.inactiveIndicatorText;
                            // let containerStyle = isActive ? styles.activeIndicatorContainer : styles.inactiveIndicatorContainer;
                            let textStyle = stylesMailList.inactiveIndicatorText;
                            let containerStyle = stylesMailList.inactiveIndicatorContainer;
                            return (
                                <TouchableOpacity
                                    style={[
                                        {
                                            marginVertical: 2,
                                            height: 14,
                                            width: 16,
                                            borderRadius: 8,
                                            flexDirection: 'row',
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                        },
                                        containerStyle,
                                    ]}
                                    onPress={() => {
                                        this._onSectionselect(index);
                                    }}
                                >
                                    <Text style={[{
                                        fontSize: 12,
                                    }, textStyle]}>
                                        {item.toUpperCase()}
                                    </Text>
                                </TouchableOpacity>
                            );
                        }}
                    />
                </View>
            </SafeAreaView>
        );
    };


    renderSearchBar = () => {
        const { searchValue } = this.state;
        return (
            <View style={{
                flexDirection: 'row',
                alignItems: 'center',
                paddingTop: 10,
                paddingBottom: 10,
                backgroundColor: '#fff',
                borderBottomWidth: 1,
                borderBottomColor: '#efefef',
            }}>
                <View style={{ flex: 1 }}>
                    <SearchBar
                        platform='android'
                        placeholder={'搜索供应商及产品...'}
                        placeholderTextColor='#999'
                        // style={{ backgroundColor: 'red', borderColor: 'red', borderWidth: 10 }}
                        //containerStyle={{ flex: 1 }}
                        containerStyle={stylesMailList.searchContainerStyle}
                        inputContainerStyle={{ backgroundColor: '#f9f9f9', width: deviceWidth * 0.94, marginLeft: deviceWidth * 0.03, borderRadius: 45 }}
                        inputStyle={{ color: '#333' }}
                        searchIcon={defaultSearchIcon()}

                        /*searchIcon={
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ fontSize: 18, marginTop: 2 }}>  材料编码</Text>
                                <Icon name='caretdown' color='#419fff' iconStyle={{ fontSize: 13, marginTop: 7, marginLeft: 8, marginRight: -1 }} type='antdesign' ></Icon>
                                <Icon type='material-community' name='power-on' color='#999' style={{ marginTop: 2}} />
                                <Icon name='search' color='#999' type='material' iconStyle={{ fontSize: 22, marginTop: 4}}></Icon>

                            </View>
                        }*/

                        
                        //rightIconContainerStyle={{ backfaceVisibility: 'visible', backgroundColor: 'red' }}
                        
                        clearIcon={defaultClearIcon()}
                        /*clearIcon={
                            <View style={{ flexDirection: 'row' }}>
                                <Icon type='material-community' name='qrcode-scan' color='#999' />
                                <Icon type='material-community' name='microphone' color='#999' />
                            </View>
                        }*/
                     
                        cancelIcon={defaultSearchIcon()}
                        round={true}
                        value={searchValue}
                        onChangeText={(text) => {
                            this.setSearchValue(text, () => {
                                this.search();
                            });
                        }}
                        input={(input) => { this.setState({ searchValue: input }) }} />
                    
                </View>
            </View>
        );
    };
}

const stylesMailList = StyleSheet.create({
    taskNodeTitleText: {
        color: '#333333',
        fontWeight: 'bold',
        fontSize: 16,
    },
    inactiveIndicatorContainer: {},
    activeIndicatorContainer: {
        backgroundColor: '#2988FF',
    },
    inactiveIndicatorText: {
        color: '#666666',
    },
    activeIndicatorText: {
        color: '#fff',
    },
    searchContainerStyle:
    {
        shadowColor: '#999', shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.3,
        shadowRadius: 6
    },
});

