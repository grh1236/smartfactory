import React from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, ListItem, BottomSheet, Overlay, Header, CheckBox } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { ToDay, YesterDay, BeforeYesterDay } from '../../CommonComponents/Data';
import { RFT } from '../../Url/Pixal';
import { ScrollView } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker'
import { NavigationEvents } from 'react-navigation';
import { styles } from '../Componment/PDAStyles';
import ScanButton from '../Componment/ScanButton';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import ListItemScan_child from '../Componment/ListItemScan_child';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import CheckBoxScan from '../Componment/CheckBoxScan';
import AvatarAdd from '../Componment/AvatarAdd';
import AvatarImg from '../Componment/AvatarImg';

import { DeviceEventEmitter, NativeModules } from 'react-native';
import overlayImg from '../Componment/OverlayImg';
import OverlayImgZoomPicker from '../Componment/OverlayImgZoomPicker';
class BackIcon extends React.PureComponent {
  render() {
    return (
      <TouchableOpacity
        onPress={this.props.onPress} >
        <Icon
          name='arrow-back'
          color='#419FFF' />
      </TouchableOpacity>
    )
  }
}

const { height, width } = Dimensions.get('window') //获取宽高

let QRkeeperName = ''
let QRStock = ''
let QRkeeperID = ''

let imageArr = [];

let compDataArr = [], compIdArr = [], QRid2 = '', tmpstr = '',
  questionChickArr = [], questionChickIdArr = [],
  isCheckedList = [], isCheckedIdList = [], isCheckedNameList = [], //二级菜单是否被全选
  visibleArr = []

export default class Inveck extends React.Component {
  constructor() {
    super();
    this.state = {
      monitorSetup: '',
      isroom: false,
      isteam: false,
      iskeeper: false,
      type: 'compid',
      bottomData: [],
      bottomVisible: false,
      inventoryState: 1,
      resData: [],
      formCode: '',
      rowguid: '',
      steelCageIDs: '',
      ServerTime: '',
      QRid: '',
      news: '',
      room: [],
      roomId: '',
      roomselect: '请选择',
      roomName: '',
      library: [],
      libraryId: '',
      libraryselect: '请选择',
      libraryName: '',
      compDataArr: [],
      compData: '',
      compId: '',
      compIdArr: [],
      compCode: '',
      isGetRefundComponentInfo: false,
      isGetStoreroom: false,
      //CardList
      hidden: -1,
      isVisible: false,
      //问题缺陷
      lstRoom: [],
      isCheckedList: [],
      isCheckedIdList: [],
      isCheckedNameList: [],
      questionChickArr: [],
      questionChickIdArr: [],
      isQuestionOverlay: false,
      reasons: [
        { "reason": "损坏/质量问题" },
        { "reason": "损坏" },
      ],
      reasonselect: '请选择',
      reason: '',
      projectId: '',
      projectName: '',
      //照片
      imageArr: [],
      pictureSelect: false,
      imageOverSize: false,
      pictureUri: '',
      //承运单位
      SupplierInfo: [],
      supId: '',
      supName: '',
      supSelect: '请选择',
      remark: '',
      carnum: '',
      focusIndex: '',
      isCheckPage: false,
      isLoading: true
    }
    //this.requestCameraPermission = this.requestCameraPermission.bind(this)
  }

  componentDidMount() {
    const { navigation } = this.props;
    let guid = navigation.getParam('guid') || ''
    let pageType = navigation.getParam('pageType') || ''
    if (pageType == 'CheckPage') {
      this.checkResData(guid)
      this.setState({ isCheckPage: true })
    } else {
      this.resData()
    }

    //通过使用DeviceEventEmitter模块来监听事件
    this.iDataScan = DeviceEventEmitter.addListener('iDataScan', (Event) => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 115 ~ SteelCage ~ Event.ScanResult", Event.ScanResult)
      if (typeof Event.ScanResult != undefined) {
        let data = Event.ScanResult
        let arr = data.split("=");
        let id = ''
        id = arr[arr.length - 1]
        console.log("🚀 ~ file: concrete_pouring.js ~ line 118 ~ SteelCage ~ id", id)
        if (this.state.type == 'libraryid') {
          this.GetStoreroom(id)
        } else if (this.state.type == 'Stockid') {
          this.GetRefundComponentInfo(id)
        }
      }
    });
  }

  UpdateControl = () => {
    if (typeof this.props.navigation.getParam('QRid') != "undefined") {
      if (this.props.navigation.getParam('QRid') != "") {
        this.toast.show(Url.isLoadingView, 0)
      }
    }
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || ''
    const type = navigation.getParam('type') || '';

    if (type == 'libraryid') {
      this.GetStoreroom(QRid)
    } else if (type == 'Stockid') {
      this.GetRefundComponentInfo(QRid)
    }

  }

  componentWillUnmount() {
    compDataArr = [], compIdArr = [], QRid2 = '', tmpstr = '',
      isCheckedList = [], //二级菜单是否被全选
      visibleArr = []
    imageArr = []
    this.iDataScan.remove()
  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  checkResData = (guid) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getstockinventorydetail",
      "servicetype": "pdaservice",
      "express": "B799EE62",
      "ciphertext": "ea2b216d46185434742dea3bca2974cf",
      "data": {
        "rowGuid": guid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      let formCode = resData.result.main.formCode
      let ServerTime = resData.result.main.editDate
      let rowguid = resData.result.main._Rowguid
      let roomId = resData.result.main.roomId
      let roomName = resData.result.main.roomName
      let remark = resData.result.main.inventoryDescription
      let inventoryState = resData.result.main.inventoryState
      let lstRoom = []
      let roomNameArray = [];
      roomNameArray = roomName.split(",");
      let roomIdArray = [];
      roomIdArray = roomId.split(",");
      roomNameArray.map((item, index) => {
        let tmp = {}
        tmp.roomId = roomIdArray[index]
        tmp.roomName = item
        lstRoom.push(tmp)
      })
      this.setState({
        resData: resData,
        formCode: formCode,
        ServerTime: ServerTime,
        rowguid: rowguid,
        lstRoom: lstRoom,
        remark: remark,
        inventoryState: inventoryState,
        isLoading: false
      })
    }).catch((error) => {
    });

  }

  resData = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "initstockinventory",
      "servicetype": "pdaservice",
      "express": "B34B2B21",
      "ciphertext": "219527459fa29fa23c7d7ef001165d2b",
      "data": { "bizid": Url.PDAFid }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      //初始化错误提示
      if (resData.status == 100) {
        let formCode = resData.result.formCode
        let ServerTime = resData.result.editDate
        let rowguid = resData.result.rowGuid
        let lstRoom = resData.result.lstRoom
        this.setState({
          resData: resData,
          formCode: formCode,
          ServerTime: ServerTime,
          rowguid: rowguid,
          lstRoom: lstRoom,
          isLoading: false
        })
      }
      else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }

    }).catch((error) => {
    });
  }

  GetStoreroom = (id) => {
    let compCode = '111'
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getStoreroom",
      "servicetype": "pda",
      "express": "162C33CC",
      "ciphertext": "3b81e5182d128db9333f0fec4d3703a5",
      "data": {
        "LibraryId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
       this.toast.close()
      if (resData.status == '100') {
        let libraryName = resData.result.libraryName
        let libraryId = resData.result.libraryId
        let roomName = resData.result.roomName
        QRid2 = id
        this.setState({
          libraryName: libraryName,
          libraryId: libraryId,
          roomName: roomName,
          libraryselect: libraryName,
          roomselect: roomName,
          isGetStoreroom: true
        })
      } else {
        Alert.alert('ERROR', resData.message)
      }

    }).catch(err => {
    })
  }

  GetRefundComponentInfo = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "GetRefundComponentInfo",
      "servicetype": "pda",
      "express": "227EB695",
      "ciphertext": "de110251c4c1faf0038a3416d728a830",
      "data": {
        "compId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
       this.toast.close()
      if (resData.status == '100') {
        let compData = resData.result
        let compId = compData.compId
        let compCode = compData.compCode
        let projectId = compData.projectId
        let projectName = compData.projectName
        if (compDataArr.length == 0) {
          compDataArr.push(compData)
          compIdArr.push(compId)
          this.setState({
            compDataArr: compDataArr,
            compIdArr: compIdArr,
            compData: compData,
            compId: "",
            compCode: compCode,
            projectName: projectName,
            projectId: projectId,
            isGetRefundComponentInfo: true
          })
          tmpstr = tmpstr + compId + ','
        } else {
          if (tmpstr.indexOf(compId) == -1) {
            if (this.state.projectId != projectId) {
              this.toast.show("项目名称与退货项目不符合")
            } else {
              compIdArr.push(compId)
              compDataArr.push(compData)
              this.setState({
                compDataArr: compDataArr,
                compIdArr: compIdArr,
                compData: compData,
                compId: "",
                compCode: compCode,
                isGetRefundComponentInfo: true
              })
              tmpstr = tmpstr + compId + ','
            }
          } else {
            this.toast.show('已经扫瞄过此构件')
          }

        }

      } else {
        Alert.alert('错误', resData.message)
        this.setState({
          compId: ""
        })
        //this.input1.focus()
      }

    }).catch(err => {
    })
  }

  comIDItem = () => {
    const { isGetcomID, buttondisable, compCode, compId } = this.state
    return (
      <View>
        {
          !isGetcomID ?
            <View>
              <View>
                <Input
                  ref={ref => { this.input1 = ref }}
                  containerStyle={styles.scan_input_container}
                  inputContainerStyle={styles.scan_inputContainerStyle}
                  inputStyle={[styles.scan_input]}
                  placeholder='请输入'
                  keyboardType='numeric'
                  value={compId}
                  onChangeText={(value) => {
                    value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
                    value = value.replace(/^\./g, ""); //验证第一个字符是数字
                    value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
                    //value = value.replace(/\b(0+)/gi, ""); //清楚开头的0
                    this.setState({
                      compId: value
                    })
                  }}
                  //onSubmitEditing={() => { this.input1.focus() }}
                  //onFocus={() => { this.setState({ focusIndex: '5' }) }}
                  /*  */
                  //returnKeyType = 'previous'
                  onSubmitEditing={() => {
                    let inputComId = this.state.compId
                    inputComId = inputComId.replace(/\b(0+)/gi, "")
                    this.GetcomID(inputComId)
                  }}
                />
              </View>
              <ScanButton
                 
                onPress={() => {
                  this.setState({
                    iscomIDdelete: false,
                    buttondisable: true
                  })
                  setTimeout(() => {
                    this.setState({ GetError: false, buttondisable: false })
                    varGetError = false
                  }, 1500)
                  this.props.navigation.navigate('QRCode', {
                    type: 'compid',
                    page: 'ReturnGoods'
                  })
                }}
              />
            </View>
            :
            <TouchableOpacity
              onPress={() => {
                console.log('onPress')
              }}
              onLongPress={() => {
                console.log('onLongPress')
                Alert.alert(
                  '提示信息',
                  '是否删除构件信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      this.setState({
                        compData: [],
                        compCode: '',
                        compId: '',
                        iscomIDdelete: true,
                        isGetcomID: false,
                      })
                    }
                  }])
              }} >
              <Text>{compCode}</Text>
            </TouchableOpacity>
        }
      </View>

    )
  }

  PostData = () => {
    const { formCode, projectId, projectName, ServerTime, reason, remark, carnum, supId, supName, rowguid, compId, compIdArr, libraryId, libraryName, roomName, questionChickArr } = this.state
    const { navigation } = this.props;
    const QRid = navigation.getParam('QRid') || '';

    let isCheckedNameStr = isCheckedNameList.toString()
    let isCheckedIdStr = isCheckedIdList.toString()

    if (isCheckedList.length == 0) {
      this.toast.show('库房不能为空')
      return
    }
    if (remark == '') {
      this.toast.show('盘点说明不能为空')
      return
    }
    this.toast.show(saveLoading, 0)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "savestockinventory",
      "servicetype": "pdaservice",
      "express": "CA685702",
      "ciphertext": "6531b65ea8d5b50001005d5be9e11b2f",
      "data": {
        "bizid": Url.PDAFid,
        "description": remark,
        "editDate": ServerTime,
        "formCode": formCode,
        "roomId": isCheckedIdStr,
        "roomName": isCheckedNameStr,
        "rowguid": rowguid,
        "username": Url.PDAusername
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('保存成功');
        setTimeout(() => {
          this.props.navigation.replace(this.props.navigation.getParam("page"), {
            title: this.props.navigation.getParam("title"),
            pageType: this.props.navigation.getParam("pageType"),
            page: this.props.navigation.getParam("page"),
            isAppPhotoSetting: this.props.navigation.getParam("isAppPhotoSetting"),
            isAppDateSetting: this.props.navigation.getParam("isAppDateSetting"),
            isAppPhotoAlbumSetting: this.props.navigation.getParam("isAppPhotoAlbumSetting"),
          })
          //this.props.navigation.navigate('PDAMainStack')
        }, 400)
      } else {
        this.toast.show('保存失败')
        Alert.alert('保存失败', resData.message)
      }
    }).catch((error) => {
      this.toast.show('保存失败')
      if (error.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('保存失败', "响应内容不是合法JSON格式")
        return
      }
      if (error.toString().indexOf('Network request faile') != -1) {
        Alert.alert('保存失败', "网络请求错误")
        return
      }
      Alert.alert('保存失败', error.toString())
    });
  }

  ReviseData = () => {
    const { formCode, projectId, projectName, ServerTime, reason, remark, carnum, supId, supName, rowguid, compId, compIdArr, libraryId, libraryName, roomName, questionChickArr } = this.state
    let formData = new FormData();
    let data = {};
    data = {
      "action": "editstockinventory",
      "servicetype": "pdaservice",
      "express": "FF4C7366",
      "ciphertext": "1c588e8167652f1528880a52a0ab4060",
      "data": {
        "rowGuid": rowguid,
        "editType": "submit",
        "username": Url.PDAusername
      }
    }

    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('保存成功');
        this.props.navigation.navigate('MainCheckPage')
      } else {
        this.toast.show('保存失败')
        Alert.alert('保存失败', resData.message)
      }
    }).catch((error) => {
      this.toast.show('保存失败')
      if (error.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('保存失败', "响应内容不是合法JSON格式")
        return
      }
      if (error.toString().indexOf('Network request faile') != -1) {
        Alert.alert('保存失败', "网络请求错误")
        return
      }
      Alert.alert('保存失败', error.toString())
    });
  }

  DeleteData = () => {
    const { formCode, projectId, projectName, ServerTime, reason, remark, carnum, supId, supName, rowguid, compId, compIdArr, libraryId, libraryName, roomName, questionChickArr } = this.state
    let formData = new FormData();
    let data = {};
    data = {
      "action": "editstockinventory",
      "servicetype": "pdaservice",
      "express": "FF4C7366",
      "ciphertext": "1c588e8167652f1528880a52a0ab4060",
      "data": {
        "rowGuid": rowguid,
        "editType": "del",
        "username": Url.PDAusername
      }
    }

    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('删除成功');
        this.props.navigation.navigate('MainCheckPage')
      } else {
        Alert.alert('删除失败', resData.message)
      }
    }).catch((error) => {
    });
  }

  cardListContont = comp => {
    if (visibleArr.indexOf(comp.compCode) == -1) {
      visibleArr.push(comp.compCode);
      this.setState({ visibleArr: visibleArr });
    } else {
      if (visibleArr.length == 1) {
        visibleArr.splice(0, 1);
        this.setState({ visibleArr: visibleArr }, () => {
          this.forceUpdate;
        });
      } else {
        visibleArr.map((item, index) => {
          if (item == comp.compCode) {
            visibleArr.splice(index, 1);
            this.setState({ visibleArr: visibleArr }, () => {
              this.forceUpdate;
            });
          }
        });
      }
    }
  };

  CardList = (compData) => {
    return (
      compData.map((item, index) => {
        let comp = item
        return (
          <View style={styles.CardList_main}>
            <TouchableCustom
              onPress={() => {
                this.cardListContont(comp);
              }}
              onLongPress={() => {
                Alert.alert(
                  '提示信息',
                  '是否删除构件信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      compDataArr.splice(index, 1)
                      compIdArr.splice(index, 1)
                      tmpstr = compIdArr.toString()
                      this.setState({
                        compDataArr: compDataArr,
                        compIdArr: compIdArr
                      })
                    }
                  }])
              }}>
              <View style={styles.CardList_title_main}>
                <View style={styles.title_num}>
                  <Text >{index + 1}</Text>
                </View>
                <View style={styles.title_title}>
                  <Text >{item.compCode}</Text>
                </View>
                <View style={styles.CardList_at_icon}>
                  <IconDown />
                </View>
              </View>
            </TouchableCustom>

            {
              visibleArr.indexOf(item.compCode) != -1 ?
                <View key={index} style={{ backgroundColor: '#F9F9F9' }} >
                  <ListItemScan_child
                    title='产品编号'
                    rightTitle={comp.compCode}
                  />
                  <ListItemScan_child
                    title='项目名称'
                    rightTitle={comp.projectName}
                  />
                  <ListItemScan_child
                    title='构件类型'
                    rightTitle={comp.compTypeName}
                  />
                  <ListItemScan_child
                    title='设计型号'
                    rightTitle={comp.designType}
                  />
                  <ListItemScan_child
                    title='楼号'
                    rightTitle={comp.floorNoName}
                  />
                  <ListItemScan_child
                    title='层号'
                    rightTitle={comp.floorName}
                  />
                  <ListItemScan_child
                    title='砼方量'
                    rightTitle={comp.volume}
                  />
                  <ListItemScan_child
                    title='重量'
                    rightTitle={comp.weight}
                  />
                  <ListItemScan_child
                    title='生产单位'
                    rightTitle={Url.PDAFname}
                  />
                </View> : <View></View>
            }

          </View>
        )
      })
    )
  }

  isBottomVisible = (data, focusIndex) => {
    if (typeof (data) != "undefined") {
      if (data.length > 0) {
        this.setState({ bottomVisible: true, bottomData: data, focusIndex: focusIndex })
      }
    } else {
      this.toast.show("无数据")
    }
  }

  cameraFunc = () => {
    launchCamera(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: parseInt(Url.isResizePhoto),
        maxWidth: parseInt(Url.isResizePhoto),
        saveToPhotos: true,

      },
      (response) => {
        if (response.uri == undefined) {
          return
        }
        imageArr.push(response.uri)
        this.setState({
          //pictureSelect: true,
          pictureUri: response.uri,
          imageArr: imageArr
        })
        console.log(response)
      },
    )
  }

  camLibraryFunc = () => {
    launchImageLibrary(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: parseInt(Url.isResizePhoto),
        maxWidth: parseInt(Url.isResizePhoto),
      },
      (response) => {
        if (response.uri == undefined) {
          return
        }
        imageArr.push(response.uri)
        this.setState({
          //pictureSelect: true,
          pictureUri: response.uri,
          imageArr: imageArr
        })
        console.log(response)
      },
    )
  }

  camandlibFunc = () => {
    Alert.alert(
      '提示信息',
      '选择拍照方式',
      [{
        text: '取消',
        style: 'cancel'
      }, {
        text: '拍照',
        onPress: () => {
          this.cameraFunc()
        }
      }, {
        text: '相册',
        onPress: () => {
          this.camLibraryFunc()
        }
      }])
  }

  CameraView = () => {
    return (
      <ListItem
        containerStyle={{ paddingBottom: 6 }}
        leftAvatar={
          <View >
            <View style={{ flexDirection: 'column' }} >
              {
                this.state.isCheckPage ? <View></View> :
                  <AvatarAdd
                    pictureSelect={this.state.pictureSelect}
                    backgroundColor='rgba(77,142,245,0.20)'
                    color='#4D8EF5'
                    title="构"
                    onPress={() => {
                      if (Url.isUsePhotograph == "YES") {
                        this.camandlibFunc()
                      } else {
                        this.cameraFunc()
                      }
                    }} />
              }
              {this.imageView()}
            </View>
          </View>
        }
      />
    )
  }

  imageView = () => {
    return (
      <View style={{ flexDirection: 'row' }}>
        {
          this.state.pictureSelect ?
            this.state.imageArr.map((item, index) => {
              return (
                <AvatarImg
                  item={item}
                  index={index}
                  onPress={() => {
                    this.setState({
                      imageOverSize: true,
                      pictureUri: item
                    })
                  }}
                  onLongPress={() => {
                    Alert.alert(
                      '提示信息',
                      '是否删除构件照片',
                      [{
                        text: '取消',
                        style: 'cancel'
                      }, {
                        text: '删除',
                        onPress: () => {
                          let i = imageArr.indexOf(item)
                          if (i > -1) {
                            imageArr.splice(i, 1)
                            this.setState({
                              imageArr: imageArr
                            }, () => {
                              this.forceUpdate()
                            })
                          }
                          if (imageArr.length == 0) {
                            this.setState({
                              pictureSelect: false
                            })
                          }
                        }
                      }])
                  }} />
              )
            }) : <View></View>
        }
      </View>
    )
  }

  _DatePicker = () => {
    const { ServerTime } = this.state
    return (
      <DatePicker
        customStyles={{
          dateInput: styles.dateInput,
          dateTouchBody: styles.dateTouchBody,
          dateText: styles.dateText,
        }}
        iconComponent={<Icon name='caretdown' color='#419fff' iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
        }
        showIcon={true}
        mode='datetime'
        date={ServerTime}
        format="YYYY-MM-DD HH:mm"
        onOpenModal={() => {
          this.setState({
            focusIndex: '4'
          })
        }}
        onDateChange={(value) => {
          this.setState({
            ServerTime: value
          })
        }}
      />
    )
  }

  render() {
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || '';
    const type = navigation.getParam('type') || '';

    const { inventoryState, resData, formCode, carnum, projectName, reasons, focusIndex, reasonselect, ServerTime, teamselect, roomselect, library, libraryId, libraryselect, libraryName, bottomVisible, bottomData, compData, compId, compCode, compDataArr, SupplierInfo, supSelect, remark } = this.state

    if (type == 'keeper') {
      keeper.map((item, index) => {
        if (QRid == item.keeperId) {
          QRkeeperID = item.keeperId
          QRkeeperName = item.keeperName
        }
      })
    } else {
      QRStock = QRid
    }

    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          <NavigationEvents
            onWillFocus={this.UpdateControl} />
          <ScrollView ref={ref => this.scoll = ref} style={styles.mainView}showsVerticalScrollIndicator={false} >
            <View style={styles.listView}>

              <ListItemScan
                title='盘点编号'
                rightTitle={formCode}
              />

              <ListItemScan
                title='盘点说明'
                rightElement={
                  <Input
                    containerStyle={[styles.quality_input_container, { top: 2 }]}
                    inputContainerStyle={styles.inputContainerStyle}
                    inputStyle={[styles.quality_input_]}
                    placeholder='请输入'
                    defaultValue={remark}
                    value={remark}
                    onFocus={() => { this.setState({ focusIndex: 6 }) }}
                    onChangeText={(value) => {
                      this.setState({
                        remark: value
                      })
                    }} />
                }
              />

              <ListItemScan
                focusStyle={focusIndex == '2' ? styles.focusColor : {}}
                title='库房名称'
                rightElement={
                  <IconDown text={'请选择'} />
                }
              />

              <View style={{ marginHorizontal: width * 0.05, backgroundColor: "#f8f8f8", borderRadius: 5, marginTop: 10 }}>
                <FlatList
                  data={this.state.lstRoom}
                  renderItem={this._renderItem.bind(this)}
                  extraData={this.state} />
              </View>

              <ListItemScan
                focusStyle={focusIndex == '4' ? styles.focusColor : {}}
                title='编制日期'
                rightTitle={this._DatePicker()}
                bottomDivider
              />

              <ListItemScan
                title='操作人'
                rightTitle={Url.PDAEmployeeName}
                rightTitleStyle={{ width: width / 1.5, textAlign: 'right', fontSize: 15 }}
                rightTitleProps={{ numberOfLines: 1 }}
                bottomDivider
              />

            </View>

            {
              this.state.isCheckPage ?
                inventoryState == '0' ?
                  <View style={{ flexDirection: 'row' }}>
                    <Button
                      title={'删除'}
                      onPress={() => {
                        this.DeleteData()
                      }}
                      titleStyle={{ color: "white", fontSize: 17 }}
                      type='solid'
                      style={{ marginVertical: 10 }}
                      containerStyle={{ marginVertical: 10, marginHorizontal: 22, flex: 1 }}
                      buttonStyle={{ backgroundColor: '#EB5D20', borderRadius: 10, paddingVertical: 16 }}
                    ></Button>
                    <Button
                      title={'盘点结束'}
                      onPress={() => {
                        this.ReviseData()
                      }}
                      titleStyle={{ color: "white", fontSize: 17 }}
                      type='solid'
                      style={{ marginVertical: 10 }}
                      containerStyle={{ marginVertical: 10, marginHorizontal: 22, flex: 1 }}
                      buttonStyle={{ backgroundColor: '#17BC29', borderRadius: 10, paddingVertical: 16 }}
                    ></Button>
                  </View> : <View></View>
                :
                <FormButton
                  backgroundColor='#17BC29'
                  title='保存'
                  onPress={() => {
                    this.PostData()
                  }}
                />
            }

            <Overlay
              fullScreen={true}
              animationType='fade'
              overlayStyle={{ backgroundColor: '#f9f9f9' }}
              isVisible={this.state.isQuestionOverlay}
              onBackdropPress={() => { this.setState({ isQuestionOverlay: !this.state.isQuestionOverlay }); }}
              onRequestClose={() => {
                this.setState({ isQuestionOverlay: !this.state.isQuestionOverlay });
              }}
            >
              <Header
                centerComponent={{ text: '问题缺陷', style: { color: 'gray', fontSize: 20 } }}
                rightComponent={<BackIcon
                  onPress={() => {
                    this.setState({
                      isQuestionOverlay: !this.state.isQuestionOverlay
                    });
                  }} />}
                backgroundColor='white'
              />
              <FlatList
                data={this.state.dictionary}
                renderItem={this._renderItem.bind(this)}
                extraData={this.state} />
            </Overlay>

            <BottomSheet
              isVisible={bottomVisible && !this.state.isCheckPage}
              onRequestClose={() => {
                this.setState({
                  bottomVisible: false
                })
              }}
              onBackdropPress={() => {
                this.setState({
                  bottomVisible: false
                })
              }}
            >
              <ScrollView style={styles.bottomsheetScroll}>
                {
                  bottomData.map((item, index) => {
                    let title = ''
                    if (item.roomId) {
                      title = item.roomName
                    } else if (item.libraryId) {
                      title = item.libraryName
                    } else if (item.driver) {
                      title = item.driver
                    } else if (item.reason) {
                      title = item.reason
                    } else if (item.supId) {
                      title = item.supName
                    }
                    return (
                      <TouchableCustom
                        onPress={() => {
                          if (item.roomId) {
                            this.setState({
                              roomId: item.roomId,
                              roomselect: item.roomName,
                              roomName: item.roomName,
                              library: item.library
                            })
                          } else if (item.libraryId) {
                            this.setState({
                              libraryId: item.libraryId,
                              libraryName: item.libraryName,
                              libraryselect: item.libraryName
                            })
                          } else if (item.reason) {
                            this.setState({
                              reason: item.reason,
                              reasonselect: item.reason,
                            })
                          } else if (item.supId) {
                            this.setState({
                              supId: item.supId,
                              supName: item.supName,
                              supSelect: item.supName
                            })
                          }
                          this.setState({
                            bottomVisible: false
                          })
                        }}
                      >
                        <BottomItem backgroundColor='white' color="#333" title={title} />
                      </TouchableCustom>
                    )

                  })
                }
              </ScrollView>

              <TouchableCustom
                onPress={() => {
                  this.setState({ bottomVisible: false })
                }}>
                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
              </TouchableCustom>
            </BottomSheet>
          </ScrollView>
        </View>
      )
    }
  }

  checkListSecondisVisible = (item) => {
    if (isCheckSecondList.indexOf(item.defectTypeId) == -1) {
      isCheckSecondList.push(item.defectTypeId);
      this.setState({ isCheckSecondList: isCheckSecondList });
    } else {
      if (isCheckSecondList.length == 1) {
        isCheckSecondList.splice(0, 1);
        this.setState({ isCheckSecondList: isCheckSecondList }, () => {
          this.forceUpdate;
        });
      } else {
        isCheckSecondList.map((item1, index) => {
          if (item1 == item.defectTypeId) {
            isCheckSecondList.splice(index, 1);
            this.setState({ isCheckSecondList: isCheckSecondList }, () => {
              this.forceUpdate;
            });
          }
        });
      }
    }
  }

  //主列全选功能
  allMainCheck = (item) => {
    //主列全选
    if (isCheckedAllList.indexOf(item.defectTypeId) == -1) {
      //遍历子列，全部勾选
      item.problemDefects.map((defectitem, defectindex) => {
        if (questionChickIdArr.indexOf(defectitem.rowguid) == -1) {
          questionChickArr.push(defectitem)
          questionChickIdArr.push(defectitem.rowguid)
          this.setState({
            questionChickArr: questionChickArr,
            questionChickIdArr: questionChickIdArr,
          })
        }
      })
      //子列全部勾选就勾选上主列
      isCheckedAllList.push(item.defectTypeId)
      this.setState({ isCheckedAllList: isCheckedAllList })
    }
    //主列取消全选
    else {
      //遍历子列，依次取消勾选
      item.problemDefects.map((defectitem, defectindex) => {
        if (questionChickIdArr.indexOf(defectitem.rowguid) != -1) {
          let num = questionChickIdArr.indexOf(defectitem.rowguid)
          questionChickIdArr.splice(num, 1)
          questionChickArr.splice(num, 1)
          this.setState({
            questionChickArr: questionChickArr,
            questionChickIdArr: questionChickIdArr,
          })
        }
      })
      //把主列数据取消勾选
      let frinum = isCheckedAllList.indexOf(item.defectTypeId)
      isCheckedAllList.splice(frinum, 1)
      this.setState({ isCheckedAllList: isCheckedAllList })
    }
  }

  sonCheck = (item, defectitem) => {
    //点击勾选☑️
    if (questionChickIdArr.indexOf(defectitem.rowguid) == -1) {
      //数组中添加勾选的子列数据
      questionChickArr.push(defectitem)
      questionChickIdArr.push(defectitem.rowguid)
      this.setState({
        questionChickArr: questionChickArr,
        questionChickIdArr: questionChickIdArr,
      })
      //记录子列是否全部勾选，用数量对比
      let count = 0
      item.problemDefects.map((judgeitem) => {
        if (questionChickIdArr.indexOf(judgeitem.rowguid) != -1) {
          count += 1
        }
      })
      //子列全部勾选，就勾选主列
      if (count == item.problemDefects.length) {
        isCheckedAllList.push(item.defectTypeId)
        this.setState({ isCheckedAllList: isCheckedAllList })
      }

    }
    //反勾选 
    else {
      //查出取消勾选项的角标
      let num = questionChickIdArr.indexOf(defectitem.rowguid)
      //删掉！
      questionChickIdArr.splice(num, 1)
      questionChickArr.splice(num, 1)
      this.setState({
        questionChickArr: questionChickArr,
        questionChickIdArr: questionChickIdArr
      })
    }
    //如果子列取消全部勾选，主列取消勾选
    if (questionChickIdArr.indexOf(defectitem.rowguid) == -1) {
      let frinum = isCheckedAllList.indexOf(item.defectTypeId)
      isCheckedAllList.splice(frinum, 1)
      this.setState({ isCheckedAllList: isCheckedAllList })
    }
  }

  //主列选择功能
  MainCheck = (item) => {
    //点击勾选☑️
    if (isCheckedIdList.indexOf(item._Rowguid) == -1) {
      //数组中添加勾选的子列数据
      isCheckedList.push(item)
      isCheckedIdList.push(item._Rowguid)
      isCheckedNameList.push(item.roomName)
      questionChickArr.push(item)
      questionChickIdArr.push(item.rowguid)
      this.setState({
        questionChickArr: questionChickArr,
        questionChickIdArr: questionChickIdArr,
        isCheckedList: isCheckedList,
        isCheckedNameList: isCheckedNameList,
        isCheckedIdList: isCheckedIdList,
      })
    }
    //反勾选 
    else {
      //查出取消勾选项的角标
      let num = isCheckedIdList.indexOf(item._Rowguid)
      //删掉！
      isCheckedList.splice(num, 1)
      isCheckedIdList.splice(num, 1)
      isCheckedNameList.splice(num, 1)
      questionChickIdArr.splice(num, 1)
      questionChickArr.splice(num, 1)
      this.setState({
        questionChickArr: questionChickArr,
        questionChickIdArr: questionChickIdArr,
        isCheckedList: isCheckedList,
        isCheckedNameList: isCheckedNameList,
        isCheckedIdList: isCheckedIdList,
      })
    }
  }

  _renderItem = ({ item, index }) => {
    const { isCheckSecondListVisible } = this.state
    return (
      <View style={{}}>
        <ListItem
          title={
            <View style={{ flexDirection: 'row' }}>
              {/* <Icon name={isCheckSecondList.indexOf(item.defectTypeId) != -1 ? 'downcircle' : 'rightcircle'} size={18} type='antdesign' color='#419FFF' /> */}
              <Text style={{ fontSize: 14, marginLeft: 4 }}>{item.roomName}</Text>
            </View>
          }
          underlayColor={'lightgray'}
          containerStyle={{ marginVertical: 0, paddingVertical: 10, borderColor: 'transparent', borderWidth: 0, backgroundColor: "transparent" }}
          onPress={() => {
            this.MainCheck(item)
          }}
          rightElement={
            <CheckBoxScan
              key={index}
              //checkedColor='red'
              checked={isCheckedIdList.indexOf(item._Rowguid) != -1}
              onPress={() => {
                this.MainCheck(item)
              }
              } />
          } />

      </View>
    )
  }



}