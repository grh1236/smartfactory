import React from 'react';
import { View, StatusBar, TouchableOpacity, TouchableHighlight, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, Input, Icon, Badge, Card, ListItem, BottomSheet, CheckBox, Avatar, Overlay, Header } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { RFT } from '../../Url/Pixal';
import { ScrollView } from 'react-native-gesture-handler';
import CardList from "../Componment/CardList";
import DatePicker from 'react-native-datepicker'
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { Image } from 'react-native';
import { NavigationEvents } from 'react-navigation';
import { styles } from '../Componment/PDAStyles';
import ScanButton from '../Componment/ScanButton';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import ListItemScan_child from '../Componment/ListItemScan_child';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import CheckBoxScan from '../Componment/CheckBoxScan';
import AvatarAdd from '../Componment/AvatarAdd';
import AvatarImg from '../Componment/AvatarImg';
import DeviceStorage from '../../Url/DeviceStorage';
import overlayImg from '../Componment/OverlayImg';
import OverlayImgZoomPicker from '../Componment/OverlayImgZoomPicker';
let varGetError = false

let DeviceStorageData = {} //缓存数据

const { height, width } = Dimensions.get('window') //获取宽高

let QRcompid = '', QRtaiwanId = '', QRacceptanceId = '', QRmonitorId = '', QRInspectorId = ''

let imageArr = [], //照片数组
  questionChickIdArr = [], questionChickArr = [], //子表问题数组
  isCheckSecondList = [], //是否展开二级
  isCheckedAllList = [] //二级菜单是否被全选

class BackIcon extends React.PureComponent {
  render() {
    return (
      <TouchableCustom
        onPress={this.props.onPress} >
        <Icon
          name='arrow-back'
          color='#419FFF' />
      </TouchableCustom>
    )
  }
}

export default class SpotCheck extends React.PureComponent {
  constructor() {
    super();
    this.state = {
      resData: [],
      rowguid: '',
      formCode: '',
      ServerTime: '2000-01-01 00:00',
      steelCageIDs: ' ',
      currentState: '',
      Inspectors: [],
      InspectorId: '',
      InspectorName: '',
      InspectorSelect: '请选择',
      acceptanceId: '',
      acceptanceName: '',
      checkResult: '合格',
      taiwanId: '',
      compData: {},
      compId: '',
      steelLabel: '',
      compCode: '',
      Tai_QRname: '',
      editEmployeeName: '',
      checked: true,
      //问题缺陷
      defectTypes: [],
      dictionary: [],
      problemDefect: '',
      measure: '',
      problemId: '',
      questionChick: -1,
      questionChickIdArr: [],
      questionChickArr: [],
      problemDefects: [],
      isCheckSecondList: [],
      isCheckedAllList: [],
      isQuestionOverlay: false,
      count: 0,
      //是否扫码成功
      isGetcomID: false,
      isGettaiwanId: false,
      isGetTeamPeopleInfo: false,
      isGetCommonMould: false,
      isGetInspector: false,
      //长按删除功能控制
      iscomIDdelete: false,
      istaiwanIddelete: false,
      isCommonMoulddelete: false,
      isTeamPeopledelete: false,
      isInspectordelete: false,
      //底栏控制
      bottomData: [],
      bottomVisible: false,
      datepickerVisible: false,
      CameraVisible: false,
      response: '',
      //照片控制
      pictureSelect: false,
      pictureUri: '',
      imageArr: [],
      imageOverSize: false,
      //
      GetError: false,
      buttondisable: false,
      //钢筋笼控制
      steelCageID: '',
      steelCageCode: '',
      isGetsteelCageInfoforPutMold: false,
      issteelCageIDdelete: false,
      focusIndex: -1,
      isLoading: true,
      //查询界面取消选择按钮
      isCheck: false,
      isCheckPage: false,
      remark: '',
    }
    //this.requestCameraPermission = this.requestCameraPermission.bind(this)
  }

  componentDidMount() {
    StatusBar.setBarStyle('dark-content')
    const { navigation } = this.props;
    let guid = navigation.getParam('guid') || ''
    let pageType = navigation.getParam('pageType') || ''
    if (pageType == 'CheckPage') {
      this.checkResData(guid)
      this.setState({ isCheckPage: true })
    } else {
      this.resData()
      DeviceStorage.get('DeviceStorageDataQI')
        .then(res => {
          if (res) {
            console.log("🚀 ~ file: qualityinspection.js ~ line 133 ~ QualityInspection ~ componentDidMount ~ res", res)
            let DeviceStorageDataObj = JSON.parse(res)
            DeviceStorageData = DeviceStorageDataObj
            console.log("🚀 ~ file: qualityinspection.js ~ line 136 ~ QualityInspection ~ componentDidMount ~ DeviceStorageData", DeviceStorageData)
            if (res.length != 0) {
              this.setState({
                "isGetInspector": DeviceStorageDataObj.isGetInspector,
                "isGetTeamPeopleInfo": DeviceStorageDataObj.isGetTeamPeopleInfo,
                "InspectorId": DeviceStorageDataObj.InspectorId,
                "InspectorName": DeviceStorageDataObj.InspectorName,
                "InspectorSelect": DeviceStorageDataObj.InspectorName,
                "monitorId": DeviceStorageDataObj.monitorId,
                "monitorName": DeviceStorageDataObj.monitorName,
                "monitorSelect": DeviceStorageDataObj.monitorName,
                "monitorTeamName": DeviceStorageDataObj.monitorTeamName,
              })
            }
          }
        }).catch(err => {
          console.log("🚀 ~ file: loading.js ~ line 131 ~ SteelCage ~ componentDidMount ~ err", err)

        })
    }
  }

  componentWillUnmount() {
    imageArr = [], //照片数组
      questionChickIdArr = [], questionChickArr = [], //子表问题数组
      isCheckSecondList = [], //是否展开二级
      isCheckedAllList = [] //二级菜单是否被全选

  }

  UpdateControl = () => {
    if (typeof this.props.navigation.getParam('QRid') != "undefined") {
      if (this.props.navigation.getParam('QRid') != "") {
        this.toast.show(Url.isLoadingView, 0)
      }
    }
    const { navigation } = this.props;
    const { isGetcomID, isGetCommonMould, isGetInspector, isGetTeamPeopleInfo, isGettaiwanId, isGetsteelCageInfoforPutMold } = this.state
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || ''
    console.log("🚀 ~ file: qualityinspection.js ~ line 11113 ~ SteelCage ~ componentDidUpdate ~ QRid", QRid)
    let type = navigation.getParam('type') || '';
    console.log("🚀 ~ file: qualityinspection.js ~ line 105 ~ SteelCage ~ componentDidUpdate ~ this.state.GetError", this.state.GetError)

    if (type == 'compid') {
      this.GetcomID(QRid)
    }
    console.log("🚀 ~ file: qualityinspection.js ~ line 11147 ~ SteelCage ~ componentDidUpdate ~ QRid", QRid)

  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  checkResData = (guid) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getsamplingdetail",
      "servicetype": "sampling",
      "express": "8AC4C0B0",
      "ciphertext": "7df83f712027c63f147f6c2d4a43f08d",
      "data": {
        "_Rowguid": guid,
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: qualityinspection.js ~ line 189 ~ QualityInspection ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 204 ~ QualityInspection ~ resData", resData)
      let rowguid = resData.result.guid
      let compCode = resData.result.compCode
      let ServerTime = resData.result.samplingDate
      let compId = resData.result.compId
      let Inspector = resData.result.inspector
      let currentState = resData.result.currentState
      let formCode = resData.result.formCode
      let projectName = resData.result.projectName
      let checkResult = resData.result.checkResult
      let dictionary = resData.result.problemDefects
      console.log("🚀 ~ file: spotcheck.js ~ line 263 ~ SpotCheck ~ projectName", projectName)

      let ImageUrls = resData.result.imageURL
      let remark = resData.result.remark
      //图片数组
      let imageArr = [], imageFileArr = [];

      ImageUrls.map((item, index) => {
        imageArr.push(item)
        let tmp = {}
        tmp.fileid = ""
        tmp.uri = item
        tmp.url = item
        imageFileArr.push(tmp)
      })
      this.setState({
        imageFileArr: imageFileArr
      })
      console.log("🚀 ~ file: qualityinspection.js ~ line 224 ~ QualityInspection ~ imageArr", imageArr)

      if (checkResult == '不合格') {
        this.setState({ checked: false })
        dictionary.map((defectitem, index) => {
          if (defectitem.ischecked == true) {
            questionChickArr.push(defectitem)
            questionChickIdArr.push(defectitem.rowguid)
          }
        })
      }

      //产品子表
      let compData = {}, result = {}
      result.projectName = resData.result.projectName
      result.compTypeName = resData.result.compTypeName
      result.designType = resData.result.designType
      result.floorNoName = resData.result.floorNoName
      result.floorName = resData.result.floorName
      result.volume = resData.result.volume
      result.weight = resData.result.weight
      compData.result = result
      this.setState({
        resData: resData,
        rowguid: rowguid,
        compData: compData,
        compCode: compCode,
        compId: compId,
        formCode: formCode,
        ServerTime: ServerTime,
        currentState: currentState,
        InspectorName: Inspector,
        InspectorSelect: Inspector,
        remark: remark,
        imageArr: imageArr,
        checkResult: checkResult,
        questionChickArr: questionChickArr,
        questionChickIdArr: questionChickIdArr,
        dictionary: dictionary,
        pictureSelect: true,
        isGetcomID: true,
        isGettaiwanId: true,
        isGetTeamPeopleInfo: true,
        isGetCommonMould: true,
        isGetInspector: true,
        isCheck: true
      })
      this.setState({
        isLoading: false,
      })
    }).catch((error) => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 215 ~ QualityInspection ~ error", error)
    });
  }

  resData = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "samplinginitial",
      "servicetype": "sampling",
      "express": "8AC4C0B0",
      "ciphertext": "7df83f712027c63f147f6c2d4a43f08d",
      "data": {
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 314 ~ QualityInspection ~ resData", resData)
      //初始化错误提示
      if (resData.status == 100) {
        let rowguid = resData.result.rowguid
        let ServerTime = resData.result.samplingDate
        let formCode = resData.result.formCode
        let Inspectors = resData.result.inspectors
        let defectTypes = resData.result.defectTypes
        this.setState({
          resData: resData,
          rowguid: rowguid,
          ServerTime: ServerTime,
          Inspectors: Inspectors,
          formCode: formCode,
          dictionary: defectTypes,
          defectTypes: defectTypes,
          isLoading: false
        })
      }
      else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }

    }).catch((error) => {
    });

  }

  GetcomID = (id) => {
    console.log("🚀 ~ file: qualityinspection.js ~ line 194 ~ SteelCage ~ GetcomID", id)
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getsamplingcomp",
      "servicetype": "sampling",
      "express": "0F51EF23",
      "ciphertext": "a91d6dc5c5a20f2a35939b6e2d91a0ef",
      "data": {
        "compId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: qualityinspection.js ~ line 367 ~ QualityInspection ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let compCode = resData.result.compCode
        let compId = resData.result.compId
        let currentState = resData.result.currentState

        this.setState({
          compData: resData,
          compCode: compCode,
          compId: compId,
          currentState: currentState,
          isGetcomID: true,
          iscomIDdelete: false
        })
      } else {
        console.log('resData', resData)
        Alert.alert('错误', resData.message)
        this.setState({
          compId: ""
        })
        //this.input1.focus()
        varGetError = true
        this.setState({
          GetError: true
        })
      }

    }).catch(err => {
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
      console.log("🚀 ~ file: spotcheck.js ~ line 417 ~ SpotCheck ~ err", err)
    })
  }

  comIDItem = (index) => {
    const { isGetcomID, buttondisable, compCode, compId } = this.state
    return (
      <View>
        {
          !isGetcomID ?
            <View style={{ flexDirection: 'row' }}>
              <View>
                <Input
                  ref={ref => { this.input1 = ref }}
                  containerStyle={styles.scan_input_container}
                  inputContainerStyle={styles.scan_inputContainerStyle}
                  inputStyle={[styles.scan_input]}
                  placeholder='请输入'
                  keyboardType='numeric'
                  value={compId}
                  onChangeText={(value) => {
                    value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
                    value = value.replace(/^\./g, ""); //验证第一个字符是数字
                    value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
                    //value = value.replace(/\b(0+)/gi, ""); //清楚开头的0
                    this.setState({
                      compId: value
                    })
                  }}
                  //onSubmitEditing={() => { this.input1.focus() }}
                  //onFocus={() => { this.setState({ focusIndex: '5' }) }}
                  /*  */
                  //returnKeyType = 'previous'
                  onSubmitEditing={() => {
                    let inputComId = this.state.compId
                    console.log("🚀 ~ file: qualityinspection.js ~ line 468 ~ QualityInspection ~ inputComId", inputComId)
                    inputComId = inputComId.replace(/\b(0+)/gi, "")
                    this.GetcomID(inputComId)
                  }}
                />
              </View>
              <ScanButton

                onPress={() => {
                  this.setState({
                    iscomIDdelete: false,
                    buttondisable: true,
                    focusIndex: 0
                  })
                  setTimeout(() => {
                    this.setState({ GetError: false, buttondisable: false })
                    varGetError = false
                  }, 1500)
                  this.props.navigation.navigate('QRCode', {
                    type: 'compid',
                    page: 'SpotCheck'
                  })
                }}
              />
            </View>
            :
            <TouchableCustom
              onPress={() => {
                console.log('onPress')
              }}
              onLongPress={() => {
                console.log('onLongPress')
                Alert.alert(
                  '提示信息',
                  '是否删除构件信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      this.setState({
                        compData: [],
                        compCode: '',
                        compId: '',
                        iscomIDdelete: true,
                        isGetcomID: false,
                      })
                    }
                  }])
              }} >
              <Text>{compCode}</Text>
            </TouchableCustom>
        }
      </View>

    )
  }

  GetsteelCageInfoforPutMold = (id) => {
    console.log("🚀 ~ file: qualityinspection.js ~ line 346 ~ SteelCage ~ id", id)
    let formData = new FormData();
    let data = {};
    data = {
      "action": "GetsteelCageInfoforPutMold",
      "servicetype": "pda",
      "express": "6C8FAB14",
      "ciphertext": "96d2d1428051129167842f3b5eaca07f",
      "data": {
        "steelCageID": id,
        "compId": this.state.compId,
        "factoryId": Url.PDAFid
      }
    }
    console.log("🚀 ~ file: qualityinspection.js ~ line 349 ~ SteelCage ~ data", data)
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let steelCageID = resData.result.steelCageID
        let steelCageCode = resData.result.steelCageCode
        console.log("🚀 ~ file: qualityinspection.js ~ line 374 ~ SteelCage ~ steelCageCode", steelCageCode)
        this.setState({
          steelCageID: steelCageID,
          steelCageCode: steelCageCode,
          isGetsteelCageInfoforPutMold: true,
          issteelCageIDdelete: false
        })
      } else {
        console.log('resData', resData)
        Alert.alert('ERROR', resData.message)
        varGetError = true
        this.setState({
          GetError: true
        })
      }

    }).catch(err => {
    })
  }

  GettaiwanId = (id) => {
    let Tai_QRname = '111'
    let formData = new FormData();
    let data = {};
    data = {
      "action": "gettaiwanInfo",
      "servicetype": "pda",
      "express": "8FBA0BDC",
      "ciphertext": "591057f7d292870338e0fdffe9ef719f",
      "data": {
        "factoryId": Url.PDAFid,
        "taiwanId": id
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let taiwanId = resData.result.taiwanId
        Tai_QRname = resData.result.Tai_QRname
        this.setState({
          taiwanId: taiwanId,
          Tai_QRname: Tai_QRname,
          isGettaiwanId: true,
          istaiwanIddelete: false
        })
      } else {
        Alert.alert('ERROR', resData.message)
        varGetError = true
        this.setState({
          GetError: true
        })
      }

    }).catch(err => {
    })
  }

  taiwanItem = (index) => {
    const { isGettaiwanId, buttondisable, Tai_QRname } = this.state
    return (
      <View>
        {
          !isGettaiwanId ?
            <View>
              <ScanButton

                onPress={() => {
                  this.setState({
                    focusIndex: 1,
                    istaiwanIddelete: false,
                    buttondisable: true
                  })
                  setTimeout(() => {
                    this.setState({ GetError: false, buttondisable: false })
                    varGetError = false
                  }, 1500)
                  this.props.navigation.navigate('QRCode', {
                    type: 'taiwanId',
                    page: 'QualityInspection'
                  })
                }}
              />
            </View>
            :
            <TouchableCustom
              onPress={() => {
                console.log('onPress')
              }}
              onLongPress={() => {
                console.log('onLongPress')
                Alert.alert(
                  '提示信息',
                  '是否删除模台信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      this.setState({
                        taiwanId: '',
                        Tai_QRname: '',
                        isGettaiwanId: false,
                        istaiwanIddelete: true,
                      })
                    }
                  }])
              }} >
              <Text>{Tai_QRname}</Text>
            </TouchableCustom>
        }
      </View>
    )
  }

  GetCommonMould = (id) => {
    let acceptanceName = '111'
    let formData = new FormData();
    let data = {};
    data = {
      "action": "GetCommonMould",
      "servicetype": "pda",
      "express": "50545047",
      "ciphertext": "f3eedf54128226aef62dd6023a1c1573",
      "data": {
        "compId": this.state.compId,
        "acceptanceId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let acceptanceId = resData.result.acceptanceId
        acceptanceName = resData.result.acceptanceName
        this.setState({
          acceptanceId: acceptanceId,
          acceptanceName: acceptanceName,
          isGetCommonMould: true,
          isCommonMoulddelete: false
        })
      } else {
        Alert.alert('ERROR', resData.message)
        varGetError = true
        this.setState({
          GetError: true
        })
      }


    }).catch(err => {
    })
  }

  GetTeamPeopleInfo = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getTeamPeopleInfo",
      "servicetype": "pda",
      "express": "2B9B0E37",
      "ciphertext": "eb1a5891270a0ebdbb68d37e27f82b92",
      "data": {
        "factoryId": Url.PDAFid,
        "monitorId": id
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let monitorId = resData.result.monitorId
        let monitorName = resData.result.monitorName
        let teamId = resData.result.teamId
        let teamName = resData.result.teamName

        this.setState({
          monitorId: monitorId,
          monitorName: monitorName,
          monitorTeamId: teamId,
          monitorTeamName: teamName,
          isGetTeamPeopleInfo: true,
          isTeamPeopledelete: false
        }, () => {
          DeviceStorageData = {
            "isGetTeamPeopleInfo": true,
            "InspectorId": this.state.InspectorId,
            "InspectorName": this.state.InspectorName,
            "InspectorSelect": this.state.InspectorName,
            "monitorId": this.state.monitorId,
            "monitorName": this.state.monitorName,
            "monitorSelect": this.state.monitorName,
            "monitorTeamName": this.state.monitorTeamName,
          }

          DeviceStorage.save('DeviceStorageDataQI', JSON.stringify(DeviceStorageData))
        })
      } else {
        Alert.alert('ERROR', resData.message)
        varGetError = true
        this.setState({
          GetError: true
        })
      }
    }).catch(err => {
    })
  }

  monitorNameItem = (index) => {
    const { isGetTeamPeopleInfo, buttondisable, monitorName, monitorSetup, monitors, monitorSelect } = this.state
    console.log("🚀 ~ file: qualityinspection.js ~ line 559 ~ SteelCage ~ monitors", monitors)
    return (
      <View>
        {
          monitorSetup == '扫码' ?
            (
              !isGetTeamPeopleInfo ?
                <View>
                  <ScanButton

                    onPress={() => {
                      this.setState({
                        isTeamPeopledelete: false,
                        focusIndex: index
                      })
                      setTimeout(() => {
                        this.setState({ GetError: false, buttondisable: false })
                        varGetError = false
                      }, 1500)
                      this.props.navigation.navigate('QRCode', {
                        type: 'monitorId',
                        page: 'QualityInspection'
                      })
                    }}
                  />
                </View>
                :
                <TouchableCustom
                  onPress={() => {
                    console.log('onPress')
                  }}
                  onLongPress={() => {
                    console.log('onLongPress')
                    Alert.alert(
                      '提示信息',
                      '是否删除班组信息',
                      [{
                        text: '取消',
                        style: 'cancel'
                      }, {
                        text: '删除',
                        onPress: () => {
                          this.setState({
                            monitorId: '',
                            monitorName: '',
                            monitorTeamId: '',
                            monitorTeamName: '',
                            isGetTeamPeopleInfo: false,
                            isTeamPeopledelete: true
                          })
                        }
                      }])
                  }} >
                  <Text>{monitorName}</Text>
                </TouchableCustom>
            )
            :
            (
              this.state.isCheck ? <Text >{monitorSelect}</Text> :
                <TouchableCustom onPress={() => { this.isBottomVisible(monitors, index) }} >
                  <IconDown text={monitorSelect} />
                </TouchableCustom>
            )

        }
      </View>
    )
  }

  GetInspector = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getInspector",
      "servicetype": "pda",
      "express": "CDAB7A2D",
      "ciphertext": "a590b603df9b5a4384c130c7c21befb7",
      "data": {
        "InspectorId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let InspectorId = resData.result.InspectorId
        let InspectorName = resData.result.InspectorName
        this.setState({
          InspectorId: InspectorId,
          InspectorName: InspectorName,
          isGetInspector: true,
          isInspectordelete: false
        }, () => {
          DeviceStorageData = {
            "isGetInspector": true,
            "InspectorId": this.state.InspectorId,
            "InspectorName": this.state.InspectorName,
            "InspectorSelect": this.state.InspectorName,
          }

          DeviceStorage.save('DeviceStorageDataQI', JSON.stringify(DeviceStorageData))
        })
      } else {
        Alert.alert('ERROR', resData.message)
        varGetError = true
        this.setState({
          GetError: true
        })
      }

    }).catch(err => {
    })
  }

  inspectorItem = (index) => {
    const { isGetInspector, buttondisable, InspectorName, monitorSetup, InspectorSelect, Inspectors } = this.state
    return (
      <View>
        {
          monitorSetup == '扫码' ?
            (
              !isGetInspector ?
                <View>
                  <ScanButton


                    onPress={() => {
                      this.setState({
                        isInspectordelete: false,
                        buttondisable: true,
                        focusIndex: 6
                      })
                      setTimeout(() => {
                        this.setState({ GetError: false, buttondisable: false })
                        varGetError = false
                      }, 1500)
                      this.props.navigation.navigate('QRCode', {
                        type: 'InspectorId',
                        page: 'QualityInspection'
                      })
                    }}
                  />
                </View>
                :
                <TouchableCustom
                  onPress={() => {
                    console.log('onPress')

                  }}
                  onLongPress={() => {
                    console.log('onLongPress')
                    Alert.alert(
                      '提示信息',
                      '是否删除质检员信息',
                      [{
                        text: '取消',
                        style: 'cancel'
                      }, {
                        text: '删除',
                        onPress: () => {
                          this.setState({
                            InspectorId: '',
                            InspectorName: '',
                            isGetInspector: false,
                            isInspectordelete: true
                          })
                        }
                      }])
                  }} >
                  <Text>{InspectorName}</Text>
                </TouchableCustom>
            ) :
            (
              this.state.isCheck ? <Text >{InspectorSelect}</Text> :
                <TouchableCustom onPress={() => { this.isBottomVisible(Inspectors, index) }} >
                  <IconDown text={InspectorSelect} />
                </TouchableCustom>
            )
        }
      </View>
    )
  }

  PostData = () => {
    const { compCode, compData, InspectorId, acceptanceId, steelLabel, monitorId, compId, ServerTime, checkResult, rowguid, taiwanId, steelCageIDs, InspectorName, steelCageID, remark } = this.state
    console.log("🚀 ~ file: spotcheck.js ~ line 919 ~ SpotCheck ~ compData", compData)
    const { navigation } = this.props;
    const QRid = navigation.getParam('QRid') || '';

    if (compId == '') {
      this.toast.show('产品编号不能为空');
      return
    } else if (InspectorId == '') {
      this.toast.show('质检员信息不能为空');
      return
    }
    if (imageArr.length == 0) {
      this.toast.show('请先拍摄构件照片');
      return
    }


    this.toast.show(saveLoading, 0)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "savesampling",
      "servicetype": "sampling",
      "express": "06C795EE",
      "ciphertext": "a1e3818d57d9bfc9437c29e7a558e32e",
      "data": {
        "checkResult": checkResult,
        "compCode": compCode,
        "compId": compId,
        "compTypeId": compData.result.compTypeId,
        "compTypeName": compData.result.compTypeName,
        "currentState": compData.result.currentState,
        "designType": compData.result.designType,
        "diffSourceType": compData.result.diffSourceType,
        "editEmployee": Url.PDAEmployeeName,
        "editEmployeeId": Url.PDAEmployeeId,
        "factoryId": Url.PDAFid,
        "floorId": compData.result.floorId,
        "floorName": compData.result.floorName,
        "floorNoId": compData.result.floorNoId,
        "floorNoName": compData.result.floorNoName,
        "inspector": InspectorName,
        "inspectorId": InspectorId,
        "libraryId": compData.result.libraryId,
        "libraryName": compData.result.libraryName,
        "loginName": Url.PDAusername,
        "problemDefects": questionChickArr,
        "productionMode": compData.result.productionMode,
        "projectId": compData.result.projectId,
        "projectName": compData.result.projectName,
        "remark": remark,
        "roomId": compData.result.roomId,
        "roomName": compData.result.roomName,
        "rowguid": rowguid,
        "samplingDate": ServerTime,
        "versionName": compData.result.versionName,
        "volume": compData.result.volume,
        "weight": compData.result.weight
      },
    }
    formData.append('jsonParam', JSON.stringify(data))
    // imageArr.map((item, index) => {
    //   formData.append('img_' + index, {
    //     uri: item,
    //     type: 'image/jpeg',
    //     name: 'img_' + index
    //   })
    // })
    console.log("🚀 ~ file: qualityinspection.js ~ line 793 ~ SteelCage ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('保存成功');
        setTimeout(() => {
          this.props.navigation.replace(this.props.navigation.getParam("page"), {
            title: this.props.navigation.getParam("title"),
            pageType: this.props.navigation.getParam("pageType"),
            page: this.props.navigation.getParam("page"),
            isAppPhotoSetting: this.props.navigation.getParam("isAppPhotoSetting"),
            isAppDateSetting: this.props.navigation.getParam("isAppDateSetting"),
            isAppPhotoAlbumSetting: this.props.navigation.getParam("isAppPhotoAlbumSetting"),
          })
          //this.props.navigation.navigate('PDAMainStack')
        }, 400)
      } else {
        this.toast.show('保存失败')
        Alert.alert('保存失败', resData.message)
        console.log('resData', resData)
      }
    }).catch((error) => {
      this.toast.show('保存失败')
      if (error.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('保存失败', "响应内容不是合法JSON格式")
        return
      }
      if (error.toString().indexOf('Network request faile') != -1) {
        Alert.alert('保存失败', "网络请求错误")
        return
      }
      Alert.alert('保存失败', error.toString())
    });
  }

  DeleteData = () => {
    const { navigation } = this.props;
    let guid = navigation.getParam('guid') || ''
    console.log('DeleteData')
    let formData = new FormData();
    let data = {};
    data = {
      "action": "deletesampling",
      "servicetype": "sampling",
      "express": "06C795EE",
      "ciphertext": "a1e3818d57d9bfc9437c29e7a558e32e",
      "data": {
        "_Rowguid": guid,
      },
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: qualityinspection.js ~ line 953 ~ QualityInspection ~ DeleteData ~ formData", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('删除成功');
        this.props.navigation.navigate('MainCheckPage')
      } else {
        Alert.alert(
          '删除失败',
          resData.message,
          [{
            text: '取消',
            style: 'cancel'
          }, {
            text: '返回上一级',
            onPress: () => {
              this.props.navigation.goBack()
            }
          }])
        console.log('resData', resData)
        console.log('resData', resData)
      }
    }).catch((error) => {
    });

  }

  imageView = () => {
    return (
      <View style={{ flexDirection: 'row' }}>
        {
          this.state.pictureSelect ?
            this.state.imageArr.map((item, index) => {
              //if (index == 0) {
              return (
                <AvatarImg
                  item={item}
                  index={index}
                  onPress={() => {
                    this.setState({
                      imageOverSize: true,
                      pictureUri: item
                    })
                  }}
                  onLongPress={() => {
                    Alert.alert(
                      '提示信息',
                      '是否删除构件照片',
                      [{
                        text: '取消',
                        style: 'cancel'
                      }, {
                        text: '删除',
                        onPress: () => {
                          let i = imageArr.indexOf(item)
                          console.log("🚀 ~ file: qualityinspection.js ~ line 636 ~ SteelCage ~ this.state.imageArr.map ~ i", i)
                          if (i > -1) {
                            imageArr.splice(i, 1)
                            this.setState({
                              imageArr: imageArr
                            }, () => {
                              this.forceUpdate()
                            })
                          }
                          if (imageArr.length == 0) {
                            this.setState({
                              pictureSelect: false
                            })
                          }
                          console.log("🚀 ~ file: qualityinspection.js ~ line 639 ~ SteelCage ~ this.state.imageArr.map ~ imageArr", imageArr)
                        }
                      }])
                  }} />
              )
            }) : <View></View>
        }
      </View>
    )
  }

  _DatePicker = () => {
    const { ServerTime } = this.state
    return (
      <DatePicker
        customStyles={{
          dateInput: styles.dateInput,
          dateTouchBody: styles.dateTouchBody,
          dateText: styles.dateText,
        }}
        iconComponent={<Icon name='caretdown' color='#419fff' iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
        }
        showIcon={true}
        mode='datetime'
        date={ServerTime}
        onOpenModal={() => { this.setState({ focusIndex: 4 }) }}
        format="YYYY-MM-DD HH:mm"
        onDateChange={(value) => {
          this.setState({
            ServerTime: value
          })
        }}
      />
    )
  }

  checkRefuseItem = (index) => {
    const { focusIndex, checkResult } = this.state
    console.log("🚀 ~ file: quality_product_inspection.js ~ line 614 ~ SteelCage ~ this.state.checkResult", this.state.checkResult)
    if (checkResult == '不合格') {
      return (
        <View >
          <ListItemScan
            focusColor={focusIndex == (index + 1) ? styles.focusColor : {}}
            title='问题缺陷/改进措施'
            rightElement={
              <TouchableCustom onPress={() => { this.setState({ isQuestionOverlay: false, focusIndex: (index + 1) }) }} >
                <IconDown text={'请选择'} />
              </TouchableCustom>
            } />
          <View style={{ marginHorizontal: width * 0.05, backgroundColor: "#f8f8f8", borderRadius: 5, marginTop: 10 }}>
            <FlatList
              data={this.state.dictionary}
              renderItem={this._renderItem.bind(this)}
              extraData={this.state}
            />
          </View>

        </View>

      )
    }
  }

  isBottomVisible = (data, focusIndex) => {
    if (typeof (data) != "undefined") {
      if (data.length > 0) {
        this.setState({ bottomVisible: true, bottomData: data, focusIndex: focusIndex })
      }
    } else {
      this.toast.show("无数据")
    }

  }

  render() {
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || ''
    const type = navigation.getParam('type') || '';
    let pageType = navigation.getParam('pageType') || ''

    if (type == 'compid') {
      QRcompid = QRid
    }
    const { buttondisable, focusIndex, formCode, currentState, steelLabel, resData, ServerTime, isGetcomID, isGetCommonMould, isGetInspector, isGetTeamPeopleInfo, isGetsteelCageInfoforPutMold, isGettaiwanId, datepickerVisible, Tai_QRname, acceptanceName, monitorSetup, monitors, monitorid, monitorName, monitorTeamName, Inspectors, InspectorsId, InspectorsName, compData, compCode, bottomVisible, bottomData, isCheck, remark } = this.state

    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          <NavigationEvents
            onWillFocus={this.UpdateControl}
            onWillBlur={() => {
              if (QRid != '') {
                navigation.setParams({ QRid: '', type: '' })
              }
            }} />
          <ScrollView ref={ref => this.scoll = ref} style={styles.mainView} showsVerticalScrollIndicator={false} >

            <View style={styles.listView}>
              <ListItem
                containerStyle={{ paddingBottom: 6 }}
                leftAvatar={
                  <View >
                    <View style={{ flexDirection: 'column' }} >
                      {
                        pageType == 'CheckPage' ? <Avatar size="large"></Avatar> :
                          <AvatarAdd
                            pictureSelect={this.state.pictureSelect}
                            backgroundColor='rgba(77,142,245,0.20)'
                            color='#4D8EF5'
                            title="构"
                            onPress={() => {
                              Alert.alert(
                                '提示信息',
                                '选择拍照方式',
                                [{
                                  text: '取消',
                                  style: 'cancel'
                                }, {
                                  text: '拍照',
                                  onPress: () => {
                                    launchCamera(
                                      {
                                        mediaType: 'photo',
                                        includeBase64: false,
                                        maxHeight: 600,
                                        maxWidth: 600,
                                        saveToPhotos: true
                                      },
                                      (response) => {
                                        if (response.uri == undefined) {
                                          return
                                        }
                                        imageArr.push(response.uri)
                                        this.setState({
                                          pictureSelect: true,
                                          pictureUri: response.uri,
                                          imageArr: imageArr
                                        })
                                        console.log(response)
                                      },
                                    )
                                  }
                                }, {
                                  text: '相册',
                                  onPress: () => {
                                    launchImageLibrary(
                                      {
                                        mediaType: 'photo',
                                        includeBase64: false,
                                        maxHeight: 600,
                                        maxWidth: 600,

                                      },
                                      (response) => {
                                        if (response.uri == undefined) {
                                          return
                                        }
                                        imageArr.push(response.uri)
                                        this.setState({
                                          pictureSelect: true,
                                          pictureUri: response.uri,
                                          imageArr: imageArr
                                        })
                                        console.log(response)
                                      },
                                    )
                                  }
                                }])
                              /* this.setState({
                                CameraVisible: true
                              }) */
                            }} />
                      }
                      {this.imageView()}
                    </View>
                  </View>
                }
              />
              <ListItemScan
                title='表单编号'
                rightTitle={formCode}
              />
              <ListItemScan
                isButton={!isGetcomID}
                focusStyle={focusIndex == '0' ? styles.focusColor : {}}
                title='产品编号'
                rightElement={
                  this.comIDItem()
                }
              />
              {
                this.state.isGetcomID ?
                  <CardList compData={compData} /> : <View></View>
              }
              <ListItemScan
                title='构件状态'
                rightTitleStyle={{ width: width / 1.5, textAlign: 'right', fontSize: 15 }}
                rightTitle={currentState}
              />
              <ListItemScan
                focusStyle={focusIndex == '4' ? styles.focusColor : {}}
                title='抽检日期'
                rightElement={
                  this._DatePicker()
                }
              //rightTitle={ServerTime}
              />
              <ListItemScan
                title='操作人'
                rightTitleStyle={{ width: width / 1.5, textAlign: 'right', fontSize: 15 }}
                rightTitle={Url.PDAEmployeeName}
              />
              <ListItemScan
                isButton={true}
                focusStyle={focusIndex == '5' ? styles.focusColor : {}}
                bottomDivider
                title='检查结果'
                rightTitle={
                  <View style={{ flexDirection: "row", marginRight: -23, paddingVertical: 0 }}>
                    <CheckBoxScan
                      title='合格'
                      checked={this.state.checked}
                      onPress={() => {
                        this.setState({
                          checked: true,
                          checkResult: '合格',
                          focusIndex: 5
                        })
                      }}
                    />
                    <CheckBoxScan
                      title='不合格'
                      checked={!this.state.checked}
                      onPress={() => {
                        this.setState({
                          checked: false,
                          checkResult: '不合格',
                          focusIndex: 5
                        })
                      }}
                    />
                  </View>
                }
              />


              {this.checkRefuseItem(5)}

              <TouchableCustom underlayColor={'lightgray'} onPress={() => {
                this.isBottomVisible(Inspectors, 7)
              }}>
                <ListItemScan
                  focusStyle={focusIndex == '7' ? styles.focusColor : {}}
                  title='质检确认'
                  rightElement={
                    this.inspectorItem('7')
                  }
                />
              </TouchableCustom>
              <ListItemScan
                focusStyle={focusIndex == '8' ? styles.focusColor : {}}
                title='备注'
                rightElement={
                  <View>
                    <Input
                      containerStyle={styles.quality_input_container}
                      inputContainerStyle={styles.inputContainerStyle}
                      inputStyle={[styles.quality_input_, { top: 7 }]}
                      placeholder='请输入'
                      value={remark}
                      onChangeText={(value) => {
                        this.setState({
                          remark: value
                        })
                      }} />
                  </View>
                }
              />
            </View>

            {
              isCheck ? <FormButton
                backgroundColor='#EB5D20'
                onLongPress={() => {
                  Alert.alert(
                    '提示信息',
                    '是否删除',
                    [{
                      text: '取消',
                      style: 'cancel'
                    }, {
                      text: '删除',
                      onPress: () => {
                        this.DeleteData()
                      }
                    }])
                }}
                title='删除'
              /> :
                <FormButton
                  backgroundColor='#17BC29'
                  onPress={this.PostData}
                  title='保存'
                />
            }

             {this.renderZoomPicture()}

            {/* {overlayImg(obj = {
              onRequestClose: () => {
                this.setState({ imageOverSize: !this.state.imageOverSize }, () => {
                  console.log("🚀 ~ file: equipment_maintenance.js:1696 ~ render ~ imageOverSize:", this.state.imageOverSize)
                })
              },
              onPress: () => {
                this.setState({
                  imageOverSize: !this.state.imageOverSize
                })
              },
              uri: this.state.pictureUri,
              isVisible: this.state.imageOverSize
            })
            }*/}

            <BottomSheet
              isVisible={this.state.CameraVisible}
              onRequestClose={() => {
                this.setState({ CameraVisible: false })
              }}
              onBackdropPress={() => {
                this.setState({
                  bottomVisible: false
                })
              }}
            >
              <TouchableCustom
                onPress={() => {
                  launchCamera(
                    {
                      mediaType: 'photo',
                      includeBase64: false,
                      maxHeight: 600,
                      maxWidth: 600,
                      saveToPhotos: true
                    },
                    (response) => {
                      if (response.uri == undefined) {
                        return
                      }
                      imageArr.push(response.uri)
                      this.setState({
                        pictureSelect: true,
                        pictureUri: response.uri,
                        imageArr: imageArr
                      })
                      console.log(response)
                    },
                  )
                  this.setState({ CameraVisible: false })
                }} >
                <ListItemScan
                  title='拍照' >
                </ListItemScan>
              </TouchableCustom>
              <TouchableCustom
                onPress={() => {
                  launchImageLibrary(
                    {
                      mediaType: 'photo',
                      includeBase64: false,
                      maxHeight: 600,
                      maxWidth: 600,

                    },
                    (response) => {
                      if (response.uri == undefined) {
                        return
                      }
                      imageArr.push(response.uri)
                      this.setState({
                        pictureSelect: true,
                        pictureUri: response.uri,
                        imageArr: imageArr
                      })
                      console.log(response)
                    },
                  )
                  this.setState({ CameraVisible: false })
                }} >
                <ListItemScan
                  title='相册' >
                </ListItemScan>
              </TouchableCustom>
              <ListItemScan
                title='关闭'
                containerStyle={{ backgroundColor: 'red' }}
                onPress={() => {
                  this.setState({ CameraVisible: false })
                }} >
              </ListItemScan>
            </BottomSheet>

            <BottomSheet
              isVisible={datepickerVisible}
              onRequestClose={() => {
                this.setState({ datepickerVisible: false })
              }}
            >

              <ListItemScan
                title='确定'
                onPress={() => {
                  this.setState({
                    datepickerVisible: false
                  })
                }}
              ></ListItemScan>
            </BottomSheet>

            <BottomSheet
              isVisible={bottomVisible && !this.state.isCheckPage}
              onRequestClose={() => {
                this.setState({
                  bottomVisible: false
                })
              }}
              onBackdropPress={() => {
                this.setState({
                  bottomVisible: false
                })
              }}
            >
              <ScrollView style={styles.bottomsheetScroll}>
                {
                  bottomData.map((item, index) => {
                    let title = ''
                    if (item.rowguid) {
                      title = item.name + ' ' + item.teamName
                    } else {
                      title = item.inspectorName
                    }
                    return (
                      <TouchableCustom
                        onPress={() => {
                          if (item.rowguid) {
                            this.setState({
                              monitorId: item.rowguid,
                              monitorSelect: item.name,
                              monitorName: item.name,
                              monitorTeamName: item.teamName,
                            }, () => {
                              DeviceStorageData = {
                                "InspectorId": this.state.InspectorId,
                                "InspectorName": this.state.InspectorName,
                                "InspectorSelect": this.state.InspectorName,
                                "monitorId": this.state.monitorId,
                                "monitorName": this.state.monitorName,
                                "monitorSelect": this.state.monitorName,
                                "monitorTeamName": this.state.monitorTeamName,
                              }

                              DeviceStorage.save('DeviceStorageDataQI', JSON.stringify(DeviceStorageData))
                            })
                          } else if (item.inspectorId) {
                            this.setState({
                              InspectorId: item.inspectorId,
                              InspectorName: item.inspectorName,
                              InspectorSelect: item.inspectorName,
                            }, () => {
                              DeviceStorageData = {
                                "InspectorId": this.state.InspectorId,
                                "InspectorName": this.state.InspectorName,
                                "InspectorSelect": this.state.InspectorName,
                              }

                              DeviceStorage.save('DeviceStorageDataQI', JSON.stringify(DeviceStorageData))
                            })
                          }
                          this.setState({
                            bottomVisible: false
                          })
                        }}
                      >
                        <BottomItem backgroundColor='white' color="#333" title={title} />
                      </TouchableCustom>
                    )

                  })
                }
              </ScrollView>
              <TouchableHighlight
                onPress={() => {
                  this.setState({ bottomVisible: false })
                }}>
                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
              </TouchableHighlight>

            </BottomSheet>
          </ScrollView>
        </View>
      )
    }
  }

  checkListSecondisVisible = (item) => {
    if (isCheckSecondList.indexOf(item.defectTypeId) == -1) {
      isCheckSecondList.push(item.defectTypeId);
      this.setState({ isCheckSecondList: isCheckSecondList }, () => {
        this.forceUpdate()
      });
    } else {
      if (isCheckSecondList.length == 1) {
        isCheckSecondList.splice(0, 1);
        this.setState({ isCheckSecondList: isCheckSecondList }, () => {
          this.forceUpdate();
        });
      } else {
        isCheckSecondList.map((item1, index) => {
          if (item1 == item.defectTypeId) {
            isCheckSecondList.splice(index, 1);
            this.setState({ isCheckSecondList: isCheckSecondList }, () => {
              this.forceUpdate();
            });
          }
        });
      }
    }
  }

  //主列全选功能
  allMainCheck = (item) => {
    //主列全选
    if (isCheckedAllList.indexOf(item.defectTypeId) == -1) {
      //遍历子列，全部勾选
      item.problemDefects.map((defectitem, defectindex) => {
        if (questionChickIdArr.indexOf(defectitem.rowguid) == -1) {
          questionChickArr.push(defectitem)
          questionChickIdArr.push(defectitem.rowguid)
          this.setState({
            questionChickArr: questionChickArr,
            questionChickIdArr: questionChickIdArr,
          }, () => {
            this.forceUpdate()
          })
        }
      })
      //子列全部勾选就勾选上主列
      isCheckedAllList.push(item.defectTypeId)
      this.setState({ isCheckedAllList: isCheckedAllList }, () => {
        this.forceUpdate()
      })
    }
    //主列取消全选
    else {
      //遍历子列，依次取消勾选
      item.problemDefects.map((defectitem, defectindex) => {
        if (questionChickIdArr.indexOf(defectitem.rowguid) != -1) {
          let num = questionChickIdArr.indexOf(defectitem.rowguid)
          questionChickIdArr.splice(num, 1)
          questionChickArr.splice(num, 1)
          this.setState({
            questionChickArr: questionChickArr,
            questionChickIdArr: questionChickIdArr,
          })
        }
      })
      //把主列数据取消勾选
      let frinum = isCheckedAllList.indexOf(item.defectTypeId)
      isCheckedAllList.splice(frinum, 1)
      this.setState({ isCheckedAllList: isCheckedAllList }, () => {
        this.forceUpdate()
      })
    }
    console.log("🚀 ~ file: spotcheck.js ~ line 1667 ~ SpotCheck ~ isCheckedAllList", isCheckedAllList)

  }

  sonCheck = (item, defectitem) => {
    //点击勾选☑️
    if (questionChickIdArr.indexOf(defectitem.rowguid) == -1) {
      //数组中添加勾选的子列数据
      questionChickArr.push(defectitem)
      questionChickIdArr.push(defectitem.rowguid)
      this.setState({
        questionChickArr: questionChickArr,
        questionChickIdArr: questionChickIdArr,
      }, () => {
        this.forceUpdate()
      })
      //记录子列是否全部勾选，用数量对比
      let count = 0
      item.problemDefects.map((judgeitem) => {
        if (questionChickIdArr.indexOf(judgeitem.rowguid) != -1) {
          count += 1
          this.setState({ count: count })
        }
      })
      //子列全部勾选，就勾选主列
      if (count == item.problemDefects.length) {
        isCheckedAllList.push(item.defectTypeId)
        this.setState({ isCheckedAllList: isCheckedAllList }, () => {
          this.forceUpdate()
        })
      }

    }
    //反勾选 
    else {
      //查出取消勾选项的角标
      let num = questionChickIdArr.indexOf(defectitem.rowguid)
      //删掉！
      questionChickIdArr.splice(num, 1)
      questionChickArr.splice(num, 1)
      this.setState({
        questionChickArr: questionChickArr,
        questionChickIdArr: questionChickIdArr
      }, () => {
        this.forceUpdate()
      })
    }
    //如果子列取消全部勾选，主列取消勾选
    if (questionChickIdArr.indexOf(defectitem.rowguid) == -1) {
      let frinum = isCheckedAllList.indexOf(item.defectTypeId)
      isCheckedAllList.splice(frinum, 1)
      this.setState({ isCheckedAllList: isCheckedAllList })
    }
  }

  _renderItem = ({ item, index }) => {
    const { isCheckSecondListVisible, isCheckSecondList, isCheckedAllList } = this.state
    this.forceUpdate();
    return (
      <View style={{}}>
        <ListItem
          title={
            <View style={{ flexDirection: 'row' }}>
              {/* <Icon name={isCheckSecondList.indexOf(item.defectTypeId) != -1 ? 'downcircle' : 'rightcircle'} size={18} type='antdesign' color='#419FFF' /> */}
              <Text style={{ fontSize: 14, marginLeft: 4 }}>{this.state.isCheck ? item.defect : item.defectTypeName}</Text>
            </View>
          }
          underlayColor={'lightgray'}
          containerStyle={{ marginVertical: 0, paddingVertical: 10, borderColor: 'transparent', borderWidth: 0, backgroundColor: "transparent" }}
          onPress={() => {
            this.checkListSecondisVisible(item)
          }}
          rightElement={
            <CheckBoxScan
              key={index}
              //checkedColor='red'
              checked={isCheckedAllList.indexOf(item.defectTypeId) != -1}
              onPress={() => {
                this.allMainCheck(item)
              }
              } />
          } />
        {
          isCheckSecondList.indexOf(item.defectTypeId) != -1 ?
            (
              <View>
                {item.problemDefects.map((defectitem, defectindex) => {
                  return (
                    <View>
                      <ListItem
                        title={defectitem.defect + ": " + defectitem.measure}
                        titleStyle={{ fontSize: 13 }}
                        containerStyle={{ marginVertical: 0, paddingVertical: 10, borderRadius: 0, borderColor: 'lightgray', borderWidth: 0, backgroundColor: "rgba(77,142,245,0.10)" }}
                        onPress={() => {
                          this.sonCheck(item, defectitem)
                        }
                        }
                        underlayColor='lightgray'
                        rightElement={
                          <CheckBoxScan
                            key={index}
                            title=''
                            //checkedColor='red'
                            checked={this.state.questionChickIdArr.indexOf(defectitem.rowguid) != -1}
                          />
                        } />
                    </View>

                  )
                })}
                <View style={{ height: 5, backgroundColor: "rgba(77,142,245,0.10)", borderBottomLeftRadius: 10, borderBottomRightRadius: 10 }}></View>
              </View>
            ) : <View></View>
        }

      </View>
    )
  }

}
