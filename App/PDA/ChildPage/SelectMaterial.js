import React from 'react';
import { View, StyleSheet, FlatList, Dimensions, ActivityIndicator, Picker, TextInput, Alert } from 'react-native';
import { Button, Text, Card, Icon, Image, ListItem, BottomSheet, Overlay, Header, SearchBar } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { NavigationEvents } from 'react-navigation';
import { styles } from '../Componment/PDAStyles';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import CheckBoxScan from '../Componment/CheckBoxScan';
import { deviceWidth } from '../../Url/Pixal';
import ModalDropdown from 'react-native-modal-dropdown';

class BackIcon extends React.PureComponent {
    render() {
        return (
            <TouchableCustom
                onPress={this.props.onPress} >
                <Icon
                    name='arrow-back'
                    color='#419FFF' />
            </TouchableCustom>
        )
    }
}

const { height, width } = Dimensions.get('window') //获取宽高
let listData = []
let pageNum = 1
let action = ""
let returnType = ""

export default class SelectMaterial extends React.PureComponent {
    constructor() {
        super();
        this.state = {
            action: '',
            resData: [],
            supplierId: '', // 供应商id
            storageCode: '', // 材料编码
            storageName: '', // 材料名称
            size: '', // 规格型号
            unit: '', // 单位
            stock: '', // 库存
            type: '', // 材料性质
            stockCount: '', // 库存数量
            stockCount2: '', // 可用库存
            accuactualcount: '', // 项目累计领用量
            totalAmount: '0', // 合计金额 
            searchValue: '',
            selectType: '材料编码',
            searchType: 'materialCode',

            roomId: '',
            projectId: '',
            materialIds: '',

            // 选择确认
            supplierSelect: false,
            roomSelect: false,

            //底栏控制
            bottomData: [],
            teamVisible: false,
            cycleVisible: false,
            inspectorVisible: false,
            CameraVisible: false,
            response: '',

            checked: true,
            pictureVisible: true, // 问题照片
            imageOverSize: false, // 照片放大
            pictureUri: "",

            focusIndex: -1,
            isLoading: false,

            //查询界面取消选择按钮
            isCheckPage: false,
            isScan: false,
            isChecked: false,

            dataCount: 0,
            focusButton: 0,
            resDataButton: []
        }
    }

    componentDidMount() {
        const { navigation } = this.props;
        let guid = navigation.getParam('guid') || ''
        let pageType = navigation.getParam('pageType') || ''
        let roomId = ''
        let projectId = ''
        let supplierId = ''
        let keyStr = navigation.getParam('keyStr') || ''
        let title = navigation.getParam('title') || ''
        let navAction = navigation.getParam('action') || ''
        if (navAction == '项目领料') action = "selectmaterial"
        else if (navAction == '部门领料') action = "selectmaterial2"
        else if (navAction == '材料退库') action = "selectmaterialoutstoragereturn"
        else if (navAction == '材料退库-砼笼材料') action = "selectmaterialconcreteoutstoragereturn"
        else if (navAction == '采购入库') action = "selectmaterialinstorage"
        else if (navAction == '设备点检' || navAction == '设备保养' || navAction == '设备维修') action = "selectequipmaterial"
        if (navAction == '项目领料' || navAction == '部门领料') {
            roomId = navigation.getParam('roomId') || ''
            projectId = navigation.getParam('projectId') || ''
        } else if (navAction == '材料退库' || navAction == '材料退库-砼笼材料') {
            roomId = navigation.getParam('deptName') || ''
            projectId = navigation.getParam('deptId') || ''
            returnType = navigation.getParam('returnType') || ''
        } else if (navAction == '采购入库') {
            roomId = navigation.getParam('supplierId') || ''
        } else if (navAction == '设备点检' || navAction == '设备保养' || navAction == '设备维修') {
            roomId = navigation.getParam('roomId') || ''
        }
        //let roomId = '9EF207F3A6C848158B92B565E29109E8'
        //let projectId = '6326CD4EB58C40C68A0F0F2A37D027D4'
        let materialIds = navigation.getParam('materialIds') || ''
        if (pageType == 'CheckPage') {

        } else {
            this.resData(roomId, projectId, materialIds, "materialCode", keyStr)
            this.setState({
                action: navAction,
                roomId: roomId,
                projectId: projectId,
                materialIds: materialIds,
                supplierId: supplierId,
                searchValue: keyStr
            })
        }
    }

    componentWillUnmount() {
        listData = []
        pageNum = 1
        action = ""
        returnType = ""
        this.setState({

        })
    }


    //顶栏
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('title')
        }
    }

    UpdateControl = () => {
        const { navigation } = this.props;
        const { } = this.state

        let type = navigation.getParam('type') || '';
        if (type == 'compid') {
            let QRid = navigation.getParam('QRid') || ''
            this.scanMaterial(QRid)
        }
    }

    resData = (roomId, projectId, materialIds, searchType, searchValue) => {
        let formData = new FormData();
        let data = {};
        if (action == "selectmaterial" || action == "selectmaterial2") {
            data = {
                "action": action,
                "servicetype": "pdamaterial",
                "express": "6DFD1C9B",
                "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
                "data": {
                    "factoryId": Url.PDAFid,
                    "roomId": roomId,
                    "projectId": projectId,
                    "materialIds": materialIds,
                    "searchType": searchType,
                    "keyStr": searchValue,
                    "pageNum": "1",
                    "viewRowCt": "10"
                }
            }
        } else if (action == "selectmaterialoutstoragereturn" || action == "selectmaterialconcreteoutstoragereturn") {
            data = {
                "action": action,
                "servicetype": "pdamaterial",
                "express": "6DFD1C9B",
                "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
                "data": {
                    "factoryId": Url.PDAFid,
                    "deptName": roomId,
                    "deptId": projectId,
                    "returnType": returnType,
                    "materialIds": materialIds,
                    "searchType": searchType,
                    "keyStr": searchValue,
                    "pageNum": "1",
                    "viewRowCt": "10"
                }
            }
        } else if (action == "selectmaterialinstorage") {
            data = {
                "action": action,
                "servicetype": "pdamaterial",
                "express": "6DFD1C9B",
                "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
                "data": {
                    "factoryId": Url.PDAFid,
                    "supplierId": roomId,
                    "materialIds": materialIds,
                    "searchType": searchType,
                    "keyStr": searchValue,
                    "pageNum": "1",
                    "viewRowCt": "10"
                }
            }
        } else if (action == "selectequipmaterial") {
            data = {
                "action": action,
                "servicetype": "pdamaterial",
                "express": "6DFD1C9B",
                "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
                "data": {
                    "factoryId": Url.PDAFid,
                    "roomId": roomId,
                    "materialIds": materialIds,
                    "searchType": searchType,
                    "keyStr": searchValue,
                    "pageNum": "1",
                    "viewRowCt": "10"
                }
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: SelectMaterial.js ~ line 130 ~ SelectMaterial ~ formData:", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res.statusText);
            //console.log(res);
            return res.json();
        }).then(resData => {
            if (resData.status == '100') {
                console.log("SelectMaterial ---- 146 ----" + JSON.stringify(resData.result))
                let res = resData.result
                res.map((item, index) => {
                    item.requisitionCount = "0",
                        item.select = false
                })
                listData = res
                this.setState({
                    resData: res,
                    isLoading: false
                })
                this.forceUpdate()
            } else {
                Alert.alert("错误提示", resData.message, [{
                    text: '取消',
                    style: 'cancel'
                }, {
                    text: '返回上一级',
                    onPress: () => {
                        this.props.navigation.goBack()
                    }
                }])
            }

        }).catch((error) => {
            console.log("🚀 ~ file: SelectMaterial.js ~ line 184 ~ SelectMaterial ~ error:", error)
        });

    }

    scanMaterial = (guid) => {
        if (typeof this.props.navigation.getParam('QRid') != "undefined") {
            if (this.props.navigation.getParam('QRid') != "") {
                this.toast.show(Url.isLoadingView, 0)
            }
        }
        let formData = new FormData();
        let data = {};
        let action = this.state.action
        let scanAction = "";
        if (action == '采购入库') scanAction = 'instoragescan'
        else if (action == '项目领料') scanAction = 'projectpickingscan'
        else if (action == '部门领料') scanAction = 'deptpickingscan'
        else if (action == '材料出库') scanAction = 'outstoragescan'
        else if (action == '材料退库' || action == '材料退库-砼笼材料') scanAction = 'outstoragereturnscan'
        else if (action == '设备点检' || action == '设备保养' || action == '设备维修') scanAction = 'selectequipmaterialscan'
        if (action == '采购入库' || action == '材料出库') {
            data = {
                "action": scanAction,
                "servicetype": "pdamaterial",
                "express": "6DFD1C9B",
                "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
                "data": {
                    "rowguid": guid
                }
            }
        } else if (action == '项目领料' || action == '部门领料') {
            data = {
                "action": scanAction,
                "servicetype": "pdamaterial",
                "express": "6DFD1C9B",
                "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
                "data": {
                    "factoryId": Url.PDAFid,
                    "rowguid": guid,
                    "roomId": this.state.roomId,
                    "projectId": this.state.projectId,
                    "materialIds": this.state.materialIds,
                }
            }
        } else if (action == '材料退库' || action == '材料退库-砼笼材料') {
            data = {
                "action": scanAction,
                "servicetype": "pdamaterial",
                "express": "6DFD1C9B",
                "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
                "data": {
                    "factoryId": Url.PDAFid,
                    "rowguid": guid,
                    "deptName": this.state.roomId,
                    "deptId": this.state.projectId,
                    "returnType": returnType,
                    "materialIds": this.state.materialIds,
                    "isConcrete": action == "材料退库" ? "false" : "true",
                }
            }
        } else if (action == '设备点检' || action == '设备保养' || action == '设备维修') {
            data = {
                "action": scanAction,
                "servicetype": "pdamaterial",
                "express": "6DFD1C9B",
                "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
                "data": {
                    "factoryId": Url.PDAFid,
                    "rowguid": guid,
                    "roomId": this.state.roomId,
                    "materialIds": this.state.materialIds,
                }
            }
        }

        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: SelectMaterial.js ~ line 319 ~ SelectMaterial ~ formData:", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res.statusText);
            //console.log(res);
            return res.json();
        }).then(resData => {
            this.toast.close()
            console.log("🚀 ~ file: SelectMaterial.js ~ line 319 ~ SelectMaterial ~ resData:", resData)
            if (resData.status == '100') {

                listData = resData.result
                this.setState({
                    resData: resData.result,
                    isScan: true,
                    dataCount: 0
                })
            } else {
                Alert.alert("错误提示", resData.message)
            }

        }).catch((error) => {
            console.log("🚀 ~ file: SelectMaterial.js ~ line 233 ~ SelectMaterial ~ error:", error)
        });
    }

    // 分类选择
    _selectType = (index, value) => {
        //console.log(index + '--' + value)
        const { roomId, projectId, materialIds, searchType, searchValue, action } = this.state;
        let type = ""
        switch (value) {
            case "材料编码": type = "materialCode"; break;
            case "材料名称": type = action == "部门领料" || action == "项目领料" ? "materialName" : "storageName"; break;
            case "规格型号": type = "materialSize"; break;
            default: type = "";
        }
        this.setState({
            searchType: type,
            selectType: value,
            dataCount: 0,
            isScan: false
        })
        this.resData(roomId, projectId, materialIds, type, searchValue)
    }
    // 下拉列表分隔符
    _separator = () => {
        return (
            <Text style={{ height: 0 }}></Text>
        )
    }

    renderSearchBar = () => {
        const { selectType, roomId, projectId, materialIds, action } = this.state;
        let type = ['材料编码', '材料名称', '规格型号']
        return (
            <View style={{
                flexDirection: 'row',
                alignItems: 'center',
                paddingTop: 10,
                paddingBottom: 10,
                backgroundColor: '#fff',
                borderBottomWidth: 1,
                borderBottomColor: '#efefef',
            }}>
                <View style={{ flex: 1 }}>
                    <View style={stylesSelectStock.searchContainerStyle}>

                        <View style={{ flexDirection: 'row', backgroundColor: '#f9f9f9', width: deviceWidth * 0.94, marginLeft: deviceWidth * 0.03, borderRadius: 45 }}>
                            <Text>   </Text>
                            <ModalDropdown
                                options={type}    //下拉内容数组
                                style={{ marginTop: 12 }}    //按钮样式
                                dropdownStyle={{ height: 36 * 3, width: 80 }}    //下拉框样式
                                dropdownTextStyle={{ fontSize: 13 }}    //下拉框文本样式
                                renderSeparator={this._separator}    //下拉框文本分隔样式
                                adjustFrame={this._adjustType}    //下拉框位置
                                dropdownTextHighlightStyle={{ color: 'rgba(42, 130, 228, 1)' }}    //下拉框选中颜色
                                onDropdownWillShow={() => { this.setState({ typeShow: true }) }}      //按下按钮显示按钮时触发
                                onDropdownWillHide={() => this.setState({ typeShow: false })}    //当下拉按钮通过触摸按钮隐藏时触发
                                onSelect={this._selectType}    //当选项行与选定的index 和 value 接触时触发
                                defaultValue={'材料编码'}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text>{this.state.selectType}</Text>
                                    <Icon name='caretdown' color='#419fff' iconStyle={{ fontSize: 13, marginTop: 1, marginLeft: 8, marginRight: -1 }} type='antdesign' ></Icon>
                                </View>
                            </ModalDropdown>

                            <Icon type='material-community' name='power-on' color='#999' style={{ marginTop: 10 }} />
                            <Icon name='search' color='#999' type='material' iconStyle={{ fontSize: 22, marginTop: 12 }}></Icon>

                            <TextInput style={stylesSelectStock.inputText}
                                keyboardType='web-search'
                                placeholder='搜索关键词'
                                value={this.state.searchValue}
                                onChangeText={(value) => {
                                    let type = ""
                                    switch (selectType) {
                                        case "材料编码": type = "materialCode"; break;
                                        case "材料名称": type = action == "部门领料" || action == "项目领料" ? "materialName" : "storageName"; break;
                                        case "规格型号": type = "materialSize"; break;
                                        default: type = ""; break;
                                    }
                                    this.setState({
                                        searchType: type,
                                        searchValue: value,
                                        dataCount: 0,
                                        isScan: false
                                    })
                                    this.resData(roomId, projectId, materialIds, type, value)
                                }}
                            />

                            <Icon type='material-community' name='qrcode-scan' color='#4D8EF5' iconStyle={{ marginTop: 12 }} onPress={() => {
                                setTimeout(() => {
                                    this.setState({ GetError: false, buttondisable: false })
                                    varGetError = false
                                }, 1500)
                                this.props.navigation.navigate('QRCode', {
                                    type: 'compid',
                                    page: 'SelectMaterial'
                                })
                            }} />
                            {/*<Icon type='material-community' name='microphone' color='#4D8EF5' style={{ marginTop: 12 }} />*/}
                            <Text>   </Text>
                        </View>



                    </View>
                </View>
            </View>
        );
    };

    ButtonTopScreen = () => {
        const { focusButton, resDataButton } = this.state;

        return (
            resDataButton.map((item, index) => {
                if (true) {
                    return (
                        <Button
                            //key={index}
                            buttonStyle={{
                                borderRadius: 20,
                                flex: 1,
                                backgroundColor: focusButton == index ? '#419FFF' : 'white',
                                width: 100
                            }}
                            containerStyle={{
                                marginHorizontal: 6,
                                borderRadius: 20,
                                marginTop: 20,
                            }}
                            title={item.name}
                            titleStyle={{ fontSize: 15, color: focusButton == index ? 'white' : '#419FFF' }}
                            //titleStyle={{ fontSize: 15, color: '#999' }}
                            //type='outline'
                            onPress={() => {
                                this.setState({
                                    focusButton: index,
                                });
                            }}
                        />
                    )
                }
            })
        )
    }


    render() {
        const { navigation } = this.props;
        //从主页面传url, 未来集成到一起
        const QRurl = navigation.getParam('QRurl') || '';
        const QRid = navigation.getParam('QRid') || ''
        const type = navigation.getParam('type') || '';
        let pageType = navigation.getParam('pageType') || ''

        if (type == 'compid') {
            QRcompid = QRid
        }
        const { isCheckPage, focusIndex, resData, isChecked, dataCount, totalAmount } = this.state

        if (this.state.isLoading) {
            return (
                <View style={styles.loading}>
                    <ActivityIndicator
                        animating={true}
                        color='#419FFF'
                        size="large" />
                </View>
            )
            //渲染页面
        } else {
            return (
                <View style={{ flex: 1, backgroundColor: '#f8f8f8' }}>
                    <Toast ref={(ref) => { this.toast = ref; }} position="center" />
                    <NavigationEvents
                        onWillFocus={this.UpdateControl}
                        onWillBlur={() => {
                            if (QRid != '') {
                                navigation.setParams({ QRid: '', type: '' })
                            }
                        }} />
                    {/*<View style={{ flex: 1 }}>*/}
                    {/*<ScrollView ref={ref => this.scoll = ref} style={styles.mainView}showsVerticalScrollIndicator={false} >*/}
                    {
                        this.renderSearchBar()
                    }

                    {/*<View style={{ flexDirection: 'row' }}>*/}
                    {/*    <ScrollView scrollEnabled={true} horizontal={true} showsHorizontalScrollIndicator={false}>*/}
                    {/*        {this.ButtonTopScreen()}*/}
                    {/*    </ScrollView>*/}
                    {/*<Button*/}
                    {/*    type='outline'*/}
                    {/*    title='更多'*/}
                    {/*    titleStyle={{ fontSize: 15, }}*/}
                    {/*    buttonStyle={{*/}
                    {/*        borderRadius: 100,*/}
                    {/*        flex: 1*/}
                    {/*    }}*/}
                    {/*    containerStyle={{*/}
                    {/*        //marginHorizontal: 6,*/}
                    {/*        borderRadius: 100,*/}
                    {/*        marginTop: 20,*/}
                    {/*    }}*/}
                    {/*    onPress={() => {*/}
                    {/*        this.setState({*/}

                    {/*        })*/}
                    {/*    }} />*/}
                    {/*</View>*/}

                    <FlatList
                        style={{ flex: 1, borderRadius: 10, backgroundColor: '#f8f8f8' }}
                        ref={(flatList) => this._flatList1 = flatList}
                        //ItemSeparatorComponent={this._separator}
                        renderItem={this._renderItem}
                        onRefresh={this.refreshing}
                        refreshing={false}
                        //控制上拉加载，两个平台需要区分开
                        onEndReachedThreshold={(Platform.OS === 'ios') ? -0.3 : 0.1}
                        onEndReached={() => {
                            this._onload()
                        }
                        }
                        numColumns={1}
                        //ListFooterComponent={this._footer}
                        //columnWrapperStyle={{borderWidth:2,borderColor:'black',paddingLeft:20}}
                        //horizontal={true}
                        data={listData}
                        scrollEnabled={true}
                        extraData={this.state}
                    />

                    <View style={{
                        marginHorizontal: width * 0.06,
                        marginTop: width * 0.04,
                        backgroundColor: 'white',
                        borderRadius: 10,
                        flexDirection: 'row',
                        justifyContent: 'flex-start',
                        alignItems: 'center',
                    }}>
                        <CheckBoxScan
                            //title='全选'
                            checked={isChecked}
                            onPress={() => {
                                let totalAmount = this.state.totalAmount
                                listData.map((item, index) => {
                                    item.select = !isChecked
                                    if (this.state.action == '材料退库' || this.state.action == '材料退库-砼笼材料') {
                                        let data = (Math.round((Number.parseFloat(item.unitPrice) * Number.parseFloat(item.canOutNumber)) * 100) / 100).toString()

                                        if (item.select) {
                                            totalAmount = (Math.round((Number.parseFloat(totalAmount) + Number.parseFloat(data)) * 100) / 100).toString()
                                        } else {
                                            totalAmount = (Math.round((Number.parseFloat(totalAmount) - Number.parseFloat(data)) * 100) / 100).toString()
                                        }
                                    }
                                })
                                this.setState({
                                    dataCount: !isChecked ? listData.length : 0,
                                    isChecked: !isChecked,
                                    totalAmount: totalAmount
                                })
                            }}
                        />
                        <View style={{ flex: 1 }}>
                            <ListItemScan focusStyle={focusIndex == '3' ? styles.focusColor : {}} title={"全选"} rightElement={"已选" + dataCount + "条"} />
                        </View>
                    </View>

                    <FormButton
                        backgroundColor='#17BC29'
                        onPress={() => {
                            let res = []
                            listData.map((item, idex) => {
                                if (item.select) {
                                    if (this.state.action == '采购入库') {
                                        //item.inStorageAmount = item.canInStorageAmount
                                    } else if (this.state.action == '材料退库' || this.state.action == '材料退库-砼笼材料') {
                                        //item.outNumber = item.canOutNumber
                                        item.price = (Math.round((Number.parseFloat(item.canOutNumber) * Number.parseFloat(item.unitPrice)) * 100) / 100).toString()
                                    }
                                    res.push(item)
                                }
                            })
                            let navAction = ''
                            if (this.state.action == '采购入库') navAction = 'MaterialInStorage'
                            else if (this.state.action == '设备点检') navAction = 'EquipmentInspection'
                            else if (this.state.action == '设备保养') navAction = 'EquipmentMaintenance'
                            else if (this.state.action == '设备维修') navAction = 'EquipmentService'
                            else if (this.state.action == '部门领料' || this.state.action == '项目领料') navAction = 'MaterialProjectPicking'
                            else if (this.state.action == '材料退库' || this.state.action == '材料退库-砼笼材料') navAction = 'MaterialOutStorageReturn'

                            this.props.navigation.navigate(navAction, {
                                type: "",
                                state: "addMaterial",
                                materialInfos: res,
                                totalAmount: totalAmount
                            })
                        }}
                        title='确定'
                    />

                    {/*</ScrollView>*/}
                    {/*</View>*/}
                </View>
            )
        }
    }

    _onload = (user) => {
        const { roomId, projectId, supplierId, materialIds, searchType, searchValue, isScan } = this.state
        if (isScan) return
        this.toast.show('加载中...', 10000)
        pageNum += 1

        let formData = new FormData();
        let data = {};
        if (action == "selectmaterial" || action == "selectmaterial2") {
            data = {
                "action": action,
                "servicetype": "pdamaterial",
                "express": "6DFD1C9B",
                "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
                "data": {
                    "factoryId": Url.PDAFid,
                    "roomId": roomId,
                    "projectId": projectId,
                    "materialIds": materialIds,
                    "searchType": searchType,
                    "keyStr": searchValue,
                    "pageNum": pageNum,
                    "viewRowCt": "10"
                }
            }
        } else if (action == "selectmaterialoutstoragereturn" || action == "selectmaterialconcreteoutstoragereturn") {
            data = {
                "action": action,
                "servicetype": "pdamaterial",
                "express": "6DFD1C9B",
                "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
                "data": {
                    "factoryId": Url.PDAFid,
                    "deptName": roomId,
                    "deptId": projectId,
                    "returnType": returnType,
                    "materialIds": materialIds,
                    "searchType": searchType,
                    "keyStr": searchValue,
                    "pageNum": pageNum,
                    "viewRowCt": "10"
                }
            }
        } else if (action == "selectmaterialinstorage") {
            data = {
                "action": action,
                "servicetype": "pdamaterial",
                "express": "6DFD1C9B",
                "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
                "data": {
                    "factoryId": Url.PDAFid,
                    "supplierId": supplierId,
                    "materialIds": materialIds,
                    "searchType": searchType,
                    "keyStr": searchValue,
                    "pageNum": pageNum,
                    "viewRowCt": "10"
                }
            }
        } else if (action == "selectequipmaterial") {
            data = {
                "action": action,
                "servicetype": "pdamaterial",
                "express": "6DFD1C9B",
                "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
                "data": {
                    "factoryId": Url.PDAFid,
                    "roomId": roomId,
                    "materialIds": materialIds,
                    "searchType": searchType,
                    "keyStr": searchValue,
                    "pageNum": pageNum,
                    "viewRowCt": "10"
                }
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        //console.log("🚀 ~ file: SelectMaterial.js ~ line 130 ~ SampleManage ~ formData:", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res.statusText);
            //console.log(res);
            return res.json();
        }).then(resData => {
            if (resData.status == '100') {
                //console.log("SelectMaterial ---- 146 ----" + JSON.stringify(resData.result))
                let res = resData.result
                res.map((item, index) => {
                    item.requisitionCount = "0",
                        item.select = false
                })
                listData.push.apply(listData, res)
                //listData = res
                this.setState({
                    resData: listData,
                    isLoading: false
                })
                if (res.length > 0 || res.length > 0) {
                    this.toast.show('加载完成');
                } else {
                    this.toast.show('无更多数据');
                }
            } else if (resData.status != '100') {
                Alert.alert('错误', resData.message)
            } else {
                this.toast.show('加载失败')
            }

        }).catch((error) => {
            console.log("🚀 ~ file: SelectMaterial.js ~ line 184 ~ SelectMaterial ~ error:", error)
        });
    }

    refreshing = () => {
        this.toast.show('刷新中...', 10000)
        this.setState({
            pageLoading: true,
            isScan: false
        })
        const { roomId, projectId, supplierId, materialIds, searchType, searchValue } = this.state
        let formData = new FormData();
        let data = {};
        if (action == "selectmaterial" || action == "selectmaterial2") {
            data = {
                "action": action,
                "servicetype": "pdamaterial",
                "express": "6DFD1C9B",
                "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
                "data": {
                    "factoryId": Url.PDAFid,
                    "roomId": roomId,
                    "projectId": projectId,
                    "materialIds": materialIds,
                    "searchType": searchType,
                    "keyStr": searchValue,
                    "pageNum": "1",
                    "viewRowCt": "10"
                }
            }
        } else if (action == "selectmaterialoutstoragereturn" || action == "selectmaterialconcreteoutstoragereturn") {
            data = {
                "action": action,
                "servicetype": "pdamaterial",
                "express": "6DFD1C9B",
                "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
                "data": {
                    "factoryId": Url.PDAFid,
                    "deptName": roomId,
                    "deptId": projectId,
                    "returnType": returnType,
                    "materialIds": materialIds,
                    "searchType": searchType,
                    "keyStr": searchValue,
                    "pageNum": "1",
                    "viewRowCt": "10"
                }
            }
        } else if (action == "selectmaterialinstorage") {
            data = {
                "action": action,
                "servicetype": "pdamaterial",
                "express": "6DFD1C9B",
                "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
                "data": {
                    "factoryId": Url.PDAFid,
                    "supplierId": supplierId,
                    "materialIds": materialIds,
                    "searchType": searchType,
                    "keyStr": searchValue,
                    "pageNum": "1",
                    "viewRowCt": "10"
                }
            }
        } else if (action == "selectequipmaterial") {
            data = {
                "action": action,
                "servicetype": "pdamaterial",
                "express": "6DFD1C9B",
                "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
                "data": {
                    "factoryId": Url.PDAFid,
                    "roomId": roomId,
                    "materialIds": materialIds,
                    "searchType": searchType,
                    "keyStr": searchValue,
                    "pageNum": "1",
                    "viewRowCt": "10"
                }
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        //console.log("🚀 ~ file: SelectMaterial.js ~ line 130 ~ SampleManage ~ formData:", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res.statusText);
            //console.log(res);
            return res.json();
        }).then(resData => {
            if (resData.status == '100') {
                //console.log("SelectMaterial ---- 146 ----" + JSON.stringify(resData.result))
                let res = resData.result
                res.map((item, index) => {
                    item.requisitionCount = "0",
                        item.select = false
                })
                //listData.push.apply(listData, res)
                listData = res
                this.setState({
                    resData: listData,
                    isLoading: false
                })
            } else {
                this.toast.show('刷新失败', 1600)
            }

        }).catch((error) => {
            console.log("🚀 ~ file: SelectMaterial.js ~ line 184 ~ SelectMaterial ~ error:", error)
        });
    }

    _renderItem = ({ item, index }) => {
        const { focusIndex, dataCount, action } = this.state
        let count = dataCount
        return (
            <TouchableCustom onPress={() => {
                item.select = !item.select
                count = item.select ? count + 1 : count - 1
                this.setState({
                    dataCount: count
                })

                if (action == '材料退库' || action == '材料退库-砼笼材料') {
                    let data = (Math.round((Number.parseFloat(item.unitPrice) * Number.parseFloat(item.canOutNumber)) * 100) / 100).toString()
                    let totalAmount = this.state.totalAmount
                    if (item.select) {
                        totalAmount = (Math.round((Number.parseFloat(totalAmount) + Number.parseFloat(data)) * 100) / 100).toString()
                    } else {
                        totalAmount = (Math.round((Number.parseFloat(totalAmount) - Number.parseFloat(data)) * 100) / 100).toString()
                    }
                    this.setState({
                        totalAmount: totalAmount
                    })
                }
            }}>
                <View>
                    <Card containerStyle={{ borderRadius: 10, elevation: 0 }}>

                        <View style={[{
                            flexDirection: 'row',
                            justifyContent: 'flex-start',
                            alignItems: 'center'
                        }]}>
                            <View><Text>{index + 1}</Text></View>
                            <CheckBoxScan checked={item.select} onPress={() => {
                                item.select = !item.select
                                count = item.select ? count + 1 : count - 1
                                this.setState({
                                    dataCount: count
                                })

                                if (action == '材料退库' || action == '材料退库-砼笼材料') {
                                    let data = (Math.round((Number.parseFloat(item.unitPrice) * Number.parseFloat(item.canOutNumber)) * 100) / 100).toString()
                                    let totalAmount = this.state.totalAmount
                                    if (item.select) {
                                        totalAmount = (Math.round((Number.parseFloat(totalAmount) + Number.parseFloat(data)) * 100) / 100).toString()
                                    } else {
                                        totalAmount = (Math.round((Number.parseFloat(totalAmount) - Number.parseFloat(data)) * 100) / 100).toString()
                                    }
                                    this.setState({
                                        totalAmount: totalAmount
                                    })
                                }
                                //this.forceUpdate()
                            }} />

                            <View style={{ flex: 1 }}>
                                <ListItem
                                    containerStyle={stylesSelectStock.list_container_style}
                                    title={item.code}
                                    //rightTitle={'单价:' + item.unitPrice + '元'}
                                    rightElement={
                                        <View style={{
                                            backgroundColor:
                                                item.type == '直接材料' || item.type == '1' ? '#31C731' :
                                                    item.type == '小五金' || item.type == '2' ? '#DAC512' :
                                                        item.type == '甲供材料' || item.type == '3' ? '#4D8EF5' : 'transparent'
                                        }}>
                                            <Text style={{ color: 'white', fontSize: 13 }}> {
                                                item.type == '1' ? '直接材料' : item.type == '2' ? '小五金' : item.type == '3' ? '甲供材料' : item.type
                                            } </Text>
                                        </View>
                                    }
                                    titleStyle={stylesSelectStock.title}
                                    rightTitleStyle={stylesSelectStock.rightTitle}
                                />
                                <ListItem
                                    containerStyle={stylesSelectStock.list_container_style}
                                    title={item.name}
                                    //rightTitle={'单价:' + item.unitPrice + '元'}
                                    rightElement={
                                        action == '采购入库' || action == '材料退库' || action == '材料退库-砼笼材料' || action == '设备点检' || action == '设备保养' || action == '设备维修' ? <View /> :
                                            <View style={{ flexDirection: 'row' }}>
                                                <Text style={{ fontSize: 13, color: '#999' }}>库存数量:</Text>
                                                <Text style={{ fontSize: 13, color: '#4D8EF5' }}>{item.stockCount}</Text>
                                            </View>
                                    }
                                    titleStyle={stylesSelectStock.title}
                                    rightTitleStyle={stylesSelectStock.rightTitle}
                                />
                                <ListItem
                                    containerStyle={stylesSelectStock.list_container_style}
                                    title={item.size}
                                    //rightTitle={'单价:' + item.unitPrice + '元'}
                                    rightElement={
                                        action == '材料退库' || action == '材料退库-砼笼材料' ?
                                            <View style={{ flexDirection: 'row' }}>
                                                <Text style={{ fontSize: 13, color: '#999' }}>可退库量:</Text>
                                                <Text style={{ fontSize: 13, color: '#4D8EF5' }}>{item.canOutNumber}</Text>
                                            </View>
                                            : action == '采购入库' ? <View />
                                                : action == '设备点检' || action == '设备保养' || action == '设备维修' ?
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <Text style={{ fontSize: 13, color: '#999' }}>可用库存:</Text>
                                                        <Text style={{ fontSize: 13, color: '#4D8EF5' }}>{item.stockCount}</Text>
                                                    </View>
                                                    :
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <Text style={{ fontSize: 13, color: '#999' }}>可用库存:</Text>
                                                        <Text style={{ fontSize: 13, color: '#4D8EF5' }}>{item.stockCount2}</Text>
                                                    </View>
                                    }
                                    titleStyle={stylesSelectStock.title}
                                    rightTitleStyle={stylesSelectStock.rightTitle}
                                />
                                <ListItem
                                    containerStyle={stylesSelectStock.list_container_style}
                                    title={item.unit}
                                    //rightTitle={'单价:' + item.unitPrice + '元'}
                                    rightElement={
                                        action == '项目领料' ?
                                            <View style={{ flexDirection: 'row' }}>
                                                <Text style={{ fontSize: 13, color: '#999' }}>项目累计领用量:</Text>
                                                <Text style={{ fontSize: 13, color: '#4D8EF5' }}>{item.accuactualcount}</Text>
                                            </View>
                                            : <View />
                                    }
                                    titleStyle={stylesSelectStock.title}
                                    rightTitleStyle={stylesSelectStock.rightTitle}
                                />
                            </View>

                        </View>

                    </Card>
                </View>
            </TouchableCustom>
        )
    }
}

const stylesSelectStock = StyleSheet.create({
    searchContainerStyle: {
        shadowColor: '#999', shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.3,
        shadowRadius: 6
    },
    inputText: {
        flex: 1,
        backgroundColor: 'transparent',
        fontSize: 15,
    },
    listView: {
        marginHorizontal: width * 0.06,
        marginTop: width * 0.04,
        paddingHorizontal: 0,
        paddingTop: 5,
        paddingBottom: 15,
        backgroundColor: 'white',
        borderRadius: 10
    },
    card_containerStyle: {
        borderRadius: 10,
        shadowOpacity: 0,
        backgroundColor: '#f8f8f8',
        borderWidth: 0,
        paddingVertical: 13,
        elevation: 0
    },
    list_container_style: { marginVertical: 0, paddingVertical: 2, backgroundColor: 'transparent' },
    title: {
        fontSize: 13,
        //color: '#999'
    },
    rightTitle: {
        fontSize: 13,
        textAlign: 'right',
        lineHeight: 14,
        flexWrap: 'wrap',
        width: width / 2,
        color: '#333'
    },
});
