import React from 'react';
import { View, StatusBar, TouchableOpacity, TouchableHighlight, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, Icon, Input, Badge, Card, ListItem, BottomSheet, CheckBox, Avatar, Overlay, Header } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { RFT } from '../../Url/Pixal';
import { ScrollView } from 'react-native-gesture-handler';
import CardList from "../Componment/CardList";
import DatePicker from 'react-native-datepicker'
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { Image } from 'react-native';
import { NavigationEvents } from 'react-navigation';
import { styles } from '../Componment/PDAStyles';
import ScanButton from '../Componment/ScanButton';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import ListItemScan_child from '../Componment/ListItemScan_child';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import CheckBoxScan from '../Componment/CheckBoxScan';
import AvatarAdd from '../Componment/AvatarAdd';
import AvatarImg from '../Componment/AvatarImg';
import DeviceStorage from '../../Url/DeviceStorage';
import Pdf from 'react-native-pdf';
import BatchListItem from '../Componment/BatchBottomItem';

import { DeviceEventEmitter, NativeModules } from 'react-native';
import overlayImg from '../Componment/OverlayImg';
import OverlayImgZoomPicker from '../Componment/OverlayImgZoomPicker';
let fileflag = "9"

let varGetError = false

const { height, width } = Dimensions.get('window') //获取宽高

let QRcompid = '', QRtaiwanId = '', QRacceptanceId = '', QRmonitorId = '', QRInspectorId = ''

let imageArr = [], imageFileArr = [];

let DeviceStorageData = {} //缓存数据

class BackIcon extends React.PureComponent {
  render() {
    return (
      <TouchableCustom
        onPress={this.props.onPress} >
        <Icon
          name='arrow-back'
          color='#419FFF' />
      </TouchableCustom>
    )
  }
}

export default class HideInspection extends React.PureComponent {
  constructor() {
    super();
    this.state = {
      resData: [],
      rowguid: '',
      type: 'compid',
      ServerTime: '2000-01-01 00:00',
      steelCageIDs: ' ',
      monitorSetup: '',
      monitors: [],
      monitorId: '',
      monitorName: '',
      monitorSelect: '请选择',
      monitorTeamId: '',
      monitorTeamName: '',
      Inspectors: [],
      InspectorId: '',
      InspectorName: '',
      InspectorSelect: '请选择',
      acceptanceId: '',
      acceptanceName: '',
      checkResult: '合格',
      taiwanId: '',
      compData: {},
      compId: '',
      steelLabel: '',
      compCode: '',
      Tai_QRname: '',
      editEmployeeName: '',
      checked: true,
      //是否扫码成功
      isGetcomID: false,
      isGettaiwanId: false,
      isGetTeamPeopleInfo: false,
      isGetCommonMould: false,
      isGetInspector: false,
      //长按删除功能控制
      iscomIDdelete: false,
      istaiwanIddelete: false,
      isCommonMoulddelete: false,
      isTeamPeopledelete: false,
      isInspectordelete: false,
      //底栏控制
      bottomData: [],
      bottomVisible: false,
      datepickerVisible: false,
      CameraVisible: false,
      response: '',
      //照片控制
      pictureSelect: false,
      pictureUri: '',
      imageArr: [],
      imageFileArr: [],
      imageOverSize: false,
      fileid: "",
      recid: "",
      isPhotoUpdate: false,
      //
      GetError: false,
      buttondisable: false,
      //钢筋笼控制
      steelCageID: '',
      steelCageCode: '',
      isGetsteelCageInfoforPutMold: false,
      issteelCageIDdelete: false,
      focusIndex: 0,
      isLoading: true,
      //图纸
      isPaperTypeVisible: false,
      paperList: ["模版图", "配筋图", "预埋件装配图", "连接件排版图"],
      paperUrl: "",
      FileName: "",
      isPDFVisible: false,
      //查询界面取消选择按钮
      isCheck: false,
      isCheckPage: false,
      remark: '',
      //批量
      isBatch: false,
      listBatchNoInfo: [],
      batchCompCode: '',
      batchCompCodeselect: '请选择',
      ProductionQuantity: '',
      hideCheckNum: '',
      productionNum: '',
      isUsedCageBusiness: '',
      //控制app拍照，日期，相册选择功能
      isAppPhotoSetting: false,
      currShowImgIndex: 0,
      isAppDateSetting: false,
      isAppPhotoAlbumSetting: false
    }
    //this.requestCameraPermission = this.requestCameraPermission.bind(this)
  }

  componentDidMount() {
    StatusBar.setBarStyle('dark-content')
    const { navigation } = this.props;
    let guid = navigation.getParam('guid') || ''
    let isBatch = navigation.getParam('isBatch') || ''
    let pageType = navigation.getParam('pageType') || ''
    let isAppPhotoSetting = navigation.getParam('isAppPhotoSetting')
    let isAppDateSetting = navigation.getParam('isAppDateSetting')
    let isAppPhotoAlbumSetting = navigation.getParam('isAppPhotoAlbumSetting')
    this.setState({
      isAppPhotoSetting: isAppPhotoSetting,
      isAppDateSetting: isAppDateSetting,
      isAppPhotoAlbumSetting: isAppPhotoAlbumSetting
    })
    if (pageType == 'CheckPage') {
      if (isBatch) {
        this.setState({ isBatch: isBatch })
        this.batchCheckResData(guid)
      } else {
        this.checkResData(guid)
      }
      this.setState({ isCheckPage: true })
    } else if (pageType == 'BatchScan') {
      this.batchResData()
      DeviceStorage.get('DeviceStorageDataQI')
        .then(res => {
          if (res) {
            console.log("🚀 ~ file: qualityinspection.js ~ line 133 ~ QualityInspection ~ componentDidMount ~ res", res)
            let DeviceStorageDataObj = JSON.parse(res)
            DeviceStorageData = DeviceStorageDataObj
            console.log("🚀 ~ file: qualityinspection.js ~ line 136 ~ QualityInspection ~ componentDidMount ~ DeviceStorageData", DeviceStorageData)
            if (res.length != 0) {
              this.setState({
                "isGetInspector": DeviceStorageDataObj.isGetInspector,
                "isGetTeamPeopleInfo": DeviceStorageDataObj.isGetTeamPeopleInfo,
                "InspectorId": DeviceStorageDataObj.InspectorId,
                "InspectorName": DeviceStorageDataObj.InspectorName,
                "InspectorSelect": DeviceStorageDataObj.InspectorName,
                "monitorId": DeviceStorageDataObj.monitorId,
                "monitorName": DeviceStorageDataObj.monitorName,
                "monitorSelect": DeviceStorageDataObj.monitorName,
                "monitorTeamName": DeviceStorageDataObj.monitorTeamName,
              })
            }
          }
        }).catch(err => {
          console.log("🚀 ~ file: loading.js ~ line 131 ~ SteelCage ~ componentDidMount ~ err", err)

        })
    } else {
      this.resData()
      DeviceStorage.get('DeviceStorageDataQI')
        .then(res => {
          if (res) {
            console.log("🚀 ~ file: qualityinspection.js ~ line 133 ~ QualityInspection ~ componentDidMount ~ res", res)
            let DeviceStorageDataObj = JSON.parse(res)
            DeviceStorageData = DeviceStorageDataObj
            console.log("🚀 ~ file: qualityinspection.js ~ line 136 ~ QualityInspection ~ componentDidMount ~ DeviceStorageData", DeviceStorageData)
            if (res.length != 0) {
              this.setState({
                "isGetInspector": DeviceStorageDataObj.isGetInspector,
                "isGetTeamPeopleInfo": DeviceStorageDataObj.isGetTeamPeopleInfo,
                "InspectorId": DeviceStorageDataObj.InspectorId,
                "InspectorName": DeviceStorageDataObj.InspectorName,
                "InspectorSelect": DeviceStorageDataObj.InspectorName,
                "monitorId": DeviceStorageDataObj.monitorId,
                "monitorName": DeviceStorageDataObj.monitorName,
                "monitorSelect": DeviceStorageDataObj.monitorName,
                "monitorTeamName": DeviceStorageDataObj.monitorTeamName,
              })
            }
          }
        }).catch(err => {
          console.log("🚀 ~ file: loading.js ~ line 131 ~ SteelCage ~ componentDidMount ~ err", err)

        })
    }

    //通过使用DeviceEventEmitter模块来监听事件
    this.iDataScan = DeviceEventEmitter.addListener('iDataScan', (Event) => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 115 ~ SteelCage ~ Event.ScanResult", Event.ScanResult)
      if (typeof Event.ScanResult != undefined) {
        let data = Event.ScanResult
        let arr = data.split("=");
        let id = ''
        id = arr[arr.length - 1]
        console.log("🚀 ~ file: concrete_pouring.js ~ line 118 ~ SteelCage ~ id", id)
        if (this.state.type == 'compid') {
          this.GetcomID(id)
        } else if (this.state.type == 'taiwanId') {
          this.GettaiwanId(id)
        } else if (this.state.type == 'acceptanceId') {
          this.GetCommonMould(id)
        } else if (this.state.type == 'monitorId') {
          this.GetTeamPeopleInfo(id)
        } else if (this.state.type == 'InspectorId') {
          this.GetInspector(id)
        } else if (this.state.type == 'steelCageID') {
          this.GetsteelCageInfoforPutMold(id)
        }

      }
    });
  }

  componentWillUnmount() {
    imageArr = [], imageFileArr = [];
    this.iDataScan.remove()
  }

  UpdateControl = () => {
    if (typeof this.props.navigation.getParam('QRid') != "undefined") {
      if (this.props.navigation.getParam('QRid') != "") {
        this.toast.show(Url.isLoadingView, 0)
      }
    }
    const { navigation } = this.props;
    const { isGetcomID, isGetCommonMould, isGetInspector, isGetTeamPeopleInfo, isGettaiwanId, isGetsteelCageInfoforPutMold } = this.state
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || ''
    console.log("🚀 ~ file: qualityinspection.js ~ line 11113 ~ SteelCage ~ componentDidUpdate ~ QRid", QRid)
    let type = navigation.getParam('type') || '';
    console.log("🚀 ~ file: qualityinspection.js ~ line 105 ~ SteelCage ~ componentDidUpdate ~ this.state.GetError", this.state.GetError)
    if (type == 'compid') {
      this.GetcomID(QRid)
    } else if (type == 'taiwanId') {
      this.GettaiwanId(QRid)
    } else if (type == 'acceptanceId') {
      this.GetCommonMould(QRid)
    } else if (type == 'monitorId') {
      this.GetTeamPeopleInfo(QRid)
    } else if (type == 'InspectorId') {
      this.GetInspector(QRid)
    } else if (type == 'steelCageID') {
      this.GetsteelCageInfoforPutMold(QRid)
    }
    if (QRid != '') {
      navigation.setParams({ QRid: '', type: '' })
    }
    console.log("🚀 ~ file: qualityinspection.js ~ line 11147 ~ SteelCage ~ componentDidUpdate ~ QRid", QRid)

  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  checkResData = (guid) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "LookHideCheck",
      "servicetype": "pda",
      "express": "8AC4C0B0",
      "ciphertext": "7df83f712027c63f147f6c2d4a43f08d",
      "data": {
        "guid": guid,
        "factoryId": Url.PDAFid,
        "merge": "0"
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: qualityinspection.js ~ line 189 ~ HideInspection ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 204 ~ HideInspection ~ resData", resData)
      let rowguid = resData.result.guid
      let ServerTime = resData.result.time
      let compCode = resData.result.compCode
      let compId = resData.result.compId
      let projectName = resData.result.projectName
      let compTypeName = resData.result.compTypeName
      let designType = resData.result.designType
      let floorName = resData.result.floorName
      let floorNoName = resData.result.floorNoName
      let volume = resData.result.volume
      let weight = resData.result.weight
      let Inspector = resData.result.Inspector
      let monitor = resData.result.monitor
      let remark = resData.result.remark
      let componentImageUrl = resData.result.componentImageUrl
      let imageArr = [], imageFileArr = [];
      let tmp = {}
      tmp.fileid = ""
      tmp.uri = componentImageUrl
      tmp.url = componentImageUrl
      imageFileArr.push(tmp)
      this.setState({
        imageFileArr: imageFileArr
      })
      imageArr.push(componentImageUrl)
      //产品子表
      let compData = {}, result = {}
      result.projectName = projectName
      result.compTypeName = compTypeName
      result.designType = designType
      result.floorNoName = floorNoName
      result.floorName = floorName
      result.volume = volume
      result.weight = weight
      compData.result = result
      this.setState({
        resData: resData,
        rowguid: rowguid,
        ServerTime: ServerTime,
        compCode: compCode,
        compId: compId,
        compData: compData,
        monitorName: monitor,
        monitorSelect: monitor,
        Inspector: Inspector,
        InspectorSelect: Inspector,
        remark: remark,
        pictureSelect: true,
        imageArr: imageArr,
        isGetcomID: true,
        isGettaiwanId: true,
        isGetTeamPeopleInfo: true,
        isGetCommonMould: true,
        isGetInspector: true,
        isCheck: true
      })
      this.setState({
        isLoading: false,
      })
    }).catch((error) => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 215 ~ HideInspection ~ error", error)
    });
  }

  batchCheckResData = (guid) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getbatchcomplexhidecheckdetail",
      "servicetype": "batchproduction",
      "express": "F7C40539",
      "ciphertext": "1869533ff218a4b43db6c19163bdc927",
      "data": { "_Rowguid": guid }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: qualityinspection.js ~ line 189 ~ HideInspection ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 204 ~ HideInspection ~ resData", resData)
      let comp = resData.result.Detail[0]
      let rowguid = comp._Rowguid
      let ServerTime = comp.productionTime
      let compCode = comp.compCode
      let compId = comp._Rowguid
      let projectName = comp.projectName
      let compTypeName = comp.compTypeName
      let designType = comp.designType
      let floorName = comp.floorName
      let floorNoName = comp.floorNoName
      let volume = comp.volume
      let weight = comp.weight
      let Inspector = comp.inspector
      let monitor = comp.teamleader
      let teamName = comp.teamName
      let productionQuantity = comp.productionQuantity
      let hideCheckCount = comp.hideCheckCount
      let remark = comp.remark

      //产品子表
      let compData = {}, result = {}
      result.projectName = projectName
      result.compTypeName = compTypeName
      result.designType = designType
      result.floorNoName = floorNoName
      result.floorName = floorName
      result.volume = volume
      result.weight = weight
      compData.result = result
      this.setState({
        resData: resData,
        rowguid: rowguid,
        ServerTime: ServerTime,
        compCode: compCode,
        batchCompCodeselect: compCode,
        ProductionQuantity: productionQuantity,
        hideCheckNum: hideCheckCount,
        productionNum: productionQuantity,
        compId: compId,
        compData: compData,
        monitorName: monitor,
        monitorSelect: monitor,
        monitorTeamName: teamName,
        Inspector: Inspector,
        InspectorSelect: Inspector,
        remark: remark,
        pictureSelect: true,
        imageArr: imageArr,
        isGetcomID: true,
        isGettaiwanId: true,
        isGetTeamPeopleInfo: true,
        isGetCommonMould: true,
        isGetInspector: true,
        isCheck: true
      })
      this.setState({
        isLoading: false,
      })
    }).catch((error) => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 215 ~ HideInspection ~ error", error)
    });
  }

  resData = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "complexhidecheckinit",
      "servicetype": "pda",
      "express": "8AC4C0B0",
      "ciphertext": "7df83f712027c63f147f6c2d4a43f08d",
      "data": {
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: hideinspection.js ~ line 445 ~ HideInspection ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      //初始化错误提示
      if (resData.status == 100) {
        let rowguid = resData.result.rowguid
        let ServerTime = resData.result.serverTime
        let monitorSetup = resData.result.monitorSetup
        if (monitorSetup != '扫码') {
          this.setState({
            isGetTeamPeopleInfo: true,
            isGetInspector: true,
          })
        }
        let monitors = resData.result.monitors
        let Inspectors = resData.result.Inspectors
        this.setState({
          resData: resData,
          rowguid: rowguid,
          ServerTime: ServerTime,
          monitorSetup: monitorSetup,
          monitors: monitors,
          Inspectors: Inspectors,
          isLoading: false
        })
      }
      else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }

    }).catch((error) => {
    });

  }

  batchResData = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getexpectantbatchno",
      "servicetype": "batchproduction",
      "express": "588DF83A",
      "ciphertext": "ba3f26d8fd1c527a0bc4362f761eab2e",
      "data": { "_bizid": Url.PDAFid }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: steel_cage_storage.js ~ line 35 ~ SteelCage ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      //初始化错误提示
      if (resData.status == 100) {
        let rowguid = resData.result.rowguid
        let ServerTime = resData.result.serverTime
        let monitorSetup = resData.result.monitorSetup
        if (monitorSetup != '扫码') {
          this.setState({
            isGetTeamPeopleInfo: true,
            isGetInspector: true,
          })
        }
        let listBatchNoInfo = resData.result.listBatchNoInfo
        let monitors = resData.result.monitors
        let Inspectors = resData.result.Inspectors
        let isUsedCageBusiness = resData.result.isUsedCageBusiness
        this.setState({
          resData: resData,
          rowguid: rowguid,
          ServerTime: ServerTime,
          monitorSetup: monitorSetup,
          listBatchNoInfo: listBatchNoInfo,
          monitors: monitors,
          Inspectors: Inspectors,
          isUsedCageBusiness: isUsedCageBusiness,
          isBatch: true,
          isLoading: false
        })
      }
      else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }

    }).catch((error) => {
      console.log("🚀 ~ file: steel_cage_storage.js ~ line 75 ~ SteelCage ~ error", error)
    });

  }

  GetcomID = (id) => {
    console.log("🚀 ~ file: qualityinspection.js ~ line 194 ~ SteelCage ~ GetcomID", id)
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getCompInfoFromProductStart",
      "servicetype": "pda",
      "express": "0F51EF23",
      "ciphertext": "a91d6dc5c5a20f2a35939b6e2d91a0ef",
      "data": {
        "compId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: qualityinspection.js ~ line 367 ~ HideInspection ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let number = resData.result.number
        let compCode = resData.result.compCode
        let compId = resData.result.compId
        if (number != 0) {
          Alert.alert(
            '提示信息',
            '重复隐检构件,是否再次隐检?',
            [{
              text: '取消',
              style: 'cancel'
            }, {
              text: '确定',
              onPress: () => {
                this.setState({
                  compData: resData,
                  compCode: compCode,
                  compId: compId,
                  isGetcomID: true,
                  iscomIDdelete: false
                })
              }
            }])
        } else {
          this.setState({
            compData: resData,
            compCode: compCode,
            compId: compId,
            isGetcomID: true,
            iscomIDdelete: false
          })
        }
        this.setState({
          focusIndex: 6,
          type: "monitorId"
        })
        setTimeout(() => { this.scoll.scrollToEnd() }, 300)
      } else {
        console.log('resData', resData)
        Alert.alert('错误', resData.message)
        this.setState({
          compId: ""
        })
        //this.input1.focus()
      }

    }).catch(err => {
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  comIDItem = (index) => {
    const { isGetcomID, buttondisable, compCode, compId } = this.state
    return (
      <View>
        {
          !isGetcomID ?
            <View style={{ flexDirection: 'row' }}>
              <View>
                <Input
                  ref={ref => { this.input1 = ref }}
                  containerStyle={styles.scan_input_container}
                  inputContainerStyle={styles.scan_inputContainerStyle}
                  inputStyle={[styles.scan_input]}
                  placeholder='请输入'
                  keyboardType='numeric'
                  value={compId}
                  onChangeText={(value) => {
                    value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
                    value = value.replace(/^\./g, ""); //验证第一个字符是数字
                    value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
                    //value = value.replace(/\b(0+)/gi, ""); //清楚开头的0
                    this.setState({
                      compId: value
                    })
                  }}
                  onFocus={() => {
                    this.setState({
                      focusIndex: index,
                      type: 'compid',
                    })
                  }}
                  onSubmitEditing={() => {
                    let inputComId = this.state.compId
                    console.log("🚀 ~ file: qualityinspection.js ~ line 468 ~ QualityInspection ~ inputComId", inputComId)
                    inputComId = inputComId.replace(/\b(0+)/gi, "")
                    this.GetcomID(inputComId)
                  }}
                />
              </View>
              <ScanButton
                onPress={() => {
                  this.setState({
                    iscomIDdelete: false,
                    buttondisable: true,
                    focusIndex: 0
                  })
                  this.props.navigation.navigate('QRCode', {
                    type: 'compid',
                    page: 'HideInspection'
                  })
                }}
                onLongPress={() => {
                  this.setState({
                    type: 'compid',
                    focusIndex: index
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}
              />
            </View>
            :
            <TouchableCustom
              onPress={() => {
                console.log('onPress')
              }}
              onLongPress={() => {
                console.log('onLongPress')
                Alert.alert(
                  '提示信息',
                  '是否删除构件信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      this.setState({
                        compData: [],
                        compCode: '',
                        compId: '',
                        iscomIDdelete: true,
                        isGetcomID: false,
                      })
                    }
                  }])
              }} >
              <Text>{compCode}</Text>
            </TouchableCustom>
        }
      </View>

    )
  }

  GetsteelCageInfoforPutMold = (id) => {
    console.log("🚀 ~ file: qualityinspection.js ~ line 346 ~ SteelCage ~ id", id)
    let formData = new FormData();
    let data = {};
    data = {
      "action": "GetsteelCageInfoforPutMold",
      "servicetype": "pda",
      "express": "6C8FAB14",
      "ciphertext": "96d2d1428051129167842f3b5eaca07f",
      "data": {
        "steelCageID": id,
        "compId": this.state.compId,
        "factoryId": Url.PDAFid
      }
    }
    console.log("🚀 ~ file: qualityinspection.js ~ line 349 ~ SteelCage ~ data", data)
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let steelCageID = resData.result.steelCageID
        let steelCageCode = resData.result.steelCageCode
        console.log("🚀 ~ file: qualityinspection.js ~ line 374 ~ SteelCage ~ steelCageCode", steelCageCode)
        this.setState({
          steelCageID: steelCageID,
          steelCageCode: steelCageCode,
          isGetsteelCageInfoforPutMold: true,
          issteelCageIDdelete: false
        })
      } else {
        console.log('resData', resData)
        Alert.alert('ERROR', resData.message)
        varGetError = true
        this.setState({
          GetError: true
        })
      }

    }).catch(err => {
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  GettaiwanId = (id) => {
    let Tai_QRname = '111'
    let formData = new FormData();
    let data = {};
    data = {
      "action": "gettaiwanInfo",
      "servicetype": "pda",
      "express": "8FBA0BDC",
      "ciphertext": "591057f7d292870338e0fdffe9ef719f",
      "data": {
        "factoryId": Url.PDAFid,
        "taiwanId": id
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let taiwanId = resData.result.taiwanId
        Tai_QRname = resData.result.Tai_QRname
        this.setState({
          taiwanId: taiwanId,
          Tai_QRname: Tai_QRname,
          isGettaiwanId: true,
          istaiwanIddelete: false
        })
      } else {
        Alert.alert('ERROR', resData.message)
        varGetError = true
        this.setState({
          GetError: true
        })
      }

    }).catch(err => {
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  taiwanItem = (index) => {
    const { isGettaiwanId, buttondisable, Tai_QRname } = this.state
    return (
      <View>
        {
          !isGettaiwanId ?
            <View>
              <ScanButton

                onPress={() => {
                  this.setState({
                    focusIndex: 1,
                    istaiwanIddelete: false,
                    buttondisable: true
                  })
                  setTimeout(() => {
                    this.setState({ GetError: false, buttondisable: false })
                    varGetError = false
                  }, 1500)
                  this.props.navigation.navigate('QRCode', {
                    type: 'taiwanId',
                    page: 'HideInspection'
                  })
                }}
              />
            </View>
            :
            <TouchableCustom
              onPress={() => {
                console.log('onPress')
              }}
              onLongPress={() => {
                console.log('onLongPress')
                Alert.alert(
                  '提示信息',
                  '是否删除模台信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      this.setState({
                        taiwanId: '',
                        Tai_QRname: '',
                        isGettaiwanId: false,
                        istaiwanIddelete: true,
                      })
                    }
                  }])
              }} >
              <Text>{Tai_QRname}</Text>
            </TouchableCustom>
        }
      </View>
    )
  }

  GetCommonMould = (id) => {
    let acceptanceName = '111'
    let formData = new FormData();
    let data = {};
    data = {
      "action": "GetCommonMould",
      "servicetype": "pda",
      "express": "50545047",
      "ciphertext": "f3eedf54128226aef62dd6023a1c1573",
      "data": {
        "compId": this.state.compId,
        "acceptanceId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let acceptanceId = resData.result.acceptanceId
        acceptanceName = resData.result.acceptanceName
        this.setState({
          acceptanceId: acceptanceId,
          acceptanceName: acceptanceName,
          isGetCommonMould: true,
          isCommonMoulddelete: false
        })
      } else {
        Alert.alert('ERROR', resData.message)
        varGetError = true
        this.setState({
          GetError: true
        })
      }


    }).catch(err => {
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  GetTeamPeopleInfo = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getTeamPeopleInfo",
      "servicetype": "pda",
      "express": "2B9B0E37",
      "ciphertext": "eb1a5891270a0ebdbb68d37e27f82b92",
      "data": {
        "factoryId": Url.PDAFid,
        "monitorId": id
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let monitorId = resData.result.monitorId
        let monitorName = resData.result.monitorName
        let teamId = resData.result.teamId
        let teamName = resData.result.teamName
        this.setState({
          monitorId: monitorId,
          monitorName: monitorName,
          monitorTeamId: teamId,
          monitorTeamName: teamName,
          isGetTeamPeopleInfo: true,
          isTeamPeopledelete: false
        }, () => {
          DeviceStorageData = {
            "isGetTeamPeopleInfo": true,
            "InspectorId": this.state.InspectorId,
            "InspectorName": this.state.InspectorName,
            "InspectorSelect": this.state.InspectorName,
            "monitorId": this.state.monitorId,
            "monitorName": this.state.monitorName,
            "monitorSelect": this.state.monitorName,
            "monitorTeamName": this.state.monitorTeamName,
          }

          DeviceStorage.save('DeviceStorageDataQI', JSON.stringify(DeviceStorageData))
        })
        this.setState({
          focusIndex: 7,
          type: 'InspectorId'
        })
        setTimeout(() => { this.scoll.scrollToEnd() }, 300)
      } else {
        Alert.alert('ERROR', resData.message)
      }
    }).catch(err => {
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  monitorNameItem = (index) => {
    const { isGetTeamPeopleInfo, buttondisable, monitorName, monitorSetup, monitors, monitorSelect } = this.state
    console.log("🚀 ~ file: qualityinspection.js ~ line 559 ~ SteelCage ~ monitors", monitors)
    return (
      <View>
        {
          monitorSetup == '扫码' ?
            (
              !isGetTeamPeopleInfo ?
                <View>
                  <ScanButton
                    onPress={() => {
                      this.setState({
                        isTeamPeopledelete: false,
                        focusIndex: index
                      })
                      this.props.navigation.navigate('QRCode', {
                        type: 'monitorId',
                        page: 'HideInspection'
                      })
                    }}
                    onLongPress={() => {
                      this.setState({
                        type: 'monitorId',
                        focusIndex: index
                      })
                      NativeModules.PDAScan.onScan();
                    }}
                    onPressOut={() => {
                      NativeModules.PDAScan.offScan();
                    }}
                  />
                </View>
                :
                <TouchableCustom
                  onPress={() => {
                    console.log('onPress')
                  }}
                  onLongPress={() => {
                    console.log('onLongPress')
                    Alert.alert(
                      '提示信息',
                      '是否删除班组信息',
                      [{
                        text: '取消',
                        style: 'cancel'
                      }, {
                        text: '删除',
                        onPress: () => {
                          this.setState({
                            monitorId: '',
                            monitorName: '',
                            monitorTeamId: '',
                            monitorTeamName: '',
                            isGetTeamPeopleInfo: false,
                            isTeamPeopledelete: true
                          })
                        }
                      }])
                  }} >
                  <Text>{monitorName}</Text>
                </TouchableCustom>
            )
            :
            (
              this.state.isCheck ? <Text >{monitorSelect}</Text> :
                <TouchableCustom onPress={() => { this.isBottomVisible(monitors, index) }} >
                  <IconDown text={monitorSelect} />
                </TouchableCustom>
            )

        }
      </View>
    )
  }

  GetInspector = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getInspector",
      "servicetype": "pda",
      "express": "CDAB7A2D",
      "ciphertext": "a590b603df9b5a4384c130c7c21befb7",
      "data": {
        "InspectorId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let InspectorId = resData.result.InspectorId
        let InspectorName = resData.result.InspectorName
        this.setState({
          InspectorId: InspectorId,
          InspectorName: InspectorName,
          isGetInspector: true,
          isInspectordelete: false
        }, () => {
          DeviceStorageData = {
            "isGetInspector": true,
            "InspectorId": this.state.InspectorId,
            "InspectorName": this.state.InspectorName,
            "InspectorSelect": this.state.InspectorName,
            "monitorId": this.state.monitorId,
            "monitorName": this.state.monitorName,
            "monitorSelect": this.state.monitorName,
            "monitorTeamName": this.state.monitorTeamName,
          }

          DeviceStorage.save('DeviceStorageDataQI', JSON.stringify(DeviceStorageData))
        })
      } else {
        Alert.alert('ERROR', resData.message)
        varGetError = true
        this.setState({
          GetError: true
        })
      }

    }).catch(err => {
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  inspectorItem = (index) => {
    const { isGetInspector, buttondisable, InspectorName, monitorSetup, InspectorSelect, Inspectors } = this.state
    return (
      <View>
        {
          monitorSetup == '扫码' ?
            (
              !isGetInspector ?
                <View>
                  <ScanButton
                    onPress={() => {
                      this.setState({
                        isInspectordelete: false,
                        buttondisable: true,
                        focusIndex: 6
                      })
                      this.props.navigation.navigate('QRCode', {
                        type: 'InspectorId',
                        page: 'HideInspection'
                      })
                    }}
                    onLongPress={() => {
                      this.setState({
                        type: 'InspectorId',
                        focusIndex: index
                      })
                      NativeModules.PDAScan.onScan();
                    }}
                    onPressOut={() => {
                      NativeModules.PDAScan.offScan();
                    }}
                  />
                </View>
                :
                <TouchableCustom
                  onPress={() => {
                    console.log('onPress')

                  }}
                  onLongPress={() => {
                    console.log('onLongPress')
                    Alert.alert(
                      '提示信息',
                      '是否删除质检员信息',
                      [{
                        text: '取消',
                        style: 'cancel'
                      }, {
                        text: '删除',
                        onPress: () => {
                          this.setState({
                            InspectorId: '',
                            InspectorName: '',
                            isGetInspector: false,
                            isInspectordelete: true
                          })
                        }
                      }])
                  }} >
                  <Text>{InspectorName}</Text>
                </TouchableCustom>
            ) :
            (
              this.state.isCheck ? <Text >{InspectorSelect}</Text> :
                <TouchableCustom onPress={() => { this.isBottomVisible(Inspectors, index) }} >
                  <IconDown text={InspectorSelect} />
                </TouchableCustom>
            )
        }
      </View>
    )
  }

  PostData = () => {
    const { InspectorId, acceptanceId, steelLabel, monitorId, compId, ServerTime, checkResult, rowguid, taiwanId, steelCageIDs, steelCageID, imageArr, remark, recid } = this.state
    const { navigation } = this.props;
    const QRid = navigation.getParam('QRid') || '';

    if (compId == '') {
      this.toast.show('产品编号不能为空');
      return
    }
    if (monitorId == '') {
      this.toast.show('班组信息不能为空');
      return
    }
    if (InspectorId == '') {
      this.toast.show('质检员信息不能为空');
      return
    }
    if (this.state.isAppPhotoSetting) {
      if (imageArr.length == 0) {
        this.toast.show('请先拍摄构件照片');
        return
      }
    }
    this.toast.show(saveLoading, 0)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "saveHideCheckInfo",
      "servicetype": "pda",
      "express": "06C795EE",
      "ciphertext": "a1e3818d57d9bfc9437c29e7a558e32e",
      "data": {
        "InspectorId": InspectorId,
        "monitorId": monitorId,
        "compId": compId,
        "Username": Url.PDAusername,
        "factoryId": Url.PDAFid,
        "hideCheckTime": ServerTime,
        "checkResult": checkResult,
        "operatorId": Url.PDAEmployeeId,
        "operator": Url.PDAEmployeeName,
        "rowguid": rowguid,
        "remark": remark,
        "recid": recid
      },
    }
    formData.append('jsonParam', JSON.stringify(data))
    if (!Url.isAppNewUpload) {
      imageArr.map((item, index) => {
        formData.append('img_' + index, {
          uri: item,
          type: 'image/jpeg',
          name: 'img_' + index
        })
      })
    }
    console.log("🚀 ~ file: qualityinspection.js ~ line 793 ~ SteelCage ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('保存成功');
        setTimeout(() => {
          this.props.navigation.replace(this.props.navigation.getParam("page"), {
            title: this.props.navigation.getParam("title"),
            pageType: this.props.navigation.getParam("pageType"),
            page: this.props.navigation.getParam("page"),
            isAppPhotoSetting: this.props.navigation.getParam("isAppPhotoSetting"),
            isAppDateSetting: this.props.navigation.getParam("isAppDateSetting"),
            isAppPhotoAlbumSetting: this.props.navigation.getParam("isAppPhotoAlbumSetting"),
          })
          //this.props.navigation.navigate('PDAMainStack')
        }, 400)
      } else {
        this.toast.close()
        Alert.alert('保存失败', resData.message)
        console.log('resData', resData)
      }
    }).catch((error) => {
      this.toast.show('保存失败')
      if (error.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('保存失败', "响应内容不是合法JSON格式")
        return
      }
      if (error.toString().indexOf('Network request faile') != -1) {
        Alert.alert('保存失败', "网络请求错误")
        return
      }
      Alert.alert('保存失败', error.toString())
    });
  }

  ReviseData = () => {
    const { isBatch, InspectorId, monitorId, rowguid, ServerTime, imageArr } = this.state;
    const { navigation } = this.props;
    let guid = navigation.getParam('guid') || ''
    console.log('DeleteData')

    if (!isBatch) {
      if (imageArr.length == 0) {
        this.toast.show('请先拍摄构件照片');
        return
      }
    }

    let formData = new FormData();
    let data = {};
    if (isBatch) {
      data = {
        "action": "deletebatchcomplexhidecheck",
        "servicetype": "batchproduction",
        "express": "948F091E",
        "ciphertext": "c9b2aae5e7339b0cc5c9d24da95ae805",
        "data": {
          "userName": Url.PDAusername,
          "rowguid": rowguid
        },
      }
    } else {
      data = {
        "action": "ReviseHideCheck",
        "servicetype": "pda",
        "express": "06C795EE",
        "ciphertext": "a1e3818d57d9bfc9437c29e7a558e32e",
        "data": {
          "result": "合格",
          "PDAuserid": Url.PDAEmployeeId,
          "InspectorId": InspectorId,
          "monitorId": monitorId,
          "factoryId": Url.PDAFid,
          "componentImageId": "componentImageId",
          "guid": rowguid,
          "time": ServerTime,
          "PDAuser": Url.PDAusername
        },
      }
    }

    if (imageArr.length > 0 || !isBatch) {
      imageArr.map((item, index) => {
        formData.append('img_' + index, {
          uri: item,
          type: 'image/jpeg',
          name: 'img_' + index
        })
      })
    }

    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: qualityinspection.js ~ line 953 ~ HideInspection ~ DeleteData ~ formData", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        if (isBatch) {
          this.toast.show('删除成功');
        } else {
          this.toast.show('修改成功');
        }
        this.props.navigation.navigate('MainCheckPage')
      } else {
        if (isBatch) {
          this.toast.show('删除失败');
        } else {
          Alert.alert(
            '修改失败',
            resData.message,
            [{
              text: '取消',
              style: 'cancel'
            }, {
              text: '返回上一级',
              onPress: () => {
                this.props.navigation.goBack()
              }
            }])
        }
      }
    }).catch((error) => {
    });

  }

  BatchPostData = () => {
    const { InspectorId, ProductionQuantity, acceptanceId, compData, steelLabel, monitorId, compId, ServerTime, checkResult, rowguid, taiwanId, steelCageIDs, steelCageID, imageArr, remark, hideCheckNum, productionNum } = this.state
    const { navigation } = this.props;
    const QRid = navigation.getParam('QRid') || '';

    if (compId == '') {
      this.toast.show('请选择生产批次');
      return
    } else if (monitorId == '') {
      this.toast.show('班组信息不能为空');
      return
    } else if (InspectorId == '') {
      this.toast.show('质检员信息不能为空');
      return
    } else if (productionNum.length == 0) {
      this.toast.show('生产数量不能为空');
      return
    } else if (hideCheckNum.length == 0) {
      this.toast.show('隐检数量不能为空');
      return
    } else if (productionNum > ProductionQuantity) {
      this.toast.show('生产数量不能超过任务单下单数量');
      return
    } else if (hideCheckNum > productionNum) {
      this.toast.show('隐检数量不能超过生产数量');
      return
    }


    this.toast.show(saveLoading, 0)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "savebatchcomplexhidecheck",
      "servicetype": "batchproduction",
      "express": "06C795EE",
      "ciphertext": "a1e3818d57d9bfc9437c29e7a558e32e",
      "data": {
        "monitorId": monitorId,
        "designType": compData.result.DesignType,
        "hideCheckTime": ServerTime,
        "remark": remark,
        "userName": Url.PDAusername,
        "checkResult": checkResult,
        "rowguid": rowguid,
        "InspectorId": InspectorId,
        "compId": compId,
        "_bizid": Url.PDAFid,
        "projectId": compData.result.ProjectId,
        "hideCheckNum": hideCheckNum,
        "productionNum": productionNum
      },
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: qualityinspection.js ~ line 793 ~ SteelCage ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('保存成功');
        setTimeout(() => {
          this.props.navigation.replace(this.props.navigation.getParam("page"), {
            title: this.props.navigation.getParam("title"),
            pageType: this.props.navigation.getParam("pageType"),
            page: this.props.navigation.getParam("page"),
            isAppPhotoSetting: this.props.navigation.getParam("isAppPhotoSetting"),
            isAppDateSetting: this.props.navigation.getParam("isAppDateSetting"),
            isAppPhotoAlbumSetting: this.props.navigation.getParam("isAppPhotoAlbumSetting"),
          })
          //this.props.navigation.navigate('PDAMainStack')
        }, 400)
      } else {
        Alert.alert('保存失败', resData.message)
        this.toast.show('保存失败')
        console.log('resData', resData)
      }
    }).catch((error) => {
      this.toast.show('保存失败')
      if (error.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('保存失败', "响应内容不是合法JSON格式")
        return
      }
      if (error.toString().indexOf('Network request faile') != -1) {
        Alert.alert('保存失败', "网络请求错误")
        return
      }
      Alert.alert('保存失败', error.toString())
    });
  }

  cameraFunc = () => {
    launchCamera(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: parseInt(Url.isResizePhoto),
        maxWidth: parseInt(Url.isResizePhoto),
        saveToPhotos: true,

      },
      (response) => {
        if (response.uri == undefined) {
          return
        }
        imageArr.push(response.uri)
        this.setState({
          pictureUri: response.uri,
          imageArr: imageArr
        })
        if (Url.isAppNewUpload) {
          this.cameraPost(response.uri)
        } else {
          this.setState({
            pictureSelect: true,
          })
        }
        console.log(response)
      },
    )
  }

  camLibraryFunc = () => {
    launchImageLibrary(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: parseInt(Url.isResizePhoto),
        maxWidth: parseInt(Url.isResizePhoto),
      },
      (response) => {
        if (response.uri == undefined) {
          return
        }
        imageArr.push(response.uri)
        this.setState({
          //pictureSelect: true,
          pictureUri: response.uri,
          imageArr: imageArr
        })
        if (Url.isAppNewUpload) {
          this.cameraPost(response.uri)
        } else {
          this.setState({
            pictureSelect: true,
          })
        }
        console.log(response)
      },
    )
  }

  camandlibFunc = () => {
    Alert.alert(
      '提示信息',
      '选择拍照方式',
      [{
        text: '取消',
        style: 'cancel'
      }, {
        text: '拍照',
        onPress: () => {
          this.cameraFunc()
        }
      }, {
        text: '相册',
        onPress: () => {
          this.camLibraryFunc()
        }
      }])
  }

   // 展示/隐藏 放大图片
  handleZoomPicture = (flag, index) => {
    this.setState({
      imageOverSize: false,
      currShowImgIndex: index || 0
    })
  }

  // 加载放大图片弹窗
  renderZoomPicture = () => {
    const { imageOverSize, currShowImgIndex, imageFileArr } = this.state
    return (
      <OverlayImgZoomPicker
        isShowImage={imageOverSize}
        currShowImgIndex={currShowImgIndex}
        zoomImages={imageFileArr}
        callBack={(flag) => this.handleZoomPicture(flag)}
      ></OverlayImgZoomPicker>
    )
  }

  cameraPost = (uri) => {
    const { recid } = this.state
    this.setState({
      isPhotoUpdate: true
    })
    this.toast.show('图片上传中', 2000)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "savephote",
      "servicetype": "photo_file",
      "express": "D2979AB2",
      "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
      "data": {
        "fileflag": fileflag,
        "username": Url.PDAusername,
        "recid": recid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    formData.append('img', {
      uri: uri,
      type: 'image/jpeg',
      name: 'img'
    })
    console.log("🚀 ~ file: concrete_pouring.js ~ line 672 ~ SteelCage ~ formData", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 690 ~ SteelCage ~ resData", resData)
      this.toast.close()
      if (resData.status == '100') {
        let fileid = resData.result.fileid;
        let recid = resData.result.recid;

        let tmp = {}
        tmp.fileid = fileid
        tmp.uri = uri
        tmp.url = uri
        imageFileArr.push(tmp)
        this.setState({
          fileid: fileid,
          recid: recid,
          pictureSelect: true,
          imageFileArr: imageFileArr
        })
        console.log("🚀 ~ file: concrete_pouring.js ~ line 704 ~ SteelCage ~ imageFileArr", imageFileArr)
        this.toast.show('图片上传成功');
        this.setState({
          isPhotoUpdate: false
        })
      } else {
        Alert.alert('图片上传失败', resData.message)
        this.toast.close(1)
      }
    }).catch((error) => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 692 ~ SteelCage ~ error", error)
    });
  }

  cameraDelete = (item) => {
    let i = imageArr.indexOf(item)

    if (Url.isAppNewUpload) {
      let formData = new FormData();
      let data = {};
      data = {
        "action": "deletephote",
        "servicetype": "photo_file",
        "express": "D2979AB2",
        "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
        "data": {
          "fileid": imageFileArr[i].fileid,
        }
      }
      formData.append('jsonParam', JSON.stringify(data))
      fetch(Url.PDAurl, {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        body: formData
      }).then(res => {
        console.log(res.statusText);
        console.log(res);
        return res.json();
      }).then(resData => {
        if (resData.status == '100') {
          if (i > -1) {
            imageArr.splice(i, 1)
            imageFileArr.splice(i, 1)
            this.setState({
              imageArr: imageArr,
              imageFileArr: imageFileArr
            }, () => {
              this.forceUpdate()
            })
          }
          if (imageArr.length == 0) {
            this.setState({
              pictureSelect: false
            })
          }
          this.toast.show('图片删除成功');
        } else {
          Alert.alert('图片删除失败', resData.message)
          this.toast.close(1)
        }
      }).catch((error) => {
        console.log("🚀 ~ file: concrete_pouring.js ~ line 761 ~ SteelCage ~ cameraDelete ~ error", error)
      });
    } else {
      if (i > -1) {
        imageArr.splice(i, 1)
        this.setState({
          imageArr: imageArr,
        }, () => {
          this.forceUpdate()
        })
      }
      if (imageArr.length == 0) {
        this.setState({
          pictureSelect: false
        })
      }
    }
  }

  imageView = () => {
    return (
      <View style={{ flexDirection: 'row' }}>
        {
          this.state.pictureSelect ?
            this.state.imageArr.map((item, index) => {
              return (
                <AvatarImg
                  item={item}
                  index={index}
                  onPress={() => {
                    this.setState({
                      imageOverSize: true,
                      pictureUri: item
                    })
                  }}
                  onLongPress={() => {
                    Alert.alert(
                      '提示信息',
                      '是否删除构件照片',
                      [{
                        text: '取消',
                        style: 'cancel'
                      }, {
                        text: '删除',
                        onPress: () => {
                          this.cameraDelete(item)
                        }
                      }])
                  }} />
              )
            }) : <View></View>
        }
      </View>
    )
  }

  _DatePicker = () => {
    const { ServerTime } = this.state
    return (
      <DatePicker
        customStyles={{
          dateInput: styles.dateInput,
          dateTouchBody: styles.dateTouchBody,
          dateText: this.state.isAppDateSetting ? styles.dateText : styles.dateDisabled,
        }}
        iconComponent={<Icon name='caretdown' color={this.state.isAppDateSetting ? '#419fff' : "#666"} iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
        }
        showIcon={true}
        mode='datetime'
        date={ServerTime}
        disabled={!this.state.isAppDateSetting}
        onOpenModal={() => { this.setState({ focusIndex: 4 }) }}
        format="YYYY-MM-DD HH:mm"
        onDateChange={(value) => {
          this.setState({
            ServerTime: value
          })
        }}
      />
    )
  }

  isBottomVisible = (data, focusIndex) => {
    console.log("🚀 ~ file: hideinspection.js ~ line 1122 ~ HideInspection ~ typeof(data)", typeof (data))
    if (typeof (data) != "undefined") {
      if (data.length > 0) {
        this.setState({ bottomVisible: true, bottomData: data, focusIndex: focusIndex })
      }
    } else {
      this.toast.show("无数据")
    }
  }

  CameraView = () => {
    return (
      <ListItem
        containerStyle={{ paddingBottom: 6 }}
        leftAvatar={
          <View >
            <View style={{ flexDirection: 'column' }} >
              {
                <AvatarAdd
                  pictureSelect={this.state.pictureSelect}
                  backgroundColor='rgba(77,142,245,0.20)'
                  color='#4D8EF5'
                  title="构"
                  onPress={() => {
                    if (this.state.isAppPhotoAlbumSetting) {
                      this.camandlibFunc()
                    } else {
                      this.cameraFunc()
                    }

                    /* this.setState({
                      CameraVisible: true
                    }) */
                  }} />
              }
              {this.imageView()}
            </View>
          </View>
        }
      />
    )
  }

  batchListView = () => {
    return (
      <TouchableCustom underlayColor={'lightgray'} onPress={() => {
        this.isBottomVisible(this.state.listBatchNoInfo, "1")
      }}>

        <ListItemScan
          isButton={false}
          focusStyle={this.state.focusIndex == '1' ? styles.focusColor : {}}
          title='生产批次'
          rightElement={
            <IconDown text={this.state.batchCompCodeselect}></IconDown>
          }
        />
      </TouchableCustom>
    )
  }

  BottomSheetBatchMain = () => {
    const { bottomVisible, bottomData, isCheckPage } = this.state;
    return (
      <BottomSheet
        isVisible={bottomVisible && !this.state.isCheckPage}
        onRequestClose={() => {
          this.setState({
            bottomVisible: false
          })
        }}
        onBackdropPress={() => {
          this.setState({
            bottomVisible: false
          })
        }}
      >
        <ScrollView style={styles.bottomsheetScroll}>
          {
            bottomData.map((item, index) => {
              let title = ''
              let dataItem = {}
              if (item.rowguid) {
                title = item.name + ' ' + item.teamName
              } else if (item.compId) {
                dataItem.Title = item.compCode
                dataItem.rightTitle = item.ProductionQuantity
                dataItem.subTitle = item.CompTypeName
                dataItem.subRightTitle = item.ProjectName
              } else {
                title = item.InspectorName
              }
              return (
                <TouchableCustom
                  onPress={() => {
                    if (item.rowguid) {
                      this.setState({
                        monitorId: item.rowguid,
                        monitorSelect: item.name,
                        monitorName: item.name,
                        monitorTeamName: item.teamName,
                      }, () => {
                        DeviceStorageData = {
                          "InspectorId": this.state.InspectorId,
                          "InspectorName": this.state.InspectorName,
                          "InspectorSelect": this.state.InspectorName,
                          "monitorId": this.state.monitorId,
                          "monitorName": this.state.monitorName,
                          "monitorSelect": this.state.monitorName,
                          "monitorTeamName": this.state.monitorTeamName,
                        }

                        DeviceStorage.save('DeviceStorageDataQI', JSON.stringify(DeviceStorageData))
                      })
                    } else if (item.compId) {
                      let compData = {}
                      compData.result = item
                      this.setState({
                        batchCompCode: item.compCode,
                        batchCompCodeselect: item.compCode,
                        productLineName: item.productLineName,
                        ProductionQuantity: item.ProductionQuantity,
                        hideCheckNum: item.ProductionQuantity,
                        productionNum: item.ProductionQuantity,
                        teamName: item.teamName,
                        compId: item.compId,
                        compCode: item.compCode,
                        compData: compData,
                        isGetcomID: true,
                      })
                    } else if (item.InspectorId) {
                      this.setState({
                        InspectorId: item.InspectorId,
                        InspectorName: item.InspectorName,
                        InspectorSelect: item.InspectorName,
                      }, () => {
                        DeviceStorageData = {
                          "InspectorId": this.state.InspectorId,
                          "InspectorName": this.state.InspectorName,
                          "InspectorSelect": this.state.InspectorName,
                          "monitorId": this.state.monitorId,
                          "monitorName": this.state.monitorName,
                          "monitorSelect": this.state.monitorName,
                          "monitorTeamName": this.state.monitorTeamName,
                        }

                        DeviceStorage.save('DeviceStorageDataQI', JSON.stringify(DeviceStorageData))
                      })
                    }
                    this.setState({
                      bottomVisible: false
                    })
                  }}
                >
                  {
                    !item.compId ? <BottomItem backgroundColor='white' color="#333" title={title} /> :
                      <BatchListItem backgroundColor='white' color="#333" dataItem={dataItem} />
                  }

                </TouchableCustom>
              )

            })
          }
        </ScrollView>
        <TouchableHighlight
          onPress={() => {
            this.setState({ bottomVisible: false })
          }}>
          <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
        </TouchableHighlight>

      </BottomSheet>
    )
  }

  func = (state) => {
    console.log("🚀 ~ file: qualityinspection.js ~ line 1166 ~ QualityInspection ~ state", state)
    this.setState({
      isPaperTypeVisible: true
    })
  }

  paperResData = (type) => {
    let formData = new FormData();
    let data = {}
    data = {
      "action": "getDrawings",
      "servicetype": "pda",
      "express": "FCEF95AF",
      "ciphertext": "985e3620ae26a2f380c723b60ce9b525",
      "data": {
        "type": type,
        "compId": this.state.compId,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 314 ~ QualityInspection ~ resData", resData)
      if (resData.status == "100") {

        let paperUrl = resData.result.Url
        let FileName = resData.result.FileName

        this.setState({
          paperUrl: paperUrl,
          FileName: FileName,
          isPDFVisible: true,
          isPaperTypeVisible: false
        })
      } else {
        Alert.alert('错误', resData.message)
        this.setState({ isPaperTypeVisible: false })
      }
    }).catch((error) => {
    });
  }

  render() {
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || ''
    const type = navigation.getParam('type') || '';
    let pageType = navigation.getParam('pageType') || ''

    if (type == 'compid') {
      QRcompid = QRid
    }
    const { isCheckPage, isBatch, buttondisable, focusIndex, steelLabel, resData, ServerTime, isGetcomID, isGetCommonMould, isGetInspector, isGetTeamPeopleInfo, isGetsteelCageInfoforPutMold, isGettaiwanId, datepickerVisible, Tai_QRname, acceptanceName, monitorSetup, monitors, monitorid, monitorName, monitorTeamName, Inspectors, InspectorsId, InspectorsName, compData, compCode, bottomVisible, bottomData, isCheck, remark, isPaperTypeVisible, isPDFVisible, paperList, paperUrl, FileName, ProductionQuantity, hideCheckNum, productionNum, isUsedCageBusiness } = this.state

    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          <NavigationEvents
            onWillFocus={this.UpdateControl}
            onWillBlur={() => {
              if (QRid != '') {
                navigation.setParams({ QRid: '', type: '' })
              }
            }} />
          <ScrollView ref={ref => this.scoll = ref} style={styles.mainView} showsVerticalScrollIndicator={false} >

            <View style={styles.listView}>
              {
                isBatch ? this.batchListView() : this.CameraView()
              }

              {
                isBatch ? <View></View> : <ListItemScan
                  isButton={!isGetcomID}
                  focusStyle={focusIndex == '0' ? styles.focusColor : {}}
                  title='产品编号'
                  onPressIn={() => {
                    this.setState({
                      type: 'compid',
                      focusIndex: 0
                    })
                    NativeModules.PDAScan.onScan();
                  }}
                  onPress={() => {
                    this.setState({
                      type: 'compid',
                      focusIndex: 0
                    })
                    NativeModules.PDAScan.onScan();
                  }}
                  onPressOut={() => {
                    NativeModules.PDAScan.offScan();
                  }}
                  rightElement={
                    this.comIDItem(0)
                  }
                />
              }
              {
                this.state.isGetcomID ?
                  <CardList compData={compData} func={this.func.bind(this)} /> : <View></View>
              }
              {
                isBatch ? <View>
                  <ListItemScan
                    title='生产数量'
                    rightElement={
                      <View>
                        <Input
                          containerStyle={styles.quality_input_container}
                          inputContainerStyle={styles.inputContainerStyle}
                          inputStyle={[styles.quality_input_, { top: 7 }]}
                          placeholder={ProductionQuantity.length == '0' ? '可编辑' : ProductionQuantity.toString()}
                          value={productionNum}
                          onChangeText={(value) => {
                            value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
                            value = value.replace(/^\./g, ""); //验证第一个字符是数字
                            value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
                            value = value.replace(".", "$#$").replace(/\./g, "").replace("$#$", ".");
                            this.setState({
                              productionNum: value,
                              hideCheckNum: value
                            })
                          }} />
                      </View>
                    }
                  />
                  <ListItemScan
                    title='隐检数量'
                    rightElement={
                      <View>
                        <Input
                          containerStyle={styles.quality_input_container}
                          inputContainerStyle={styles.inputContainerStyle}
                          inputStyle={[styles.quality_input_, { top: 7 }]}
                          placeholder={ProductionQuantity.length == '0' ? '可编辑' : (productionNum.length > 0 ? productionNum.toString() : ProductionQuantity.toString())}
                          //value={hideCheckNum}
                          onChangeText={(value) => {
                            value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
                            value = value.replace(/^\./g, ""); //验证第一个字符是数字
                            value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
                            value = value.replace(".", "$#$").replace(/\./g, "").replace("$#$", ".");
                            this.setState({
                              hideCheckNum: value
                            })
                          }} />
                      </View>
                    }
                  />
                  {
                    isUsedCageBusiness == '使用' ? <ListItemScan
                      title='钢筋笼数量'
                      rightElement={
                        <View>
                          <Input
                            disabled={true}
                            containerStyle={styles.quality_input_container}
                            inputContainerStyle={styles.inputContainerStyle}
                            inputStyle={[styles.quality_input_, { top: 7 }]}
                            placeholder={ProductionQuantity.length == '0' ? '不可编辑' : ProductionQuantity.toString()}
                            value={productionNum}
                            onChangeText={(value) => {
                              this.setState({
                                ProductionQuantity: value
                              })
                            }} />
                        </View>
                      }
                    /> : <View></View>
                  }
                  <ListItemScan
                    title='模具数量'
                    rightElement={
                      <View>
                        <Input
                          disabled={true}
                          containerStyle={styles.quality_input_container}
                          inputContainerStyle={styles.inputContainerStyle}
                          inputStyle={[styles.quality_input_, { top: 7 }]}
                          placeholder={ProductionQuantity.length == '0' ? '不可编辑' : ProductionQuantity.toString()}
                          value={productionNum}
                          onChangeText={(value) => {
                            this.setState({
                              ProductionQuantity: value
                            })
                          }} />
                      </View>
                    }
                  />
                </View> : <View></View>
              }

              <ListItemScan
                focusStyle={focusIndex == '4' ? styles.focusColor : {}}
                title='隐检日期'
                rightElement={
                  this._DatePicker()
                }
              //rightTitle={ServerTime}
              />
              <ListItemScan
                title='操作人'
                rightTitleStyle={{ width: width / 1.5, textAlign: 'right', fontSize: 15 }}
                rightTitle={Url.PDAEmployeeName}
              />
              <ListItemScan
                isButton={true}
                focusStyle={focusIndex == '5' ? styles.focusColor : {}}
                bottomDivider
                title='检查结果'
                rightTitle={
                  <View style={{ flexDirection: "row", marginRight: -23, paddingVertical: 0 }}>
                    <CheckBoxScan
                      title='合格'
                      checked={this.state.checked}
                      onPress={() => {
                        this.setState({
                          checked: true,
                          checkResult: '合格',
                          focusIndex: 5
                        })
                      }}
                    />
                    <CheckBoxScan
                      title='不合格'
                      checked={false}
                      onPress={() => {
                        this.setState({
                          //checked: false,
                          //checkResult: '不合格'
                        })
                      }}
                    />
                  </View>
                }
              />
              <TouchableCustom underlayColor={'lightgray'} onPress={() => {
                if (monitorSetup != '扫码') {
                  this.isBottomVisible(monitors, 6)
                } else {
                  this.setState({
                    type: 'monitorId',
                    focusIndex: 6
                  })
                  NativeModules.PDAScan.onScan();
                }
              }}
                onPressIn={() => {
                  if (monitorSetup == '扫码') {
                    this.setState({
                      type: 'monitorId',
                      focusIndex: 6
                    })
                    NativeModules.PDAScan.onScan();
                  }
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}>
                <ListItemScan
                  isButton={monitorSetup == '扫码' && !isGetTeamPeopleInfo}
                  focusStyle={focusIndex == '6' ? styles.focusColor : {}}
                  title='班长确认'
                  rightElement={
                    this.monitorNameItem(6)
                  }
                />
              </TouchableCustom>

              <ListItemScan
                title='班组名称'
                rightTitleStyle={{ width: width / 1.5, textAlign: 'right', fontSize: 15 }}
                rightTitle={this.state.monitorTeamName}
              />

              <TouchableCustom underlayColor={'lightgray'} onPress={() => {
                if (monitorSetup != '扫码') {
                  this.isBottomVisible(Inspectors, 7)
                } else {
                  this.setState({
                    type: 'InspectorId',
                    focusIndex: 7
                  })
                  NativeModules.PDAScan.onScan();
                }
              }}
                onPressIn={() => {
                  if (monitorSetup == '扫码') {
                    this.setState({
                      type: 'InspectorId',
                      focusIndex: 7
                    })
                    NativeModules.PDAScan.onScan();
                  }
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}>
                <ListItemScan
                  isButton={monitorSetup == '扫码' && !isGetInspector}
                  focusStyle={focusIndex == '7' ? styles.focusColor : {}}
                  title='质检确认'
                  rightElement={
                    this.inspectorItem('7')
                  }
                />
              </TouchableCustom>

              <ListItemScan
                focusStyle={focusIndex == '8' ? styles.focusColor : {}}
                title='备注'
                rightElement={
                  <View>
                    <Input
                      containerStyle={styles.quality_input_container}
                      inputContainerStyle={styles.inputContainerStyle}
                      inputStyle={[styles.quality_input_, { top: 7 }]}
                      placeholder='请输入'
                      value={remark}
                      onChangeText={(value) => {
                        this.setState({
                          remark: value
                        })
                      }} />
                  </View>
                }
              />
            </View>

            <FormButton
              onPress={isCheckPage ? this.ReviseData : (isBatch ? this.BatchPostData : this.PostData)}
              title={isCheckPage ? (isBatch ? '删除' : '修改') : '保存'}
              backgroundColor={isCheckPage ? '#EB5D20' : '#17BC29'}
              disabled={this.state.isPhotoUpdate}
            />

             {this.renderZoomPicture()}

            {/* {overlayImg(obj = {
              onRequestClose: () => {
                this.setState({ imageOverSize: !this.state.imageOverSize }, () => {
                  console.log("🚀 ~ file: equipment_maintenance.js:1696 ~ render ~ imageOverSize:", this.state.imageOverSize)
                })
              },
              onPress: () => {
                this.setState({
                  imageOverSize: !this.state.imageOverSize
                })
              },
              uri: this.state.pictureUri,
              isVisible: this.state.imageOverSize
            })
            }*/}

            <Overlay
              fullScreen={true}
              animationType='fade'
              isVisible={this.state.isPDFVisible}
              onRequestClose={() => {
                this.setState({ isPDFVisible: !this.state.isPDFVisible });
              }}
            >
              <Header
                containerStyle={{ height: 45, paddingTop: 0, top: -5 }}
                centerComponent={{ text: '图纸预览', style: { color: 'gray', fontSize: 20 } }}
                rightComponent={<BackIcon
                  onPress={() => {
                    this.setState({
                      isPDFVisible: !this.state.isPDFVisible
                    });
                  }} />}
                backgroundColor='white'
              />
              <View style={{ flex: 1 }}>
                <Pdf
                  source={{
                    uri: paperUrl,
                    //method: 'GET', //默认 'GET'，请求 url 的方式
                  }}
                  fitWidth={true} //默认 false，若为 true 则不能将 fitWidth = true 与 scale 一起使用
                  fitPolicy={0} // 0:宽度对齐，1：高度对齐，2：适合两者（默认）
                  page={1}
                  //scale={1}
                  onLoadComplete={(numberOfPages, filePath, width, height, tableContents) => {
                    console.log(`number of pages: ${numberOfPages}`); //总页数
                    console.log(`number of filePath: ${filePath}`); //本地返回的路径
                    console.log(`number of width: `, JSON.stringify(width));
                    console.log(`number of height: ${JSON.stringify(height)}`);
                    console.log(`number of tableContents: ${tableContents}`);
                  }}
                  onError={(error) => {
                    console.log(error);
                  }}
                  minScale={1} //最小模块
                  maxScale={3}
                  enablePaging={true} //在屏幕上只能显示一页
                  style={{
                    flex: 1,
                    width: width
                  }}
                />
              </View>

            </Overlay>
            <BottomSheet
              isVisible={this.state.CameraVisible}
              onRequestClose={() => {
                this.setState({ CameraVisible: false })
              }}
            >
              <TouchableCustom
                onPress={() => {
                  launchCamera(
                    {
                      mediaType: 'photo',
                      includeBase64: false,
                      maxHeight: 600,
                      maxWidth: 600,
                      saveToPhotos: true
                    },
                    (response) => {
                      if (response.uri == undefined) {
                        return
                      }
                      imageArr.push(response.uri)
                      this.setState({
                        pictureSelect: true,
                        pictureUri: response.uri,
                        imageArr: imageArr
                      })
                      console.log(response)
                    },
                  )
                  this.setState({ CameraVisible: false })
                }} >
                <ListItemScan
                  title='拍照' >
                </ListItemScan>
              </TouchableCustom>
              <TouchableCustom
                onPress={() => {
                  launchImageLibrary(
                    {
                      mediaType: 'photo',
                      includeBase64: false,
                      maxHeight: 600,
                      maxWidth: 600,

                    },
                    (response) => {
                      if (response.uri == undefined) {
                        return
                      }
                      imageArr.push(response.uri)
                      this.setState({
                        pictureSelect: true,
                        pictureUri: response.uri,
                        imageArr: imageArr
                      })
                      console.log(response)
                    },
                  )
                  this.setState({ CameraVisible: false })
                }} >
                <ListItemScan
                  title='相册' >
                </ListItemScan>
              </TouchableCustom>
              <ListItemScan
                title='关闭'
                containerStyle={{ backgroundColor: 'red' }}
                onPress={() => {
                  this.setState({ CameraVisible: false })
                }} >
              </ListItemScan>
            </BottomSheet>

            <BottomSheet
              isVisible={datepickerVisible}
              onRequestClose={() => {
                this.setState({ datepickerVisible: false })
              }}
            >

              <ListItemScan
                title='确定'
                onPress={() => {
                  this.setState({
                    datepickerVisible: false
                  })
                }}
              ></ListItemScan>
            </BottomSheet>

            {this.BottomSheetBatchMain()}

            <BottomSheet
              isVisible={isPaperTypeVisible}
              onRequestClose={() => {
                this.setState({
                  isPaperTypeVisible: false
                })
              }}
            >
              <ScrollView style={styles.bottomsheetScroll}>
                {
                  paperList.map((item, index) => {
                    return (
                      <TouchableCustom
                        onPress={() => {
                          this.paperResData(item)
                        }}
                      >
                        <BottomItem backgroundColor='white' color="#333" title={item} />
                      </TouchableCustom>
                    )

                  })
                }
              </ScrollView>
              <TouchableHighlight
                onPress={() => {
                  this.setState({ isPaperTypeVisible: false })
                }}>
                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
              </TouchableHighlight>

            </BottomSheet>
          </ScrollView>
        </View>
      )
    }
  }

}
