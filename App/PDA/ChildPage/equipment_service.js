import React from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, Icon, Card, ListItem, BottomSheet, CheckBox, Avatar, Overlay, Header, Input, Image } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { RFT } from '../../Url/Pixal';
import { ScrollView } from 'react-native-gesture-handler';
import CardList from "../Componment/CardList";
import DatePicker from 'react-native-datepicker'
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { NavigationEvents } from 'react-navigation';
import ScanButton from '../Componment/ScanButton';
import { styles } from '../Componment/PDAStyles';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import ListItemScan_child from '../Componment/ListItemScan_child';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import CheckBoxScan from '../Componment/CheckBoxScan';
import AvatarAdd from '../Componment/AvatarAdd';
import AvatarImg from '../Componment/AvatarImg';
import DeviceStorage from '../../Url/DeviceStorage';

import { DeviceEventEmitter, NativeModules } from 'react-native';
import overlayImg from '../Componment/OverlayImg';
import OverlayImgZoomPicker from '../Componment/OverlayImgZoomPicker';

let varGetError = false

const { height, width } = Dimensions.get('window') //获取宽高

let fileflag = "5"

let imageArr = [], imageFileArr = [], careIdStr = '', careIdArr = []

let DeviceStorageData = {}

class BackIcon extends React.PureComponent {
  render() {
    return (
      <TouchableCustom
        onPress={this.props.onPress} >
        <Icon
          name='arrow-back'
          color='#419FFF' />
      </TouchableCustom>
    )
  }
}


export default class EquipmentMaintenance extends React.PureComponent {
  constructor() {
    super();
    this.state = {
      resData: {},
      title: '',
      type: 'InspectorId',
      pageType: '',
      compData: {},
      MonitorSetup: [],
      MaintainApplitions: [],
      MaintainManagers: [],
      beginDate: '',
      endDate: '',
      remainremark: '',
      errremark: '',
      accremark: '',
      mainremark: '',
      //设备编码
      rowguid: '',
      formCode: '',
      formCodeSelect: '请选择',
      equipId: '',
      equipIdSelect: '',
      equipName: '',
      equipSize: '',
      ServerTime: '2000-01-01 00:00',
      EditEmployeeName: '',
      workshopName: '',
      productLineName: '',
      remark: '',
      Attachment: '',
      roomName: '请选择',
      roomId: '',
      room: [],
      outStorageCode: '',
      materialInfos: [],
      //维修主管
      MaintainManagers: [],
      MainManager: [],
      MainManagerId: '',
      MainManagerSelect: '',
      //维修主管-扫码
      InspectorId: '',
      InspectorName: '',
      InspectorSelect: '请选择',
      isGetInspector: false,
      isInspectordelete: false,
      //是否扫码成功
      isGetcomID: false,
      //长按删除功能控制
      iscomIDdelete: false,
      //底栏控制
      bottomData: [],
      bottomVisible: false,
      datepickerVisible: false,
      CameraVisible: false,
      response: '',
      //照片控制
      pictureSelect: false,
      pictureUri: '',
      imageArr: [],
      imageFileArr: [],
      fileid: "",
      recid: "",
      isPhotoUpdate: false,
      imageOverSize: false,
      //
      GetError: false,
      buttondisable: false,
      //卡片
      isEquipVisible: false,
      focusIndex: 3,
      isCheckPage: false,
      isLoading: true,
      isUsedSpareParts: '', //备品备件是否出库
      //控制app拍照，日期，相册选择功能
      isAppPhotoSetting: false,
      currShowImgIndex: 0,
      isAppDateSetting: false,
      isAppPhotoAlbumSetting: false
    }
    //this.requestCameraPermission = this.requestCameraPermission.bind(this)
  }

  componentDidMount() {
    const { navigation } = this.props;
    let guid = navigation.getParam('guid') || ''
    let pageType = navigation.getParam('pageType') || ''
    let title = navigation.getParam('title') || ''
    let isUsedSpareParts = Url.isUsedSpareParts || '0'
    let isAppPhotoSetting = navigation.getParam('isAppPhotoSetting')
    let isAppDateSetting = navigation.getParam('isAppDateSetting')
    let isAppPhotoAlbumSetting = navigation.getParam('isAppPhotoAlbumSetting')
    this.setState({
      isAppPhotoSetting: isAppPhotoSetting,
      isAppDateSetting: isAppDateSetting,
      isAppPhotoAlbumSetting: isAppPhotoAlbumSetting
    })
    this.setState({
      title: title,
      isUsedSpareParts: isUsedSpareParts
    })
    if (pageType == 'CheckPage' || title == '设备维修确认') {
      this.checkResData(guid)
      this.setState({
        isCheckPage: true,
        pageType: pageType
      })
    } else {
      this.resData()
    }

    //通过使用DeviceEventEmitter模块来监听事件
    this.iDataScan = DeviceEventEmitter.addListener('iDataScan', (Event) => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 115 ~ SteelCage ~ Event.ScanResult", Event.ScanResult)
      if (typeof Event.ScanResult != undefined) {
        let data = Event.ScanResult
        let arr = data.split("=");
        let id = ''
        id = arr[arr.length - 1]
        console.log("🚀 ~ file: concrete_pouring.js ~ line 118 ~ SteelCage ~ id", id)
        if (this.state.type == 'InspectorId') {
          this.GetInspector(id)
        }

      }
    });
  }

  componentWillUnmount() {
    imageArr = [], imageFileArr = [], careIdStr = '', careIdArr = []
    this.iDataScan.remove()
  }

  UpdateControl = () => {
    if (typeof this.props.navigation.getParam('QRid') != "undefined") {
      if (this.props.navigation.getParam('QRid') != "") {
        this.toast.show(Url.isLoadingView, 0)
      }
    }
    const { navigation } = this.props;
    const { isGetcomID, isGetInspector, isInspectordelete, iscomIDdelete } = this.state
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || ''
    let type = navigation.getParam('type') || '';

    if (type == 'compid') {
      this.GetcomID(QRid)
    } else if (type == 'InspectorId') {
      this.GetInspector(QRid)
    } else if (type == 'compid2') {
      this.scanMaterial(QRid)
    }

    if (QRid != '') {
      navigation.setParams({ QRid: '', type: '' })
    }

    let state = navigation.getParam('state') || ""
    let mis = []
    mis = navigation.getParam('materialInfos') || []
    if (state == "addMaterial") {
      this.state.materialInfos.map((item, index) => {
        mis.push(item)
      })
      this.setState({
        materialInfos: mis
      })
    } else if (state == "requisitionCount") {
      let pickingInfoId = navigation.getParam('pickingInfoId') || ""
      let requisitionCount = navigation.getParam('requisitionCount') || "0"
      let remark = navigation.getParam('remark') || ""

      mis = this.state.materialInfos
      mis.map((item, index) => {
        if (item.id == pickingInfoId) {
          item.requisitionCount = requisitionCount
          item.remark = remark
        }
      })
      this.setState({
        materialInfos: mis,
      })
    }
    this.props.navigation.setParams({
      type: "",
      state: "",
      materialInfos: []
    })
  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  checkResData = (guid) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getrepairdetail",
      "servicetype": "pdaservice",
      "express": "9C321E1C",
      "ciphertext": "b72efa21a24a6c3cb26e9ab2f819cb55",
      "data": { "_Rowguid": guid }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: equipment_service.js ~ line 161 ~ EquipmentMaintenance ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: equipment_service.js ~ line 217 ~ EquipmentMaintenance ~ resData", resData)

      let rowguid = resData.result[0]._Rowguid
      let formCode = resData.result[0].FormCode
      let ServerTime = resData.result[0].EditDate
      let Attachment = ''
      if (resData.result[0].Attachment.length == 0) {
        Attachment = resData.result[0].AttachmentMain
      } else {
        Attachment = resData.result[0].Attachment
      }
      let equipId = resData.result[0].EquipId
      let equipName = resData.result[0].EquipName
      let equipSize = resData.result[0].EquipSize
      let workshopName = resData.result[0].WorkshopName
      let productLineName = resData.result[0].ProductLineName
      let EditEmployeeName = resData.result[0].EditEmployeeName
      let Remark = resData.result[0].Remark
      let errremark = resData.result[0].ErrRemark
      let mainremark = resData.result[0].MainRemark
      let remainremark = resData.result[0].RemainRemark
      let accremark = resData.result[0].AccRemark
      let beginDate = resData.result[0].BeginDate
      let endDate = resData.result[0].EndDate
      let EditDate = resData.result[0].EditDate
      let ReportForRepairName = resData.result[0].ReportForRepairName
      let EquipMainApplicationSub = JSON.parse(resData.result[0].EquipMainApplicationSub)
      let room = typeof resData.result[0].roomName == "undefined" ? "" : []
      let roomName = typeof resData.result[0].roomName == "undefined" ? "" : resData.result[0].roomName
      let roomId = typeof resData.result[0].roomName == "undefined" ? "" : resData.result[0].roomId
      let outStorageCode = typeof resData.result[0].roomName == "undefined" ? "" : resData.result[0].outStorageCode

      //图片数组
      let imageArr = [], imageFileArr = [];
      let tmp = {}
      tmp.fileid = ""
      tmp.uri = Attachment
      tmp.url = Attachment
      imageFileArr.push(tmp)
      this.setState({
        imageFileArr: imageFileArr
      })
      imageArr.push(Attachment)

      //产品子表
      let compData = {}
      compData.EquipId = equipId
      compData.EquipName = equipName
      compData.EquipSize = equipSize
      compData.WorkshopName = workshopName
      compData.ProductLineName = productLineName
      compData.EditDate = EditDate
      compData.EditEmployeeName = EditEmployeeName
      compData.Remark = Remark
      if (typeof EquipMainApplicationSub != 'undefined') {
        this.setState({
          materialInfos: EquipMainApplicationSub,
          roomName: roomName,
          roomId: roomId,
          outStorageCode: outStorageCode
        })
      }

      this.setState({
        resData: resData,
        formCode: formCode,
        formCodeSelect: formCode,
        rowguid: rowguid,
        ServerTime: ServerTime,
        EditEmployeeName: EditEmployeeName,
        compData: compData,
        remark: Remark,
        equipId: equipId,
        equipName: equipName,
        equipSize: equipSize,
        workshopName: workshopName,
        productLineName: productLineName,
        ReportForRepairName: ReportForRepairName,
        InspectorSelect: ReportForRepairName,
        imageArr: imageArr,
        remainremark: remainremark,
        errremark: errremark,
        accremark: accremark,
        mainremark: mainremark,
        beginDate: beginDate,
        endDate: endDate,
        room: room,
        isEquipVisible: true,
        pictureSelect: true,
        isGetcomID: true
      })
      this.setState({
        isLoading: false,
      })
    }).catch((error) => {
    });
  }

  resData = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getrepairinitlist",
      "servicetype": "pdaservice",
      "express": "8AC4C0B0",
      "ciphertext": "7df83f712027c63f147f6c2d4a43f08d",
      "data": {
        "_bizid": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: equipment_service.js ~ line 332 ~ EquipmentMaintenance ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: equipment_service.js ~ line 344 ~ EquipmentMaintenance ~ resData", resData)
      //初始化错误提示
      if (resData.status == 100) {
        let ServerTime = resData.result.ServerTime
        let MonitorSetup = resData.result.MonitorSetup
        let MaintainApplitions = resData.result.MaintainApplitions
        let MaintainManagers = resData.result.MaintainManagers
        let room = typeof resData.result.room == "undefined" ? "" : resData.result.room
        if (MonitorSetup != '扫码') {
          this.setState({
            InspectorId: Url.PDAEmployeeId,
            InspectorName: Url.PDAEmployeeName,
            InspectorSelect: Url.PDAEmployeeName,
          })
        }
        this.setState({
          resData: resData,
          ServerTime: ServerTime,
          beginDate: ServerTime,
          endDate: ServerTime,
          MonitorSetup: MonitorSetup,
          MaintainApplitions: MaintainApplitions,
          MaintainManagers: MaintainManagers,
          room: room,
          isLoading: false
        })

        DeviceStorage.get('DeviceStorageDataEquipment')
          .then(res => {
            if (res) {
              let DeviceStorageDataObj = JSON.parse(res)
              DeviceStorageData = DeviceStorageDataObj
              if (res.length != 0) {
                MaintainManagers.map((item, index) => {
                  if (DeviceStorageData.perId.indexOf(item.MainManagerId) != -1) {
                    this.setState({
                      "InspectorId": DeviceStorageDataObj.perId,
                      "InspectorSelect": DeviceStorageDataObj.perName,
                      "InspectorName": DeviceStorageDataObj.perName,
                    })
                  }
                })
              }
            }
          }).catch(err => {
            console.log("🚀 ~ file: loading.js ~ line 131 ~ SteelCage ~ componentDidMount ~ err", err)
          })
      }
      else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }

    }).catch((error) => {
      console.log("🚀 ~ file: equipment_service.js ~ line 405 ~ EquipmentMaintenance ~ error", error)
    });

  }

  GetcomID = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getrepairequipinfo",
      "servicetype": "pdaservice",
      "express": "0F51EF23",
      "ciphertext": "a91d6dc5c5a20f2a35939b6e2d91a0ef",
      "data": {
        "equipguid": id,
        "_bizid": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let compData = resData.result
        let equipguid = resData.result.Equipguid
        let equipId = resData.result.EquipId
        let equipName = resData.result.EquipName
        let equipSize = resData.result.EquipSize
        let workshopName = resData.result.WorkshopName
        let productLineName = resData.result.ProductLineName
        this.setState({
          compData: compData,
          equipguid: equipguid,
          equipId: equipId,
          equipName: equipName,
          equipSize: equipSize,
          workshopName: workshopName,
          productLineName: productLineName,
          isEquipVisible: true,
          isGetcomID: true
        })
      } else {
        console.log('resData', resData)
        Alert.alert('ERROR', resData.message)
        varGetError = true
        this.setState({
          GetError: true
        })
      }

    }).catch(err => {
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  comIDItem = () => {
    const { isGetcomID, buttondisable, equipId } = this.state
    return (
      <View>
        {
          !isGetcomID ?
            <View>
              <ScanButton

                onPress={() => {
                  this.setState({
                    iscomIDdelete: false,
                    buttondisable: true,
                    focusIndex: '1'
                  })
                  setTimeout(() => {
                    this.setState({ GetError: false, buttondisable: false })
                    varGetError = false
                  }, 1500)
                  this.props.navigation.navigate('QRCode', {
                    type: 'compid',
                    page: 'EquipmentRepair'
                  })
                }}
              />
            </View>
            :
            <TouchableCustom
              onPress={() => {
                console.log('onPress')
              }}
              onLongPress={() => {
                console.log('onLongPress')
                Alert.alert(
                  '提示信息',
                  '是否删除构件信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      this.setState({
                        compData: {},
                        equipguid: '',
                        equipId: '',
                        equipName: '',
                        equipSize: '',
                        workshopName: '',
                        productLineName: '',
                        iscomIDdelete: true,
                        isGetcomID: false,
                        isEquipVisible: false,
                      })
                    }
                  }])
              }} >
              <Text>{equipId}</Text>
            </TouchableCustom>
        }
      </View>

    )
  }

  GetInspector = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getInspector",
      "servicetype": "pda",
      "express": "CDAB7A2D",
      "ciphertext": "a590b603df9b5a4384c130c7c21befb7",
      "data": {
        "InspectorId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let InspectorId = resData.result.InspectorId
        let InspectorName = resData.result.InspectorName
        this.setState({
          InspectorId: InspectorId,
          InspectorName: InspectorName,
          MainManagerId: InspectorId,
          MainManager: InspectorName,
          MainManagerSelect: InspectorName,
          isGetInspector: true,
          isInspectordelete: false
        })
      } else {
        Alert.alert('ERROR', resData.message)
        varGetError = true
        this.setState({
          GetError: true
        })
      }

    }).catch(err => {
    })
  }

  inspectorItem = (index) => {
    const { isGetInspector, buttondisable, InspectorName, MonitorSetup, InspectorSelect, MaintainManagers } = this.state
    return (
      <View>
        {
          MonitorSetup == '扫码' ?
            (
              !isGetInspector ?
                <View>
                  <ScanButton
                    onPress={() => {
                      this.setState({
                        isInspectordelete: false,
                        buttondisable: true,
                        focusIndex: 3
                      })
                      this.props.navigation.navigate('QRCode', {
                        type: 'InspectorId',
                        page: 'EquipmentService'
                      })
                    }}
                    onLongPress={() => {
                      this.setState({
                        type: 'InspectorId',
                        focusIndex: index
                      })
                      NativeModules.PDAScan.onScan();
                    }}
                    onPressOut={() => {
                      NativeModules.PDAScan.offScan();
                    }}
                  />
                </View>
                :
                <TouchableCustom
                  onPress={() => {
                    console.log('onPress')
                  }}
                  onLongPress={() => {
                    console.log('onLongPress')
                    Alert.alert(
                      '提示信息',
                      '是否删除质检员信息',
                      [{
                        text: '取消',
                        style: 'cancel'
                      }, {
                        text: '删除',
                        onPress: () => {
                          this.setState({
                            InspectorId: '',
                            InspectorName: '',
                            isGetInspector: false,
                            isInspectordelete: true
                          })
                        }
                      }])
                  }} >
                  <Text>{InspectorName}</Text>
                </TouchableCustom>
            ) :
            (
              <IconDown text={InspectorSelect} />
            )
        }
      </View>
    )
  }

  GetEquipcarecontent = (id) => {
    let formData = new FormData();
    const { equipguid } = this.state
    let data = {};
    data = {
      "action": "getEquipcarecontent",
      "servicetype": "pda",
      "express": "0F51EF23",
      "ciphertext": "a91d6dc5c5a20f2a35939b6e2d91a0ef",
      "data": {
        "careCycleId": id,
        "factoryId": Url.PDAFid,
        "equipGuid": equipguid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: equipment_maintenance.js ~ line 303 ~ EquipmentMaintenance ~ JSON.stringify(data)", JSON.stringify(data))

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let careSubData = resData.result
        let equipCareContent = resData.result.equipCareContent
        this.setState({
          careSubData: careSubData,
          equipCareContent: equipCareContent,
        })
      } else {
        console.log('resData', resData)
        Alert.alert('ERROR', resData.message)
      }
    }).catch(err => {
    })
  }

  PostData = () => {
    const { ServerTime, rowguid, endDate, beginDate, errremark, remainremark, accremark, mainremark, formCode, InspectorId, InspectorName, remark, materialInfos, roomName, roomId, recid } = this.state
    const { navigation } = this.props;
    const QRid = navigation.getParam('QRid') || '';


    if (formCode == '') {
      this.toast.show('设备编码不能为空');
      return
    }
    if (InspectorId.length == 0) {
      this.toast.show('维修主管不能为空');
      return
    }
    if (this.state.isAppPhotoSetting) {
      if (imageArr.length == 0) {
        this.toast.show('请先拍摄检查照片');
        return
      }
    }


    let message = ''
    if (materialInfos.length > 0) {
      materialInfos.map((item, index) => {
        if (item.requisitionCount == '0' || item.requisitionCount == '') {
          message = "材料编码" + item.code + "，请选择申领数量"
          return
        }
      })
    }
    if (message != "") {
      this.toast.show(message)
      return
    }

    this.toast.show(saveLoading, 0)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "saverepair",
      "servicetype": "pdaservice",
      "express": "939B066B",
      "ciphertext": "fee35451796126acd3f799bf037992dc",
      "data": {
        "beginDate": beginDate,
        "remainremark": remainremark,
        "errremark": errremark,
        "endDate": endDate,
        "mainmanagerid": InspectorId,
        "accremark": accremark,
        "mainPerson": Url.PDAEmployeeName,
        "mainmanager": InspectorName,
        "userName": Url.PDAusername,
        "_bizid": Url.PDAFid,
        "rowguid": rowguid,
        "mainremark": mainremark,
        "roomName": roomName,
        "roomId": roomId,
        "materialInfos": materialInfos,
        "recid": recid
      }
    }

    formData.append('jsonParam', JSON.stringify(data))
    if (!Url.isAppNewUpload) {
      imageArr.map((item, index) => {
        formData.append('img_' + index, {
          uri: item,
          type: 'image/jpeg',
          name: 'img_' + index
        })
      })
    }
    console.log('========================' + formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log('============' + JSON.stringify(resData))
      if (resData.status == '100') {
        this.toast.show('保存成功');
        setTimeout(() => {
          this.props.navigation.replace(this.props.navigation.getParam("page"), {
            title: this.props.navigation.getParam("title"),
            pageType: this.props.navigation.getParam("pageType"),
            page: this.props.navigation.getParam("page"),
            isAppPhotoSetting: this.props.navigation.getParam("isAppPhotoSetting"),
            isAppDateSetting: this.props.navigation.getParam("isAppDateSetting"),
            isAppPhotoAlbumSetting: this.props.navigation.getParam("isAppPhotoAlbumSetting"),
          })
          //this.props.navigation.navigate('PDAMainStack')
        }, 400)
      } else {
        this.toast.show('保存失败')
        Alert.alert('保存失败', resData.message)
        console.log('resData', resData)
      }
    }).catch((error) => {
      this.toast.show('保存失败')
      if (error.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('保存失败', "响应内容不是合法JSON格式")
        return
      }
      if (error.toString().indexOf('Network request faile') != -1) {
        Alert.alert('保存失败', "网络请求错误")
        return
      }
      Alert.alert('保存失败', error.toString())
    });
  }

  PostData2 = () => {
    const { rowguid, } = this.state
    const { navigation } = this.props;
    const QRid = navigation.getParam('QRid') || '';

    this.toast.show(saveLoading, 0)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "saverepairconfirm",
      "servicetype": "pdaservice",
      "express": "939B066B",
      "ciphertext": "fee35451796126acd3f799bf037992dc",
      "data": {
        "rowguid": rowguid,
        "confirmPerson": Url.PDAEmployeeName
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('保存成功');
        setTimeout(() => {
          this.props.navigation.goBack()
          //this.props.navigation.navigate('PDAMainStack')
        }, 400)
      } else {
        this.toast.show('保存失败')
        Alert.alert('保存失败', resData.message)
        console.log('resData', resData)
      }
    }).catch((error) => {
      this.toast.show('保存失败')
      if (error.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('保存失败', "响应内容不是合法JSON格式")
        return
      }
      if (error.toString().indexOf('Network request faile') != -1) {
        Alert.alert('保存失败', "网络请求错误")
        return
      }
      Alert.alert('保存失败', error.toString())
    });
  }

  cameraFunc = () => {
    launchCamera(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: parseInt(Url.isResizePhoto),
        maxWidth: parseInt(Url.isResizePhoto),
        saveToPhotos: true,

      },
      (response) => {
        if (response.uri == undefined) {
          return
        }
        imageArr.push(response.uri)
        this.setState({
          //pictureSelect: true,
          pictureUri: response.uri,
          imageArr: imageArr
        })
        if (Url.isAppNewUpload) {
          this.cameraPost(response.uri)
        } else {
          this.setState({
            pictureSelect: true,
          })
        }
        console.log(response)
      },
    )
  }

  camLibraryFunc = () => {
    launchImageLibrary(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: parseInt(Url.isResizePhoto),
        maxWidth: parseInt(Url.isResizePhoto),
      },
      (response) => {
        if (response.uri == undefined) {
          return
        }
        imageArr.push(response.uri)
        this.setState({
          //pictureSelect: true,
          pictureUri: response.uri,
          imageArr: imageArr
        })
        if (Url.isAppNewUpload) {
          this.cameraPost(response.uri)
        } else {
          this.setState({
            pictureSelect: true,
          })
        }
        console.log(response)
      },
    )
  }

  camandlibFunc = () => {
    Alert.alert(
      '提示信息',
      '选择拍照方式',
      [{
        text: '取消',
        style: 'cancel'
      }, {
        text: '拍照',
        onPress: () => {
          this.cameraFunc()
        }
      }, {
        text: '相册',
        onPress: () => {
          this.camLibraryFunc()
        }
      }])
  }

   // 展示/隐藏 放大图片
  handleZoomPicture = (flag, index) => {
    this.setState({
      imageOverSize: false,
      currShowImgIndex: index || 0
    })
  }

  // 加载放大图片弹窗
  renderZoomPicture = () => {
    const { imageOverSize, currShowImgIndex, imageFileArr } = this.state
    return (
      <OverlayImgZoomPicker
        isShowImage={imageOverSize}
        currShowImgIndex={currShowImgIndex}
        zoomImages={imageFileArr}
        callBack={(flag) => this.handleZoomPicture(flag)}
      ></OverlayImgZoomPicker>
    )
  }

  cameraPost = (uri) => {
    const { recid } = this.state
    this.setState({
      isPhotoUpdate: true
    })
    this.toast.show('图片上传中', 2000)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "savephote",
      "servicetype": "photo_file",
      "express": "D2979AB2",
      "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
      "data": {
        "fileflag": fileflag,
        "username": Url.PDAusername,
        "recid": recid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    formData.append('img', {
      uri: uri,
      type: 'image/jpeg',
      name: 'img'
    })
    console.log("🚀 ~ file: concrete_pouring.js ~ line 672 ~ SteelCage ~ formData", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 690 ~ SteelCage ~ resData", resData)
      this.toast.close()
      if (resData.status == '100') {
        let fileid = resData.result.fileid;
        let recid = resData.result.recid;

        let tmp = {}
        tmp.fileid = fileid
        tmp.uri = uri
        tmp.url = uri
        imageFileArr.push(tmp)
        this.setState({
          fileid: fileid,
          recid: recid,
          pictureSelect: true,
          imageFileArr: imageFileArr
        })
        console.log("🚀 ~ file: concrete_pouring.js ~ line 704 ~ SteelCage ~ imageFileArr", imageFileArr)
        this.toast.show('图片上传成功');
        this.setState({
          isPhotoUpdate: false
        })
      } else {
        Alert.alert('图片上传失败', resData.message)
        this.toast.close(1)
      }
    }).catch((error) => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 692 ~ SteelCage ~ error", error)
    });
  }

  cameraDelete = (item) => {
    let i = imageArr.indexOf(item)

    if (Url.isAppNewUpload) {
      let formData = new FormData();
      let data = {};
      data = {
        "action": "deletephote",
        "servicetype": "photo_file",
        "express": "D2979AB2",
        "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
        "data": {
          "fileid": imageFileArr[i].fileid,
        }
      }
      formData.append('jsonParam', JSON.stringify(data))
      fetch(Url.PDAurl, {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        body: formData
      }).then(res => {
        console.log(res.statusText);
        console.log(res);
        return res.json();
      }).then(resData => {
        if (resData.status == '100') {
          if (i > -1) {
            imageArr.splice(i, 1)
            imageFileArr.splice(i, 1)
            this.setState({
              imageArr: imageArr,
              imageFileArr: imageFileArr
            }, () => {
              this.forceUpdate()
            })
          }
          if (imageArr.length == 0) {
            this.setState({
              pictureSelect: false
            })
          }
          this.toast.show('图片删除成功');
        } else {
          Alert.alert('图片删除失败', resData.message)
          this.toast.close(1)
        }
      }).catch((error) => {
        console.log("🚀 ~ file: concrete_pouring.js ~ line 761 ~ SteelCage ~ cameraDelete ~ error", error)
      });
    } else {
      if (i > -1) {
        imageArr.splice(i, 1)
        this.setState({
          imageArr: imageArr,
        }, () => {
          this.forceUpdate()
        })
      }
      if (imageArr.length == 0) {
        this.setState({
          pictureSelect: false
        })
      }
    }
  }

  imageView = () => {
    return (
      <View style={{ flexDirection: 'row' }}>
        {
          this.state.pictureSelect ?
            this.state.imageArr.map((item, index) => {
              return (
                <AvatarImg
                  item={item}
                  index={index}
                  onPress={() => {
                    this.setState({
                      imageOverSize: true,
                      pictureUri: item
                    })
                  }}
                  onLongPress={() => {
                    Alert.alert(
                      '提示信息',
                      '是否删除构件照片',
                      [{
                        text: '取消',
                        style: 'cancel'
                      }, {
                        text: '删除',
                        onPress: () => {
                          this.cameraDelete(item)
                        }
                      }])
                  }} />
              )
            }) : <View></View>
        }
      </View>
    )
  }

  isBottomVisible = (data, focusIndex) => {
    if (typeof (data) != "undefined") {
      if (data.length > 0) {
        this.setState({ bottomVisible: true, bottomData: data, focusIndex: focusIndex })
      }
    } else {
      this.toast.show("无数据")
    }
  }

  _DatePicker = (Time, focusIndex) => {
    const { ServerTime } = this.state
    return (
      <DatePicker
        customStyles={{
          dateInput: styles.dateInput,
          dateTouchBody: styles.dateTouchBody,
          dateText: this.state.isAppDateSetting ? styles.dateText : styles.dateDisabled,
        }}

        onOpenModal={() => {
          this.setState({ focusIndex: focusIndex })
        }}
        iconComponent={<Icon name='caretdown' color={this.state.isAppDateSetting ? '#419fff' : "#666"} iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
        }
        showIcon={true}
        mode='datetime'
        date={Time}
        disabled={!this.state.isAppDateSetting}
        format="YYYY-MM-DD HH:mm"
        onDateChange={(value) => {
          this.setState({
            Time: value
          })
        }}
      />
    )
  }

  scanMaterial = (guid) => {
    const { projectId, roomId, materialInfos, title } = this.state
    let formData = new FormData();
    let data = {};
    let materialIds = ''
    materialInfos.map((item, index) => {
      materialIds += index == materialInfos.length - 1 ? item.id : item.id + ','
    })
    data = {
      "action": "selectequipmaterialscan",
      "servicetype": "pdamaterial",
      "express": "6DFD1C9B",
      "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
      "data": {
        "factoryId": Url.PDAFid,
        "rowguid": guid,
        "roomId": roomId,
        "materialIds": materialIds,
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: EquipmentService.js ~ line 1003 ~ EquipmentService ~ formData:", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      //console.log(res.statusText);
      //console.log(res);
      return res.json();
    }).then(resData => {
      console.log("EquipmentMaintenance ---- 1016 ---- resData:" + JSON.stringify(resData))
      this.toast.close()
      if (resData.status == '100') {
        let mis = materialInfos
        let materialIds = ''
        mis.map((item, index) => {
          materialIds += index == mis.length - 1 ? item.id : item.id + ','
        })
        let result = resData.result
        if (result.length > 1) {
          setTimeout(() => {
            this.props.navigation.navigate('SelectMaterial', {
              title: '选择备品备件',
              action: '设备维修',
              roomId: roomId,
              materialIds: materialIds,
              keyStr: result[0].code
            })
          }, 1000)
        } else {
          if (mis != []) {
            result.map((item, index) => {
              let check = true
              materialInfos.map((item2, index2) => {
                if (item.id == item2.id) {
                  check = false
                  return
                }
              })
              if (check) mis.push(item)
            })
          } else {
            mis = result
          }
          this.setState({
            materialInfos: mis,
          })
        }
      } else {
        Alert.alert("错误提示", resData.message)
      }

    }).catch((error) => {
      console.log("🚀 ~ file: EquipmentService.js ~ line 1068 ~ EquipmentService ~ error:", error)
    });
  }

  comIDItem2 = (index) => {
    const { roomId, materialInfos } = this.state
    return (
      <View>
        {
          <View style={{ flexDirection: 'row' }}>

            <Button
              titleStyle={{ fontSize: 14 }}
              title='选择材料'
              type='solid'
              buttonStyle={{ paddingVertical: 6, backgroundColor: '#18BC28', marginVertical: 0 }}
              onPress={() => {
                let materialIds = ''
                materialInfos.map((item, index) => {
                  materialIds += index == materialInfos.length - 1 ? item.id : item.id + ','
                })
                this.props.navigation.navigate('SelectMaterial', {
                  title: '选择备品备件',
                  action: '设备维修',
                  roomId: roomId,
                  materialIds: materialIds
                })
              }}
            />
            <Text>   </Text>
            <ScanButton
              disabled={false}
              onPress={() => {
                this.props.navigation.navigate('QRCode', {
                  type: 'compid2',
                  page: 'EquipmentService'
                })
              }}
              onLongPress={() => {
                this.setState({
                  type: 'compid2',
                  focusIndex: index
                })
                NativeModules.PDAScan.onScan();
              }}
              onPressOut={() => {
                NativeModules.PDAScan.offScan();
              }}
            />
          </View>
        }
      </View>
    )
  }

  jumpMaterialPickingInfo = (item) => {
    const { title } = this.state
    this.props.navigation.navigate('MaterialPickingInfo', {
      title: '设备维修',
      action: title,
      storageId: item.id,
      storageCode: item.code,
      storageName: item.name,
      size: item.size,
      unit: item.unit,
      stockCount2: item.stockCount,
      requisitionCount: item.requisitionCount,
      remark: item.remark
    })
  }

  render() {
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || ''
    const type = navigation.getParam('type') || '';

    if (type == 'compid') {
      QRcompid = QRid
    }
    const { buttondisable, MaintainManagers, isGetInspector, isGetcomID, ServerTime, beginDate, endDate, remark, focusIndex, isEquipVisible, compData, MonitorSetup, equipId, formCode, formCodeSelect, mainremark, errremark, accremark, remainremark, MaintainApplitions, bottomVisible, bottomData, isCheckPage, room, roomId, roomName, materialInfos, title, outStorageCode, pageType, isUsedSpareParts } = this.state

    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          <NavigationEvents
            onWillFocus={this.UpdateControl} />
          <ScrollView ref={ref => this.scoll = ref} style={styles.mainView} showsVerticalScrollIndicator={false} >
            <View style={styles.listView}>
              <ListItem
                containerStyle={{ paddingBottom: 6 }}
                leftAvatar={
                  <View >
                    <View style={{ flexDirection: 'column' }} >
                      {
                        isCheckPage ? <View></View> :
                          <AvatarAdd
                            pictureSelect={this.state.pictureSelect}
                            backgroundColor='rgba(77,142,245,0.20)'
                            color='#4D8EF5'
                            title="构"
                            onPress={() => {
                              if (this.state.isAppPhotoAlbumSetting) {
                                this.camandlibFunc()
                              } else {
                                this.cameraFunc()
                              }
                            }} />
                      }
                      {this.imageView()}
                    </View>
                  </View>
                }
              />
              <TouchableCustom underlayColor={'lightgray'} onPress={() => { this.isBottomVisible(MaintainApplitions, '0') }} >
                <ListItemScan
                  focusStyle={focusIndex == '0' ? styles.focusColor : {}}
                  bottomDivider
                  title='表单编号'
                  rightElement={
                    <IconDown text={formCodeSelect} />
                  }
                />
              </TouchableCustom>

              {isEquipVisible ? <ListItemScan
                title='设备编码'
                rightTitle={equipId}
              /> : <View></View>}
              {
                isEquipVisible ?
                  this.EquipCardList(compData)
                  : <View></View>
              }

              <ListItemScan
                focusStyle={focusIndex == '1' ? styles.focusColor : {}}
                title='开始日期'
                rightElement={
                  this._DatePicker(beginDate, 1)
                }
              //rightTitle={ServerTime}
              />

              <ListItemScan
                focusStyle={focusIndex == '2' ? styles.focusColor : {}}
                title='结束日期'
                rightElement={
                  this._DatePicker(endDate, 2)
                }
              //rightTitle={ServerTime}
              />
              <ListItemScan
                title='操作人'
                rightTitle={Url.PDAEmployeeName}
              />

              <TouchableCustom underlayColor={'lightgray'} onPress={() => {
                if (MonitorSetup != '扫码') {
                  this.isBottomVisible(MaintainManagers, 3)
                } else {
                  this.setState({
                    type: 'InspectorId',
                    focusIndex: 3
                  })
                  NativeModules.PDAScan.onScan();
                }
              }}
                onPressIn={() => {
                  if (MonitorSetup == '扫码') {
                    this.setState({
                      type: 'InspectorId',
                      focusIndex: 3
                    })
                    NativeModules.PDAScan.onScan();
                  }
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}>
                <ListItemScan
                  isButton={MonitorSetup == '扫码' && !isGetInspector}
                  focusStyle={focusIndex == '3' ? styles.focusColor : {}}
                  title='维修主管'

                  rightElement={
                    this.inspectorItem('3')
                  }
                />
              </TouchableCustom>

              <ListItemScan
                focusStyle={focusIndex == '4' ? styles.focusColor : {}}
                title='故障原因分析'
                rightElement={
                  title == '设备维修确认' ? errremark :
                    <Input
                      ref={ref => { this.input0 = ref }}
                      onSubmitEditing={() => { this.input1.focus() }}
                      containerStyle={styles.quality_input_container}
                      inputContainerStyle={styles.inputContainerStyle}
                      inputStyle={[styles.quality_input_, { top: 1 }]}
                      placeholder='请输入'
                      value={errremark}
                      //onFocus={() => { this.setState({ focusIndex: 4 }) }}
                      onChangeText={(value) => {
                        this.setState({
                          errremark: value
                        })
                      }} />
                }
              />
              <ListItemScan
                focusStyle={focusIndex == '5' ? styles.focusColor : {}}
                title='维修记录'
                rightElement={
                  title == '设备维修确认' ? mainremark :
                    <Input
                      ref={ref => { this.input1 = ref }}
                      onSubmitEditing={() => { this.input2.focus() }}
                      containerStyle={styles.quality_input_container}
                      inputContainerStyle={styles.inputContainerStyle}
                      inputStyle={[styles.quality_input_, { top: 1 }]}
                      placeholder='请输入'
                      value={mainremark}
                      //onFocus={() => { this.setState({ focusIndex: 5 }) }}
                      onChangeText={(value) => {
                        this.setState({
                          mainremark: value
                        })
                      }} />
                }
              />
              <ListItemScan
                focusStyle={focusIndex == '6' ? styles.focusColor : {}}
                title='未修复原因'
                rightElement={
                  title == '设备维修确认' ? remainremark :
                    <Input
                      ref={ref => { this.input2 = ref }}
                      onSubmitEditing={() => { this.input3.focus() }}
                      containerStyle={styles.quality_input_container}
                      inputContainerStyle={styles.inputContainerStyle}
                      inputStyle={[styles.quality_input_, { top: 1 }]}
                      placeholder='请输入'
                      value={remainremark}
                      //onFocus={() => { this.setState({ focusIndex: 6 }) }}
                      onChangeText={(value) => {
                        this.setState({
                          remainremark: value
                        })
                      }} />
                }
              />
              <ListItemScan
                bottomDivider
                containerStyle={focusIndex == 7 ? { backgroundColor: '#A9E2F3' } : {}}
                title='更换备品备件记录'
                rightElement={
                  title == '设备维修确认' ? accremark :
                    <Input
                      ref={ref => { this.input3 = ref }}
                      containerStyle={styles.quality_input_container}
                      inputContainerStyle={styles.inputContainerStyle}
                      inputStyle={[styles.quality_input_, { top: 1 }]}
                      placeholder='请输入'
                      value={accremark}
                      //onFocus={() => { this.setState({ focusIndex: 7 }) }}
                      onChangeText={(value) => {
                        this.setState({
                          accremark: value
                        })
                      }} />

                }
              />

              {
                room ?
                  <TouchableCustom underlayColor={'lightgray'} onPress={() => {
                    this.isBottomVisible(room, '10')
                  }} >
                    <ListItemScan
                      focusStyle={focusIndex == '10' ? styles.focusColor : {}}
                      title='库房名称'
                      rightElement={
                        <IconDown text={roomName} />
                      }
                    />
                  </TouchableCustom>
                  : <View />
              }
              {
                outStorageCode && isCheckPage ?
                  <ListItemScan focusStyle={focusIndex == '11' ? styles.focusColor : {}} title='出库单号:' rightElement={outStorageCode} />
                  : <View />
              }
              {
                roomId && !isCheckPage ?
                  //<View style={{ flexDirection: 'row', paddingHorizontal: 10, justifyContent: 'center', paddingVertical: 11 }}>
                  //    <Button
                  //        titleStyle={{ fontSize: 14 }}
                  //        title='选择备品备件'
                  //        type='solid'
                  //        buttonStyle={{ paddingVertical: 6, backgroundColor: '#17BC29', marginVertical: 0 }}
                  //        onPress={() => {
                  //            let materialIds = ''
                  //            materialInfos.map((item, index) => {
                  //                materialIds += index == materialInfos.length - 1 ? item.id : item.id + ','
                  //            })
                  //            this.props.navigation.navigate('SelectMaterial', {
                  //                title: '选择备品备件',
                  //                action: '设备维修',
                  //                roomId: roomId,
                  //                materialIds: materialIds
                  //            })
                  //        }}
                  //    />
                  //</View>
                  <ListItemScan focusStyle={focusIndex == '12' ? styles.focusColor : {}} title='材料明细:' rightElement={isCheckPage ? <View /> :
                    <View>
                      {this.comIDItem2(12)}
                    </View>}
                  />
                  : <View />
              }
              {
                materialInfos.map((item, index) => {
                  return (
                    <TouchableCustom
                      onLongPress={() => {
                        isCheckPage ? <View /> : Alert.alert(
                          '提示信息',
                          '是否删除备品备件信息',
                          [{
                            text: '取消',
                            style: 'cancel'
                          }, {
                            text: '删除',
                            onPress: () => {
                              let mis = this.state.materialInfos
                              mis.splice(index, 1)
                              this.setState({
                                materialInfos: mis
                              })
                              this.forceUpdate()
                            }
                          }])
                      }}
                      onPress={() => { isCheckPage ? <View /> : this.jumpMaterialPickingInfo(item) }}
                    >
                      <View>
                        <Card containerStyle={styles.card_containerStyle}>
                          <View style={[{
                            flexDirection: 'row',
                            justifyContent: 'flex-start',
                            alignItems: 'center',
                          }]}>
                            <View style={{ flex: 1 }}>
                              <ListItem
                                containerStyle={styles.list_container_style}
                                title={item.code}
                                //rightTitle={}
                                titleStyle={styles.card_titleStyle}
                                rightTitleStyle={styles.rightTitle}
                              />
                              <ListItem
                                containerStyle={styles.list_container_style}
                                title={item.name}
                                //rightTitle={}
                                titleStyle={styles.card_titleStyle}
                                rightTitleStyle={styles.rightTitle}
                              />
                              <ListItem
                                containerStyle={styles.list_container_style}
                                title={item.size}
                                //rightTitle={}
                                titleStyle={styles.card_titleStyle}
                                rightTitleStyle={styles.rightTitle}
                              />
                              <ListItem
                                containerStyle={styles.list_container_style}
                                title={item.requisitionCount + "(" + item.unit + ")"}
                                //rightTitle={'税额:' + item.tax}
                                titleStyle={styles.card_titleStyle}
                                rightTitleStyle={styles.rightTitle}
                              />
                            </View>
                            {
                              isCheckPage ? <View /> :
                                <Icon type='antdesign' name='right' color='#999' onPress={() => { this.jumpMaterialPickingInfo(item) }} />
                            }
                          </View>
                        </Card>
                        <Text />
                      </View>
                    </TouchableCustom>
                  )
                })
              }

            </View>
            {isCheckPage ? <View></View> :
              <FormButton
                onPress={this.PostData}
                title='保存'
                backgroundColor='#17BC29'
                disabled={this.state.isPhotoUpdate}
              />
            }
            {
              title == '设备维修确认' && pageType == 'Scan' ?
                <FormButton
                  onPress={this.PostData2}
                  disabled={this.state.isPhotoUpdate}
                  title='确定'
                  backgroundColor='#17BC29'
                />
                : <View />
            }

             {this.renderZoomPicture()}

            {/* {overlayImg(obj = {
              onRequestClose: () => {
                this.setState({ imageOverSize: !this.state.imageOverSize }, () => {
                  console.log("🚀 ~ file: equipment_maintenance.js:1696 ~ render ~ imageOverSize:", this.state.imageOverSize)
                })
              },
              onPress: () => {
                this.setState({
                  imageOverSize: !this.state.imageOverSize
                })
              },
              uri: this.state.pictureUri,
              isVisible: this.state.imageOverSize
            })
            }*/}

            <BottomSheet
              isVisible={bottomVisible && !isCheckPage}
              // modalProps={{transparent: false, style: {borderColor: '#000', borderWidth:'10'}}}
              onRequestClose={() => {
                this.setState({
                  bottomVisible: false
                })
              }}
              onBackdropPress={() => {
                this.setState({
                  bottomVisible: false
                })
              }}
            >
              <ScrollView style={styles.bottomsheetScroll}>
                {
                  bottomData.map((item, index) => {
                    let title = ''
                    if (item.EquipId) {
                      title = item.FormCode + ' ' + item.EquipName
                    } else if (item.MainManagerId) {
                      title = item.MainManager
                    } else if (item.roomId) {
                      title = item.roomName
                    }
                    return (
                      <TouchableCustom
                        onPress={() => {
                          if (item.EquipId) {
                            this.setState({
                              compData: item,
                              equipId: item.EquipId,
                              rowguid: item._Rowguid,
                              formCode: item.FormCode,
                              formCodeSelect: item.FormCode,
                              isEquipVisible: true,
                              focusIndex: '3',
                              type: 'InspectorId'
                            })
                            setTimeout(() => { this.scoll.scrollTo({ x: 0, y: 600, animated: true }) }, 300)
                          } else if (item.MainManagerId) {
                            this.setState({
                              InspectorId: item.MainManagerId,
                              InspectorName: item.MainManager,
                              InspectorSelect: item.MainManager,
                            })
                            DeviceStorageData = {
                              "perId": item.MainManagerId,
                              "perName": item.MainManager,
                              "perNameselect": item.MainManager
                            }
                            DeviceStorage.save('DeviceStorageDataEquipment', JSON.stringify(DeviceStorageData))

                          } else if (item.roomId) {
                            this.setState({
                              roomId: item.roomId,
                              roomName: item.roomName,
                              materialInfos: []
                            })
                          }
                          this.setState({
                            bottomVisible: false
                          })
                        }}
                      >
                        <BottomItem backgroundColor='white' color="#333" title={title} />
                      </TouchableCustom>
                    )

                  })
                }
              </ScrollView>
              <TouchableCustom
                onPress={() => {
                  this.setState({ bottomVisible: false })
                }}>
                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
              </TouchableCustom>

            </BottomSheet>
          </ScrollView>

        </View>
      )
    }
  }

  EquipCardList = (EquipData) => {
    return (
      <View>
        <Card containerStyle={styles.card_containerStyle}>
          <ListItemScan_child
            title='设备名称'
            rightTitle={EquipData.EquipName}
          />
          <ListItemScan_child
            title='设备型号'
            rightTitle={EquipData.EquipSize}
          />
          <ListItemScan_child
            title='所属车间'
            rightTitle={EquipData.WorkshopName}
          />
          <ListItemScan_child
            title='生产线'
            rightTitle={EquipData.ProductLineName}
          />
          <ListItemScan_child
            title='报修日期'
            rightTitle={EquipData.EditDate}
          />
          <ListItemScan_child
            title='报修人'
            rightTitle={EquipData.EditEmployeeName}
          />
          <ListItemScan_child
            title='问题描述'
            rightTitle={EquipData.Remark}
          />
          <ListItemScan_child
            title='报修图片'
            //rightTitle={EquipData.Attachment}
            rightElement={
              <Text onPress={() => { this.setState({ imageOverSize: true, pictureUri: EquipData.Attachment }) }} style={[styles.rightTitle, { color: '#419FFF', fontSize: 13, textAlign: 'right' }]} numberOfLines={1}>
                查看图片
              </Text>
            }
          />
        </Card>
      </View>
    )
  }

}

class EquipCardList extends React.Component {
  render() {
    const { EquipData } = this.props
    return (
      <View>
        <Card containerStyle={{ borderRadius: 10, shadowOpacity: 0 }}>
          <ListItem
            containerStyle={styles.list_container_style}
            title='设备名称'
            rightTitleProps={{ numberOfLines: 1 }}
            rightTitle={EquipData.EquipName}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='设备型号'
            rightTitleProps={{ numberOfLines: 1 }}
            rightTitle={EquipData.EquipSize}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='所属车间'
            rightTitleProps={{ numberOfLines: 1 }}
            rightTitle={EquipData.WorkshopName}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='生产线'
            rightTitleProps={{ numberOfLines: 1 }}
            rightTitle={EquipData.ProductLineName}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='报修日期'
            rightTitleProps={{ numberOfLines: 1 }}
            rightTitle={EquipData.EditDate}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='报修人'
            rightTitleProps={{ numberOfLines: 1 }}
            rightTitle={EquipData.EditEmployeeName}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='问题描述'
            rightTitleProps={{ numberOfLines: 1 }}
            rightTitle={EquipData.Remark}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='报修图片'
            //rightTitle={EquipData.Attachment}
            rightElement={
              <TouchableCustom onPress={() => { this.setState({ imageOverSize: true, pictureUri: EquipData.Attachment }) }}>
                <Text style={[/* styles.rightTitle */ { color: '#419FFF' }]} numberOfLines={1}>
                  查看图片
                </Text>
              </TouchableCustom>
            }
            titleStyle={styles.title}
          />
        </Card>
      </View>
    )
  }
}