import React from 'react';
import { View, TouchableHighlight, Dimensions, ActivityIndicator, Alert, Image } from 'react-native';
import { Button, Text, Input, Icon, Card, ListItem, BottomSheet, Overlay, Header } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { ScrollView } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker'
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { NavigationEvents } from 'react-navigation';
import { styles } from '../Componment/PDAStyles';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import CheckBoxScan from '../Componment/CheckBoxScan';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import AvatarImg from '../Componment/AvatarImg';
import ListItemScan_child from '../Componment/ListItemScan_child';
import overlayImg from '../Componment/OverlayImg';
import OverlayImgZoomPicker from '../Componment/OverlayImgZoomPicker';
let letGetError = false

let DeviceStorageData = {} //缓存数据

const { height, width } = Dimensions.get('window') //获取宽高

let fileflag = "11"

let imageArr = [], imageFileArr = [];

let checkStandardListAll = [] //危险源列表

let postNameAndCheckStandardInfoAll = [] //所有岗位对应风险源

class BackIcon extends React.PureComponent {
    render() {
        return (
            <TouchableCustom
                onPress={this.props.onPress} >
                <Icon
                    name='arrow-back'
                    color='#419FFF' />
            </TouchableCustom>
        )
    }
}

export default class SafeManageNotice extends React.PureComponent {
    constructor() {
        super();
        this.state = {
            rowguid: "",
            resData: [],
            ServerTime: "", // 时间工具用

            rectifyNoticeInfo: '', // 整改单号
            inspector: '', // 检查人
            issueDate: '', // 签发时间
            rectifyBy: '', // 整改签收人
            teamName: '', // 班组
            teamSelect: '请选择',
            postListByTeam: [], // 班组过滤后岗位
            postName: [], // 岗位
            postSelect: '请选择',
            checkStandard: [], // 危险源
            checkStandardSelect: '请选择',
            rectifyForm: [], // 整改形式
            rectifyFormSelect: '请选择',
            remark: '', // 备注

            //底栏控制
            bottomData: [],
            bottomData2: "",
            bottomVisible: false,
            datepickerVisible: false,
            CameraVisible: false,
            response: '',

            checked: true,
            teamVisible: false, // 班组
            postVisible: false, // 岗位
            checkStandardVisible: false, // 危险源
            checkStandardSaveVisible: true, // 危险源列表
            rectifyFormVisible: false, // 整改期限
            pictureVisible: true, // 问题照片
            imageOverSize: false, // 照片放大
            pictureUri: "",
            imageArr: [],
            imageFileArr: [],
            fileid: "",
            recid: "",
            isPhotoUpdate: false,
            focusIndex: -1,
            isLoading: false,

            //查询界面取消选择按钮
            isCheck: false,
            isCheckPage: false,

            //控制app拍照，日期，相册选择功能
            isAppPhotoSetting: false,
            currShowImgIndex: 0,
            isAppDateSetting: false,
            isAppPhotoAlbumSetting: false
        }
        //this.requestCameraPermission = this.requestCameraPermission.bind(this)
    }

    componentDidMount() {
        const { navigation } = this.props;
        let guid = navigation.getParam('guid') || ''
        let pageType = navigation.getParam('pageType') || ''

        let dateNow = this.dateFormat("YYYY-mm-dd", new Date());

        let isAppPhotoSetting = navigation.getParam('isAppPhotoSetting')
        let isAppDateSetting = navigation.getParam('isAppDateSetting')
        let isAppPhotoAlbumSetting = navigation.getParam('isAppPhotoAlbumSetting')
        this.setState({
            isAppPhotoSetting: isAppPhotoSetting,
            isAppDateSetting: isAppDateSetting,
            isAppPhotoAlbumSetting: isAppPhotoAlbumSetting
        })

        if (pageType == 'CheckPage') {
            this.setState({
                isCheckPage: true,
                ServerTime: dateNow,
            })
            this.checkResData(guid)
        } else {
            this.setState({
                isLoading: true,
                ServerTime: dateNow,
                inspector: Url.PDAEmployeeName,
                issueDate: this.dateFormat("YYYY-mm-dd", new Date),
                rectifyForm: [{ "name": "停工" }, { "name": "限期" }]
            })
            this.resData()
        }

    }

    componentWillUnmount() {
        checkStandardListAll = []
        postNameAndCheckStandardInfoAll = []
        this.setState({
            rectifyNoticeInfo: ''
        })
    }

    resData = () => {
        let formData = new FormData();
        let data = {};
        data = {
            "action": "safemanagenoticeinit",
            "servicetype": "pda",
            "express": "8AC4C0B0",
            "ciphertext": "7df83f712027c63f147f6c2d4a43f08d",
            "data": {
                "factoryId": Url.PDAFid
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: SafeManageNotice.js:164 ~ SafeManageNotice ~ formData:", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log("res ---- " + JSON.stringify(Url));
            return res.json();
        }).then(resData => {
            console.log("🚀 ~ file: SafeManageNotice.js:174 ~ SafeManageNotice ~ resData:", resData)
            this.setState({
                resData: resData,
            })
            let teamName = resData.result.teamInfo
            let postName = resData.result.postNameAndCheckStandardInfo
            let rectifyNoticeInfo = resData.result.rectifyNoticeInfo.rectifyNo

            // 获取新添加整改单号
            //let rectifyNoticeInfoSplit = "";
            //if (rectifyNoticeInfo != null) {
            //    rectifyNoticeInfoSplit = rectifyNoticeInfo.split("-")
            //}
            //let time = this.dateFormat("YYYYmmdd", new Date)
            //if (rectifyNoticeInfoSplit == null || rectifyNoticeInfoSplit == "" || rectifyNoticeInfoSplit[1] != time) {
            //    rectifyNoticeInfo = "ZGD-" + time + "-001"
            //} else {
            //    rectifyNoticeInfo = rectifyNoticeInfoSplit[0] + "-" + rectifyNoticeInfoSplit[1] + "-" + ("00" + (Number(rectifyNoticeInfoSplit[2]) + 1 )).slice(-3)
            //}

            postNameAndCheckStandardInfoAll = postName
            this.setState({
                rowguid: resData.result.rowguid,
                resData: resData,
                teamName: teamName,
                postName: postName,
                rectifyNoticeInfo: rectifyNoticeInfo,
                isLoading: false
            })
        }).catch((error) => {
            console.log("error --- " + error)
        });

    }

    checkResData = (guid) => {
        let formData = new FormData();
        let data = {};
        data = {
            "action": "safemanagenoticequerydetail",
            "servicetype": "pda",
            "express": "FA51025B",
            "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
            "data": {
                "guid": guid,
                "factoryId": Url.PDAFid
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: qualityinspection.js ~ line 226 ~ QualityInspection ~ formData", guid + '----' + Url.PDAFid)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log("res ---- " + JSON.stringify(Url));
            return res.json();
        }).then(resData => {
            console.log("🚀 ~ file: qualityinspection.js ~ line 226 ~ QualityInspection ~ resData", resData)
            this.checkStandardListAllInit(resData.result.rectifyNoticeSub)
            this.setState({
                rectifyNoticeInfo: resData.result.rectifyNoticeInfo,
                teamName: resData.result.team,
                inspector: resData.result.inspector,
                issueDate: resData.result.issueDate,
                rectifyBy: resData.result.signedBy,
                postSelect: resData.result.post,
                remark: resData.result.remark,
            })

        }).catch((error) => {
            console.log("error --- " + error)
        });

    }

    //顶栏
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('title')
        }
    }

    // 时间工具
    _DatePicker = (index, data) => {
        //const { ServerTime } = this.state
        data.rectify_term == "" ? data.rectify_term = this.dateFormat("YYYY-mm-dd", new Date()) : true
        return (
            <DatePicker
                customStyles={{
                    dateInput: styles.dateInput,
                    dateTouchBody: styles.dateTouchBody,
                    dateText: this.state.isAppDateSetting ? styles.dateText : styles.dateDisabled,
                }}
                iconComponent={<Icon name='caretdown' color={this.state.isAppDateSetting ? '#419fff' : "#666"} iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
                }
                showIcon={true}
                date={data.rectify_term}
                onOpenModal={() => {
                    this.setState({ focusIndex: index })
                }}
                format="YYYY-MM-DD"
                mode="date"
                disabled={!this.state.isAppDateSetting}
                onDateChange={(value) => {
                    data.rectify_term = value
                    this.setState({
                        ServerTime: value
                    })
                }}
            />
        )
    }

    PostData = () => {
        const { teamSelect, rectifyBy, inspector, issueDate, postSelect, rectifyNoticeInfo, remark, rowguid, recid } = this.state
        if (teamSelect == '请选择' || teamSelect == '') {
            this.toast.show('请选择班组信息');
            return
        } else if (rectifyBy == '请输入' || rectifyBy == '') {
            this.toast.show('责任签收人不能为空');
            return
        } else if (postSelect == '请选择' || postSelect == '') {
            this.toast.show('请选择岗位信息');
            return
        }
        let opt = false
        let opt2 = false
        let NotTakePhoto = false
        let checkStandardListResult = []
        checkStandardListAll.forEach((r, i) => {
            if (r.is_select) {
                opt = true;
                if (r.image_arr == "") {
                    NotTakePhoto = true
                }
                if (r.rectifyBy == "") {
                    this.toast.show('问题描述信息不能为空');
                    opt2 = true;
                } else if (r.rectify_form == "请选择" || r.rectify_form == "") {
                    this.toast.show('请选择整改形式');
                    opt2 = true;
                }
                checkStandardListResult.push(r)
            }
        })
        console.log("🚀 ~ file: SafeManageNotice.js:321 ~ SafeManageNotice ~ checkStandardListAll.forEach ~ checkStandardListResult:", checkStandardListResult)
        console.log("🚀 ~ file: SafeManageNotice.js:326 ~ SafeManageNotice ~ NotTakePhoto:", NotTakePhoto)

        if (this.state.isAppPhotoSetting) {
            if (NotTakePhoto) {
                this.toast.show('没有拍摄整改照片')
                opt2 = true
            }
        }
        if (!opt) {
            this.toast.show('请选择危险源信息');
            return
        }
        if (opt2) return;
        this.toast.show(saveLoading, 0)

        let formData = new FormData();
        let data = {};

        data = {
            "action": "safemanagenoticesave",
            "servicetype": "pda",
            "express": "06C795EE",
            "ciphertext": "a1e3818d57d9bfc9437c29e7a558e32e",
            "data": {
                "userName": inspector,
                "factoryId": Url.PDAFid,
                "rowguid": rowguid,
                "rectifyNoticeInfo": rectifyNoticeInfo,
                "teamName": teamSelect,
                "inspector": inspector,
                "issueDate": issueDate,
                "rectifyBy": rectifyBy,
                "postName": postSelect,
                "remark": remark,
                "checkStandardList": checkStandardListResult,
                "editEmployeeId": Url.PDAEmployeeId,
                "editEmployeeName": Url.PDAEmployeeName,
            },
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: SafeManageNotice.js:363 ~ SafeManageNotice ~ formData:", formData)
        if (!Url.isAppNewUpload) {
            checkStandardListResult.map((item, index) => {
                formData.append('img_' + index, {
                    uri: item.image_arr,
                    type: 'image/jpeg',
                    name: 'img_' + index
                })
            })
        }
        fetch(Url.PDAurl, {
            method: 'post',
            headers: {
                'content-type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res);
            return res.json();
        }).then(resData => {
            console.log("🚀 ~ file: qualityinspection.js ~ line 947 ~ qualityinspection ~ resdata", resData)
            if (resData.status == '100') {
                this.toast.show('保存成功');
                setTimeout(() => {
                    this.props.navigation.replace(this.props.navigation.getParam("page"), {
                        title: this.props.navigation.getParam("title"),
                        pageType: this.props.navigation.getParam("pageType"),
                        page: this.props.navigation.getParam("page"),
                        isAppPhotoSetting: this.props.navigation.getParam("isAppPhotoSetting"),
                        isAppDateSetting: this.props.navigation.getParam("isAppDateSetting"),
                        isAppPhotoAlbumSetting: this.props.navigation.getParam("isAppPhotoAlbumSetting"),
                    })
                }, 1000)
            } else {
                this.toast.show('保存失败')
                Alert.alert('保存失败', resData.message)
                //console.log('resdata', resData)
            }
        }).catch((error) => {
            console.log("error --- " + error)
            if (error.toString().indexOf('not valid JSON') != -1) {
                Alert.alert('保存失败', "响应内容不是合法JSON格式")
                return
            }
            if (error.toString().indexOf('Network request faile') != -1) {
                Alert.alert('保存失败', "网络请求错误")
                return
            }
            Alert.alert('保存失败', error.toString())
        });
    }

    // 获取登录时间
    getTodayDate() {
        let time = this.state.isCheckPage ? this.state.issueDate : this.dateFormat("YYYY-mm-dd", new Date)
        return (
            <Text>{time}</Text>
        )
    }

    // 日期格式化
    dateFormat(fmt, date) {
        let ret;
        const opt = {
            "Y+": date.getFullYear().toString(),        // 年
            "m+": (date.getMonth() + 1).toString(),     // 月
            "d+": date.getDate().toString(),            // 日
            "H+": date.getHours().toString(),           // 时
            "M+": date.getMinutes().toString(),         // 分
            "S+": date.getSeconds().toString()          // 秒
            // 有其他格式化字符需求可以继续添加，必须转化成字符串
        };
        for (let k in opt) {
            ret = new RegExp("(" + k + ")").exec(fmt);
            if (ret) {
                fmt = fmt.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")))
            };
        };
        return fmt;
    }

    // 下拉菜单
    downItem = (index, i, data) => {
        const { teamName, postListByTeam, teamSelect, postName, postSelect, rectifyForm, rectifyFormSelect, checkStandardSelect } = this.state
        switch (index) {
            case '1': return (
                // 班组
                <View>{(
                    this.state.isCheck ? <Text >{teamSelect}</Text> :
                        <TouchableCustom onPress={() => { this.isItemVisible(teamName, index, i) }} >
                            <IconDown text={teamSelect} />
                        </TouchableCustom>
                )}</View>
            ); break;
            case '5': return (
                // 岗位
                <View>{(
                    this.state.isCheck ? <Text >{postSelect}</Text> :
                        <TouchableCustom onPress={() => {
                            if (teamSelect == '' || teamSelect == '请选择') {
                                this.toast.show('请选择班组信息');
                            } else {
                                this.isItemVisible(postListByTeam, index, i)
                            }
                        }} >
                            <IconDown text={postSelect} />
                        </TouchableCustom>
                )}</View>
            ); break;
            case '6': return (
                // 危险源
                <View>{(
                    this.state.isCheck ? <Text >{checkStandardSelect}</Text> :
                        <TouchableCustom onPress={() => {
                            if (teamSelect == '请选择' || teamSelect == '') {
                                this.toast.show('请选择班组信息');
                            } else if (postSelect == '请选择' || postSelect == '') {
                                this.toast.show('请选择岗位信息');
                            } else {
                                this.isItemVisible(checkStandardListAll, index, i)
                            }
                        }} >
                            {/*<IconDown text={checkStandardSelect} />*/}
                        </TouchableCustom>
                )}</View>
            ); break;
            case '9' + i: return (
                // 整改形式
                <View>{(
                    this.state.isCheck ? <Text >{data.rectify_form}</Text> :
                        <TouchableCustom onPress={() => { this.isItemVisible(data, index, i) }} >
                            <IconDown text={data.rectify_form} />
                        </TouchableCustom>
                )}</View>
            ); break;
            default: return;
        }
    }

    isItemVisible = (data, focusIndex, i) => {
        //console.log("************** " + focusIndex + "-" + i)
        if (typeof (data) != "undefined") {
            if (data != [] && data != "") {
                switch (focusIndex) {
                    case '1': this.setState({ teamVisible: true, bottomData: data, focusIndex: focusIndex }); break;
                    case '5': this.setState({ postVisible: true, bottomData: data, focusIndex: focusIndex }); break;
                    case '6': this.setState({ checkStandardVisible: true, bottomData: data, focusIndex: focusIndex }); break;
                    case '9' + i: this.setState({ rectifyFormVisible: true, bottomData2: data, focusIndex: focusIndex }); break;
                    default: return;
                }
            }
        } else {
            this.toast.show("无数据")
        }
    }

    cameraFunc = (data) => {
        launchCamera(
            {
                mediaType: 'photo',
                includeBase64: false,
                maxHeight: parseInt(Url.isResizePhoto),
                maxWidth: parseInt(Url.isResizePhoto),
                saveToPhotos: true,

            },
            (response) => {
                if (response.uri == undefined) {
                    return
                }
                data.image_arr = response.uri

                data.picture_visible = false
                data.picture_select = true

                this.setState({ check: !this.state.check })
                if (Url.isAppNewUpload) {
                    this.cameraPost(response.uri, data)
                } else {
                    this.setState({
                        pictureSelect: true,
                    })
                }
                console.log(response)
            },
        )
    }

    camLibraryFunc = (data) => {
        launchImageLibrary(
            {
                mediaType: 'photo',
                includeBase64: false,
                maxHeight: parseInt(Url.isResizePhoto),
                maxWidth: parseInt(Url.isResizePhoto),
            },
            (response) => {
                if (response.uri == undefined) {
                    return
                }
                data.image_arr = response.uri
                data.picture_visible = false
                data.picture_select = true
                this.setState({ check: !this.state.check })
                if (Url.isAppNewUpload) {
                    this.cameraPost(response.uri, data)
                } else {
                    this.setState({
                        pictureSelect: true,
                    })
                }
                console.log(response)
            },
        )
    }

    camandlibFunc = (data) => {
        Alert.alert(
            '提示信息',
            '选择拍照方式',
            [{
                text: '取消',
                style: 'cancel'
            }, {
                text: '拍照',
                onPress: () => {
                    this.cameraFunc(data)
                }
            }, {
                text: '相册',
                onPress: () => {
                    this.camLibraryFunc(data)
                }
            }])
    }

    // 展示/隐藏 放大图片
    handleZoomPicture = (flag, index) => {
        this.setState({
            imageOverSize: false,
            currShowImgIndex: index || 0
        })
    }

    // 加载放大图片弹窗
    renderZoomPicture = () => {
        const { imageOverSize, currShowImgIndex, imageFileArr, pictureUri } = this.state
        let imgArr = [{
            url: pictureUri
        }]
        return (
            <OverlayImgZoomPicker
                isShowImage={imageOverSize}
                currShowImgIndex={currShowImgIndex}
                zoomImages={imgArr}
                callBack={(flag) => this.handleZoomPicture(flag)}
            ></OverlayImgZoomPicker>
        )
    }

    cameraPost = (uri, data) => {
        const { recid } = this.state
        this.toast.show('图片上传中', 2000)

        let formData = new FormData();
        let data1 = {};
        data1 = {
            "action": "savephote",
            "servicetype": "photo_file",
            "express": "D2979AB2",
            "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
            "data": {
                "fileflag": fileflag,
                "username": Url.PDAusername,
                "recid": recid
            }
        }
        formData.append('jsonParam', JSON.stringify(data1))
        formData.append('img', {
            uri: uri,
            type: 'image/jpeg',
            name: 'img'
        })
        console.log("🚀 ~ file: concrete_pouring.js ~ line 672 ~ SteelCage ~ formData", formData)

        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            console.log(res.statusText);
            console.log(res);
            return res.json();
        }).then(resData => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 690 ~ SteelCage ~ resData", resData)
            if (resData.status == '100') {
                let fileid = resData.result.fileid;
                let recid = resData.result.recid;

                let tmp = {}
                tmp.fileid = fileid
                tmp.uri = uri
                tmp.url = uri
                imageFileArr.push(tmp)
                data.image_arr = uri
                data.fileid = fileid
                data.recid = recid
                this.setState({
                    fileid: fileid,
                    recid: "",
                    pictureSelect: true,
                    imageFileArr: imageFileArr
                })
                console.log("🚀 ~ file: concrete_pouring.js ~ line 704 ~ SteelCage ~ imageFileArr", imageFileArr)
                this.toast.show('图片上传成功');
            } else {
                Alert.alert('图片上传失败', resData.message)
                this.toast.close(1)
            }
        }).catch((error) => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 692 ~ SteelCage ~ error", error)
        });
    }

    cameraDelete = (data) => {
        if (Url.isAppNewUpload) {
            let formData = new FormData();
            let data1 = {};
            data1 = {
                "action": "deletephote",
                "servicetype": "photo_file",
                "express": "D2979AB2",
                "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
                "data": {
                    "fileid": data.fileid,
                }
            }
            formData.append('jsonParam', JSON.stringify(data1))
            console.log("🚀 ~ file: concrete_pouring.js ~ line 733 ~ SteelCage ~ cameraDelete ~ formData", formData)
            fetch(Url.PDAurl, {
                method: 'POST',
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                body: formData
            }).then(res => {
                console.log(res.statusText);
                console.log(res);
                return res.json();
            }).then(resData => {
                console.log("🚀 ~ file: concrete_pouring.js ~ line 745 ~ SteelCage ~ cameraDelete ~ resData", resData)
                if (resData.status == '100') {
                    data.image_arr = ""
                    data.fileid = ""
                    data.picture_select = false
                    data.picture_visible = true
                    this.setState({ check: !this.state.check }, () => { this.forceUpdate() })
                    this.toast.show('图片删除成功');
                } else {
                    Alert.alert('图片删除失败', resData.message)
                    this.toast.close(1)
                }
            }).catch((error) => {
                console.log("🚀 ~ file: concrete_pouring.js ~ line 761 ~ SteelCage ~ cameraDelete ~ error", error)
            });
        } else {
            data.image_arr = ""
            data.picture_select = false
            data.picture_visible = true
            this.setState({ check: !this.state.check }, () => { this.forceUpdate() })
        }

    }

    // 问题照片
    getPicture = (data) => {
        return (
            <View>
                {
                    data.picture_visible ? <Icon name='camera-alt' color='#4D8EF5' iconProps={{ size: 32 }} onPress={() => {
                        if (this.state.isPhotoUpdate == false) {
                            if (this.state.isAppPhotoAlbumSetting) {
                                this.camandlibFunc(data)
                            } else {
                                this.cameraFunc(data)
                            }
                        }

                        /* this.setState({
                          CameraVisible: true
                        }) */
                    }}
                    ></Icon> : <View />

                }
                {this.imageView(data)}
            </View>
        )
    }

    imageView = (data) => {
        console.log("🚀 ~ file: SafeManageNotice.js:766 ~ SafeManageNotice ~ data:", data)
        data.picture_select = this.state.isCheckPage ? true : data.picture_select
        data.image_arr = this.state.isCheckPage ? data.picture : data.image_arr
        return (
            <View style={{ flexDirection: 'row' }}>
                {
                    data.picture_select ?
                        <AvatarImg item={data.image_arr} isRounded="false" index='0'
                            onPress={() => {
                                this.setState({
                                    imageOverSize: true,
                                    pictureUri: data.image_arr
                                })
                            }}
                            onLongPress={() => {
                                Alert.alert(
                                    '提示信息',
                                    '是否删除照片',
                                    [{
                                        text: '取消',
                                        style: 'cancel'
                                    }, {
                                        text: '删除',
                                        onPress: () => {
                                            //console.log("🚀 ~ file: qualityinspection.js ~ line 649 ~ SteelCage ~ this.state.imageArr.map ~ i", data.image_arr)
                                            this.cameraDelete(data)
                                            //console.log("🚀 ~ file: qualityinspection.js ~ line 656 ~ SteelCage ~ this.state.imageArr.map ~ i", data.image_arr)
                                        }
                                    }])
                            }}
                        /> : <View></View>
                }
            </View>
        )
    }

    // 危险源列表数据初始化
    checkStandardListAllInit(check) {
        checkStandardListAll = []
        check.map((r, i) => {
            checkStandardListAll.push(
                {
                    "is_select": false,
                    "check_standard": r.check_standard,
                    "cycle": r.cycle,
                    "risk_level": r.risk_level,
                    "check_method": r.check_method,
                    "rectifyBy": r.describe || "",
                    "rectify_form": r.rectify_form || "请选择",
                    "rectify_term": r.rectify_term || "",
                    "rectifyForm": [{ "name": "停工" }, { "name": "限期" }],
                    "image_arr": r.picture || "",
                    "picture": r.picture || "",
                    "picture_visible": true,
                    "picture_select": false,
                    "recid": ''
                }
            )
        })
    }

    render() {
        const { navigation } = this.props;
        //从主页面传url, 未来集成到一起
        const QRurl = navigation.getParam('QRurl') || '';
        const QRid = navigation.getParam('QRid') || ''
        const type = navigation.getParam('type') || '';
        let pageType = navigation.getParam('pageType') || ''

        if (type == 'compid') {
            QRcompid = QRid
        }
        const { isCheckPage, focusIndex, teamVisible, postVisible, rectifyFormVisible, pictureVisible, checkStandardVisible, bottomData, bottomData2, rectifyNoticeInfo, inspector, issueDate, postSelect, rectifyBy, remark, teamName, teamSelect, postListByTeam, rectifyForm } = this.state

        if (this.state.isLoading) {
            return (
                <View style={styles.loading}>
                    <ActivityIndicator
                        animating={true}
                        color='#419FFF'
                        size="large" />
                </View>
            )
            //渲染页面
        } else {
            return (
                <View style={{ flex: 1 }}>
                    <Toast ref={(ref) => { this.toast = ref; }} position="center" />
                    <NavigationEvents
                        onWillFocus={this.UpdateControl}
                        onWillBlur={() => {
                            if (QRid != '') {
                                navigation.setParams({ QRid: '', type: '' })
                            }
                        }} />
                    <ScrollView ref={ref => this.scoll = ref} style={styles.mainView} showsVerticalScrollIndicator={false} >

                        <View style={styles.listView}>
                            <ListItemScan focusStyle={focusIndex == '0' ? styles.focusColor : {}} title='整改单号:' rightElement={rectifyNoticeInfo} />

                            {
                                isCheckPage ? <ListItemScan focusStyle={focusIndex == '1' ? styles.focusColor : {}} title='班组:' rightElement={teamName} /> :
                                    <TouchableCustom underlayColor={'lightgray'} onPress={() => { this.isItemVisible(teamName, '1', '') }}>
                                        <ListItemScan focusStyle={focusIndex == '1' ? styles.focusColor : {}} title='班组:' rightElement={this.downItem('1', '')} />
                                    </TouchableCustom>
                            }


                            <ListItemScan focusStyle={focusIndex == '2' ? styles.focusColor : {}} title='检查人:' rightElement={inspector} />

                            <ListItemScan focusStyle={focusIndex == '3' ? styles.focusColor : {}} title='签发时间:' rightElement={this.getTodayDate()} />

                            <ListItemScan focusStyle={focusIndex == '4' ? styles.focusColor : {}} title='责任签收人:' rightElement={() =>
                                isCheckPage ? <Text>{rectifyBy}</Text> :
                                    <View>
                                        <Input
                                            containerStyle={styles.quality_input_container}
                                            inputContainerStyle={styles.inputContainerStyle}
                                            inputStyle={[styles.quality_input_, { top: 7 }]}
                                            placeholder='请输入'
                                            value={rectifyBy}
                                            onChangeText={(value) => {
                                                this.setState({
                                                    rectifyBy: value
                                                })
                                            }} />
                                    </View>} />

                            {
                                isCheckPage ? <ListItemScan focusStyle={focusIndex == '5' ? styles.focusColor : {}} title='岗位:' rightElement={postSelect} /> :
                                    <TouchableCustom underlayColor={'lightgray'} onPress={() => {
                                        if (teamSelect == '' || teamSelect == '请选择') {
                                            this.toast.show('请选择班组信息');
                                        }
                                        this.isItemVisible(postListByTeam, '5', '')
                                    }}>
                                        <ListItemScan focusStyle={focusIndex == '5' ? styles.focusColor : {}} title='岗位:' rightElement={this.downItem('5', '')} />
                                    </TouchableCustom>
                            }
                            {
                                isCheckPage ? <View /> :
                                    <ListItemScan focusStyle={focusIndex == '6' ? styles.focusColor : {}} title='危险源:' rightElement={
                                        <Button titleStyle={{ fontSize: 14 }}
                                            title='选择'
                                            type='solid'
                                            onPress={() => {
                                                if (teamSelect == '请选择' || teamSelect == '') {
                                                    this.toast.show('请选择班组信息');
                                                } else if (postSelect == '请选择' || postSelect == '') {
                                                    this.toast.show('请选择岗位信息');
                                                } else {
                                                    this.isItemVisible(checkStandardListAll, "6", "")
                                                }
                                            }}
                                            buttonStyle={{ paddingVertical: 6, backgroundColor: '#4D8EF5', marginVertical: 0 }} />
                                    }>
                                    </ListItemScan>
                            }
                        </View>


                        {/*危险源列表*/}
                        {
                            checkStandardListAll.map((l, i) => {
                                if (isCheckPage || l.is_select) {
                                    return (
                                        <TouchableCustom onLongPress={() => {
                                            !isCheckPage ?
                                                Alert.alert(
                                                    '提示信息',
                                                    '是否删除危险源信息',
                                                    [{
                                                        text: '取消',
                                                        style: 'cancel'
                                                    }, {
                                                        text: '删除',
                                                        onPress: () => {
                                                            l.is_select = false
                                                            this.setState({
                                                                checked: !this.state.checked
                                                            })
                                                        }
                                                    }]) : ""
                                        }} >
                                            <View style={styles.listView} key={i}>
                                                <Card containerStyle={styles.card_containerStyle} >
                                                    <ListItemScan_child><Text>{l.check_standard}</Text></ListItemScan_child>
                                                    <ListItemScan_child><Text>检查周期：{l.cycle}</Text></ListItemScan_child>
                                                    <ListItemScan_child><Text>排查方法：{l.check_method}</Text></ListItemScan_child>
                                                    <ListItemScan_child><Text>风险等级：{l.risk_level}</Text></ListItemScan_child>
                                                </Card>
                                                <ListItemScan focusStyle={focusIndex == '7' + i ? styles.focusColor : {}} title='问题照片:' rightElement={() =>
                                                    isCheckPage ? this.imageView(l) :
                                                        pictureVisible ? this.getPicture(l) : this.imageView()} />
                                                <ListItemScan focusStyle={focusIndex == '8' + i ? styles.focusColor : {}} title='问题描述:' rightElement={() =>
                                                    isCheckPage ? <Text>{rectifyBy}</Text> :
                                                        <View>
                                                            <Input
                                                                containerStyle={styles.quality_input_container}
                                                                inputContainerStyle={styles.inputContainerStyle}
                                                                inputStyle={[styles.quality_input_, { top: 7 }]}
                                                                placeholder='请输入'
                                                                value={l.rectifyBy}
                                                                onChangeText={(value) => {
                                                                    l.rectifyBy = value
                                                                    this.setState({
                                                                        checked: !this.state.checked
                                                                    })
                                                                }} />
                                                        </View>} />
                                                {
                                                    isCheckPage ? <ListItemScan focusStyle={focusIndex == '9' ? styles.focusColor : {}} title='整改形式:' rightElement={l.rectify_form} /> :
                                                        <TouchableCustom underlayColor={'lightgray'} onPress={() => { this.isItemVisible(l, '9' + i, i) }}>
                                                            <ListItemScan focusStyle={focusIndex == '9' + i ? styles.focusColor : {}} title='整改形式:' rightElement={this.downItem('9' + i, i, l)} />
                                                        </TouchableCustom>
                                                }

                                                <ListItemScan focusStyle={focusIndex == '10' + i ? styles.focusColor : {}} title='整改期限:' rightElement={() =>
                                                    isCheckPage ? <Text>{l.rectify_term}</Text> : this._DatePicker('10' + i, l)} />
                                            </View>
                                        </TouchableCustom>
                                    )
                                }

                            })

                        }
                        <View style={styles.listView}>
                            <ListItemScan focusStyle={focusIndex == '11' ? styles.focusColor : {}} title='备注:' rightElement={() =>
                                isCheckPage ? < Text>{remark}</Text> :
                                    <View>
                                        <Input
                                            containerStyle={styles.quality_input_container}
                                            inputContainerStyle={styles.inputContainerStyle}
                                            inputStyle={[styles.quality_input_, { top: 7 }]}
                                            placeholder='请输入'
                                            value={remark}
                                            onChangeText={(value) => {
                                                this.setState({
                                                    remark: value
                                                })
                                            }} />
                                    </View>} />
                        </View>

                        {!isCheckPage ?
                            <FormButton
                                backgroundColor='#17BC29'
                                onPress={() => {
                                    this.PostData()
                                    this.setState({
                                        checkStandardSaveVisible: true
                                    })

                                }}
                                disabled={this.state.isPhotoUpdate}
                                title='保存'
                            /> : <Text />
                        }

                        {/*班组底栏*/}
                        <BottomSheet
                            onBackdropPress={() => {
                                this.setState({
                                    teamVisible: false
                                })
                            }}
                            isVisible={teamVisible && !this.state.isCheckPage} onRequestClose={() => {
                                this.setState({
                                    teamVisible: false
                                })
                            }}
                        >
                            <ScrollView style={styles.bottomsheetScroll}>
                                {bottomData.map((item, index) => {
                                    let title = item.teamName
                                    return (
                                        <TouchableCustom
                                            onPress={() => {
                                                checkStandardListAll = []
                                                let lstPost = item.postName.filter(i => i.name != "")
                                                this.setState({
                                                    teamSelect: item.teamName,
                                                    teamVisible: false,
                                                    postListByTeam: lstPost,
                                                    postSelect: '请选择'
                                                })
                                                if (lstPost == "" || lstPost == {}) this.setState({ postSelect: '暂无对应岗位' })
                                            }}
                                        >
                                            <BottomItem backgroundColor='white' color="#333" title={title} />
                                        </TouchableCustom>
                                    )
                                })}
                            </ScrollView>
                            <TouchableHighlight
                                onPress={() => {
                                    this.setState({ teamVisible: false })
                                }}>
                                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
                            </TouchableHighlight>
                        </BottomSheet>

                        {/*岗位底栏*/}
                        <BottomSheet
                            onBackdropPress={() => {
                                this.setState({
                                    postVisible: false
                                })
                            }}
                            isVisible={postVisible && !this.state.isCheckPage} onRequestClose={() => {
                                this.setState({
                                    postVisible: false
                                })
                            }}
                        >
                            <ScrollView style={styles.bottomsheetScroll}>
                                {bottomData.map((item, index) => {
                                    let title = item.name
                                    let check = false
                                    return (
                                        <TouchableCustom
                                            onPress={() => {
                                                postNameAndCheckStandardInfoAll.map((r, i) => {
                                                    if (r.postName == item.name) {
                                                        this.checkStandardListAllInit(r.checkStandard)
                                                        check = true
                                                    }
                                                })
                                                if (!check) this.checkStandardListAllInit([])
                                                this.setState({
                                                    postSelect: item.name,
                                                    postVisible: false
                                                })
                                            }}
                                        >
                                            <BottomItem backgroundColor='white' color="#333" title={title} />
                                        </TouchableCustom>
                                    )
                                })}
                            </ScrollView>
                            <TouchableHighlight
                                onPress={() => {
                                    this.setState({ postVisible: false })
                                }}>
                                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
                            </TouchableHighlight>
                        </BottomSheet>

                        {/*危险源底栏*/}
                        <BottomSheet
                            onBackdropPress={() => {
                                this.setState({
                                    checkStandardVisible: false
                                })
                            }}
                            isVisible={checkStandardVisible && !this.state.isCheckPage} onRequestClose={() => {
                                this.setState({
                                    checkStandardVisible: false
                                })
                            }}
                        >
                            <ScrollView style={styles.bottomsheetScroll}>
                                {bottomData.map((item, index) => {
                                    this.setState({
                                        checked: item.is_select
                                    })
                                    let title = item.check_standard
                                    return (
                                        <TouchableCustom
                                            onPress={() => {
                                                item.is_select = !item.is_select
                                                this.setState({
                                                    checked: !this.state.checked
                                                })
                                            }}
                                        >
                                            <View style={{
                                                paddingVertical: 18,
                                                backgroundColor: 'white',
                                                alignItems: 'center',
                                                flexDirection: 'row',
                                                borderBottomColor: '#DDDDDD',
                                                borderBottomWidth: 0.6
                                            }}>
                                                <CheckBoxScan
                                                    checked={item.is_select}
                                                    onPress={() => {
                                                        item.is_select = !item.is_select
                                                        this.setState({
                                                            checked: !this.state.checked
                                                        })
                                                    }}
                                                />
                                                <Text style={{ color: '#333', textAlign: 'center', fontSize: 14, textAlignVertical: 'center' }}>{title}</Text>
                                            </View>
                                        </TouchableCustom>
                                    )
                                })}
                            </ScrollView>
                            <TouchableHighlight
                                onPress={() => {
                                    this.setState({ checkStandardVisible: false, checkStandardSaveVisible: true })
                                }}>
                                <BottomItem backgroundColor='#e74c3c' color="white" title={'确定'} />
                            </TouchableHighlight>
                        </BottomSheet>

                        {/*整改形式底栏*/}
                        <BottomSheet
                            onBackdropPress={() => {
                                this.setState({
                                    rectifyFormVisible: false
                                })
                            }}
                            isVisible={rectifyFormVisible && !this.state.isCheckPage} onRequestClose={() => {
                                this.setState({
                                    rectifyFormVisible: false
                                })
                            }}
                        >
                            <ScrollView style={styles.bottomsheetScroll}>
                                {
                                    rectifyForm.map((item, index) => {
                                        let title = item.name
                                        return (
                                            <TouchableCustom
                                                onPress={() => {
                                                    bottomData2.rectify_form = title
                                                    this.setState({
                                                        rectifyFormSelect: item.name,
                                                        rectifyFormVisible: false
                                                    })
                                                }}
                                            >
                                                <BottomItem backgroundColor='white' color="#333" title={title} />
                                            </TouchableCustom>
                                        )
                                    })
                                }
                            </ScrollView>
                            <TouchableHighlight
                                onPress={() => {
                                    this.setState({ rectifyFormVisible: false })
                                }}>
                                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
                            </TouchableHighlight>
                        </BottomSheet>

                        {/*照片放大*/}
                        {this.renderZoomPicture()}
                        {/* {overlayImg(obj = {
                            onRequestClose: () => {
                                this.setState({ imageOverSize: !this.state.imageOverSize }, () => {
                                    console.log("🚀 ~ file: equipment_maintenance.js:1696 ~ render ~ imageOverSize:", this.state.imageOverSize)
                                })
                            },
                            onPress: () => {
                                this.setState({
                                    imageOverSize: !this.state.imageOverSize
                                })
                            },
                            uri: this.state.pictureUri,
                            isVisible: this.state.imageOverSize
                        })
                        } */}
                    </ScrollView>
                </View>
            )
        }
    }

}


