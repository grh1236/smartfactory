import React from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, ListItem, BottomSheet } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { ToDay, YesterDay, BeforeYesterDay } from '../../CommonComponents/Data';
import { RFT } from '../../Url/Pixal';
import { ScrollView } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker'
import { NavigationEvents } from 'react-navigation';
import ScanButton from '../Componment/ScanButton';

const { height, width } = Dimensions.get('window') //获取宽高

let compDataArr = [], compIdArr = [], QRid2 = '', tmpstr = ''

export default class ChangeLocation extends React.Component {
  constructor() {
    super();
    this.state = {
      monitorSetup: '',
      isroom: false,
      isteam: false,
      iskeeper: false,
      EditName: Url.PDAEmployeeName,
      bottomData: [],
      bottomVisible: false,
      resData: [],
      formCode: '',
      rowguid: '',
      steelCageIDs: '',
      ServerTime: '',
      QRid: '',
      news: '',
      room: [],
      roomId: '',
      roomselect: '请选择',
      roomName: '',
      library: [],
      libraryId: '',
      libraryselect: '请选择',
      libraryName: '',
      compDataArr: [],
      compData: '',
      compId: '',
      compIdArr: [],
      compCode: '',
      isGetStoreComponentInfo: false,
      isGetStoreroom: false,
      //CardList
      hidden: -1,
      isVisible: false,
      isUpdate: false,
      isCheckPage: false
    }
    //this.requestCameraPermission = this.requestCameraPermission.bind(this)
  }

  componentDidMount() {
    const { navigation } = this.props;
    let guid = navigation.getParam('guid') || ''
    let pageType = navigation.getParam('pageType') || ''
    if (pageType == 'CheckPage') {
      this.checkResData(guid)
      this.setState({ isCheckPage: true })
    } else {
      this.resData()
    }
  }

  UpdateControl = () => {
    if (typeof this.props.navigation.getParam('QRid') != "undefined") {
      if (this.props.navigation.getParam('QRid') != "") {
        this.toast.show(Url.isLoadingView, 0)
      }
    }
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid')
    const type = navigation.getParam('type') || '';

    if (type == 'libraryid' ) {
      this.GetStoreroom(QRid)
    } else if (type == 'Stockid') {
      console.log("🚀 ~ file: change_location.js ~ line 253 ~ ChangeLocation ~ componentDidUpdate ~ this.state.isGetStoreComponentInfo", this.state.isGetStoreComponentInfo)
      this.GetStoreComponentInfo(QRid)
    }

  }

  componentWillUnmount() {
    compDataArr = [], QRid2 = '', tmpstr = '', compIdArr = []
  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  checkResData = (guid) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getstoragelocationchangedetail",
      "servicetype": "pdaservice",
      "express": "8AC4C0B0",
      "ciphertext": "7df83f712027c63f147f6c2d4a43f08d",
      "data": {
        "_Rowguid": guid,
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: change_location.js ~ line 335 ~ ChangeLocation ~ resData", resData)
      let FormCode = resData.result.FormCode
      let roomName = resData.result.StorageRoom
      let libraryName = resData.result.StorageLocation
      let ChangeTime = resData.result.ChangeTime
      let EditName = resData.result.EditName
      let ComponentInfo = resData.result.ComponentInfo
      compDataArr = ComponentInfo

      this.setState({
        resData: resData,
        formCode: FormCode,
        roomName: roomName,
        roomselect: roomName,
        libraryName: libraryName,
        libraryselect: libraryName,
        compDataArr: compDataArr,
        ServerTime: ChangeTime,
        EditName: EditName,
        isGetStoreComponentInfo: true,
        isGetStoreroom: true,

      })
      this.setState({
        isLoading: false,
      })
    }).catch((error) => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 215 ~ QualityInspection ~ error", error)
    });
  }

  resData = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "storagelocationinit",
      "servicetype": "pdaservice",
      "express": "B34B2B21",
      "ciphertext": "219527459fa29fa23c7d7ef001165d2b",
      "data": { "_bizid": Url.PDAFid }
    }

    formData.append('jsonParam', JSON.stringify(data))

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == 100) {
        let formCode = resData.result.formCode
        let ServerTime = resData.result.ChangeTime
        let rowguid = resData.result.rowguid
        this.setState({
          resData: resData,
          formCode: formCode,
          ServerTime: ServerTime,
          rowguid: rowguid,
        })
      }
      else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }
    }).catch((error) => {
    });
  }

  GetStoreroom = (id) => {
    let compCode = '111'
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getStoreroom",
      "servicetype": "pda",
      "express": "162C33CC",
      "ciphertext": "3b81e5182d128db9333f0fec4d3703a5",
      "data": {
        "LibraryId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
       this.toast.close()
      if (resData.status == '100') {
        let libraryName = resData.result.libraryName
        let libraryId = resData.result.libraryId
        let roomName = resData.result.roomName
        this.setState({
          libraryName: libraryName,
          libraryId: libraryId,
          roomName: roomName,
          libraryselect: libraryName,
          roomselect: roomName,
          isGetStoreroom: true
        })
      } else {
        Alert.alert('ERROR', resData.message)
      }

    }).catch(err => {
    })
  }

  GetStoreComponentInfo = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getcompinfoforstolocationchange",
      "servicetype": "pdaservice",
      "express": "227EB695",
      "ciphertext": "de110251c4c1faf0038a3416d728a830",
      "data": {
        "compId": id,
        "_bizid": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
       this.toast.close()
      if (resData.status == '100') {
        let compData = resData.result
        let compId = compData.compId
        let compCode = compData.compCode
        if (compDataArr.length == 0) {
          compDataArr.push(compData)
          compIdArr.push(compId)
          this.setState({
            compDataArr: compDataArr,
            compIdArr: compIdArr,
            compData: compData,
            compId: compId,
            compCode: compCode,
            isGetStoreComponentInfo: true
          })
          tmpstr = tmpstr + JSON.stringify(compData) + ' '
        } else {
          if (tmpstr.indexOf(compData.compCode) == -1) {
            compIdArr.push(compId)
            compId = this.state.compId + ',' + compId
            compDataArr.push(compData)
            this.setState({
              compDataArr: compDataArr,
              compIdArr: compIdArr,
              compData: compData,
              compId: compId,
              compCode: compCode,
              isGetStoreComponentInfo: true
            })
            tmpstr = tmpstr + JSON.stringify(compData) + ' '
          }

        }

      } else {
        Alert.alert('错误', resData.message)
       this.setState({
          compId: ""
        })
        //this.input1.focus()
      }

    }).catch(err => {
    })
  }

  comIDItem = () => {
    const { isGetcomID, buttondisable, compCode } = this.state
    return (
      <View>
        {
          !isGetcomID ?
            <View>
              <ScanButton
                 
                onPress={() => {
                  this.setState({
                    iscomIDdelete: false,
                    buttondisable: true,
                    isUpdate: true
                  })
                  setTimeout(() => {
                    this.setState({ GetError: false, buttondisable: false })
                    varGetError = false
                  }, 1500)
                  this.props.navigation.navigate('QRCode', {
                    type: 'compid',
                    page: 'ChangeLocation'
                  })
                }}
              />
            </View>
            :
            <TouchableOpacity
              onPress={() => {
                console.log('onPress')
              }}
              onLongPress={() => {
                console.log('onLongPress')
                Alert.alert(
                  '提示信息',
                  '是否删除构件信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      this.setState({
                        compData: [],
                        compCode: '',
                        compId: '',
                        iscomIDdelete: true,
                        isGetcomID: false,
                      })
                    }
                  }])
              }} >
              <Text>{compCode}</Text>
            </TouchableOpacity>
        }
      </View>

    )
  }

  PostData = () => {
    const { formCode, ServerTime, rowguid, compId, compIdArr, libraryId, libraryName, roomName, } = this.state
    const { navigation } = this.props;
    const QRid = navigation.getParam('QRid') || '';

    console.log("🚀 ~ file: change_location.js ~ line 491 ~ ChangeLocation ~ compIdArr", compIdArr)
    let compIdArray = compIdArr

    if (libraryId.length == 0) {
      this.toast.show('库位不能为空')
      return
    } else if (compIdArray.length == 0) {
      this.toast.show('入库集合不能为空')
      return
    } else if (roomName.length == 0) {
      this.toast.show('库房不能为空')
      return
    }

    this.toast.show(saveLoading, 0)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "savestoragelocationchange",
      "servicetype": "pdaservice",
      "express": "54FA70C0",
      "ciphertext": "042b311dd63cfff370a4230b6412a775",
      "data": {
        "userName": Url.PDAusername,
        "compIdArray": compIdArray,
        "StorageLactionId": libraryId,
        "ChangeTime": ServerTime,
        "_bizid": Url.PDAFid,
        "rowguid": rowguid,
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: change_location.js ~ line 516 ~ ChangeLocation ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('保存成功');
        setTimeout(() => {
          this.props.navigation.replace(this.props.navigation.getParam("page"), {
            title: this.props.navigation.getParam("title"),
            pageType: this.props.navigation.getParam("pageType"),
            page: this.props.navigation.getParam("page"),
            isAppPhotoSetting: this.props.navigation.getParam("isAppPhotoSetting"),
            isAppDateSetting: this.props.navigation.getParam("isAppDateSetting"),
            isAppPhotoAlbumSetting: this.props.navigation.getParam("isAppPhotoAlbumSetting"),
          })
          //this.props.navigation.navigate('PDAMainStack')
        }, 400)
        setTimeout(() => {
          this.props.navigation.navigate('PDAMainStack')
        }, 1000)
      } else {
        this.toast.show('保存失败')
        Alert.alert('保存失败', resData.message)
      }
    }).catch((error) => {
      this.toast.show('保存失败')
      if (error.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('保存失败', "响应内容不是合法JSON格式")
        return
      }
      if (error.toString().indexOf('Network request faile') != -1) {
        Alert.alert('保存失败', "网络请求错误")
        return
      }
      Alert.alert('保存失败', error.toString())
      console.log("🚀 ~ file: change_location.js ~ line 409 ~ ChangeLocation ~ error", error)
    });
  }

  CardList = (compData) => {
    return (
      compData.map((item, index) => {
        let comp = item
        return (
          <View style={{ width: width * 0.9, marginLeft: width * 0.05, }}>
            <ListItem
              key={index}
              onPress={() => {
                this.setState({
                  hidden: index,
                  isVisible: !this.state.isVisible
                }, () => {
                  console.log("🚀 ~ file: storage.js ~ line 126 ~ CardList2 ~ compData.map ~ this.state.isVisible", this.state.isVisible)
                })
              }}
              onLongPress={() => {
                Alert.alert(
                  '提示信息',
                  '是否删除构件信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      compDataArr.splice(index, 1)
                      compIdArr.splice(index, 1)
                      tmpstr = compIdArr.toString()


                      this.setState({
                        compDataArr: compDataArr,
                        compIdArr: compIdArr
                      })
                    }
                  }])

              }}
              containerStyle={[{ backgroundColor: '#36A7FF' }]}
              title={(index + 1) + '    ' + item.compCode}
              titleStyle={{ color: 'white' }}
              rightElement={
                <View style={{ flexDirection: 'row' }}>
                  <Text style={{ color: 'white', fontSize: 16, marginTop: 3 }}>1个</Text>
                  <Icon name={this.state.hidden == index ? 'down' : 'up'} type='antdesign' color='white' />
                </View>
              }
              bottomDivider >
            </ListItem>
            {
              this.state.hidden == index ?
                <View key={index} style={{ backgroundColor: '#F9F9F9' }} >
                  <ListItem
                    containerStyle={styles.list_container_style}
                    title='产品编号'
                    rightTitle={comp.compCode}
                    titleStyle={styles.title}
                    rightTitleProps={{ numberOfLines: 1 }}
                    rightTitleStyle={styles.rightTitle}
                  />
                  <ListItem
                    containerStyle={styles.list_container_style}
                    title='项目名称'
                    rightTitle={comp.projectName}
                    rightTitleProps={{ numberOfLines: 1 }}
                    titleStyle={styles.title}
                    rightTitleStyle={styles.rightTitle}
                  />
                  <ListItem
                    rightTitleProps={{ numberOfLines: 1 }}
                    containerStyle={styles.list_container_style}
                    title='构件类型'
                    rightTitle={comp.compTypeName}
                    titleStyle={styles.title}
                    rightTitleStyle={styles.rightTitle}
                  />
                  <ListItem
                    containerStyle={styles.list_container_style}
                    title='设计型号'
                    rightTitle={comp.designType}
                    titleStyle={styles.title}
                    rightTitleProps={{ numberOfLines: 1 }}
                    rightTitleStyle={styles.rightTitle}
                  />
                  <ListItem
                    containerStyle={styles.list_container_style}
                    title='楼号'
                    rightTitle={comp.floorNoName}
                    titleStyle={styles.title}
                    rightTitleProps={{ numberOfLines: 1 }}
                    rightTitleStyle={styles.rightTitle}
                  />
                  <ListItem
                    containerStyle={styles.list_container_style}
                    title='层号'
                    rightTitle={comp.floorName}
                    titleStyle={styles.title}
                    rightTitleProps={{ numberOfLines: 1 }}
                    rightTitleStyle={styles.rightTitle}
                  />
                  <ListItem
                    containerStyle={styles.list_container_style}
                    title='砼方量'
                    rightTitle={comp.volume}
                    titleStyle={styles.title}
                    rightTitleProps={{ numberOfLines: 1 }}
                    rightTitleStyle={styles.rightTitle}
                  />
                  <ListItem
                    containerStyle={styles.list_container_style}
                    title='重量'
                    rightTitle={comp.weight}
                    titleStyle={styles.title}
                    rightTitleProps={{ numberOfLines: 1 }}
                    rightTitleStyle={styles.rightTitle}
                  />
                  <ListItem
                    containerStyle={styles.list_container_style}
                    title='生产单位'
                    rightTitle={Url.PDAFname}
                    titleStyle={styles.title}
                    rightTitleProps={{ numberOfLines: 1 }}
                    rightTitleStyle={styles.rightTitle}
                  />
                </View> : <View></View>
            }

          </View>
        )
      })
    )
  }

  _DatePicker = () => {
    const { ServerTime } = this.state
    return (
      <DatePicker
        customStyles={{
          dateInput: styles.dateInput,
          dateTouchBody: styles.dateTouchBody,
          dateText: styles.dateText,
        }}
        iconComponent={<Icon name='caretdown' color='#419fff' iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
        }
        showIcon={true}
        mode='datetime'
        date={ServerTime}
        format="YYYY-MM-DD HH:mm"
        onDateChange={(value) => {
          this.setState({
            ServerTime: value
          })
        }}
      />
    )
  }

  render() {
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || '';
    const type = navigation.getParam('type') || '';

    const { resData, formCode, ServerTime, teamselect, roomselect, library, libraryId, libraryselect, libraryName, bottomVisible, bottomData, compData, compId, compCode } = this.state

    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (
        <ScrollView>
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          <NavigationEvents
            onWillFocus={this.UpdateControl} />
          <ListItem
            title='表单编号'
            rightTitleStyle={{ width: width / 1.5, textAlign: 'right', fontSize: 15 }}
            rightTitle={formCode}
            bottomDivider
          />
          <ListItem
            title='库房'
            rightElement={
              <TouchableOpacity onPress={() => { this.setState({ bottomVisible: true, bottomData: Url.PDAnews }) }} >
                <View
                  style={{ flexDirection: "row" }}
                >
                  <Text>{roomselect}</Text>
                  <Icon name='downsquare' color='#419fff' iconStyle={{ fontSize: 17, marginLeft: 2 }} type='antdesign' ></Icon>
                </View>
              </TouchableOpacity>

            }
            bottomDivider
          />
          <ListItem
            title='库位'
            rightElement={
              <View>

                <ScanButton
                  onPress={() => {
                    this.setState({ isUpdate: true })
                    this.props.navigation.navigate('QRCode', {
                      type: 'libraryid',
                      page: 'ChangeLocation'
                    })
                  }}
                />
                <TouchableOpacity onPress={() => {
                  if (library.length != 0) {
                    this.setState({ bottomVisible: true, bottomData: library })
                  } else {
                    this.toast.show('请先选库房')
                  }
                }} >
                  <View style={{ flexDirection: "row" }}>
                    <Text >{libraryselect}</Text>
                    <Icon name='downsquare' color='#419fff' iconStyle={{ fontSize: 17, marginLeft: 2 }} type='antdesign' ></Icon>
                  </View>
                </TouchableOpacity>

              </View>
            }
            bottomDivider
          />
          <ListItem
            title='变更日期'
            rightTitle={this._DatePicker()}
            bottomDivider
          />
          <ListItem
            title='登陆人'
            rightTitle={this.state.EditName}
            rightTitleStyle={{ width: width / 1.5, textAlign: 'right', fontSize: 15 }}
            bottomDivider
          />
          <ListItem
            title='变更明细'
            rightElement={
              <View style={{ flexDirection: 'row' }}>
                <View>
                  <Input
                    ref={ref => { this.input1 = ref }}
                    containerStyle={styles.scan_input_container}
                    inputContainerStyle={styles.scan_inputContainerStyle}
                    inputStyle={[styles.scan_input]}
                    placeholder='请输入'
                    keyboardType='numeric'
                    value={compId}
                    onChangeText={(value) => {
                      value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
                      value = value.replace(/^\./g, ""); //验证第一个字符是数字
                      value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
                      //value = value.replace(/\b(0+)/gi, ""); //清楚开头的0
                      this.setState({
                        compId: value
                      })
                    }}
                    //onSubmitEditing={() => { this.input1.focus() }}
                    //onFocus={() => { this.setState({ focusIndex: '5' }) }}
                    /*  */
                    //returnKeyType = 'previous'
                    onSubmitEditing={() => {
                      let inputComId = this.state.compId
                      console.log("🚀 ~ file: qualityinspection.js ~ line 468 ~ QualityInspection ~ inputComId", inputComId)
                      inputComId = inputComId.replace(/\b(0+)/gi, "")
                      this.GetStoreComponentInfo(inputComId)
                    }}
                  />
                </View>
                <ScanButton
                  onPress={() => {
                    this.setState({
                      isGetStoreComponentInfo: false,
                      isUpdate: true
                    }, () => {
                      this.props.navigation.navigate('QRCode', {
                        type: 'Stockid',
                        page: 'ChangeLocation',
                        isUpdateFun: (isUpdate, type, page, QRid) => {
                          this.setState({
                            isUpdate: isUpdate,
                            type: type,
                            page: page,
                            QRid: QRid,
                          })
                        }
                      })
                    })


                  }}
                />
              </View>
            }

          />

          {
            compDataArr.length > 0 ? this.CardList(compDataArr) : <View></View>
          }

          {this.state.isCheckPage ? <View></View> :
            <Button
              //disabled={!username || !password}

              //onPress={this.login}
              title='保存'
              type='outline'
              onPress={() => {
                this.PostData()
              }}
              style={{ marginVertical: 10 }}
              containerStyle={{ marginVertical: 10, marginHorizontal: 20 }}
              buttonStyle={{ backgroundColor: '#FFFFFF', borderRadius: 60 }}
            ></Button>

          }


          <BottomSheet
            isVisible={bottomVisible && !this.state.isCheckPage}
            onRequestClose
          >
            {
              bottomData.map((item, index) => {
                return (
                  <TouchableOpacity
                    onPress={() => {
                      if (item.roomId) {
                        this.setState({
                          roomId: item.roomId,
                          roomselect: item.roomName,
                          roomName: item.roomName,
                          library: item.library
                        })
                      } else if (item.libraryId) {
                        this.setState({
                          libraryId: item.libraryId,
                          libraryName: item.libraryName,
                          libraryselect: item.libraryName
                        })
                      }
                      this.setState({
                        bottomVisible: false
                      })
                    }}
                  >
                    <ListItem
                      title={item.roomName ? item.roomName : (item.libraryName)}
                      //titleStyle={styles.text}
                      //contentContainerStyle={styles.content}
                      // containerStyle={styles.container}
                      bottomDivider
                    />
                  </TouchableOpacity>
                )

              })
            }
          </BottomSheet>
        </ScrollView>
      )
    }
  }

}

const styles = StyleSheet.create({
  title: {
    fontSize: 13,
  },
  rightTitle: {
    fontSize: 13,
    textAlign: 'left',
    lineHeight: 14,
    flexWrap: 'wrap',
    width: width / 2
  },
  list_container_style: { height: 14 }
})