import React from 'react';
import { View, TouchableHighlight, Dimensions, ActivityIndicator, Alert, Image, FlatList } from 'react-native';
import { Button, Text, Input, Icon, Card, ListItem, BottomSheet, Overlay, Header } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { ScrollView } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker'
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { NavigationEvents } from 'react-navigation';
import { styles } from '../Componment/PDAStyles';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import CheckBoxScan from '../Componment/CheckBoxScan';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import AvatarImg from '../Componment/AvatarImg';
import ListItemScan_child from '../Componment/ListItemScan_child';
import ScanButton from '../Componment/ScanButton';
import CardListSampleManage from "../Componment/CardListSampleManage";
import AvatarAdd from '../Componment/AvatarAdd';
import Pdf from 'react-native-pdf';

import { DeviceEventEmitter, NativeModules } from 'react-native';
import overlayImg from '../Componment/OverlayImg';
import OverlayImgZoomPicker from '../Componment/OverlayImgZoomPicker';
let letGetError = false

let DeviceStorageData = {} //缓存数据

const { height, width } = Dimensions.get('window') //获取宽高

let imageArr = [], imageFileArr = [], imageArr2 = [], imageFileArr2 = [];//照片数组
let rectifyimageArr = [], //整改照片数组
    questionChickIdArr = [], questionChickArr = [], //子表问题数组
    isCheckSecondList = [], //是否展开二级
    isCheckedAllList = [] //二级菜单是否被全选
let rectifyLiables = [] //整改责任人
let rectifyLiablesCheck = [] //整改责任人是否选中

class BackIcon extends React.PureComponent {
    render() {
        return (
            <TouchableCustom
                onPress={this.props.onPress} >
                <Icon
                    name='arrow-back'
                    color='#419FFF' />
            </TouchableCustom>
        )
    }
}

export default class SampleManage extends React.PureComponent {
    constructor() {
        super();
        this.state = {
            type: 'compid',

            title: '', //区别页面
            titleList: ['质量抽检管理', '抽检整改单', '整改复检'],
            isTitleOne: false,
            isTitleTwo: false,
            isTitleThree: false,

            rowguid: '',
            sampleNo: '', // 检查单号
            sampleDate: '', // 抽检日期
            currentState: '', // 当前状态
            originalOperator: '', // 原操作人
            sampleResult: '合格', // 抽检结果
            sampleResultCheck: true,
            room: '', // 库房信息
            library: '', // 库位信息
            samplePerson: '', // 抽检员
            remarkSample: '', // 抽检备注
            rectifyLiables: '', // 整改责任人
            rectifyLiablesSelect: '请选择', // 选中整改责任人
            rectifyLiablesIdSelect: '', // 选中整改责任人id
            projectName: '', // 项目名称
            designType: '', // 设计型号
            version: '', // 建立版本
            floorNoName: '', // 分部
            floorName: '', // 分项
            volume: '', // 体积
            weight: '', // 重量
            labour: '', //劳务队
            productLine: '', // 生产线
            productionMode: '', // 生产方式
            compTypeName: '', // 构件类型
            diffSourceType: '', // 子型号
            samplePicture: '', // 照片附件
            compDrawing: '', // 构件图纸
            sampleRecord: '', // 抽检记录
            sampleRecordList: [],
            rectifyDate: '', // 整改日期
            rectifyPerson: '', // 整改人
            remarkRectify: '', // 整改备注
            rectifyPicture: '', //整改照片

            pictureVisible: true, // 问题照片
            imageOverSize: false, // 照片放大
            pictureUri: '',
            pictureUri2: '',
            uriShow: '',
            pictureSelect: false,
            pictureSelect2: false,
            imageArr: [],
            imageFileArr: [],
            imageArr2: [],
            imageFileArr2: [],
            fileid: "",
            recid: "",
            isPhotoUpdate: false,

            buttondisable: false,
            compId: '',
            compCode: '',
            compData: {},

            focusIndex: 0,
            isLoading: false,

            //问题缺陷
            dictionary: [],
            problemDefect: '',
            measure: '',
            problemId: '',
            questionChick: -1,
            questionChickIdArr: [],
            questionChickArr: [],
            problemDefects: [],
            isCheckSecondList: [],
            isCheckedAllList: [],
            isQuestionOverlay: false,

            //底栏控制
            bottomData: [],
            checked: false,
            rectifyLiablesVisible: false,

            //是否扫码成功
            isGetcomID: false,
            isGetCompCode: false,
            isGettaiwanId: false,

            //图纸
            isPaperTypeVisible: false,
            paperList: ["模版图", "配筋图", "预埋件装配图", "连接件排版图"],
            paperUrl: "",
            FileName: "",
            isPDFVisible: false,

            //长按删除功能控制
            iscomIDdelete: false,

            //查询界面取消选择按钮
            isCheckPage: false,

            // 验证构件id
            isScanCheck: false,
            isCheckComID: false,

            mainCheckPageScanMessage: '',
            isMainCheckPageScan: true,
            mainCheckPageScanGoto: false,

            //控制app拍照，日期，相册选择功能
            isAppPhotoSetting: false,
            currShowImgIndex: 0,
            isAppDateSetting: false,
            isAppPhotoAlbumSetting: false
        }
        //this.requestCameraPermission = this.requestCameraPermission.bind(this)
    }

    componentDidMount() {
        const { navigation } = this.props;
        let guid = navigation.getParam('guid') || ''
        let pageType = navigation.getParam('pageType') || ''
        let title = navigation.getParam('title')
        let isCheckComID = navigation.getParam('isCheckComID') || false
        let titleList = this.state.titleList
        this.setState({
            title: title,
            isTitleOne: title == titleList[0] ? true : false,
            isTitleTwo: title == titleList[1] ? true : false,
            isTitleThree: title == titleList[2] ? true : false,
        })
        this.setState({
            isCheckPage: pageType == 'CheckPage' ? true : false,
        })

        let isAppPhotoSetting = navigation.getParam('isAppPhotoSetting')
        let isAppDateSetting = navigation.getParam('isAppDateSetting')
        let isAppPhotoAlbumSetting = navigation.getParam('isAppPhotoAlbumSetting')
        this.setState({
            isAppPhotoSetting: isAppPhotoSetting,
            isAppDateSetting: isAppDateSetting,
            isAppPhotoAlbumSetting: isAppPhotoAlbumSetting
        })

        if (pageType == 'CheckPage' || title == titleList[2]) {
            this.checkResData(guid, pageType, title, titleList)
        } else {
            rectifyimageArr = []
            if (title == titleList[0]) {
                this.resData()
                this.setState({
                    title: title,
                    samplePerson: Url.PDAEmployeeName,
                    isMainCheckPageScan: false
                })
            } else if (title == titleList[1]) {
                // 是否操作界面扫码跳转过来
                this.setState({
                    isCheckComID: isCheckComID
                })
                this.checkResData(guid, pageType, title, titleList)
            }
        }

        //通过使用DeviceEventEmitter模块来监听事件
        this.iDataScan = DeviceEventEmitter.addListener('iDataScan', (Event) => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 115 ~ SteelCage ~ Event.ScanResult", Event.ScanResult)
            if (typeof Event.ScanResult != undefined) {
                let data = Event.ScanResult
                let arr = data.split("=");
                let id = ''
                id = arr[arr.length - 1]
                console.log("🚀 ~ file: concrete_pouring.js ~ line 118 ~ SteelCage ~ id", id)
                if (this.state.type == 'compid') {
                    if (this.state.isTitleTwo || this.state.isTitleThree) {
                        this.setState({
                            isScanCheck: true
                        })
                    }
                    this.GetcomID(id)
                }

            }
        });
    }

    componentWillUnmount() {
        titleList = []
        imageArr = [], imageFileArr = [], imageArr2 = [], imageFileArr2 = [] //照片数组
        rectifyImageArr = [], //整改照片数组
            questionChickIdArr = [], questionChickArr = [], //子表问题数组
            isCheckSecondList = [], //是否展开二级
            isCheckedAllList = [] //二级菜单是否被全选
        rectifyLiables = [] //整改责任人
        rectifyLiablesCheck = [] //整改责任人是否选中
        this.iDataScan.remove()
    }

    resData = () => {
        let formData = new FormData();
        let data = {};
        data = {
            "action": "samplinginitial",
            "servicetype": "sampling",
            "express": "6DFD1C9B",
            "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
            "data": { "factoryId": Url.PDAFid }
        }
        formData.append('jsonParam', JSON.stringify(data))
        //console.log("🚀 ~ file: SampleManage.js ~ line 138 ~ SampleManage ~ formData:", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res.statusText);
            //console.log(res);
            return res.json();
        }).then(resData => {
            if (resData.status == '100') {
                //let rowguid = resData.result.rowguid
                //let ServerTime = resData.result.time
                //let monitorSetup = resData.result.monitorSetup
                //if (monitorSetup != '扫码') {
                //    this.setState({
                //        isGetInspector: true,
                //    })
                //}
                let dictionary = resData.result.defectTypes

                // 整改责任人
                rectifyLiables = resData.result.inspectors

                let sampleDate = this.dateFormat("YYYY-mm-dd", new Date());
                if (typeof resData.result.sampleDate != 'undefined') {
                    sampleDate = resData.result.sampleDate
                }

                //let inspectors = resData.result.inspectors
                console.log("🚀 ~ file: SampleManage.js ~ line 161 ~ SampleManage ~ dictionary:", dictionary)
                this.setState({
                    //resData: resData,
                    //rowguid: rowguid,
                    //ServerTime: ServerTime,
                    //monitorSetup: monitorSetup,
                    dictionary: dictionary,
                    sampleDate: sampleDate,
                    //inspectors: inspectors,
                    isLoading: false
                })
            } else {
                Alert.alert("错误提示", resData.message, [{
                    text: '取消',
                    style: 'cancel'
                }, {
                    text: '返回上一级',
                    onPress: () => {
                        this.props.navigation.goBack()
                    }
                }])
            }

        }).catch((error) => {
            console.log("🚀 ~ file: SampleManage.js ~ line 184 ~ SampleManage ~ error:", error)
        });

    }

    checkResData = (guid, pageType, title, titleList) => {
        let formData = new FormData();
        let data = {};
        data = {
            "action": "samplemanagequerydetail",
            "servicetype": "pda",
            "express": "FA51025B",
            "ciphertext": "0d8c5b09401d9d1059417086718ed55c",
            "data": {
                "guid": guid,
                "factoryId": Url.PDAFid
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: qualityinspection.js ~ line 226 ~ QualityInspection ~ formData", guid + '----' + Url.PDAFid)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log("res ---- " + JSON.stringify(Url));
            return res.json();
        }).then(resData => {
            console.log("🚀 ~ file: qualityinspection.js ~ line 226 ~ QualityInspection ~ resData", resData)
            this.compDataInit(resData.result.sampleManage)
            if (pageType == 'CheckPage' || title == titleList[2]) {
                if (resData.result.sampleManage.sample_result == '不合格') {
                    // 展示问题缺陷二级
                    var items = []
                    items = resData.result.dictionary
                    items.map((item, index) => {
                        isCheckSecondList.push(item.defectTypeId);
                        this.setState({ isCheckSecondList: isCheckSecondList });
                    })
                    this.setState({
                        // 质量问题
                        sampleResultCheck: false,
                        dictionary: resData.result.dictionary
                    })
                }
                let pictureList = resData.result.pictureList
                if (pictureList.length > 0) {
                    pictureList.map((item, index) => {
                        let tmp = {}
                        tmp.fileid = ""
                        tmp.uri = item
                        tmp.url = item
                        imageFileArr.push(tmp)
                    })
                }
                this.setState({
                    imageFileArr: imageFileArr
                })
                this.setState({
                    samplePicture: resData.result.sampleManage.sample_picture,
                    imageArr: resData.result.pictureList || [],
                    pictureSelect: true,
                    compCode: resData.result.sampleManage.comp_code,
                    projectName: resData.result.sampleManage.project_name,
                    compTypeName: resData.result.sampleManage.comp_type,
                    designType: resData.result.sampleManage.design_type,
                    floorNoName: resData.result.sampleManage.floor_no,
                    floorName: resData.result.sampleManage.floor,
                    //compDrawing: resData.result.sampleManage.comp_drawing,
                    compId: resData.result.sampleManage.comp_drawing,
                    sampleDate: resData.result.sampleManage.sample_date.split('T')[0],
                    currentState: resData.result.sampleManage.current_state,
                    originalOperator: resData.result.sampleManage.original_operator,
                    sampleResult: resData.result.sampleManage.sample_result,
                    rectifyLiablesSelect: resData.result.sampleManage.rectify_liables,
                    room: resData.result.sampleManage.room,
                    library: resData.result.sampleManage.library,
                    samplePerson: resData.result.sampleManage.sample_person,
                    remarkSample: resData.result.sampleManage.remark_sample,
                    isMainCheckPageScan: false
                })

                let pictureRectifyList = resData.result.pictureRectifyList
                if (pictureRectifyList.length > 0) {
                    pictureRectifyList.map((item, index) => {
                        let tmp = {}
                        tmp.fileid = ""
                        tmp.url = item
                        imageFileArr2.push(tmp)
                    })
                }
                this.setState({
                    imageFileArr2: imageFileArr2
                })
                // 抽检整改相关
                if (title == titleList[1] || title == titleList[2]) {
                    this.setState({
                        sampleNo: resData.result.sampleManage.sample_no,
                        imageArr2: resData.result.pictureRectifyList || [],
                        pictureSelect2: true,
                        rectifyPicture: resData.result.sampleManage.rectify_picture,
                        sampleRecord: resData.result.sampleManage.sample_record,
                        sampleRecordList: resData.result.sampleManage.sample_record.split(';'),
                        rectifyDate: resData.result.sampleManage.rectify_date.split('T')[0],
                        rectifyPerson: resData.result.sampleManage.rectify_person,
                        remarkRectify: resData.result.sampleManage.remark_rectify,
                        isGetcomID: true,
                    })
                }
            } else {
                // 抽检整改相关
                if (title == titleList[1]) {
                    //this.compDataInit(resData.result.sampleManage)
                    // 展示问题缺陷二级
                    var items = []
                    items = resData.result.dictionary
                    items.map((item, index) => {
                        isCheckSecondList.push(item.defectTypeId);
                        this.setState({ isCheckSecondList: isCheckSecondList });
                    })
                    this.setState({
                        rectifyDate: this.dateFormat("YYYY-mm-dd HH:MM:SS", new Date),
                        sampleRecord: resData.result.sampleManage.sample_record,
                        sampleRecordList: resData.result.sampleManage.sample_record.split(';'),
                        rectifyPerson: Url.PDAEmployeeName,

                        sampleNo: resData.result.sampleManage.sample_no,
                        samplePicture: resData.result.sampleManage.sample_picture,
                        imageArr: resData.result.pictureList || [],
                        pictureSelect: true,
                        compCode: resData.result.sampleManage.comp_code,
                        projectName: resData.result.sampleManage.project_name,
                        compTypeName: resData.result.sampleManage.comp_type,
                        designType: resData.result.sampleManage.design_type,
                        floorNoName: resData.result.sampleManage.floor_no,
                        floorName: resData.result.sampleManage.floor,
                        //compDrawing: resData.result.sampleManage.comp_drawing,
                        compId: resData.result.sampleManage.comp_drawing,
                        sampleDate: resData.result.sampleManage.sample_date.split('T')[0],
                        currentState: resData.result.sampleManage.current_state,
                        originalOperator: resData.result.sampleManage.original_operator,
                        sampleResult: resData.result.sampleManage.sample_result,
                        rectifyLiablesSelect: resData.result.sampleManage.rectify_liables,
                        room: resData.result.sampleManage.room,
                        library: resData.result.sampleManage.library,
                        samplePerson: resData.result.sampleManage.sample_person,
                        remarkSample: resData.result.sampleManage.remark_sample,

                        isGetcomID: true,
                        sampleResultCheck: false,
                        dictionary: resData.result.dictionary,
                        isMainCheckPageScan: false,
                        isLoading: false
                    })

                }
            }
        }).catch((error) => {
            console.log("error --- " + error)
        });

    }

    //顶栏
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('type') == 'compid2' ? '抽检整改单' : navigation.getParam('title')
        }
    }

    // 时间工具
    _DatePicker1 = (index, data) => {
        //const { ServerTime } = this.state
        data.rectify_term == "" ? data.rectify_term = this.dateFormat("YYYY-mm-dd", new Date()) : true
        return (
            <DatePicker
                customStyles={{
                    dateInput: styles.dateInput,
                    dateTouchBody: styles.dateTouchBody,
                    dateText: this.state.isAppDateSetting ? styles.dateText : styles.dateDisabled,
                }}
                iconComponent={<Icon name='caretdown' color={this.state.isAppDateSetting ? '#419fff' : "#666"} iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
                }
                showIcon={true}
                date={data.rectify_term}
                onOpenModal={() => {
                    this.setState({ focusIndex: index })
                }}
                format="YYYY-MM-DD"
                mode="date"
                disabled={!this.state.isAppDateSetting}
                onDateChange={(value) => {
                    data.rectify_term = value
                    this.setState({
                        ServerTime: value
                    })
                }}
            />
        )
    }

    // 时间工具
    _DatePicker = (index) => {
        const { sampleDate } = this.state
        return (
            <DatePicker
                customStyles={{
                    dateInput: styles.dateInput,
                    dateTouchBody: styles.dateTouchBody,
                    dateText: this.state.isAppDateSetting ? styles.dateText : styles.dateDisabled,
                }}
                iconComponent={<Icon name='caretdown' color={this.state.isAppDateSetting ? '#419fff' : "#666"} iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
                }
                showIcon={true}
                date={sampleDate}
                onOpenModal={() => {
                    this.setState({ focusIndex: index })
                }}
                format="YYYY-MM-DD HH:mm"
                mode="datetime"
                disabled={!this.state.isAppDateSetting}
                onDateChange={(value) => {
                    this.setState({
                        sampleDate: value
                    })
                }}
            />
        )
    }

    _DatePickerRectifyDate = (index) => {
        const { rectifyDate } = this.state
        return (
            <DatePicker
                customStyles={{
                    dateInput: styles.dateInput,
                    dateTouchBody: styles.dateTouchBody,
                    dateText: this.state.isAppDateSetting ? styles.dateText : styles.dateDisabled,
                }}
                iconComponent={<Icon name='caretdown' color={this.state.isAppDateSetting ? '#419fff' : "#666"} iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
                }
                showIcon={true}
                date={rectifyDate}
                onOpenModal={() => {
                    this.setState({ focusIndex: index })
                }}
                format="YYYY-MM-DD HH:mm"
                mode="datetime"
                disabled={!this.state.isAppDateSetting}
                onDateChange={(value) => {
                    this.setState({
                        rectifyDate: value
                    })
                }}
            />
        )
    }

    // 保存提交数据
    PostData = () => {
        const { isTitleOne, isTitleTwo, isTitleThree, isGetcomID, rectifyLiablesSelect, sampleNo, sampleDate, compCode, currentState, projectName, productionMode, designType, compTypeName, diffSourceType, version, floorNoName, floorName,
            volume, weight, labour, productLine, samplePerson, originalOperator, room, library, sampleResult, samplePicture, remarkSample, compDrawing, compId, questionChickArr, isCheckComID, sampleRecord, rectifyPicture, rectifyDate, rectifyPerson, remarkRectify, mainCheckPageScanGoto, rowguid, rectifyLiablesIdSelect, recid } = this.state
        let formData = new FormData();
        let data = {};
        let type = isTitleOne ? '0' : isTitleTwo ? '1' : isTitleThree ? '2' : '0';
        console.log("🚀 ~ file: SampleManage.js:617 ~ this.state.isAppPhotoSetting:", this.state.isAppPhotoSetting)

        if (isTitleOne) {
            if (!isGetcomID) {
                this.toast.show('请扫码');
                return
            }
            if (this.state.isAppPhotoSetting) {
                if (imageArr.length == 0) {
                    this.toast.show('请选择检查照片');
                    return
                }
            }
            if (sampleResult == '不合格') {
                if (questionChickArr.length == 0) {
                    this.toast.show('请选择问题缺陷');
                    return
                }
                if (rectifyLiablesSelect == '请选择' || rectifyLiablesSelect == '') {
                    this.toast.show('请选择整改责任人');
                    return
                }
                if (imageArr.length == 0) {
                    this.toast.show('请选择检查照片');
                    return
                }
            }

            this.toast.show(saveLoading, 0)

            data = {
                "action": "samplemanagesave",
                "servicetype": "pda",
                "express": "06C795EE",
                "ciphertext": "a1e3818d57d9bfc9437c29e7a558e32e",
                "data": {
                    "pageType": type,
                    "userName": samplePerson,
                    "factoryId": Url.PDAFid,
                    "editEmployeeId": Url.PDAEmployeeId,
                    "editEmployeeName": Url.PDAEmployeeName,
                    "rowguid": rowguid,
                    "sampleNo": sampleNo,
                    "sampleDate": sampleDate,
                    "compCode": compCode,
                    "currentState": currentState,
                    "projectName": projectName,
                    "productionMode": productionMode,
                    "designType": designType,
                    "compTypeName": compTypeName,
                    "diffSourceType": diffSourceType,
                    "version": version,
                    "floorNoName": floorNoName,
                    "floorName": floorName,
                    "volume": volume,
                    "weight": weight,
                    "labour": labour,
                    "productLine": productLine,
                    "samplePerson": samplePerson,
                    "originalOperator": originalOperator,
                    "room": room,
                    "library": library,
                    "sampleResult": sampleResult,
                    "samplePicture": samplePicture,
                    "remarkSample": remarkSample,
                    "rectifyLiables": rectifyLiablesSelect,
                    "rectifyLiablesIds": rectifyLiablesIdSelect,
                    "compDrawing": compId,
                    "defectList": questionChickArr,
                    "sampleRecord": sampleRecord,
                    "recid": recid
                },
            }
            formData.append('jsonParam', JSON.stringify(data))
            if (!Url.isAppNewUpload) {
                if (imageArr.length > 0) {
                    imageArr.map((item, index) => {
                        formData.append('img_' + index, {
                            uri: item,
                            type: 'image/jpeg',
                            name: 'img_' + index
                        })
                    })
                }
            }

        } else if (isTitleTwo) {
            if (!isCheckComID) {
                this.toast.show('请扫码验证');
                return
            }
            if (this.state.isAppPhotoSetting) {
                if (rectifyimageArr.length == 0) {
                    this.toast.show('请选择整改照片');
                    return
                }
            }

            this.toast.show(saveLoading, 0)

            data = {
                "action": "samplemanagerectifysave",
                "servicetype": "pda",
                "express": "06C795EE",
                "ciphertext": "a1e3818d57d9bfc9437c29e7a558e32e",
                "data": {
                    "pageType": type,
                    "userName": samplePerson,
                    "factoryId": Url.PDAFid,
                    "editEmployeeId": Url.PDAEmployeeId,
                    "editEmployeeName": Url.PDAEmployeeName,
                    "sampleNo": sampleNo,
                    "rectifyPicture": rectifyPicture,
                    "rectifyDate": rectifyDate,
                    "rectifyPerson": rectifyPerson,
                    "remarkRectify": remarkRectify == '请输入' ? '' : remarkRectify,
                    "recid": recid
                },
            }
            formData.append('jsonParam', JSON.stringify(data))
            if (!Url.isAppNewUpload) {
                if (rectifyimageArr.length > 0) {
                    rectifyimageArr.map((item, index) => {
                        formData.append('img_' + index, {
                            uri: item,
                            type: 'image/jpeg',
                            name: 'img_' + index
                        })
                    })
                }
            }
        } else if (isTitleThree) {
            this.toast.show(saveLoading, 0)

            data = {
                "action": "samplemanagerechecksave",
                "servicetype": "pda",
                "express": "06C795EE",
                "ciphertext": "a1e3818d57d9bfc9437c29e7a558e32e",
                "data": {
                    "pageType": type,
                    "userName": samplePerson,
                    "factoryId": Url.PDAFid,
                    "editEmployeeId": Url.PDAEmployeeId,
                    "editEmployeeName": Url.PDAEmployeeName,
                    "sampleNo": sampleNo
                },
            }
            formData.append('jsonParam', JSON.stringify(data))
        }
        console.log("🚀 ~ file: SampleManage.js ~ line 460 ~ SampleManage ~ formData", formData)

        console.log("🚀 ~ file: samplemanage.js ~ line 302 ~ samplemanage ~ formData", JSON.stringify(formData))
        fetch(Url.PDAurl, {
            method: 'post',
            headers: {
                'content-type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res);
            return res.json();
        }).then(resData => {
            //console.log("🚀 ~ file: qualityinspection.js ~ line 947 ~ qualityinspection ~ resdata", resData)
            if (resData.status == '100') {
                this.toast.show('保存成功');
                if (mainCheckPageScanGoto) {
                    setTimeout(() => {
                        this.props.navigation.navigate('MainCheckPage', {
                            title: '抽检整改单',
                            pageType: 'Scan',
                            sonTitle: '抽检整改单',
                            page: 'MainCheckPage',
                            url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject',
                            sonUrl: '',
                            main: 'SampleManage'
                        })
                    }, 1000)
                } else {
                    setTimeout(() => {
                        this.props.navigation.goBack()
                    }, 1000)
                }
            } else {
                this.toast.close
                Alert.alert('保存失败', resData.message)
                //console.log('resdata', resData)
            }
        }).catch((error) => {
            this.toast.show('保存失败')
            if (error.toString().indexOf('not valid JSON') != -1) {
                Alert.alert('保存失败', "响应内容不是合法JSON格式")
                return
            }
            if (error.toString().indexOf('Network request faile') != -1) {
                Alert.alert('保存失败', "网络请求错误")
                return
            }
            Alert.alert('保存失败', error.toString())
        });
    }

    //扫码获取构件信息
    GetcomID = (id) => {
        let formData = new FormData();
        let data = {};
        let type = this.state.isTitleOne ? '0' : this.state.isTitleTwo ? '1' : '0'
        let action = this.state.isScanCheck
        data = {
            "action": action ? "getcompinfoforsamplemanagecheck" : "getcompinfoforsamplemanage",
            "servicetype": "pda",
            "express": "4287BAEA",
            "ciphertext": "bbbaad34794eef7a726f6b3832a24924",
            "data": {
                "compId": id,
                "factoryId": Url.PDAFid,
                "pageType": type
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: SampleManage.js ~ line 552 ~ SampleManage ~ data:", data)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log("🚀 ~ file: SampleManage.js ~ line 179 ~ SampleManage ~ res:", res.json())
            return res.json();
        }).then(resData => {
            this.toast.close()
            if (resData.status == '100') {

                //let compCode = resData.result.compCode
                //let compId = resData.result.compId
                //let room = resData.result.room
                //let library = resData.result.library
                if (this.state.isTitleOne) {
                    // 获取新添加检查单号
                    let sampleManageInfo = resData.result.sampleNo
                    //let sampleManageInfoSplit = "";
                    //if (sampleManageInfo != null && resData.result.sampleNo != '') {
                    //    sampleManageInfoSplit = sampleManageInfo.split("-")
                    //}
                    //let time = this.dateFormat("YYYYmmdd", new Date)
                    //if (sampleManageInfoSplit == null || sampleManageInfoSplit == "" || sampleManageInfoSplit[1] != time) {
                    //    sampleManageInfo = "CJD-" + time + "-001"
                    //} else {
                    //    sampleManageInfo = sampleManageInfoSplit[0] + "-" + sampleManageInfoSplit[1] + "-" + ("00" + (Number(sampleManageInfoSplit[2]) + 1)).slice(-3)
                    //}

                    // 整改责任人
                    //rectifyLiables = resData.result.rectifyLiables
                    rectifyLiables.map((item, index) => {
                        if (item.name == resData.result.originalOperator) {
                            rectifyLiablesCheck.push(true)
                            this.setState({
                                rectifyLiablesSelect: resData.result.originalOperator,
                            })
                        } else {
                            rectifyLiablesCheck.push(false)
                        }
                    })

                    this.setState({
                        rowguid: resData.result.rowguid,
                        compData: resData,
                        compCode: resData.result.compCode,
                        compId: resData.result.compId,
                        room: resData.result.room,
                        library: resData.result.library,
                        currentState: resData.result.currentState,
                        originalOperator: resData.result.originalOperator,
                        version: resData.result.version,
                        floorNoName: resData.result.floorNoName,
                        floorName: resData.result.floorName,
                        volume: resData.result.volume,
                        weight: resData.result.weight,
                        labour: resData.result.labour,
                        productLine: resData.result.productLine,
                        rectifyLiables: resData.result.originalOperator,
                        //rectifyLiablesSelect: resData.result.originalOperator,
                        productionMode: resData.result.productionMode,
                        sampleNo: sampleManageInfo,
                        compTypeName: resData.result.compTypeName,
                        diffSourceType: resData.result.diffSourceType,
                        designType: resData.result.designType,
                        projectName: resData.result.projectName,
                        sampleRecord: resData.result.sampleRecord,
                        sampleRecordList: resData.result.sampleRecord.split(';'),

                        isGetcomID: true,
                        iscomIDdelete: false
                    })
                } else if (this.state.isTitleTwo) {
                    if (resData.result.compCode == this.state.compCode) {
                        this.toast.show('验证通过');
                        this.setState({
                            isCheckComID: true
                        })
                    } else {
                        this.toast.show('扫码构件与整改构件不符');
                    }
                }

            } else {
                console.log('resData', resData)
                Alert.alert('错误', resData.message)
                this.setState({
                    compId: ""
                })
                //this.input1.focus()
                varGetError = true
                this.setState({
                    GetError: true
                })
            }

        }).catch(err => {
            this.toast.show("扫码错误")
            if (err.toString().indexOf('not valid JSON') != -1) {
                Alert.alert('扫码失败', "响应内容不是合法JSON格式")
                return
            }
            if (err.toString().indexOf('Network request faile') != -1) {
                Alert.alert('扫码失败', "网络请求错误")
                return
            }
            Alert.alert('扫码失败', err.toString())
            console.log("🚀 ~ file: SampleManage.js ~ line 210 ~ SampleManage ~ err", err)
        })
    }

    //MainCheckPage扫码获取构件信息
    /*GetcomID2 = (id) => {
        let formData = new FormData();
        let data = {};
        //let type = this.state.isTitleOne ? '0' : this.state.isTitleTwo ? '1' : '0'
        data = {
            "action": "samplerectifyscan",
            "servicetype": "pda",
            "express": "4287BAEA",
            "ciphertext": "bbbaad34794eef7a726f6b3832a24924",
            "data": {
                "compId": id,
                "factoryId": Url.PDAFid,
                //"pageType": type
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: MainCheckPage.js ~ line 123 ~ MainCheckPage ~ data:", data)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log("🚀 ~ file: MainCheckPage.js ~ line 131 ~ MainCheckPage ~ res:", res.json())
            return res.json();
        }).then(resData => {
            if (resData.status == '113') {
                this.setState({
                    isMainCheckPageScan: true,
                })
                Alert.alert('错误', resData.message);
                setTimeout(() => {
                    this.props.navigation.navigate('MainCheckPage', {
                        title: '抽检整改单',
                        pageType: 'Scan',
                        sonTitle: '抽检整改单',
                        page: 'MainCheckPage',
                        url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject',
                        sonUrl: '',
                        main: 'SampleManage'
                    })
                }, 4000)
            } else if (resData.status == '100') {
                this.compDataInit(resData.result.sampleManage)
                this.setState({
                    //title: title,
                    isTitleTwo: true,
                    isCheckPage: false,
                    rectifyDate: this.dateFormat("YYYY-mm-dd", new Date),
                    sampleRecord: resData.result.sampleManage.sample_record,
                    sampleRecordList: resData.result.sampleManage.sample_record.split(';'),
                    rectifyPerson: Url.PDAEmployeeName,

                    sampleNo: resData.result.sampleManage.sample_no,
                    samplePicture: resData.result.sampleManage.sample_picture,
                    imageArr: resData.result.pictureList || [],
                    pictureSelect: true,
                    compCode: resData.result.sampleManage.comp_code,
                    projectName: resData.result.sampleManage.project_name,
                    compTypeName: resData.result.sampleManage.comp_type,
                    designType: resData.result.sampleManage.design_type,
                    floorNoName: resData.result.sampleManage.floor_no,
                    floorName: resData.result.sampleManage.floor,
                    //compDrawing: navigation.getParam('data').sampleManage.comp_drawing,
                    compId: resData.result.sampleManage.comp_drawing,
                    sampleDate: resData.result.sampleManage.sample_date.split('T')[0],
                    currentState: resData.result.sampleManage.current_state,
                    originalOperator: resData.result.sampleManage.original_operator,
                    sampleResult: resData.result.sampleManage.sample_result,
                    rectifyLiablesSelect: resData.result.sampleManage.rectify_liables,
                    room: resData.result.sampleManage.room,
                    library: resData.result.sampleManage.library,
                    samplePerson: resData.result.sampleManage.sample_person,
                    remarkSample: resData.result.sampleManage.remark_sample,

                    isGetcomID: true,
                    sampleResultCheck: false,
                    dictionary: resData.result.dictionary,
                    isCheckComID: true,

                    isMainCheckPageScan: false,
                    mainCheckPageScanGoto: true,
                    isLoading: false
                })
            } else {
                console.log('resData', resData)
                Alert.alert('错误', resData.message)
                this.setState({
                    compId: ""
                })
                this.input1.focus()
                var GetError = true
                this.setState({
                    GetError: true
                })
            }

        }).catch(err => {
            console.log("🚀 ~ file: MainCheckPage.js ~ line 156 ~ MainCheckPage ~ err", err)
        })
    }
    */

    // 接口获取图纸信息
    paperResData = (type) => {
        let formData = new FormData();
        let data = {}
        data = {
            "action": "getDrawings",
            "servicetype": "pda",
            "express": "FCEF95AF",
            "ciphertext": "985e3620ae26a2f380c723b60ce9b525",
            "data": {
                "type": type,
                "compId": this.state.compId,
                "factoryId": Url.PDAFid
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            console.log(res.statusText);
            console.log(res);
            return res.json();
        }).then(resData => {
            console.log("🚀 ~ file: SampleManage.js ~ line 314 ~ SampleManage ~ resData:", resData)
            if (resData.status == "100") {

                let paperUrl = resData.result.Url
                let FileName = resData.result.FileName

                this.setState({
                    compDrawing: paperUrl,
                    FileName: FileName,
                    isPDFVisible: true,
                    isPaperTypeVisible: false
                })
            } else {
                Alert.alert('错误', resData.message)
                this.setState({ isPaperTypeVisible: false })
            }
        }).catch((error) => {
        });
    }

    UpdateControl = () => {
        if (typeof this.props.navigation.getParam('QRid') != "undefined") {
            if (this.props.navigation.getParam('QRid') != "") {
                this.toast.show(Url.isLoadingView, 0)
            }
        }
        const { navigation } = this.props;
        const { isGetcomID, isGetCommonMould, isGetInspector, isGetTeamPeopleInfo, isGettaiwanId, isGetsteelCageInfoforPutMold } = this.state
        //从主页面传url, 未来集成到一起
        const QRurl = navigation.getParam('QRurl') || '';
        const QRid = navigation.getParam('QRid') || ''
        console.log("🚀 ~ file: SampleManage.js ~ line 211 ~ SteelCage ~ SampleManage ~ QRid", QRid)
        let type = navigation.getParam('type') || '';
        console.log("🚀 ~ file: SampleManage.js ~ line 213 ~ SteelCage ~ SampleManage ~ type", type)

        if (type == 'compid') {
            if (QRid.indexOf("000") == 0) {
                let QRid1 = QRid.replace(/\b(0+)/gi, "")
                this.GetcomID(QRid1)
            } else {
                this.GetcomID(QRid)
            }
        }
        /*else if (type == 'compid2') {
            let qid = QRid.indexOf("000") == 0 ? QRid.replace(/\b(0+)/gi, "") : QRid
            this.GetcomID2(qid)
        }*/

        if (QRid != '') {
            navigation.setParams({ QRid: '', type: '' })
        }

    }

    // 日期格式化
    dateFormat(fmt, date) {
        let ret;
        const opt = {
            "Y+": date.getFullYear().toString(),        // 年
            "m+": (date.getMonth() + 1).toString(),     // 月
            "d+": date.getDate().toString(),            // 日
            "H+": date.getHours().toString(),           // 时
            "M+": date.getMinutes().toString(),         // 分
            "S+": date.getSeconds().toString()          // 秒
            // 有其他格式化字符需求可以继续添加，必须转化成字符串
        };
        for (let k in opt) {
            ret = new RegExp("(" + k + ")").exec(fmt);
            if (ret) {
                fmt = fmt.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")))
            };
        };
        return fmt;
    }

    // 下拉菜单
    downItem = (index, i, data) => {
        const { rectifyLiablesSelect } = this.state
        switch (index) {
            case '9': return (
                // 整改责任人
                <View>{(
                    this.state.isCheck ? <Text>{rectifyLiablesSelect.length > 12 ? rectifyLiablesSelect.substring(0, 11) + '...' : rectifyLiablesSelect}</Text> :
                        <TouchableCustom onPress={() => { this.isItemVisible(rectifyLiables, index, i) }} >
                            <IconDown text={rectifyLiablesSelect.length > 12 ? rectifyLiablesSelect.substring(0, 11) + '...' : rectifyLiablesSelect} />
                        </TouchableCustom>
                )}</View>
            ); break;

            default: return;
        }
    }

    isItemVisible = (data, focusIndex, i) => {
        //console.log("************** " + focusIndex + "-" + i)
        if (typeof (data) != "undefined") {
            if (data != [] && data != "") {
                switch (focusIndex) {
                    case '9': this.setState({ rectifyLiablesVisible: true, bottomData: data, focusIndex: focusIndex }); break;

                    default: return;
                }
            }
        } else {
            this.toast.show("无数据")
        }
    }

    camandlibFunc = () => {
        Alert.alert(
            '提示信息',
            '选择拍照方式',
            [{
                text: '取消',
                style: 'cancel'
            }, {
                text: '拍照',
                onPress: () => {
                    this.cameraFunc()
                }
            }, {
                text: '相册',
                onPress: () => {
                    this.camLibraryFunc()
                }
            }])
    }

    cameraFunc = () => {
        launchCamera(
            {
                mediaType: 'photo',
                includeBase64: false,
                maxHeight: parseInt(Url.isResizePhoto),
                maxWidth: parseInt(Url.isResizePhoto),
                saveToPhotos: true,

            },
            (response) => {
                if (response.uri == undefined) {
                    return
                }
                if (this.state.isTitleOne) {
                    imageArr.push(response.uri)
                    this.setState({
                        //pictureSelect: true,
                        pictureUri: response.uri,
                        imageArr: imageArr
                    })
                    if (Url.isAppNewUpload) {
                        this.cameraPost(response.uri)
                    } else {
                        this.setState({
                            pictureSelect: true,
                        })
                    }
                } else if (this.state.isTitleTwo) {
                    rectifyimageArr.push(response.uri)
                    this.setState({
                        //pictureSelect2: true,
                        pictureUri2: response.uri,
                        imageArr2: rectifyimageArr
                    })
                    if (Url.isAppNewUpload) {
                        this.cameraPost2(response.uri)
                    } else {
                        this.setState({
                            pictureSelect2: true,
                        })
                    }
                }

                console.log(response)
            },
        )
    }

    camLibraryFunc = () => {
        launchImageLibrary(
            {
                mediaType: 'photo',
                includeBase64: false,
                maxHeight: parseInt(Url.isResizePhoto),
                maxWidth: parseInt(Url.isResizePhoto),
            },
            (response) => {
                if (response.uri == undefined) {
                    return
                }
                if (this.state.isTitleOne) {
                    imageArr.push(response.uri)
                    this.setState({
                        //pictureSelect: true,
                        pictureUri: response.uri,
                        imageArr: imageArr
                    })
                    if (Url.isAppNewUpload) {
                        this.cameraPost(response.uri)
                    } else {
                        this.setState({
                            pictureSelect: true,
                        })
                    }
                }
                else if (this.state.isTitleTwo) {
                    rectifyimageArr.push(response.uri)
                    this.setState({
                        //pictureSelect2: true,
                        pictureUri2: response.uri,
                        imageArr2: rectifyimageArr
                    })
                    if (Url.isAppNewUpload) {
                        this.cameraPost2(response.uri)
                    } else {
                        this.setState({
                            pictureSelect2: true,
                        })
                    }
                }

                console.log(response)
            },
        )
    }

    // 展示/隐藏 放大图片
    handleZoomPicture = (flag, index) => {
        this.setState({
            imageOverSize: false,
            currShowImgIndex: index || 0
        })
    }

    // 加载放大图片弹窗
    renderZoomPicture = () => {
        const { imageOverSize, currShowImgIndex, imageFileArr, uriShow } = this.state
        console.log("🚀 ~ file: SampleManage.js:1294 ~ uriShow:", uriShow)
        let imgArr = [{
            url: uriShow
        }]
        return (
            <OverlayImgZoomPicker
                isShowImage={imageOverSize}
                currShowImgIndex={currShowImgIndex}
                zoomImages={imgArr}
                callBack={(flag) => this.handleZoomPicture(flag)}
            ></OverlayImgZoomPicker>
        )
    }

    cameraPost = (uri) => {
        const { recid } = this.state
        this.toast.show('图片上传中', 2000)

        let formData = new FormData();
        let data = {};
        data = {
            "action": "savephote",
            "servicetype": "photo_file",
            "express": "D2979AB2",
            "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
            "data": {
                "fileflag": "1",
                "username": Url.PDAusername,
                "recid": recid
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        formData.append('img', {
            uri: uri,
            type: 'image/jpeg',
            name: 'img'
        })
        console.log("🚀 ~ file: concrete_pouring.js ~ line 672 ~ SteelCage ~ formData", formData)

        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            console.log(res.statusText);
            console.log(res);
            return res.json();
        }).then(resData => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 690 ~ SteelCage ~ resData", resData)
            if (resData.status == '100') {
                let fileid = resData.result.fileid;
                let recid = resData.result.recid;

                let tmp = {}
                tmp.fileid = fileid
                tmp.uri = uri
                tmp.url = uri
                imageFileArr.push(tmp)
                this.setState({
                    fileid: fileid,
                    recid: recid,
                    pictureSelect: true,
                    imageFileArr: imageFileArr
                })
                console.log("🚀 ~ file: concrete_pouring.js ~ line 704 ~ SteelCage ~ imageFileArr", imageFileArr)
                this.toast.show('图片上传成功');
                this.setState({
                    isPhotoUpdate: false
                })
            } else {
                Alert.alert('图片上传失败', resData.message)
                this.toast.close(1)
            }
        }).catch((error) => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 692 ~ SteelCage ~ error", error)
        });
    }

    cameraPost2 = (uri) => {
        const { recid } = this.state
        this.toast.show('图片上传中', 2000)

        let formData = new FormData();
        let data = {};
        data = {
            "action": "savephote",
            "servicetype": "photo_file",
            "express": "D2979AB2",
            "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
            "data": {
                "fileflag": "1",
                "username": Url.PDAusername,
                "recid": recid
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        formData.append('img', {
            uri: uri,
            type: 'image/jpeg',
            name: 'img'
        })
        console.log("🚀 ~ file: concrete_pouring.js ~ line 672 ~ SteelCage ~ formData", formData)

        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            console.log(res.statusText);
            console.log(res);
            return res.json();
        }).then(resData => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 690 ~ SteelCage ~ resData", resData)
            if (resData.status == '100') {
                let fileid = resData.result.fileid;
                let recid = resData.result.recid;

                let tmp = {}
                tmp.fileid = fileid
                tmp.uri = uri
                tmp.url = uri
                imageFileArr2.push(tmp)
                this.setState({
                    fileid: fileid,
                    recid: recid,
                    pictureSelect2: true,
                    imageFileArr2: imageFileArr2
                })
                console.log("🚀 ~ file: concrete_pouring.js ~ line 704 ~ SteelCage ~ imageFileArr", imageFileArr)
                this.toast.show('图片上传成功');
                this.setState({
                    isPhotoUpdate: false
                })
            } else {
                Alert.alert('图片上传失败', resData.message)
                this.toast.close(1)
            }
        }).catch((error) => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 692 ~ SteelCage ~ error", error)
        });
    }

    cameraDelete = (item) => {
        let i = imageArr.indexOf(item)

        let formData = new FormData();
        let data = {};
        data = {
            "action": "deletephote",
            "servicetype": "photo_file",
            "express": "D2979AB2",
            "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
            "data": {
                "fileid": imageFileArr[i].fileid,
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: concrete_pouring.js ~ line 733 ~ SteelCage ~ cameraDelete ~ formData", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            console.log(res.statusText);
            console.log(res);
            return res.json();
        }).then(resData => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 745 ~ SteelCage ~ cameraDelete ~ resData", resData)
            if (resData.status == '100') {
                if (i > -1) {
                    imageArr.splice(i, 1)
                    imageFileArr.splice(i, 1)
                    this.setState({
                        imageArr: imageArr,
                        imageFileArr: imageFileArr
                    }, () => {
                        this.forceUpdate()
                    })
                }
                if (imageArr.length == 0) {
                    this.setState({
                        pictureSelect: false
                    })
                }
                this.toast.show('图片删除成功');
            } else {
                Alert.alert('图片删除失败', resData.message)
                this.toast.close(1)
            }
        }).catch((error) => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 761 ~ SteelCage ~ cameraDelete ~ error", error)
        });
    }

    cameraDelete2 = (item) => {
        let i = imageArr2.indexOf(item)

        let formData = new FormData();
        let data = {};
        data = {
            "action": "deletephote",
            "servicetype": "photo_file",
            "express": "D2979AB2",
            "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
            "data": {
                "fileid": imageFileArr2[i].fileid,
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: concrete_pouring.js ~ line 733 ~ SteelCage ~ cameraDelete ~ formData", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            console.log(res.statusText);
            console.log(res);
            return res.json();
        }).then(resData => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 745 ~ SteelCage ~ cameraDelete ~ resData", resData)
            if (resData.status == '100') {
                if (i > -1) {
                    imageArr2.splice(i, 1)
                    imageFileArr2.splice(i, 1)
                    this.setState({
                        imageArr2: imageArr2,
                        imageFileArr2: imageFileArr2
                    }, () => {
                        this.forceUpdate()
                    })
                }
                if (imageArr2.length == 0) {
                    this.setState({
                        pictureSelect2: false
                    })
                }
                this.toast.show('图片删除成功');
            } else {
                Alert.alert('图片删除失败', resData.message)
                this.toast.close(1)
            }
        }).catch((error) => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 761 ~ SteelCage ~ cameraDelete ~ error", error)
        });
    }

    imageView = () => {
        return (
            <View style={{ flexDirection: 'row' }}>
                {
                    this.state.pictureSelect ?
                        this.state.imageArr.map((item, index) => {
                            //if (index == 0) {
                            return (
                                <AvatarImg
                                    item={item}
                                    index={index}
                                    onPress={() => {
                                        this.setState({
                                            imageOverSize: true,
                                            uriShow: item
                                        })
                                    }}
                                    onLongPress={() => {
                                        Alert.alert(
                                            '提示信息',
                                            '是否删除抽检照片',
                                            [{
                                                text: '取消',
                                                style: 'cancel'
                                            }, {
                                                text: '删除',
                                                onPress: () => {
                                                    this.cameraDelete(item)
                                                }
                                            }])
                                    }} />
                            )
                        }) : <View></View>
                }
            </View>
        )
    }

    imageView2 = () => {
        return (
            <View style={{ flexDirection: 'row' }}>
                {
                    this.state.pictureSelect2 || this.state.isTitleThree ?
                        this.state.imageArr2.map((item, index) => {
                            //if (index == 0) {
                            return (
                                <AvatarImg
                                    item={item}
                                    index={index}
                                    onPress={() => {
                                        this.setState({
                                            imageOverSize: true,
                                            uriShow: item
                                        })
                                    }}
                                    onLongPress={() => {
                                        Alert.alert(
                                            '提示信息',
                                            '是否删除整改照片',
                                            [{
                                                text: '取消',
                                                style: 'cancel'
                                            }, {
                                                text: '删除',
                                                onPress: () => {
                                                    this.cameraDelete2(item)
                                                }
                                            }])
                                    }} />
                            )
                        }) : <View></View>
                }
            </View>
        )
    }

    // 扫码二维码
    comIDItem = (index) => {
        const { isGetcomID, buttondisable, compCode, compId, compData } = this.state
        return (
            <View>
                {
                    !isGetcomID ?
                        <View style={{ flexDirection: 'row' }}>

                            <ScanButton
                                onPress={() => {
                                    this.props.navigation.navigate('QRCode', {
                                        type: 'compid',
                                        page: 'SampleManage'
                                    })
                                }}
                                onLongPress={() => {
                                    this.setState({
                                        type: 'compid',
                                        focusIndex: index
                                    })
                                    NativeModules.PDAScan.onScan();
                                }}
                                onPressOut={() => {
                                    NativeModules.PDAScan.offScan();
                                }}
                            />
                        </View>
                        :
                        <TouchableCustom
                            onPress={() => {
                                console.log('onPress')
                            }}
                            onLongPress={() => {
                                console.log('onLongPress')
                                Alert.alert(
                                    '提示信息',
                                    '是否删除抽检信息',
                                    [{
                                        text: '取消',
                                        style: 'cancel'
                                    }, {
                                        text: '删除',
                                        onPress: () => {
                                            this.setState({
                                                compData: [],
                                                compCode: '',
                                                compId: '',
                                                iscomIDdelete: true,
                                                isGetcomID: false,
                                            })
                                        }
                                    }])
                            }} >
                            <Text>{compCode}</Text>
                        </TouchableCustom>
                }
            </View>

        )
    }

    // 图纸相关
    func = (state) => {
        console.log("🚀 ~ file: samplemanage.js ~ line 468 ~ SampleManage ~ state:", state)
        this.setState({
            isPaperTypeVisible: true
        })
    }

    // 不合格展示列表
    checkRefuseItem = (index) => {
        const { isCheckPage, isTitleOne, isTitleTwo, focusIndex, rectifyLiablesSelect } = this.state
        if (!this.state.sampleResultCheck) {
            return (
                <View>
                    <ListItemScan
                        topDivider
                        containerStyle={{ backgroundColor: '#A9E2F3' }}
                        title='质量问题'
                        rightElement={
                            isCheckPage || !isTitleOne ? <View /> :
                                <TouchableCustom onPress={() => { this.setState({ isQuestionOverlay: false }) }} >
                                    <IconDown text={'请选择'} />
                                </TouchableCustom>
                        } />
                    <View style={{ marginHorizontal: width * 0.05, backgroundColor: "#f8f8f8", borderRadius: 5, marginTop: 10 }}>
                        <FlatList
                            data={this.state.dictionary}
                            renderItem={this._renderItem.bind(this)}
                            extraData={this.state} />
                    </View>
                    <Text />

                    {
                        !isTitleOne ? <View /> :
                            isCheckPage ? <ListItemScan topDivider focusStyle={focusIndex == '9' ? styles.focusColor : {}} title='整改责任人:' rightElement={rectifyLiablesSelect.length > 12 ? rectifyLiablesSelect.substring(0, 11) + '...' : rectifyLiablesSelect} /> :
                                <TouchableCustom underlayColor={'lightgray'} onPress={() => {
                                    this.isItemVisible(rectifyLiables, '9', '')
                                }}>
                                    <ListItemScan topDivider focusStyle={focusIndex == '9' ? styles.focusColor : {}} title='整改责任人:' rightElement={this.downItem('9', '')} />
                                </TouchableCustom>
                    }

                    {
                        isTitleOne ? <View /> :
                            <ListItemScan topDivider focusStyle={focusIndex == '10' ? styles.focusColor : {}} title='抽检照片:' rightElement={() => this.state.imageArr.map((item, index) => {
                                //if (index == 0) {
                                return (
                                    <AvatarImg
                                        item={item}
                                        index={index}
                                        onPress={() => {
                                            this.setState({
                                                imageOverSize: true,
                                                uriShow: item
                                            })
                                        }}
                                        onLongPress={() => {
                                            Alert.alert(
                                                '提示信息',
                                                '是否删除抽检照片',
                                                [{
                                                    text: '取消',
                                                    style: 'cancel'
                                                }, {
                                                    text: '删除',
                                                    onPress: () => {
                                                        let i = imageArr.indexOf(item)
                                                        if (i > -1) {
                                                            imageArr.splice(i, 1)
                                                            this.setState({
                                                                imageArr: imageArr
                                                            }, () => {
                                                                this.forceUpdate()
                                                            })
                                                        }
                                                        if (imageArr.length == 0) {
                                                            this.setState({
                                                                pictureSelect: false
                                                            })
                                                        }
                                                    }
                                                }])
                                        }} />
                                )
                            })} />

                    }
                </View>
            )
        }
    }

    sampleImage = () => {
        const { isTitleOne, isTitleTwo, isTitleThree, titleList, pageType, isCheckComID, buttondisable } = this.state
        if (isTitleOne) {
            // 质量抽检管理页面
            return (
                <ListItem
                    containerStyle={{ paddingBottom: 6 }}
                    leftElement={
                        <View style={{ width: width / 2.7 }}>
                            <View >
                                {
                                    this.state.isCheckPage ? <View /> :
                                        <AvatarAdd
                                            pictureSelect={this.state.pictureSelect}
                                            backgroundColor='#4D8EF5'
                                            color='white'
                                            title="检"
                                            onPress={() => {
                                                if (this.state.isAppPhotoAlbumSetting) {
                                                    if (pageType == 'CheckPage') {
                                                        this.toast.show('抽检照片不可修改')
                                                    } else {
                                                        this.camandlibFunc()
                                                    }
                                                } else {
                                                    this.cameraFunc()
                                                }
                                            }} />
                                }
                                {this.imageView()}
                            </View>
                        </View>
                    }
                />
            )
        } else if (isTitleTwo || isTitleThree) {
            // 抽检整改页面
            return (
                <ListItem
                    containerStyle={{ paddingBottom: 6 }}
                    leftElement={
                        <View style={{ width: width / 2.7 }}>
                            <View >
                                {
                                    this.state.isCheckPage || isTitleThree ? <View /> :
                                        <AvatarAdd
                                            pictureSelect={this.state.pictureSelect2}
                                            backgroundColor='#4D8EF5'
                                            color='white'
                                            title="检"
                                            onPress={() => {
                                                if (this.state.isAppPhotoAlbumSetting) {
                                                    if (pageType == 'CheckPage') {
                                                        this.toast.show('抽检照片不可修改')
                                                    } else {
                                                        this.camandlibFunc()
                                                    }
                                                } else {
                                                    this.cameraFunc()
                                                }
                                            }} />
                                }
                                {this.imageView2()}
                            </View>
                        </View>
                    }
                />
            )
        }
    }

    sampleComCode = () => {
        const { isTitleOne, isTitleTwo, isTitleThree, isCheckPage, isGetcomID, compCode, focusIndex, isCheckComID, buttondisable } = this.state
        if (isTitleOne) {
            // 质量抽检管理页面
            return (
                isCheckPage ? <ListItemScan
                    topDivider
                    isButton={!isGetcomID}
                    focusStyle={focusIndex == '0' ? styles.focusColor : {}}
                    title='产品编号'
                    rightElement={compCode}
                /> :
                    <ListItemScan
                        topDivider
                        isButton={!isGetcomID}
                        focusStyle={focusIndex == '0' ? styles.focusColor : {}}
                        title='产品编号'
                        onPressIn={() => {
                            this.setState({
                                type: 'compid',
                                focusIndex: 0
                            })
                            NativeModules.PDAScan.onScan();
                        }}
                        onPress={() => {
                            this.setState({
                                type: 'compid',
                                focusIndex: 0
                            })
                            NativeModules.PDAScan.onScan();
                        }}
                        onPressOut={() => {
                            NativeModules.PDAScan.offScan();
                        }}
                        rightElement={
                            this.comIDItem(0)
                        }
                    />
            )
        } else if (isTitleTwo || isTitleThree) {
            // 抽检整改页面
            return (
                <ListItemScan
                    topDivider
                    isButton={!isGetcomID}
                    focusStyle={focusIndex == '0' ? styles.focusColor : {}}
                    title='产品编号'
                    onPressIn={() => {
                        this.setState({
                            isScanCheck: true
                        })
                        this.setState({
                            type: 'compid',
                            focusIndex: 0
                        })
                        NativeModules.PDAScan.onScan();
                    }}
                    onPress={() => {
                        this.setState({
                            isScanCheck: true
                        })
                        this.setState({
                            type: 'compid',
                            focusIndex: 0
                        })
                        NativeModules.PDAScan.onScan();
                    }}
                    onPressOut={() => {
                        this.setState({
                            isScanCheck: true
                        })
                        NativeModules.PDAScan.offScan();
                    }}
                    rightElement={
                        isCheckComID || isCheckPage || isTitleThree ? <Text>{compCode}</Text> :
                            <View>
                                {
                                    isCheckComID ? <View /> :
                                        <View style={{ flexDirection: 'row' }}>
                                            <Button
                                                onPressIn={() => {
                                                    this.setState({
                                                        isScanCheck: true
                                                    })
                                                    this.setState({
                                                        type: 'compid',
                                                        focusIndex: 0
                                                    })
                                                    NativeModules.PDAScan.onScan();
                                                }}
                                                onPress={() => {
                                                    this.setState({
                                                        isScanCheck: true
                                                    })
                                                    setTimeout(() => {
                                                        this.setState({ GetError: false, buttondisable: false })
                                                        varGetError = false
                                                    }, 1500)
                                                    this.props.navigation.navigate('QRCode', {
                                                        type: 'compid',
                                                        page: 'SampleManage'
                                                    })
                                                }}
                                                onPressOut={() => {
                                                    this.setState({
                                                        isScanCheck: true
                                                    })
                                                    NativeModules.PDAScan.offScan();
                                                }}
                                                titleStyle={{ fontSize: 14 }}
                                                title='扫码验证'
                                                //iconRight={true}
                                                //icon={<Icon type='antdesign' name='scan1' color='white' size={14} />}
                                                type='solid'
                                                buttonStyle={{ paddingVertical: 6, backgroundColor: '#4D8EF5', marginVertical: 0 }} />
                                        </View>
                                }
                            </View>
                    }
                />
            )
        }


    }

    // 查询页面扫码数据初始化
    compDataInit(data) {
        this.setState({
            "compData": {
                "result": [{
                    "projectName": data.project_name,
                    "compTypeName": data.comp_type,
                    "designType": data.design_type,
                    "floorNoName": data.floor_no,
                    "floorName": data.floor
                }]
            }
        })
    }
    //}

    render() {
        const { navigation } = this.props;
        //从主页面传url, 未来集成到一起
        const QRurl = navigation.getParam('QRurl') || '';
        const QRid = navigation.getParam('QRid') || ''
        const type = navigation.getParam('type') || '';
        let pageType = navigation.getParam('pageType') || ''

        if (type == 'compid') {
            QRcompid = QRid
        }
        const { isCheckPage, isTitleOne, isTitleTwo, isGetcomID, focusIndex, currentState, originalOperator, sampleResult, room, library, samplePerson, remarkSample, rectifyLiables, compData, pictureSelect, rectifyLiablesVisible, bottomData, sampleDate, isPaperTypeVisible, paperList, compDrawing, sampleResultCheck, rectifyDate, rectifyPerson, remarkRectify, sampleRecord, sampleRecordList, isTitleThree, isMainCheckPageScan, mainCheckPageScanMessage } = this.state

        if (this.state.isLoading) {
            return (
                <View style={styles.loading}>
                    <ActivityIndicator
                        animating={true}
                        color='#419FFF'
                        size="large" />
                </View>
            )
            //渲染页面
        } else {
            return (
                <View style={{ flex: 1 }}>
                    <Toast ref={(ref) => { this.toast = ref; }} position="center" />
                    <NavigationEvents
                        onWillFocus={this.UpdateControl}
                        onWillBlur={() => {
                            if (QRid != '') {
                                navigation.setParams({ QRid: '', type: '' })
                            }
                        }} />
                    <ScrollView ref={ref => this.scoll = ref} style={styles.mainView} showsVerticalScrollIndicator={false} >

                        <View style={styles.listView}>

                            {this.sampleImage()}

                            {this.sampleComCode()}

                            {/*扫码后展示信息*/}
                            {
                                isCheckPage ? <CardListSampleManage compData={compData} func={this.func.bind(this)} />
                                    : this.state.isGetcomID ?
                                        <CardListSampleManage compData={compData} func={this.func.bind(this)} /> : <View></View>
                            }

                            {
                                !isTitleOne ? <View /> : <ListItemScan topDivider focusStyle={focusIndex == '1' ? styles.focusColor : {}} title='抽检日期:' rightElement={this._DatePicker(1)} />

                            }
                            {
                                !isTitleOne ? <View /> : <ListItemScan focusStyle={focusIndex == '2' ? styles.focusColor : {}} title='当前状态:' rightElement={currentState} />

                            }
                            {
                                !isTitleOne ? <View /> : <ListItemScan focusStyle={focusIndex == '3' ? styles.focusColor : {}} title='原操作人:' rightElement={originalOperator} />

                            }

                            {
                                !isTitleOne ? <View /> :
                                    isCheckPage ? <ListItemScan focusStyle={focusIndex == '4' ? styles.focusColor : {}} title='检查结果:' rightElement={sampleResult} /> :
                                        <ListItemScan focusStyle={focusIndex == '4' ? styles.focusColor : {}} title='检查结果:' rightTitle={
                                            <View style={{ flexDirection: "row", marginRight: -23, paddingVertical: 0 }}>
                                                <CheckBoxScan
                                                    title='合格'
                                                    checked={sampleResultCheck}
                                                    onPress={() => {
                                                        this.setState({
                                                            sampleResultCheck: true,
                                                            sampleResult: '合格'
                                                        })
                                                    }}
                                                />
                                                <CheckBoxScan
                                                    title='不合格'
                                                    checked={!sampleResultCheck}
                                                    onPress={() => {
                                                        this.setState({
                                                            sampleResultCheck: false,
                                                            sampleResult: '不合格'
                                                        })
                                                    }}
                                                />
                                            </View>
                                        } />
                            }

                            {/*质量问题及改进措施*/}
                            {this.checkRefuseItem(9)}

                            {
                                !isTitleOne ? <View /> : <ListItemScan focusStyle={focusIndex == '5' ? styles.focusColor : {}} title='所在库房:' rightElement={room} />

                            }
                            {
                                !isTitleOne ? <View /> : <ListItemScan focusStyle={focusIndex == '6' ? styles.focusColor : {}} title='所在库位:' rightElement={library} />
                            }
                            {
                                !isTitleOne ? <View /> : <ListItemScan focusStyle={focusIndex == '7' ? styles.focusColor : {}} title='抽检员:' rightElement={samplePerson} />
                            }
                            {
                                !isTitleOne ? <View /> : <ListItemScan focusStyle={focusIndex == '8' ? styles.focusColor : {}} title='备注:' rightElement={() =>
                                    isCheckPage ? <Text>{remarkSample}</Text> :
                                        <View>
                                            <Input
                                                containerStyle={styles.quality_input_container}
                                                inputContainerStyle={styles.inputContainerStyle}
                                                inputStyle={[styles.quality_input_, { top: 7 }]}
                                                placeholder='请输入'
                                                value={remarkSample}
                                                onChangeText={(value) => {
                                                    this.setState({
                                                        remarkSample: value
                                                    })
                                                }} />
                                        </View>} />
                            }

                            {
                                isTitleOne ? <View /> : isMainCheckPageScan ? <View /> :
                                    <Card containerStyle={styles.card_containerStyle} >
                                        <Text style={{ textAlign: 'center' }}>构件抽检记录</Text>
                                        {
                                            sampleRecordList.map((item, index) => {
                                                if (index < sampleRecordList.length - 1) {
                                                    return (
                                                        <ListItemScan_child><Text>{sampleRecordList[index] + ';'}</Text></ListItemScan_child>
                                                    )
                                                }
                                            })
                                        }
                                    </Card>
                            }
                            <Text />

                            {
                                !isTitleTwo ? <View /> : <ListItemScan topDivider focusStyle={focusIndex == '11' ? styles.focusColor : {}} title='整改日期:' rightElement={this._DatePickerRectifyDate(11)} />
                            }
                            {
                                !isTitleTwo ? <View /> : <ListItemScan focusStyle={focusIndex == '12' ? styles.focusColor : {}} title='整改人:' rightElement={rectifyPerson} />
                            }
                            {
                                !isTitleTwo ? <View /> : <ListItemScan focusStyle={focusIndex == '13' ? styles.focusColor : {}} title='备注:' rightElement={() =>
                                    isCheckPage ? <Text>{remarkRectify}</Text> :
                                        <View>
                                            <Input
                                                containerStyle={styles.quality_input_container}
                                                inputContainerStyle={styles.inputContainerStyle}
                                                inputStyle={[styles.quality_input_, { top: 7 }]}
                                                placeholder='请输入'
                                                value={remarkRectify}
                                                onChangeText={(value) => {
                                                    this.setState({
                                                        remarkRectify: value
                                                    })
                                                }} />
                                        </View>} />
                            }
                        </View>

                        {isCheckPage ? <View /> :
                            isTitleOne ? <FormButton
                                backgroundColor='#17BC29'
                                onPress={() => {
                                    this.PostData()

                                }}
                                disabled={this.state.isPhotoUpdate}
                                title='保存'
                            />
                                : isTitleTwo ? <FormButton
                                    backgroundColor='#17BC29'
                                    onPress={() => {
                                        this.PostData()

                                    }}
                                    disabled={this.state.isPhotoUpdate}
                                    title='申请复检'
                                />
                                    : isTitleThree ? <FormButton
                                        backgroundColor='#17BC29'
                                        onPress={() => {
                                            this.PostData()

                                        }}
                                        disabled={this.state.isPhotoUpdate}
                                        title='复检合格'
                                    /> : <View />
                        }

                        {/*整改责任人底栏*/}
                        <BottomSheet
                            onBackdropPress={() => {
                                this.setState({
                                    rectifyLiablesVisible: false
                                })
                            }}
                            isVisible={rectifyLiablesVisible && !this.state.isCheckPage} onRequestClose={() => {
                                this.setState({
                                    rectifyLiablesVisible: false
                                })
                            }}
                        >
                            <ScrollView style={styles.bottomsheetScroll}>
                                {bottomData.map((item, index) => {
                                    this.setState({
                                        checked: rectifyLiablesCheck[index]
                                    })
                                    let title = item.inspectorName
                                    return (
                                        <TouchableCustom
                                            onPress={() => {
                                                rectifyLiablesCheck[index] = !rectifyLiablesCheck[index]
                                                this.setState({
                                                    checked: !this.state.checked
                                                })
                                            }}
                                        >
                                            <View style={{
                                                paddingVertical: 18,
                                                backgroundColor: 'white',
                                                alignItems: 'center',
                                                flexDirection: 'row',
                                                borderBottomColor: '#DDDDDD',
                                                borderBottomWidth: 0.6
                                            }}>
                                                <CheckBoxScan
                                                    checked={rectifyLiablesCheck[index]}
                                                    onPress={() => {
                                                        rectifyLiablesCheck[index] = !rectifyLiablesCheck[index]
                                                        this.setState({
                                                            checked: !this.state.checked
                                                        })
                                                    }}
                                                />
                                                <Text style={{ color: '#333', textAlign: 'center', fontSize: 14, textAlignVertical: 'center' }}>{title}</Text>
                                            </View>
                                        </TouchableCustom>
                                    )
                                })}
                            </ScrollView>
                            <TouchableHighlight
                                onPress={() => {
                                    let result = '请选择'
                                    let resultId = ''
                                    bottomData.map((item, index) => {
                                        if (rectifyLiablesCheck[index] == true) {
                                            if (result == '请选择') result = ''
                                            result = result + ',' + item.inspectorName
                                            resultId = resultId + ',' + item.inspectorId
                                        }
                                    })
                                    result = result == '请选择' ? result : result.substring(1, result.length)
                                    resultId = result == '请选择' ? resultId : resultId.substring(1, resultId.length)
                                    this.setState({ rectifyLiablesVisible: false, rectifyLiablesSelect: result, rectifyLiablesIdSelect: resultId })
                                }}>
                                <BottomItem backgroundColor='#e74c3c' color="white" title={'确定'} />
                            </TouchableHighlight>
                        </BottomSheet>

                        {/*查看图纸底栏*/}
                        <BottomSheet
                            isVisible={isPaperTypeVisible}
                            onRequestClose={() => {
                                this.setState({
                                    isPaperTypeVisible: false
                                })
                            }}
                        >
                            <ScrollView style={styles.bottomsheetScroll}>
                                {
                                    paperList.map((item, index) => {
                                        return (
                                            <TouchableCustom
                                                key={index}
                                                onPress={() => {
                                                    this.paperResData(item)
                                                }}
                                            >
                                                <BottomItem backgroundColor='white' color="#333" title={item} />
                                            </TouchableCustom>
                                        )

                                    })
                                }
                            </ScrollView>
                            <TouchableHighlight
                                onPress={() => {
                                    this.setState({ isPaperTypeVisible: false })
                                }}>
                                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
                            </TouchableHighlight>

                        </BottomSheet>

                        {/*照片放大*/}
                        {this.renderZoomPicture()}
                        {/* {overlayImg(obj = {
                            onRequestClose: () => {
                                this.setState({ imageOverSize: !this.state.imageOverSize }, () => {
                                    console.log("🚀 ~ file: equipment_maintenance.js:1696 ~ render ~ imageOverSize:", this.state.imageOverSize)
                                })
                            },
                            onPress: () => {
                                this.setState({
                                    imageOverSize: !this.state.imageOverSize
                                })
                            },
                            uri: this.state.uriShow,
                            isVisible: this.state.imageOverSize
                        })
                        } */}

                        {/*图纸相关*/}
                        <Overlay
                            fullScreen={true}
                            animationType='fade'
                            isVisible={this.state.isPDFVisible}
                            onRequestClose={() => {
                                this.setState({ isPDFVisible: !this.state.isPDFVisible });
                            }}
                        >
                            <Header
                                containerStyle={{ height: 45, paddingTop: 0, top: -5 }}
                                centerComponent={{ text: '图纸预览', style: { color: 'gray', fontSize: 20 } }}
                                rightComponent={<BackIcon
                                    onPress={() => {
                                        this.setState({
                                            isPDFVisible: !this.state.isPDFVisible
                                        });
                                    }} />}
                                backgroundColor='white'
                            />
                            <View style={{ flex: 1 }}>
                                <Pdf
                                    source={{
                                        uri: compDrawing,
                                        //method: 'GET', //默认 'GET'，请求 url 的方式
                                    }}
                                    fitWidth={true} //默认 false，若为 true 则不能将 fitWidth = true 与 scale 一起使用
                                    fitPolicy={0} // 0:宽度对齐，1：高度对齐，2：适合两者（默认）
                                    page={1}
                                    //scale={1}
                                    onLoadComplete={(numberOfPages, filePath, width, height, tableContents) => {
                                        console.log(`number of pages: ${numberOfPages}`); //总页数
                                        console.log(`number of filePath: ${filePath}`); //本地返回的路径
                                        console.log(`number of width: `, JSON.stringify(width));
                                        console.log(`number of height: ${JSON.stringify(height)}`);
                                        console.log(`number of tableContents: ${tableContents}`);
                                    }}
                                    onError={(error) => {
                                        console.log(error);
                                    }}
                                    minScale={1} //最小模块
                                    maxScale={3}
                                    enablePaging={true} //在屏幕上只能显示一页
                                    style={{
                                        flex: 1,
                                        width: width
                                    }}
                                />
                            </View>

                        </Overlay>
                    </ScrollView>
                </View>
            )
        }
    }

    checkListSecondisVisible = (item) => {
        if (isCheckSecondList.indexOf(item.defectTypeId) == -1) {
            console.log('1812')
            isCheckSecondList.push(item.defectTypeId);
            this.setState({ isCheckSecondList: isCheckSecondList });
        } else {
            if (isCheckSecondList.length == 1) {
                isCheckSecondList.splice(0, 1);
                this.setState({ isCheckSecondList: isCheckSecondList }, () => {
                    this.forceUpdate;
                });
            } else {
                isCheckSecondList.map((item1, index) => {
                    if (item1 == item.defectTypeId) {
                        isCheckSecondList.splice(index, 1);
                        this.setState({ isCheckSecondList: isCheckSecondList }, () => {
                            this.forceUpdate;
                        });
                    }
                });
            }
        }
    }

    //主列全选功能
    allMainCheck = (item) => {
        //主列全选
        if (isCheckedAllList.indexOf(item.defectTypeId) == -1) {
            //遍历子列，全部勾选
            item.problemDefects.map((defectitem, defectindex) => {
                if (questionChickIdArr.indexOf(defectitem.rowguid) == -1) {
                    questionChickArr.push(defectitem)
                    questionChickIdArr.push(defectitem.rowguid)
                    this.setState({
                        questionChickArr: questionChickArr,
                        questionChickIdArr: questionChickIdArr,
                    })
                }
            })
            //子列全部勾选就勾选上主列
            isCheckedAllList.push(item.defectTypeId)
            this.setState({ isCheckedAllList: isCheckedAllList })
        }
        //主列取消全选
        else {
            //遍历子列，依次取消勾选
            item.problemDefects.map((defectitem, defectindex) => {
                if (questionChickIdArr.indexOf(defectitem.rowguid) != -1) {
                    let num = questionChickIdArr.indexOf(defectitem.rowguid)
                    questionChickIdArr.splice(num, 1)
                    questionChickArr.splice(num, 1)
                    this.setState({
                        questionChickArr: questionChickArr,
                        questionChickIdArr: questionChickIdArr,
                    })
                }
            })
            //把主列数据取消勾选
            let frinum = isCheckedAllList.indexOf(item.defectTypeId)
            isCheckedAllList.splice(frinum, 1)
            this.setState({ isCheckedAllList: isCheckedAllList })
        }
    }

    sonCheck = (item, defectitem) => {
        //点击勾选☑️
        if (questionChickIdArr.indexOf(defectitem.rowguid) == -1) {
            //数组中添加勾选的子列数据
            questionChickArr.push(defectitem)
            questionChickIdArr.push(defectitem.rowguid)
            this.setState({
                questionChickArr: questionChickArr,
                questionChickIdArr: questionChickIdArr,
            }, () => {
                this.forceUpdate()
            })
            //记录子列是否全部勾选，用数量对比
            let count = 0
            item.problemDefects.map((judgeitem) => {
                if (questionChickIdArr.indexOf(judgeitem.rowguid) != -1) {
                    count += 1
                }
            })
            //子列全部勾选，就勾选主列
            if (count == item.problemDefects.length) {
                isCheckedAllList.push(item.defectTypeId)
                this.setState({ isCheckedAllList: isCheckedAllList })
            }

        }
        //反勾选 
        else {
            //查出取消勾选项的角标
            let num = questionChickIdArr.indexOf(defectitem.rowguid)
            //删掉！
            questionChickIdArr.splice(num, 1)
            questionChickArr.splice(num, 1)
            this.setState({
                questionChickArr: questionChickArr,
                questionChickIdArr: questionChickIdArr
            })
        }
        //如果子列取消全部勾选，主列取消勾选
        if (questionChickIdArr.indexOf(defectitem.rowguid) == -1) {
            let frinum = isCheckedAllList.indexOf(item.defectTypeId)
            isCheckedAllList.splice(frinum, 1)
            this.setState({ isCheckedAllList: isCheckedAllList })
        }
    }

    _renderItem = ({ item, index }) => {
        const { isCheckSecondListVisible, isCheckSecondList } = this.state
        return (
            <View style={{}}>
                <ListItem
                    title={
                        <View style={{ flexDirection: 'row' }}>
                            {/* <Icon name={isCheckSecondList.indexOf(item.defectTypeId) != -1 ? 'downcircle' : 'rightcircle'} size={18} type='antdesign' color='#419FFF' /> */}
                            <Text style={{ fontSize: 14, marginLeft: 4 }}>{item.defectTypeName}</Text>
                        </View>
                    }
                    underlayColor={'lightgray'}
                    containerStyle={{ marginVertical: 0, paddingVertical: 10, borderColor: 'transparent', borderWidth: 0, backgroundColor: "transparent" }}
                    onPress={() => {
                        this.checkListSecondisVisible(item)
                        this.forceUpdate()
                    }}
                    rightElement={
                        this.state.isCheckPage || !this.state.isTitleOne ? <View /> :
                            <CheckBoxScan
                                key={index}
                                //checkedColor='red'
                                checked={this.state.isCheckedAllList.indexOf(item.defectTypeId) != -1}
                                onPress={() => {
                                    this.allMainCheck(item)
                                    this.forceUpdate()
                                }
                                } />
                    } />
                {
                    isCheckSecondList.indexOf(item.defectTypeId) != -1 ?
                        (
                            <View>
                                {item.problemDefects.map((defectitem, defectindex) => {
                                    return (
                                        <View>
                                            <ListItem
                                                title={defectitem.defect + ": " + defectitem.measure}
                                                titleStyle={{ fontSize: 13 }}
                                                containerStyle={{ marginVertical: 0, paddingVertical: 10, borderRadius: 0, borderColor: 'lightgray', borderWidth: 0, backgroundColor: "rgba(77,142,245,0.10)" }}
                                                onPress={() => {
                                                    this.sonCheck(item, defectitem)
                                                    this.forceUpdate()
                                                }
                                                }
                                                underlayColor='lightgray'
                                                rightElement={
                                                    this.state.isCheckPage || !this.state.isTitleOne ? <View /> :
                                                        <CheckBoxScan
                                                            key={index}
                                                            title=''
                                                            //checkedColor='red'
                                                            checked={this.state.questionChickIdArr.indexOf(defectitem.rowguid) != -1}
                                                        />
                                                } />
                                        </View>

                                    )
                                })}
                                <View style={{ height: 5, backgroundColor: "rgba(77,142,245,0.10)", borderBottomLeftRadius: 10, borderBottomRightRadius: 10 }}></View>
                            </View>
                        ) : <View></View>
                }

            </View>
        )
    }
}
