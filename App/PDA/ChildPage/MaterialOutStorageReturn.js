import React from 'react';
import { View, StyleSheet, TouchableHighlight, Dimensions, ActivityIndicator, Alert, Image } from 'react-native';
import { Button, Text, Input, Icon, Card, ListItem, BottomSheet, Overlay, Header } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { ScrollView } from 'react-native-gesture-handler';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { NavigationEvents } from 'react-navigation';
import { styles } from '../Componment/PDAStyles';
import ListItemScan from '../Componment/ListItemScan';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import AvatarImg from '../Componment/AvatarImg';
import DatePicker from 'react-native-datepicker'
import ScanButton from '../Componment/ScanButton';

import { DeviceEventEmitter, NativeModules } from 'react-native';
import overlayImg from '../Componment/OverlayImg';
import OverlayImgZoomPicker from '../Componment/OverlayImgZoomPicker';
class BackIcon extends React.PureComponent {
    render() {
        return (
            <TouchableCustom
                onPress={this.props.onPress} >
                <Icon
                    name='arrow-back'
                    color='#419FFF' />
            </TouchableCustom>
        )
    }
}

let fileflag = "20"

let imageArr = [], imageFileArr = [];

let QRidArr = []

const { height, width } = Dimensions.get('window') //获取宽高

export default class MaterialOutStorageReturn extends React.PureComponent {
    constructor() {
        super();
        this.state = {
            title: "",
            type: 'compid',
            resData: [],
            rowguid: '',
            formNo: '', // 表单编号
            tabulationDate: '', // 编制日期
            returnType: '请选择', // 退库类型
            department: '请选择', // 部门
            departCode: '',
            departmentId: '',
            shop: '请选择', // 车间
            shopId: '',
            room: '请选择', // 库房
            roomId: '',
            totalAmount: '0', // 金额
            tabulator: '', // 编制人
            remark: '', // 备注,

            materialInfos: [],
            //materialIds: '', // 已选材料id
            returnTypeList: [],
            departmentList: [],
            shopList: [],
            roomList: [],

            // 选择确认


            //底栏控制
            bottomData: [],
            returnTypeVisible: false,
            departmentVisible: false,
            shopVisible: false,
            roomVisible: false,

            checked: true,

            focusIndex: -1,
            isLoading: false,

            imageArr: [],
            imageFileArr: [],
            pictureSelect: false,
            pictureUri: '',
            imageOverSize: false,
            fileid: "",
            recid: "",
            isPhotoUpdate: false,
            uriShow: '',

            //查询界面取消选择按钮
            isCheckPage: false,
            isCheckPageEdit: false,

            //控制app拍照，日期，相册选择功能
            isAppPhotoSetting: false,
      currShowImgIndex: 0,
            isAppDateSetting: false,
            isAppPhotoAlbumSetting: false
        }
    }

    componentDidMount() {
        const { navigation } = this.props;
        let guid = navigation.getParam('guid') || ''
        let pageType = navigation.getParam('pageType') || ''
        let title = navigation.getParam('title')
        let isAppPhotoSetting = navigation.getParam('isAppPhotoSetting')
        let isAppDateSetting = navigation.getParam('isAppDateSetting')
        let isAppPhotoAlbumSetting = navigation.getParam('isAppPhotoAlbumSetting')
        this.setState({
            isAppPhotoSetting: isAppPhotoSetting,
            isAppDateSetting: isAppDateSetting,
            isAppPhotoAlbumSetting: isAppPhotoAlbumSetting
        })
        this.setState({
            title: title
        })
        if (pageType == 'CheckPage') {
            let guid = navigation.getParam('guid') || ''
            this.checkResData(guid)
        } else {
            this.resData()
        }
    }

    componentWillUnmount() {
        imageArr = [], imageFileArr = [];
    }

    UpdateControl = () => {
        const { navigation } = this.props;
        const { } = this.state

        let type = navigation.getParam('type') || '';
        if (type == 'compid') {
            let QRid = navigation.getParam('QRid') || ''
            this.scanMaterial(QRid)
        }

        let mis = []
        mis = navigation.getParam('materialInfos') || []
        let pickingInfoId = navigation.getParam('pickingInfoId') || ""
        let state = navigation.getParam('state') || ""
        let totalAmount = navigation.getParam('totalAmount') || "0"
        if (state == "addMaterial") {
            totalAmount = (Math.round((Number.parseFloat(totalAmount) + Number.parseFloat(this.state.totalAmount)) * 100) / 100).toString()
            mis.map((item, index) => {
                item._IsDel = "0"
            })
            this.state.materialInfos.map((item1, index1) => {
                let check = true
                mis.map((item2, index2) => {
                    if (item1.id == item2.id) {
                        check = false
                        return
                    }
                })
                if (check) mis.push(item1)
            })
            this.setState({
                materialInfos: mis,
                totalAmount: totalAmount
            })
        } else if (state == "requisitionCount") {
            let outNumber = navigation.getParam('outNumber') || "0"
            let price = navigation.getParam('price') || "0"
            mis = this.state.materialInfos
            mis.map((item, index) => {
                if (item.id == pickingInfoId) {
                    item.outNumber = outNumber
                    item.price = price
                }
                totalAmount = (Math.round((Number.parseFloat(totalAmount) + Number.parseFloat(item.price)) * 100) / 100).toString()
            })
            this.setState({
                materialInfos: mis,
                totalAmount: totalAmount
            })
        }
        this.props.navigation.setParams({
            type: '',
            state: '',
            totalAmount: '',
            materialInfos: []
        })

        this.forceUpdate()
    }

    scanMaterial = (guid) => {
        if (typeof this.props.navigation.getParam('QRid') != "undefined") {
            if (this.props.navigation.getParam('QRid') != "") {
                this.toast.show(Url.isLoadingView, 0)
            }
        }
        const { returnType, shop, shopId, department, departmentId, materialInfos, title } = this.state
        let formData = new FormData();
        let data = {};
        if (QRidArr.indexOf(guid) != -1) {
            this.toast.show('材料重复')
            return
        } else {
            QRidArr.push(guid)
        }
        let deptName = returnType == '车间' ? shop : department
        let deptId = returnType == '车间' ? shopId : departmentId
        let materialIds = ''
        materialInfos.map((item, index) => {
            if (item._IsDel == '0') materialIds += index == materialInfos.length - 1 ? item.id : item.id + ','
        })
        data = {
            "action": "outstoragereturnscan",
            "servicetype": "pdamaterial",
            "express": "6DFD1C9B",
            "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
            "data": {
                "factoryId": Url.PDAFid,
                "rowguid": guid,
                "returnType": returnType,
                "materialIds": materialIds,
                "deptName": deptName,
                "deptId": deptId
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: MaterialOutStorageReturn.js ~ line 130 ~ MaterialOutStorageReturn ~ formData:", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res.statusText);
            //console.log(res);
            return res.json();
        }).then(resData => {
            this.toast.close()
            if (resData.status == '100') {
                console.log("MaterialOutStorageReturn ---- 267 ----" + JSON.stringify(resData.result))
                let mis = this.state.materialInfos
                let result = resData.result
                let totalAmount = this.state.totalAmount
                if (result.length > 1) {
                    setTimeout(() => {
                        this.props.navigation.navigate('SelectMaterial', {
                            title: '选择材料',
                            action: title,
                            returnType: returnType,
                            materialIds: materialIds,
                            deptName: deptName,
                            deptId: deptId,
                            keyStr: result[0].code
                        })
                    }, 1000)
                } else {
                    if (mis != []) {
                        result.map((item, index) => {
                            let check = true
                            this.state.materialInfos.map((item2, index2) => {
                                if (item.id == item2.id) {
                                    check = false
                                    return
                                }
                            })
                            if (check) {
                                item._IsDel = "0"
                                mis.push(item)
                                totalAmount = (Math.round((Number.parseFloat(totalAmount) + Number.parseFloat(item.price)) * 100) / 100).toString()
                            }
                        })
                    } else {
                        mis = result
                        mis.map((item, index) => {
                            item._IsDel = "0"
                            totalAmount = (Math.round((Number.parseFloat(totalAmount) + Number.parseFloat(item.price)) * 100) / 100).toString()
                        })
                    }
                    this.setState({
                        materialInfos: mis,
                        totalAmount: totalAmount
                    })
                }
            } else {
                Alert.alert("错误提示", resData.message)
            }

        }).catch((error) => {
            console.log("🚀 ~ file: MaterialOutStorageReturn.js ~ line 233 ~ MaterialOutStorageReturn ~ error:", error)
        });
    }

    //顶栏
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('title')
        }
    }

    resData = () => {
        let formData = new FormData();
        let data = {};
        data = {
            "action": "outstoragereturninit",
            "servicetype": "pdamaterial",
            "express": "6DFD1C9B",
            "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
            "data": { "factoryId": Url.PDAFid }
        }
        formData.append('jsonParam', JSON.stringify(data))
        //console.log("🚀 ~ file: MaterialOutStorageReturn.js ~ line 130 ~ MaterialOutStorageReturn ~ formData:", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res.statusText);
            //console.log(res);
            return res.json();
        }).then(resData => {
            if (resData.status == '100') {

                //let dictionary = resData.result.defectTypes
                //console.log("MaterialOutStorageReturn ---- 145 ----" + JSON.stringify(resData.result))
                this.setState({
                    //resData: resData.result,
                    rowguid: resData.result.rowguid,
                    formNo: resData.result.formNo,
                    tabulationDate: resData.result.tabulationDate,
                    returnTypeList: resData.result.returnType,
                    departmentList: resData.result.department,
                    shopList: resData.result.shop,
                    roomList: resData.result.room,
                    tabulator: Url.PDAEmployeeName,
                    isLoading: false
                })
            } else {
                Alert.alert("错误提示", resData.message, [{
                    text: '取消',
                    style: 'cancel'
                }, {
                    text: '返回上一级',
                    onPress: () => {
                        this.props.navigation.goBack()
                    }
                }])
            }

        }).catch((error) => {
            console.log("🚀 ~ file: MaterialOutStorageReturn.js ~ line 233 ~ MaterialOutStorageReturn ~ error:", error)
        });

    }

    checkResData = (guid) => {
        let formData = new FormData();
        let data = {};
        data = {
            "action": "outstoragereturnquerydetail",
            "servicetype": "pdamaterial",
            "express": "6DFD1C9B",
            "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
            "data": {
                "factoryId": Url.PDAFid,
                "guid": guid
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: MaterialOutStorageReturn.js ~ line 130 ~ MaterialOutStorageReturn ~ formData:", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res.statusText);
            //console.log(res);
            return res.json();
        }).then(resData => {
            if (resData.status == '100') {
                console.log("MaterialProjectPicking ---- 267 ----" + JSON.stringify(resData.result))
                if (resData.result.chkstatus == '在编') {
                    this.setState({
                        returnTypeList: resData.result.returnTypeList,
                        departmentList: resData.result.departmentList,
                        shopList: resData.result.shopList,
                        roomList: resData.result.roomList,
                    })
                }
                imageArr = resData.result.fileAttach
                let fileAttach = resData.result.fileAttach
                if (fileAttach.length > 0) {
                    fileAttach.map((item, index) => {
                        let tmp = {}
                        tmp.fileid = ""
                        tmp.uri = item
                        tmp.url = item
                        imageFileArr.push(tmp)
                    })
                }
                this.setState({
                    imageFileArr: imageFileArr
                })
                let materialInfo = resData.result.materialInfo
                if (materialInfo != null && materialInfo != []) {
                    materialInfo.map((item, index) => {
                        item._IsDel = '0'
                    })
                }
                this.setState({
                    rowguid: resData.result.rowguid,
                    formNo: resData.result.formNo,
                    tabulationDate: resData.result.tabulationDate,
                    returnType: resData.result.returnType,
                    department: resData.result.returnType == '部门' ? resData.result.deptName : '',
                    departmentId: resData.result.returnType == '部门' ? resData.result.deptId : '',
                    shop: resData.result.returnType == '车间' ? resData.result.deptName : '',
                    shopId: resData.result.returnType == '车间' ? resData.result.deptId : '',
                    room: resData.result.room,
                    roomId: resData.result.roomId,
                    totalAmount: resData.result.totalAmount,
                    tabulator: resData.result.tabulator,
                    remark: resData.result.remark,
                    materialInfos: materialInfo,
                    imageArr: resData.result.fileAttach,
                    pictureSelect: true,
                    isCheckPage: resData.result.chkstatus == '在编' ? false : true,
                    isCheckPageEdit: resData.result.chkstatus == '在编' ? true : false,
                    isLoading: false
                })
            } else {
                Alert.alert("错误提示", resData.message, [{
                    text: '取消',
                    style: 'cancel'
                }, {
                    text: '返回上一级',
                    onPress: () => {
                        this.props.navigation.goBack()
                    }
                }])
            }

        }).catch((error) => {
            console.log("🚀 ~ file: MaterialOutStorageReturn.js ~ line 233 ~ MaterialOutStorageReturn ~ error:", error)
        });
    }

    PostData = (checkstate) => {
        const { isCheckPageEdit, room, materialInfos, recid } = this.state
        let formData = new FormData();
        let data = {};
        let message = ''
        if (room == '请选择' || room == '') {
            message = '请选择退回库房'
            return
        }

        if (materialInfos.length == 0) {
            message = '请选择材料'
        } else {
            materialInfos.map((item, index) => {
                if (item._IsDel == '0' && item.outNumber == '0') message = "材料编码" + item.code + "，请选择退库数量"
                return
            })
        }

        if (message != '') {
            this.toast.show(message)
            return
        }

        if (this.state.isAppPhotoSetting) {
            if (imageArr.length == 0) {
                this.toast.show('请先拍摄构件照片');
                return
            }
        }

        this.toast.show(saveLoading, 0)
        let MOutStorageReturn = this.MOutStorageReturnFormat(checkstate)
        let MOutStorageReturnSub = this.MOutStorageReturnSubFormat()

        data = {
            "action": "outstoragereturnsave",
            "servicetype": "routematerial",
            "express": "6DFD1C9B",
            "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
            "userName": Url.PDAusername,
            "factoryId": Url.PDAFid,
            "data": {
                "MOutStorageReturn": MOutStorageReturn,
                "MOutStorageReturnSub": MOutStorageReturnSub,
                "MOutStorageReturnSub2": "",
                "DbActionType": isCheckPageEdit ? "Update" : "Add",
                "recid": recid
            },
        }
        if (Url.isAppNewUpload) {
            data.action = "outstoragereturnsavenew"
        }
        formData.append('jsonParam', JSON.stringify(data))
        if (!Url.isAppNewUpload) {
            if (imageArr.length > 0) {
                imageArr.map((item, index) => {
                    formData.append('img_' + index, {
                        uri: item,
                        type: 'image/jpeg',
                        name: 'img_' + index
                    })
                })
            }
        }
        console.log("🚀 ~ file: MaterialOutStorageReturn.js ~ line 302 ~ MaterialOutStorageReturn ~ formData", JSON.stringify(formData))
        fetch(Url.PDAurl, {
            method: 'post',
            headers: {
                'content-type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res);
            return res.json();
        }).then(resData => {
            //console.log("🚀 ~ file: MaterialProjectPicking.js ~ line 947 ~ MaterialProjectPicking ~ resdata", resData)
            if (resData.Success == true) {
                this.toast.show('保存成功');
                setTimeout(() => {
                    if (isCheckPageEdit) {
                        this.props.navigation.goBack()
                    } else {
                        this.props.navigation.replace(this.props.navigation.getParam("page"), {
                            title: this.props.navigation.getParam("title"),
                            pageType: this.props.navigation.getParam("pageType"),
                            page: this.props.navigation.getParam("page"),
                            isAppPhotoSetting: this.props.navigation.getParam("isAppPhotoSetting"),
                            isAppDateSetting: this.props.navigation.getParam("isAppDateSetting"),
                            isAppPhotoAlbumSetting: this.props.navigation.getParam("isAppPhotoAlbumSetting"),
                        })
                    }
                }, 1000)
            } else {
                this.toast.close()
                Alert.alert('保存失败', resData.ErrMsg)
                //console.log('resdata', resData)
            }
        }).catch((error) => {
            Alert.alert('保存失败')
            this.toast.close()
            console.log("error --- " + error)
        });
    }

    MOutStorageReturnFormat = (checkstate) => {
        const { rowguid, formNo, returnType, department, departmentId, departCode, room, roomId, shop, shopId, tabulator, remark, totalAmount, tabulationDate } = this.state
        let MOutStorageReturn = {}
        MOutStorageReturn._rowguid = rowguid
        MOutStorageReturn.formCode = formNo
        MOutStorageReturn.projectName = ''
        MOutStorageReturn.projectId = ''
        MOutStorageReturn.teamName = ''
        MOutStorageReturn.teamId = ''
        MOutStorageReturn.returnPerson = ''
        MOutStorageReturn.returnPersonId = ''
        MOutStorageReturn.sumMoney = totalAmount
        MOutStorageReturn.editEmployeeName = tabulator
        MOutStorageReturn.editEmployeeId = Url.PDAEmployeeId
        MOutStorageReturn.editDate = tabulationDate
        MOutStorageReturn.remark = remark
        MOutStorageReturn.isCommit = '0'
        MOutStorageReturn.roomName = room
        MOutStorageReturn.roomId = roomId
        MOutStorageReturn.checkstate = checkstate
        MOutStorageReturn.isClosed = '0'
        MOutStorageReturn.depttype = returnType == '车间' ? '2' : '1'
        MOutStorageReturn.deptname = returnType == '车间' ? '车间' : department
        MOutStorageReturn.deptcode = returnType == '车间' ? shop : departCode
        MOutStorageReturn.deptid = returnType == '车间' ? shopId : departmentId
        MOutStorageReturn.appMsg = checkstate == '0' ? '' : '请审核'
        MOutStorageReturn.ReturnType = '1'
        MOutStorageReturn.FormCode_PB = ''
        MOutStorageReturn.MaterialType = ''
        //MOutStorageReturn.StarDate = ''
        //MOutStorageReturn.EndDate = ''
        MOutStorageReturn.ShareMethod = ''
        //MOutStorageReturn.fileAttach = ''

        return MOutStorageReturn
    }

    MOutStorageReturnSubFormat = () => {
        const { materialInfos, isCheckPageEdit } = this.state
        let MOutStorageReturnSub = []
        materialInfos.map((item, index) => {
            let MOutStorageReturnSubObj = {}
            let rowguid = item.rowguid;
            MOutStorageReturnSubObj._rowguid = isCheckPageEdit && rowguid != null ? item.rowguid : this.getRandom()
            MOutStorageReturnSubObj.mainid = '0'
            MOutStorageReturnSubObj.materialCode = item.code
            MOutStorageReturnSubObj.materialName = item.name
            MOutStorageReturnSubObj.materialSize = item.size
            MOutStorageReturnSubObj.materialUnit = item.unit
            MOutStorageReturnSubObj.materialId = item.materialId
            MOutStorageReturnSubObj.returnNumber = item.outNumber
            MOutStorageReturnSubObj.unitPrice = item.unitPrice
            MOutStorageReturnSubObj.returnMoney = item.price
            MOutStorageReturnSubObj.remark = ''
            //MOutStorageReturnSubObj.state = ''
            MOutStorageReturnSubObj.materialType = item.typeId
            MOutStorageReturnSubObj.materialTypeName = item.type
            MOutStorageReturnSubObj.projectName = item.projectName
            MOutStorageReturnSubObj.projectId = item.projectId
            MOutStorageReturnSubObj.teamName = item.teamName
            MOutStorageReturnSubObj.teamId = item.teamId
            MOutStorageReturnSubObj.reqType = item.reqType
            MOutStorageReturnSubObj.reqType2 = item.reqType2
            MOutStorageReturnSubObj.storageGuid = ''
            MOutStorageReturnSubObj.outStorageSubGuid = ''
            //MOutStorageReturnSubObj.OutStorageNumber = ''
            //MOutStorageReturnSubObj.TheoryNumber = ''
            //MOutStorageReturnSubObj.DiffNumber = ''
            MOutStorageReturnSubObj.CompTypeName = ''
            MOutStorageReturnSubObj.CompTypeId = ''
            MOutStorageReturnSubObj._IsDel = item._IsDel

            MOutStorageReturnSub.push(MOutStorageReturnSubObj)
        })
        return MOutStorageReturnSub
    }

    getRandom = () => {
        var arr = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"],
            num = "";
        for (var i = 0; i < 32; i++) {
            num += arr[parseInt(Math.random() * 36)];
        }
        return num;
    }

    // 时间工具
    _DatePicker = (index) => {
        //const { ServerTime } = this.state
        return (
            <DatePicker
                customStyles={{
                    dateInput: styles.dateInput,
                    dateTouchBody: styles.dateTouchBody,
                    dateText: this.state.isAppDateSetting ? styles.dateText : styles.dateDisabled,
                }}
                iconComponent={<Icon name='caretdown' color={this.state.isAppDateSetting ? '#419fff' : "#666"} iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
                }
                showIcon={true}
                date={this.state.tabulationDate}
                onOpenModal={() => {
                    this.setState({ focusIndex: index })
                }}
                format="YYYY-MM-DD"
                mode="date"
                disabled={!this.state.isAppDateSetting}
                onDateChange={(value) => {
                    this.setState({
                        tabulationDate: value
                    })
                }}
            />
        )
    }

    uploadImage = () => {
        const { pictureSelect, isCheckPage, pageType } = this.state
        return (
            <View style={{ flexDirection: 'row' }}>
                {/*{this.imageView()}*/}
                {/*{*/}
                {/*    isCheckPage ? <View /> :*/}
                {/*        <AvatarAdd*/}
                {/*            pictureSelect={pictureSelect}*/}
                {/*            backgroundColor='#4D8EF5'*/}
                {/*            color='white'*/}
                {/*            title="材"*/}
                {/*            onPress={() => {*/}
                {/*                if (Url.isUsePhotograph == "YES") {*/}
                {/*                    if (pageType == 'CheckPage') {*/}
                {/*                        this.toast.show('照片不可修改')*/}
                {/*                    } else {*/}
                {/*                        this.camandlibFunc()*/}
                {/*                    }*/}
                {/*                } else {*/}
                {/*                    this.cameraFunc()*/}
                {/*                }*/}
                {/*            }} />*/}
                {/*}*/}
                {
                    !isCheckPage ? <Icon name='camera-alt' color='#4D8EF5' iconProps={{ size: 32 }} onPress={() => {
                        if (this.state.isAppPhotoAlbumSetting) {
                            this.camandlibFunc()
                        } else {
                            this.cameraFunc()
                        }
                    }}
                    ></Icon> : <View />

                }
            </View>
        )

    }

    camandlibFunc = () => {
        Alert.alert(
            '提示信息',
            '选择拍照方式',
            [{
                text: '取消',
                style: 'cancel'
            }, {
                text: '拍照',
                onPress: () => {
                    this.cameraFunc()
                }
            }, {
                text: '相册',
                onPress: () => {
                    this.camLibraryFunc()
                }
            }])
    }

    cameraFunc = () => {
        launchCamera(
            {
                mediaType: 'photo',
                includeBase64: false,
                maxHeight: parseInt(Url.isResizePhoto),
                maxWidth: parseInt(Url.isResizePhoto),
                saveToPhotos: true,

            },
            (response) => {
                if (response.uri == undefined) {
                    return
                }
                imageArr.push(response.uri)
                this.setState({
                    pictureSelect: true,
                    pictureUri: response.uri,
                    imageArr: imageArr
                })
                if (Url.isAppNewUpload) {
                    this.cameraPost(response.uri)
                } else {
                    this.setState({
                        pictureSelect: true,
                    })
                }
            },
        )
    }

    camLibraryFunc = () => {
        launchImageLibrary(
            {
                mediaType: 'photo',
                includeBase64: false,
                maxHeight: parseInt(Url.isResizePhoto),
                maxWidth: parseInt(Url.isResizePhoto),
            },
            (response) => {
                if (response.uri == undefined) {
                    return
                }
                imageArr.push(response.uri)
                this.setState({

                    pictureUri: response.uri,
                    imageArr: imageArr
                })
                if (Url.isAppNewUpload) {
                    this.cameraPost(response.uri)
                } else {
                    this.setState({
                        pictureSelect: true,
                    })
                }
            },
        )
    }

     // 展示/隐藏 放大图片
  handleZoomPicture = (flag, index) => {
    this.setState({
      imageOverSize: false,
      currShowImgIndex: index || 0
    })
  }

  // 加载放大图片弹窗
  renderZoomPicture = () => {
    const { imageOverSize, currShowImgIndex, imageFileArr } = this.state
    return (
      <OverlayImgZoomPicker
        isShowImage={imageOverSize}
        currShowImgIndex={currShowImgIndex}
        zoomImages={imageFileArr}
        callBack={(flag) => this.handleZoomPicture(flag)}
      ></OverlayImgZoomPicker>
    )
  }

  cameraPost = (uri) => {
        const { recid } = this.state
        this.setState({ isPhotoUpdate: true })
        this.toast.show('图片上传中', 2000)

        let formData = new FormData();
        let data = {};
        data = {
            "action": "savephote",
            "servicetype": "photo_file",
            "express": "D2979AB2",
            "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
            "data": {
                "fileflag": fileflag,
                "username": Url.PDAusername,
                "recid": recid
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        formData.append('img', {
            uri: uri,
            type: 'image/jpeg',
            name: 'img'
        })
        console.log("🚀 ~ file: concrete_pouring.js ~ line 672 ~ SteelCage ~ formData", formData)

        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            console.log(res.statusText);
            console.log(res);
            return res.json();
        }).then(resData => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 690 ~ SteelCage ~ resData", resData)
            if (resData.status == '100') {
                let fileid = resData.result.fileid;
                let recid = resData.result.recid;

                let tmp = {}
                tmp.fileid = fileid
                tmp.uri = uri
        tmp.url = uri
                imageFileArr.push(tmp)
                this.setState({
                    fileid: fileid,
                    recid: recid,
                    pictureSelect: true,
                    imageFileArr: imageFileArr
                })
                console.log("🚀 ~ file: concrete_pouring.js ~ line 704 ~ SteelCage ~ imageFileArr", imageFileArr)
                this.toast.show('图片上传成功');
                this.setState({
                    isPhotoUpdate: false
                })
            } else {
                Alert.alert('图片上传失败', resData.message)
                this.toast.close(1)
            }
        }).catch((error) => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 692 ~ SteelCage ~ error", error)
        });
    }

    cameraDelete = (item) => {
        let i = imageArr.indexOf(item)

        let formData = new FormData();
        let data = {};
        data = {
            "action": "deletephote",
            "servicetype": "photo_file",
            "express": "D2979AB2",
            "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
            "data": {
                "fileid": imageFileArr[i].fileid,
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: concrete_pouring.js ~ line 733 ~ SteelCage ~ cameraDelete ~ formData", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            console.log(res.statusText);
            console.log(res);
            return res.json();
        }).then(resData => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 745 ~ SteelCage ~ cameraDelete ~ resData", resData)
            if (resData.status == '100') {
                if (i > -1) {
                    imageArr.splice(i, 1)
                    imageFileArr.splice(i, 1)
                    this.setState({
                        imageArr: imageArr,
                        imageFileArr: imageFileArr
                    }, () => {
                        this.forceUpdate()
                    })
                }
                if (imageArr.length == 0) {
                    this.setState({
                        pictureSelect: false
                    })
                }
                this.toast.show('图片删除成功');
            } else {
                Alert.alert('图片删除失败', resData.message)
                this.toast.close(1)
            }
        }).catch((error) => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 761 ~ SteelCage ~ cameraDelete ~ error", error)
        });
    }

    imageView = () => {
        return (
            <View style={{ flexDirection: 'row' }}>
                {
                    this.state.pictureSelect ?
                        this.state.imageArr.map((item, index) => {
                            //if (index == 0) {
                            return (
                                <AvatarImg
                                    item={item}
                                    index={index}
                                    isRounded="false"
                                    marginRight="true"
                                    onPress={() => {
                                        this.setState({
                                            imageOverSize: true,
                                            uriShow: item
                                        })
                                    }}
                                    onLongPress={() => {
                                        Alert.alert(
                                            '提示信息',
                                            '是否删除材料照片',
                                            [{
                                                text: '取消',
                                                style: 'cancel'
                                            }, {
                                                text: '删除',
                                                onPress: () => {
                                                    this.cameraDelete(item)
                                                }
                                            }])
                                    }} />
                            )
                        }) : <View></View>
                }
            </View>
        )
    }

    // 下拉菜单
    downItem = (index) => {
        const { returnType, returnTypeList, shop, shopList, department, departmentList, room, roomList } = this.state
        switch (index) {
            case '2': return (
                // 退库类型
                <View>{(
                    <TouchableCustom onPress={() => { this.isItemVisible(returnTypeList, index) }} >
                        <IconDown text={returnType} />
                    </TouchableCustom>
                )}</View>
            ); break;
            case '3': return (
                // 车间
                <View>{(
                    <TouchableCustom onPress={() => { this.isItemVisible(shopList, index) }} >
                        <IconDown text={shop} />
                    </TouchableCustom>
                )}</View>
            ); break;
            case '4': return (
                // 部门
                <View>{(
                    <TouchableCustom onPress={() => { this.isItemVisible(departmentList, index) }} >
                        <IconDown text={department} />
                    </TouchableCustom>
                )}</View>
            ); break;
            case '5': return (
                // 退回库房
                <View>{(
                    <TouchableCustom onPress={() => { this.isItemVisible(roomList, index) }} >
                        <IconDown text={room} />
                    </TouchableCustom>
                )}</View>
            ); break;
            default: return;
        }
    }

    isItemVisible = (data, focusIndex) => {
        //console.log("************** " + focusIndex + "-" + i)
        if (typeof (data) != "undefined") {
            if (data != [] && data != "") {
                switch (focusIndex) {
                    case '2': this.setState({ returnTypeVisible: true, bottomData: data, focusIndex: focusIndex }); break;
                    case '3': this.setState({ shopVisible: true, bottomData: data, focusIndex: focusIndex }); break;
                    case '4': this.setState({ departmentVisible: true, bottomData: data, focusIndex: focusIndex }); break;
                    case '5': this.setState({ roomVisible: true, bottomData: data, focusIndex: focusIndex }); break;
                    default: return;
                }
            } else {
                this.toast.show("无数据")
            }
        } else {
            this.toast.show("无数据")
        }
    }

    jumpMaterialPickingInfo = (item) => {
        const { title } = this.state
        this.props.navigation.navigate('MaterialPickingInfo', {
            title: '退库信息',
            action: title,
            storageId: item.id,
            storageCode: item.code,
            storageName: item.name,
            size: item.size,
            unit: item.unit,
            unitPrice: item.unitPrice,
            outNumber: item.outNumber || "0",
            canOutNumber: item.canOutNumber,
        })
    }

    render() {
        const { navigation } = this.props;
        //从主页面传url, 未来集成到一起
        const QRurl = navigation.getParam('QRurl') || '';
        const QRid = navigation.getParam('QRid') || ''
        const type = navigation.getParam('type') || '';
        let pageType = navigation.getParam('pageType') || ''

        if (type == 'compid') {
            QRcompid = QRid
        }
        const { title, formNo, isCheckPage, focusIndex, returnType, returnTypeList, returnTypeVisible, totalAmount, bottomData, shop, shopId, shopList, team, tabulationDate, tabulator, supplyUse, remark, materialInfos, department, departmentId, departmentList, shopVisible, departmentVisible, room, roomList, roomVisible } = this.state

        if (this.state.isLoading) {
            return (
                <View style={styles.loading}>
                    <ActivityIndicator
                        animating={true}
                        color='#419FFF'
                        size="large" />
                </View>
            )
            //渲染页面
        } else {
            return (
                <View style={{ flex: 1 }}>
                    <Toast ref={(ref) => { this.toast = ref; }} position="center" />
                    <NavigationEvents
                        onWillFocus={this.UpdateControl}
                        onWillBlur={() => {
                            if (QRid != '') {
                                navigation.setParams({ QRid: '', type: '' })
                            }
                        }} />
                    <ScrollView ref={ref => this.scoll = ref} style={styles.mainView} showsVerticalScrollIndicator={false} >
                        <View style={styles.listView}>
                            <ListItemScan focusStyle={focusIndex == '0' ? styles.focusColor : {}} title='表单编号:' rightElement={formNo} />
                            <ListItemScan focusStyle={focusIndex == '1' ? styles.focusColor : {}} title='编制日期:' rightElement={this._DatePicker(1)} />
                            {
                                isCheckPage ? <ListItemScan focusStyle={focusIndex == '2' ? styles.focusColor : {}} title='退库类型:' rightElement={returnType} /> :
                                    <TouchableCustom underlayColor={'lightgray'} onPress={() => { this.isItemVisible(returnTypeList, '2') }}>
                                        <ListItemScan focusStyle={focusIndex == '2' ? styles.focusColor : {}} title='退库类型:' rightElement={this.downItem('2')} />
                                    </TouchableCustom>
                            }
                            {
                                returnType == '车间' ?
                                    isCheckPage ? <ListItemScan focusStyle={focusIndex == '3' ? styles.focusColor : {}} title={'部门/车间:'} rightElement={shop} /> :
                                        <TouchableCustom underlayColor={'lightgray'} onPress={() => { this.isItemVisible(shopList, '3') }}>
                                            <ListItemScan focusStyle={focusIndex == '3' ? styles.focusColor : {}} title={'部门/车间:'} rightElement={this.downItem('3')} />
                                        </TouchableCustom>
                                    :
                                    isCheckPage ? <ListItemScan focusStyle={focusIndex == '4' ? styles.focusColor : {}} title={'部门/车间:'} rightElement={department} /> :
                                        <TouchableCustom underlayColor={'lightgray'} onPress={() => { this.isItemVisible(departmentList, '4') }}>
                                            <ListItemScan focusStyle={focusIndex == '4' ? styles.focusColor : {}} title={'部门/车间:'} rightElement={this.downItem('4')} />
                                        </TouchableCustom>
                            }
                            {
                                isCheckPage ? <ListItemScan focusStyle={focusIndex == '5' ? styles.focusColor : {}} title='退回库房:' rightElement={room} /> :
                                    <TouchableCustom underlayColor={'lightgray'} onPress={() => { this.isItemVisible(roomList, '5') }}>
                                        <ListItemScan focusStyle={focusIndex == '5' ? styles.focusColor : {}} title='退回库房:' rightElement={this.downItem('5')} />
                                    </TouchableCustom>
                            }

                            {
                                isCheckPage ? <View /> :
                                    //<View style={{ flexDirection: 'row', paddingHorizontal: 10, justifyContent: 'center', paddingVertical: 11 }}>
                                    //    <Button
                                    //        titleStyle={{ fontSize: 14 }}
                                    //        title='选择材料'
                                    //        //iconRight={true}
                                    //        //icon={<Icon type='antdesign' name='scan1' color='white' size={14} />}
                                    //        type='solid'
                                    //        buttonStyle={{ paddingVertical: 6, backgroundColor: '#17BC29', marginVertical: 0 }}
                                    //        onPress={() => {
                                    //            if (returnType == '请选择') {
                                    //                this.toast.show('请选择退库类型')
                                    //                return
                                    //            } else if ((returnType == '车间' && shop == '请选择') || (returnType == '部门' && department == '请选择')) {
                                    //                this.toast.show('请选择车间或部门')
                                    //                return
                                    //            }
                                    //            let deptName = returnType == '车间' ? shop : department
                                    //            let deptId = returnType == '车间' ? shopId : departmentId
                                    //            let materialIds = ''
                                    //            materialInfos.map((item, index) => {
                                    //                materialIds += index == materialInfos.length - 1 ? item.id : item.id + ','
                                    //            })
                                    //            this.props.navigation.navigate('SelectMaterial', {
                                    //                title: '选择材料',
                                    //                action: title,
                                    //                returnType: returnType,
                                    //                materialIds: materialIds,
                                    //                deptName: deptName,
                                    //                deptId: deptId
                                    //            })
                                    //        }}
                                    //    />
                                    //    <Text>   </Text>
                                    //    {
                                    //        returnType == '车间' ?
                                    //            <Button
                                    //                titleStyle={{ fontSize: 14 }}
                                    //                title='砼笼材料'
                                    //                //iconRight={true}
                                    //                //icon={<Icon type='antdesign' name='scan1' color='white' size={14} />}
                                    //                type='solid'
                                    //                buttonStyle={{ paddingVertical: 6, backgroundColor: '#17BC29', marginVertical: 0 }}
                                    //                onPress={() => {
                                    //                    if (returnType == '请选择') {
                                    //                        this.toast.show('请选择退库类型')
                                    //                        return
                                    //                    } else if ((returnType == '车间' && shop == '请选择') || (returnType == '部门' && department == '请选择')) {
                                    //                        this.toast.show('请选择车间或部门')
                                    //                        return
                                    //                    }
                                    //                    let deptName = shop
                                    //                    let deptId = shopId
                                    //                    let materialIds = ''
                                    //                    materialInfos.map((item, index) => {
                                    //                        materialIds += index == materialInfos.length - 1 ? item.id : item.id + ','
                                    //                    })
                                    //                    this.props.navigation.navigate('SelectMaterial', {
                                    //                        title: '选择材料',
                                    //                        action: title + '-砼笼材料',
                                    //                        returnType: returnType,
                                    //                        materialIds: materialIds,
                                    //                        deptName: deptName,
                                    //                        deptId: deptId
                                    //                    })
                                    //                }}
                                    //            />
                                    //            : <View />
                                    //    }
                                    //    <Text>   </Text>
                                    //    <Icon type='material-community' name='qrcode-scan' color='#4D8EF5' style={{ marginTop: 4 }} onPress={() => {
                                    //        if (returnType == '请选择') {
                                    //            this.toast.show('请选择退库类型')
                                    //            return
                                    //        } else if ((returnType == '车间' && shop == '请选择') || (returnType == '部门' && department == '请选择')) {
                                    //            this.toast.show('请选择车间或部门')
                                    //            return
                                    //        }
                                    //        setTimeout(() => {
                                    //            this.setState({ GetError: false, buttondisable: false })
                                    //            varGetError = false
                                    //        }, 1500)
                                    //        this.props.navigation.navigate('QRCode', {
                                    //            type: 'compid',
                                    //            page: 'MaterialOutStorageReturn'
                                    //        })
                                    //    }} />
                                    //</View>

                                    <ListItemScan focusStyle={focusIndex == '15' ? styles.focusColor : {}} title='材料明细:' rightElement={
                                        <View style={{ flexDirection: 'row' }}>

                                            <Button
                                                titleStyle={{ fontSize: 14 }}
                                                title='选择材料'
                                                type='solid'
                                                buttonStyle={{ paddingVertical: 6, backgroundColor: '#18BC28', marginVertical: 0 }}
                                                onPress={() => {
                                                    if (returnType == '请选择') {
                                                        this.toast.show('请选择退库类型')
                                                        return
                                                    } else if ((returnType == '车间' && shop == '请选择') || (returnType == '部门' && department == '请选择')) {
                                                        this.toast.show('请选择车间或部门')
                                                        return
                                                    }
                                                    let deptName = returnType == '车间' ? shop : department
                                                    let deptId = returnType == '车间' ? shopId : departmentId
                                                    let materialIds = ''
                                                    materialInfos.map((item, index) => {
                                                        if (item._IsDel == '0') materialIds += index == materialInfos.length - 1 ? item.id : item.id + ','
                                                    })
                                                    this.props.navigation.navigate('SelectMaterial', {
                                                        title: '选择材料',
                                                        action: title,
                                                        returnType: returnType,
                                                        materialIds: materialIds,
                                                        deptName: deptName,
                                                        deptId: deptId
                                                    })
                                                }}
                                            />
                                            {
                                                returnType == '车间' ? <Text>   </Text> : <View />
                                            }
                                            {
                                                returnType == '车间' ?
                                                    <Button
                                                        titleStyle={{ fontSize: 14 }}
                                                        title='砼笼材料'
                                                        //iconRight={true}
                                                        //icon={<Icon type='antdesign' name='scan1' color='white' size={14} />}
                                                        type='solid'
                                                        buttonStyle={{ paddingVertical: 6, backgroundColor: '#17BC29', marginVertical: 0 }}
                                                        onPress={() => {
                                                            if (returnType == '请选择') {
                                                                this.toast.show('请选择退库类型')
                                                                return
                                                            } else if ((returnType == '车间' && shop == '请选择') || (returnType == '部门' && department == '请选择')) {
                                                                this.toast.show('请选择车间或部门')
                                                                return
                                                            }
                                                            let deptName = shop
                                                            let deptId = shopId
                                                            let materialIds = ''
                                                            materialInfos.map((item, index) => {
                                                                if (item._IsDel == '0') materialIds += index == materialInfos.length - 1 ? item.id : item.id + ','
                                                            })
                                                            this.props.navigation.navigate('SelectMaterial', {
                                                                title: '选择材料',
                                                                action: title + '-砼笼材料',
                                                                returnType: returnType,
                                                                materialIds: materialIds,
                                                                deptName: deptName,
                                                                deptId: deptId
                                                            })
                                                        }}
                                                    />
                                                    : <View />
                                            }
                                            <Text>   </Text>
                                        </View>
                                    } />
                            }

                            {
                                materialInfos.map((item, index) => {
                                    return (
                                        item._IsDel == '1' ? <View /> : <TouchableCustom
                                            onLongPress={() => {
                                                !isCheckPage ?
                                                    Alert.alert(
                                                        '提示信息',
                                                        '是否删除材料信息',
                                                        [{
                                                            text: '取消',
                                                            style: 'cancel'
                                                        }, {
                                                            text: '删除',
                                                            onPress: () => {
                                                                let mis = this.state.materialInfos
                                                                //mis.splice(index, 1)
                                                                item._IsDel = "1"
                                                                let data = (Math.round((Number.parseFloat(item.unitPrice) * Number.parseFloat(item.outNumber)) * 100) / 100).toString()
                                                                let totalAmount = this.state.totalAmount
                                                                totalAmount = (Math.round((Number.parseFloat(totalAmount) - Number.parseFloat(data)) * 100) / 100).toString()

                                                                this.setState({
                                                                    materialInfos: mis,
                                                                    totalAmount: totalAmount || '0'
                                                                })
                                                                this.forceUpdate()
                                                            }
                                                        }]) : ""
                                            }}
                                            onPress={() => { isCheckPage ? <View /> : this.jumpMaterialPickingInfo(item) }}
                                        >
                                            <View>
                                                <Card containerStyle={stylesMeterial.card_containerStyle}>
                                                    <View style={[{
                                                        flexDirection: 'row',
                                                        justifyContent: 'flex-start',
                                                        alignItems: 'center',
                                                    }]}>
                                                        <View style={{ flex: 1 }}>
                                                            <ListItem
                                                                containerStyle={stylesMeterial.list_container_style}
                                                                title={item.code}
                                                                /*rightTitle={
                                                                    <View style={{
                                                                        backgroundColor:
                                                                            item.type == '直接材料' ? '#31C731' :
                                                                                item.type == '小五金' ? '#DAC512' :
                                                                                    item.type == '甲供材料' ? '#4D8EF5' : 'transparent'
                                                                    }}>
                                                                        <Text style={{ color: 'white', fontSize: 13 }}> {item.type} </Text>
                                                                    </View>
                                                                }*/
                                                                titleStyle={stylesMeterial.title}
                                                                rightTitleStyle={stylesMeterial.rightTitle}
                                                            />
                                                            <ListItem
                                                                containerStyle={stylesMeterial.list_container_style}
                                                                title={item.name}
                                                                //rightTitle={'单价:  2200' }
                                                                titleStyle={stylesMeterial.title}
                                                                rightTitleStyle={stylesMeterial.rightTitle}
                                                            />
                                                            <ListItem
                                                                containerStyle={stylesMeterial.list_container_style}
                                                                title={item.size}
                                                                //rightTitle={}
                                                                titleStyle={stylesMeterial.title}
                                                                ightTitleStyle={stylesMeterial.rightTitle}
                                                            />
                                                            <ListItem
                                                                containerStyle={stylesMeterial.list_container_style}
                                                                title={item.outNumber + "(" + item.unit + ")"}
                                                                //rightTitle={'是否暂估:  是'}
                                                                titleStyle={stylesMeterial.title}
                                                                rightTitleStyle={stylesMeterial.rightTitle}
                                                            />
                                                        </View>
                                                        {
                                                            isCheckPage ? <View /> :
                                                                <Icon type='antdesign' name='right' color='#999' onPress={() => { this.jumpMaterialPickingInfo(item) }} />
                                                        }
                                                    </View>
                                                </Card>
                                                <Text />
                                            </View>
                                        </TouchableCustom>
                                    )
                                })
                            }

                            <ListItemScan focusStyle={focusIndex == '8' ? styles.focusColor : {}} title='合计金额:' rightElement={totalAmount} />
                            <ListItemScan focusStyle={focusIndex == '9' ? styles.focusColor : {}} title='制表人:' rightElement={tabulator} />
                            <ListItemScan focusStyle={focusIndex == '10' ? styles.focusColor : {}} title='备注:' rightElement={isCheckPage ? <Text>{remark}</Text> :
                                <View>
                                    <Input
                                        containerStyle={styles.quality_input_container}
                                        inputContainerStyle={styles.inputContainerStyle}
                                        inputStyle={[styles.quality_input_, { top: 7 }]}
                                        placeholder='请填写'
                                        value={remark}
                                        onChangeText={(value) => {
                                            this.setState({
                                                remark: value
                                            })
                                        }} />
                                </View>
                            } />
                            <ListItemScan focusStyle={focusIndex == '11' ? styles.focusColor : {}} title='附件:' rightElement={() =>
                                this.uploadImage()} />
                            {
                                this.state.pictureSelect && this.state.imageArr != [] && this.state.imageArr != "" ?
                                    <ListItemScan focusStyle={focusIndex == '12' ? styles.focusColor : {}} title={this.imageView()} />
                                    : <View />
                            }
                        </View>
                        {
                            isCheckPage ? <Text /> :
                                <View style={{ flexDirection: 'row' }}>
                                    <View style={{ flex: 1 }}><Button
                                        title='暂存'
                                        titleStyle={{ color: "white", fontSize: 17 }}
                                        type='solid'
                                        style={{ marginVertical: 10 }}
                                        containerStyle={{ marginVertical: 10, marginHorizontal: 8 }}
                                        buttonStyle={{ backgroundColor: '#17BC29', borderRadius: 10, paddingVertical: 16 }}
                                        onPress={() => {
                                            this.PostData('0')
                                        }}
                                        disabled={this.state.isPhotoUpdate}
                                    ></Button></View>
                                    <View style={{ flex: 1 }}><Button
                                        title='报审'
                                        titleStyle={{ color: "white", fontSize: 17 }}
                                        type='solid'
                                        style={{ marginVertical: 10 }}
                                        containerStyle={{ marginVertical: 10, marginHorizontal: 8 }}
                                        buttonStyle={{ backgroundColor: '#FF9E00', borderRadius: 10, paddingVertical: 16 }}
                                        onPress={() => {
                                            this.PostData('1')
                                        }}
                                        disabled={this.state.isPhotoUpdate}
                                    ></Button></View>
                                </View>
                        }

                        {/*退库类型底栏*/}
                        <BottomSheet
                            onBackdropPress={() => {
                                this.setState({
                                    returnTypeVisible: false
                                })
                            }}
                            isVisible={returnTypeVisible && !this.state.isCheckPage} onRequestClose={() => {
                                this.setState({
                                    returnTypeVisible: false
                                })
                            }}
                        >
                            <ScrollView style={styles.bottomsheetScroll}>
                                {bottomData.map((item, index) => {
                                    return (
                                        <TouchableCustom
                                            onPress={() => {
                                                this.setState({
                                                    returnType: item,
                                                    returnTypeVisible: false,
                                                    materialInfos: [],
                                                    totalAmount: '0'
                                                })
                                            }}
                                        >
                                            <BottomItem backgroundColor='white' color="#333" title={item} />
                                        </TouchableCustom>
                                    )
                                })}
                            </ScrollView>
                            <TouchableHighlight
                                onPress={() => {
                                    this.setState({ returnTypeVisible: false })
                                }}>
                                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
                            </TouchableHighlight>
                        </BottomSheet>
                        {/*车间底栏*/}
                        <BottomSheet
                            onBackdropPress={() => {
                                this.setState({
                                    shopVisible: false
                                })
                            }}
                            isVisible={shopVisible && !this.state.isCheckPage} onRequestClose={() => {
                                this.setState({
                                    shopVisible: false
                                })
                            }}
                        >
                            <ScrollView style={styles.bottomsheetScroll}>
                                {bottomData.map((item, index) => {
                                    return (
                                        <TouchableCustom
                                            onPress={() => {
                                                this.setState({
                                                    shop: item.name,
                                                    shopId: item.id,
                                                    shopVisible: false,
                                                    materialInfos: [],
                                                    totalAmount: '0'
                                                })
                                            }}
                                        >
                                            <BottomItem backgroundColor='white' color="#333" title={item.name} />
                                        </TouchableCustom>
                                    )
                                })}
                            </ScrollView>
                            <TouchableHighlight
                                onPress={() => {
                                    this.setState({ shopVisible: false })
                                }}>
                                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
                            </TouchableHighlight>
                        </BottomSheet>
                        {/*部门底栏*/}
                        <BottomSheet
                            onBackdropPress={() => {
                                this.setState({
                                    departmentVisible: false
                                })
                            }}
                            isVisible={departmentVisible && !this.state.isCheckPage} onRequestClose={() => {
                                this.setState({
                                    departmentVisible: false
                                })
                            }}
                        >
                            <ScrollView style={styles.bottomsheetScroll}>
                                {bottomData.map((item, index) => {
                                    return (
                                        <TouchableCustom
                                            onPress={() => {
                                                this.setState({
                                                    department: item.name,
                                                    departmentId: item.id,
                                                    departCode: item.code,
                                                    departmentVisible: false,
                                                    materialInfos: [],
                                                    totalAmount: '0'
                                                })
                                            }}
                                        >
                                            <BottomItem backgroundColor='white' color="#333" title={item.name} />
                                        </TouchableCustom>
                                    )
                                })}
                            </ScrollView>
                            <TouchableHighlight
                                onPress={() => {
                                    this.setState({ departmentVisible: false })
                                }}>
                                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
                            </TouchableHighlight>
                        </BottomSheet>
                        {/*退回库房底栏*/}
                        <BottomSheet
                            onBackdropPress={() => {
                                this.setState({
                                    roomVisible: false
                                })
                            }}
                            isVisible={roomVisible && !this.state.isCheckPage} onRequestClose={() => {
                                this.setState({
                                    roomVisible: false
                                })
                            }}
                        >
                            <ScrollView style={styles.bottomsheetScroll}>
                                {bottomData.map((item, index) => {
                                    return (
                                        <TouchableCustom
                                            onPress={() => {
                                                this.setState({
                                                    room: item.name,
                                                    roomId: item.id,
                                                    roomVisible: false,
                                                })
                                            }}
                                        >
                                            <View style={{
                                                paddingVertical: 18,
                                                backgroundColor: 'white',
                                                //alignItems: 'center',
                                                borderBottomColor: '#DDDDDD',
                                                borderBottomWidth: 0.6,
                                                flexDirection: 'row',
                                                justifyContent: 'space-between',
                                                paddingLeft: 20,
                                                paddingRight: 20
                                            }}>
                                                <Text style={{ color: '#333', fontSize: 14 }}>{item.name}</Text>
                                                <Text style={{ color: '#333', fontSize: 14 }}>{item.code}</Text>
                                            </View>
                                        </TouchableCustom>
                                    )
                                })}
                            </ScrollView>
                            <TouchableHighlight
                                onPress={() => {
                                    this.setState({ roomVisible: false })
                                }}>
                                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
                            </TouchableHighlight>
                        </BottomSheet>
                        {/*照片放大*/}
                        {this.renderZoomPicture()}
                        {/* {overlayImg(obj = {
                            onRequestClose: () => {
                                this.setState({ imageOverSize: !this.state.imageOverSize }, () => {
                                    console.log("🚀 ~ file: equipment_maintenance.js:1696 ~ render ~ imageOverSize:", this.state.imageOverSize)
                                })
                            },
                            onPress: () => {
                                this.setState({
                                    imageOverSize: !this.state.imageOverSize
                                })
                            },
                            uri: this.state.uriShow,
                            isVisible: this.state.imageOverSize
                        })
                        } */}
                    </ScrollView>
                </View>
            )
        }
    }
}

const stylesMeterial = StyleSheet.create({
    card_containerStyle: {
        borderRadius: 10,
        shadowOpacity: 0,
        backgroundColor: '#f8f8f8',
        borderWidth: 0,
        paddingVertical: 13,
        elevation: 0
    },
    title: {
        fontSize: 13,
        color: '#999'
    },
    rightTitle: {
        fontSize: 13,
        textAlign: 'right',
        lineHeight: 14,
        flexWrap: 'wrap',
        width: width / 2,
        color: '#333'
    },
    list_container_style: { marginVertical: 0, paddingVertical: 9, backgroundColor: 'transparent' }

})