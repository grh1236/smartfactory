import React from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, Icon, Card, ListItem, BottomSheet, CheckBox, Avatar, Overlay, Header, Input } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';

import { ScrollView } from 'react-native-gesture-handler';
import CardList from "../Componment/CardList";
import DatePicker from 'react-native-datepicker'
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { Image } from 'react-native';
import { NavigationEvents } from 'react-navigation';
import ScanButton from '../Componment/ScanButton';
import { styles } from '../Componment/PDAStyles';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import ListItemScan_child from '../Componment/ListItemScan_child';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import CheckBoxScan from '../Componment/CheckBoxScan';
import AvatarAdd from '../Componment/AvatarAdd';
import AvatarImg from '../Componment/AvatarImg';
import DeviceStorage from '../../Url/DeviceStorage';

import { DeviceEventEmitter, NativeModules } from 'react-native';
import overlayImg from '../Componment/OverlayImg';
import OverlayImgZoomPicker from '../Componment/OverlayImgZoomPicker';
let varGetError = false

const { height, width } = Dimensions.get('window') //获取宽高

let imageArr = [], imageFileArr = [], careIdStr = '', careIdArr = []

let DeviceStorageData = {}

let fileflag = "5"

class BackIcon extends React.PureComponent {
  render() {
    return (
      <TouchableOpacity
        onPress={this.props.onPress} >
        <Icon
          name='arrow-back'
          color='#419FFF' />
      </TouchableOpacity>
    )
  }
}


export default class EquipmentMaintenance extends React.PureComponent {
  constructor() {
    super();
    this.state = {
      resData: [],
      rowguid: '',
      type: 'compid',
      ServerTime: '2000-01-01 00:00',
      //人员
      MaintainManagers: [],
      MainManager: '',
      MainManagerId: '',
      MainManagerselect: '请选择',
      compData: {},
      formCode: '',
      EditEmployeeName: Url.PDAEmployeeName,
      //设备编码
      equipguid: '',
      equipId: '',
      equipName: '',
      equipSize: '',
      workshopName: '',
      productLineName: '',
      equipTypeId: '',
      equipCareCycle: [],
      careCycleId: '',
      careCycle: '',
      careCycleselect: '请选择',
      //保养内容
      careSubData: {},
      equipCareContent: [],
      careSubId: '',
      carecontent: '',
      carestandard: '',
      careIdArr: [],
      careIdStr: '',
      //是否扫码成功
      isGetcomID: false,
      //长按删除功能控制
      iscomIDdelete: false,
      //底栏控制
      bottomData: [],
      bottomVisible: false,
      datepickerVisible: false,
      CameraVisible: false,
      response: '',
      //照片控制
      pictureSelect: false,
      pictureUri: '',
      imageArr: [],
      imageFileArr: [],
      fileid: "",
      recid: "",
      isPhotoUpdate: false,
      imageOverSize: false,
      //
      GetError: false,
      buttondisable: false,
      //卡片
      isEquipVisible: false,
      remark: '',
      focusIndex: '1',
      isCheckPage: false,
      isLoading: true,
      //控制app拍照，日期，相册选择功能
      isAppPhotoSetting: false,
      currShowImgIndex: 0,
      isAppDateSetting: false,
      isAppPhotoAlbumSetting: false
    }
    //this.requestCameraPermission = this.requestCameraPermission.bind(this)
  }

  componentDidMount() {
    const { navigation } = this.props;
    let guid = navigation.getParam('guid') || ''
    let pageType = navigation.getParam('pageType') || ''
    let isAppPhotoSetting = navigation.getParam('isAppPhotoSetting')
    let isAppDateSetting = navigation.getParam('isAppDateSetting')
    let isAppPhotoAlbumSetting = navigation.getParam('isAppPhotoAlbumSetting')
    this.setState({
      isAppPhotoSetting: isAppPhotoSetting,
      isAppDateSetting: isAppDateSetting,
      isAppPhotoAlbumSetting: isAppPhotoAlbumSetting
    })
    if (pageType == 'CheckPage') {
      this.checkResData(guid)
      this.setState({ isCheckPage: true })
    } else {
      this.resData()
    }

    //通过使用DeviceEventEmitter模块来监听事件
    this.iDataScan = DeviceEventEmitter.addListener('iDataScan', (Event) => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 115 ~ SteelCage ~ Event.ScanResult", Event.ScanResult)
      if (typeof Event.ScanResult != undefined) {
        let data = Event.ScanResult
        let arr = data.split("=");
        let id = ''
        id = arr[arr.length - 1]
        console.log("🚀 ~ file: concrete_pouring.js ~ line 118 ~ SteelCage ~ id", id)
        if (this.state.type == 'compid') {
          this.GetcomID(id)
        }

      }
    });
  }

  componentWillUnmount() {
    imageArr = [], imageFileArr = [], careIdStr = '', careIdArr = []
    this.iDataScan.remove()
  }

  UpdateControl = () => {
    if (typeof this.props.navigation.getParam('QRid') != "undefined") {
      if (this.props.navigation.getParam('QRid') != "") {
        this.toast.show(Url.isLoadingView, 0)
      }
    }
    const { navigation } = this.props;
    const { isGetcomID } = this.state
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || ''
    let type = navigation.getParam('type') || '';
    if (type == 'compid') {
      this.GetcomID(QRid)
    }

  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  checkResData = (guid) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getmainapplicationdetail",
      "servicetype": "pdaservice",
      "express": "9C321E1C",
      "ciphertext": "b72efa21a24a6c3cb26e9ab2f819cb55",
      "data": { "_Rowguid": guid }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: equipment_repair.js ~ line 207 ~ EquipmentMaintenance ~ resData", resData)
      let rowguid = resData.result[0]._Rowguid
      let formCode = resData.result[0].FormCode
      let ServerTime = resData.result[0].EditDate
      let equipId = resData.result[0].EquipId
      let equipName = resData.result[0].EquipName
      let equipSize = resData.result[0].EquipSize
      let workshopName = resData.result[0].WorkshopName
      let productLineName = resData.result[0].ProductLineName
      let EditEmployeeName = resData.result[0].EditEmployeeName
      let Remark = resData.result[0].Remark
      let Attachment = ''
      if (resData.result[0].Attachment.length == 0) {
        Attachment = resData.result[0].AttachmentMain
      } else {
        Attachment = resData.result[0].Attachment
      }
      let ReportForRepairName = resData.result[0].ReportForRepairName

      //图片数组
      let imageArr = [], imageFileArr = [];
      let tmp = {}
      tmp.fileid = ""
      tmp.uri = Attachment
      tmp.url = Attachment
      imageFileArr.push(tmp)
      this.setState({
        imageFileArr: imageFileArr
      })
      imageArr.push(Attachment)

      //产品子表
      let compData = {}
      compData.EquipId = equipId
      compData.EquipName = equipName
      compData.EquipSize = equipSize
      compData.WorkshopName = workshopName
      compData.ProductLineName = productLineName

      this.setState({
        resData: resData,
        formCode: formCode,
        rowguid: rowguid,
        ServerTime: ServerTime,
        EditEmployeeName: EditEmployeeName,
        compData: compData,
        remark: Remark,
        equipId: equipId,
        equipName: equipName,
        equipSize: equipSize,
        workshopName: workshopName,
        productLineName: productLineName,
        ReportForRepairName: ReportForRepairName,
        MainManagerselect: ReportForRepairName,
        imageArr: imageArr,
        isEquipVisible: true,
        pictureSelect: true,
        isGetcomID: true
      })
      this.setState({
        isLoading: false,
      })
    }).catch((error) => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 215 ~ QualityInspection ~ error", error)
    });
  }

  resData = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getdatetimeandformcode",
      "servicetype": "pdaservice",
      "express": "8AC4C0B0",
      "ciphertext": "7df83f712027c63f147f6c2d4a43f08d",
      "data": {
        "_bizid": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: equipment_repair.js ~ line 243 ~ EquipmentMaintenance ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: equipment_repair.js ~ line 254 ~ EquipmentMaintenance ~ resData", resData)
      //初始化错误提示
      if (resData.status == 100) {
        let ServerTime = resData.result.EditDate
        let formCode = resData.result.FormCode
        let rowguid = resData.result._Rowguid
        let MaintainManagers = resData.result.MaintainManagers
        this.setState({
          resData: resData,
          formCode: formCode,
          ServerTime: ServerTime,
          rowguid: rowguid,
          MaintainManagers: MaintainManagers,
          MainManagerId: Url.PDAEmployeeId,
          MainManager: Url.PDAEmployeeName,
          MainManagerselect: Url.PDAEmployeeName,
          isLoading: false
        })
        DeviceStorage.get('DeviceStorageDataRepair')
          .then(res => {
            console.log("🚀 ~ file: equipment_repair.js ~ line 277 ~ EquipmentMaintenance ~ res", res)
            if (res) {
              let DeviceStorageDataObj = JSON.parse(res)
              DeviceStorageData = DeviceStorageDataObj
              if (res.length != 0) {
                MaintainManagers.map((item, index) => {
                  if (DeviceStorageData.MainManagerId.indexOf(item.MainManagerId) != -1) {
                    this.setState({
                      "MainManagerId": DeviceStorageDataObj.MainManagerId,
                      "MainManagerselect": DeviceStorageDataObj.MainManagerselect,
                      "MainManager": DeviceStorageDataObj.MainManager,
                    })
                  }
                })
              }
            }
          }).catch(err => {
            console.log("🚀 ~ file: loading.js ~ line 131 ~ SteelCage ~ componentDidMount ~ err", err)
          })
      }
      else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }

    }).catch((error) => {
      console.log("🚀 ~ file: equipment_repair.js ~ line 307 ~ EquipmentMaintenance ~ error", error)
    });

  }

  GetcomID = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getrepairequipinfo",
      "servicetype": "pdaservice",
      "express": "0F51EF23",
      "ciphertext": "a91d6dc5c5a20f2a35939b6e2d91a0ef",
      "data": {
        "equipguid": id,
        "_bizid": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
       this.toast.close()
      if (resData.status == '100') {
        let compData = resData.result
        let equipguid = resData.result.Equipguid
        let equipId = resData.result.EquipId
        let equipName = resData.result.EquipName
        let equipSize = resData.result.EquipSize
        let workshopName = resData.result.WorkshopName
        let productLineName = resData.result.ProductLineName
        this.setState({
          compData: compData,
          equipguid: equipguid,
          equipId: equipId,
          equipName: equipName,
          equipSize: equipSize,
          workshopName: workshopName,
          productLineName: productLineName,
          isEquipVisible: true,
          isGetcomID: true
        })
      } else {
        console.log('resData', resData)
        Alert.alert('ERROR', resData.message)

      }

    }).catch(err => {
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  comIDItem = () => {
    const { isGetcomID, buttondisable, equipId } = this.state
    return (
      <View>
        {
          !isGetcomID ?
            <View>
              <ScanButton
                onPress={() => {
                  this.setState({
                    iscomIDdelete: false,
                    buttondisable: true,
                    focusIndex: '1'
                  })
                  this.props.navigation.navigate('QRCode', {
                    type: 'compid',
                    page: 'EquipmentRepair'
                  })
                }}
                onLongPress={() => {
                  this.setState({
                    type: 'compid',
                    focusIndex: 1
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}
              />
            </View>
            :
            <TouchableOpacity
              onPress={() => {
                console.log('onPress')
              }}
              onLongPress={() => {
                console.log('onLongPress')
                Alert.alert(
                  '提示信息',
                  '是否删除构件信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      this.setState({
                        compData: {},
                        equipguid: '',
                        equipId: '',
                        equipName: '',
                        equipSize: '',
                        workshopName: '',
                        productLineName: '',
                        iscomIDdelete: true,
                        isGetcomID: false,
                        isEquipVisible: false,
                      })
                    }
                  }])
              }} >
              <Text>{equipId}</Text>
            </TouchableOpacity>
        }
      </View>

    )
  }

  GetEquipcarecontent = (id) => {
    let formData = new FormData();
    const { equipguid } = this.state
    let data = {};
    data = {
      "action": "getEquipcarecontent",
      "servicetype": "pda",
      "express": "0F51EF23",
      "ciphertext": "a91d6dc5c5a20f2a35939b6e2d91a0ef",
      "data": {
        "careCycleId": id,
        "factoryId": Url.PDAFid,
        "equipGuid": equipguid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: equipment_maintenance.js ~ line 303 ~ EquipmentMaintenance ~ JSON.stringify(data)", JSON.stringify(data))

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
       this.toast.close()
      if (resData.status == '100') {
        let careSubData = resData.result
        let equipCareContent = resData.result.equipCareContent
        this.setState({
          careSubData: careSubData,
          equipCareContent: equipCareContent,
        })
      } else {
        console.log('resData', resData)
        Alert.alert('ERROR', resData.message)
      }
    }).catch(err => {
    })
  }

  PostData = () => {
    const { ServerTime, MainManager, MainManagerId, rowguid, equipguid, careIdStr, careCycleId, remark, recid } = this.state
    const { navigation } = this.props;
    const QRid = navigation.getParam('QRid') || '';

    if (equipguid == '') {
      this.toast.show('设备编码不能为空');
      return
    }
    if (this.state.isAppPhotoSetting) {
      if (imageArr.length == 0) {
        this.toast.show('请先拍摄检查照片');
        return
      }
    }

    this.toast.show(saveLoading, 0)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "saverepairequipapplication",
      "servicetype": "pdaservice",
      "express": "939B066B",
      "ciphertext": "fee35451796126acd3f799bf037992dc",
      "data": {
        "reportForRepairId": MainManagerId,
        "reportForRepairName": MainManager,
        "equipguid": equipguid,
        "_bizid": Url.PDAFid,
        "editDate": ServerTime,
        "remark": remark,
        "userName": Url.PDAusername,
        "rowguid": rowguid,
        "recid": recid
      }
    }
    if (Url.isAppNewUpload) {
      data.action = "saverepairequipapplicationnew"
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: equipment_repair.js:560 ~ formData:", formData)

    if (!Url.isAppNewUpload) {
      imageArr.map((item, index) => {
        formData.append('img_' + index, {
          uri: item,
          type: 'image/jpeg',
          name: 'img_' + index
        })
      })
    }
    /* formData.append('action', JSON.stringify("SaveProduction"))
    formData.append('servicetype', JSON.stringify('pda'))
    formData.append('express', JSON.stringify('06C795EE'))
    formData.append('ciphertext', JSON.stringify('a1e3818d57d9bfc9437c29e7a558e32e'))
    formData.append('data', JSON.stringify(datatest))
    formData.append('jsonParam', JSON.stringify(data)) */

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('保存成功');
        setTimeout(() => {
          this.props.navigation.replace(this.props.navigation.getParam("page"), {
            title: this.props.navigation.getParam("title"),
            pageType: this.props.navigation.getParam("pageType"),
            page: this.props.navigation.getParam("page"),
            isAppPhotoSetting: this.props.navigation.getParam("isAppPhotoSetting"),
            isAppDateSetting: this.props.navigation.getParam("isAppDateSetting"),
            isAppPhotoAlbumSetting: this.props.navigation.getParam("isAppPhotoAlbumSetting"),
          })
          //this.props.navigation.navigate('PDAMainStack')
        }, 400)
      } else {
        this.toast.close()
        Alert.alert('保存失败', resData.message)
        console.log('resData', resData)
      }
    }).catch((error) => {
      this.toast.show('保存失败')
      if (error.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('保存失败', "响应内容不是合法JSON格式")
        return
      }
      if (error.toString().indexOf('Network request faile') != -1) {
        Alert.alert('保存失败', "网络请求错误")
        return
      }
      Alert.alert('保存失败', error.toString())
    });
  }

  DeleteData = () => {
    const { navigation } = this.props;
    let guid = navigation.getParam('guid') || ''
    this.toast.show('删除中...', 0)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "deletmainapplication",
      "servicetype": "pda",
      "express": "939B066B",
      "ciphertext": "fee35451796126acd3f799bf037992dc",
      "data": {
        "_Rowguid": guid,
      }
    }

    formData.append('jsonParam', JSON.stringify(data))

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('删除成功');
        this.props.navigation.navigate('MainCheckPage')
      } else {
        this.toast.close()
        Alert.alert('保存失败', resData.message)
        console.log('resData', resData)
      }
    }).catch((error) => {
      this.toast.show('删除失败')
    });
  }

  cameraFunc = () => {
    launchCamera(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: parseInt(Url.isResizePhoto),
        maxWidth: parseInt(Url.isResizePhoto),
        saveToPhotos: true,

      },
      (response) => {
        if (response.uri == undefined) {
          return
        }
        imageArr.push(response.uri)
        this.setState({
          //pictureSelect: true,
          pictureUri: response.uri,
          imageArr: imageArr
        })
        if (Url.isAppNewUpload) {
          this.cameraPost(response.uri)
        } else {
          this.setState({
            pictureSelect: true,
          })
        }
        console.log(response)
      },
    )
  }

  camLibraryFunc = () => {
    launchImageLibrary(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: parseInt(Url.isResizePhoto),
        maxWidth: parseInt(Url.isResizePhoto),
      },
      (response) => {
        if (response.uri == undefined) {
          return
        }
        imageArr.push(response.uri)
        this.setState({
          //pictureSelect: true,
          pictureUri: response.uri,
          imageArr: imageArr
        })
        if (Url.isAppNewUpload) {
          this.cameraPost(response.uri)
        } else {
          this.setState({
            pictureSelect: true,
          })
        }
        console.log(response)
      },
    )
  }

  camandlibFunc = () => {
    Alert.alert(
      '提示信息',
      '选择拍照方式',
      [{
        text: '取消',
        style: 'cancel'
      }, {
        text: '拍照',
        onPress: () => {
          this.cameraFunc()
        }
      }, {
        text: '相册',
        onPress: () => {
          this.camLibraryFunc()
        }
      }])
  }

   // 展示/隐藏 放大图片
  handleZoomPicture = (flag, index) => {
    this.setState({
      imageOverSize: false,
      currShowImgIndex: index || 0
    })
  }

  // 加载放大图片弹窗
  renderZoomPicture = () => {
    const { imageOverSize, currShowImgIndex, imageFileArr } = this.state
    return (
      <OverlayImgZoomPicker
        isShowImage={imageOverSize}
        currShowImgIndex={currShowImgIndex}
        zoomImages={imageFileArr}
        callBack={(flag) => this.handleZoomPicture(flag)}
      ></OverlayImgZoomPicker>
    )
  }

  cameraPost = (uri) => {
    const { recid } = this.state
    this.setState({
      isPhotoUpdate: true
    })
    this.toast.show('图片上传中', 2000)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "savephote",
      "servicetype": "photo_file",
      "express": "D2979AB2",
      "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
      "data": {
        "fileflag": fileflag,
        "username": Url.PDAusername,
        "recid": recid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    formData.append('img', {
      uri: uri,
      type: 'image/jpeg',
      name: 'img'
    })
    console.log("🚀 ~ file: concrete_pouring.js ~ line 672 ~ SteelCage ~ formData", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 690 ~ SteelCage ~ resData", resData)
       this.toast.close()
      if (resData.status == '100') {
        let fileid = resData.result.fileid;
        let recid = resData.result.recid;

        let tmp = {}
        tmp.fileid = fileid
        tmp.uri = uri
        tmp.url = uri
        imageFileArr.push(tmp)
        this.setState({
          fileid: fileid,
          recid: recid,
          pictureSelect: true,
          imageFileArr: imageFileArr
        })
        console.log("🚀 ~ file: concrete_pouring.js ~ line 704 ~ SteelCage ~ imageFileArr", imageFileArr)
        this.toast.show('图片上传成功');
        this.setState({
          isPhotoUpdate: false
        })
      } else {
        Alert.alert('图片上传失败', resData.message)
        this.toast.close(1)
      }
    }).catch((error) => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 692 ~ SteelCage ~ error", error)
    });
  }

  cameraDelete = (item) => {
    let i = imageArr.indexOf(item)

    if (Url.isAppNewUpload) {
      let formData = new FormData();
      let data = {};
      data = {
        "action": "deletephote",
        "servicetype": "photo_file",
        "express": "D2979AB2",
        "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
        "data": {
          "fileid": imageFileArr[i].fileid,
        }
      }
      formData.append('jsonParam', JSON.stringify(data))
      fetch(Url.PDAurl, {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        body: formData
      }).then(res => {
        console.log(res.statusText);
        console.log(res);
        return res.json();
      }).then(resData => {
        if (resData.status == '100') {
          if (i > -1) {
            imageArr.splice(i, 1)
            imageFileArr.splice(i, 1)
            this.setState({
              imageArr: imageArr,
              imageFileArr: imageFileArr
            }, () => {
              this.forceUpdate()
            })
          }
          if (imageArr.length == 0) {
            this.setState({
              pictureSelect: false
            })
          }
          this.toast.show('图片删除成功');
        } else {
          Alert.alert('图片删除失败', resData.message)
          this.toast.close(1)
        }
      }).catch((error) => {
        console.log("🚀 ~ file: concrete_pouring.js ~ line 761 ~ SteelCage ~ cameraDelete ~ error", error)
      });
    } else {
      if (i > -1) {
        imageArr.splice(i, 1)
        this.setState({
          imageArr: imageArr,
        }, () => {
          this.forceUpdate()
        })
      }
      if (imageArr.length == 0) {
        this.setState({
          pictureSelect: false
        })
      }
    }
  }

  imageView = () => {
    return (
      <View style={{ flexDirection: 'row' }}>
        {
          this.state.pictureSelect ?
            this.state.imageArr.map((item, index) => {
              //if (index == 0) {
              return (
                <AvatarImg
                  item={item}
                  index={index}
                  onPress={() => {
                    this.setState({
                      imageOverSize: true,
                      pictureUri: item
                    })
                  }}
                  onLongPress={() => {
                    Alert.alert(
                      '提示信息',
                      '是否删除构件照片',
                      [{
                        text: '取消',
                        style: 'cancel'
                      }, {
                        text: '删除',
                        onPress: () => {
                          this.cameraDelete(item)
                        }
                      }])
                  }} />
              )
            }) : <View></View>
        }
      </View>
    )
  }

  isBottomVisible = (data, focusIndex) => {
    if (typeof (data) != "undefined") {
      if (data.length > 0) {
        this.setState({ bottomVisible: true, bottomData: data, focusIndex: focusIndex })
      }
    } else {
      this.toast.show("无数据")
    }
  }

  _DatePicker = () => {
    const { ServerTime } = this.state
    return (
      <DatePicker
        customStyles={{
          dateInput: styles.dateInput,
          dateTouchBody: styles.dateTouchBody,
          dateText: this.state.isAppDateSetting ? styles.dateText : styles.dateDisabled,
        }}
        onOpenModal={() => {
          this.setState({ focusIndex: '3' })
        }}
        iconComponent={<Icon name='caretdown' color={this.state.isAppDateSetting ? '#419fff' : "#666"} iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
        }
        showIcon={true}
        mode='datetime'
        date={ServerTime}
        format="YYYY-MM-DD HH:mm"
        disabled={!this.state.isAppDateSetting}
        onDateChange={(value) => {
          this.setState({
            ServerTime: value
          })
        }}
      />
    )
  }

  render() {
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || ''
    const type = navigation.getParam('type') || '';

    const { buttondisable, MaintainManagers, MainManagerselect, isGetcomID, remark, focusIndex, isEquipVisible, compData, equipCareCycle, careCycleselect, equipCareContent, careCycle, careCycleId, isGetCommonMould, isGetInspector, formCode, bottomVisible, bottomData, isCheckPage } = this.state

    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          <NavigationEvents
            onWillFocus={this.UpdateControl} />
          <ScrollView ref={ref => this.scoll = ref} style={styles.mainView} showsVerticalScrollIndicator={false} >
            <View style={styles.listView}>
              <ListItem
                containerStyle={{ paddingBottom: 6 }}
                leftAvatar={
                  <View >
                    <View style={{ flexDirection: 'column' }} >
                      {
                        isCheckPage ? <View></View> :
                          <AvatarAdd
                            pictureSelect={this.state.pictureSelect}
                            backgroundColor='rgba(77,142,245,0.20)'
                            color='#4D8EF5'
                            title="构"
                            onPress={() => {
                              if (this.state.isAppPhotoAlbumSetting) {
                                this.camandlibFunc()
                              } else {
                                this.cameraFunc()
                              }
                            }} />
                      }
                      {this.imageView()}
                    </View>
                  </View>
                }
              />
              <ListItemScan
                title='表单编号'
                rightTitle={formCode}
              />
              <ListItemScan
                isButton={!isGetcomID}
                focusStyle={focusIndex == '1' ? styles.focusColor : {}}
                title='设备编码'
                onPressIn={() => {
                  this.setState({
                    type: 'compid',
                    focusIndex: 1
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPress={() => {
                  this.setState({
                    type: 'compid',
                    focusIndex: 1
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}
                rightElement={
                  this.comIDItem()
                }
              />
              {
                isEquipVisible ?
                  <EquipCardList EquipData={compData} /> : <View></View>
              }

              <ListItemScan
                focusStyle={focusIndex == '3' ? styles.focusColor : {}}
                title='报修日期'
                rightElement={
                  this._DatePicker()
                }
              //rightTitle={ServerTime}
              />

              <ListItemScan
                title='操作人'
                rightTitle={this.state.EditEmployeeName}
              />

              <TouchableCustom underlayColor={'lightgray'} onPress={() => {
                this.isBottomVisible(MaintainManagers, '6')
              }} >
                <ListItemScan
                  focusStyle={focusIndex == '6' ? styles.focusColor : {}}
                  title='报修人'
                  rightElement={
                    <IconDown text={MainManagerselect} />
                  }
                />
              </TouchableCustom>


              <ListItemScan
                focusStyle={focusIndex == '5' ? styles.focusColor : {}}
                title='问题描述'
                rightElement={
                  <View>
                    <Input
                      containerStyle={styles.quality_input_container}
                      inputContainerStyle={styles.inputContainerStyle}
                      inputStyle={[styles.quality_input_, { top: 7 }]}
                      placeholder='请输入'
                      value={remark}
                      //onFocus={() => { this.setState({ focusIndex: '5' }) }}
                      onChangeText={(value) => {
                        this.setState({
                          remark: value
                        })
                      }} />
                  </View>
                }
              />
            </View>

            {
              this.state.isCheckPage ? <FormButton
                backgroundColor='#EB5D20'
                onPress={this.DeleteData}
                title='删除'
                disabled={this.state.isPhotoUpdate}
              /> :
                <FormButton
                  backgroundColor='#17BC29'
                  onPress={this.PostData}
                  title='保存'
                  disabled={this.state.isPhotoUpdate}
                />
            }



             {this.renderZoomPicture()}

            {/* {overlayImg(obj = {
              onRequestClose: () => {
                this.setState({ imageOverSize: !this.state.imageOverSize }, () => {
                  console.log("🚀 ~ file: equipment_maintenance.js:1696 ~ render ~ imageOverSize:", this.state.imageOverSize)
                })
              },
              onPress: () => {
                this.setState({
                  imageOverSize: !this.state.imageOverSize
                })
              },
              uri: this.state.pictureUri,
              isVisible: this.state.imageOverSize
            })
            }*/}

            <BottomSheet
              isVisible={bottomVisible && !isCheckPage}
              onRequestClose={() => {
                this.setState({
                  bottomVisible: false
                })
              }}
              onBackdropPress={() => {
                this.setState({
                  bottomVisible: false
                })
              }}
            >
              <ScrollView style={styles.bottomsheetScroll}>
                {
                  bottomData.map((item, index) => {
                    let title = ''
                    if (item.MainManagerId) {
                      title = item.MainManager
                    }
                    return (
                      <TouchableOpacity
                        onPress={() => {
                          if (item.MainManagerId) {
                            this.setState({
                              MainManagerId: item.MainManagerId,
                              MainManagerselect: item.MainManager,
                              MainManager: item.MainManager,
                              focusIndex: '6'
                            })
                            DeviceStorageData = {
                              "MainManagerId": item.MainManagerId,
                              "MainManagerselect": item.MainManager,
                              "MainManager": item.MainManager,
                            }

                            DeviceStorage.save('DeviceStorageDataRepair', JSON.stringify(DeviceStorageData))
                          }
                          this.setState({
                            bottomVisible: false
                          })
                        }}
                      >
                        <BottomItem backgroundColor='white' color="#333" title={title} />
                      </TouchableOpacity>
                    )

                  })
                }
              </ScrollView>
              <TouchableCustom
                onPress={() => {
                  this.setState({ bottomVisible: false })
                }}>
                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
              </TouchableCustom>
            </BottomSheet>
          </ScrollView>
        </View>
      )
    }
  }

}

class EquipCardList extends React.Component {
  render() {
    const { EquipData } = this.props
    return (
      <View>
        <Card containerStyle={styles.card_containerStyle}>
          <ListItemScan_child
            containerStyle={styles.list_container_style}
            title='设备名称'
            rightTitle={EquipData.EquipName}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItemScan_child
            containerStyle={styles.list_container_style}
            title='设备型号'
            rightTitle={EquipData.EquipSize}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItemScan_child
            containerStyle={styles.list_container_style}
            title='所属车间'
            rightTitle={EquipData.WorkshopName}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItemScan_child
            containerStyle={styles.list_container_style}
            title='生产线'
            rightTitle={EquipData.ProductLineName}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
        </Card>
      </View>
    )
  }
}

