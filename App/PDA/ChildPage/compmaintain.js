import React, { Component } from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, Icon, Card, ListItem, BottomSheet, CheckBox, Avatar, Overlay, Header, Input } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { deviceHeight, deviceWidth, RFT } from '../../Url/Pixal';
import { ScrollView } from 'react-native-gesture-handler';
import CardList from "../Componment/CardListXMCX";
import DatePicker from 'react-native-datepicker'
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { Image } from 'react-native';
import { NavigationEvents } from 'react-navigation';
import ScanButton from '../Componment/ScanButton';
import { styles } from '../Componment/PDAStyles';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import ListItemScan_child from '../Componment/ListItemScan_child';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import CheckBoxScan from '../Componment/CheckBoxScan';
import AvatarAdd from '../Componment/AvatarAdd';
import AvatarImg from '../Componment/AvatarImg';
import CameraButton from '../Componment/CameraButton';
import DeviceStorage from '../../Url/DeviceStorage';

import { DeviceEventEmitter, NativeModules } from 'react-native';
import overlayImg from '../Componment/OverlayImg';
import OverlayImgZoomPicker from '../Componment/OverlayImgZoomPicker';

let imageArr = [], pictureSelectArr = [];

let pageType = ''

class BackIcon extends React.PureComponent {
  render() {
    return (
      <TouchableOpacity
        onPress={this.props.onPress} >
        <Icon
          name='arrow-back'
          color='#419FFF' />
      </TouchableOpacity>
    )
  }
}


export default class compmaintain extends Component {
  constructor(props) {
    super(props);
    this.state = {
      resData: [],
      formCode: '',
      type: 'compid',
      compData: {},
      compId: '',
      compCode: '',
      projectId: '',
      projectName: '',
      applyDate: '',
      applyEmployeeName: '',
      problemDescription: '',
      maintainDate: '',
      maintainResult: '',
      //照片
      imageArr: [],
      pictureSelect: false,
      pictureSelectArr: [],
      imageOverSize: false,
      pictureUri: '',
      isLoading: true,
      focusIndex: '',
      bottomData: [],
      bottomVisible: false,
      isGetcomID: false,
      isCheckPage: false,
      focusIndex: 0
    };
  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  componentDidMount() {
    const { navigation } = this.props;
    guid = navigation.getParam('guid') || ''
    pageType = navigation.getParam('pageType') || ''
    let projectId = navigation.getParam('projectId') || ''
    if (pageType == 'CheckPage') {
      this.checkResData(guid, projectId)
      this.setState({ isCheckPage: true })
    } else {
      this.resData()
    }

    //通过使用DeviceEventEmitter模块来监听事件
    this.iDataScan = DeviceEventEmitter.addListener('iDataScan', (Event) => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 115 ~ SteelCage ~ Event.ScanResult", Event.ScanResult)
      if (typeof Event.ScanResult != undefined) {
        let data = Event.ScanResult
        let arr = data.split("=");
        let id = ''
        id = arr[arr.length - 1]
        console.log("🚀 ~ file: concrete_pouring.js ~ line 118 ~ SteelCage ~ id", id)
        if (this.state.type == 'compid') {
          this.GetcomID(id)
        }

      }
    });
  }

  componentWillUnmount() {
    imageArr = [], pictureSelectArr = [];
    pageType = ''
    this.iDataScan.remove()
  }

  UpdateControl = () => {
    if (typeof this.props.navigation.getParam('QRid') != "undefined") {
      if (this.props.navigation.getParam('QRid') != "") {
        this.toast.show(Url.isLoadingView, 0)
      }
    }
    const { navigation } = this.props;
    const QRid = navigation.getParam('QRid') || ''
    let type = navigation.getParam('type') || '';

    navigation.setParams({ QRid: '', type: '' })
    if (type == 'compid') {
      this.GetcomID(QRid)
    }
  }

  checkResData = (guid, projectId) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "initcompmaintainqueryinfo",
      "servicetype": "projectsystem",
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        "factoryId": Url.PDAFid,
        "projectId": projectId,
        "rowGuid": guid,
        "type": "choose"
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: main_comprepair.js ~ line 110 ~ comprepair ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: main_comprepair.js ~ line 122 ~ comprepair ~ resData", resData)
      if (resData.status == 100) {
        let resultcheck = resData.result.main
        let formCode = resultcheck.formCode
        let compId = resultcheck.compId
        let compCode = resultcheck.compCode
        let maintainImg = resultcheck.maintainImg
        let maintainResult = resultcheck.maintainResult
        let maintainDate = resultcheck.maintainDate
        let projectName = resultcheck.projectName
        let compTypeName = resultcheck.compTypeName
        let designType = resultcheck.designType
        let diffSourceType = resultcheck.diffSourceType
        let floorNoName = resultcheck.floorNoName
        let floorName = resultcheck.floorName
        let volume = resultcheck.compVolume
        let weight = resultcheck.compWeight

        let tmp = {}
        tmp.name = compId
        tmp.value = maintainImg
        imageArr.push(tmp)

        let compData = {}, result = {}
        result.projectName = projectName
        result.compTypeName = compTypeName
        result.designType = designType
        result.diffSourceType = diffSourceType
        result.floorNoName = floorNoName
        result.floorName = floorName
        result.volume = volume
        result.weight = weight
        compData.result = result

        this.setState({
          resData: resData,
          formCode: formCode,
          compId: compId,
          compCode: compCode,
          maintainResult: maintainResult,
          maintainDate: maintainDate,
          compData: compData,
          formCode: formCode,
          imageArr: imageArr,
          pictureSelect: true,
          isGetcomID: true,
        })
        this.setState({
          isLoading: false,
        })
      } else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }


      this.setState({
        isLoading: false,
      })
    }).catch((error) => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 215 ~ QualityInspection ~ error", error)
    });
  }

  // 展示/隐藏 放大图片
  handleZoomPicture = (flag, index) => {
    this.setState({
      imageOverSize: false,
      currShowImgIndex: index || 0
    })
  }

  // 加载放大图片弹窗
  renderZoomPicture = () => {
    const { imageOverSize, currShowImgIndex, imageFileArr } = this.state
    return (
      <OverlayImgZoomPicker
        isShowImage={imageOverSize}
        currShowImgIndex={currShowImgIndex}
        zoomImages={imageFileArr}
        callBack={(flag) => this.handleZoomPicture(flag)}
      ></OverlayImgZoomPicker>
    )
  }

  resData = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "initcompmaintaininfo",
      "servicetype": "projectsystem",
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        "factoryId": Url.PDAFid,
        "rowGuid": "",
        "type": "maintain",
        "userName": Url.PDAusername
      }
    }

    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: main_comprepair.js ~ line 89 ~ comprepair ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: miain_compreceive.js ~ line 77 ~ miain_compreceive ~ resData", resData)
      let result = resData.result
      //初始化错误提示
      if (resData.status == 100) {
        let formCode = result.formCode

        this.setState({
          resData: resData,
          formCode: formCode,
          isLoading: false
        })
      }
      else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }

    }).catch((error) => {
    });

  }

  PostData = () => {
    const { resData, compData, compId, imageArr, applyDate, maintainResult, formCode } = this.state
    console.log("🚀 ~ file: main_comprepair.js ~ line 251 ~ comprepair ~ compData", compData)
    console.log("🚀 ~ file: main_comprepair.js ~ line 251 ~ comprepair ~ resData", resData)

    if (compId == '') {
      this.toast.show('未扫描构件')
      return
    }
    if (imageArr.length == 0) {
      this.toast.show('未拍摄构件照片')
      return
    }
    if (maintainResult.length == 0) {
      this.toast.show('未填写维修记录')
      return
    }

    let mainData = resData.result
    let _compData = compData.result

    mainData.projectId = _compData.projectId
    mainData.projectName = _compData.projectName

    mainData.formCode = formCode

    mainData.compId = _compData.compId
    mainData.designType = _compData.designType
    mainData.diffSourceType = _compData.diffSourceType
    mainData.versionName = _compData.versionName
    mainData.compVolume = _compData.compVolume
    mainData.floorNoId = _compData.floorNoId
    mainData.floorNoName = _compData.floorNoName
    mainData.floorId = _compData.floorId
    mainData.floorName = _compData.floorName
    mainData.buildingUnit = _compData.buildingUnit
    mainData.compTypeId = _compData.compTypeId
    mainData.compTypeName = _compData.compTypeName
    mainData.compCode = _compData.compCode
    mainData.compAutoCode = _compData.compAutoCode
    mainData.maintainState = "维修完成"
    mainData.maintainResult = maintainResult
    mainData.floorNoName = _compData.floorNoName
    mainData._bizid = Url.PDAFid


    let formData = new FormData();
    let data = {};

    data = {
      "action": "savecompmaintain",
      "servicetype": "projectsystem",
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        "type": "maintain",
        "userName": Url.username,
        "main": mainData
      }
    }

    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: main_comprepair.js ~ line 295 ~ comprepair ~ formData", formData)
    imageArr.map((item, index) => {
      formData.append(item.name, {
        uri: item.value,
        type: 'image/jpeg',
        name: item.name
      })
    })

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('保存成功');
        this.props.navigation.goBack()
      } else {
        this.toast.show('保存失败')
        Alert.alert('保存失败', resData.message)
        console.log('resData', resData)
      }
    }).catch((error) => {
      this.toast.show('保存失败')
      if (error.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('保存失败', "响应内容不是合法JSON格式")
        return
      }
      if (error.toString().indexOf('Network request faile') != -1) {
        Alert.alert('保存失败', "网络请求错误")
        return
      }
      Alert.alert('保存失败', error.toString())
    });

  }

  DeleteData = () => {

    this.toast.show('删除中', 10000);

    let formData = new FormData();
    let data = {};
    data = {
      "action": "deletecompmaintain",
      "servicetype": "projectsystem",
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        "rowGuid": guid
      }
    }

    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: main_compreceive.js ~ line 358 ~ miain_compreceive ~ DeleteData ~ formData", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('删除成功');
        this.props.navigation.goBack()
      } else {
        Alert.alert('删除失败', resData.message)
      }
    }).catch((error) => {
    });
  }

  GetcomID = (id) => {

    console.log("🚀 ~ file: main_comprepair.js ~ line 351 ~ comprepair ~ id", id)
    let formData = new FormData();
    let data = {};
    data = {
      "action": "initscancompinfo",
      "servicetype": "projectsystem",
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        "factoryId": Url.PDAFid,
        //"projectId": "2F321804475D4BBCAB5D7605433718A7",
        "compId": id
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: main_comprepair.js ~ line 356 ~ comprepair ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: main_comprepair.js ~ line 377 ~ comprepair ~ resData", resData)
      this.toast.close()
      if (resData.status == '100') {
        let formCode = resData.result.formCode
        let comp = resData.result.comp
        let compCode = comp.compCode
        let compId = comp.compId
        let projectId = comp.projectId
        let projectName = comp.projectName
        let compData = {}
        compData.result = comp
        this.setState({
          compData: compData,
          compCode: compCode,
          compId: compId,
          formCode: formCode,
          projectId: projectId,
          projectName: projectName,
          isGetcomID: true,
        })
      } else {
        console.log('resData', resData)
        Alert.alert(resData.message)
        Alert.alert('错误', resData.message)
        this.setState({
          compId: ""
        })
      }

    }).catch(err => {
      console.log("🚀 ~ file: compmaintain.js ~ line 437 ~ compmaintain ~ err", err)
    })
  }

  comIDItem = (index) => {
    const { isGetcomID, compCode, compId } = this.state
    return (
      <View>
        {
          !isGetcomID ?
            <View style={{ flexDirection: 'row' }}>
              <View>
                <Input
                  ref={ref => { this.input1 = ref }}
                  containerStyle={styles.scan_input_container}
                  inputContainerStyle={styles.scan_inputContainerStyle}
                  inputStyle={[styles.scan_input]}
                  placeholder='请输入'
                  keyboardType='numeric'
                  value={compId}
                  onFocus={() => {
                    this.setState({
                      focusIndex: 0,
                      type: 'compid',
                    })
                  }}
                  onChangeText={(value) => {
                    value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
                    value = value.replace(/^\./g, ""); //验证第一个字符是数字
                    value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
                    //value = value.replace(/\b(0+)/gi, ""); //清楚开头的0
                    this.setState({
                      compId: value
                    })
                  }}
                  //onSubmitEditing={() => { this.input1.focus() }}
                  //onFocus={() => { this.setState({ focusIndex: '5' }) }}
                  /*  */
                  //returnKeyType = 'previous'
                  onSubmitEditing={() => {
                    let inputComId = this.state.compId
                    console.log("🚀 ~ file: qualityinspection.js ~ line 468 ~ QualityInspection ~ inputComId", inputComId)
                    inputComId = inputComId.replace(/\b(0+)/gi, "")
                    this.GetcomID(inputComId)
                  }}
                />
              </View>
              <ScanButton
                onPress={() => {
                  this.setState({
                    focusIndex: 0
                  })
                  this.props.navigation.navigate('QRCode', {
                    type: 'compid',
                    page: 'CompMaintain'
                  })
                }}
                onLongPress={() => {
                  this.setState({
                    type: 'compid',
                    focusIndex: 0
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}
              />
            </View>
            :
            <TouchableCustom
              onPress={() => {
                console.log('onPress')
              }}
              onLongPress={() => {
                console.log('onLongPress')
                Alert.alert(
                  '提示信息',
                  '是否删除构件信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      this.setState({
                        compData: [],
                        compCode: '',
                        compId: '',
                        iscomIDdelete: true,
                        isGetcomID: false,
                      })
                    }
                  }])
              }} >
              <Text>{compCode}</Text>
            </TouchableCustom>
        }
      </View>

    )
  }

  _DatePicker = () => {
    const { maintainDate } = this.state
    return (
      <DatePicker
        customStyles={{
          dateInput: styles.dateInput,
          dateTouchBody: styles.dateTouchBody,
          dateText: styles.dateText,
        }}
        iconComponent={<Icon name='caretdown' color='#419fff' iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
        }
        showIcon={true}
        mode='datetime'
        date={maintainDate}
        format="YYYY-MM-DD HH:mm"
        onOpenModal={() => {
          this.setState({
            focusIndex: '4'
          })
        }}
        onDateChange={(value) => {
          this.setState({
            maintainDate: value
          })
        }}
      />
    )
  }

  isBottomVisible = (data, focusIndex) => {
    if (typeof (data) != "undefined") {
      if (data.length > 0) {
        this.setState({ bottomVisible: true, bottomData: data, focusIndex: focusIndex })
      }
    } else {
      this.toast.show("无数据")
    }
  }

  cameraFunc = (compId) => {
    launchCamera(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: parseInt(Url.isResizePhoto),
        maxWidth: parseInt(Url.isResizePhoto),
        saveToPhotos: true,

      },
      (response) => {
        if (response.uri == undefined) {
          return
        }
        let tmp = {}
        tmp.name = compId
        tmp.value = response.uri
        imageArr.push(tmp)
        this.setState({
          pictureSelect: true,
          imageArr: imageArr
        })
        pictureSelectArr.push(compId)
        this.setState({
          pictureSelectArr: pictureSelectArr
        })
        console.log(response)
      },
    )
  }

  camLibraryFunc = (compId) => {
    launchImageLibrary(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: parseInt(Url.isResizePhoto),
        maxWidth: parseInt(Url.isResizePhoto),
      },
      (response) => {
        if (response.uri == undefined) {
          return
        }
        let tmp = {}
        tmp.name = compId
        tmp.value = response.uri
        imageArr.push(tmp)
        this.setState({
          pictureSelect: true,
          imageArr: imageArr
        })
        pictureSelectArr.push(compId)
        this.setState({
          pictureSelectArr: pictureSelectArr
        })
        console.log(response)
      },
    )
  }

  camandlibFunc = (compId) => {
    Alert.alert(
      '提示信息',
      '选择拍照方式',
      [{
        text: '取消',
        style: 'cancel'
      }, {
        text: '拍照',
        onPress: () => {
          this.cameraFunc(compId)
        }
      }, {
        text: '相册',
        onPress: () => {
          this.camLibraryFunc(compId)
        }
      }])
  }

  imageView = (compId) => {
    return (
      <View style={{ flexDirection: 'row' }}>
        {
          // this.state.pictureSelect ?
          this.state.imageArr.map((item, index) => {
            if (item.name == compId) {
              return (
                <AvatarImg
                  item={item.value}
                  index={index}
                  onPress={() => {
                    this.setState({
                      imageOverSize: true,
                      pictureUri: item.value
                    })
                  }}
                  onLongPress={() => {
                    Alert.alert(
                      '提示信息',
                      '是否删除构件照片',
                      [{
                        text: '取消',
                        style: 'cancel'
                      }, {
                        text: '删除',
                        onPress: () => {


                          console.log("🚀 ~ file: miain_compreceive.js ~ line 375 ~ miain_compreceive ~ this.state.imageArr.map ~ imageArr", imageArr)
                          let p = pictureSelectArr.indexOf(item.value)
                          imageArr.splice(p, 1)
                          pictureSelectArr.splice(p, 1)
                          console.log("🚀 ~ file: miain_compreceive.js ~ line 375 ~ miain_compreceive ~ this.state.imageArr.map ~ pictureSelectArr", pictureSelectArr)
                          this.setState({
                            imageArr: imageArr,
                            pictureSelectArr: pictureSelectArr
                          }, () => {
                            this.forceUpdate()
                          })
                          // if (i > -1) {

                          // }
                          if (imageArr.length == 0) {
                            this.setState({
                              pictureSelect: false
                            })
                          }
                          console.log("🚀 ~ file: qualityinspection.js ~ line 639 ~ SteelCage ~ this.state.imageArr.map ~ imageArr", imageArr)
                        }
                      }])
                  }} />
              )
            }
            //if (index == 0) {

          })
          // : <View></View>
        }
      </View>
    )
  }

  cardListContont = comp => {
    if (visibleArr.indexOf(comp.compCode) == -1) {
      visibleArr.push(comp.compCode);
      this.setState({ visibleArr: visibleArr });
    } else {
      if (visibleArr.length == 1) {
        visibleArr.splice(0, 1);
        this.setState({ visibleArr: visibleArr }, () => {
          this.forceUpdate;
        });
      } else {
        visibleArr.map((item, index) => {
          if (item == comp.compCode) {
            visibleArr.splice(index, 1);
            this.setState({ visibleArr: visibleArr }, () => {
              this.forceUpdate;
            });
          }
        });
      }
    }
  };

  CardList = (compData) => {

    return (
      compData.map((item, index) => {
        let comp = item
        console.log("🚀 ~ file: main_compreceive.js ~ line 463 ~ miain_compreceive ~ compData.map ~ comp", comp)
        return (
          <View style={styles.CardList_main}>
            <TouchableCustom
              onPress={() => {
                this.cardListContont(comp);
              }}
              onLongPress={() => {
                Alert.alert(
                  '提示信息',
                  '是否删除构件信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      compDataArr.splice(index, 1)
                      compIdArr.splice(index, 1)
                      subsSelect.splice(index, 1)
                      tmpstr = compIdArr.toString()
                      console.log("🚀 ~ file: return_goods.js ~ line 559 ~ ReturnGoods ~ compData.map ~ tmpstr", tmpstr)
                      this.setState({
                        compDataArr: compDataArr,
                        compIdArr: compIdArr,
                        subsSelect: subsSelect
                      })
                    }
                  }])
              }}>
              <View style={styles.CardList_title_main}>
                <View style={styles.title_num}>
                  <Text >{index + 1}</Text>
                </View>
                <View style={styles.title_title}>
                  <Text >{item.productCode}</Text>
                </View>
                <View style={styles.CardList_at_icon}>
                  <IconDown />
                </View>
              </View>
            </TouchableCustom>

            {
              visibleArr.indexOf(item.compCode) != -1 ?
                <View key={index} style={{ backgroundColor: '#F9F9F9' }} >
                  <ListItemScan_child
                    title='产品编号'
                    rightTitle={comp.compCode}
                  />
                  <ListItemScan_child
                    title='构件类型'
                    rightTitle={comp.compTypeName}
                  />
                  <ListItemScan_child
                    title='设计型号'
                    rightTitle={comp.designType}
                  />
                  <ListItemScan_child
                    title='楼号'
                    rightTitle={comp.floorNoName}
                  />
                  <ListItemScan_child
                    title='层号'
                    rightTitle={comp.floorName}
                  />
                  <ListItemScan_child
                    title='砼方量'
                    rightTitle={comp.compVolume}
                  />
                  <ListItemScan_child
                    title='重量'
                    rightTitle={comp.compWeight}
                  />
                  <ListItemScan_child
                    title='拍照'
                    rightElement={
                      pictureSelectArr.indexOf(item.compId) == -1 ?
                        <CameraButton
                          onPress={() => {
                            this.camandlibFunc(item.compId)
                          }}
                        /> : this.imageView(item.compId)
                    }
                  />
                  <ListItemScan_child
                    title='问题描述'
                    rightElement={
                      <Input
                        containerStyle={[styles.quality_input_container, { top: 2 }]}
                        inputContainerStyle={styles.inputContainerStyle}
                        inputStyle={[styles.quality_input_]}
                        placeholder='请输入'
                        value={comp.problemDescription}
                        onFocus={() => { this.setState({ focusIndex: 6 }) }}
                        onChangeText={(value) => {
                          let tmp = {}
                          tmp.compId = comp.compId
                          tmp.descript = value

                        }} />
                    }
                  />
                </View> : <View></View>
            }

          </View>
        )
      })
    )
  }

  render() {
    const { formCode, compData, compId, compCode, problemDescription, remark, isGetcomID, isCheckPage, focusIndex, bottomData, bottomVisible, pictureSelect, maintainResult } = this.state

    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          <NavigationEvents
            onWillFocus={this.UpdateControl}
          />
          <ScrollView ref={ref => this.scoll = ref} style={styles.mainView} showsVerticalScrollIndicator={false} >
            <View style={styles.listView}>
              <ListItemScan
                title='维修编号'
                rightTitle={formCode}
              />
              <ListItemScan
                isButton={!isGetcomID}
                focusStyle={focusIndex == '0' ? styles.focusColor : {}}
                title='产品编号'
                onPressIn={() => {
                  this.setState({
                    type: 'compid',
                    focusIndex: 0
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPress={() => {
                  this.setState({
                    type: 'compid',
                    focusIndex: 0
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}
                rightElement={
                  this.comIDItem()
                }
              />

              {
                isGetcomID ? <CardList compData={compData} /> : <View></View>
              }

              <ListItemScan
                focusStyle={focusIndex == '1' ? styles.focusColor : {}}
                isButton={true}
                title='问题照片'
                rightElement={
                  !pictureSelect ?
                    <CameraButton
                      onPress={() => {
                        this.setState({ focusIndex: "1" })
                        this.camandlibFunc(compId)
                      }}
                    /> : this.imageView(compId)
                }
              />

              <ListItemScan
                title='维修记录'
                rightElement={
                  <Input
                    containerStyle={[styles.quality_input_container, { top: 2 }]}
                    inputContainerStyle={styles.inputContainerStyle}
                    inputStyle={[styles.quality_input_]}
                    placeholder='请输入'
                    value={maintainResult}
                    onFocus={() => { this.setState({ focusIndex: "2" }) }}
                    onChangeText={(value) => {
                      this.setState({
                        maintainResult: value
                      })
                    }} />
                }
              />

              <ListItemScan
                focusStyle={focusIndex == '3' ? styles.focusColor : {}}
                title='维修日期'
                rightTitle={this._DatePicker()}
                bottomDivider
              />
              <ListItemScan
                title='维修人'
                rightTitleStyle={{ width: deviceWidth / 1.5, textAlign: 'right', fontSize: 15 }}
                rightTitle={Url.employeeName}
              />

            </View>

            <FormButton
              onPress={isCheckPage ? this.DeleteData : this.PostData}
              title={isCheckPage ? '删除' : '保存'}
              backgroundColor={isCheckPage ? '#EB5D20' : '#17BC29'}
            />
          </ScrollView>


          {overlayImg(obj = {
            onRequestClose: () => {
              this.setState({ imageOverSize: !this.state.imageOverSize }, () => {
                console.log("🚀 ~ file: equipment_maintenance.js:1696 ~ render ~ imageOverSize:", this.state.imageOverSize)
              })
            },
            onPress: () => {
              this.setState({
                imageOverSize: !this.state.imageOverSize
              })
            },
            uri: this.state.pictureUri,
            isVisible: this.state.imageOverSize
          })
          }

          <BottomSheet
            isVisible={bottomVisible && !this.state.isCheckPage}
            onRequestClose={() => {
              this.setState({
                bottomVisible: false
              })
            }}
            onBackdropPress={() => {
              this.setState({
                bottomVisible: false
              })
            }}
          >
            <ScrollView style={styles.bottomsheetScroll}>
              {
                bottomData.map((item, index) => {
                  let title = ''
                  title = item.productCode
                  return (
                    <TouchableCustom
                      onPress={() => {
                        let compData = item
                        let compId = item.compId
                        if (compDataArr.length == 0) {
                          subsSelect.push(item)
                          compDataArr.push(item)
                          compIdArr.push(item.compId)
                          this.setState({
                            subsSelect: subsSelect,
                            compDataArr: compDataArr,
                            compIdArr: compIdArr,
                            productCode: item.productCode
                          })
                          tmpstr = tmpstr + compId + ','
                        } else {
                          if (tmpstr.indexOf(compId) == -1) {
                            subsSelect.push(item)
                            compDataArr.push(item)
                            compIdArr.push(item.compId)
                            this.setState({
                              subsSelect: subsSelect,
                              compDataArr: compDataArr,
                              compIdArr: compIdArr,
                              productCode: item.productCode
                            })
                            tmpstr = tmpstr + compId + ','
                          } else {
                            this.toast.show('已经t添加过此构件')
                          }
                        }
                        confirmNum = compDataArr.length
                        confirmVol += item.compVolume
                        noConfirmNum = deliveryNum - confirmNum
                        noConfirmVol = deliveryVol - confirmVol
                        console.log("🚀 ~ file: miain_compreceive.js ~ line 461 ~ miain_compreceive ~ bottomData.map ~ deliveryNum", deliveryNum)
                        console.log("🚀 ~ file: miain_compreceive.js ~ line 461 ~ miain_compreceive ~ bottomData.map ~ confirmNum", confirmNum)
                        console.log("🚀 ~ file: miain_compreceive.js ~ line 461 ~ miain_compreceive ~ bottomData.map ~ noConfirmNum", noConfirmNum)

                        this.setState({
                          confirmNum: confirmNum,
                          confirmVol: confirmVol,
                          noConfirmNum: noConfirmNum,
                          noConfirmVol: noConfirmVol
                        }, () => {
                          this.forceUpdate()

                        })

                        this.setState({
                          bottomVisible: false
                        })
                      }}
                    >
                      <BottomItem backgroundColor='white' color="#333" title={title} />
                    </TouchableCustom>
                  )

                })
              }
            </ScrollView>

            <TouchableCustom
              onPress={() => {
                this.setState({ bottomVisible: false })
              }}>
              <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
            </TouchableCustom>
          </BottomSheet>
        </View>
      );
    }

  }
}


