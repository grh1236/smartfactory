import React from 'react';
import { View, StyleSheet, Dimensions, ActivityIndicator, Alert, Image } from 'react-native';
import { Button, Text, Input, Icon, Card, ListItem, BottomSheet, Overlay, Header } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { ScrollView } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker'
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { NavigationEvents } from 'react-navigation';
import { styles } from '../Componment/PDAStyles';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import CheckBoxScan from '../Componment/CheckBoxScan';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import AvatarImg from '../Componment/AvatarImg';
import ListItemScan_child from '../Componment/ListItemScan_child';
import ScanButton from '../Componment/ScanButton';
import AvatarAdd from '../Componment/AvatarAdd';

import { DeviceEventEmitter, NativeModules } from 'react-native';
import { deviceWidth } from '../../Url/Pixal';
import overlayImg from '../Componment/OverlayImg';
import OverlayImgZoomPicker from '../Componment/OverlayImgZoomPicker';
let letGetError = false

let DeviceStorageData = {} //缓存数据

const { height, width } = Dimensions.get('window') //获取宽高

let fileflag = "21"

let imageArr = [], imageFileArr = []; //照片数组

class BackIcon extends React.PureComponent {
    render() {
        return (
            <TouchableCustom
                onPress={this.props.onPress} >
                <Icon
                    name='arrow-back'
                    color='#419FFF' />
            </TouchableCustom>
        )
    }
}

export default class QualityScrap extends React.PureComponent {
    constructor() {
        super();
        this.state = {
            type: 'compid',
            rowguid: '',
            isScan: false, // 是否已扫码
            formCode: '', // 构件报废单号
            compCode: '', // 产品编号
            compId: '', //构件id
            compTypeName: '', // 构件类型
            projectName: '', //项目名称
            projectId: '',
            compTypeId: '',
            designType: '', // 设计型号
            diffSourceType: '', // 子型号
            versionName: '', // 版本号
            volume: '', // 体积
            weight: '', // 重量
            floorNoName: '', // 楼号
            floorNoId: '',
            floorName: '', // 楼层
            floorId: '',
            compState: '', // 构件状态
            compStateInt: '', // 构件状态号
            scrapDate: '', // 报废日期
            operatorUser: '', // 经办人
            remark: '', // 报废原因
            comment: '', // 备注
            fileId: '', // 附件


            pictureVisible: true, // 问题照片
            pictureSelect: false,
            imageOverSize: false, // 照片放大
            pictureUri: "",
            imageArr: [],
            imageFileArr: [],
            fileid: "",
            recid: "",
            isPhotoUpdate: false,
            focusIndex: 0,
            isLoading: false,

            //查询界面取消选择按钮
            isCheck: false,
            isCheckPage: false,

            //控制app拍照，日期，相册选择功能
            isAppPhotoSetting: false,
      currShowImgIndex: 0,
            isAppDateSetting: false,
            isAppPhotoAlbumSetting: false
        }
    }

    componentDidMount() {
        const { navigation } = this.props;
        let guid = navigation.getParam('guid') || ''
        let pageType = navigation.getParam('pageType') || ''
        let isAppPhotoSetting = navigation.getParam('isAppPhotoSetting')
        let isAppDateSetting = navigation.getParam('isAppDateSetting')
        let isAppPhotoAlbumSetting = navigation.getParam('isAppPhotoAlbumSetting')
        this.setState({
            isAppPhotoSetting: isAppPhotoSetting,
            isAppDateSetting: isAppDateSetting,
            isAppPhotoAlbumSetting: isAppPhotoAlbumSetting
        })
        if (pageType == 'CheckPage') {
            this.checkResData(guid)
        } else {
            this.setState({

            })
            //this.resData("753E5EC2EE3D485380BB2618F93391B9")
        }

        //通过使用DeviceEventEmitter模块来监听事件
        this.iDataScan = DeviceEventEmitter.addListener('iDataScan', (Event) => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 115 ~ SteelCage ~ Event.ScanResult", Event.ScanResult)
            if (typeof Event.ScanResult != undefined) {
                let data = Event.ScanResult
                let arr = data.split("=");
                let id = ''
                id = arr[arr.length - 1]
                console.log("🚀 ~ file: concrete_pouring.js ~ line 118 ~ SteelCage ~ id", id)
                if (this.state.type == 'compid') {
                    this.resData(id)
                }

            }
        });
    }

    componentWillUnmount() {
        imageArr = [], imageFileArr = [];
        this.iDataScan.remove()
    }

    UpdateControl = () => {
        if (typeof this.props.navigation.getParam('QRid') != "undefined") {
            if (this.props.navigation.getParam('QRid') != "") {
                this.toast.show(Url.isLoadingView, 0)
            }
        }
        const { navigation } = this.props;
        const QRurl = navigation.getParam('QRurl') || '';
        const QRid = navigation.getParam('QRid') || ''
        const type = navigation.getParam('type') || '';

        if (type == 'compid') {
            this.resData(QRid)
        }

    }

    resData = (QRid) => {
        let formData = new FormData();
        let data = {};
        data = {
            "action": "qualityscrapinit",
            "servicetype": "pdamaterial",
            "express": "6DFD1C9B",
            "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
            "data": {
                "factoryId": Url.PDAFid,
                "compId": QRid,
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: QualityScrap.js ~ line 141 ~ QualityScrap ~ formData:" + formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log("res ---- " + JSON.stringify(Url));
            return res.json();
        }).then(resData => {
            this.toast.close()
            console.log("🚀 ~ file: QualityScrap.js ~ line 152 ~ QualityScrap ~ resData:" + JSON.stringify(resData))
            if (resData.status == '100') {
                let result = resData.result
                this.setState({
                    isScan: true,
                    rowguid: result.rowguid,
                    formCode: result.formCode,
                    compCode: result.compCode,
                    compId: result.compId,
                    compTypeName: result.compTypeName,
                    projectName: result.projectName,
                    projectId: result.projectId,
                    compTypeId: result.compTypeId,
                    designType: result.designType,
                    diffSourceType: result.diffSourceType,
                    versionName: result.versionName,
                    volume: result.volume,
                    weight: result.weight,
                    floorNoName: result.floorNoName,
                    floorNoId: result.floorNoId,
                    floorName: result.floorName,
                    floorId: result.floorId,
                    compState: result.compState,
                    compStateInt: result.compStateInt,
                    scrapDate: result.scrapDate,
                    operatorUser: Url.PDAEmployeeName,
                })
            } else {
                Alert.alert('错误', resData.message)
            }
        }).catch((error) => {
            console.log("error --- " + error)
        });

    }

    checkResData = (guid) => {
        let formData = new FormData();
        let data = {};
        data = {
            "action": "qualityscrapquerydetail",
            "servicetype": "pdamaterial",
            "express": "6DFD1C9B",
            "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
            "data": {
                "guid": guid,
                "factoryId": Url.PDAFid
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: QualityScrap.js ~ line 226 ~ QualityScrap ~ formData:" + JSON.stringify(data))
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log("res ---- " + JSON.stringify(Url));
            return res.json();
        }).then(resData => {
            console.log("🚀 ~ file: QualityScrap.js ~ line 226 ~ QualityScrap ~ resData", resData)
            if (resData.status == '100') {
                let result = resData.result
                imageArr = result.fileId
                if (imageArr.length != 0) {
                    imageArr.map(item => {
                        let tmp = {}
                        tmp.fileid = ""
                        tmp.uri = item
                        tmp.url = item
                        imageFileArr.push(tmp)
                    })
                }
                this.setState({
                    isScan: true,
                    isCheckPage: true,
                    rowguid: result.rowguid,
                    formCode: result.formCode,
                    compCode: result.compCode,
                    compId: result.compId,
                    compTypeName: result.compTypeName,
                    projectName: result.projectName,
                    projectId: result.projectId,
                    compTypeId: result.compTypeId,
                    designType: result.designType,
                    diffSourceType: result.diffSourceType,
                    versionName: result.versionName,
                    volume: result.volume,
                    weight: result.weight,
                    floorNoName: result.floorNoName,
                    floorNoId: result.floorNoId,
                    floorName: result.floorName,
                    floorId: result.floorId,
                    compState: result.compState,
                    compStateInt: result.compStateInt,
                    scrapDate: result.scrapDate,
                    operatorUser: result.operatorUser,
                    remark: result.remark,
                    comment: result.comment,
                    imageArr: result.fileId,
                    imageFileArr: imageFileArr
                })
            } else {
                Alert.alert('错误', resData.message)
            }
        }).catch((error) => {
            console.log("error --- " + error)
        });

    }

    //顶栏
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('title')
        }
    }

    // 时间工具
    _DatePicker = (index) => {
        const { scrapDate } = this.state
        return (
            <DatePicker
                customStyles={{
                    dateInput: styles.dateInput,
                    dateTouchBody: styles.dateTouchBody,
                    dateText: this.state.isAppDateSetting ? styles.dateText : styles.dateDisabled,
                }}
                iconComponent={<Icon name='caretdown' color={this.state.isAppDateSetting ? '#419fff' : "#666"} iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
                }
                showIcon={true}
                date={scrapDate}
                onOpenModal={() => {
                    this.setState({ focusIndex: index })
                }}
                format="YYYY-MM-DD"
                mode="date"
                disabled={!this.state.isAppDateSetting}
                onDateChange={(value) => {
                    this.setState({
                        scrapDate: value
                    })
                }}
            />
        )
    }

    PostData = () => {
        const { rowguid, formCode, compCode, compId, compTypeName, compTypeId, projectName, projectId, designType, diffSourceType, versionName, volume, weight,
            floorNoName, floorNoId, floorName, floorId, compState, compStateInt, scrapDate, operatorUser, remark, comment, recid } = this.state

        if (formCode == '') {
            this.toast.show('请选择报废构件')
            return
        }

        if (this.state.isAppPhotoSetting) {
            if (imageArr.length == 0) {
                this.toast.show('请先拍摄构件照片');
                return
            }
        }

        this.toast.show(saveLoading, 0)

        let formData = new FormData();
        let data = {};

        data = {
            "action": "qualityscrapsave",
            "servicetype": "pdamaterial",
            "express": "6DFD1C9B",
            "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
            "data": {
                "factoryId": Url.PDAFid,
                "editEmployeeId": Url.PDAEmployeeId,
                "editEmployeeName": Url.PDAEmployeeName,
                "rowguid": rowguid,
                "formCode": formCode,
                "compCode": compCode,
                "compId": compId,
                "compTypeName": compTypeName,
                "compTypeId": compTypeId,
                "projectName": projectName,
                "projectId": projectId,
                "designType": designType,
                "diffSourceType": diffSourceType,
                "versionName": versionName,
                "volume": volume,
                "weight": weight,
                "floorNoName": floorNoName,
                "floorNoId": floorNoId,
                "floorName": floorName,
                "floorId": floorId,
                "compState": compState,
                "compStateInt": compStateInt,
                "scrapDate": scrapDate,
                "operatorUser": operatorUser,
                "remark": remark,
                "comment": comment,
                "recid": recid
            },
        }
        if (Url.isAppNewUpload) {
            data.action = "qualityscrapsavenew"
        }
        formData.append('jsonParam', JSON.stringify(data))
        if (!Url.isAppNewUpload) {
            imageArr.map((item, index) => {
                formData.append('img_' + index, {
                    uri: item,
                    type: 'image/jpeg',
                    name: 'img_' + index
                })
            })
        }
        console.log("🚀 ~ file: QualityScrap.js ~ line 793 ~ QualityScrap ~ formData", formData)
        fetch(Url.PDAurl, {
            method: 'post',
            headers: {
                'content-type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res);
            return res.json();
        }).then(resData => {
            console.log("🚀 ~ file: QualityScrap.js ~ line 947 ~ QualityScrap ~ resdata", resData)
            if (resData.status == '100') {
                this.toast.show('保存成功');
                setTimeout(() => {
                    this.props.navigation.replace(this.props.navigation.getParam("page"), {
                        title: this.props.navigation.getParam("title"),
                        pageType: this.props.navigation.getParam("pageType"),
                        page: this.props.navigation.getParam("page"),
                        isAppPhotoSetting: this.props.navigation.getParam("isAppPhotoSetting"),
                        isAppDateSetting: this.props.navigation.getParam("isAppDateSetting"),
                        isAppPhotoAlbumSetting: this.props.navigation.getParam("isAppPhotoAlbumSetting"),
                    })
                }, 1000)
            } else {
                this.toast.show('保存失败')
                Alert.alert('保存失败', resData.message)
                //console.log('resdata', resData)
            }
        }).catch((error) => {
            this.toast.show('保存失败')
            if (error.toString().indexOf('not valid JSON') != -1) {
                Alert.alert('保存失败', "响应内容不是合法JSON格式")
                return
            }
            if (error.toString().indexOf('Network request faile') != -1) {
                Alert.alert('保存失败', "网络请求错误")
                return
            }
            Alert.alert('保存失败', error.toString())
            console.log("error --- " + error)
        });
    }


    // 下拉菜单
    downItem = (index, i, data) => {
        const { teamName, teamSelect } = this.state
        switch (index) {
            case '1': return (
                // 班组
                <View>{(
                    this.state.isCheck ? <Text >{teamSelect}</Text> :
                        <TouchableCustom onPress={() => { this.isItemVisible(teamName, index, i) }} >
                            <IconDown text={teamSelect} />
                        </TouchableCustom>
                )}</View>
            ); break;

            default: return;
        }
    }

    isItemVisible = (data, focusIndex, i) => {
        if (typeof (data) != "undefined") {
            if (data != [] && data != "") {
                switch (focusIndex) {
                    case '1': this.setState({ teamVisible: true, bottomData: data, focusIndex: focusIndex }); break;
                    default: return;
                }
            }
        } else {
            this.toast.show("无数据")
        }
    }

    getPicture = () => {
        return (
            <ListItem
                containerStyle={{ paddingBottom: 6 }}
                leftAvatar={
                    <View >
                        <View style={{ flexDirection: 'column' }} >
                            {
                                this.state.isCheckPage ? <View></View> :
                                    <AvatarAdd
                                        pictureSelect={this.state.pictureSelect}
                                        backgroundColor='rgba(77,142,245,0.20)'
                                        color='#4D8EF5'
                                        title="构"
                                        onPress={() => {
                                            if (this.state.isAppPhotoAlbumSetting) {
                                                this.camandlibFunc()
                                            } else {
                                                this.cameraFunc()
                                            }
                                        }} />
                            }
                            {this.imageView()}
                        </View>
                    </View>
                }
            />
        )
    }

    camandlibFunc = () => {
        Alert.alert(
            '提示信息',
            '选择拍照方式',
            [{
                text: '取消',
                style: 'cancel'
            }, {
                text: '拍照',
                onPress: () => {
                    this.cameraFunc()
                }
            }, {
                text: '相册',
                onPress: () => {
                    this.camLibraryFunc()
                }
            }])
    }

    cameraFunc = () => {
        launchCamera(
            {
                mediaType: 'photo',
                includeBase64: false,
                maxHeight: parseInt(Url.isResizePhoto),
                maxWidth: parseInt(Url.isResizePhoto),
                saveToPhotos: true,

            },
            (response) => {
                if (response.uri == undefined) {
                    return
                }
                imageArr.push(response.uri)
                this.setState({
                    //pictureSelect: true,
                    pictureUri: response.uri,
                    imageArr: imageArr
                })
                if (Url.isAppNewUpload) {
                    this.cameraPost(response.uri)
                } else {
                    this.setState({
                        pictureSelect: true,
                    })
                }
                console.log(response)
            },
        )
    }

    camLibraryFunc = () => {
        launchImageLibrary(
            {
                mediaType: 'photo',
                includeBase64: false,
                maxHeight: parseInt(Url.isResizePhoto),
                maxWidth: parseInt(Url.isResizePhoto),
            },
            (response) => {
                if (response.uri == undefined) {
                    return
                }
                imageArr.push(response.uri)
                this.setState({
                    //pictureSelect: true,
                    pictureUri: response.uri,
                    imageArr: imageArr
                })
                if (Url.isAppNewUpload) {
                    this.cameraPost(response.uri)
                } else {
                    this.setState({
                        pictureSelect: true,
                    })
                }
                console.log(response)
            },
        )
    }

     // 展示/隐藏 放大图片
  handleZoomPicture = (flag, index) => {
    this.setState({
      imageOverSize: false,
      currShowImgIndex: index || 0
    })
  }

  // 加载放大图片弹窗
  renderZoomPicture = () => {
    const { imageOverSize, currShowImgIndex, imageFileArr } = this.state
    return (
      <OverlayImgZoomPicker
        isShowImage={imageOverSize}
        currShowImgIndex={currShowImgIndex}
        zoomImages={imageFileArr}
        callBack={(flag) => this.handleZoomPicture(flag)}
      ></OverlayImgZoomPicker>
    )
  }

  cameraPost = (uri) => {
        const { recid } = this.state
        this.toast.show('图片上传中', 2000)

        let formData = new FormData();
        let data = {};
        data = {
            "action": "savephote",
            "servicetype": "photo_file",
            "express": "D2979AB2",
            "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
            "data": {
                "fileflag": fileflag,
                "username": Url.PDAusername,
                "recid": recid
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        formData.append('img', {
            uri: uri,
            type: 'image/jpeg',
            name: 'img'
        })
        console.log("🚀 ~ file: concrete_pouring.js ~ line 672 ~ SteelCage ~ formData", formData)

        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            console.log(res.statusText);
            console.log(res);
            return res.json();
        }).then(resData => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 690 ~ SteelCage ~ resData", resData)
            if (resData.status == '100') {
                let fileid = resData.result.fileid;
                let recid = resData.result.recid;

                let tmp = {}
                tmp.fileid = fileid
                tmp.uri = uri
        tmp.url = uri
                imageFileArr.push(tmp)
                this.setState({
                    fileid: fileid,
                    recid: recid,
                    pictureSelect: true,
                    imageFileArr: imageFileArr
                })
                console.log("🚀 ~ file: concrete_pouring.js ~ line 704 ~ SteelCage ~ imageFileArr", imageFileArr)
                this.toast.show('图片上传成功');
                this.setState({
                    isPhotoUpdate: false
                })
            } else {
                Alert.alert('图片上传失败', resData.message)
                this.toast.close(1)
            }
        }).catch((error) => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 692 ~ SteelCage ~ error", error)
        });
    }

    cameraDelete = (item) => {
        let i = imageArr.indexOf(item)

        let formData = new FormData();
        let data = {};
        data = {
            "action": "deletephote",
            "servicetype": "photo_file",
            "express": "D2979AB2",
            "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
            "data": {
                "fileid": item.fileid,
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: concrete_pouring.js ~ line 733 ~ SteelCage ~ cameraDelete ~ formData", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            console.log(res.statusText);
            console.log(res);
            return res.json();
        }).then(resData => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 745 ~ SteelCage ~ cameraDelete ~ resData", resData)
            if (resData.status == '100') {
                if (i > -1) {
                    imageArr.splice(i, 1)
                    imageFileArr.splice(i, 1)
                    this.setState({
                        imageArr: imageArr,
                        imageFileArr: imageFileArr
                    }, () => {
                        this.forceUpdate()
                    })
                }
                if (imageArr.length == 0) {
                    this.setState({
                        pictureSelect: false
                    })
                }
                this.toast.show('图片删除成功');
            } else {
                Alert.alert('图片删除失败', resData.message)
                this.toast.close(1)
            }
        }).catch((error) => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 761 ~ SteelCage ~ cameraDelete ~ error", error)
        });
    }

    imageView = () => {
        return (
            <View style={{ flexDirection: 'row' }}>
                {
                    this.state.imageFileArr == [] ? <View /> :
                        this.state.imageArr.map((item, index) => {
                            return (
                                <AvatarImg
                                    item={item}
                                    index={index}
                                    onPress={() => {
                                        this.setState({
                                            imageOverSize: true,
                                            pictureUri: item
                                        })
                                    }}
                                    onLongPress={() => {
                                        Alert.alert(
                                            '提示信息',
                                            '是否删除构件照片',
                                            [{
                                                text: '取消',
                                                style: 'cancel'
                                            }, {
                                                text: '删除',
                                                onPress: () => {
                                                    this.cameraDelete(item)
                                                }
                                            }])
                                    }} />
                            )
                        })
                }
            </View>
        )
    }

    render() {
        const { navigation } = this.props;
        //从主页面传url, 未来集成到一起
        const QRurl = navigation.getParam('QRurl') || '';
        const QRid = navigation.getParam('QRid') || ''
        const type = navigation.getParam('type') || '';
        let pageType = navigation.getParam('pageType') || ''

        if (type == 'compid') {
            QRcompid = QRid
        }
        const { isCheckPage, focusIndex, isScan, formCode, compCode, projectName, compTypeName, designType, diffSourceType, versionName, volume, weight, floorNoName, floorName, compState, scrapDate, operatorUser, remark, comment, fileId } = this.state

        if (this.state.isLoading) {
            return (
                <View style={styles.loading}>
                    <ActivityIndicator
                        animating={true}
                        color='#419FFF'
                        size="large" />
                </View>
            )
            //渲染页面
        } else {
            return (
                <View style={{ flex: 1 }}>
                    <Toast ref={(ref) => { this.toast = ref; }} position="center" />
                    <NavigationEvents
                        onWillFocus={this.UpdateControl}
                        onWillBlur={() => {
                            if (QRid != '') {
                                navigation.setParams({ QRid: '', type: '' })
                            }
                        }} />
                    <ScrollView ref={ref => this.scoll = ref} style={styles.mainView} showsVerticalScrollIndicator={false} >

                        <View style={styles.listView}>
                            {this.getPicture()}
                            <ListItemScan
                                focusStyle={focusIndex == '0' ? styles.focusColor : {}}
                                onPressIn={() => {
                                    this.setState({
                                        type: 'compid',
                                        focusIndex: 0
                                    })
                                    NativeModules.PDAScan.onScan();
                                }}
                                onPress={() => {
                                    this.setState({
                                        type: 'compid',
                                        focusIndex: 0
                                    })
                                    NativeModules.PDAScan.onScan();
                                }}
                                onPressOut={() => {
                                    NativeModules.PDAScan.offScan();
                                }}
                                title='产品编号:'
                                rightElement={
                                    isScan ? compCode :
                                        <ScanButton
                                            onPress={() => {
                                                this.props.navigation.navigate('QRCode', {
                                                    type: 'compid',
                                                    page: 'QualityScrap'
                                                })
                                            }}
                                            onLongPress={() => {
                                                this.setState({
                                                    type: 'compid',
                                                    focusIndex: 0
                                                })
                                                NativeModules.PDAScan.onScan();
                                            }}
                                            onPressOut={() => {
                                                NativeModules.PDAScan.offScan();
                                            }}
                                        />
                                } />
                            {
                                !isScan ? <View /> :
                                    <Card containerStyle={QSstyles.card_containerStyle}>
                                        <ListItem
                                            containerStyle={QSstyles.list_container_style}
                                            title='项目名称'
                                            rightTitle={projectName}
                                            titleStyle={QSstyles.title}
                                            rightTitleStyle={QSstyles.rightTitle}
                                            rightTitleProps={{ numberOfLines: 1 }}
                                        />
                                        <ListItem
                                            containerStyle={QSstyles.list_container_style}
                                            title='构件类型'
                                            rightTitle={compTypeName}
                                            titleStyle={QSstyles.title}
                                            rightTitleStyle={QSstyles.rightTitle}
                                        />
                                        <ListItem
                                            containerStyle={QSstyles.list_container_style}
                                            title='设计型号'
                                            rightTitle={designType}
                                            titleStyle={QSstyles.title}
                                            rightTitleStyle={QSstyles.rightTitle}
                                        />
                                        <ListItem
                                            containerStyle={QSstyles.list_container_style}
                                            title='楼号'
                                            rightTitle={floorNoName}
                                            titleStyle={QSstyles.title}
                                            rightTitleStyle={QSstyles.rightTitle}
                                        />
                                        <ListItem
                                            containerStyle={QSstyles.list_container_style}
                                            title='楼层'
                                            rightTitle={floorName}
                                            titleStyle={QSstyles.title}
                                            rightTitleStyle={QSstyles.rightTitle}
                                        />
                                        {
                                            typeof (weight) == 'undefined' ? <View></View> :
                                                <ListItem
                                                    containerStyle={QSstyles.list_container_style}
                                                    title='重量'
                                                    rightTitle={weight}
                                                    titleStyle={QSstyles.title}
                                                    rightTitleStyle={QSstyles.rightTitle}
                                                />
                                        }
                                        <ListItem
                                            containerStyle={QSstyles.list_container_style}
                                            title='生产单位'
                                            rightTitle={Url.PDAFname}
                                            titleStyle={QSstyles.title}
                                            rightTitleStyle={QSstyles.rightTitle}
                                            rightTitleProps={{ numberOfLines: 1 }}
                                        />
                                    </Card>
                            }

                            {/*<ListItemScan focusStyle={focusIndex == '1' ? styles.focusColor : {}} title='产品编号:' rightElement={compCode} />*/}
                            {/*<ListItemScan focusStyle={focusIndex == '2' ? styles.focusColor : {}} title='项目名称:' rightElement={projectName} />*/}
                            {/*<ListItemScan focusStyle={focusIndex == '3' ? styles.focusColor : {}} title='构件类型:' rightElement={compTypeName} />*/}
                            {/*<ListItemScan focusStyle={focusIndex == '4' ? styles.focusColor : {}} title='设计型号:' rightElement={designType} />*/}
                            {/*<ListItemScan focusStyle={focusIndex == '5' ? styles.focusColor : {}} title='子型号:' rightElement={diffSourceType} />*/}
                            {/*<ListItemScan focusStyle={focusIndex == '6' ? styles.focusColor : {}} title='版本号:' rightElement={versionName} />*/}
                            {/*<ListItemScan focusStyle={focusIndex == '7' ? styles.focusColor : {}} title='体积(m³):' rightElement={volume} />*/}
                            {/*<ListItemScan focusStyle={focusIndex == '8' ? styles.focusColor : {}} title='重量(吨):' rightElement={weight} />*/}
                            {/*<ListItemScan focusStyle={focusIndex == '9' ? styles.focusColor : {}} title='楼号:' rightElement={floorNoName} />*/}
                            {/*<ListItemScan focusStyle={focusIndex == '10' ? styles.focusColor : {}} title='楼层:' rightElement={floorName} />*/}
                            {/*<ListItemScan focusStyle={focusIndex == '11' ? styles.focusColor : {}} title='构件状态:' rightElement={compState} />*/}
                            <ListItemScan focusStyle={focusIndex == '12' ? styles.focusColor : {}} title='报废日期:' rightElement={isCheckPage ? scrapDate : this._DatePicker(12)} />
                            <ListItemScan focusStyle={focusIndex == '13' ? styles.focusColor : {}} title='经办人:' rightElement={operatorUser} />
                            <ListItemScan focusStyle={focusIndex == '14' ? styles.focusColor : {}} title='报废原因:' rightElement={() =>
                                isCheckPage ? <Text>{remark}</Text> :
                                    <View>
                                        <Input
                                            containerStyle={styles.quality_input_container}
                                            inputContainerStyle={styles.inputContainerStyle}
                                            inputStyle={[styles.quality_input_, { top: 7 }]}
                                            placeholder='请输入'
                                            value={remark}
                                            onChangeText={(value) => {
                                                this.setState({
                                                    remark: value
                                                })
                                            }} />
                                    </View>
                            }
                            />
                            <ListItemScan focusStyle={focusIndex == '15' ? styles.focusColor : {}} title='备注:' rightElement={() =>
                                isCheckPage ? <Text>{comment}</Text> :
                                    <View>
                                        <Input
                                            containerStyle={styles.quality_input_container}
                                            inputContainerStyle={styles.inputContainerStyle}
                                            inputStyle={[styles.quality_input_, { top: 7 }]}
                                            placeholder='请输入'
                                            value={comment}
                                            onChangeText={(value) => {
                                                this.setState({
                                                    comment: value
                                                })
                                            }} />
                                    </View>
                            }
                            />
                        </View>
                        {isCheckPage ? <View /> :
                            <FormButton
                                backgroundColor='#17BC29'
                                onPress={() => {
                                    this.PostData()
                                }}
                                title='保存'
                                disabled={this.state.isPhotoUpdate}
                            />
                        }



                        {/*照片放大*/}
                        {this.renderZoomPicture()}
                        {/* {overlayImg(obj = {
                            onRequestClose: () => {
                                this.setState({ imageOverSize: !this.state.imageOverSize }, () => {
                                    console.log("🚀 ~ file: equipment_maintenance.js:1696 ~ render ~ imageOverSize:", this.state.imageOverSize)
                                })
                            },
                            onPress: () => {
                                this.setState({
                                    imageOverSize: !this.state.imageOverSize
                                })
                            },
                            uri: this.state.pictureUri,
                            isVisible: this.state.imageOverSize
                        })
                        } */}
                    </ScrollView>
                </View>
            )
        }
    }

}


const QSstyles = StyleSheet.create({
    card_containerStyle: {
        borderRadius: 10,
        shadowOpacity: 0,
        backgroundColor: '#f8f8f8',
        borderWidth: 0,
        paddingVertical: 13,
        elevation: 0
    },
    title: {
        fontSize: 13,
        color: '#999'
    },
    rightTitle: {
        fontSize: 13,
        textAlign: 'right',
        lineHeight: 14,
        flexWrap: 'wrap',
        width: width / 2,
        color: '#333'
    },
    list_container_style: { marginVertical: 0, paddingVertical: 9, backgroundColor: 'transparent' },

})

if (deviceWidth <= 330) {
    QSstyles.card_containerStyle = {
        borderRadius: 10,
        shadowOpacity: 0,
        backgroundColor: '#f8f8f8',
        borderWidth: 0,
        paddingVertical: 10,
        paddingHorizontal: 5,
        elevation: 0
    }
    QSstyles.list_container_style = {
        marginVertical: 0,
        paddingVertical: 5,
        backgroundColor: 'transparent'
    }
    QSstyles.rightTitle = {
        fontSize: 13,
        textAlign: 'right',
        lineHeight: 16,
        width: width * 0.4,
        color: '#333'
    }
}