import React from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, Icon, Card, ListItem, BottomSheet, CheckBox, Avatar, Overlay, Header, Input, Image } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { RFT } from '../../Url/Pixal';
import { ScrollView } from 'react-native-gesture-handler';
import CardList from "../Componment/CardList";
import DatePicker from 'react-native-datepicker'
import { styles } from "../Componment/PDAStyles";
import { NavigationEvents } from 'react-navigation';
import ScanButton from '../Componment/ScanButton';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import ListItemScan_child from '../Componment/ListItemScan_child';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import BatchListItem from '../Componment/BatchBottomItem';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import CheckBoxScan from '../Componment/CheckBoxScan';
import AvatarAdd from '../Componment/AvatarAdd';
import AvatarImg from '../Componment/AvatarImg';

import { DeviceEventEmitter, NativeModules } from 'react-native';


let varGetError = false

const { height, width } = Dimensions.get('window') //获取宽高

let compIdStr = '', compIdArr = [], compDataArr = [], VolumeSum = 0, tmpstr = '', visibleArr = [];

export default class Wantconcrete extends React.PureComponent {
  constructor() {
    super();
    this.state = {
      resData: [],
      type: "compid",
      rowguid: '',
      formCode: '',
      ServerTime: '2000-01-01 00:00',
      TheoryVolume: 0,
      State: 0,
      remark: '',
      compId: '',
      compData: {},
      compIdStr: '',
      compIdArr: [],
      compDataArr: [],
      //混凝土类型
      ConcreteTypes: [],
      DispContent: '',
      DBContent: '',
      DispContentSelect: '请选择',
      //混凝土强度
      ConcreteGrades: [],
      ConcreteGrade: '',
      ConcreteGradeSelect: '请选择',
      //
      WastageRate: "",
      ActualVolume: "",
      //生产线
      ProductLineId: '',
      ProductLineName: '',
      //是否扫码成功
      isGetcomID: false,
      //长按删除功能控制
      iscomIDdelete: false,
      //底栏控制
      bottomData: [],
      bottomVisible: false,
      datepickerVisible: false,
      response: '',
      //
      GetError: false,
      buttondisable: false,
      //方量
      Volume: 0,
      VolumeSum: 0,
      //焦点框
      focusIndex: '5',
      visibleArr: [],
      isLoading: true,
      isCheckPage: false,
      keyboardHeight: 0,
      //批量
      isBatch: false,
      listBatchNoInfo: [],
      batchCompCode: '',
      batchCompCodeselect: '请选择',
      CCallSub: [],
    }
  }

  componentDidMount() {
    const { navigation } = this.props;
    let guid = navigation.getParam('guid') || ''
    let pageType = navigation.getParam('pageType') || ''
    if (pageType == 'CheckPage') {
      this.checkResData(guid)
      this.setState({ isCheckPage: true })
    } else if (pageType == 'BatchScan') {
      this.batchResData()
    } else {
      this.resData()
    }

    //通过使用DeviceEventEmitter模块来监听事件
    this.iDataScan = DeviceEventEmitter.addListener('iDataScan', (Event) => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 115 ~ SteelCage ~ Event.ScanResult", Event.ScanResult)
      if (typeof Event.ScanResult != undefined) {
        let data = Event.ScanResult
        let arr = data.split("=");
        let id = ''
        id = arr[arr.length - 1]
        console.log("🚀 ~ file: concrete_pouring.js ~ line 118 ~ SteelCage ~ id", id)
        if (this.state.type == 'compid') {
          this.GetcomID(id)
        }

      }
    });
  }

  componentWillUnmount() {
    compIdStr = '', compIdArr = [], compDataArr = [], VolumeSum = 0, tmpstr = '', visibleArr = [];
    this.iDataScan.remove()
  }

  UpdateControl = () => {
    if (typeof this.props.navigation.getParam('QRid') != "undefined") {
      if (this.props.navigation.getParam('QRid') != "") {
        this.toast.show(Url.isLoadingView, 0)
      }
    }
    const { navigation } = this.props;
    const { isGetcomID } = this.state
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || ''
    let type = navigation.getParam('type') || '';
    if (type == 'compid') {
      this.GetcomID(QRid)
    }
  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  checkResData = (guid) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getcalldata",
      "servicetype": "concrete",
      "express": "8AC4C0B0",
      "ciphertext": "7df83f712027c63f147f6c2d4a43f08d",
      "data": {
        "_Rowguid": guid,
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: qualityinspection.js ~ line 189 ~ QualityInspection ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: wantconcrete.js ~ line 152 ~ Wantconcrete ~ resData", resData)
      let ProductLineName = resData.result.ProductLineName
      let formCode = resData.result.FormCode
      let ServerTime = resData.result.EditDate
      let DispContentSelect = resData.result.ConcreteType
      let DispContent = resData.result.ConcreteType
      let DBContent = resData.result.ConcreteType
      let ConcreteGrade = resData.result.ConcreteGrade
      let ConcreteGradeSelect = resData.result.ConcreteGrade
      let WastageRate = resData.result.WastageRate;
      let ActualVolume = resData.result.ActualVolume;
      let remark = resData.result.Remark
      let CCallSub = resData.result.CCallSub
      let VolumeSum = resData.result.TheoryVolume
      compDataArr = CCallSub

      compDataArr.map((item, index) => {
        let Volume = item.ConcretrVolume;
        VolumeSum += Volume
      })


      this.setState({
        resData: resData,
        ServerTime: ServerTime,
        ProductLineName: ProductLineName,
        formCode: formCode,
        DispContentSelect: DispContentSelect,
        DispContent: DispContent,
        DBContent: DBContent,
        ConcreteGrade: ConcreteGrade,
        ConcreteGradeSelect: ConcreteGradeSelect,
        WastageRate: WastageRate,
        ActualVolume: ActualVolume,
        remark: remark,
        VolumeSum: VolumeSum,
        TheoryVolume: VolumeSum,
        compDataArr: compDataArr,
        isCheck: true
      })

      this.setState({
        isLoading: false,
      })
    }).catch((error) => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 215 ~ QualityInspection ~ error", error)
    });
  }

  resData = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getcreatecalldata",
      "servicetype": "concrete",
      "express": "8AC4C0B0",
      "ciphertext": "7df83f712027c63f147f6c2d4a43f08d",
      "data": {
        "_bizid": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
       this.toast.close()
      if (resData.status == '100') {
        let ServerTime = resData.result.EditDate
        let formCode = resData.result.FormCode;
        let TheoryVolume = resData.result.TheoryVolume;
        let State = resData.result.State;
        let ConcreteTypes = resData.result.ConcreteTypes;
        let DispContent = ConcreteTypes[0].DispContent;
        let DBContent = ConcreteTypes[0].DBContent;
        let ConcreteGrades = resData.result.ConcreteGrades;
        let WastageRate = resData.result.WastageRate;
        console.log("🚀 ~ file: wantconcrete.js ~ line 219 ~ Wantconcrete ~ WastageRate", WastageRate)
        let ActualVolume = resData.result.ActualVolume;
        this.setState({
          resData: resData,
          formCode: formCode,
          ServerTime: ServerTime,
          TheoryVolume: TheoryVolume,
          State: State,
          ConcreteTypes: ConcreteTypes,
          ConcreteGrades: ConcreteGrades,
          DispContentSelect: DispContent,
          DBContent: DBContent,
          DispContent: DispContent,
          WastageRate: WastageRate,
          ActualVolume: ActualVolume,
          isLoading: false
        });
      } else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }
    }).catch((error) => {
    });

  }

  batchResData = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getcreatecalldata",
      "servicetype": "concrete",
      "express": "8AC4C0B0",
      "ciphertext": "7df83f712027c63f147f6c2d4a43f08d",
      "data": {
        "_bizid": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
       this.toast.close()
      if (resData.status == '100') {
        let ServerTime = resData.result.EditDate
        let formCode = resData.result.FormCode;
        let TheoryVolume = resData.result.TheoryVolume;
        let State = resData.result.State;
        let ConcreteTypes = resData.result.ConcreteTypes;
        let DispContent = ConcreteTypes[0].DispContent;
        let DBContent = ConcreteTypes[0].DBContent;
        let ConcreteGrades = resData.result.ConcreteGrades;
        let WastageRate = resData.result.WastageRate;
        console.log("🚀 ~ file: wantconcrete.js ~ line 219 ~ Wantconcrete ~ WastageRate", WastageRate)
        let ActualVolume = resData.result.ActualVolume;
        let CCallSub = resData.result.CCallSub;

        this.setState({
          resData: resData,
          formCode: formCode,
          ServerTime: ServerTime,
          TheoryVolume: TheoryVolume,
          State: State,
          ConcreteTypes: ConcreteTypes,
          ConcreteGrades: ConcreteGrades,
          DispContentSelect: DispContent,
          DBContent: DBContent,
          CCallSub: CCallSub,
          DispContent: DispContent,
          WastageRate: WastageRate,
          ActualVolume: ActualVolume,
          isLoading: false,
          isBatch: true
        });
      } else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }
    }).catch((error) => {
    });

  }

  GetcomID = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getcallbasedata",
      "servicetype": "concrete",
      "express": "227EB695",
      "ciphertext": "de110251c4c1faf0038a3416d728a830",
      "data": {
        "CompId": id,
        "_bizid": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: storage.js ~ line 241 ~ PDAStorage ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      if (resData.status == "100") {
        let compData = resData.result;
        console.log(
          "🚀 ~ file: storage.js ~ line 301 ~ PDAStorage ~ compData",
          compData
        );
        let ProductLineId = compData.ProductLineId;
        let ProductLineName = compData.ProductLineName;
        let TheoryVolume = compData.TheoryVolume;
        let ConcreteGrade = compData.ConcreteGrade;
        let State = compData.State;
        let CCallSub = compData.CCallSub[0];
        let CompId = CCallSub.CompId;
        let Volume = CCallSub.ConcretrVolume;
        if (compDataArr.length == 0) {
          compDataArr.push(CCallSub);
          compIdArr.push(CompId);
          VolumeSum = Volume;
          let ActualVolume =
            VolumeSum + VolumeSum * this.state.WastageRate * 0.01;
          this.setState({
            compDataArr: compDataArr,
            compIdArr: compIdArr,
            compData: compData,
            compId: "",
            ProductLineId: ProductLineId,
            ProductLineName: ProductLineName,
            TheoryVolume: VolumeSum.toFixed(2),
            ActualVolume: ActualVolume.toFixed(3),
            ConcreteGrade: ConcreteGrade,
            ConcreteGradeSelect: ConcreteGrade,
            State: State,
            Volume: Volume,
            VolumeSum: Volume.toFixed(2),
            isGetcomID: true
          });
          tmpstr = tmpstr + JSON.stringify(CCallSub) + ",";
        } else {
          if (this.state.ProductLineId != ProductLineId) {
            this.toast.show("生产线与已选构件不符");
            return;
          } else if (this.state.ConcreteGrade != ConcreteGrade) {
            this.toast.show("混凝土强度与已选构件不符");
            return;
          }
          if (tmpstr.indexOf(CompId) == -1) {
            compIdArr.push(CompId);
            console.log(
              "🚀 ~ file: wantconcrete.js ~ line 213 ~ Wantconcrete ~ compIdArr",
              compIdArr
            );
            compDataArr.push(CCallSub);
            VolumeSum += Volume;
            let ActualVolume =
              VolumeSum + VolumeSum * this.state.WastageRate * 0.01;
            this.setState({
              compDataArr: compDataArr,
              compIdArr: compIdArr,
              compData: compData,
              compId: "",
              ProductLineId: ProductLineId,
              ProductLineName: ProductLineName,
              TheoryVolume: VolumeSum.toFixed(2),
              ActualVolume: ActualVolume.toFixed(3),
              State: State,
              Volume: Volume,
              VolumeSum: VolumeSum.toFixed(2),
              isGetcomID: true
            });
            tmpstr = tmpstr + JSON.stringify(CCallSub) + ",";
            console.log(
              "🚀 ~ file: storage.js ~ line 307 ~ PDAStorage ~ tmpstr",
              tmpstr
            );
          } else {
            this.toast.show('已经扫瞄过此构件')
          }
        }
        this.setState({
          compId: ""
        })
        console.log(
          "🚀 ~ file: storage.js ~ line 292 ~ PDAStorage ~ compDataArr",
          compDataArr
        );
      } else {
        Alert.alert('错误', resData.message)
        this.setState({
          compId: ""
        })
        //this.input1.focus()
      }

    }).catch(err => {
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
      console.log("🚀 ~ file: qualityinspection.js ~ line 210 ~ SteelCage ~ err", err)
    })
  }

  PostData = () => {
    const { ConcreteGrade, compDataArr, compIdArr, DBContent, DispContent, formCode, ActualVolume, WastageRate, remark } = this.state

    let compIdstr = compIdArr.toString()

    let CCallSub = []

    if (compIdArr.length == 0) {
      this.toast.show('叫料明细不能为空');
      return
    } else if (WastageRate == '') {
      this.toast.show('搅拌损耗率不能为空');
      return
    } else if (ActualVolume == '') {
      this.toast.show('理论方量+损耗量不能为空');
      return
    }

    compIdArr.map((item, index) => {
      let tmpobj = {}
      tmpobj.compId = item
      tmpobj.compMode = '按件',
        tmpobj.num = 0
      CCallSub.push(tmpobj)
    })

    let formData = new FormData();
    let data = {};
    data = {
      "action": "savecalldata",
      "servicetype": "concrete",
      "express": "3C835260",
      "ciphertext": "1c80b8262c5c849015b3259389b28261",
      "data": {
        "ConcreteGrade": ConcreteGrade,
        "CCallSub": CCallSub,
        "ConcreteType": DBContent,
        "_bizid": Url.PDAFid,
        "FormCode": formCode,
        "Remark": remark,
        "username": Url.PDAusername,
        "ActualVolume": ActualVolume,
        "WastageRate": WastageRate
      }
    }

    this.toast.show(saveLoading, 0)

    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: steel_cage_storage.js ~ line 193 ~ SteelCage ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('保存成功');
        setTimeout(() => {
          this.props.navigation.replace(this.props.navigation.getParam("page"), {
            title: this.props.navigation.getParam("title"),
            pageType: this.props.navigation.getParam("pageType"),
            page: this.props.navigation.getParam("page"),
            isAppPhotoSetting: this.props.navigation.getParam("isAppPhotoSetting"),
            isAppDateSetting: this.props.navigation.getParam("isAppDateSetting"),
            isAppPhotoAlbumSetting: this.props.navigation.getParam("isAppPhotoAlbumSetting"),
          })
          //this.props.navigation.navigate('PDAMainStack')
        }, 400)
      } else {
        this.toast.show('保存失败')
        Alert.alert('保存失败', resData.message)
      }
      console.log("🚀 ~ file: steel_cage_storage.js ~ line 195 ~ SteelCage ~ resData", resData)
    }).catch((error) => {
      console.log("🚀 ~ file: steel_cage_storage.js ~ line 75 ~ SteelCage ~ error", error)
    }).catch(error => {
      this.toast.show('保存失败')
      if (error.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('保存失败', "响应内容不是合法JSON格式")
        return
      }
      if (error.toString().indexOf('Network request faile') != -1) {
        Alert.alert('保存失败', "网络请求错误")
        return
      }
      Alert.alert('保存失败', error.toString())
    });
  }

  CardList = (compData) => {
    console.log("🚀 ~ file: wantconcrete.js ~ line 438 ~ Wantconcrete ~ compData", compData)
    const { WastageRate, visibleArr } = this.state
    return (
      compData.map((item, index) => {
        let comp = item
        return (
          <View style={styles.CardList_main}>
            <TouchableCustom
              onPress={() => {
                this.cardListContont(comp);
                this.forceUpdate();
                console.log("🚀 ~ file: wantconcrete.js ~ line 623 ~ Wantconcrete ~ visibleArr", visibleArr)
                console.log("🚀 ~ file: wantconcrete.js ~ line 497 ~ Wantconcrete ~ compData.map ~ visibleArr.indexOf(item.ProductCode) != -1", visibleArr.indexOf(item._Rowguid) != -1)
              }}
              onLongPress={() => {
                Alert.alert(
                  '提示信息',
                  '是否删除构件信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      console.log("🚀 ~ file: wantconcrete.js ~ line 350 ~ Wantconcrete ~ compData.map ~ compDataArr", compDataArr)
                      compDataArr.splice(index, 1)
                      compIdArr.splice(index, 1)
                      let ActualVolume1 = 0
                      VolumeSum = 0
                      compDataArr.map((deleteitem) => {
                        let Volume1 = deleteitem.ConcretrVolume;
                        VolumeSum += Volume1;
                        ActualVolume1 =
                          VolumeSum + VolumeSum * WastageRate * 0.01;
                      })
                      tmpstr = compIdArr.toString();
                      console.log("🚀 ~ file: wantconcrete.js ~ line 350 ~ Wantconcrete ~ compData.map ~ compDataArr", compDataArr)
                      this.setState({
                        compDataArr: compDataArr,
                        compIdArr: compIdArr,
                        TheoryVolume: VolumeSum.toFixed(2),
                        ActualVolume: ActualVolume1.toFixed(2)
                      }, () => {
                        this.forceUpdate()
                      })
                    }
                  }])

              }}>
              <View style={styles.CardList_title_main}>
                <View style={styles.title_num}>
                  <Text >{index + 1}</Text>
                </View>
                <View style={styles.title_title}>
                  <Text >{item.ProductCode}</Text>
                </View>
                <View style={styles.CardList_at_icon}>
                  <IconDown />
                </View>
              </View>
            </TouchableCustom>
            {
              visibleArr.indexOf(item._Rowguid) != -1 ?
                <View key={index} style={{ backgroundColor: 'transparent' }} >
                  <ListItemScan_child
                    title='项目名称'
                    rightTitle={comp.ProjectName}
                  />
                  <ListItemScan_child
                    title='产品编号'
                    rightTitle={comp.ProductCode}
                  />
                  <ListItemScan_child
                    title='设计型号'
                    rightTitle={comp.DesignType}
                  />
                  <ListItemScan_child
                    title='楼号'
                    rightTitle={comp.FloorNoName}
                  />
                  <ListItemScan_child
                    title='层号'
                    rightTitle={comp.FloorName}
                  />
                  <ListItemScan_child
                    title='砼方量'
                    rightTitle={comp.ConcretrVolume}
                  />
                  <ListItemScan_child
                    title='数量'
                    rightTitle={comp.Num}
                  />
                  <View style={{ height: 8 }}></View>
                </View> : <View></View>
            }

          </View>
        )
      })
    )
  }

  _DatePicker = () => {
    const { ServerTime } = this.state
    return (
      <DatePicker
        customStyles={{
          dateInput: styles.dateInput,
          dateTouchBody: styles.dateTouchBody,
          dateText: styles.dateText,
        }}
        onOpenModal={() => {
          this.setState({ focusIndex: '3' })
        }}
        iconComponent={<Icon name='caretdown' color='#419fff' iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
        }
        showIcon={true}
        mode='datetime'
        date={ServerTime}
        format="YYYY-MM-DD HH:mm"
        onDateChange={(value) => {
          this.setState({
            ServerTime: value
          })
        }}
      />
    )
  }

  BottomList = () => {
    const { bottomVisible, bottomData } = this.state
    return (
      <BottomSheet
        isVisible={bottomVisible && !this.state.isCheckPage}
        onRequestClose={() => {
          this.setState({
            bottomVisible: false
          })
        }}
        onBackdropPress={() => {
          this.setState({
            bottomVisible: false
          })
        }}
      //modalProps={{ style: { height: 100 }, }}
      >
        <ScrollView style={styles.bottomsheetScroll}>
          {
            bottomData.map((item, index) => {
              let title = ''
              let dataItem = {}
              if (item.DispContent) {
                title = item.DBContent
              } else if (item.CompId) {
                dataItem.Title = item.ProductCode
                dataItem.rightTitle = item.Num
                dataItem.subTitle = item.CompTypeName
                dataItem.subRightTitle = item.ProjectName
              } else if (item.ConcreteGrade) {
                title = item.ConcreteGrade
              }
              return (
                <TouchableCustom
                  onPress={() => {
                    if (item.DispContent) {
                      this.setState({
                        DispContentSelect: item.DispContent,
                        DBContent: item.DBContent,
                        DispContent: item.DispContent,
                      })
                    } else if (item.CompId) {
                      compDataArr = []
                      compIdArr = []
                      let ProductLineId = item.ProductLineId;
                      let ProductLineName = item.ProductLineName;
                      let TheoryVolume = item.ConcretrVolume * item.Num;
                      let ConcreteGrade = item.ConcreteGrade;
                      let State = item.State;
                      let CCallSub = item;
                      let CompId = item.CompId;
                      let Volume = item.ConcretrVolume;
                      let ActualVolume =
                        TheoryVolume + TheoryVolume * this.state.WastageRate * 0.01;
                      compDataArr.push(item)
                      compIdArr.push(CompId)
                      this.setState({
                        compDataArr: compDataArr,
                        compIdArr: compIdArr,
                        compData: item,
                        compId: CompId,
                        ProductLineId: ProductLineId,
                        ProductLineName: ProductLineName,
                        ConcreteGrade: ConcreteGrade,
                        ConcreteGradeSelect: ConcreteGrade,
                        TheoryVolume: TheoryVolume.toFixed(2),
                        ActualVolume: ActualVolume.toFixed(3),
                        State: State,
                        Volume: Volume,
                        isGetcomID: true
                      });
                    } else if (item.ConcreteGrade) {
                      this.setState({
                        ConcreteGrade: item.ConcreteGrade,
                        ConcreteGradeSelect: item.ConcreteGrade,
                      })
                    }
                    this.setState({
                      bottomVisible: false
                    })
                  }}
                >
                  {
                    !item.CompId ? <BottomItem backgroundColor='white' color="#333" title={title} /> :
                      <BatchListItem backgroundColor='white' color="#333" dataItem={dataItem} />
                  }
                </TouchableCustom>
              )
            })
          }
        </ScrollView>
        <TouchableCustom
          onPress={() => {
            this.setState({ bottomVisible: false })
          }}>
          <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
        </TouchableCustom>
      </BottomSheet>

    )
  }

  cardListContont = comp => {
    if (visibleArr.indexOf(comp._Rowguid) == -1) {
      visibleArr.push(comp._Rowguid);
      this.setState({ visibleArr: visibleArr });
    } else {
      if (visibleArr.length == 1) {
        visibleArr.splice(0, 1);
        this.setState({ visibleArr: visibleArr }, () => {
          this.forceUpdate;
        });
      } else {
        visibleArr.map((item, index) => {
          if (item == comp._Rowguid) {
            visibleArr.splice(index, 1);
            this.setState({ visibleArr: visibleArr }, () => {
              this.forceUpdate;
            });
          }
        });
      }
    }
  };

  isBottomVisible = (data, focusIndex) => {
    if (typeof (data) != "undefined") {
      if (data.length > 0) {
        this.setState({ bottomVisible: true, bottomData: data, focusIndex: focusIndex })
      }
    } else {
      this.toast.show("无数据")
    }
  }

  batchListView = () => {
    return (
      <TouchableCustom underlayColor={'lightgray'} onPress={() => {
        this.isBottomVisible(this.state.CCallSub, "5")
      }}>

        <ListItemScan
          isButton={false}
          focusStyle={this.state.focusIndex == '5' ? styles.focusColor : {}}
          title='生产批次'
          rightElement={
            <IconDown text={this.state.batchCompCodeselect}></IconDown>
          }
        />
      </TouchableCustom>
    )
  }

  BottomSheetBatchMain = () => {
    const { bottomVisible, bottomData, isCheckPage } = this.state;
    return (
      <BottomSheet
        isVisible={bottomVisible && !isCheckPage}
        onRequestClose={() => {
          this.setState({
            bottomVisible: false
          })
        }}
        onBackdropPress={() => {
          this.setState({
            bottomVisible: false
          })
        }}
      >
        <ScrollView style={styles.bottomsheetScroll}>
          {
            bottomData.map((item, index) => {
              let dataItem = {}
              dataItem.Title = item.compCode
              dataItem.rightTitle = item.ProductionQuantity
              dataItem.subTitle = item.CompTypeName
              dataItem.subRightTitle = item.ProjectName
              return (
                <TouchableOpacity
                  onPress={() => {
                    let compData = {}
                    compData.result = item
                    this.setState({
                      batchCompCode: item.compCode,
                      batchCompCodeselect: item.compCode,
                      productLineName: item.productLineName,
                      teamName: item.teamName,
                      compId: item.compId,
                      compCode: item.compCode,
                      compData: compData,
                      isGetcomID: true,
                    })
                    this.setState({
                      bottomVisible: false
                    })
                  }}
                >
                  <BatchBottomItem backgroundColor='white' color="#333" dataItem={dataItem} />
                </TouchableOpacity>
              )

            })
          }
        </ScrollView>
        <TouchableCustom
          onPress={() => {
            this.setState({ bottomVisible: false })
          }}>
          <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
        </TouchableCustom>
      </BottomSheet>
    )
  }
  render() {
    const { navigation } = this.props;

    const { isBatch, isLoading, focusIndex, formCode, ConcreteTypes, DispContentSelect, ConcreteGrades, ConcreteGradeSelect, ProductLineName, remark, isGetcomID, compDataArr, VolumeSum, TheoryVolume, WastageRate, ActualVolume, compId } = this.state

    if (isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Toast ref={(ref) => { this.toast = ref; }} position='center' /* positionValue={300} */ />
          <NavigationEvents
            onWillFocus={this.UpdateControl} />
          <ScrollView ref={ref => this.scoll = ref} style={styles.mainView}showsVerticalScrollIndicator={false} >
            <View style={styles.listView}>
              <ListItemScan
                title='表单编号'
                rightTitle={formCode}
              />
              <ListItemScan
                focusStyle={focusIndex == '3' ? styles.focusColor : {}}
                title='叫料日期'
                rightElement={this._DatePicker()}
              />
              <ListItemScan
                title='叫料人'
                rightElement={Url.PDAEmployeeName}
              />
              <ListItemScan
                focusStyle={focusIndex == '4' ? styles.focusColor : {}}
                title='备注'
                rightElement={
                  <Input
                    containerStyle={styles.quality_input_container}
                    inputContainerStyle={styles.inputContainerStyle}
                    inputStyle={[styles.quality_input_, { top: 2 }]}
                    placeholder='请输入'
                    value={remark}
                    //onFocus={() => { this.setState({ focusIndex: 4 }) }}
                    onChangeText={(value) => {
                      this.setState({
                        remark: value
                      })
                    }} />
                }
              />
              <TouchableCustom underlayColor={'lightgray'} onPress={() => { this.isBottomVisible(ConcreteTypes, '1') }} >
                <ListItemScan
                  focusStyle={focusIndex == '1' ? styles.focusColor : {}}
                  title='混凝土类别'
                  rightElement={
                    <IconDown text={DispContentSelect} />
                  }
                />
              </TouchableCustom>
              <TouchableCustom underlayColor={'lightgray'} onPress={() => { this.isBottomVisible(ConcreteGrades, '2') }} >
                <ListItemScan
                  focusStyle={focusIndex == '2' ? styles.focusColor : {}}
                  title='混凝土强度'
                  rightElement={
                    <TouchableCustom onPress={() => {
                      this.setState({ bottomVisible: true, bottomData: ConcreteGrades, focusIndex: '2' })
                    }} >
                      <IconDown text={ConcreteGradeSelect} />
                    </TouchableCustom>
                  }
                />
              </TouchableCustom>

              <ListItemScan
                title='生产线'
                rightTitle={ProductLineName}
              />
              <ListItemScan
                title='理论方量'
                rightTitle={TheoryVolume.toString()}
              />
              <ListItemScan
                focusStyle={focusIndex == '6' ? styles.focusColor : {}}
                title='搅拌损耗率(%)'
                rightElement={
                  <Input
                    ref={ref => { this.input0 = ref }}
                    containerStyle={styles.quality_input_container}
                    inputContainerStyle={styles.inputContainerStyle}
                    inputStyle={[styles.quality_input_, { top: 2 }]}
                    placeholder={WastageRate.toString()}
                    placeholderTextColor={"#333"}
                    defaultValue={WastageRate.toString()}
                    value={WastageRate.toString()}
                    //onFocus={() => { this.setState({ focusIndex: 4 }) }}
                    onChangeText={(value) => {
                      value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
                      value = value.replace(/^\./g, ""); //验证第一个字符是数字
                      value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
                      value = value.replace(".", "$#$").replace(/\./g, "").replace("$#$", ".");
                      value = value.replace(/^(\-)*(\d+)\.(\d\d).*$/, '$1$2.$3'); //只能输入两个小数
                      if (value > 100) {
                        this.toast.show('损耗率异常')
                        this.setState({
                          ActualVolume: '',
                          WastageRate: ''
                        });
                        this.input0.blur()
                        return
                      }
                      let ActualVolume1 = 0;
                      ActualVolume1 =
                        Number(TheoryVolume) +
                        Number(TheoryVolume * value * 0.01);
                      ActualVolume1 = ActualVolume1.toFixed(2);
                      this.setState({
                        WastageRate: value,
                        ActualVolume: ActualVolume1.toString()
                      });
                    }} />
                }
              />
              <ListItemScan
                focusStyle={focusIndex == '7' ? styles.focusColor : {}}
                title='理论方量+损耗量'
                rightElement={
                  <Input
                    ref={ref => { this.input1 = ref }}
                    containerStyle={styles.quality_input_container}
                    inputContainerStyle={styles.inputContainerStyle}
                    inputStyle={[styles.quality_input_, { top: 2 }]}
                    placeholder={ActualVolume.toString()}
                    placeholderTextColor={"#333"}
                    defaultValue={ActualVolume.toString()}
                    value={ActualVolume.toString()}
                    //onFocus={() => { this.setState({ focusIndex: 4 }) }}
                    onChangeText={(value) => {
                      value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
                      value = value.replace(/^\./g, ""); //验证第一个字符是数字
                      value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
                      value = value.replace(".", "$#$").replace(/\./g, "").replace("$#$", ".");
                      value = value.replace(/^(\-)*(\d+)\.(\d\d).*$/, '$1$2.$3'); //只能输入两个小数
                      let WastageRate1 = (value - TheoryVolume) / (0.01 * TheoryVolume);
                      if (WastageRate1 > 100) {
                        this.toast.show('损耗率异常')
                        this.setState({
                          ActualVolume: '',
                          WastageRate: ''
                        });
                        this.input1.blur()
                        return
                      }
                      this.setState({
                        ActualVolume: value,
                        WastageRate: WastageRate1.toFixed(2).toString()
                      });
                    }} />
                }
              />
              {
                isBatch ? this.batchListView() : <ListItemScan
                  isButton={true}
                  focusStyle={focusIndex == '5' ? styles.focusColor : {}}
                  onPressIn={() => {
                    this.setState({
                      type: 'compid',
                      focusIndex: 5
                    })
                    NativeModules.PDAScan.onScan();
                  }}
                  onPress={() => {
                    this.setState({
                      type: 'compid',
                      focusIndex: 5
                    })
                    NativeModules.PDAScan.onScan();
                  }}
                  onPressOut={() => {
                    NativeModules.PDAScan.offScan();
                  }}
                  title='叫料明细'
                  rightElement={
                    <View style={{ flexDirection: 'row' }}>
                      <View>
                        <Input
                          ref={ref => { this.input1 = ref }}
                          containerStyle={styles.scan_input_container}
                          inputContainerStyle={styles.scan_inputContainerStyle}
                          inputStyle={[styles.scan_input]}
                          placeholder='请输入'
                          keyboardType='numeric'
                          value={compId}
                          onChangeText={(value) => {
                            value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
                            value = value.replace(/^\./g, ""); //验证第一个字符是数字
                            value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
                            //value = value.replace(/\b(0+)/gi, ""); //清楚开头的0
                            this.setState({
                              compId: value
                            })
                          }}
                          onFocus={() => {
                            this.setState({
                              focusIndex: 5,
                              type: 'compid',
                            })
                          }}
                          onSubmitEditing={() => {
                            let inputComId = this.state.compId
                            console.log("🚀 ~ file: qualityinspection.js ~ line 468 ~ QualityInspection ~ inputComId", inputComId)
                            inputComId = inputComId.replace(/\b(0+)/gi, "")
                            this.GetcomID(inputComId)
                          }}
                        />
                      </View>
                      <ScanButton
                        onPress={() => {
                          this.setState({
                            isGetcomID: false,
                            focusIndex: 5
                          })
                          this.props.navigation.navigate('QRCode', {
                            type: 'compid',
                            page: 'Wantconcrete'
                          })
                        }}
                        onLongPress={() => {
                          this.setState({
                            type: 'compid',
                            focusIndex: 5
                          })
                          NativeModules.PDAScan.onScan();
                        }}
                        onPressOut={() => {
                          NativeModules.PDAScan.offScan();
                        }}
                      />
                    </View>
                  }
                />
              }

              {
                compDataArr.length != 0 ? this.CardList(compDataArr) : <View></View>
              }
            </View>


            {this.state.isCheckPage ? <View></View> :
              <FormButton
                onPress={this.PostData}
                title='保存'
                backgroundColor='#17BC29'
              />
            }

            {this.BottomList()}

          </ScrollView>

        </View>
      )
    }

  }

}

