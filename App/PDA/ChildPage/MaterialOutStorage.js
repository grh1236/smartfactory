import React from 'react';
import { View, StyleSheet, TouchableHighlight, Dimensions, ActivityIndicator, Alert, Image } from 'react-native';
import { Button, Text, Input, Icon, Card, ListItem, BottomSheet, Overlay, Header } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { ScrollView } from 'react-native-gesture-handler';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { NavigationEvents } from 'react-navigation';
import { styles } from '../Componment/PDAStyles';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import CheckBoxScan from '../Componment/CheckBoxScan';
import AvatarImg from '../Componment/AvatarImg';
import DatePicker from 'react-native-datepicker'
import ScanButton from '../Componment/ScanButton';

import { DeviceEventEmitter, NativeModules } from 'react-native';
import overlayImg from '../Componment/OverlayImg';
import OverlayImgZoomPicker from '../Componment/OverlayImgZoomPicker';
class BackIcon extends React.PureComponent {
    render() {
        return (
            <TouchableCustom
                onPress={this.props.onPress} >
                <Icon
                    name='arrow-back'
                    color='#419FFF' />
            </TouchableCustom>
        )
    }
}

let fileflag = "19"

let imageArr = [], imageFileArr = [];

let QRidArr = []

const { height, width } = Dimensions.get('window') //获取宽高

export default class MaterialOutStorage extends React.PureComponent {
    constructor() {
        super();
        this.state = {
            title: "",
            resData: [],
            type: 'compid',
            rowguid: '',
            formNo: '', // 表单编号
            supplyNo: '请选择', // 领料单号
            supplyNoId: '',
            reqtype: '', // 领料类型
            reqtype2: '',// 领料类型
            outType: '', // 出库类型
            reqtypeName: '车间领料', // 领料类型
            outStorageDate: '', // 出库日期
            shop: '', // 车间
            shopId: '',
            department: '', // 部门
            departmentId: '',
            team: '', // 班组
            teamId: '',
            supplyPerson: '', // 领料人
            supplyPersonId: '',
            supplyUse: '', // 领料用途
            totalAmount: '', // 合计金额
            tabulator: '', // 制表人
            remark: '', // 备注,
            projectName: '',
            projectId: '',
            room: '',
            roomCode: '',
            roomId: '',

            materialInfos: [],// 材料明细
            //materialIds: '', // 已选材料id
            department: '', // 领料部门
            departmentId: '',

            // 选择确认
            //projectNameSelect: false,
            //roomSelect: false,
            //shopSelect: false,
            //teamSelect: false,
            //supplyUseSelect: false,

            supplyNoList: [],


            //底栏控制
            bottomData: [],
            supplyNoVisible: false,

            checked: true,

            focusIndex: 7,
            isLoading: false,

            imageArr: [],
            imageFileArr: [],
            pictureSelect: false,
            pictureUri: '',
            fileid: "",
            recid: "",
            isPhotoUpdate: false,
            imageOverSize: false,
            uriShow: '',

            //查询界面取消选择按钮
            isCheckPage: false,
            isCheckPageEdit: false,
            //控制app拍照，日期，相册选择功能
            isAppPhotoSetting: false,
            currShowImgIndex: 0,
            isAppDateSetting: false,
            isAppPhotoAlbumSetting: false
        }
    }

    componentDidMount() {
        const { navigation } = this.props;
        let guid = navigation.getParam('guid') || ''
        let pageType = navigation.getParam('pageType') || ''
        let title = navigation.getParam('title')
        let isAppPhotoSetting = navigation.getParam('isAppPhotoSetting')
        let isAppDateSetting = navigation.getParam('isAppDateSetting')
        let isAppPhotoAlbumSetting = navigation.getParam('isAppPhotoAlbumSetting')
        this.setState({
            isAppPhotoSetting: isAppPhotoSetting,
            isAppDateSetting: isAppDateSetting,
            isAppPhotoAlbumSetting: isAppPhotoAlbumSetting
        })
        if (pageType == 'CheckPage') {
            let guid = navigation.getParam('guid') || ''
            this.checkResData(guid)
        } else {
            this.resData()
        }
        //通过使用DeviceEventEmitter模块来监听事件
        this.iDataScan = DeviceEventEmitter.addListener('iDataScan', (Event) => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 115 ~ SteelCage ~ Event.ScanResult", Event.ScanResult)
            if (typeof Event.ScanResult != undefined) {
                let data = Event.ScanResult
                let arr = data.split("=");
                let id = ''
                id = arr[arr.length - 1]
                console.log("🚀 ~ file: concrete_pouring.js ~ line 118 ~ SteelCage ~ id", id)
                if (this.state.type == 'compid') {
                    this.scanMaterial(id)
                }

            }
        });
    }

    componentWillUnmount() {
        imageArr = [], imageFileArr = [];
        QRidArr = []
        this.iDataScan.remove()
    }

    UpdateControl = () => {
        const { navigation } = this.props;
        const { } = this.state

        let type = navigation.getParam('type') || '';
        if (type == 'compid') {
            let QRid = navigation.getParam('QRid') || ''
            this.scanMaterial(QRid)
        }

        let mis = []
        mis = navigation.getParam('materialInfos') || []
        let pickingInfoId = navigation.getParam('pickingInfoId') || ""
        let state = navigation.getParam('state') || ""
        if (state == "requisitionCount") {
            let requisitionCount = navigation.getParam('requisitionCount') || "0"
            mis = this.state.materialInfos
            mis.map((item, index) => {
                if (item.id == pickingInfoId) {
                    item.requisitionCount = requisitionCount
                    return
                }
            })
            this.setState({
                materialInfos: mis
            })
        } else if (state == "addMaterial") {
            this.state.materialInfos.map((item, index) => {
                mis.push(item)
            })
            this.setState({
                materialInfos: mis
            })
        }
        this.props.navigation.setParams({
            state: "",
            materialInfos: []
        })

        this.forceUpdate()
    }

    //顶栏
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('title')
        }
    }

    resData = () => {
        let formData = new FormData();
        let data = {};
        data = {
            "action": "outstorageinit",
            "servicetype": "pdamaterial",
            "express": "6DFD1C9B",
            "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
            "data": { "factoryId": Url.PDAFid }
        }
        formData.append('jsonParam', JSON.stringify(data))
        //console.log("🚀 ~ file: MaterialOutStorage.js ~ line 130 ~ MaterialOutStorage ~ formData:", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res.statusText);
            //console.log(res);
            return res.json();
        }).then(resData => {
            if (resData.status == '100') {

                //let dictionary = resData.result.defectTypes
                //console.log("MaterialOutStorage ---- 145 ----" + JSON.stringify(resData.result))
                this.setState({
                    //resData: resData.result,
                    rowguid: resData.result.rowguid,
                    formNo: resData.result.formNo,
                    supplyNoList: resData.result.supplyNo,
                    outStorageDate: resData.result.tabulationDate,
                    tabulator: Url.PDAEmployeeName,
                    isLoading: false
                })
            } else {
                Alert.alert("错误提示", resData.message, [{
                    text: '取消',
                    style: 'cancel'
                }, {
                    text: '返回上一级',
                    onPress: () => {
                        this.props.navigation.goBack()
                    }
                }])
            }

        }).catch((error) => {
            console.log("🚀 ~ file: MaterialOutStorage.js ~ line 233 ~ MaterialOutStorage ~ error:", error)
        });

    }

    checkResData = (guid) => {
        let formData = new FormData();
        let data = {};
        data = {
            "action": "outstoragequerydetail",
            "servicetype": "pdamaterial",
            "express": "6DFD1C9B",
            "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
            "data": {
                "factoryId": Url.PDAFid,
                "guid": guid
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: MaterialOutStorage.js ~ line 130 ~ MaterialOutStorage ~ formData:", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res.statusText);
            //console.log(res);
            return res.json();
        }).then(resData => {
            if (resData.status == '100') {
                console.log("MaterialProjectPicking ---- 267 ----" + JSON.stringify(resData.result))
                let fileAttach = resData.result.fileAttach
                if (fileAttach.length > 0) {
                    fileAttach.map((item, index) => {
                        let tmp = {}
                        tmp.fileid = ""
                        tmp.uri = item
                        tmp.url = item
                        imageFileArr.push(tmp)
                        imageArr.push(item)
                    })
                }
                this.setState({
                    imageFileArr: imageFileArr
                })
                this.setState({
                    //rowguid: resData.result.rowguid,
                    formNo: resData.result.formNo,
                    supplyNo: resData.result.supplyNo,
                    reqtypeName: resData.result.reqtype,
                    outStorageDate: resData.result.outStorageDate,
                    projectName: resData.result.projectName,
                    shop: resData.result.shop,
                    room: resData.result.room,
                    team: resData.result.team,
                    department: resData.result.department,
                    supplyUse: resData.result.supplyUse,
                    supplyPerson: resData.result.supplyPerson,
                    tabulator: resData.result.tabulator,
                    remark: resData.result.remark,
                    materialInfos: resData.result.materialInfo,
                    totalAmount: resData.result.totalAmount,
                    imageArr: resData.result.fileAttach,
                    pictureSelect: true,
                    //isCheckPage: resData.result.state == '在编' ? true : false,
                    //isCheckPageEdit: resData.result.state == '在编' ? true : false,
                    isCheckPage: true,
                    isLoading: false
                })
            } else {
                Alert.alert("错误提示", resData.message, [{
                    text: '取消',
                    style: 'cancel'
                }, {
                    text: '返回上一级',
                    onPress: () => {
                        this.props.navigation.goBack()
                    }
                }])
            }

        }).catch((error) => {
            console.log("🚀 ~ file: MaterialOutStorage.js ~ line 233 ~ MaterialOutStorage ~ error:", error)
        });
    }

    GetoutStorageInfo = (supplyNo) => {
        let formData = new FormData();
        let data = {};
        data = {
            "action": "getoutstorageinfo",
            "servicetype": "pdamaterial",
            "express": "6DFD1C9B",
            "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
            "data": {
                "factoryId": Url.PDAFid,
                "supplyNo": supplyNo
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        //console.log("🚀 ~ file: MaterialOutStorage.js ~ line 219 ~ MaterialOutStorage ~ formData:", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res.statusText);
            //console.log(res);
            return res.json();
        }).then(resData => {
            if (resData.status == '100') {
                //let dictionary = resData.result.defectTypes
                //console.log("MaterialOutStorage ---- 233 ----" + JSON.stringify(resData.result))
                let materialInfos = resData.result.materialInfo
                if (materialInfos != []) {
                    materialInfos.map((item, index) => {
                        item.isChecked = false
                    })
                }
                this.setState({
                    supplyNoId: resData.result.rowguid,
                    reqtypeName: resData.result.reqtypeName,
                    shop: resData.result.shop,
                    shopId: resData.result.shopId,
                    team: resData.result.team,
                    teamId: resData.result.teamId,
                    supplyPerson: resData.result.supplyPerson,
                    supplyPersonId: resData.result.supplyPersonId,
                    supplyUse: resData.result.supplyUse,
                    totalAmount: resData.result.totalAmount,
                    remark: resData.result.remark,
                    materialInfos: materialInfos,
                    department: resData.result.department,
                    departmentId: resData.result.departmentId,
                    projectName: resData.result.projectName,
                    projectId: resData.result.projectId,
                    room: resData.result.room,
                    roomCode: resData.result.roomCode,
                    roomId: resData.result.roomId,
                })
            } else {
                Alert.alert("错误提示", resData.message, [{
                    text: '取消',
                    style: 'cancel'
                }, {
                    text: '返回上一级',
                    onPress: () => {
                        this.props.navigation.goBack()
                    }
                }])
            }

        }).catch((error) => {
            console.log("🚀 ~ file: MaterialOutStorage.js ~ line 233 ~ MaterialOutStorage ~ error:", error)
        });

    }

    scanMaterial = (guid) => {
        if (typeof this.props.navigation.getParam('QRid') != "undefined") {
            if (this.props.navigation.getParam('QRid') != "") {
                this.toast.show(Url.isLoadingView, 0)
            }
        }
        let formData = new FormData();
        let data = {};
        if (QRidArr.indexOf(guid) != -1) {
            this.toast.show('材料重复')
            return
        }
        let materialIds = ''
        this.state.materialInfos.map((item, index) => {
            materialIds += index == this.state.materialInfos.length - 1 ? item.id : item.id + ','
        })
        data = {
            "action": "outstoragescan",
            "servicetype": "pdamaterial",
            "express": "6DFD1C9B",
            "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
            "data": {
                "rowguid": guid,
                "materialIds": materialIds
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: MaterialOutStorage.js ~ line 130 ~ MaterialOutStorage ~ formData:", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res.statusText);
            //console.log(res);
            return res.json();
        }).then(resData => {
            this.toast.close()
            if (resData.status == '100') {
                console.log("MaterialOutStorage ---- 267 ----" + JSON.stringify(resData.result))
                let mis = this.state.materialInfos
                let result = resData.result
                if (mis != []) {
                    result.map((item, index) => {
                        let check = true
                        mis.map((item2, index2) => {
                            if (item.id == item2.materialId) {
                                item2.isChecked = true
                                check = false
                                return
                            }
                        })
                        if (check) {
                            Alert.alert("提示", "领料单中不含此材料，请检查！", [{
                                text: '确定',
                                style: 'cancel'
                            }])
                        }
                    })
                }
                this.setState({
                    materialInfos: mis,
                }, () => {
                    this.forceUpdate()
                })
                QRidArr.push(guid)
            } else {
                Alert.alert("错误提示", resData.message)
            }

        }).catch((error) => {
            console.log("🚀 ~ file: MaterialOutStorage.js ~ line 233 ~ MaterialOutStorage ~ error:", error)
        });
    }

    PostData = () => {
        const { materialInfos, supplyNo, recid } = this.state
        let formData = new FormData();
        let data = {};

        let check = false
        if (supplyNo == '请选择') {
            this.toast.show('请选择领料单')
            return
        } else {
            materialInfos.map((item, index) => {
                if (item.isChecked == false) {
                    check = true
                    return
                }
            })
            if (check) {
                this.toast.show('领料单未完成，请检查！')
                return
            }
        }

        if (this.state.isAppPhotoSetting) {
            if (imageArr.length == 0) {
                this.toast.show('请先拍摄构件照片');
                return
            }
        }

        this.toast.show(saveLoading, 0)
        let MOutStorage = this.MOutStorageFormat()
        let MOutStorage_sub = this.MOutStorageSubFormat()

        data = {
            "action": "outstoragesave",
            "servicetype": "routematerial",
            "express": "6DFD1C9B",
            "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
            "userName": Url.PDAusername,
            "factoryId": Url.PDAFid,
            "data": {
                "MOutStorage": MOutStorage,
                "MOutStorage_sub": MOutStorage_sub,
                "MOutStorage_sub2": "",
                "DbActionType": "Add",
                "recid": recid
            },
        }
        if (Url.isAppNewUpload) {
            data.action = "outstoragesavenew"
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: MaterialOutStorage.js:560 ~ formData:", formData)
        if (!Url.isAppNewUpload) {
            imageArr.map((item, index) => {
                formData.append('img_' + index, {
                    uri: item,
                    type: 'image/jpeg',
                    name: 'img_' + index
                })
            })
        }
        fetch(Url.PDAurl, {
            method: 'post',
            headers: {
                'content-type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res);
            return res.json();
        }).then(resData => {
            //console.log("🚀 ~ file: MaterialProjectPicking.js ~ line 947 ~ MaterialProjectPicking ~ resdata", resData)
            if (resData.Success == true) {
                this.toast.show('保存成功');
                setTimeout(() => {
                    this.props.navigation.goBack()
                }, 1000)
            } else {
                this.toast.close()
                Alert.alert('保存失败', resData.ErrMsg)
                //console.log('resdata', resData)
            }
        }).catch((error) => {
            Alert.alert('保存失败')
            this.toast.close()
            console.log("error --- " + error)
        });
    }

    MOutStorageFormat = () => {
        const { rowguid, formNo, supplyNo, department, departmentId, shop, shopId, team, teamId, supplyUse, supplyPerson, supplyPersonId, tabulator, remark, totalAmount, outStorageDate, supplyNoId, projectName, projectId, room, roomCode, roomId, reqtype, reqtype2, outType } = this.state
        let MOutStorage = {}
        MOutStorage._rowguid = rowguid
        MOutStorage.formCode_YC = ''
        MOutStorage.formCode_WJ = ''
        MOutStorage.deptName = department
        MOutStorage.deptId = departmentId
        MOutStorage.teamName = team
        MOutStorage.teamId = teamId
        MOutStorage.reqEmployeeName = supplyPerson
        MOutStorage.reqEmployeeId = supplyPersonId
        MOutStorage.formCode_REQ = supplyNo
        MOutStorage.mUsage = supplyUse
        MOutStorage.roomName = room
        MOutStorage.roomCode = roomCode
        MOutStorage.roomId = roomId
        MOutStorage.projectName = projectName
        MOutStorage.projectId = projectId
        MOutStorage.editEmployeeName = tabulator
        MOutStorage.editEmployeeId = Url.PDAEmployeeId
        MOutStorage.editDate = outStorageDate
        MOutStorage.out_type = outType
        MOutStorage.state = '0'
        MOutStorage.factoryName = Url.PDAFname
        MOutStorage.factoryId = Url.PDAFid
        MOutStorage.reqRowGuid = supplyNoId
        MOutStorage.sumMoney = totalAmount
        MOutStorage.formCode = formNo
        MOutStorage.isCommit = '0' //待审
        MOutStorage.remark = remark
        MOutStorage.formCode_GJ = formNo
        MOutStorage.formCode_HNT = ''
        MOutStorage.formCode_SH = ''
        MOutStorage.formCode_PK = ''
        MOutStorage.reqType = reqtype
        MOutStorage.checkstate = '1' //待审
        MOutStorage.balanceGuid = ''
        MOutStorage.balanceFormCode = ''
        MOutStorage.mType = '0'
        MOutStorage.isClosed = '0'
        MOutStorage.workShopName = shop
        MOutStorage.workShopId = shopId
        MOutStorage.reqtype2 = reqtype2
        MOutStorage.appMsg = '请审核'
        MOutStorage.formCode_GJW = ''
        MOutStorage.accountCycleCode = '1'
        MOutStorage.isClosedToCurrentCycle = ''
        MOutStorage.ConcreteUse = ''
        MOutStorage.ConcreteGrade = ''
        MOutStorage.ConcreteVolume = '.000'
        MOutStorage.CompTypeName = ''
        MOutStorage.CompTypeId = ''
        MOutStorage.formCode_TC = ''
        MOutStorage.formCode_JE = ''

        return MOutStorage
    }

    MOutStorageSubFormat = () => {
        const { materialInfos } = this.state
        let MOutStorageSub = []
        materialInfos.map((item, index) => {
            let MOutStorageSubObj = {}
            MOutStorageSubObj._rowguid = this.getRandom()
            MOutStorageSubObj.mainid = '0'
            MOutStorageSubObj.materialCode = item.code
            MOutStorageSubObj.materialName = item.name
            MOutStorageSubObj.materialSize = item.size
            MOutStorageSubObj.materialUnit = item.unit
            MOutStorageSubObj.materialId = item.materialId
            MOutStorageSubObj.outNumber = item.requisitionCount
            MOutStorageSubObj.unitPrice = item.unitPrice
            MOutStorageSubObj.outMoney = item.requisitionAccount
            MOutStorageSubObj.storageGuid = item.id
            MOutStorageSubObj.state = '0'
            MOutStorageSubObj.materialType = item.materialType
            MOutStorageSubObj.materialTypeName = item.materialTypeName
            MOutStorageSubObj.roomId = ''
            MOutStorageSubObj.roomName = ''
            MOutStorageSubObj.projectId = ''
            MOutStorageSubObj.projectName = ''
            MOutStorageSubObj.remark = ''

            MOutStorageSub.push(MOutStorageSubObj)
        })
        return MOutStorageSub
    }

    getRandom = () => {
        var arr = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"],
            num = "";
        for (var i = 0; i < 32; i++) {
            num += arr[parseInt(Math.random() * 36)];
        }
        return num;
    }

    // 时间工具
    _DatePicker = (index) => {
        //const { ServerTime } = this.state
        return (
            <DatePicker
                customStyles={{
                    dateInput: styles.dateInput,
                    dateTouchBody: styles.dateTouchBody,
                    dateText: this.state.isAppDateSetting ? styles.dateText : styles.dateDisabled,
                }}
                iconComponent={<Icon name='caretdown' color={this.state.isAppDateSetting ? '#419fff' : "#666"} iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
                }
                showIcon={true}
                date={this.state.outStorageDate}
                onOpenModal={() => {
                    this.setState({ focusIndex: index })
                }}
                format="YYYY-MM-DD"
                mode="date"
                disabled={!this.state.isAppDateSetting}
                onDateChange={(value) => {
                    this.setState({
                        outStorageDate: value
                    })
                }}
            />
        )
    }

    uploadImage = () => {
        const { pictureSelect, isCheckPage, pageType } = this.state
        return (
            <View style={{ flexDirection: 'row' }}>
                {/*{this.imageView()}*/}
                {/*{*/}
                {/*    isCheckPage ? <View /> :*/}
                {/*        <AvatarAdd*/}
                {/*            pictureSelect={pictureSelect}*/}
                {/*            backgroundColor='#4D8EF5'*/}
                {/*            color='white'*/}
                {/*            title="材"*/}
                {/*            onPress={() => {*/}
                {/*                if (Url.isUsePhotograph == "YES") {*/}
                {/*                    if (pageType == 'CheckPage') {*/}
                {/*                        this.toast.show('照片不可修改')*/}
                {/*                    } else {*/}
                {/*                        this.camandlibFunc()*/}
                {/*                    }*/}
                {/*                } else {*/}
                {/*                    this.cameraFunc()*/}
                {/*                }*/}
                {/*            }} />*/}
                {/*}*/}
                {
                    !isCheckPage ? <Icon name='camera-alt' color='#4D8EF5' iconProps={{ size: 32 }} onPress={() => {
                        if (this.state.isAppPhotoAlbumSetting) {
                            this.camandlibFunc()
                        } else {
                            this.cameraFunc()
                        }
                    }}
                    ></Icon> : <View />

                }
            </View>
        )

    }

    camandlibFunc = () => {
        Alert.alert(
            '提示信息',
            '选择拍照方式',
            [{
                text: '取消',
                style: 'cancel'
            }, {
                text: '拍照',
                onPress: () => {
                    this.cameraFunc()
                }
            }, {
                text: '相册',
                onPress: () => {
                    this.camLibraryFunc()
                }
            }])
    }

    cameraFunc = () => {
        launchCamera(
            {
                mediaType: 'photo',
                includeBase64: false,
                maxHeight: parseInt(Url.isResizePhoto),
                maxWidth: parseInt(Url.isResizePhoto),
                saveToPhotos: true,

            },
            (response) => {
                if (response.uri == undefined) {
                    return
                }
                imageArr.push(response.uri)
                this.setState({
                    pictureSelect: true,
                    pictureUri: response.uri,
                    imageArr: imageArr
                })
                if (Url.isAppNewUpload) {
                    this.cameraPost(response.uri)
                } else {
                    this.setState({
                        pictureSelect: true,
                    })
                }
            },
        )
    }

    camLibraryFunc = () => {
        launchImageLibrary(
            {
                mediaType: 'photo',
                includeBase64: false,
                maxHeight: parseInt(Url.isResizePhoto),
                maxWidth: parseInt(Url.isResizePhoto),
            },
            (response) => {
                if (response.uri == undefined) {
                    return
                }
                imageArr.push(response.uri)
                this.setState({

                    pictureUri: response.uri,
                    imageArr: imageArr
                })
                if (Url.isAppNewUpload) {
                    this.cameraPost(response.uri)
                } else {
                    this.setState({
                        pictureSelect: true,
                    })
                }
            },
        )
    }

    // 展示/隐藏 放大图片
    handleZoomPicture = (flag, index) => {
        this.setState({
            imageOverSize: false,
            currShowImgIndex: index || 0
        })
    }

    // 加载放大图片弹窗
    renderZoomPicture = () => {
        const { imageOverSize, currShowImgIndex, imageFileArr } = this.state
        return (
            <OverlayImgZoomPicker
                isShowImage={imageOverSize}
                currShowImgIndex={currShowImgIndex}
                zoomImages={imageFileArr}
                callBack={(flag) => this.handleZoomPicture(flag)}
            ></OverlayImgZoomPicker>
        )
    }

    cameraPost = (uri) => {
        const { recid } = this.state
        this.setState({ isPhotoUpdate: true })
        this.toast.show('图片上传中', 2000)

        let formData = new FormData();
        let data = {};
        data = {
            "action": "savephote",
            "servicetype": "photo_file",
            "express": "D2979AB2",
            "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
            "data": {
                "fileflag": fileflag,
                "username": Url.PDAusername,
                "recid": recid
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        formData.append('img', {
            uri: uri,
            type: 'image/jpeg',
            name: 'img'
        })
        console.log("🚀 ~ file: concrete_pouring.js ~ line 672 ~ SteelCage ~ formData", formData)

        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            console.log(res.statusText);
            console.log(res);
            return res.json();
        }).then(resData => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 690 ~ SteelCage ~ resData", resData)
            if (resData.status == '100') {
                let fileid = resData.result.fileid;
                let recid = resData.result.recid;

                let tmp = {}
                tmp.fileid = fileid
                tmp.uri = uri
                tmp.url = uri
                imageFileArr.push(tmp)
                this.setState({
                    fileid: fileid,
                    recid: recid,
                    pictureSelect: true,
                    imageFileArr: imageFileArr
                })
                console.log("🚀 ~ file: concrete_pouring.js ~ line 704 ~ SteelCage ~ imageFileArr", imageFileArr)
                this.toast.show('图片上传成功');
                this.setState({
                    isPhotoUpdate: false
                })
            } else {
                Alert.alert('图片上传失败', resData.message)
                this.toast.close(1)
            }
        }).catch((error) => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 692 ~ SteelCage ~ error", error)
        });
    }

    cameraDelete = (item) => {
        let i = imageArr.indexOf(item)

        let formData = new FormData();
        let data = {};
        data = {
            "action": "deletephote",
            "servicetype": "photo_file",
            "express": "D2979AB2",
            "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
            "data": {
                "fileid": imageFileArr[i].fileid,
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: concrete_pouring.js ~ line 733 ~ SteelCage ~ cameraDelete ~ formData", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            console.log(res.statusText);
            console.log(res);
            return res.json();
        }).then(resData => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 745 ~ SteelCage ~ cameraDelete ~ resData", resData)
            if (resData.status == '100') {
                if (i > -1) {
                    imageArr.splice(i, 1)
                    imageFileArr.splice(i, 1)
                    this.setState({
                        imageArr: imageArr,
                        imageFileArr: imageFileArr
                    }, () => {
                        this.forceUpdate()
                    })
                }
                if (imageArr.length == 0) {
                    this.setState({
                        pictureSelect: false
                    })
                }
                this.toast.show('图片删除成功');
            } else {
                Alert.alert('图片删除失败', resData.message)
                this.toast.close(1)
            }
        }).catch((error) => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 761 ~ SteelCage ~ cameraDelete ~ error", error)
        });
    }

    imageView = () => {
        return (
            <View style={{ flexDirection: 'row' }}>
                {
                    this.state.pictureSelect ?
                        this.state.imageArr.map((item, index) => {
                            //if (index == 0) {
                            return (
                                <AvatarImg
                                    item={item}
                                    index={index}
                                    isRounded="false"
                                    marginRight="true"
                                    onPress={() => {
                                        this.setState({
                                            imageOverSize: true,
                                            uriShow: item
                                        })
                                    }}
                                    onLongPress={() => {
                                        Alert.alert(
                                            '提示信息',
                                            '是否删除材料照片',
                                            [{
                                                text: '取消',
                                                style: 'cancel'
                                            }, {
                                                text: '删除',
                                                onPress: () => {
                                                    this.cameraDelete(item)
                                                }
                                            }])
                                    }} />
                            )
                        }) : <View></View>
                }
            </View>
        )
    }

    // 下拉菜单
    downItem = (index) => {
        const { supplyNo, supplyNoList } = this.state
        switch (index) {
            case '2': return (
                // 领料单号
                <View>{(
                    <TouchableCustom onPress={() => { this.isItemVisible(supplyNoList, index) }} >
                        <IconDown text={supplyNo} />
                    </TouchableCustom>
                )}</View>
            ); break;
            default: return;
        }
    }

    isItemVisible = (data, focusIndex) => {
        //console.log("************** " + focusIndex + "-" + i)
        if (typeof (data) != "undefined") {
            if (data != [] && data != "") {
                switch (focusIndex) {
                    case '2': this.setState({ supplyNoVisible: true, bottomData: data, focusIndex: focusIndex }); break;
                    default: return;
                }
            } else {
                this.toast.show("无数据")
            }
        } else {
            this.toast.show("无数据")
        }
    }


    render() {
        const { navigation } = this.props;
        //从主页面传url, 未来集成到一起
        const QRurl = navigation.getParam('QRurl') || '';
        const QRid = navigation.getParam('QRid') || ''
        const type = navigation.getParam('type') || '';
        let pageType = navigation.getParam('pageType') || ''

        if (type == 'compid') {
            QRcompid = QRid
        }
        const { title, formNo, isCheckPage, focusIndex, supplyNo, outStorageDate, totalAmount, bottomData, shop, team, supplyPerson, tabulator, supplyUse, remark, materialInfos, supplyNoList, supplyNoVisible, reqtypeName, department } = this.state

        if (this.state.isLoading) {
            return (
                <View style={styles.loading}>
                    <ActivityIndicator
                        animating={true}
                        color='#419FFF'
                        size="large" />
                </View>
            )
            //渲染页面
        } else {
            return (
                <View style={{ flex: 1 }}>
                    <Toast ref={(ref) => { this.toast = ref; }} position="center" />
                    <NavigationEvents
                        onWillFocus={this.UpdateControl}
                        onWillBlur={() => {
                            if (QRid != '') {
                                navigation.setParams({ QRid: '', type: '' })
                            }
                        }} />
                    <ScrollView ref={ref => this.scoll = ref} style={styles.mainView} showsVerticalScrollIndicator={false} >
                        <View style={styles.listView}>
                            <ListItemScan focusStyle={focusIndex == '0' ? styles.focusColor : {}} title='表单编号:' rightElement={formNo} />
                            <ListItemScan focusStyle={focusIndex == '1' ? styles.focusColor : {}} title='出库日期:' rightElement={this._DatePicker(1)} />
                            {
                                isCheckPage ? <ListItemScan focusStyle={focusIndex == '2' ? styles.focusColor : {}} title='领料单号:' rightElement={supplyNo} /> :
                                    <TouchableCustom underlayColor={'lightgray'} onPress={() => { this.isItemVisible(supplyNoList, '2') }}>
                                        <ListItemScan focusStyle={focusIndex == '2' ? styles.focusColor : {}} title='领料单号:' rightElement={this.downItem('2')} />
                                    </TouchableCustom>
                            }
                            {
                                reqtypeName == '部门领料' ?
                                    <ListItemScan focusStyle={focusIndex == '3' ? styles.focusColor : {}} title='领用部门:' rightElement={department} />
                                    :
                                    <ListItemScan focusStyle={focusIndex == '3' ? styles.focusColor : {}} title='领用车间:' rightElement={shop} />
                            }

                            <ListItemScan focusStyle={focusIndex == '4' ? styles.focusColor : {}} title='领料班组:' rightElement={team} />
                            <ListItemScan focusStyle={focusIndex == '5' ? styles.focusColor : {}} title='领料人:' rightElement={supplyPerson} />
                            <ListItemScan focusStyle={focusIndex == '6' ? styles.focusColor : {}} title='领料用途:' rightElement={supplyUse} />
                            <ListItemScan
                                focusStyle={focusIndex == '7' ? styles.focusColor : {}}
                                title='扫描:'
                                onPressIn={() => {
                                    this.setState({
                                        type: 'compid',
                                        focusIndex: 7
                                    })
                                    NativeModules.PDAScan.onScan();
                                }}
                                onPress={() => {
                                    this.setState({
                                        type: 'compid',
                                        focusIndex: 7
                                    })
                                    NativeModules.PDAScan.onScan();
                                }}
                                onPressOut={() => {
                                    NativeModules.PDAScan.offScan();
                                }}
                                rightElement={
                                    isCheckPage ? <View /> :
                                        <ScanButton
                                            onPress={() => {
                                                this.setState({
                                                    type: 'compid',
                                                    focusIndex: 7
                                                })
                                                this.props.navigation.navigate('QRCode', {
                                                    type: 'compid',
                                                    page: 'MaterialOutStorage'
                                                })
                                            }}
                                            onLongPress={() => {
                                                this.setState({
                                                    type: 'compid',
                                                    focusIndex: 7
                                                })
                                                NativeModules.PDAScan.onScan();
                                            }}
                                            onPressOut={() => {
                                                NativeModules.PDAScan.offScan();
                                            }}
                                        />
                                }
                            />

                            {
                                materialInfos.map((item, index) => {
                                    return (
                                        <View>
                                            <Card containerStyle={stylesMeterial.card_containerStyle}>
                                                <View style={[{
                                                    flexDirection: 'row',
                                                    justifyContent: 'flex-start',
                                                    alignItems: 'center',
                                                }]}>
                                                    <View style={{ flex: 1 }}>
                                                        <ListItem
                                                            containerStyle={stylesMeterial.list_container_style}
                                                            title={item.code}
                                                            /*rightTitle={
                                                                <View style={{
                                                                    backgroundColor:
                                                                        item.type == '直接材料' ? '#31C731' :
                                                                            item.type == '小五金' ? '#DAC512' :
                                                                                item.type == '甲供材料' ? '#4D8EF5' : 'transparent'
                                                                }}>
                                                                    <Text style={{ color: 'white', fontSize: 13 }}> {item.type} </Text>
                                                                </View>
                                                            }*/
                                                            titleStyle={stylesMeterial.title}
                                                            rightTitleStyle={stylesMeterial.rightTitle}
                                                        />
                                                        <ListItem
                                                            containerStyle={stylesMeterial.list_container_style}
                                                            title={item.name}
                                                            //rightTitle={'单价:  2200' }
                                                            titleStyle={stylesMeterial.title}
                                                            rightTitleStyle={stylesMeterial.rightTitle}
                                                        />
                                                        <ListItem
                                                            containerStyle={stylesMeterial.list_container_style}
                                                            title={item.size}
                                                            //rightTitle={}
                                                            titleStyle={stylesMeterial.title}
                                                            ightTitleStyle={stylesMeterial.rightTitle}
                                                        />
                                                        <ListItem
                                                            containerStyle={stylesMeterial.list_container_style}
                                                            title={item.requisitionCount + "(" + item.unit + ")"}
                                                            //rightTitle={'是否暂估:  是'}
                                                            titleStyle={stylesMeterial.title}
                                                            rightTitleStyle={stylesMeterial.rightTitle}
                                                        />
                                                    </View>
                                                    {
                                                        isCheckPage ? <View /> :
                                                            <CheckBoxScan
                                                                checked={item.isChecked}
                                                                onPress={() => {

                                                                }}
                                                            />
                                                    }
                                                </View>
                                            </Card>
                                            <Text />
                                        </View>
                                    )
                                })
                            }

                            <ListItemScan focusStyle={focusIndex == '8' ? styles.focusColor : {}} title='合计金额:' rightElement={totalAmount} />
                            <ListItemScan focusStyle={focusIndex == '9' ? styles.focusColor : {}} title='制表人:' rightElement={tabulator} />
                            <ListItemScan focusStyle={focusIndex == '10' ? styles.focusColor : {}} title='备注:' rightElement={remark} />
                            <ListItemScan focusStyle={focusIndex == '11' ? styles.focusColor : {}} title='附件:' rightElement={() =>
                                this.uploadImage()} />
                            {
                                this.state.pictureSelect && this.state.imageArr != [] && this.state.imageArr != "" ?
                                    <ListItemScan focusStyle={focusIndex == '12' ? styles.focusColor : {}} title={this.imageView()} />
                                    : <View />
                            }
                        </View>
                        {isCheckPage ? <View /> :
                            <FormButton
                                backgroundColor='#17BC29'
                                onPress={() => {
                                    this.PostData()
                                }}
                                title='保存'
                                disabled={this.state.isPhotoUpdate}
                            />
                        }

                        {/*领料单号底栏*/}
                        <BottomSheet
                            onBackdropPress={() => {
                                this.setState({
                                    supplyNoVisible: false
                                })
                            }}
                            isVisible={supplyNoVisible && !this.state.isCheckPage} onRequestClose={() => {
                                this.setState({
                                    supplyNoVisible: false
                                })
                            }}
                        >
                            <ScrollView style={styles.bottomsheetScroll}>
                                {bottomData.map((item, index) => {
                                    return (
                                        <TouchableCustom
                                            onPress={() => {
                                                this.GetoutStorageInfo(item.code)
                                                this.setState({
                                                    supplyNo: item.code,
                                                    supplyNoVisible: false,
                                                    reqtype: item.reqtype,
                                                    reqtype2: item.reqtype2,
                                                    outType: item.outType
                                                })
                                            }}
                                        >
                                            <View style={{
                                                paddingVertical: 18,
                                                backgroundColor: 'white',
                                                //alignItems: 'center',
                                                borderBottomColor: '#DDDDDD',
                                                borderBottomWidth: 0.6,
                                                flexDirection: 'row',
                                                justifyContent: 'space-between',
                                                paddingLeft: 20,
                                                paddingRight: 20
                                            }}>
                                                <Text style={{ color: '#333', fontSize: 14 }}>{item.code}</Text>
                                                <Text style={{ color: '#333', fontSize: 14 }}>{item.company}</Text>
                                            </View>
                                        </TouchableCustom>
                                    )
                                })}
                            </ScrollView>
                            <TouchableHighlight
                                onPress={() => {
                                    this.setState({ supplyNoVisible: false })
                                }}>
                                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
                            </TouchableHighlight>
                        </BottomSheet>
                        {/*照片放大*/}
                        {this.renderZoomPicture()}
                        {/* {overlayImg(obj = {
                            onRequestClose: () => {
                                this.setState({ imageOverSize: !this.state.imageOverSize }, () => {
                                    console.log("🚀 ~ file: equipment_maintenance.js:1696 ~ render ~ imageOverSize:", this.state.imageOverSize)
                                })
                            },
                            onPress: () => {
                                this.setState({
                                    imageOverSize: !this.state.imageOverSize
                                })
                            },
                            uri: this.state.uriShow,
                            isVisible: this.state.imageOverSize
                        })
                        } */}
                    </ScrollView>
                </View>
            )
        }
    }
}

const stylesMeterial = StyleSheet.create({
    card_containerStyle: {
        borderRadius: 10,
        shadowOpacity: 0,
        backgroundColor: '#f8f8f8',
        borderWidth: 0,
        paddingVertical: 13,
        elevation: 0
    },
    title: {
        fontSize: 13,
        color: '#999'
    },
    rightTitle: {
        fontSize: 13,
        textAlign: 'right',
        lineHeight: 14,
        flexWrap: 'wrap',
        width: width / 2,
        color: '#333'
    },
    list_container_style: { marginVertical: 0, paddingVertical: 9, backgroundColor: 'transparent' }

})