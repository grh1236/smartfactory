import React, { Component } from 'react';
import { View, Text } from 'react-native';
import Url from '../../Url/Url';
import { Alert, TouchableHighlight, TouchableOpacity } from 'react-native';
import { ScrollView } from 'react-navigation';
import { Card } from 'react-native-elements';
import { styles } from '../Componment/PDAStyles';
import DeviceStorage from '../../Url/DeviceStorage';

import { NavigationActions } from 'react-navigation';

let loadingData = {}, compDataArr = [], compIdArr = [], compIdArrTmp = []

export default class loadingTemData extends Component {
  constructor(props) {
    super(props);
    this.state = {
      temData: [],
      isLoading: true
    };
  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: '暂存出库选择'
    }
  }

  componentDidMount() {
    this.props.navigation.setParams({ type: 'TmpData' })
    this.props.navigation.getParam('type')
    console.log("🚀 ~ file: loadingTemData.js:31 ~ loadingTemData ~ componentDidMount ~ this.props.navigation.getParam('type'):", this.props.navigation.getParam('type'))
    this.resData()
  }

  componentWillUnmount() {
    loadingData = {}, compDataArr = [], compIdArr = [], compIdArrTmp = []
  }

  resData = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "GetTemporaryloadOut",
      "servicetype": "pda",
      "express": "2066CAB8",
      "ciphertext": "36a65b6b6fd779dc9e0143147f65ae13",
      "data": { "factoryId": Url.PDAFid }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: loadingTemData.js:27 ~ loadingTemData ~ formData:", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: loadingTemData.js:40 ~ loadingTemData ~ resData:", resData)
      if (resData.status == 100) {
        let temData = resData.result.TeamInfo;

        this.setState({
          resData: resData,
          temData: temData
        })
        this.setState({
          isLoading: false,
        })
      } else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }

    }).catch((error) => {
      if (error.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('初始化失败', "响应内容不是合法JSON格式")
        return
      }
      if (error.toString().indexOf('Network request faile') != -1) {
        Alert.alert('初始化失败', "网络请求错误")
        return
      }
      Alert.alert('初始化失败', error.toString())
    });

  }

  render() {
    const { temData } = this.state
    return (
      <View style={{ flex: 1 }}>
        <ScrollView style={{ flex: 1 }}>
          {
            temData.map((item, index) => {
              return (
                <TouchableHighlight
                  underlayColor={'lightgray'}
                  onPress={() => {
                    console.log("🚀 ~ file: loadingTemData.js:99 ~ loadingTemData ~ item.compInfo.map ~ item:", item)

                    item.compInfo.map((itemComp, index) => {
                      compDataArr.push(itemComp)
                      let tmpObj = {}, tmpObjTmp = {}
                      tmpObj.compId = itemComp.compId_source
                      compIdArr.push(tmpObj)
                      tmpObjTmp.compId_source = itemComp.compId_source
                      tmpObjTmp.compId_target = itemComp.compId_target
                      compIdArrTmp.push(tmpObjTmp)
                    })
                    loadingData = {
                      "loadOutTime": item.loadOutTime,
                      "Bracket": item.Bracket,
                      "formCode": item.formCode,
                      "carNum": item.carNum,
                      "cartrains": item.cartrains,
                      "carBoardNum": item.carBoardNum,
                      "remark": item.remark,
                      "sweepnum": item.carBoardNum,
                      "compDataArr": compDataArr,
                      "compIdArr": compIdArr,
                      "compIdArrTmp": compIdArrTmp,
                      "truckModel": item.truckModel,
                      "customer": item.customer,
                      "companyid": item.companyid,
                      "companyname": item.carrier,
                      "carrier": item.carrier,
                      "driver": item.driver,
                      "driverTelephone": item.driverTelephone,
                      "arrivalTime": item.arrivalTime,
                      "WoodBeam": item.WoodBeam,
                      "planId": item.planId,
                      "planselect": item.planformCode,
                      "planformCode": item.planformCode,
                      "projectName": item.projectName,
                      "LoadCarMethod": item.loadCarMethod,
                      "rowguid": item.rowguid
                    }


                    DeviceStorage.save('loadingDataTmp', JSON.stringify(loadingData))
                    DeviceStorage.save('isLoadingDataTmpUse', true)

                    this.props.navigation.goBack();
                  }}>
                  <Card containerStyle={{ borderRadius: 10 }}>
                    <View style={{ flexDirection: 'row', }}>
                      <View style={[styles.SN_View]}>
                        <View style={[styles.SN_Text_View]}>
                          <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                        </View>
                      </View>
                      <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                        <View style={{ flexDirection: 'row', }}>
                          <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 1, }}>
                            <View style={{ flexDirection: 'row' }}>
                              <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.projectAbb}</Text>
                              <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.formCode}</Text>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                              <Text style={[styles.text]} numberOfLines={1}> {item.loadOutTime}</Text>
                              <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.userName}</Text>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                              <Text style={[styles.text, { flex: 1 }]} numberOfLines={1}> {'' + item.compNumber + '件(' + item.compvolume + 'm³)'}</Text>
                              <Text style={[styles.text, styles.textright]} numberOfLines={1}> {'' + item.compweight + '吨'}</Text>
                            </View>
                          </View>
                        </View>
                      </View>
                    </View>
                  </Card>
                </TouchableHighlight>
              )
            })
          }
        </ScrollView>
      </View >
    );
  }
}
