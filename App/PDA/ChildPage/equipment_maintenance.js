import React from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, Icon, Card, ListItem, BottomSheet, CheckBox, Avatar, Overlay, Header, Input } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { deviceWidth, RFT } from '../../Url/Pixal';
import { ScrollView } from 'react-native-gesture-handler';
import CardList from "../Componment/CardList";
import DatePicker from 'react-native-datepicker'
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { Image } from 'react-native';
import { NavigationEvents } from 'react-navigation';
import ScanButton from '../Componment/ScanButton';
import { styles } from '../Componment/PDAStyles';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import ListItemScan_child from '../Componment/ListItemScan_child';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import CheckBoxScan from '../Componment/CheckBoxScan';
import AvatarAdd from '../Componment/AvatarAdd';
import AvatarImg from '../Componment/AvatarImg';
import CheckBoxchild from '../Componment/CheckBoxchild';
import overlayImg from '../Componment/OverlayImg';
import OverlayImgZoomPicker from '../Componment/OverlayImgZoomPicker'; import DeviceStorage from '../../Url/DeviceStorage';

import { DeviceEventEmitter, NativeModules } from 'react-native';
import OverlayImgClass from '../Componment/OverlayImgClass';

let varGetError = false

const { height, width } = Dimensions.get('window') //获取宽高

let imageArr = [], imageFileArr = [], careIdStr = '', careIdArr = []

let fileflag = "4"

let DeviceStorageData = {}

class BackIcon extends React.PureComponent {
  render() {
    return (
      <TouchableOpacity
        onPress={this.props.onPress} >
        <Icon
          name='arrow-back'
          color='#419FFF' />
      </TouchableOpacity>
    )
  }
}


export default class EquipmentMaintenance extends React.PureComponent {
  constructor() {
    super();
    this.state = {
      resData: [],
      type: "compid",
      title: '',
      rowguid: '',
      ServerTime: '2000-01-01 00:00',
      person: [],
      perName: '',
      perNameselect: '请选择',
      perId: '',
      compData: {},
      formCode: '',
      editEmployeeName: Url.PDAEmployeeName,
      //设备编码
      equipguid: '',
      equipId: '',
      equipName: '',
      equipSize: '',
      workshopName: '',
      productLineName: '',
      equipTypeId: '',
      roomName: '请选择',
      roomId: '',
      room: [],
      outStorageCode: '',
      materialInfos: [],
      //保养周期
      equipCareCycle: [
        { careCycleId: '1', cycleName: '每日' },
        { careCycleId: '2', cycleName: '每周' },
        { careCycleId: '3', cycleName: '每月' },
        { careCycleId: '4', cycleName: '每季' },
        { careCycleId: '5', cycleName: '每年' },
        { careCycleId: '6', cycleName: '开机前' },
      ],
      careCycleId: '',
      careCycle: '',
      careCycleselect: '请选择',
      //设备
      equipArr: [],
      //保养内容
      careSubData: {},
      equipCareContent: [],
      careSubId: '',
      carecontent: '',
      carestandard: '',
      careIdArr: [],
      careIdStr: '',
      //是否扫码成功
      isGetcomID: false,
      //长按删除功能控制
      iscomIDdelete: false,
      //底栏控制
      bottomData: [],
      bottomVisible: false,
      datepickerVisible: false,
      CameraVisible: false,
      response: '',
      //照片控制
      pictureSelect: false,
      pictureUri: '',
      fileid: "",
      recid: "",
      isPhotoUpdate: false,
      imageArr: [],
      imageFileArr: [],
      imageOverSize: false,
      //
      GetError: false,
      buttondisable: false,
      //卡片
      isEquipVisible: false,
      isEquipCareContentVisible: false,
      remark: '',
      focusIndex: 1,
      isCheckPage: false,
      isLoading: true,
      isUsedSpareParts: '', //备品备件是否出库
      //控制app拍照，日期，相册选择功能
      isAppPhotoSetting: false,
      currShowImgIndex: 0,
      isAppDateSetting: false,
      isAppPhotoAlbumSetting: false
    }
    //this.requestCameraPermission = this.requestCameraPermission.bind(this)
  }

  componentDidMount() {
    const { navigation } = this.props;
    let guid = navigation.getParam('guid') || ''
    let pageType = navigation.getParam('pageType') || ''
    let isUsedSpareParts = Url.isUsedSpareParts || '0'
    let isAppPhotoSetting = navigation.getParam('isAppPhotoSetting')
    let isAppDateSetting = navigation.getParam('isAppDateSetting')
    let isAppPhotoAlbumSetting = navigation.getParam('isAppPhotoAlbumSetting')
    this.setState({
      isAppPhotoSetting: isAppPhotoSetting,
      isAppDateSetting: isAppDateSetting,
      isAppPhotoAlbumSetting: isAppPhotoAlbumSetting
    })
    this.setState({
      title: navigation.getParam('title'),
      isUsedSpareParts: isUsedSpareParts
    })
    if (pageType == 'CheckPage') {
      this.checkResData(guid)
      this.setState({ isCheckPage: true })
    } else {
      this.resData()
    }

    //通过使用DeviceEventEmitter模块来监听事件
    this.iDataScan = DeviceEventEmitter.addListener('iDataScan', (Event) => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 115 ~ SteelCage ~ Event.ScanResult", Event.ScanResult)
      if (typeof Event.ScanResult != undefined) {
        let data = Event.ScanResult
        let arr = data.split("=");
        let id = ''
        id = arr[arr.length - 1]
        console.log("🚀 ~ file: concrete_pouring.js ~ line 118 ~ SteelCage ~ id", id)

        if (this.state.type == 'compid') {
          this.GetcomID(id)
        } else if (this.state.type == 'compid2') {
          this.scanMaterial(id)
        }

      }
    });
  }

  componentWillUnmount() {
    imageArr = [], imageFileArr = [], careIdStr = '', careIdArr = []
    this.iDataScan.remove()
  }

  UpdateControl = () => {
    if (typeof this.props.navigation.getParam('QRid') != "undefined") {
      if (this.props.navigation.getParam('QRid') != "") {
        this.toast.show(Url.isLoadingView, 0)
      }
    }
    const { navigation } = this.props;
    const { isGetcomID } = this.state
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || ''
    let type = navigation.getParam('type') || '';

    navigation.setParams({ QRid: '', type: '' })

    if (type == 'compid') {
      this.GetcomID(QRid)
    } else if (type == 'compid2') {
      this.scanMaterial(QRid)
    }

    let state = navigation.getParam('state') || ""
    let mis = []
    mis = navigation.getParam('materialInfos') || []
    if (state == "addMaterial") {
      this.state.materialInfos.map((item, index) => {
        mis.push(item)
      })
      this.setState({
        materialInfos: mis
      })
    } else if (state == "requisitionCount") {
      let pickingInfoId = navigation.getParam('pickingInfoId') || ""
      let requisitionCount = navigation.getParam('requisitionCount') || "0"
      let remark = navigation.getParam('remark') || ""

      mis = this.state.materialInfos
      mis.map((item, index) => {
        if (item.id == pickingInfoId) {
          item.requisitionCount = requisitionCount
          item.remark = remark
        }
      })
      this.setState({
        materialInfos: mis,
      })
    }
    this.props.navigation.setParams({
      type: "",
      state: "",
      materialInfos: []
    })


  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  checkResData = (guid) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "obtainEquipmentMaintance",
      "servicetype": "pda",
      "express": "8AC4C0B0",
      "ciphertext": "7df83f712027c63f147f6c2d4a43f08d",
      "data": {
        "guid": guid,
        "factoryId": Url.PDAFid,
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: equipment_maintenance.js ~ line 153 ~ EquipmentMaintenance ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: equipment_maintenance.js ~ line 178 ~ EquipmentMaintenance ~ resData", resData)

      let rowguid = resData.result.guid
      let formCode = resData.result.formCode
      let ServerTime = resData.result.careDate
      let editEmployeeName = resData.result.editEmployeeName
      let attachment = resData.result.attachment
      console.log("🚀 ~ file: equipment_maintenance.js:289 ~ attachment:", attachment)
      let equipguid = resData.result.equipguid
      let equipId = resData.result.equipId
      let perName = resData.result.perName
      let careCycle = resData.result.careCycle
      let remark = resData.result.remark
      let equipmentMaintanceSub = resData.result.equipmentMaintanceSub
      let equipmentMaintanceSub2 = resData.result.equipmentMaintanceSub2
      let room = typeof resData.result.roomName == "undefined" ? "" : []
      let roomName = typeof resData.result.roomName == "undefined" ? "" : resData.result.roomName
      let roomId = typeof resData.result.roomName == "undefined" ? "" : resData.result.roomId
      let outStorageCode = typeof resData.result.roomName == "undefined" ? "" : resData.result.outStorageCode

      careIdArr = [], careIdStr = ''
      equipmentMaintanceSub.map((item, index) => {
        if (item.careStandard !== undefined) {
          item.carestandard = item.careStandard
        }
        item.careSubId = item.careId
        if (item.state == "是") {
          careIdArr.push(item.careId)
          careIdStr = careIdArr.toString();
        }
      })
      if (typeof equipmentMaintanceSub2 != 'undefined') {
        this.setState({
          materialInfos: equipmentMaintanceSub2,
          roomName: roomName,
          roomId: roomId,
          outStorageCode: outStorageCode
        })
      }
      console.log("🚀 ~ file: equipment_maintenance.js ~ line 179 ~ EquipmentMaintenance ~ careIdArr", careIdArr)


      //图片数组
      let imageArr = [], imageFileArr = [];
      let tmp = {}
      tmp.fileid = ""
      tmp.uri = attachment
      tmp.url = attachment

      imageFileArr.push(tmp)
      this.setState({
        imageFileArr: imageFileArr
      })
      imageArr.push(attachment)
      console.log("🚀 ~ file: equipment_maintenance.js:333 ~ imageArr:", imageArr)

      //子表
      let compData = {}, result = {}
      compData.equipName = resData.result.equipName
      compData.equipSize = resData.result.equipSize
      compData.workshopName = resData.result.workshopName
      compData.productLineName = resData.result.productLineName
      this.setState({
        resData: resData,
        rowguid: rowguid,
        formCode: formCode,
        ServerTime: ServerTime,
        editEmployeeName: editEmployeeName,
        equipguid: equipguid,
        equipId: equipId,
        compData: compData,
        careCycle: careCycle,
        careCycleselect: careCycle,
        equipCareContent: equipmentMaintanceSub,
        careIdArr: careIdArr,
        careIdStr: careIdStr,
        remark: remark,
        perNameselect: perName,
        perName: perName,
        imageArr: imageArr,
        room: room,
        pictureSelect: true,
        isEquipVisible: true,
        isEquipCareContentVisible: true,
        isGetcomID: true,
      })
      this.setState({
        isLoading: false,
      })
    }).catch((error) => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 215 ~ QualityInspection ~ error", error)
    });
  }

  resData = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getTimeAndFormCodeForeQuipmaintance",
      "servicetype": "pda",
      "express": "8AC4C0B0",
      "ciphertext": "7df83f712027c63f147f6c2d4a43f08d",
      "data": {
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: equipment_maintenance.js:367 ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: equipment_maintenance.js:379 ~ resData", resData)
      //初始化错误提示
      if (resData.status == 100) {
        let ServerTime = resData.result.time
        let formCode = resData.result.formCode
        let person = resData.result.person
        let cycle = []
        if (typeof resData.result.cycle != "undefined") {
          cycle = resData.result.cycle
          this.setState({
            equipCareCycle: cycle,
          })
        }
        let room = typeof resData.result.room == "undefined" ? "" : resData.result.room
        this.setState({
          resData: resData,
          formCode: formCode,
          ServerTime: ServerTime,
          person: person,
          perId: Url.PDAEmployeeId,
          perName: Url.PDAEmployeeName,
          perNameselect: Url.PDAEmployeeName,
          room: room,
          isLoading: false,
        })

        DeviceStorage.get('DeviceStorageDataEquipment')
          .then(res => {
            if (res) {
              let DeviceStorageDataObj = JSON.parse(res)
              DeviceStorageData = DeviceStorageDataObj
              if (res.length != 0) {
                person.map((item, index) => {
                  if (DeviceStorageData.perId.indexOf(item.perId) != -1) {
                    this.setState({
                      "perId": DeviceStorageDataObj.perId,
                      "perNameselect": DeviceStorageDataObj.perNameselect,
                      "perName": DeviceStorageDataObj.perName,
                    })
                  }
                })
              }
            }
          }).catch(err => {
            console.log("🚀 ~ file: loading.js ~ line 131 ~ SteelCage ~ componentDidMount ~ err", err)
          })
      }
      else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }

    }).catch((error) => {
    });

  }

  Getequipmentinfoforequipcare = (careCycle) => {
    const { ServerTime } = this.state
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getequipmentinfoforequipcare",
      "servicetype": "pda",
      "express": "0F51EF23",
      "ciphertext": "a91d6dc5c5a20f2a35939b6e2d91a0ef",
      "data": {
        "cycle": careCycle,
        "factoryId": Url.PDAFid,
        "time": ServerTime
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: equipment_maintenance.js ~ line 315 ~ EquipmentMaintenance ~ resData", resData)

      this.toast.close()
      if (resData.status == '100') {
        let equipArr = resData.result
        this.setState({
          equipArr: equipArr
        })
      } else {
        Alert.alert('错误', resData.message)
        this.setState({
          equipArr: []
        })
      }

    })
  }

  GetcomID = (id) => {
    const { ServerTime, careCycle } = this.state
    let formData = new FormData();
    let data = {};
    if (careCycle.length == 0) {
      this.toast.show('请先选择保养周期')
      return
    }
    data = {
      "action": "getesinglequipmentinfoforequipcare",
      "servicetype": "pda",
      "express": "0F51EF23",
      "ciphertext": "a91d6dc5c5a20f2a35939b6e2d91a0ef",
      "data": {
        "time": ServerTime,
        "cycle": careCycle,
        "rowGuid": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: equipment_maintenance.js ~ line 335 ~ EquipmentMaintenance ~ formData", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        console.log("🚀 ~ file: equipment_maintenance.js ~ line 360 ~ EquipmentMaintenance ~ resData", resData)
        let compData = resData.result
        let equipguid = resData.result.equipguid
        let equipId = resData.result.equipId
        let equipName = resData.result.equipName
        let equipSize = resData.result.equipSize
        let workshopName = resData.result.workshopName
        let equipTypeId = resData.result.equipTypeId
        let cycleId = resData.result.cycleId
        let productLineName = resData.result.productLineName
        let equipCareCycle = resData.result.equipCareCycle
        let equipCareContent = resData.result.equipCareContent
        this.setState({
          compData: compData,
          equipguid: equipguid,
          equipId: equipId,
          equipName: equipName,
          equipSize: equipSize,
          workshopName: workshopName,
          productLineName: productLineName,
          careCycleId: cycleId,
          equipTypeId: equipTypeId,
          equipCareCycle: equipCareCycle,
          equipCareContent: equipCareContent,
          isEquipVisible: true,
          isEquipCareContentVisible: true,
          isGetcomID: true
        }, () => {
          this.forceUpdate
        })
      } else {
        console.log('resData', resData)
        Alert.alert('ERROR', resData.message)
      }

    }).catch(err => {
      console.log("🚀 ~ file: equipment_maintenance.js ~ line 400 ~ EquipmentMaintenance ~ err", err)
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  comIDItem = () => {
    const { isGetcomID, careCycle, buttondisable, equipId, equipArr, equipguid } = this.state
    return (
      <View>
        <View style={[{ flexDirection: 'row' }, (isGetcomID && deviceWidth <= 330) && { left: -30 }]}>
          <TouchableCustom
            style={{ height: 30 }}
            onPress={() => {
              if (careCycle.length == 0) {
                this.toast.show('请先选择保养周期')
                return
              } else if (equipArr.length == 0) {
                this.toast.show('设备信息为空')
                return
              } else {
                this.setState({ bottomVisible: true, bottomData: equipArr, focusIndex: 1 })
              }
            }} >
            <View style={{ marginRight: 16, top: 9 }}>
              <IconDown text={'请选择'} />
            </View>
          </TouchableCustom>
          {
            !isGetcomID ?
              <ScanButton
                onPress={() => {
                  this.setState({ focusIndex: 1, isGetStoreroom: false })
                  if (careCycle.length == 0) {
                    this.toast.show('请先选择保养周期')
                    return
                  } else {
                    this.props.navigation.navigate('QRCode', {
                      type: 'compid',
                      page: 'EquipmentMaintenance'
                    })
                  }
                }}
                onLongPress={() => {
                  this.setState({
                    type: 'compid',
                    focusIndex: 1
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}
              /> :
              <View style={{ top: 8 }}>
                <TouchableOpacity
                  style={{ height: 30 }}
                  onPress={() => {
                    console.log('onPress')
                  }}
                  onLongPress={() => {
                    console.log('onLongPress')
                    Alert.alert(
                      '提示信息',
                      '是否删除构件信息',
                      [{
                        text: '取消',
                        style: 'cancel'
                      }, {
                        text: '删除',
                        onPress: () => {
                          this.setState({
                            compData: [],
                            compCode: '',
                            compId: '',
                            iscomIDdelete: true,
                            isGetcomID: false,
                            isEquipVisible: false,
                            isEquipCareContentVisible: false
                          })
                        }
                      }])
                  }} >
                  <Text>{equipId}</Text>
                </TouchableOpacity>
              </View>
          }
        </View>
      </View>

    )
  }

  GetEquipcarecontent = (careCycleId, equipGuid) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getEquipcarecontent",
      "servicetype": "pda",
      "express": "0F51EF23",
      "ciphertext": "a91d6dc5c5a20f2a35939b6e2d91a0ef",
      "data": {
        "careCycleId": careCycleId,
        "factoryId": Url.PDAFid,
        "equipGuid": equipGuid,
        "careCycleName": this.state.careCycleselect
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: equipment_maintenance.js ~ line 303 ~ EquipmentMaintenance ~ JSON.stringify(data)", JSON.stringify(data))

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        let careSubData = resData.result
        let equipCareContent = resData.result.equipCareContent
        this.setState({
          careSubData: careSubData,
          equipCareContent: equipCareContent,
          isEquipCareContentVisible: true
        })
      } else {
        console.log('resData', resData)
        Alert.alert('ERROR', resData.message)
      }
    }).catch(err => {
    })
  }

  PostData = () => {
    const { ServerTime, perId, perName, equipguid, careIdStr, careCycleId, remark, materialInfos, roomName, roomId, careCycleselect, recid } = this.state
    const { navigation } = this.props;
    const QRid = navigation.getParam('QRid') || '';


    if (equipguid == '') {
      //this.toast.show(saveLoading, 0);
      this.toast.show('设备编码不能为空');
      return
    }
    if (careCycleId == '') {
      this.toast.show('请选择保养周期');
      return
    }
    if (perId == '') {
      this.toast.show('请选择保养人');
      return
    }
    if (this.state.isAppPhotoSetting) {
      if (imageArr.length == 0) {
        this.toast.show('请先拍摄检查照片');
        return
      }
    }


    let message = ''
    if (materialInfos.length > 0) {
      materialInfos.map((item, index) => {
        if (item.requisitionCount == '0' || item.requisitionCount == '') {
          message = "材料编码" + item.code + "，请选择申领数量"
          return
        }
      })
    }
    if (message != "") {
      this.toast.show(message)
      return
    }

    this.toast.show(saveLoading, 0)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "SaveEquipCareInfo",
      "servicetype": "pda",
      "express": "939B066B",
      "ciphertext": "fee35451796126acd3f799bf037992dc",
      "data": {
        "EquipGuid": equipguid,
        "perId": perId,
        "perName": perName,
        "editEmployeeId": Url.PDAEmployeeId,
        "careIdString": careIdStr,
        "careCycleId": careCycleId,
        "factoryId": Url.PDAFid,
        "careDate": ServerTime,
        "Remark": remark,
        "editEmployeeName": Url.PDAEmployeeName,
        "roomName": roomName,
        "roomId": roomId,
        "materialInfos": materialInfos,
        "careCycle": careCycleselect,
        "userName": Url.PDAusername,
        "recid": recid
      }
    }
    if (!Url.isAppNewUpload) data.data.editEmployeeName = Url.PDAusername

    formData.append('jsonParam', JSON.stringify(data))
    if (!Url.isAppNewUpload) {
      data.data.editEmployeeName = Url.PDAusername
      imageArr.map((item, index) => {
        formData.append('img_' + index, {
          uri: item,
          type: 'image/jpeg',
          name: 'img_' + index
        })
      })
    }
    console.log("🚀 ~ file: qualityinspection.js ~ line 793 ~ SteelCage ~ formData", formData)


    /* formData.append('action', JSON.stringify("SaveProduction"))
    formData.append('servicetype', JSON.stringify('pda'))
    formData.append('express', JSON.stringify('06C795EE'))
    formData.append('ciphertext', JSON.stringify('a1e3818d57d9bfc9437c29e7a558e32e'))
    formData.append('data', JSON.stringify(datatest))
    formData.append('jsonParam', JSON.stringify(data)) */

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('保存成功');
        setTimeout(() => {
          this.props.navigation.replace(this.props.navigation.getParam("page"), {
            title: this.props.navigation.getParam("title"),
            pageType: this.props.navigation.getParam("pageType"),
            page: this.props.navigation.getParam("page"),
            isAppPhotoSetting: this.props.navigation.getParam("isAppPhotoSetting"),
            isAppDateSetting: this.props.navigation.getParam("isAppDateSetting"),
            isAppPhotoAlbumSetting: this.props.navigation.getParam("isAppPhotoAlbumSetting"),
          })
          //this.props.navigation.navigate('PDAMainStack')
        }, 400)
      } else {
        this.toast.close()
        Alert.alert('保存失败', resData.message)
        console.log('resData', resData)
      }
    }).catch((error) => {
      if (error.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('保存失败', "响应内容不是合法JSON格式")
        return
      }
      if (error.toString().indexOf('Network request faile') != -1) {
        Alert.alert('保存失败', "网络请求错误")
        return
      }
      Alert.alert('保存失败', error.toString())
    });
  }

  ReviseData = () => {
    const { navigation } = this.props;
    let guid = navigation.getParam('guid') || ''
    let formData = new FormData();
    let data = {};
    if (this.state.careIdStr.length == 0) {
      this.toast.show('请至少选择一项保养内容');
      return
    }
    data = {
      "action": "reviseEquipcareInfo",
      "servicetype": "pda",
      "express": "933B507F",
      "ciphertext": "285dff505e4821bb0f46d785c40b5823",
      "data": {
        "guid": guid,
        "factoryId": Url.PDAFid,
        "careIdString": this.state.careIdStr
      }
    }

    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: equipment_maintenance.js ~ line 533 ~ EquipmentMaintenance ~ formData", formData)


    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('修改成功');
        this.props.navigation.navigate('MainCheckPage')
      } else {
        Alert.alert('修改失败', resData.message)
        console.log('resData', resData)
      }
    }).catch((error) => {
    });
  }

  cameraFunc = () => {
    launchCamera(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: parseInt(Url.isResizePhoto),
        maxWidth: parseInt(Url.isResizePhoto),
        saveToPhotos: true,

      },
      (response) => {
        if (response.uri == undefined) {
          return
        }
        imageArr.push(response.uri)
        this.setState({
          //pictureSelect: true,
          pictureUri: response.uri,
          imageArr: imageArr
        })
        if (Url.isAppNewUpload) {
          this.cameraPost(response.uri)
        } else {
          this.setState({
            pictureSelect: true,
          })
        }
        console.log(response)
      },
    )
  }

  camLibraryFunc = () => {
    launchImageLibrary(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: parseInt(Url.isResizePhoto),
        maxWidth: parseInt(Url.isResizePhoto),
      },
      (response) => {
        if (response.uri == undefined) {
          return
        }
        imageArr.push(response.uri)
        this.setState({
          //pictureSelect: true,
          pictureUri: response.uri,
          imageArr: imageArr
        })
        if (Url.isAppNewUpload) {
          this.cameraPost(response.uri)
        } else {
          this.setState({
            pictureSelect: true,
          })
        }
        console.log(response)
      },
    )
  }

  camandlibFunc = () => {
    Alert.alert(
      '提示信息',
      '选择拍照方式',
      [{
        text: '取消',
        style: 'cancel'
      }, {
        text: '拍照',
        onPress: () => {
          this.cameraFunc()
        }
      }, {
        text: '相册',
        onPress: () => {
          this.camLibraryFunc()
        }
      }])
  }

  // 展示/隐藏 放大图片
  handleZoomPicture = (flag, index) => {
    this.setState({
      imageOverSize: false,
      currShowImgIndex: index || 0
    })
  }

  // 加载放大图片弹窗
  renderZoomPicture = () => {
    const { imageOverSize, currShowImgIndex, imageFileArr } = this.state
    return (
      <OverlayImgZoomPicker
        isShowImage={imageOverSize}
        currShowImgIndex={currShowImgIndex}
        zoomImages={imageFileArr}
        callBack={(flag) => this.handleZoomPicture(flag)}
      ></OverlayImgZoomPicker>
    )
  }

  cameraPost = (uri) => {
    const { recid } = this.state
    this.setState({
      isPhotoUpdate: true
    })
    this.toast.show('图片上传中', 2000)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "savephote",
      "servicetype": "photo_file",
      "express": "D2979AB2",
      "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
      "data": {
        "fileflag": fileflag,
        "username": Url.PDAusername,
        "recid": recid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    formData.append('img', {
      uri: uri,
      type: 'image/jpeg',
      name: 'img'
    })
    console.log("🚀 ~ file: concrete_pouring.js ~ line 672 ~ SteelCage ~ formData", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 690 ~ SteelCage ~ resData", resData)
      this.toast.close()
      if (resData.status == '100') {
        let fileid = resData.result.fileid;
        let recid = resData.result.recid;

        let tmp = {}
        tmp.fileid = fileid
        tmp.uri = uri
        tmp.url = uri
        imageFileArr.push(tmp)
        this.setState({
          fileid: fileid,
          recid: recid,
          pictureSelect: true,
          imageFileArr: imageFileArr
        })
        console.log("🚀 ~ file: concrete_pouring.js ~ line 704 ~ SteelCage ~ imageFileArr", imageFileArr)
        this.toast.show('图片上传成功');
        this.setState({
          isPhotoUpdate: false
        })
      } else {
        Alert.alert('图片上传失败', resData.message)
        this.toast.close(1)
      }
    }).catch((error) => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 692 ~ SteelCage ~ error", error)
    });
  }

  cameraDelete = (item) => {
    let i = imageArr.indexOf(item)

    if (Url.isAppNewUpload) {
      let formData = new FormData();
      let data = {};
      data = {
        "action": "deletephote",
        "servicetype": "photo_file",
        "express": "D2979AB2",
        "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
        "data": {
          "fileid": imageFileArr[i].fileid,
        }
      }
      formData.append('jsonParam', JSON.stringify(data))
      fetch(Url.PDAurl, {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        body: formData
      }).then(res => {
        console.log(res.statusText);
        console.log(res);
        return res.json();
      }).then(resData => {
        if (resData.status == '100') {
          if (i > -1) {
            imageArr.splice(i, 1)
            imageFileArr.splice(i, 1)
            this.setState({
              imageArr: imageArr,
              imageFileArr: imageFileArr
            }, () => {
              this.forceUpdate()
            })
          }
          if (imageArr.length == 0) {
            this.setState({
              pictureSelect: false
            })
          }
          this.toast.show('图片删除成功');
        } else {
          Alert.alert('图片删除失败', resData.message)
          this.toast.close(1)
        }
      }).catch((error) => {
        console.log("🚀 ~ file: concrete_pouring.js ~ line 761 ~ SteelCage ~ cameraDelete ~ error", error)
      });
    } else {
      if (i > -1) {
        imageArr.splice(i, 1)
        this.setState({
          imageArr: imageArr,
        }, () => {
          this.forceUpdate()
        })
      }
      if (imageArr.length == 0) {
        this.setState({
          pictureSelect: false
        })
      }
    }
  }

  imageView = () => {
    return (
      <View style={{ flexDirection: 'row' }}>
        {
          this.state.pictureSelect ?
            this.state.imageArr.map((item, index) => {
              //if (index == 0) {
              return (
                <AvatarImg
                  item={item}
                  index={index}
                  onPress={() => {
                    this.setState({
                      imageOverSize: true,
                      pictureUri: item
                    })
                  }}
                  onLongPress={() => {
                    Alert.alert(
                      '提示信息',
                      '是否删除构件照片',
                      [{
                        text: '取消',
                        style: 'cancel'
                      }, {
                        text: '删除',
                        onPress: () => {
                          this.cameraDelete(item)
                        }
                      }])
                  }}
                />
              )
            }) : <View></View>
        }
      </View>
    )
  }

  _DatePicker = () => {
    const { ServerTime } = this.state
    return (
      <DatePicker
        customStyles={{
          dateInput: styles.dateInput,
          dateTouchBody: styles.dateTouchBody,
          dateText: this.state.isAppDateSetting ? styles.dateText : styles.dateDisabled,
        }}
        onOpenModal={() => {
          this.setState({ focusIndex: '3' })
        }}
        iconComponent={<Icon name='caretdown' color={this.state.isAppDateSetting ? '#419fff' : "#666"} iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
        }
        showIcon={true}
        mode='datetime'
        date={ServerTime}
        format="YYYY-MM-DD HH:mm"
        disabled={!this.state.isAppDateSetting}
        onDateChange={(value) => {
          this.setState({
            ServerTime: value
          })
        }}
      />
    )
  }

  CareContentList = ({ item, index }) => {
    let checkedObj = {}
    return (
      <View style={{ marginTop: 8 }}>
        <View style={{ width: width * 0.78, marginLeft: width * 0.05, backgroundColor: '#f8f8f8', borderColor: 'transparent', borderWidth: 0, borderRadius: 10 }}>
          <View style={{ paddingTop: 10, paddingBottom: 7, paddingHorizontal: 10 }}>
            <Text style={{ fontSize: 14, marginVertical: 3, paddingHorizontal: 6 }} >{item.carecontent || item.careContent}</Text>
            <Text style={{ fontSize: 14, marginVertical: 3, paddingHorizontal: 6, fontWeight: 'bold' }} >标准：{item.carestandard}</Text>
            <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
              <CheckBoxchild
                containerStyle={{ flex: 1 }}
                title='是'
                checked={this.state.careIdStr.indexOf(item.careSubId) != -1}
                onPress={() => {
                  if (careIdStr.indexOf(item.careSubId) == -1) {
                    careIdArr.push(item.careSubId)
                    careIdStr = careIdArr.toString();
                    this.setState({
                      careIdArr: careIdArr,
                      careIdStr: careIdStr
                    })
                  }
                }} />
              <CheckBoxchild
                containerStyle={{ flex: 1 }}
                title='否'
                checked={this.state.careIdStr.indexOf(item.careSubId) == -1}
                onPress={() => {
                  if (careIdStr.indexOf(item.careSubId) != -1) {
                    careIdArr.map((u, i) => {
                      if (u.indexOf(item.careSubId) != -1) {
                        careIdArr.splice(i, 1)
                        careIdStr = careIdArr.toString();
                        this.setState({
                          careIdArr: careIdArr,
                          careIdStr: careIdStr
                        })
                      }

                    })
                  }

                }} />
            </View>
          </View>

        </View>
      </View>

    )
  }

  isBottomVisible = (data, focusIndex) => {
    if (typeof (data) != "undefined") {
      if (data.length > 0) {
        this.setState({ bottomVisible: true, bottomData: data, focusIndex: focusIndex })
      }
    } else {
      this.toast.show("无数据")
    }
  }

  scanMaterial = (guid) => {
    const { projectId, roomId, materialInfos, title } = this.state
    let formData = new FormData();
    let data = {};
    let materialIds = ''
    materialInfos.map((item, index) => {
      materialIds += index == materialInfos.length - 1 ? item.id : item.id + ','
    })
    data = {
      "action": "selectequipmaterialscan",
      "servicetype": "pdamaterial",
      "express": "6DFD1C9B",
      "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
      "data": {
        "factoryId": Url.PDAFid,
        "rowguid": guid,
        "roomId": roomId,
        "materialIds": materialIds,
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: EquipmentMaintenance.js ~ line 1003 ~ EquipmentMaintenance ~ formData:", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      //console.log(res.statusText);
      //console.log(res);
      return res.json();
    }).then(resData => {
      console.log("EquipmentMaintenance ---- 1016 ---- resData:" + JSON.stringify(resData))
      this.toast.close()
      if (resData.status == '100') {
        let mis = materialInfos
        let materialIds = ''
        mis.map((item, index) => {
          materialIds += index == mis.length - 1 ? item.id : item.id + ','
        })
        let result = resData.result
        if (result.length > 1) {
          setTimeout(() => {
            this.props.navigation.navigate('SelectMaterial', {
              title: '选择备品备件',
              action: '设备保养',
              roomId: roomId,
              materialIds: materialIds,
              keyStr: result[0].code
            })
          }, 1000)
        } else {
          if (mis != []) {
            result.map((item, index) => {
              let check = true
              materialInfos.map((item2, index2) => {
                if (item.id == item2.id) {
                  check = false
                  return
                }
              })
              if (check) mis.push(item)
            })
          } else {
            mis = result
          }
          this.setState({
            materialInfos: mis,
          })
        }
      } else {
        Alert.alert("错误提示", resData.message)
      }

    }).catch((error) => {
      console.log("🚀 ~ file: EquipmentMaintenance.js ~ line 1068 ~ EquipmentMaintenance ~ error:", error)
    });
  }

  comIDItem2 = (index) => {
    const { roomId, materialInfos } = this.state
    return (
      <View>
        {
          <View style={{ flexDirection: 'row' }}>

            <Button
              titleStyle={{ fontSize: 14 }}
              title='选择材料'
              type='solid'
              buttonStyle={{ paddingVertical: 6, backgroundColor: '#18BC28', marginVertical: 0 }}
              onPress={() => {
                this.setState({
                  type: 'compid2',
                  focusIndex: index
                })
                let materialIds = ''
                materialInfos.map((item, index) => {
                  materialIds += index == materialInfos.length - 1 ? item.id : item.id + ','
                })
                this.props.navigation.navigate('SelectMaterial', {
                  title: '选择备品备件',
                  action: '设备保养',
                  roomId: roomId,
                  materialIds: materialIds
                })
              }}
            />
            <Text>   </Text>
            <ScanButton
              disabled={false}
              onPress={() => {
                this.setState({
                  type: 'compid2',
                  focusIndex: index
                })
                this.props.navigation.navigate('QRCode', {
                  type: 'compid2',
                  page: 'EquipmentMaintenance'
                })
              }}
              onLongPress={() => {
                this.setState({
                  type: 'compid2',
                  focusIndex: index
                })
                NativeModules.PDAScan.onScan();
              }}
              onPressOut={() => {
                NativeModules.PDAScan.offScan();
              }}
            />
          </View>
        }
      </View>
    )
  }

  jumpMaterialPickingInfo = (item) => {
    const { title } = this.state
    this.props.navigation.navigate('MaterialPickingInfo', {
      title: '设备保养',
      action: title,
      storageId: item.id,
      storageCode: item.code,
      storageName: item.name,
      size: item.size,
      unit: item.unit,
      stockCount2: item.stockCount,
      requisitionCount: item.requisitionCount,
      remark: item.remark
    })
  }

  render() {
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || ''
    const type = navigation.getParam('type') || '';

    if (type == 'compid') {
      QRcompid = QRid
    }
    const { buttondisable, isGetcomID, person, perNameselect, remark, focusIndex, isEquipVisible, compData, equipCareCycle, careCycleselect, isEquipCareContentVisible, equipCareContent, careCycle, careCycleId, isGetCommonMould, isGetInspector, formCode, bottomVisible, bottomData, isCheckPage, room, roomId, roomName, materialInfos, title, outStorageCode, isUsedSpareParts } = this.state

    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          <NavigationEvents
            onWillFocus={this.UpdateControl} />
          <ScrollView ref={ref => this.scoll = ref} style={styles.mainView} showsVerticalScrollIndicator={false} >
            <View style={styles.listView}>
              <ListItem
                containerStyle={{ paddingBottom: 6 }}
                leftAvatar={
                  <View >
                    <View style={{ flexDirection: 'column' }} >
                      {
                        isCheckPage ? <View></View> :
                          <AvatarAdd
                            pictureSelect={this.state.pictureSelect}
                            backgroundColor='rgba(77,142,245,0.20)'
                            color='#4D8EF5'
                            title="构"
                            onPress={() => {
                              if (this.state.isAppPhotoAlbumSetting) {
                                this.camandlibFunc()
                              } else {
                                this.cameraFunc()
                              }
                            }} />
                      }
                      {this.imageView()}
                    </View>
                  </View>
                }
              />
              <ListItemScan
                title='表单编号'
                rightTitle={formCode}
              />
              <TouchableCustom underlayColor={'lightgray'} onPress={() => {
                this.isBottomVisible(equipCareCycle, '2')
              }} >
                <ListItemScan
                  focusStyle={focusIndex == '2' ? styles.focusColor : {}}
                  title='保养周期'
                  rightElement={
                    <IconDown text={careCycleselect} />
                  }
                />
              </TouchableCustom>

              <ListItemScan
                isButton={true}
                focusStyle={focusIndex == '1' ? styles.focusColor : {}}
                title='设备编码'
                //rightTitleStyle = {{marginLeft: deviceWidth <= 330 ? 35 : 0}}
                onPressIn={() => {
                  this.setState({
                    type: 'compid',
                    focusIndex: 1
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPress={() => {
                  this.setState({
                    type: 'compid',
                    focusIndex: 1
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}
                rightElement={
                  this.comIDItem()
                }
              />
              {
                isEquipVisible ?
                  <EquipCardList EquipData={compData} /> : <View></View>
              }
              <ListItemScan
                focusStyle={focusIndex == '3' ? styles.focusColor : {}}
                title='保养日期'
                rightElement={
                  this._DatePicker()
                }
              //rightTitle={ServerTime}
              />

              <TouchableCustom underlayColor={'lightgray'} onPress={() => {
                this.isBottomVisible(person, '6')
              }} >
                <ListItemScan
                  focusStyle={focusIndex == '6' ? styles.focusColor : {}}
                  title='保养人'
                  rightElement={
                    <IconDown text={perNameselect} />
                  }
                />
              </TouchableCustom>

              <ListItemScan
                title='操作人'
                rightTitle={Url.PDAEmployeeName}
              />

              <ListItemScan
                focusStyle={focusIndex == '4' ? styles.focusColor : {}}
                title='保养内容'
                rightElement={
                  <IconDown text='请选择' />
                }
              />

              {
                isEquipCareContentVisible ?
                  <FlatList
                    data={equipCareContent}
                    renderItem={this.CareContentList}
                    extraData={this.state}
                    style={{ marginTop: 6, borderRadius: 10 }}
                  /> : <View></View>
              }

              {
                room ?
                  <TouchableCustom underlayColor={'lightgray'} onPress={() => {
                    this.isBottomVisible(room, '10')
                  }} >
                    <ListItemScan
                      focusStyle={focusIndex == '10' ? styles.focusColor : {}}
                      title='库房名称'
                      rightElement={
                        <IconDown text={roomName} />
                      }
                    />
                  </TouchableCustom>
                  : <View />
              }
              {
                outStorageCode && isCheckPage ?
                  <ListItemScan focusStyle={focusIndex == '11' ? styles.focusColor : {}} title='出库单号:' rightElement={outStorageCode} />
                  : <View />
              }
              {
                roomId && !isCheckPage ?
                  //<View style={{ flexDirection: 'row', paddingHorizontal: 10, justifyContent: 'center', paddingVertical: 11 }}>
                  //    <Button
                  //        titleStyle={{ fontSize: 14 }}
                  //        title='选择备品备件'
                  //        type='solid'
                  //        buttonStyle={{ paddingVertical: 6, backgroundColor: '#17BC29', marginVertical: 0 }}
                  //        onPress={() => {
                  //            let materialIds = ''
                  //            materialInfos.map((item, index) => {
                  //                materialIds += index == materialInfos.length - 1 ? item.id : item.id + ','
                  //            })
                  //            this.props.navigation.navigate('SelectMaterial', {
                  //                title: '选择备品备件',
                  //                action: '设备保养',
                  //                roomId: roomId,
                  //                materialIds: materialIds
                  //            })
                  //        }}
                  //    />
                  //</View>
                  <ListItemScan focusStyle={focusIndex == '12' ? styles.focusColor : {}} title='材料明细:' rightElement={isCheckPage ? <View /> :
                    <View>
                      {this.comIDItem2("12")}
                    </View>}
                  />
                  : <View />
              }
              {
                materialInfos.map((item, index) => {
                  return (
                    <TouchableCustom
                      onLongPress={() => {
                        isCheckPage ? <View /> : Alert.alert(
                          '提示信息',
                          '是否删除备品备件信息',
                          [{
                            text: '取消',
                            style: 'cancel'
                          }, {
                            text: '删除',
                            onPress: () => {
                              let mis = this.state.materialInfos
                              mis.splice(index, 1)
                              this.setState({
                                materialInfos: mis
                              })
                              this.forceUpdate()
                            }
                          }])
                      }}
                      onPress={() => { isCheckPage ? <View /> : this.jumpMaterialPickingInfo(item) }}
                    >
                      <View>
                        <Card containerStyle={styles.card_containerStyle}>
                          <View style={[{
                            flexDirection: 'row',
                            justifyContent: 'flex-start',
                            alignItems: 'center',
                          }]}>
                            <View style={{ flex: 1 }}>
                              <ListItem
                                containerStyle={styles.list_container_style}
                                title={item.code}
                                //rightTitle={}
                                titleStyle={styles.card_titleStyle}
                                rightTitleStyle={styles.rightTitle}
                              />
                              <ListItem
                                containerStyle={styles.list_container_style}
                                title={item.name}
                                //rightTitle={}
                                titleStyle={styles.card_titleStyle}
                                rightTitleStyle={styles.rightTitle}
                              />
                              <ListItem
                                containerStyle={styles.list_container_style}
                                title={item.size}
                                //rightTitle={}
                                titleStyle={styles.card_titleStyle}
                                rightTitleStyle={styles.rightTitle}
                              />
                              <ListItem
                                containerStyle={styles.list_container_style}
                                title={item.requisitionCount + "(" + item.unit + ")"}
                                //rightTitle={'税额:' + item.tax}
                                titleStyle={styles.card_titleStyle}
                                rightTitleStyle={styles.rightTitle}
                              />
                            </View>
                            {
                              isCheckPage ? <View /> :
                                <Icon type='antdesign' name='right' color='#999' onPress={() => { this.jumpMaterialPickingInfo(item) }} />
                            }
                          </View>
                        </Card>
                        <Text />
                      </View>
                    </TouchableCustom>
                  )
                })
              }

              <ListItemScan
                containerStyle={focusIndex == '5' ? { backgroundColor: '#A9E2F3' } : {}}
                bottomDivider
                title='备注'
                rightElement={
                  <View>
                    <Input
                      containerStyle={styles.quality_input_container}
                      inputContainerStyle={styles.inputContainerStyle}
                      inputStyle={[styles.quality_input_, { top: 7 }]}
                      placeholder='请输入'
                      value={remark}
                      //onFocus={() => { this.setState({ focusIndex: '5' }) }}
                      onChangeText={(value) => {
                        this.setState({
                          remark: value
                        })
                      }} />
                  </View>
                }
              />

            </View>

            <FormButton
              onPress={isCheckPage ? this.ReviseData : this.PostData}
              title={isCheckPage ? '修改' : '保存'}
              backgroundColor={isCheckPage ? '#EB5D20' : '#17BC29'}
              disabled={this.state.isPhotoUpdate}
            />


            <OverlayImgClass
              onRequestClose={() => {
                this.setState({ imageOverSize: !this.state.imageOverSize }, () => {
                  console.log("🚀 ~ file: equipment_maintenance.js:1696 ~ render ~ imageOverSize:", this.state.imageOverSize)
                })
              }}
              onPress={() => {
                this.setState({
                  imageOverSize: !this.state.imageOverSize
                })
              }}
              uri={this.state.pictureUri}
              isVisible={this.state.imageOverSize} />

            {this.renderZoomPicture()}

            <BottomSheet
              isVisible={bottomVisible && !isCheckPage}
              onRequestClose={() => {
                this.setState({
                  bottomVisible: false
                })
              }}
              onBackdropPress={() => {
                this.setState({
                  bottomVisible: false
                })
              }}
            >
              <ScrollView style={styles.bottomsheetScroll}>
                {
                  bottomData.map((item, index) => {
                    let title = ''
                    if (item.cycleName) {
                      title = item.cycleName
                    } else if (item.equipId) {
                      title = item.equipName + ' ' + item.equipId + ' ' + item.equipSize
                    } else if (item.perId) {
                      title = item.perName
                    } else if (item.roomId) {
                      title = item.roomName
                    }
                    return (
                      <TouchableOpacity
                        onPress={() => {
                          if (item.cycleName) {
                            this.setState({
                              careCycleselect: item.cycleName,
                              careCycle: item.cycleName,
                              focusIndex: '2'
                            })
                            this.Getequipmentinfoforequipcare(item.cycleName)
                          } else if (item.InspectorId) {
                            this.setState({
                              InspectorId: item.InspectorId,
                              InspectorName: item.InspectorName,
                              InspectorSelect: item.InspectorName,
                            })
                          } else if (item.equipId) {
                            this.setState({
                              equipId: item.equipId,
                              equipguid: item.equipguid,
                              compData: item,
                              careCycleId: item.cycleId,
                              isEquipVisible: true,
                              isGetcomID: true
                            })
                            this.GetEquipcarecontent(item.cycleId, item.equipguid)
                          } else if (item.perId) {

                            this.setState({
                              perId: item.perId,
                              perNameselect: item.perName,
                              perName: item.perName,
                              focusIndex: '6'
                            })
                            DeviceStorageData = {
                              "perId": item.perId,
                              "perNameselect": item.perName,
                              "perName": item.perName,
                            }
                            DeviceStorage.save('DeviceStorageDataEquipment', JSON.stringify(DeviceStorageData))

                          } else if (item.roomId) {
                            this.setState({
                              roomId: item.roomId,
                              roomName: item.roomName,
                              materialInfos: []
                            })
                          }
                          this.setState({
                            bottomVisible: false
                          })
                        }}
                      >
                        <BottomItem backgroundColor='white' color="#333" title={title} />
                      </TouchableOpacity>
                    )

                  })
                }
              </ScrollView>
              <TouchableCustom
                onPress={() => {
                  this.setState({ bottomVisible: false })
                }}>
                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
              </TouchableCustom>
            </BottomSheet>
          </ScrollView>
        </View>
      )
    }
  }

}

class EquipCardList extends React.Component {
  render() {
    const { EquipData } = this.props
    return (
      <View>
        <Card containerStyle={styles.card_containerStyle}>
          <ListItemScan_child
            title='设备名称'
            rightTitle={EquipData.equipName}
          />
          <ListItemScan_child
            title='设备型号'
            rightTitle={EquipData.equipSize}
          />
          <ListItemScan_child
            title='所属车间'
            rightTitle={EquipData.workshopName}
          />
          <ListItemScan_child
            title='生产线'
            rightTitle={EquipData.productLineName}
          />
        </Card>
      </View>
    )
  }
}

