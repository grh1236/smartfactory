import React from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, Icon, Card, ListItem, BottomSheet, CheckBox, Input, Overlay, Header } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { RFT } from '../../Url/Pixal';
import { ScrollView } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker'
import { NavigationEvents } from 'react-navigation';
import ScanButton from '../Componment/ScanButton';
import { styles } from '../Componment/PDAStyles';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import ListItemScan_child from '../Componment/ListItemScan_child';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import CheckBoxScan from '../Componment/CheckBoxScan';
import AvatarAdd from '../Componment/AvatarAdd';
import AvatarImg from '../Componment/AvatarImg';

import { DeviceEventEmitter, NativeModules } from 'react-native';


const { height, width } = Dimensions.get('window') //获取宽高

let varGetError = false

let compDataArr = [], compIdArr = [], QRid2 = '', tmpstr = '', weightAccount = 0, visibleArr = [];

export default class SteelCage extends React.Component {
  constructor() {
    super();
    this.state = {
      bottomData: [1, 2, 3, 4, 5, 6, 7, 8, 9, 9],
      bottomVisible: false,
      buttomTitle: "",
      resData: [],
      formCodeSetup: "",
      formCode: "",
      type: 'compid',
      rowguid: "",
      ServerTime: "",
      loadOutTime: "",
      arrivalTime: "",
      //发货单号
      plans: [],
      planselect: "请选择",
      planId: "",
      planprojectName: "",
      isPlanVisible: false,
      planprojectName: "",
      PlanData: {},
      carrier: "",
      customer: "",
      cartrains: "",
      receiver: "",
      receiverPhone: "",
      formCode_jh: "",
      projectName: "",
      //发货引导
      deliveryguide: [],
      isDeliveryGuideVisible: false,
      //车辆型号
      truckModels: [],
      truckModel: "",
      truckModelselect: "",
      //
      loadCarMethods: [],
      loadCarMethod: "",
      //司机
      drivers: [],
      driver: "",
      driverselect: "",
      //车牌号
      carnums: [],
      carnum: "",
      carnumselect: "",
      //车板号
      sweepnums: [],
      sweepnum: "",
      sweepselect: "",
      //承运单位
      SupplierInfo: [],
      companyid: "",
      companyname: "",
      companyselct: "请选择",
      //产品编号
      compId: "",
      compCode: "",
      compData: [],
      compDataArr: [],
      compIdArr: [],
      isGetcomID: false,
      //木方
      WoodBeam: 0,
      //托架
      Bracket: 0,
      //装车方式
      loadCarMethods: [],
      loadCarMethod: "",
      loadCarMethodselect: "请选择",
      remark: "",
      hidden: -1,
      GetError: false,
      QRID: "",
      focusIndex: "15",
      visibleArr: [],
    }
    //this.requestCameraPermission = this.requestCameraPermission.bind(this)
  }

  componentDidMount() {
    const { navigation } = this.props;
    let guid = navigation.getParam('guid') || ''
    let pageType = navigation.getParam('pageType') || ''
    if (pageType == 'CheckPage') {
      this.checkResData(guid)
    } else {
      this.resData()
    }

    //通过使用DeviceEventEmitter模块来监听事件
    this.iDataScan = DeviceEventEmitter.addListener('iDataScan', (Event) => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 115 ~ SteelCage ~ Event.ScanResult", Event.ScanResult)
      if (typeof Event.ScanResult != undefined) {
        let data = Event.ScanResult
        let arr = data.split("=");
        console.log("🚀 ~ file: scan_loading.js:130 ~ SteelCage ~ this.iDataScan=DeviceEventEmitter.addListener ~ arr:", arr)
        let id = ''
        id = arr[arr.length - 1]
        console.log("🚀 ~ file: concrete_pouring.js ~ line 118 ~ SteelCage ~ id", id)
        if (this.state.type == 'compid') {
          this.GetcomID(id)
        }

      }
    });
  }

  componentWillUnmount() {
    compDataArr = [], compIdArr = [], QRid2 = '', tmpstr = '', weightAccount = 0, visibleArr = [];
    this.iDataScan.remove()
  }

  UpdateControl = () => {
    if (typeof this.props.navigation.getParam('QRid') != "undefined") {
      if (this.props.navigation.getParam('QRid') != "") {
        this.toast.show(Url.isLoadingView, 0)
      }
    }
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || ''
    const type = navigation.getParam('type') || '';

    {
      if (type == 'compid') {
        this.GetcomID(QRid)
      } else if (type == 'trackingid') {
        this.GetManyComponentbyHandlingtool(QRid)
      }
    }

    if (QRid != '') {
      navigation.setParams({ QRid: '', type: '' })
    }

  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: '扫码出库'
    }
  }

  checkResData = (guid) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getProductStockOutDetail",
      "servicetype": "pdaproductstockoutservice",
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        "_Rowguid": guid
      }
    };
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: scan_loading.js ~ line 161 ~ SteelCage ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: loading.js ~ line 189 ~ SteelCage ~ resData", resData)
      let rowguid = resData.result._Rowguid;
      let formCode = resData.result.formCode;
      let loadOutTime = resData.result.libtime;
      let arrivalTime = resData.result.arrivalTime;
      let carNum = resData.result.carnum;
      let carBoardNum = resData.result.sweepnum;
      let carrier = resData.result.carrier;
      let cartrains = resData.result.cartrains;
      let receiver = resData.result.receiver;
      let receiverPhone = resData.result.receiverPhone;
      let customer = resData.result.customer;
      let driver = resData.result.driver;
      let projectName = resData.result.projectName;
      let loadPeople = resData.result.libpeople;
      let WoodBeam = resData.result.WoodBeam;
      let Bracket = resData.result.Bracket;
      let loadCarMethod = resData.result.OutMethod;
      let truckModel = resData.result.truckModel;
      let components = resData.result.componets;
      let compDataArr = [];
      components.map((item, index) => {
        compDataArr.push(item)
      })

      let PlanData = {}, planResult = {}
      planResult.projectName = projectName
      planResult.carrier = carrier
      planResult.customer = customer
      planResult.cartrains = cartrains
      PlanData.result = planResult

      this.setState({
        resData: resData,
        formCode: formCode,
        rowguid: rowguid,
        loadOutTime: loadOutTime,
        arrivalTime: arrivalTime,
        carnum: carNum,
        carnumselect: carNum,
        projectName: projectName,
        editEmployeeName: loadPeople,
        sweepnum: carBoardNum,
        sweepselect: carBoardNum,
        carrier: carrier,
        companyselct: carrier,
        cartrains: cartrains,
        driver: driver,
        driverselect: driver,
        receiver: receiver,
        receiverPhone: receiverPhone,
        customer: customer,
        WoodBeam: WoodBeam,
        Bracket: Bracket,
        loadCarMethod: loadCarMethod,
        loadCarMethodselect: loadCarMethod,
        truckModel: truckModel,
        truckModelselect: truckModel,
        compData: components,
        compDataArr: compDataArr,
        isGetcomID: true
      });
      this.setState({
        isLoading: false
      });
    }).catch((error) => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 215 ~ QualityInspection ~ error", error)
    });
  }

  resData = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "LoadOutInitial",
      "servicetype": "pda",
      "express": "2066CAB8",
      "ciphertext": "36a65b6b6fd779dc9e0143147f65ae13",
      "data": { "factoryId": Url.PDAFid }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: loading.js ~ line 254 ~ SteelCage ~ formData", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      //初始化错误提示
      if (resData.status == 100) {
        let formCodeSetup = resData.result.formCodeSetup;
        let formCode = resData.result.formCode;
        let rowguid = resData.result.rowguid;
        let ServerTime = resData.result.ServerTime;
        let truckModels = resData.result.truckModels;
        let loadCarMethods = resData.result.loadCarMethods;
        let drivers = resData.result.drivers;
        let carnums = resData.result.carnums;
        let sweepnums = resData.result.sweepnums;
        let SupplierInfo = resData.result.SupplierInfo;
        this.setState({
          resData: resData,
          rowguid: rowguid,
          ServerTime: ServerTime,
          arrivalTime: ServerTime,
          loadOutTime: ServerTime,
          formCodeSetup: formCodeSetup,
          formCode: formCode,
          truckModels: truckModels,
          loadCarMethods: loadCarMethods,
          loadCarMethod: "单张吊装",
          loadCarMethodselect: "单张吊装",
          drivers: drivers,
          carnums: carnums,
          sweepnums: sweepnums,
          SupplierInfo: SupplierInfo
        });
      }
      else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }

    }).catch((error) => {
    });
  }

  DeleteData = () => {
    const { navigation } = this.props;
    let guid = navigation.getParam('guid') || ''
    console.log('DeleteData')
    let formData = new FormData();
    let data = {};
    data = {
      "action": "deleteLoadcar",
      "servicetype": "pda",
      "express": "06C795EE",
      "ciphertext": "a1e3818d57d9bfc9437c29e7a558e32e",
      "data": {
        "guid": guid,
        "factoryId": Url.PDAFid
      },
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: loading.js ~ line 314 ~ SteelCage ~ DeleteData ~ formData", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('删除成功');
        this.props.navigation.navigate('MainCheckPage')
      } else {
        Alert.alert(
          '删除失败',
          resData.message,
          [{
            text: '取消',
            style: 'cancel'
          }, {
            text: '返回上一级',
            onPress: () => {
              this.props.navigation.goBack()
            }
          }])
      }
    }).catch((error) => {
    });

  }

  PostData = () => {
    const {
      ServerTime,
      loadOutTime,
      arrivalTime,
      Bracket,
      formCode,
      carnum,
      remark,
      sweepnum,
      rowguid,
      compId,
      truckModel,
      companyid,
      companyname,
      driver,
      WoodBeam,
      planId,
      loadCarMethod,
      monitorId,
      cartrains,
      receiver,
      receiverPhone,
      compDataArr,
      compIdArr
    } = this.state;
    const { navigation } = this.props;
    const QRid = navigation.getParam('QRid') || '';

    if (companyid == '') {
      this.toast.show('承运单位不能为空');
      return
    } else if (driver == '') {
      this.toast.show('司机姓名不能为空');
      return
    } else if (carnum == '') {
      this.toast.show('车牌号不能为空');
      return
    } else if (carnum.length > 10) {
      this.toast.show('车牌号名字过长');
      return
    } else if (sweepnum == '') {
      this.toast.show('车板号不能为空');
      return
    } else if (sweepnum.length > 10) {
      this.toast.show('车板号名字过长');
      return
    } else if (truckModel == '') {
      this.toast.show('车辆型号不能为空');
      return
    } else if (truckModel.length > 10) {
      this.toast.show('车辆型号名字过长');
      return
    } else if (loadCarMethod.length == 0) {
      this.toast.show('装车方式不能为空');
      return
    } else if (compIdArr.length == 0) {
      this.toast.show('装车明细不能为空');
      return
    } else if (cartrains.length == 0) {
      this.toast.show('组合车次不能为空');
      return;
    } else if (receiver.length == 0) {
      this.toast.show('收货人不能为空');
      return;
    } else if (receiverPhone.length == 0) {
      this.toast.show('收货人电话不能为空');
      return;
    }

    this.toast.show(saveLoading, 0)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "saveproductstockout",
      "servicetype": "pdaproductstockoutservice",
      "express": "173B0BD0",
      "ciphertext": "554ae676049cd672b7bb193a10fab046",
      "data": {
        "receiver": receiver,
        "formCode": formCode,
        "factoryId": Url.PDAFid,
        "cartrains": cartrains,
        "remark": remark,
        "compIdArray": compDataArr,
        "userName": Url.PDAusername,
        "carnum": carnum,
        "rowguid": rowguid,
        "sweepnum": sweepnum,
        "truckModel": truckModel,
        "companyid": companyid,
        "receiverPhone": receiverPhone,
        "driver": driver,
        "companyname": companyname,
        "arrivalTime": arrivalTime,
        "woodBeam": WoodBeam,
        "bracket": Bracket,
        "libtime": loadOutTime,
        "loadCarMethod": loadCarMethod
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: loading.js ~ line 461 ~ SteelCage ~ formData", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('保存成功');
        setTimeout(() => {
          this.props.navigation.replace(this.props.navigation.getParam("page"), {
            title: this.props.navigation.getParam("title"),
            pageType: this.props.navigation.getParam("pageType"),
            page: this.props.navigation.getParam("page"),
            isAppPhotoSetting: this.props.navigation.getParam("isAppPhotoSetting"),
            isAppDateSetting: this.props.navigation.getParam("isAppDateSetting"),
            isAppPhotoAlbumSetting: this.props.navigation.getParam("isAppPhotoAlbumSetting"),
          })
          //this.props.navigation.navigate('PDAMainStack')
        }, 400)
      } else {
        this.toast.show('保存失败')
        Alert.alert('保存失败', resData.message)
      }
    }).catch((error) => {
      this.toast.show('保存失败')
      if (error.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('保存失败', "响应内容不是合法JSON格式")
        return
      }
      if (error.toString().indexOf('Network request faile') != -1) {
        Alert.alert('保存失败', "网络请求错误")
        return
      }
      Alert.alert('保存失败', error.toString())
    });
  }

  GetcomID = (id) => {
    let formData = new FormData();
    let data = {};

    data = {
      action: "getcompinfo",
      servicetype: "pdaproductstockoutservice",
      express: "E2BCEB1E",
      ciphertext: "077d106a940be035e6d80eb61a1a01c3",
      data: {
        factoryId: Url.PDAFid,
        compId: id
      }
    };

    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: scan_loading.js:547 ~ SteelCage ~ formData:", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
       this.toast.close()
      if (resData.status == '100') {
        let compId = resData.result.compId;
        let compData = resData.result;
        let compCode = resData.result.compCode;
        let projectName = compData.projectName;
        let tmpobj = {};
        if (compDataArr.length == 0) {
          compDataArr.push(compData);
          tmpobj.compId = compId;
          compIdArr.push(tmpobj);
          this.setState({
            compData: compData,
            compIdArr: compIdArr,
            compDataArr: compDataArr,
            compCode: compCode,
            projectName: projectName,
            compId: compId,
            isGetcomID: true
          });
          tmpstr = tmpstr + JSON.stringify(compId) + ",";
          weightAccount += compData.weight;
        } else {
          if (tmpstr.indexOf(compId) == -1) {
            if (this.state.projectName != projectName) {
              this.toast.show('扫描构件与装车构件项目名称不一致');
              return;
            }
            tmpobj.compId = compId;
            compIdArr.push(tmpobj);
            compDataArr.push(compData);
            this.setState({
              compData: compData,
              compIdArr: compIdArr,
              compDataArr: compDataArr,
              compCode: compCode,
              compId: compId,
              isGetcomID: true
            });

            tmpstr = tmpstr + tmpobj.compId + ",";
            weightAccount += compData.weight;
          } else {
            this.toast.show('已经扫瞄过此构件')
          }
        }
      } else {
        Alert.alert('ERROR', resData.message)
      }

    }).catch(err => {
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  GetManyComponentbyHandlingtool = (id) => {
    let formData = new FormData();
    let data = {};
    const { planId } = this.state
    if (planId == '') {
      this.toast.show('请先选择发货单号')
      return
    }
    data = {
      "action": "GetManyComponentbyHandlingtool",
      "servicetype": "pda",
      "express": "905DFC2D",
      "ciphertext": "96b3b3b57fff0e8170754b7b58036e09",
      "data": {
        "planId": planId,
        "handlingToolId": id,
        "corresponding": [

        ],
        "factoryId": Url.PDAFid
      }
    }

    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
       this.toast.close()
      if (resData.status == '100') {
        let compData = resData.result.correspondingComponents
        console.log("🚀 ~ file: loading.js ~ line 581 ~ SteelCage ~ compData", compData)
        compData.map((item, index) => {
          console.log("🚀 ~ file: loading.js ~ line 583 ~ SteelCage ~ compData.map ~ item", item)

          let compId = item.compId_source
          let compCode = item.compCode
          let tmpobj = {}
          if (compDataArr.length == 0) {
            compDataArr.push(item)
            tmpobj.compId = compId
            compIdArr.push(tmpobj)
            this.setState({
              compData: compData,
              compIdArr: compIdArr,
              compDataArr: compDataArr,
              compCode: compCode,
              compId: compId,
              isGetcomID: true,
            })
            tmpstr = tmpstr + JSON.stringify(tmpobj) + ' '
            weightAccount += item.weight
          } else {
            console.log("🚀 ~ file: loading.js ~ line 584 ~ SteelCage ~ compData.map ~ tmpstr", tmpstr)

            if (tmpstr.indexOf(compId) == -1) {
              tmpobj.compId = compId
              compIdArr.push(tmpobj)
              compId = this.state.compId + ',' + compId
              compDataArr.push(item)
              this.setState({
                compData: compData,
                compIdArr: compIdArr,
                compDataArr: compDataArr,
                compCode: compCode,
                compId: compId,
                isGetcomID: true,
              })
              tmpstr = tmpstr + JSON.stringify(tmpobj) + ' '
              weightAccount += compData[0].weight
            }
          }
        })
      } else {
        Alert.alert('ERROR', resData.message)
      }

    }).catch(err => {
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  GetInvoice = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "GetInvoice",
      "servicetype": "pda",
      "express": "1A352485",
      "ciphertext": "ab5b14cd3652990e568ab2b747b23b2f",
      "data": {
        "planId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: loading.js ~ line 550 ~ SteelCage ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
       this.toast.close()
      if (resData.status == '100') {
        let planprojectName = resData.result.projectName
        let carrier = resData.result.carrier
        let customer = resData.result.customer
        let cartrains = resData.result.cartrains
        this.setState({
          PlanData: resData,
          planprojectName: planprojectName,
          carrier: carrier,
          customer: customer,
          cartrains: cartrains,
          isPlanVisible: true,
          companyselct: carrier,
          companyname: carrier
        })
        this.state.SupplierInfo.map((item, index) => {
          if (item.companyname == carrier) {
            this.setState({ companyid: item.companyid })
          }
        })
      } else {
        Alert.alert('ERROR', resData.message)
      }

    }).catch(err => {
    })
  }

  GetDeliveryGuide = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getdeliveryguide",
      "servicetype": "pda",
      "express": "1A352485",
      "ciphertext": "ab5b14cd3652990e568ab2b747b23b2f",
      "data": {
        "planId": this.state.planId,
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
       this.toast.close()
      if (resData.status == '100') {
        let result = resData.result
        let deliveryguide = result
        this.setState({
          deliveryguide: deliveryguide
        })
        this.setState({ isDeliveryGuideVisible: true })
      } else {
        Alert.alert('ERROR', resData.message)
      }

    }).catch(err => {
    })
  }

  GetCommonMould = (id) => {
    let acceptanceName = '111'
    let formData = new FormData();
    let data = {};
    data = {
      "action": "GetCommonMould",
      "servicetype": "pda",
      "express": "50545047",
      "ciphertext": "f3eedf54128226aef62dd6023a1c1573",
      "data": {
        "compId": this.state.compId,
        "acceptanceId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
       this.toast.close()
      if (resData.status == '100') {
        let acceptanceId = resData.result.acceptanceId
        acceptanceName = resData.result.acceptanceName
        this.setState({
          acceptanceId: acceptanceId,
          acceptanceName: acceptanceName
        })
      } else {
        Alert.alert('ERROR', resData.message)
      }


    }).catch(err => {
    })
  }

  GetTeamPeopleInfo = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getTeamPeopleInfo",
      "servicetype": "pda",
      "express": "2B9B0E37",
      "ciphertext": "eb1a5891270a0ebdbb68d37e27f82b92",
      "data": {
        "factoryId": Url.PDAFid,
        "monitorId": id
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
       this.toast.close()
      if (resData.status == '100') {
        let monitorId = resData.result.monitorId
        let monitorName = resData.result.monitorName
        let teamId = resData.result.teamId
        let teamName = resData.result.teamName
        this.setState({
          monitorId: monitorId,
          monitorName: monitorName,
          monitorTeamId: teamId,
          monitorTeamName: teamName
        })
      } else {
        Alert.alert('ERROR', resData.message)
      }
    }).catch(err => {
    })
  }

  GetInspector = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getInspector",
      "servicetype": "pda",
      "express": "CDAB7A2D",
      "ciphertext": "a590b603df9b5a4384c130c7c21befb7",
      "data": {
        "InspectorId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
       this.toast.close()
      if (resData.status == '100') {
        let InspectorId = resData.result.InspectorId
        let InspectorName = resData.result.InspectorName
        this.setState({
          InspectorId: InspectorId,
          InspectorName: InspectorName,
        })
      } else {
        Alert.alert('ERROR', resData.message)
      }

    }).catch(err => {
    })
  }

  _DatePicker = (Time, focusIndex) => {
    const { ServerTime, loadOutTime, arrivalTime } = this.state
    return (
      <DatePicker
        customStyles={{
          dateInput: styles.dateInput,
          dateTouchBody: styles.dateTouchBody,
          dateText: styles.dateText,
        }}
        iconComponent={<Icon name='caretdown' color='#419fff' iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
        }
        showIcon={true}
        onOpenModal={() => {
          this.setState({ focusIndex: focusIndex })
        }}
        mode='datetime'
        date={Time}
        format="YYYY-MM-DD HH:mm"
        onDateChange={(value) => {
          this.setState({
            Time: value
          })
        }}
      />
    )
  }

  CardList = (compData) => {
    let tmpDesignType = ''
    let sameTypeArr = []

    return (
      compData.map((item, index) => {
        let comp = item
        console.log("🚀 ~ file: loading.js ~ line 506 ~ SteelCage ~ compData.map ~ comp.compCode", comp.compCode)
        return (
          <View style={styles.CardList_main}>
            <TouchableCustom
              onPress={() => {
                this.cardListContont(comp);
              }}
              onLongPress={() => {
                Alert.alert(
                  '提示信息',
                  '是否删除构件信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      compDataArr.splice(index, 1)
                      compIdArr.splice(index, 1)
                      weightAccount = 0
                      tmpstr = compIdArr.toString()
                      compDataArr.map((item1) => {
                        weightAccount += item1.weight;
                      })
                      compIdArr.map(itemID => {
                        tmpstr = "";
                        tmpstr += itemID.compId + ",";
                      });

                      this.setState({
                        compDataArr: compDataArr,
                        compIdArr: compIdArr
                      })
                    }
                  }])

              }}
            >
              <View style={styles.CardList_title_main}>
                <View style={styles.title_num}>
                  <Text >{index + 1}</Text>
                </View>
                <View style={styles.title_title}>
                  <Text >{comp.designType}</Text>
                </View>
                <View style={styles.CardList_at_icon}>
                  <IconDown text={'1个'} />
                </View>
              </View>
            </TouchableCustom>

            {
              visibleArr.indexOf(item.compCode) != -1 ?
                <View key={index} >
                  <ListItemScan_child
                    title='产品编号'
                    rightTitle={comp.compCode}
                  />
                  {/*   <ListItem
                       
                      title='项目名称'
                      rightTitle={comp.projectName}
                       
                       
                    /> */}
                  <ListItemScan_child
                    title='构件类型'
                    rightTitle={comp.compTypeName}
                  />
                  <ListItemScan_child
                    title='设计型号'
                    rightTitle={comp.designType}
                  />
                  <ListItemScan_child
                    title='楼号'
                    rightTitle={comp.floorNoName}
                  />
                  <ListItemScan_child
                    title='层号'
                    rightTitle={comp.floorName}
                  />
                  <ListItemScan_child
                    title='砼方量'
                    rightTitle={comp.volume}
                  />
                  <ListItemScan_child
                    title='重量'
                    rightTitle={comp.weight}
                  />
                  <ListItemScan_child
                    title='库房'
                    rightTitle={comp.roomName}
                  />
                  <ListItemScan_child
                    title='库位'
                    rightTitle={comp.libraryName}
                  />
                  <View style={{ height: 8 }}></View>
                </View> : <View></View>
            }

          </View>
        )

      })
    )
  }

  BottomList = () => {
    const { bottomVisible, bottomData } = this.state
    const { navigation } = this.props;
    let pageType = navigation.getParam('pageType') || ''
    return (
      <BottomSheet
        isVisible={bottomVisible && pageType != 'CheckPage'}
      //modalProps={{ style: { height: 100 }, }}
      >
        <ScrollView style={styles.bottomsheetScroll}>
          {
            bottomData.map((item, index) => {
              let title = ''
              if (item.planId) {
                title = item.formCode_jh + ' ' + item.projectName
              } else if (item.companyid) {
                title = item.companyname
              } else if (item.driver) {
                title = item.driver
              } else if (item.carnum) {
                title = item.carnum
              } else if (item.sweepnum) {
                title = item.sweepnum
              } else if (item.truckModel) {
                title = item.truckModel
              } else if (item.loadCarMethod) {
                title = item.loadCarMethod
              }
              return (
                <TouchableCustom
                  onPress={() => {
                    if (item.planId) {
                      this.setState({
                        planId: item.planId,
                        formCode_jh: item.formCode_jh,
                        projectName: item.projectName,
                        planselect: item.formCode_jh,
                      })
                      this.GetInvoice(item.planId)
                    } else if (item.companyid) {
                      this.setState({
                        companyid: item.companyid,
                        companyname: item.companyname,
                        companyselct: item.companyname,
                      })
                    } else if (item.driver) {
                      this.setState({
                        driver: item.driver,
                        driverselect: item.driver,
                      })
                    } else if (item.carnum) {
                      this.setState({
                        carnum: item.carnum,
                        carnumselect: item.carnum,
                      })
                    } else if (item.sweepnum) {
                      this.setState({
                        sweepnum: item.sweepnum,
                        sweepselect: item.sweepnum,
                      })
                    } else if (item.truckModel) {
                      this.setState({
                        truckModel: item.truckModel,
                        truckModelselect: item.truckModel,
                      })
                    } else if (item.loadCarMethod) {
                      this.setState({
                        loadCarMethod: item.loadCarMethod,
                        loadCarMethodselect: item.loadCarMethod,
                      })
                    }
                    this.setState({
                      bottomVisible: false
                    })
                  }}
                >
                  <BottomItem backgroundColor='white' color="#333" title={title} />
                </TouchableCustom>
              )
            })
          }
        </ScrollView>
        <TouchableCustom
          onPress={() => {
            this.setState({ bottomVisible: false })
          }}>
          <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
        </TouchableCustom>
      </BottomSheet>

    )
  }

  cardListContont = comp => {
    if (visibleArr.indexOf(comp.compCode) == -1) {
      visibleArr.push(comp.compCode);
      this.setState({ visibleArr: visibleArr });
    } else {
      if (visibleArr.length == 1) {
        visibleArr.splice(0, 1);
        this.setState({ visibleArr: visibleArr }, () => {
          this.forceUpdate;
        });
      } else {
        visibleArr.map((item, index) => {
          if (item == comp.compCode) {
            visibleArr.splice(index, 1);
            this.setState({ visibleArr: visibleArr }, () => {
              this.forceUpdate;
            });
          }
        });
      }
    }
  };

  isBottomVisible = (data, focusIndex) => {
    if (typeof (data) != "undefined") {
      if (data.length > 0) {
        this.setState({ bottomVisible: true, bottomData: data, focusIndex: focusIndex })
      }
    } else {
      this.toast.show("无数据")
    }
  }

  PlanCardList = (PlanData) => {
    return (
      <View>
        <Card containerStyle={styles.card_containerStyle}>
          <ListItemScan_child
            title='项目名称'
            rightTitle={PlanData.result.projectName}
          />
          <ListItemScan_child
            title='承运单位'
            rightTitle={PlanData.result.carrier}
          />
          <ListItemScan_child
            title='客户名称'
            rightTitle={PlanData.result.customer}
          />
          <ListItemScan_child
            title='组合车次'
            rightTitle={PlanData.result.cartrains}
          />
          <ListItemScan_child
            title='发货引导'
            rightElement={
              <View>
                <Text onPress={() => { this.GetDeliveryGuide() }} style={[styles.rightTitle, { color: '#419FFF', fontSize: 13, textAlign: 'right' }]} numberOfLines={1}>
                  查看
                </Text>
              </View>}
          />
        </Card>
      </View>
    )
  }

  render() {
    const { navigation } = this.props;
    let pageType = navigation.getParam('pageType') || ''

    const { resData, ServerTime, receiver, receiverPhone, isUsedPRacking, focusIndex, loadOutTime, arrivalTime, formCode, plans, compData, planId, projectName, planselect, isPlanVisible, planprojectName, customer, carrier, cartrains, SupplierInfo, companyid, companyname, companyselct, drivers, driver, driverselect, carnums, carnum, carnumselect, sweepnums, sweepnum, sweepselect, truckModels, truckModel, truckModelselect, WoodBeam, Bracket, loadCarMethods, loadCarMethod, loadCarMethodselect, compId, PlanData, compCode, remark, bottomVisible, bottomData, compDataArr } = this.state

    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          <NavigationEvents
            onWillFocus={this.UpdateControl} />
          <ScrollView ref={ref => this.scoll = ref} style={styles.mainView}>
            <View style={styles.listView}>
              <ListItemScan
                title='表单编号'
                rightTitle={formCode}
                focusStyle={focusIndex == '1' ? styles.focusColor : {}}
              />
              <ListItemScan
                title='运单编号'
                rightTitle={formCode}
                focusStyle={focusIndex == '3' ? styles.focusColor : {}}
              />
              <TouchableCustom underlayColor={'lightgray'} onPress={() => { this.isBottomVisible(SupplierInfo, '4') }} >
                <ListItemScan
                  title='承运单位'
                  focusStyle={focusIndex == '4' ? styles.focusColor : {}}
                  rightElement={
                    <TouchableOpacity onPress={() => { this.setState({ bottomVisible: true, bottomData: SupplierInfo, focusIndex: '4' }) }} >
                      <IconDown text={companyselct} />
                    </TouchableOpacity>
                  }
                />
              </TouchableCustom>
              <ListItemScan
                title='项目名称'
                rightTitle={projectName}
              />
              <ListItemScan
                title='司机姓名'
                ref={ref => { this.input1 = ref }}
                focusStyle={focusIndex == '5' ? styles.focusColor : {}}
                rightElement={
                  <View style={styles.loading_input_view}>
                    <Input
                      ref={ref => { this.input1 = ref }}
                      containerStyle={styles.input_container}
                      inputContainerStyle={styles.inputContainerStyle}
                      inputStyle={styles.input_}
                      placeholder='请输入或选择'
                      value={driverselect}
                      onChangeText={(value) => {
                        this.setState({
                          driverselect: value,
                          driver: value,
                        })
                      }}
                      //onFocus={() => { this.setState({ focusIndex: '5' }) }}
                      onSubmitEditing={() => { this.input2.focus() }}
                      underlineColorAndroid='transparent'
                    ></Input>
                    <Icon onPress={() => { this.isBottomVisible(drivers, 5) }}
                      name='caretdown'
                      type='antdesign'
                      color='#419fff'
                      iconStyle={{ fontSize: 13, marginLeft: 8, marginRight: -1 }}
                    />
                  </View>
                }
              />
              <ListItemScan
                title='车牌号'
                focusStyle={focusIndex == '6' ? styles.focusColor : {}}
                rightElement={
                  <View style={styles.loading_input_view}>
                    <Input
                      ref={ref => { this.input2 = ref }}
                      containerStyle={styles.input_container}
                      inputContainerStyle={styles.inputContainerStyle}
                      inputStyle={styles.input_}
                      placeholder='请输入或选择'
                      value={carnumselect}
                      onChangeText={(value) => {
                        this.setState({
                          carnumselect: value,
                          carnum: value
                        })
                      }}
                      //onFocus={() => { this.setState({ focusIndex: 6 }) }}
                      onSubmitEditing={() => { this.input3.focus() }}
                      underlineColorAndroid='transparent'
                    ></Input>
                    <Icon onPress={() => { this.isBottomVisible(carnums, 6) }}
                      name='caretdown'
                      type='antdesign'
                      color='#419fff'
                      iconStyle={{ fontSize: 13, marginLeft: 8, marginRight: -1 }}
                    />
                  </View>
                }
              />
              <ListItemScan
                title='车板号'
                focusStyle={focusIndex == '7' ? styles.focusColor : {}}
                rightElement={
                  <View style={styles.loading_input_view}>
                    <Input
                      ref={ref => { this.input3 = ref }}
                      containerStyle={styles.input_container}
                      inputContainerStyle={styles.inputContainerStyle}
                      inputStyle={styles.input_}
                      placeholder='请输入或选择'
                      value={sweepselect}
                      onChangeText={(value) => {
                        this.setState({
                          sweepselect: value,
                          sweepnum: value
                        })
                      }}
                      //onFocus={() => { this.setState({ focusIndex: 7 }) }}
                      onSubmitEditing={() => { this.input4.focus() }}
                      underlineColorAndroid='transparent'
                    ></Input>
                    <Icon onPress={() => { this.isBottomVisible(sweepnums, 7) }}
                      name='caretdown'
                      type='antdesign'
                      color='#419fff'
                      iconStyle={{ fontSize: 13, marginLeft: 8, marginRight: -1 }}
                    />
                  </View>
                }
              />
              <ListItemScan
                title='车辆型号'
                focusStyle={focusIndex == '8' ? styles.focusColor : {}}
                rightElement={
                  <View style={styles.loading_input_view}>
                    <Input
                      ref={ref => { this.input4 = ref }}
                      containerStyle={styles.input_container}
                      inputContainerStyle={styles.inputContainerStyle}
                      inputStyle={styles.input_}
                      placeholder='请输入或选择'
                      value={truckModelselect}
                      onChangeText={(value) => {
                        this.setState({
                          truckModelselect: value,
                          truckModel: value
                        })
                      }}
                      //onFocus={() => { this.setState({ focusIndex: 8 }) }}
                      onSubmitEditing={() => { this.input5.focus() }}
                      underlineColorAndroid='transparent'
                    ></Input>
                    <Icon onPress={() => { this.isBottomVisible(truckModels, 8) }}
                      name='caretdown'
                      type='antdesign'
                      color='#419fff'
                      iconStyle={{ fontSize: 13, marginLeft: 8, marginRight: -1 }}
                    />
                  </View>
                }
              />
              <ListItemScan
                focusStyle={focusIndex == '17' ? styles.focusColor : {}}
                title='组合车次'
                rightElement={
                  <Input
                    ref={ref => { this.input5 = ref }}
                    containerStyle={[styles.quality_input_container, { top: 2 }]}
                    inputContainerStyle={styles.inputContainerStyle}
                    inputStyle={[styles.quality_input_]}
                    placeholder='请输入'
                    value={cartrains}
                    //onFocus={() => { this.setState({ focusIndex: 17 }) }}
                    onSubmitEditing={() => { this.input6.focus() }}
                    onChangeText={(value) => {
                      this.setState({
                        cartrains: value
                      })
                    }} />
                }
              />
              <ListItemScan
                focusStyle={focusIndex == '18' ? styles.focusColor : {}}
                title='收货人'
                rightElement={
                  <Input
                    ref={ref => { this.input6 = ref }}
                    containerStyle={[styles.quality_input_container, { top: 2 }]}
                    inputContainerStyle={styles.inputContainerStyle}
                    inputStyle={[styles.quality_input_]}
                    placeholder='请输入'
                    value={receiver}
                    //onFocus={() => { this.setState({ focusIndex: 18 }) }}
                    onSubmitEditing={() => { this.input7.focus() }}
                    onChangeText={(value) => {
                      this.setState({
                        receiver: value
                      })
                    }} />
                }
              />
              <ListItemScan
                focusStyle={focusIndex == '19' ? styles.focusColor : {}}
                title='收货人电话'
                rightElement={
                  <Input
                    ref={ref => { this.input7 = ref }}
                    containerStyle={[styles.quality_input_container, { top: 2 }]}
                    inputContainerStyle={styles.inputContainerStyle}
                    inputStyle={[styles.quality_input_]}
                    placeholder='请输入'
                    value={receiverPhone}
                    //onFocus={() => { this.setState({ focusIndex: 19 }) }}
                    onSubmitEditing={() => { this.input8.focus() }}
                    onChangeText={(value) => {
                      this.setState({
                        receiverPhone: value
                      })
                    }} />
                }
              />
              <ListItemScan
                focusStyle={focusIndex == '9' ? styles.focusColor : {}}
                leftElement={
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.text_beforeinput} >木方</Text>
                    <Input
                      ref={ref => { this.input8 = ref }}
                      keyboardType='number-pad'
                      containerStyle={[styles.input_container, { width: width / 4, }]}
                      inputContainerStyle={styles.inputContainerStyle}
                      inputStyle={[styles.input_, { textAlign: 'center', top: 3 }]}
                      placeholder='请输入'
                      value={WoodBeam}
                      onChangeText={(value) => {
                        this.setState({
                          WoodBeam: value
                        })
                      }}
                      //onFocus={() => { this.setState({ focusIndex: 9 }) }}
                      onSubmitEditing={() => {
                        this.input9.focus()
                      }} />
                  </View>
                }
                rightElement={
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.text_beforeinput} >托架</Text>
                    <Input
                      ref={ref => { this.input9 = ref }}
                      keyboardType='number-pad'
                      containerStyle={[styles.input_container, { width: width / 4, }]}
                      inputContainerStyle={styles.inputContainerStyle}
                      inputStyle={[styles.input_, { textAlign: 'center', top: 3 }]}
                      placeholder='请输入'
                      //onFocus={() => { this.setState({ focusIndex: 9 }) }}
                      value={Bracket}
                      onChangeText={(value) => {
                        this.setState({
                          Bracket: value
                        })
                      }}
                    />
                  </View>
                }
              />
              <TouchableCustom underlayColor={'lightgray'} onPress={() => { this.isBottomVisible(loadCarMethods, '10') }} >
                <ListItemScan
                  focusStyle={focusIndex == '10' ? styles.focusColor : {}}
                  title='装车方式'
                  rightElement={
                    <IconDown text={loadCarMethodselect} />
                  }
                />
              </TouchableCustom>

              <ListItemScan
                focusStyle={focusIndex == '11' ? styles.focusColor : {}}
                title='出库日期'
                rightElement={this._DatePicker(loadOutTime, '11')}
              />
              <ListItemScan
                focusStyle={focusIndex == '12' ? styles.focusColor : {}}
                title='到货日期'
                rightElement={this._DatePicker(arrivalTime, '12')}
              />
              <ListItemScan
                focusStyle={focusIndex == '13' ? styles.focusColor : {}}
                title='出库人'
                rightTitle={Url.PDAEmployeeName}
              />
              <ListItemScan
                focusStyle={focusIndex == '14' ? styles.focusColor : {}}
                title='备注'
                rightElement={
                  <Input
                    containerStyle={[styles.quality_input_container, { top: 2 }]}
                    inputContainerStyle={styles.inputContainerStyle}
                    inputStyle={[styles.quality_input_]}
                    placeholder='请输入'
                    value={remark}
                    onFocus={() => { this.setState({ focusIndex: 14 }) }}
                    onChangeText={(value) => {
                      this.setState({
                        remark: value
                      })
                    }} />
                }
              />
              <ListItemScan
                title='重量合计'
                rightTitle={weightAccount.toFixed(2)}
              />
              <ListItemScan
                isButton={true}
                focusStyle={focusIndex == '15' ? styles.focusColor : {}}
                title='出库明细'
                onPressIn={() => {
                  this.setState({
                    type: 'compid',
                    focusIndex: 15
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPress={() => {
                  this.setState({
                    type: 'compid',
                    focusIndex: 15
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}
                rightElement={
                  <View>
                    <ScanButton
                      onPress={() => {
                        this.setState({
                          isGetcomID: false,
                          focusIndex: 15
                        })
                        this.props.navigation.navigate('QRCode', {
                          type: 'compid',
                          page: 'PDASacnLoading'
                        })
                      }}
                      onLongPress={() => {
                        this.setState({
                          type: 'compid',
                          focusIndex: 15
                        })
                        NativeModules.PDAScan.onScan();
                      }}
                      onPressOut={() => {
                        NativeModules.PDAScan.offScan();
                      }}
                    />
                  </View>
                }
              />
              {
                compDataArr.length != 0 ? this.CardList(compDataArr) : <View></View>
              }

            </View>

            {
              pageType == 'CheckPage' ?
                <FormButton
                  backgroundColor='#EB5D20'
                  title='删除'
                  onLongPress={() => {
                    Alert.alert(
                      '提示信息',
                      '是否删除',
                      [{
                        text: '取消',
                        style: 'cancel'
                      }, {
                        text: '删除',
                        onPress: () => {
                          this.DeleteData()
                        }
                      }])
                  }}
                /> :
                <FormButton
                  backgroundColor='#17BC29'
                  title='保存'
                  onPress={() => {
                    this.PostData()
                  }}
                />
            }

            {this.BottomList()}

          </ScrollView>

          <Overlay
            fullScreen={true}
            animationType='fade'
            isVisible={this.state.isDeliveryGuideVisible}
            onRequestClose={() => {
              this.setState({ isDeliveryGuideVisible: !this.state.isDeliveryGuideVisible });
            }}>
            <Header
              centerComponent={{ text: '发货引导', style: { color: 'gray', fontSize: 20 } }}
              rightComponent={<BackIcon
                onPress={() => {
                  this.setState({
                    isDeliveryGuideVisible: !this.state.isDeliveryGuideVisible
                  });
                }} />}
              backgroundColor='white'
            />
            {
              this.state.deliveryguide.map((item, index) => {
                return (
                  <Card containerStyle={{ borderRadius: 10 }}>
                    <TouchableOpacity >
                      <View style={{ flexDirection: 'row', }}>
                        <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 1, }}>
                          <View style={{ flexDirection: 'row' }}>
                            <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.compCode}</Text>
                            <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.diffSourceType}</Text>
                          </View>
                          <View style={{ flexDirection: 'row' }}>
                            <Text style={[styles.text]} numberOfLines={1}> {item.designType}</Text>
                            <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.roomName}</Text>
                          </View>
                          <View style={{ flexDirection: 'row' }}>
                            <Text style={[styles.text, styles.textthi]} numberOfLines={1}> {item.compTypeName}</Text>
                            <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.libraryName}</Text>
                          </View>
                        </View>
                      </View>
                    </TouchableOpacity>
                  </Card>
                )
              })
            }
          </Overlay>
        </View>
      )
    }
  }

}

class InputView extends React.Component {
  render() {
    const { value, onChangeText, onFocus, onSubmitEditing, onPress } = this.props
    return (
      <View style={styles.loading_input_view}>
        <Input
          {...this.props}
          containerStyle={styles.input_container}
          inputContainerStyle={styles.inputContainerStyle}
          inputStyle={styles.input_}
          placeholder='请输入或选择'
          value={value}
          onChangeText={onChangeText}
          onFocus={onFocus}
          onSubmitEditing={onSubmitEditing}
          underlineColorAndroid='transparent'
        ></Input>
        <Icon onPress={onPress}
          name='caretdown'
          type='antdesign'
          color='#419fff'
          iconStyle={{ fontSize: 13, marginLeft: 8, marginRight: -1 }}
        />

      </View>
    )
  }
}

class PlanCardList extends React.Component {
  render() {
    const { PlanData } = this.props
    return (
      <View>
        <Card containerStyle={{ borderRadius: 10, shadowOpacity: 0 }}>
          <ListItem
            containerStyle={styles.list_container_style}
            title='项目名称'
            rightTitle={PlanData.result.projectName}
            titleStyle={styles.title}
            rightTitleProps={{ numberOfLines: 1 }}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='承运单位'
            rightTitle={PlanData.result.carrier}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='客户名称'
            rightTitle={PlanData.result.customer}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='组合车次'
            rightTitle={PlanData.result.cartrains}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='发货引导'
            rightElement={<View>
              <Text onPress={() => { this.setState({}) }} style={[styles.rightTitle, { color: '#419FFF', fontSize: 13, textAlign: 'right' }]} numberOfLines={1}>
                查看
              </Text>
            </View>}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
        </Card>
      </View>
    )
  }
}

class BackIcon extends React.PureComponent {
  render() {
    return (
      <TouchableOpacity
        onPress={this.props.onPress} >
        <Icon
          name='arrow-back'
          color='#419FFF' />
      </TouchableOpacity>
    )
  }
}

/* const styles = StyleSheet.create({
  title: {
    fontSize: 13,
  },
  rightTitle: {
    fontSize: 13,
    textAlign: 'right',
    lineHeight: 14,
    flexWrap: 'wrap',
    width: width / 2
  },
  list_container_style: { height: 14 },
  input_container: {
    width: width / 4,
    height: 14,
    marginTop: -8,
  },
  input_: {
    fontSize: 14,
    textAlign: 'right'
  },
  inputContainerStyle: { backgroundColor: 'transparent', borderColor: 'transparent' },
  text_beforeinput: { width: width / 5, textAlign: 'center', fontSize: RFT * 3 }
}) */