import React from 'react';
import { View, StyleSheet, TouchableHighlight, Dimensions, ActivityIndicator, Alert, Image } from 'react-native';
import { Button, Text, Input, Icon, Card, ListItem, BottomSheet, Overlay, Header } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { ScrollView } from 'react-native-gesture-handler';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { NavigationEvents } from 'react-navigation';
import { styles } from '../Componment/PDAStyles';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import CheckBoxScan from '../Componment/CheckBoxScan';
import AvatarImg from '../Componment/AvatarImg';
import DeviceStorage from '../../Url/DeviceStorage';
import AvatarAdd from '../Componment/AvatarAdd';
import DatePicker from 'react-native-datepicker'
import ScanButton from '../Componment/ScanButton';

import { DeviceEventEmitter, NativeModules } from 'react-native';
import overlayImg from '../Componment/OverlayImg';
import OverlayImgZoomPicker from '../Componment/OverlayImgZoomPicker';
class BackIcon extends React.PureComponent {
    render() {
        return (
            <TouchableCustom
                onPress={this.props.onPress} >
                <Icon
                    name='arrow-back'
                    color='#419FFF' />
            </TouchableCustom>
        )
    }
}

let fileflag = "18"

let imageArr = [], imageFileArr = [];

let QRidArr = []

const { height, width } = Dimensions.get('window') //获取宽高

export default class MaterialProjectPicking extends React.PureComponent {
    constructor() {
        super();
        this.state = {
            title: "",
            type: 'compid',
            resData: [],
            rowguid: '',
            supplyNo: '', // 领料单号
            tabulationDate: '', // 制表日期
            projectName: '请选择', // 项目名称
            projectId: '',
            reqtype: '请选择', // 领料类型
            room: '请选择', // 库房
            roomCode: '',
            roomId: '',
            shop: '请选择', // 车间
            shopId: '',
            team: '请选择', // 班组
            teamId: '',
            supplyUse: '', // 领料用途
            supplyPerson: '请选择', // 领料人
            devPerson: '',
            supplyPersonId: '',
            tabulator: '', // 制表人
            remark: '', // 备注,
            materialInfos: [], // 材料明细
            //materialIds: '', // 已选材料id
            department: '请选择', // 领料部门
            departmentId: '',

            // 选择确认
            //projectNameSelect: false,
            //roomSelect: false,
            //shopSelect: false,
            //teamSelect: false,
            //supplyUseSelect: false,

            projectNameList: [],
            reqtypeList: ['部门领料', '车间领料'],
            deptList: [],
            roomList: [],
            shopList: [],
            teamList: [],
            supplyPersonList: [],


            //底栏控制
            bottomData: [],
            projectNameVisible: false,
            deptVisible: false,
            reqtypeVisible: false,
            roomVisible: false,
            shopVisible: false,
            teamVisible: false,
            supplyPersonVisible: false,

            checked: true,

            focusIndex: 7,
            isLoading: false,

            imageArr: [],
            imageFileArr: [],
            pictureSelect: false,
            pictureUri: '',
            fileid: "",
            recid: "",
            isPhotoUpdate: false,
            imageOverSize: false,
            uriShow: '',

            //查询界面取消选择按钮
            isCheckPage: false,

            //控制app拍照，日期，相册选择功能
            isAppPhotoSetting: false,
      currShowImgIndex: 0,
            isAppDateSetting: false,
            isAppPhotoAlbumSetting: false
        }
    }

    componentDidMount() {
        const { navigation } = this.props;
        let guid = navigation.getParam('guid') || ''
        let pageType = navigation.getParam('pageType') || ''
        let title = navigation.getParam('title')
        let action = 'projectpickinginit'
        let isAppPhotoSetting = navigation.getParam('isAppPhotoSetting')
        let isAppDateSetting = navigation.getParam('isAppDateSetting')
        let isAppPhotoAlbumSetting = navigation.getParam('isAppPhotoAlbumSetting')
        this.setState({
            isAppPhotoSetting: isAppPhotoSetting,
            isAppDateSetting: isAppDateSetting,
            isAppPhotoAlbumSetting: isAppPhotoAlbumSetting
        })
        if (title == '部门领料') action = 'deptpickinginit'
        DeviceStorage.get('MaterialDeptPickingSupplyPerson').then(res => {
            if (res != null) {
                this.setState({
                    devPerson: res
                })
            }
        }).catch(err => { console.log("MaterialDeptPickingSupplyPerson error: " + error) });
        this.setState({
            title: title,
        })
        if (pageType == 'CheckPage') {
            let guid = navigation.getParam('guid') || ''
            this.checkResData(guid)
            this.setState({
                isCheckPage: true
            })
        } else {
            this.resData(action)

        }

        //通过使用DeviceEventEmitter模块来监听事件
        this.iDataScan = DeviceEventEmitter.addListener('iDataScan', (Event) => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 115 ~ SteelCage ~ Event.ScanResult", Event.ScanResult)
            if (typeof Event.ScanResult != undefined) {
                let data = Event.ScanResult
                let arr = data.split("=");
                let id = ''
                id = arr[arr.length - 1]
                console.log("🚀 ~ file: concrete_pouring.js ~ line 118 ~ SteelCage ~ id", id)
                if (this.state.type == 'compid') {
                    const { projectName, projectId, room, roomId, shop, team, materialInfos, title, reqtype, department } = this.state
                    let message = ""
                    if (title == '项目领料' && projectName == '请选择') {
                        message = "请选择项目"
                    } else if (title == '部门领料' && reqtype == '请选择') {
                        message = "请选择领料类型"
                    } else if (room == '请选择') {
                        message = "请选择库房"
                    } else if (reqtype != '部门领料' && shop == '请选择') {
                        message = "请选择车间"
                    } else if (reqtype != '部门领料' && team == '请选择') {
                        message = "请选择班组"
                    } else if (reqtype == '部门领料' && department == '请选择') {
                        message = "请选择部门"
                    }
                    if (message != "") {
                        this.toast.show(message)
                        return
                    }
                    this.scanMaterial(id)
                }

            }
        });
    }

    componentWillUnmount() {
        imageArr = [], imageFileArr = [];
        QRidArr = []
        this.iDataScan.remove()
    }

    UpdateControl = () => {

        const { navigation } = this.props;
        const { } = this.state

        let type = navigation.getParam('type') || '';
        if (type == 'compid') {
            let QRid = navigation.getParam('QRid') || ''
            this.scanMaterial(QRid)
        }
        let mis = []
        mis = navigation.getParam('materialInfos') || []
        let pickingInfoId = navigation.getParam('pickingInfoId') || ""
        let state = navigation.getParam('state') || ""
        if (state == "requisitionCount") {
            let requisitionCount = navigation.getParam('requisitionCount') || "0"
            mis = this.state.materialInfos
            mis.map((item, index) => {
                if (item.id == pickingInfoId) {
                    item.requisitionCount = requisitionCount
                    return
                }
            })
            this.setState({
                materialInfos: mis
            })
        } else if (state == "addMaterial") {
            this.state.materialInfos.map((item, index) => {
                mis.push(item)
            })
            this.setState({
                materialInfos: mis
            })
        } else if (state == "selectSupplier") {
            let projectName = navigation.getParam('projectName') || ""
            let projectId = navigation.getParam('projectId') || ""
            this.setState({
                projectName: projectName,
                projectId: projectId
            })
        }
        this.props.navigation.setParams({
            type: "",
            state: "",
            materialInfos: []
        })
    }

    //顶栏
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('title')
        }
    }

    resData = (action) => {
        let formData = new FormData();
        let data = {};
        data = {
            "action": action,
            "servicetype": "pdamaterial",
            "express": "6DFD1C9B",
            "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
            "data": { "factoryId": Url.PDAFid }
        }
        formData.append('jsonParam', JSON.stringify(data))
        //console.log("🚀 ~ file: MaterialProjectPicking.js ~ line 130 ~ SampleManage ~ formData:", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res.statusText);
            //console.log(res);
            return res.json();
        }).then(resData => {
            console.log("MaterialProjectPicking ---- 145 ----" + JSON.stringify(resData.result))
            if (resData.status == '100') {

                //let dictionary = resData.result.defectTypes
                //console.log("MaterialProjectPicking ---- 145 ----" + JSON.stringify(resData.result))
                if (this.state.title == "项目领料") {
                    this.setState({
                        //resData: resData.result,
                        rowguid: resData.result.rowguid,
                        supplyNo: resData.result.supplyNo,
                        tabulationDate: resData.result.tabulationDate,
                        projectNameList: resData.result.projectName,
                        shopList: resData.result.shop,
                        roomList: resData.result.room,
                        teamList: resData.result.team,
                        tabulator: Url.PDAEmployeeName,
                        isLoading: false
                    })
                } else if (this.state.title == "部门领料") {
                    this.setState({
                        rowguid: resData.result.rowguid,
                        supplyNo: resData.result.supplyNo,
                        tabulationDate: resData.result.tabulationDate,
                        reqtypeList: resData.result.pickingType,
                        deptList: resData.result.department,
                        shopList: resData.result.shop,
                        roomList: resData.result.room,
                        teamList: resData.result.team,
                        tabulator: Url.PDAEmployeeName,
                        isLoading: false
                    })
                }

            } else {
                Alert.alert("错误提示", resData.message, [{
                    text: '取消',
                    style: 'cancel'
                }, {
                    text: '返回上一级',
                    onPress: () => {
                        this.props.navigation.goBack()
                    }
                }])
            }

        }).catch((error) => {
            console.log("🚀 ~ file: MaterialProjectPicking.js ~ line 233 ~ MaterialProjectPicking ~ error:", error)
        });

    }

    checkResData = (guid) => {
        let formData = new FormData();
        let data = {};
        data = {
            "action": "projectpickingquerydetail",
            "servicetype": "pdamaterial",
            "express": "6DFD1C9B",
            "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
            "data": {
                "factoryId": Url.PDAFid,
                "guid": guid
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: MaterialProjectPicking.js ~ line 130 ~ SampleManage ~ formData:", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res.statusText);
            //console.log(res);
            return res.json();
        }).then(resData => {
            if (resData.status == '100') {
                console.log("MaterialProjectPicking ---- 267 ----" + JSON.stringify(resData.result))
                let fileAttach = resData.result.fileAttach
                if (fileAttach.length > 0) {
                    fileAttach.map((item, index) => {
                        let tmp = {}
                        tmp.fileid = ""
                        tmp.uri = item
                        tmp.url = item
                        imageFileArr.push(tmp)
                        imageArr.push(item)
                    })
                }
                this.setState({
                    imageFileArr: imageFileArr
                })
                this.setState({
                    //rowguid: resData.result.rowguid,
                    supplyNo: resData.result.supplyNo,
                    reqtype: resData.result.reqtype,
                    tabulationDate: resData.result.tabulationDate,
                    projectName: resData.result.projectName,
                    shop: resData.result.shop,
                    room: resData.result.room,
                    team: resData.result.team,
                    department: resData.result.department,
                    supplyUse: resData.result.supplyUse,
                    supplyPerson: resData.result.supplyPerson,
                    tabulator: resData.result.tabulator,
                    remark: resData.result.remark,
                    materialInfos: resData.result.materialInfo,
                    imageArr: resData.result.fileAttach,
                    pictureSelect: true,
                    isLoading: false
                })
            } else {
                Alert.alert("错误提示", resData.message, [{
                    text: '取消',
                    style: 'cancel'
                }, {
                    text: '返回上一级',
                    onPress: () => {
                        this.props.navigation.goBack()
                    }
                }])
            }

        }).catch((error) => {
            console.log("🚀 ~ file: MaterialProjectPicking.js ~ line 233 ~ MaterialProjectPicking ~ error:", error)
        });
    }

    scanMaterial = (guid) => {
        if (typeof this.props.navigation.getParam('QRid') != "undefined") {
            if (this.props.navigation.getParam('QRid') != "") {
                this.toast.show(Url.isLoadingView, 0)
            }
        }
        const { projectId, roomId, materialInfos, title } = this.state
        let formData = new FormData();
        let data = {};
        if (QRidArr.indexOf(guid) != -1) {
            this.toast.show('材料重复')
            return
        }

        let scanAction = ""
        if (title == '项目领料') scanAction = 'projectpickingscan'
        else if (title == '部门领料') scanAction = 'deptpickingscan'
        let materialIds = ''
        materialInfos.map((item, index) => {
            materialIds += index == materialInfos.length - 1 ? item.id : item.id + ','
        })
        data = {
            "action": scanAction,
            "servicetype": "pdamaterial",
            "express": "6DFD1C9B",
            "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
            "data": {
                "factoryId": Url.PDAFid,
                "rowguid": guid,
                "roomId": roomId,
                "projectId": projectId,
                "materialIds": materialIds
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: MaterialProjectPicking.js ~ line 130 ~ MaterialProjectPicking ~ formData:", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res.statusText);
            //console.log(res);
            return res.json();
        }).then(resData => {
            this.toast.close()
            if (resData.status == '100') {
                console.log("MaterialProjectPicking ---- 267 ----" + JSON.stringify(resData.result))

                let mis = materialInfos
                let materialIds = ''
                mis.map((item, index) => {
                    materialIds += index == mis.length - 1 ? item.id : item.id + ','
                })
                let result = resData.result
                if (result.length > 1) {
                    setTimeout(() => {
                        this.props.navigation.navigate('SelectMaterial', {
                            title: '选择材料',
                            action: title,
                            roomId: roomId,
                            projectId: projectId,
                            materialIds: materialIds,
                            keyStr: result[0].code
                        })
                    }, 1000)
                } else {
                    if (mis != []) {
                        result.map((item, index) => {
                            let check = true
                            materialInfos.map((item2, index2) => {
                                if (item.id == item2.id) {
                                    check = false
                                    return
                                }
                            })
                            if (check) mis.push(item)
                        })
                    } else {
                        mis = result
                    }
                    this.setState({
                        materialInfos: mis,
                    }, () => {
                        this.forceUpdate()
                    })
                }
                QRidArr.push(guid)
            } else {
                Alert.alert("错误提示", resData.message)
            }

        }).catch((error) => {
            console.log("🚀 ~ file: MaterialInStorage.js ~ line 233 ~ MaterialInStorage ~ error:", error)
        });
    }

    PostData = () => {
        const { title, reqtype, projectName, room, shop, team, department, materialInfos, recid } = this.state
        let formData = new FormData();
        let data = {};
        let message = ""
        if (title == '项目领料' && projectName == '请选择') {
            message = "请选择项目"
        } else if (title == '部门领料' && reqtype == '请选择') {
            message = "请选择领料类型"
        } else if (room == '请选择') {
            message = "请选择库房"
        } else if (reqtype != '部门领料' && shop == '请选择') {
            message = "请选择车间"
        } else if (reqtype != '部门领料' && team == '请选择') {
            message = "请选择班组"
        } else if (reqtype == '部门领料' && department == '请选择') {
            message = "请选择部门"
        } else if (materialInfos == [] || materialInfos == '') {
            message = "请选择材料"
        }
        materialInfos.map((item, index) => {
            if (item.requisitionCount == '0' || item.requisitionCount == '') {
                message = "材料编码" + item.code + "，请选择领料数量"
                return
            }
        })
        if (message != "") {
            this.toast.show(message)
            return
        }

        if (this.state.isAppPhotoSetting) {
            if (imageArr.length == 0) {
                this.toast.show('请先拍摄构件照片');
                return
            }
        }

        this.toast.show(saveLoading, 0)

        let MRequisition = this.MRequisitionFormat()
        let MRequisition_sub = this.MRequisitionSubFormat()
        //console.log("===========" + JSON.stringify(MRequisition_sub))

        data = {
            "action": "materialrequisitionsave",
            "servicetype": "routematerial",
            "express": "6DFD1C9B",
            "ciphertext": "8e77764c07d5ab103cc6e6a0449b91ba",
            "userName": Url.PDAusername,
            "factoryId": Url.PDAFid,
            "data": {
                "MRequisition": MRequisition,
                "Material_Requisition_sub": MRequisition_sub,
                "MaterialRequisitionsub2": "",
                "Material_Requisition_sub3": "",
                "Material_Requisition_sub4": "",
                "DbActionType": "Add",
                "recid": recid
            },
        }
        if (Url.isAppNewUpload) {
            data.action = "materialrequisitionsavenew"
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: MaterialProjectPicking.js:579 ~ formData:", formData)
        if (!Url.isAppNewUpload) {
            imageArr.map((item, index) => {
                formData.append('img_' + index, {
                    uri: item,
                    type: 'image/jpeg',
                    name: 'img_' + index
                })
            })
        }
        fetch(Url.PDAurl, {
            method: 'post',
            headers: {
                'content-type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            //console.log(res);
            return res.json();
        }).then(resData => {
            this.toast.close()
            //console.log("🚀 ~ file: MaterialInStorage.js ~ line 947 ~ MaterialInStorage ~ resdata", resData)
            if (resData.Success == true) {
                this.toast.show('保存成功');
                setTimeout(() => {
                    this.props.navigation.replace(this.props.navigation.getParam("page"), {
                        title: this.props.navigation.getParam("title"),
                        pageType: this.props.navigation.getParam("pageType"),
                        page: this.props.navigation.getParam("page"),
                        isAppPhotoSetting: this.props.navigation.getParam("isAppPhotoSetting"),
                        isAppDateSetting: this.props.navigation.getParam("isAppDateSetting"),
                        isAppPhotoAlbumSetting: this.props.navigation.getParam("isAppPhotoAlbumSetting"),
                    })
                }, 1000)
            } else {
                Alert.alert('保存失败', resData.ErrMsg)
                //console.log('resdata', resData)
            }
        }).catch((error) => {
            console.log("error --- " + error)
        });

    }

    MRequisitionFormat = () => {
        const { rowguid, title, team, teamId, supplyUse, room, roomId, roomCode, tabulationDate, projectName, projectId, supplyPerson, supplyPersonId, remark, supplyNo, shop, shopId, department, departmentId } = this.state
        let reqtype = '1'
        let reqtype2 = '0'
        if (title == '部门领料') {
            reqtype = '2'
            if (this.state.reqtype == '部门领料') reqtype2 = '1'
            else if (this.state.reqtype == '车间领料') reqtype2 = '2'
        }
        let MRequisition = {}
        MRequisition._rowguid = rowguid
        MRequisition.formCode_YC = ''
        MRequisition.deptName = this.state.reqtype == '部门领料' ? department : ''
        MRequisition.deptId = this.state.reqtype == '部门领料' ? departmentId : ''
        MRequisition.teamName = this.state.reqtype == '部门领料' ? '' : team
        MRequisition.teamId = this.state.reqtype == '部门领料' ? '' : teamId
        MRequisition.mUsage = supplyUse
        MRequisition.roomName = room
        MRequisition.roomCode = roomCode
        MRequisition.roomId = roomId
        MRequisition.editEmployeeName = Url.PDAEmployeeName
        MRequisition.editEmployeeId = Url.PDAEmployeeId
        MRequisition.editDate = tabulationDate
        MRequisition.projectName = title == '项目领料' ? projectName : ''
        MRequisition.projectId = title == '项目领料' ? projectId : ''
        MRequisition.factoryName = Url.PDAFname
        MRequisition.factoryId = Url.PDAFid
        MRequisition.state = '0'
        MRequisition.reqEmployeeName = supplyPerson == '请选择' ? '' : supplyPerson
        MRequisition.reqEmployeeId = supplyPerson == '请选择' ? '' : supplyPersonId
        MRequisition.formCode_WJ = '' // 待处理
        MRequisition.reqtype = reqtype
        MRequisition.remark = remark
        MRequisition.formCode = supplyNo
        MRequisition.workShopName = this.state.reqtype == '部门领料' ? '' : shop
        MRequisition.workShopId = this.state.reqtype == '部门领料' ? '' : shopId
        MRequisition.reqtype2 = reqtype2
        MRequisition.FormCode_PB = ''
        MRequisition.WastageRate = '.00'
        MRequisition.MixtureRrange = '' // 待处理
        MRequisition.CompTypeName = ''
        MRequisition.CompTypeId = ''

        return MRequisition
    }

    MRequisitionSubFormat = () => {
        const { materialInfos, rowguid, projectName, projectId } = this.state
        let MRequisitionSub = []
        materialInfos.map((item, index) => {
            let MRequisitionSubObj = {}
            MRequisitionSubObj._rowguid = this.getRandom()
            MRequisitionSubObj.mainid = '0'
            MRequisitionSubObj.projectName = projectName
            MRequisitionSubObj.projectId = projectId
            MRequisitionSubObj.materialCode = item.code
            MRequisitionSubObj.materialName = item.name
            MRequisitionSubObj.materialSize = item.size
            MRequisitionSubObj.materialUnit = item.unit
            MRequisitionSubObj.materialId = item.materialId
            MRequisitionSubObj.stockCount = item.stockCount2
            MRequisitionSubObj.requisitionCount = item.requisitionCount
            MRequisitionSubObj.accuActualCount = item.accuactualcount
            MRequisitionSubObj.remark = ''
            MRequisitionSubObj.sourceAutoid = item.id
            MRequisitionSubObj.materialType = item.typeCode
            MRequisitionSubObj.materialTypeName = item.type

            MRequisitionSub.push(MRequisitionSubObj)
        })
        return MRequisitionSub
    }

    getRandom = () => {
        var arr = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"],
            num = "";
        for (var i = 0; i < 32; i++) {
            num += arr[parseInt(Math.random() * 36)];
        }
        return num;
    }

    // 时间工具
    _DatePicker = (index) => {
        //const { ServerTime } = this.state
        return (
            <DatePicker
                customStyles={{
                    dateInput: styles.dateInput,
                    dateTouchBody: styles.dateTouchBody,
                    dateText: this.state.isAppDateSetting ? styles.dateText : styles.dateDisabled,
                }}
                iconComponent={<Icon name='caretdown' color={this.state.isAppDateSetting ? '#419fff' : "#666"} iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
                }
                showIcon={true}
                date={this.state.tabulationDate}
                onOpenModal={() => {
                    this.setState({ focusIndex: index })
                }}
                format="YYYY-MM-DD"
                mode="date"
                disabled={!this.state.isAppDateSetting}
                onDateChange={(value) => {
                    this.setState({
                        tabulationDate: value
                    })
                }}
            />
        )
    }

    uploadImage = () => {
        const { pictureSelect, isCheckPage, pageType } = this.state
        return (
            <View style={{ flexDirection: 'row' }}>
                {/*{this.imageView()}*/}
                {/*{*/}
                {/*    isCheckPage ? <View /> :*/}
                {/*        <AvatarAdd*/}
                {/*            pictureSelect={pictureSelect}*/}
                {/*            backgroundColor='#4D8EF5'*/}
                {/*            color='white'*/}
                {/*            title="材"*/}
                {/*            onPress={() => {*/}
                {/*                if (Url.isUsePhotograph == "YES") {*/}
                {/*                    if (pageType == 'CheckPage') {*/}
                {/*                        this.toast.show('照片不可修改')*/}
                {/*                    } else {*/}
                {/*                        this.camandlibFunc()*/}
                {/*                    }*/}
                {/*                } else {*/}
                {/*                    this.cameraFunc()*/}
                {/*                }*/}
                {/*            }} />*/}
                {/*}*/}
                {
                    !isCheckPage ? <Icon name='camera-alt' color='#4D8EF5' iconProps={{ size: 32 }} onPress={() => {
                        if (this.state.isAppPhotoAlbumSetting) {
                            this.camandlibFunc()
                        } else {
                            this.cameraFunc()
                        }
                    }}
                    ></Icon> : <View />

                }
            </View>
        )

    }

    camandlibFunc = () => {
        Alert.alert(
            '提示信息',
            '选择拍照方式',
            [{
                text: '取消',
                style: 'cancel'
            }, {
                text: '拍照',
                onPress: () => {
                    this.cameraFunc()
                }
            }, {
                text: '相册',
                onPress: () => {
                    this.camLibraryFunc()
                }
            }])
    }

    cameraFunc = () => {
        launchCamera(
            {
                mediaType: 'photo',
                includeBase64: false,
                maxHeight: parseInt(Url.isResizePhoto),
                maxWidth: parseInt(Url.isResizePhoto),
                saveToPhotos: true,

            },
            (response) => {
                if (response.uri == undefined) {
                    return
                }
                imageArr.push(response.uri)
                this.setState({
                    pictureSelect: true,
                    pictureUri: response.uri,
                    imageArr: imageArr
                })
                if (Url.isAppNewUpload) {
                    this.cameraPost(response.uri)
                } else {
                    this.setState({
                        pictureSelect: true,
                    })
                }
            },
        )
    }

    camLibraryFunc = () => {
        launchImageLibrary(
            {
                mediaType: 'photo',
                includeBase64: false,
                maxHeight: parseInt(Url.isResizePhoto),
                maxWidth: parseInt(Url.isResizePhoto),
            },
            (response) => {
                if (response.uri == undefined) {
                    return
                }
                imageArr.push(response.uri)
                this.setState({

                    pictureUri: response.uri,
                    imageArr: imageArr
                })
                if (Url.isAppNewUpload) {
                    this.cameraPost(response.uri)
                } else {
                    this.setState({
                        pictureSelect: true,
                    })
                }
            },
        )
    }

     // 展示/隐藏 放大图片
  handleZoomPicture = (flag, index) => {
    this.setState({
      imageOverSize: false,
      currShowImgIndex: index || 0
    })
  }

  // 加载放大图片弹窗
  renderZoomPicture = () => {
    const { imageOverSize, currShowImgIndex, imageFileArr } = this.state
    return (
      <OverlayImgZoomPicker
        isShowImage={imageOverSize}
        currShowImgIndex={currShowImgIndex}
        zoomImages={imageFileArr}
        callBack={(flag) => this.handleZoomPicture(flag)}
      ></OverlayImgZoomPicker>
    )
  }

  cameraPost = (uri) => {
        const { recid } = this.state
        this.toast.show('图片上传中', 2000)

        let formData = new FormData();
        let data = {};
        data = {
            "action": "savephote",
            "servicetype": "photo_file",
            "express": "D2979AB2",
            "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
            "data": {
                "fileflag": fileflag,
                "username": Url.PDAusername,
                "recid": recid
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        formData.append('img', {
            uri: uri,
            type: 'image/jpeg',
            name: 'img'
        })
        console.log("🚀 ~ file: concrete_pouring.js ~ line 672 ~ SteelCage ~ formData", formData)

        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            console.log(res.statusText);
            console.log(res);
            return res.json();
        }).then(resData => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 690 ~ SteelCage ~ resData", resData)
            if (resData.status == '100') {
                let fileid = resData.result.fileid;
                let recid = resData.result.recid;

                let tmp = {}
                tmp.fileid = fileid
                tmp.uri = uri
        tmp.url = uri
                imageFileArr.push(tmp)
                this.setState({
                    fileid: fileid,
                    recid: recid,
                    pictureSelect: true,
                    imageFileArr: imageFileArr
                })
                console.log("🚀 ~ file: concrete_pouring.js ~ line 704 ~ SteelCage ~ imageFileArr", imageFileArr)
                this.toast.show('图片上传成功');
                this.setState({
                    isPhotoUpdate: false
                })
            } else {
                Alert.alert('图片上传失败', resData.message)
                this.toast.close(1)
            }
        }).catch((error) => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 692 ~ SteelCage ~ error", error)
        });
    }

    cameraDelete = (item) => {
        let i = imageArr.indexOf(item)

        let formData = new FormData();
        let data = {};
        data = {
            "action": "deletephote",
            "servicetype": "photo_file",
            "express": "D2979AB2",
            "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
            "data": {
                "fileid": imageFileArr[i].fileid,
            }
        }
        formData.append('jsonParam', JSON.stringify(data))
        console.log("🚀 ~ file: concrete_pouring.js ~ line 733 ~ SteelCage ~ cameraDelete ~ formData", formData)
        fetch(Url.PDAurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        }).then(res => {
            console.log(res.statusText);
            console.log(res);
            return res.json();
        }).then(resData => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 745 ~ SteelCage ~ cameraDelete ~ resData", resData)
            if (resData.status == '100') {
                if (i > -1) {
                    imageArr.splice(i, 1)
                    imageFileArr.splice(i, 1)
                    this.setState({
                        imageArr: imageArr,
                        imageFileArr: imageFileArr
                    }, () => {
                        this.forceUpdate()
                    })
                }
                if (imageArr.length == 0) {
                    this.setState({
                        pictureSelect: false
                    })
                }
                this.toast.show('图片删除成功');
            } else {
                Alert.alert('图片删除失败', resData.message)
                this.toast.close(1)
            }
        }).catch((error) => {
            console.log("🚀 ~ file: concrete_pouring.js ~ line 761 ~ SteelCage ~ cameraDelete ~ error", error)
        });
    }

    imageView = () => {
        return (
            <View style={{ flexDirection: 'row' }}>
                {
                    this.state.pictureSelect ?
                        this.state.imageArr.map((item, index) => {
                            //if (index == 0) {
                            return (
                                <AvatarImg
                                    item={item}
                                    index={index}
                                    isRounded="false"
                                    marginRight="true"
                                    onPress={() => {
                                        this.setState({
                                            imageOverSize: true,
                                            uriShow: item
                                        })
                                    }}
                                    onLongPress={() => {
                                        Alert.alert(
                                            '提示信息',
                                            '是否删除材料照片',
                                            [{
                                                text: '取消',
                                                style: 'cancel'
                                            }, {
                                                text: '删除',
                                                onPress: () => {
                                                    this.cameraDelete(item)
                                                }
                                            }])
                                    }} />
                            )
                        }) : <View></View>
                }
            </View>
        )
    }

    // 扫码二维码
    comIDItem = (index) => {
        const { projectName, projectId, room, roomId, shop, team, materialInfos, title, reqtype, department } = this.state
        let message = ""
        if (title == '项目领料' && projectName == '请选择') {
            message = "请选择项目"
        } else if (title == '部门领料' && reqtype == '请选择') {
            message = "请选择领料类型"
        } else if (room == '请选择') {
            message = "请选择库房"
        } else if (reqtype != '部门领料' && shop == '请选择') {
            message = "请选择车间"
        } else if (reqtype != '部门领料' && team == '请选择') {
            message = "请选择班组"
        } else if (reqtype == '部门领料' && department == '请选择') {
            message = "请选择部门"
        }
        let materialIds = ''
        materialInfos.map((item, index) => {
            materialIds += index == materialInfos.length - 1 ? item.id : item.id + ','
        })
        return (
            <View>
                {
                    <View style={{ flexDirection: 'row' }}>

                        <Button
                            titleStyle={{ fontSize: 14 }}
                            title='选择材料'
                            //iconRight={true}
                            //icon={<Icon type='antdesign' name='scan1' color='white' size={14} />}
                            type='solid'
                            buttonStyle={{ paddingVertical: 6, backgroundColor: '#18BC28', marginVertical: 0 }}
                            onPress={() => {
                                if (message != "") {
                                    this.toast.show(message)
                                    return
                                }
                                this.props.navigation.navigate('SelectMaterial', {
                                    title: '选择材料',
                                    action: title,
                                    roomId: roomId,
                                    projectId: projectId,
                                    materialIds: materialIds
                                })
                            }}
                        />
                        <Text>   </Text>
                        <ScanButton
                            onPress={() => {
                                if (message != "") {
                                    this.toast.show(message)
                                    return
                                }
                                this.setState({
                                })
                                this.props.navigation.navigate('QRCode', {
                                    type: 'compid',
                                    page: 'MaterialProjectPicking'
                                })
                            }}
                            onLongPress={() => {
                                this.setState({
                                    type: 'compid',
                                    focusIndex: index
                                })
                                if (message != "") {
                                    this.toast.show(message)
                                    return
                                }
                                NativeModules.PDAScan.onScan();
                            }}
                            onPressOut={() => {
                                NativeModules.PDAScan.offScan();
                            }}
                        />
                    </View>
                }
            </View>
        )
    }

    // 下拉菜单
    downItem = (index) => {
        const { title, projectName, projectNameList, reqtype, reqtypeList, room, roomList, shop, shopList, team, teamList, supplyPerson, supplyPersonList, department, deptList } = this.state
        switch (index) {
            case '2': return (
                // 项目名称
                <View>{(
                    <TouchableCustom onPress={() => { this.isItemVisible(projectNameList, index) }} >
                        <IconDown text={projectName} />
                    </TouchableCustom>
                )}</View>
            ); break;
            case '3': return (
                // 库房名称
                <View>{(
                    <TouchableCustom onPress={() => { this.isItemVisible(roomList, index) }} >
                        <IconDown text={room} />
                    </TouchableCustom>
                )}</View>
            ); break;
            case '4': return (
                // 领料车间
                <View>{(
                    <TouchableCustom onPress={() => { this.isItemVisible(shopList, index) }} >
                        <IconDown text={shop} />
                    </TouchableCustom>
                )}</View>
            ); break;
            case '5': return (
                // 领料班组
                <View>{(
                    <TouchableCustom onPress={() => { this.isItemVisible(teamList, index) }} >
                        <IconDown text={team} />
                    </TouchableCustom>
                )}</View>
            ); break;
            case '8': return (
                // 领料人
                <View>{(
                    <TouchableCustom onPress={() => { this.isItemVisible(supplyPersonList, index) }} >
                        <IconDown text={supplyPerson} />
                    </TouchableCustom>
                )}</View>
            ); break;
            case '11': return (
                // 领料类型
                <View>{(
                    <TouchableCustom onPress={() => { this.isItemVisible(reqtypeList, index) }} >
                        <IconDown text={reqtype} />
                    </TouchableCustom>
                )}</View>
            ); break;
            case '12': return (
                // 生产车间
                <View>{(
                    <TouchableCustom onPress={() => { this.isItemVisible(deptList, index) }} >
                        <IconDown text={department} />
                    </TouchableCustom>
                )}</View>
            ); break;
            default: return;
        }
    }

    isItemVisible = (data, focusIndex) => {
        //console.log("************** " + focusIndex + "-" + i)
        if (typeof (data) != "undefined") {
            if (focusIndex == '8' && this.state.team == '请选择' && this.state.reqtype == '车间领料') {
                this.toast.show('请选择领料班组');
                return;
            }
            if (data != [] && data != "") {
                let title = this.state.title
                switch (focusIndex) {
                    case '2': this.setState({ projectNameVisible: true, bottomData: data, focusIndex: focusIndex }); break;
                    case '3': this.setState({ roomVisible: true, bottomData: data, focusIndex: focusIndex }); break;
                    case '4': this.setState({ shopVisible: true, bottomData: data, focusIndex: focusIndex }); break;
                    case '5': this.setState({ teamVisible: true, bottomData: data, focusIndex: focusIndex }); break;
                    case '8': this.setState({ supplyPersonVisible: true, bottomData: data, focusIndex: focusIndex }); break;
                    case '11': this.setState({ reqtypeVisible: true, bottomData: data, focusIndex: focusIndex }); break;
                    case '12': this.setState({ deptVisible: true, bottomData: data, focusIndex: focusIndex }); break;

                    default: return;
                }
            } else {
                this.toast.show("无数据")
            }
        } else {
            this.toast.show("无数据")
        }
    }

    jumpMaterialPickingInfo = (item) => {
        const { title } = this.state
        this.props.navigation.navigate('MaterialPickingInfo', {
            title: '领料信息',
            action: title,
            storageId: item.id,
            storageCode: item.code,
            storageName: item.name,
            size: item.size,
            unit: item.unit,
            requisitionCount: item.requisitionCount || "0",
            accuactualcount: item.accuactualcount,
            stockCount2: item.stockCount2
        })
    }

    render() {
        const { navigation } = this.props;
        //从主页面传url, 未来集成到一起
        const QRurl = navigation.getParam('QRurl') || '';
        const QRid = navigation.getParam('QRid') || ''
        const type = navigation.getParam('type') || '';
        let pageType = navigation.getParam('pageType') || ''

        if (type == 'compid') {
            QRcompid = QRid
        }
        const { title, bottomData, isCheckPage, focusIndex, supplyNo, projectName, reqtype, room, shop, team, supplyPerson, tabulator, supplyUse, remark, materialInfos, department, deptList,
            projectNameList, reqtypeList, projectNameVisible, reqtypeVisible, roomList, roomVisible, shopList, shopVisible, teamList, teamVisible, supplyPersonList, supplyPersonVisible, deptVisible, tabulationDate } = this.state

        if (this.state.isLoading) {
            return (
                <View style={styles.loading}>
                    <ActivityIndicator
                        animating={true}
                        color='#419FFF'
                        size="large" />
                </View>
            )
            //渲染页面
        } else {
            return (
                <View style={{ flex: 1 }}>
                    <Toast ref={(ref) => { this.toast = ref; }} position="center" />
                    <NavigationEvents
                        onWillFocus={this.UpdateControl}
                        onWillBlur={() => {
                            if (QRid != '') {
                                navigation.setParams({ QRid: '', type: '' })
                            }
                        }} />
                    <ScrollView ref={ref => this.scoll = ref} style={styles.mainView} showsVerticalScrollIndicator={false} >
                        <View style={styles.listView}>
                            <ListItemScan focusStyle={focusIndex == '0' ? styles.focusColor : {}} title='领料单号:' rightElement={supplyNo} />
                            <ListItemScan focusStyle={focusIndex == '1' ? styles.focusColor : {}} title='制表日期:' rightElement={() =>
                                isCheckPage ? <Text>{tabulationDate}</Text> :
                                    this._DatePicker('1')} />
                            {
                                title == "项目领料" ?
                                    isCheckPage ? <ListItemScan focusStyle={focusIndex == '2' ? styles.focusColor : {}} title='项目名称:' rightElement={projectName} /> :

                                        <ListItemScan focusStyle={focusIndex == '2' ? styles.focusColor : {}} title='项目名称:' rightElement={
                                            <View>{(
                                                <TouchableCustom onPress={() => {
                                                    this.props.navigation.navigate('SelectSupplier', {
                                                        title: "选择项目名称",
                                                        resData: projectNameList
                                                    })
                                                }} >
                                                    <IconDown text={projectName} />
                                                </TouchableCustom>
                                            )}</View>
                                        } />
                                    :
                                    title == "部门领料" ?
                                        isCheckPage ? <ListItemScan focusStyle={focusIndex == '11' ? styles.focusColor : {}} title='领料类型:' rightElement={reqtype} /> :
                                            <TouchableCustom underlayColor={'lightgray'} onPress={() => { this.isItemVisible(reqtypeList, '11') }}>
                                                <ListItemScan focusStyle={focusIndex == '11' ? styles.focusColor : {}} title='领料类型:' rightElement={this.downItem('11')} />
                                            </TouchableCustom>
                                        : <View />
                            }
                            {
                                isCheckPage ? <ListItemScan focusStyle={focusIndex == '3' ? styles.focusColor : {}} title='库房名称:' rightElement={room} /> :
                                    <TouchableCustom underlayColor={'lightgray'} onPress={() => { this.isItemVisible(roomList, '3') }}>
                                        <ListItemScan focusStyle={focusIndex == '3' ? styles.focusColor : {}} title='库房名称:' rightElement={this.downItem('3')} />
                                    </TouchableCustom>
                            }

                            {
                                reqtype == '部门领料' ?
                                    isCheckPage ? <ListItemScan focusStyle={focusIndex == '12' ? styles.focusColor : {}} title='领用部门:' rightElement={department} /> :
                                        <TouchableCustom underlayColor={'lightgray'} onPress={() => { this.isItemVisible(deptList, '12') }}>
                                            <ListItemScan focusStyle={focusIndex == '12' ? styles.focusColor : {}} title='领用部门:' rightElement={this.downItem('12')} />
                                        </TouchableCustom>
                                    :
                                    reqtype == '车间领料' ?
                                        isCheckPage ? <ListItemScan focusStyle={focusIndex == '4' ? styles.focusColor : {}} title='生产车间:' rightElement={shop} /> :
                                            <TouchableCustom underlayColor={'lightgray'} onPress={() => { this.isItemVisible(shopList, '4') }}>
                                                <ListItemScan focusStyle={focusIndex == '4' ? styles.focusColor : {}} title='生产车间:' rightElement={this.downItem('4')} />
                                            </TouchableCustom>
                                        : <View />
                            }

                            {
                                title == '项目领料' ?
                                    isCheckPage ? <ListItemScan focusStyle={focusIndex == '4' ? styles.focusColor : {}} title='领料车间:' rightElement={shop} /> :
                                        <TouchableCustom underlayColor={'lightgray'} onPress={() => { this.isItemVisible(shopList, '4') }}>
                                            <ListItemScan focusStyle={focusIndex == '4' ? styles.focusColor : {}} title='领料车间:' rightElement={this.downItem('4')} />
                                        </TouchableCustom>
                                    : <View />
                            }
                            {
                                reqtype == '部门领料' ? <View /> :
                                    isCheckPage ? <ListItemScan focusStyle={focusIndex == '5' ? styles.focusColor : {}} title='领料班组:' rightElement={team} /> :
                                        <TouchableCustom underlayColor={'lightgray'} onPress={() => { this.isItemVisible(teamList, '5') }}>
                                            <ListItemScan focusStyle={focusIndex == '5' ? styles.focusColor : {}} title='领料班组:' rightElement={this.downItem('5')} />
                                        </TouchableCustom>
                            }
                            <ListItemScan focusStyle={focusIndex == '6' ? styles.focusColor : {}} title='领料用途:' rightElement={() =>
                                isCheckPage ? <Text>{supplyUse}</Text> :
                                    <View>
                                        <Input
                                            containerStyle={styles.quality_input_container}
                                            inputContainerStyle={styles.inputContainerStyle}
                                            inputStyle={[styles.quality_input_, { top: 7 }]}
                                            placeholder='请填写'
                                            value={supplyUse}
                                            onChangeText={(value) => {
                                                this.setState({
                                                    supplyUse: value
                                                })
                                            }} />
                                    </View>}
                            />
                            <ListItemScan
                                focusStyle={focusIndex == '7' ? styles.focusColor : {}}
                                title='材料明细:'
                                onPressIn={() => {
                                    this.setState({
                                        type: 'compid',
                                        focusIndex: 7
                                    })
                                    NativeModules.PDAScan.onScan();
                                }}
                                onPress={() => {
                                    this.setState({
                                        type: 'compid',
                                        focusIndex: 7
                                    })
                                    NativeModules.PDAScan.onScan();
                                }}
                                onPressOut={() => {
                                    NativeModules.PDAScan.offScan();
                                }}
                                rightElement={isCheckPage ? <View /> :
                                    <View>
                                        {this.comIDItem(7)}
                                    </View>}
                            />

                            {
                                materialInfos.map((item, index) => {
                                    return (
                                        <TouchableCustom
                                            onLongPress={() => {
                                                !isCheckPage ?
                                                    Alert.alert(
                                                        '提示信息',
                                                        '是否删除材料信息',
                                                        [{
                                                            text: '取消',
                                                            style: 'cancel'
                                                        }, {
                                                            text: '删除',
                                                            onPress: () => {
                                                                let mis = this.state.materialInfos
                                                                mis.splice(index, 1)
                                                                this.setState({
                                                                    materialInfos: mis
                                                                })
                                                                this.forceUpdate()
                                                            }
                                                        }]) : ""
                                            }}
                                            onPress={() => { isCheckPage ? <View /> : this.jumpMaterialPickingInfo(item) }}
                                        >
                                            <View>
                                                <Card containerStyle={stylesMeterial.card_containerStyle}>
                                                    <View style={[{
                                                        flexDirection: 'row',
                                                        justifyContent: 'flex-start',
                                                        alignItems: 'center',
                                                    }]}>
                                                        <View style={{ flex: 1 }}>
                                                            <ListItem
                                                                containerStyle={stylesMeterial.list_container_style}
                                                                title={item.code}
                                                                rightTitle={
                                                                    <View style={{
                                                                        backgroundColor:
                                                                            item.type == '直接材料' ? '#31C731' :
                                                                                item.type == '小五金' ? '#DAC512' :
                                                                                    item.type == '甲供材料' ? '#4D8EF5' : 'transparent'
                                                                    }}>
                                                                        <Text style={{ color: 'white', fontSize: 13 }}> {item.type} </Text>
                                                                    </View>
                                                                }
                                                                titleStyle={stylesMeterial.title}
                                                                rightTitleStyle={stylesMeterial.rightTitle}
                                                            />
                                                            <ListItem
                                                                containerStyle={stylesMeterial.list_container_style}
                                                                title={item.name}
                                                                //rightTitle={'单价:  2200' }
                                                                titleStyle={stylesMeterial.title}
                                                                rightTitleStyle={stylesMeterial.rightTitle}
                                                            />
                                                            <ListItem
                                                                containerStyle={stylesMeterial.list_container_style}
                                                                title={item.size}
                                                                //rightTitle={}
                                                                titleStyle={stylesMeterial.title}
                                                                ightTitleStyle={stylesMeterial.rightTitle}
                                                            />
                                                            <ListItem
                                                                containerStyle={stylesMeterial.list_container_style}
                                                                title={item.requisitionCount + "(" + item.unit + ")"}
                                                                //rightTitle={'是否暂估:  是'}
                                                                titleStyle={stylesMeterial.title}
                                                                rightTitleStyle={stylesMeterial.rightTitle}
                                                            />
                                                        </View>

                                                        {
                                                            isCheckPage ? <View /> :
                                                                <Icon type='antdesign' name='right' color='#999' onPress={() => { this.jumpMaterialPickingInfo(item) }} />
                                                        }
                                                    </View>
                                                </Card>
                                                <Text />
                                            </View>
                                        </TouchableCustom>
                                    )
                                })
                            }


                            {
                                isCheckPage ? <ListItemScan focusStyle={focusIndex == '8' ? styles.focusColor : {}} title='领料人:' rightElement={supplyPerson} />
                                    :
                                    reqtype == '部门领料' ?
                                        <ListItemScan focusStyle={focusIndex == '8' ? styles.focusColor : {}} title='领料人:' rightElement={() =>
                                            isCheckPage ? <Text>{supplyPerson}</Text> :
                                                <View>
                                                    <Input
                                                        containerStyle={styles.quality_input_container}
                                                        inputContainerStyle={styles.inputContainerStyle}
                                                        inputStyle={[styles.quality_input_, { top: 7 }]}
                                                        placeholder='请填写'
                                                        value={supplyPerson}
                                                        onChangeText={(value) => {
                                                            DeviceStorage.save('MaterialDeptPickingSupplyPerson', value);
                                                            this.setState({
                                                                supplyPerson: value
                                                            })
                                                        }} />
                                                </View>}
                                        />
                                        :
                                        <TouchableCustom underlayColor={'lightgray'} onPress={() => { this.isItemVisible(supplyPersonList, '8') }}>
                                            <ListItemScan focusStyle={focusIndex == '8' ? styles.focusColor : {}} title='领料人:' rightElement={this.downItem('8')} />
                                        </TouchableCustom>
                            }
                            <ListItemScan focusStyle={focusIndex == '9' ? styles.focusColor : {}} title='制表人:' rightElement={tabulator} />
                            <ListItemScan focusStyle={focusIndex == '10' ? styles.focusColor : {}} title='备注:' rightElement={() =>
                                isCheckPage ? <Text>{remark}</Text> :
                                    <View>
                                        <Input
                                            containerStyle={styles.quality_input_container}
                                            inputContainerStyle={styles.inputContainerStyle}
                                            inputStyle={[styles.quality_input_, { top: 7 }]}
                                            placeholder='请填写'
                                            value={remark}
                                            onChangeText={(value) => {
                                                this.setState({
                                                    remark: value
                                                })
                                            }} />
                                    </View>}
                            />
                            <ListItemScan focusStyle={focusIndex == '13' ? styles.focusColor : {}} title='附件:' rightElement={() =>
                                this.uploadImage()} />
                            {
                                this.state.pictureSelect && this.state.imageArr != [] && this.state.imageArr != "" ?
                                    <ListItemScan focusStyle={focusIndex == '14' ? styles.focusColor : {}} title={this.imageView()} />
                                    : <View />
                            }
                        </View>
                        {isCheckPage ? <View /> :
                            <FormButton
                                backgroundColor='#17BC29'
                                onPress={() => {
                                    this.PostData()
                                }}
                                title='保存'
                                disabled={this.state.isPhotoUpdate}
                            />
                        }

                        {/*项目名称底栏*/}
                        <BottomSheet
                            onBackdropPress={() => {
                                this.setState({
                                    projectNameVisible: false
                                })
                            }}
                            isVisible={projectNameVisible && !this.state.isCheckPage} onRequestClose={() => {
                                this.setState({
                                    projectNameVisible: false
                                })
                            }}
                        >
                            <ScrollView style={styles.bottomsheetScroll}>
                                {bottomData.map((item, index) => {
                                    return (
                                        <TouchableCustom
                                            onPress={() => {
                                                this.setState({
                                                    projectName: item.name,
                                                    projectNameVisible: false,
                                                    projectId: item.id,
                                                    materialInfos: []
                                                })
                                            }}
                                        >
                                            <View style={{
                                                paddingVertical: 18,
                                                backgroundColor: 'white',
                                                //alignItems: 'center',
                                                borderBottomColor: '#DDDDDD',
                                                borderBottomWidth: 0.6,
                                                flexDirection: 'row',
                                                justifyContent: 'space-between',
                                                paddingLeft: 20,
                                                paddingRight: 20
                                            }}>
                                                <Text style={{ color: '#333', fontSize: 14 }}>{item.name}</Text>
                                                <Text style={{ color: '#333', fontSize: 14 }}>{item.code}</Text>
                                            </View>
                                        </TouchableCustom>
                                    )
                                })}
                            </ScrollView>
                            <TouchableHighlight
                                onPress={() => {
                                    this.setState({ projectNameVisible: false })
                                }}>
                                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
                            </TouchableHighlight>
                        </BottomSheet>

                        {/*领料类型底栏*/}
                        <BottomSheet
                            onBackdropPress={() => {
                                this.setState({
                                    reqtypeVisible: false
                                })
                            }}
                            isVisible={reqtypeVisible && !this.state.isCheckPage} onRequestClose={() => {
                                this.setState({
                                    reqtypeVisible: false
                                })
                            }}
                        >
                            <ScrollView style={styles.bottomsheetScroll}>
                                {bottomData.map((item, index) => {
                                    return (
                                        <TouchableCustom
                                            onPress={() => {
                                                this.setState({
                                                    reqtype: item,
                                                    reqtypeVisible: false,
                                                    materialInfos: []
                                                })
                                                if (item == '部门领料') {
                                                    this.setState({
                                                        shop: '请选择',
                                                        team: '请选择',
                                                        supplyPerson: this.state.devPerson
                                                    })
                                                } else if (item == '车间领料') {
                                                    this.setState({
                                                        department: '请选择',
                                                        supplyPerson: '请选择'
                                                    })
                                                }
                                            }}
                                        >
                                            <BottomItem backgroundColor='white' color="#333" title={item} />
                                        </TouchableCustom>
                                    )
                                })}
                            </ScrollView>
                            <TouchableHighlight
                                onPress={() => {
                                    this.setState({ reqtypeVisible: false })
                                }}>
                                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
                            </TouchableHighlight>
                        </BottomSheet>

                        {/*库房名称底栏*/}
                        <BottomSheet
                            onBackdropPress={() => {
                                this.setState({
                                    roomVisible: false
                                })
                            }}
                            isVisible={roomVisible && !this.state.isCheckPage} onRequestClose={() => {
                                this.setState({
                                    roomVisible: false
                                })
                            }}
                        >
                            <ScrollView style={styles.bottomsheetScroll}>
                                {bottomData.map((item, index) => {
                                    return (
                                        <TouchableCustom
                                            onPress={() => {
                                                this.setState({
                                                    room: item.name,
                                                    roomVisible: false,
                                                    roomId: item.id,
                                                    roomCode: item.code,
                                                    materialInfos: []
                                                });
                                                QRidArr = [];
                                            }}
                                        >
                                            <View style={{
                                                paddingVertical: 18,
                                                backgroundColor: 'white',
                                                //alignItems: 'center',
                                                borderBottomColor: '#DDDDDD',
                                                borderBottomWidth: 0.6,
                                                flexDirection: 'row',
                                                justifyContent: 'space-between',
                                                paddingLeft: 20,
                                                paddingRight: 20
                                            }}>
                                                <Text style={{ color: '#333', fontSize: 14 }}>{item.name}</Text>
                                                <Text style={{ color: '#333', fontSize: 14 }}>{item.code}</Text>
                                            </View>
                                        </TouchableCustom>
                                    )
                                })}
                            </ScrollView>
                            <TouchableHighlight
                                onPress={() => {
                                    this.setState({ roomVisible: false })
                                }}>
                                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
                            </TouchableHighlight>
                        </BottomSheet>

                        {/*领料车间底栏*/}
                        <BottomSheet
                            onBackdropPress={() => {
                                this.setState({
                                    shopVisible: false
                                })
                            }}
                            isVisible={shopVisible && !this.state.isCheckPage} onRequestClose={() => {
                                this.setState({
                                    shopVisible: false
                                })
                            }}
                        >
                            <ScrollView style={styles.bottomsheetScroll}>
                                {bottomData.map((item, index) => {
                                    let title = item.name
                                    return (
                                        <TouchableCustom
                                            onPress={() => {
                                                this.setState({
                                                    shop: item.name,
                                                    shopId: item.id,
                                                    shopVisible: false
                                                })
                                            }}
                                        >
                                            <BottomItem backgroundColor='white' color="#333" title={title} />
                                        </TouchableCustom>
                                    )
                                })}
                            </ScrollView>
                            <TouchableHighlight
                                onPress={() => {
                                    this.setState({ shopVisible: false })
                                }}>
                                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
                            </TouchableHighlight>
                        </BottomSheet>

                        {/*领料班组底栏*/}
                        <BottomSheet
                            onBackdropPress={() => {
                                this.setState({
                                    teamVisible: false
                                })
                            }}
                            isVisible={teamVisible && !this.state.isCheckPage} onRequestClose={() => {
                                this.setState({
                                    teamVisible: false
                                })
                            }}
                        >
                            <ScrollView style={styles.bottomsheetScroll}>
                                {bottomData.map((item, index) => {
                                    let title = item.name
                                    return (
                                        <TouchableCustom
                                            onPress={() => {
                                                if (this.state.team != item.name) {
                                                    this.setState({
                                                        supplyPerson: "请选择"
                                                    })
                                                }
                                                this.setState({
                                                    team: item.name,
                                                    teamId: item.id,
                                                    teamVisible: false,
                                                    supplyPersonList: item.supplyPerson
                                                })
                                            }}
                                        >
                                            <BottomItem backgroundColor='white' color="#333" title={title} />
                                        </TouchableCustom>
                                    )
                                })}
                            </ScrollView>
                            <TouchableHighlight
                                onPress={() => {
                                    this.setState({ teamVisible: false })
                                }}>
                                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
                            </TouchableHighlight>
                        </BottomSheet>

                        {/*领料人底栏*/}
                        <BottomSheet
                            onBackdropPress={() => {
                                this.setState({
                                    supplyPersonVisible: false
                                })
                            }}
                            isVisible={supplyPersonVisible && !this.state.isCheckPage} onRequestClose={() => {
                                this.setState({
                                    supplyPersonVisible: false
                                })
                            }}
                        >
                            <ScrollView style={styles.bottomsheetScroll}>
                                {bottomData.map((item, index) => {
                                    let title = item.name
                                    return (
                                        <TouchableCustom
                                            onPress={() => {
                                                this.setState({
                                                    supplyPerson: item.name,
                                                    supplyPersonId: item.id,
                                                    supplyPersonVisible: false
                                                })
                                            }}
                                        >
                                            <BottomItem backgroundColor='white' color="#333" title={title} />
                                        </TouchableCustom>
                                    )
                                })}
                            </ScrollView>
                            <TouchableHighlight
                                onPress={() => {
                                    this.setState({ supplyPersonVisible: false })
                                }}>
                                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
                            </TouchableHighlight>
                        </BottomSheet>

                        {/*领用部门底栏*/}
                        <BottomSheet
                            onBackdropPress={() => {
                                this.setState({
                                    deptVisible: false
                                })
                            }}
                            isVisible={deptVisible && !this.state.isCheckPage} onRequestClose={() => {
                                this.setState({
                                    deptVisible: false
                                })
                            }}
                        >
                            <ScrollView style={styles.bottomsheetScroll}>
                                {bottomData.map((item, index) => {
                                    let title = item.name
                                    return (
                                        <TouchableCustom
                                            onPress={() => {
                                                this.setState({
                                                    department: item.name,
                                                    departmentId: item.id,
                                                    deptVisible: false
                                                })
                                            }}
                                        >
                                            <BottomItem backgroundColor='white' color="#333" title={title} />
                                        </TouchableCustom>
                                    )
                                })}
                            </ScrollView>
                            <TouchableHighlight
                                onPress={() => {
                                    this.setState({ deptVisible: false })
                                }}>
                                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
                            </TouchableHighlight>
                        </BottomSheet>
                        {/*照片放大*/}
                        {this.renderZoomPicture()}
                        {/* {overlayImg(obj = {
                            onRequestClose: () => {
                                this.setState({ imageOverSize: !this.state.imageOverSize }, () => {
                                    console.log("🚀 ~ file: equipment_maintenance.js:1696 ~ render ~ imageOverSize:", this.state.imageOverSize)
                                })
                            },
                            onPress: () => {
                                this.setState({
                                    imageOverSize: !this.state.imageOverSize
                                })
                            },
                            uri: this.state.uriShow,
                            isVisible: this.state.imageOverSize
                        })
                        } */}
                    </ScrollView>
                </View>
            )
        }
    }
}

const stylesMeterial = StyleSheet.create({
    card_containerStyle: {
        borderRadius: 10,
        shadowOpacity: 0,
        backgroundColor: '#f8f8f8',
        borderWidth: 0,
        paddingVertical: 13,
        elevation: 0
    },
    title: {
        fontSize: 13,
        color: '#999'
    },
    rightTitle: {
        fontSize: 13,
        textAlign: 'right',
        lineHeight: 14,
        flexWrap: 'wrap',
        width: width / 2,
        color: '#333'
    },
    list_container_style: { marginVertical: 0, paddingVertical: 9, backgroundColor: 'transparent' }

})