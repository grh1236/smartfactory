import React from 'react';
import { View, ScrollView, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, ListItem, BottomSheet, Tooltip, Overlay, Header } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import ScanButton from '../Componment/ScanButton';
import { styles } from '../Componment/PDAStyles';
import { ToDay, YesterDay, BeforeYesterDay } from '../../CommonComponents/Data';
import { deviceWidth, RFT } from '../../Url/Pixal';
import CardList from "../Componment/CardList";
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import ListItemScan_child from '../Componment/ListItemScan_child';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import CheckBoxScan from '../Componment/CheckBoxScan';
import AvatarAdd from '../Componment/AvatarAdd';
import AvatarImg from '../Componment/AvatarImg';

import { DeviceEventEmitter, NativeModules } from 'react-native';
import Pdf from 'react-native-pdf';
import WebView from 'react-native-webview';
import PDATopBottom from '../CheckPage/MainCheckPage';


const { height, width } = Dimensions.get('window') //获取宽高

class BackIcon extends React.PureComponent {
  render() {
    return (
      <TouchableCustom
        onPress={this.props.onPress} >
        <Icon
          name='arrow-back'
          color='#419FFF' />
      </TouchableCustom>
    )
  }
}

export default class BuriedCard extends React.Component {
  constructor() {
    super();
    this.state = {
      monitorSetup: '',
      isroom: false,
      isteam: false,
      iskeeper: false,
      bottomData: [],
      bottomVisible: false,
      resData: [],
      formCode: '',
      rowguid: '',
      steelCageIDs: '',
      room: [],
      roomselect: '请选择',
      roomId: '',
      team: [],
      teamselect: '请选择',
      teamId: '',
      labourName: '',
      QRkeeperName: '',
      QRkeeperID: '',
      keeper: [],
      keeperselect: '请选择',
      keeperId: '',
      roomtext: '',
      ServerTime: '',
      QRid: '',
      QRStock: '',
      rowguid: '',
      focusIndex: 0,
      type: '',
      //图纸
      isPaperTypeVisible: false,
      paperList: ["模版图", "配筋图", "预埋件装配图", "连接件排版图"],
      paperUrl: "",
      FileName: "",
      isPDFVisible: false,
      //构件
      compCode: '',
      compId: '',
      compData: {},
      isGetcomID: false,
      //RFID
      isRFIDRead: false,
      RFID: "",
      isWriteRFID: false,
      isCompModalOpen: false
    }

    //this.requestCameraPermission = this.requestCameraPermission.bind(this)
  }


  componentDidMount() {
    this.resData()

    NativeModules.RFIDModule.isPowerOn();

    //通过使用DeviceEventEmitter模块来监听事件
    this.isPowerOn = DeviceEventEmitter.addListener('isPowerOn', (Event) => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 115 ~ SteelCage ~ Event.isPowerOn", Event.isPowerOn)
      if (typeof Event.isPowerOn != undefined) {
        let isPowerOn = Event.isPowerOn
        console.log("🚀 ~ file: buried_card.js:78 ~ SteelCage ~ this.isPowerOn=DeviceEventEmitter.addListener ~ isPowerOn:", isPowerOn)
        if (isPowerOn == "true") {
          this.toast.show('模块初始化成功')
        } else {
          Alert.alert("错误", "模块初始化失败", [
            {
              text: "返回上一级", onPress: () => {
                this.props.navigation.goBack();
              }
            }
          ])
        }
      }
    });
    this.iDataRFIDListener = DeviceEventEmitter.addListener('iDataRFID', (Event) => {
      console.log("🚀 ~ file: buried_card.js:89 ~ SteelCage ~ this.iDataRFID=DeviceEventEmitter.addListener ~ Event:", Event)
      if (typeof Event.RFIDResult != "undefined") {
        let RFIDResult = Event.RFIDResult
        console.log("🚀 ~ file: buried_card.js:96 ~ SteelCage ~ this.iDataRFIDListener=DeviceEventEmitter.addListener ~ RFIDResult:", RFIDResult)

      }
    })
    this.iRFIDRdataListener = DeviceEventEmitter.addListener('iRdata', (Event) => {
      console.log("🚀 ~ file: buried_card.js:89 ~ SteelCage ~ this.iDataRFID=DeviceEventEmitter.addListener ~ Event:", Event)
      if (typeof Event.iRdataResult != "undefined") {
        let iRdataResult = Event.iRdataResult
        console.log("🚀 ~ file: buried_card.js:96 ~ SteelCage ~ this.iRFIDRdataListener=DeviceEventEmitter.addListener ~ iRdataResult:", iRdataResult)
        if (iRdataResult == null) {
          this.toast.show("读取失败")
          //Alert.alert('错误提示', "读取失败")
        } else {
          this.setState({
            isRFIDRead: true,
            RFID: iRdataResult,
            type: 'compid',
            focusIndex: 1,
            isCompModalOpen: true
          })
        }
      }
    })
    this.iRFIDWdataListener = DeviceEventEmitter.addListener('iWdata', (Event) => {
      console.log("🚀 ~ file: buried_card.js:89 ~ SteelCage ~ this.iDataRFID=DeviceEventEmitter.addListener ~ Event:", Event)
      if (typeof Event.iWdataResult != "undefined") {
        let iWdataResult = Event.iWdataResult
        if (iWdataResult.indexOf('成功') != -1) {
          this.setState({
            isWriteRFID: true
          })
        }
        this.toast.show(iWdataResult)
        //Alert.alert('提示', iWdataResult)
      }
    })
    //通过使用DeviceEventEmitter模块来监听事件
    this.iDataScan = DeviceEventEmitter.addListener('iDataScan', (Event) => {
      console.log("🚀 ~ file: buried_card.js:134 ~ SteelCage ~ this.iDataScan=DeviceEventEmitter.addListener ~ Event:", Event)
      if (typeof Event.ScanResult != undefined) {
        let data = Event.ScanResult
        console.log("🚀 ~ file: buried_card.js:127 ~ SteelCage ~ this.iDataScan=DeviceEventEmitter.addListener ~ data:", data)
        let arr = data.split("=");
        console.log("🚀 ~ file: buried_card.js:129 ~ SteelCage ~ this.iDataScan=DeviceEventEmitter.addListener ~ arr:", arr)
        let id = ''
        id = arr[arr.length - 1]
        console.log("🚀 ~ file: buried_card.js:132 ~ SteelCage ~ this.iDataScan=DeviceEventEmitter.addListener ~ id:", id)
        if (this.state.type == 'compid') {
          this.GetcomID(id)
        }
      }
    });
  }

  componentWillUnmount() {
    this.isPowerOn.remove();
    this.iDataRFIDListener.remove();
    this.iRFIDRdataListener.remove();
    this.iRFIDWdataListener.remove();
    this.iDataScan.remove();
  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  resData = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "init",
      "servicetype": "pda",
      "express": "249E955B",
      "ciphertext": "ddd23c2aae6f7b7e6e591356efe0edd2",
      "data": {
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: buried_card.js:176 ~ BuriedCard ~ formData:", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: buried_card.js:186 ~ BuriedCard ~ resData:", resData)
      let ServerTime = resData.result.serverTime
      let rowguid = resData.result.rowguid
      this.setState({
        resData: resData,
        ServerTime: ServerTime,
        rowguid: rowguid,
      })
    }).catch((error) => {
      console.log("🚀 ~ file: steel_cage_storage.js ~ line 75 ~ SteelCage ~ error", error)
    });
  }

  QRIdtrans = (type) => {
    const { navigation } = this.props;
    const QRid = navigation.getParam('QRid') || '';
    if (type == 'keeper') {
      this.state.keeper.map((item, index) => {
        if (QRid == item.keeperId) {
          this.setState({
            keeperselect: item.keeperName
          })
        }
      })
    }
  }

  PostData = () => {
    const { formCode, RFID, isWriteRFID, compCode, compId, ServerTime, roomId, rowguid, } = this.state

    if (RFID.length == 0) {
      this.toast.show("请读取RFID")
      return
    }
    if (!isWriteRFID) {
      this.toast.show("写卡未成功")
      return
    }
    if (compCode.length == 0) {
      this.toast.show("请扫描构件编码")
      return
    }


    this.toast.show(saveLoading, 0)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "saveBuriedCard",
      "servicetype": "pda",
      "express": "4ED13340",
      "ciphertext": "3221ac6498a0e8b94ef042a37a728588",
      "data": {
        "compId": compId,
        "Buriedtime": ServerTime,
        "Burieduser": Url.PDAEmployeeName,
        "BurieduserID": Url.PDAEmployeeId,
        "factoryId": Url.PDAFid,
        "rowguid": rowguid,
        "ServerTime": ServerTime,
        "rowguid": rowguid
      }
    }
    console.log("🚀 ~ file: steel_cage_storage.js ~ line 191 ~ SteelCage ~ data", data)
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: steel_cage_storage.js ~ line 193 ~ SteelCage ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        this.toast.show('保存成功');
        setTimeout(() => {
          this.props.navigation.replace(this.props.navigation.getParam("page"), {
            title: this.props.navigation.getParam("title"),
            pageType: this.props.navigation.getParam("pageType"),
            page: this.props.navigation.getParam("page"),
            isAppPhotoSetting: this.props.navigation.getParam("isAppPhotoSetting"),
            isAppDateSetting: this.props.navigation.getParam("isAppDateSetting"),
            isAppPhotoAlbumSetting: this.props.navigation.getParam("isAppPhotoAlbumSetting"),
          })
          //this.props.navigation.navigate('PDAMainStack')
        }, 400)
      } else {
        this.toast.show('保存失败')
        Alert.alert('保存失败', resData.message)
      }
      console.log("🚀 ~ file: steel_cage_storage.js ~ line 195 ~ SteelCage ~ resData", resData)
    }).catch((error) => {
      this.toast.close()
      if (error.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('保存失败', "响应内容不是合法JSON格式")
        return
      }
      if (error.toString().indexOf('Network request faile') != -1) {
        Alert.alert('保存失败', "网络请求错误")
        return
      }
      Alert.alert('保存失败', error.toString())
      console.log("🚀 ~ file: steel_cage_storage.js ~ line 75 ~ SteelCage ~ error", error)
    });
  }

  func = (state) => {
    console.log("🚀 ~ file: qualityinspection.js ~ line 1166 ~ QualityInspection ~ state", state)
    this.setState({
      isPaperTypeVisible: true
    })
  }

  paperResData = (type) => {
    let formData = new FormData();
    let data = {}
    data = {
      "action": "getDrawings",
      "servicetype": "pda",
      "express": "FCEF95AF",
      "ciphertext": "985e3620ae26a2f380c723b60ce9b525",
      "data": {
        "type": type,
        "compId": this.state.compId,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 314 ~ QualityInspection ~ resData", resData)
      if (resData.status == "100") {

        let paperUrl = resData.result.Url
        let FileName = resData.result.FileName

        this.setState({
          paperUrl: paperUrl,
          FileName: FileName,
          isPDFVisible: true,
          isPaperTypeVisible: false
        })
      } else {
        Alert.alert('错误', resData.message)
        this.setState({ isPaperTypeVisible: false })
      }
    }).catch((error) => {
    });
  }

  GetcomID = (id) => {
    console.log("🚀 ~ file: qualityinspection.js ~ line 194 ~ SteelCage ~ GetcomID", id)
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getCompInfoFromProductStart",
      "servicetype": "pda",
      "express": "0F51EF23",
      "ciphertext": "a91d6dc5c5a20f2a35939b6e2d91a0ef",
      "data": {
        "compId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: qualityinspection.js ~ line 367 ~ HideInspection ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: buried_card.js:327 ~ SteelCage ~ resData:", resData)
      if (resData.status == '100') {
        let compCode = resData.result.compCode
        let compId = resData.result.compId
        this.setState({
          compData: resData,
          compCode: compCode,
          compId: compId,
          isGetcomID: true,
          iscomIDdelete: false
        })
        //setTimeout(() => { this.scoll.scrollToEnd() }, 300)
      } else {
        console.log('resData', resData)
        Alert.alert('错误', resData.message)
        this.setState({
          compId: ""
        })
        //this.input1.focus()
      }

    }).catch(err => {
      this.toast.show("扫码错误")
      if (err.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('扫码失败', "响应内容不是合法JSON格式")
        return
      }
      if (err.toString().indexOf('Network request faile') != -1) {
        Alert.alert('扫码失败', "网络请求错误")
        return
      }
      Alert.alert('扫码失败', err.toString())
    })
  }

  comIDItem = (index) => {
    const { isGetcomID, buttondisable, compCode, compId, focusIndex } = this.state
    return (
      <View>
        {
          !isGetcomID ?
            <View style={{ flexDirection: 'row' }}>
              <View>
                <Input
                  ref={ref => { this.input1 = ref }}
                  containerStyle={styles.scan_input_container}
                  inputContainerStyle={styles.scan_inputContainerStyle}
                  inputStyle={[styles.scan_input]}
                  placeholder='请输入'
                  keyboardType='numeric'
                  value={compId}
                  onChangeText={(value) => {
                    value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
                    value = value.replace(/^\./g, ""); //验证第一个字符是数字
                    value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
                    //value = value.replace(/\b(0+)/gi, ""); //清楚开头的0
                    this.setState({
                      compId: value
                    })
                  }}
                  onFocus={() => {
                    this.setState({
                      focusIndex: index,
                      type: 'compid',
                    })
                  }}
                  onSubmitEditing={() => {
                    let inputComId = this.state.compId
                    console.log("🚀 ~ file: qualityinspection.js ~ line 468 ~ QualityInspection ~ inputComId", inputComId)
                    inputComId = inputComId.replace(/\b(0+)/gi, "")
                    this.GetcomID(inputComId)
                  }}
                />
              </View>
              <ScanButton
                onPress={() => {
                  this.setState({
                    iscomIDdelete: false,
                    buttondisable: true,
                    focusIndex: index
                  })
                  this.props.navigation.navigate('QRCode', {
                    type: 'compid',
                    page: 'HideInspection'
                  })
                }}
                onLongPress={() => {
                  this.setState({
                    type: 'compid',
                    focusIndex: index
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}
              />
            </View>
            :
            <TouchableCustom
              onPress={() => {
                console.log('onPress')
              }}
              onLongPress={() => {
                console.log('onLongPress')
                Alert.alert(
                  '提示信息',
                  '是否删除构件信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      this.setState({
                        compData: [],
                        compCode: '',
                        compId: '',
                        iscomIDdelete: true,
                        isGetcomID: false,
                      })
                    }
                  }])
              }} >
              <Text>{compCode}</Text>
            </TouchableCustom>
        }
      </View>

    )
  }

  render() {

    const { resData, monitorSetup, formCode, compId, compCode, compData, isRFIDRead, RFID, focusIndex, ServerTime, team, teamselect, keeper, keeperselect, room, roomselect, labourName, bottomVisible, bottomData, paperUrl } = this.state

    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          <ScrollView ref={ref => this.scoll = ref} style={styles.mainView} showsVerticalScrollIndicator={false} >
            <View style={styles.listView}>
              <ListItemScan
                isButton={!this.state.isRFIDRead}
                focusStyle={focusIndex == '0' ? styles.focusColor : {}}
                title='RFID读取'
                rightElement={
                  <View>
                    {
                      <ScanButton
                        title='读取RFID码'
                        iconRight={true}
                        icon={<Icon type='antdesign' name='wifi' color='white' size={14} />}
                        onPress={() => {
                          this.setState({
                            type: 'RFID',
                            focusIndex: 0
                          })
                          NativeModules.RFIDModule.readTag();
                        }}
                      />

                    }

                  </View>
                }
                bottomDivider
              />
            </View>

            <PDATopBottom  />

          </ScrollView>


          <Overlay
            fullScreen={true}
            animationType='fade'
            isVisible={this.state.isPDFVisible}
            onRequestClose={() => {
              this.setState({ isPDFVisible: !this.state.isPDFVisible });
            }}
          >
            <Header
              containerStyle={{ height: 45, paddingTop: 0, top: -5 }}
              centerComponent={{ text: '图纸预览', style: { color: 'gray', fontSize: 20 } }}
              rightComponent={<BackIcon
                onPress={() => {
                  this.setState({
                    isPDFVisible: !this.state.isPDFVisible
                  });
                }} />}
              backgroundColor='white'
            />
            <View style={{ flex: 1 }}>
              <Pdf
                source={{
                  uri: paperUrl,
                  //method: 'GET', //默认 'GET'，请求 url 的方式
                }}
                fitWidth={true} //默认 false，若为 true 则不能将 fitWidth = true 与 scale 一起使用
                fitPolicy={0} // 0:宽度对齐，1：高度对齐，2：适合两者（默认）
                page={1}
                //scale={1}
                onLoadComplete={(numberOfPages, filePath, width, height, tableContents) => {
                  console.log(`number of pages: ${numberOfPages}`); //总页数
                  console.log(`number of filePath: ${filePath}`); //本地返回的路径
                  console.log(`number of width: `, JSON.stringify(width));
                  console.log(`number of height: ${JSON.stringify(height)}`);
                  console.log(`number of tableContents: ${tableContents}`);
                }}
                onError={(error) => {
                  console.log(error);
                }}
                minScale={1} //最小模块
                maxScale={3}
                enablePaging={true} //在屏幕上只能显示一页
                style={{
                  flex: 1,
                  width: width
                }}
              />
            </View>

          </Overlay>

          <Overlay
            fullScreen={true}
            animationType='fade'
            isVisible={this.state.isCompModalOpen}
            onRequestClose={() => {
              this.setState({ isCompModalOpen: !this.state.isCompModalOpen });
            }}>
            <Header
              containerStyle={{ height: 45, paddingTop: 0, top: -5 }}
              centerComponent={{ text: '构件详情', style: { color: 'gray', fontSize: 20 } }}
              rightComponent={<BackIcon
                onPress={() => {
                  this.setState({
                    isCompModalOpen: !this.state.isCompModalOpen
                  });
                }} />}
              backgroundColor='white'
            />
            <View style={{ flex: 1 }}>
              <WebView source={{ uri: "http://auth.smart.pkpm.cn/QRPageFrame.aspx?sitecode=" + Url.ID + "&qrbiz=/PCIS/Production/ShowCompInfo.aspx?compid=" + RFID }}></WebView>

            </View>
          </Overlay>

          <BottomSheet
            isVisible={bottomVisible}
            onRequestClose
          >
            {
              bottomData.map((item, index) => {
                return (
                  <TouchableOpacity
                    onPress={() => {
                      if (item.roomId) {
                        this.setState({
                          roomId: item.roomId,
                          roomselect: item.roomName
                        })
                      } else if (item.teamId) {
                        this.setState({
                          teamId: item.teamId,
                          teamselect: item.teamName,
                          labourName: item.labourName
                        })
                      } else {
                        this.setState({
                          keeperId: item.keeperId,
                          keeperselect: item.keeperName
                        })
                      }
                      this.setState({
                        bottomVisible: false
                      })
                    }}
                  >
                    <ListItem
                      title={item.roomName ? item.roomName : (item.teamName ? item.teamName : item.keeperName)}
                      //titleStyle={styles.text}
                      //contentContainerStyle={styles.content}
                      // containerStyle={styles.container}
                      bottomDivider
                    />
                  </TouchableOpacity>
                )

              })
            }
          </BottomSheet>
        </View>
      )
    }
  }

}