
import React from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, ListItem, BottomSheet } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { ToDay, YesterDay, BeforeYesterDay } from '../../CommonComponents/Data';
import { RFT } from '../../Url/Pixal';
import { ScrollView } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker'
import { NavigationEvents } from 'react-navigation';
import ScanButton from '../Componment/ScanButton';
import { styles } from '../Componment/PDAStyles';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import ListItemScan_child from '../Componment/ListItemScan_child';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import CheckBoxScan from '../Componment/CheckBoxScan';
import AvatarAdd from '../Componment/AvatarAdd';
import AvatarImg from '../Componment/AvatarImg';

import { DeviceEventEmitter, NativeModules } from 'react-native';

const { height, width } = Dimensions.get('window') //获取宽高

let compDataArr = [], compIdArr = [], QRid2 = '', tmpstr = '', visibleArr = [];

export default class ChangeLocation extends React.Component {
  constructor() {
    super();
    this.state = {
      monitorSetup: '',
      type: 'Stockid',
      EditName: Url.PDAEmployeeName,
      bottomData: [],
      bottomVisible: false,
      resData: [],
      formCode: '',
      rowguid: '',
      ServerTime: '',
      //工装是否使用
      isUsedPRacking: '',
      handingToolName: '',
      handingUniqueCode: '',
      handlingToolId: '',
      isGettrackingID: false,
      istrackingIDdelete: false,
      QRid: '',
      news: '',
      room: [],
      roomId: '',
      roomselect: '请选择',
      roomName: '',
      library: [],
      libraryId: '',
      libraryselect: '请选择',
      libraryName: '',
      compDataArr: [],
      compData: '',
      compId: '',
      compIdArr: [],
      compCode: '',
      isGetStoreComponentInfo: false,
      isGetStoreroom: false,
      //CardList
      hidden: -1,
      isVisible: false,
      isUpdate: false,
      isCheckPage: false,
      isLoading: true,
      focusIndex: 4,
      visibleArr: [],
    }
    //this.requestCameraPermission = this.requestCameraPermission.bind(this)
  }

  componentDidMount() {
    const { navigation } = this.props;
    let guid = navigation.getParam('guid') || ''
    let pageType = navigation.getParam('pageType') || ''
    if (pageType == 'CheckPage') {
      this.checkResData(guid)
      this.setState({ isCheckPage: true })
    } else {
      this.resData()
    }

    //通过使用DeviceEventEmitter模块来监听事件
    this.iDataScan = DeviceEventEmitter.addListener('iDataScan', (Event) => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 115 ~ SteelCage ~ Event.ScanResult", Event.ScanResult)
      if (typeof Event.ScanResult != undefined) {
        let data = Event.ScanResult
        let arr = data.split("=");
        let id = ''
        id = arr[arr.length - 1]
        console.log("🚀 ~ file: concrete_pouring.js ~ line 118 ~ SteelCage ~ id", id)
        if (this.state.type == 'libraryid') {
          this.GetStoreroom(id)
        } else if (this.state.type == 'Stockid') {
          this.GetStoreComponentInfo(id)
        } else if (this.state.type == 'trackingid') {
          this.GetPalleTracking(id)
        }
      }
    });
  }

  UpdateControl = () => {
    if (typeof this.props.navigation.getParam('QRid') != "undefined") {
      if (this.props.navigation.getParam('QRid') != "") {
        this.toast.show(Url.isLoadingView, 0)
      }
    }
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid')
    const type = navigation.getParam('type') || '';

    if (type == 'libraryid') {
      this.GetStoreroom(QRid)
    } else if (type == 'Stockid') {
      this.GetStoreComponentInfo(QRid)
    } else if (type == 'trackingid') {
      this.GetPalleTracking(QRid)
    }

  }

  componentWillUnmount() {
    compDataArr = [], QRid2 = '', tmpstr = '', compIdArr = [], visibleArr = [];
    this.iDataScan.remove()
  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  checkResData = (guid) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getstoragelocationchangedetail",
      "servicetype": "pdaservice",
      "express": "8AC4C0B0",
      "ciphertext": "7df83f712027c63f147f6c2d4a43f08d",
      "data": {
        "_Rowguid": guid,
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: change_location.js ~ line 335 ~ ChangeLocation ~ resData", resData)
      let FormCode = resData.result.FormCode
      let roomName = resData.result.StorageRoom
      let libraryName = resData.result.StorageLocation
      let ChangeTime = resData.result.ChangeTime
      let EditName = resData.result.EditName
      let ComponentInfo = resData.result.ComponentInfo
      compDataArr = ComponentInfo

      this.setState({
        resData: resData,
        formCode: FormCode,
        roomName: roomName,
        roomselect: roomName,
        libraryName: libraryName,
        libraryselect: libraryName,
        compDataArr: compDataArr,
        ServerTime: ChangeTime,
        EditName: EditName,
        isGetStoreComponentInfo: true,
        isGetStoreroom: true,
      })
      this.setState({
        isLoading: false,
      })
    }).catch((error) => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 215 ~ QualityInspection ~ error", error)
    });
  }

  resData = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "storagelocationinit",
      "servicetype": "pdaservice",
      "express": "B34B2B21",
      "ciphertext": "219527459fa29fa23c7d7ef001165d2b",
      "data": { "_bizid": Url.PDAFid }
    }

    formData.append('jsonParam', JSON.stringify(data))

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      //初始化错误提示
      if (resData.status == 100) {
        let formCode = resData.result.formCode
        let ServerTime = resData.result.ChangeTime
        let rowguid = resData.result.rowguid
        let isUsedPRacking = resData.result.isUsedPRacking
        this.setState({
          resData: resData,
          formCode: formCode,
          ServerTime: ServerTime,
          rowguid: rowguid,
          isUsedPRacking: isUsedPRacking,
          isLoading: false
        })
      }
      else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }
    }).catch((error) => {
    });
  }

  GetStoreroom = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getStoreroom",
      "servicetype": "pda",
      "express": "162C33CC",
      "ciphertext": "3b81e5182d128db9333f0fec4d3703a5",
      "data": {
        "LibraryId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
       this.toast.close()
      if (resData.status == '100') {
        let libraryName = resData.result.libraryName
        let libraryId = resData.result.libraryId
        let roomName = resData.result.roomName
        this.setState({
          libraryName: libraryName,
          libraryId: libraryId,
          roomName: roomName,
          libraryselect: libraryName,
          roomselect: roomName,
          isGetStoreroom: true
        })
      } else {
        Alert.alert('ERROR', resData.message)
      }

    }).catch(err => {
    })
  }

  GetStoreComponentInfo = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getcompinfoforstolocationchange",
      "servicetype": "pdaservice",
      "express": "227EB695",
      "ciphertext": "de110251c4c1faf0038a3416d728a830",
      "data": {
        "compId": id,
        "_bizid": Url.PDAFid
      }
    }
    console.log("🚀 ~ file: change_location.js ~ line 277 ~ ChangeLocation ~ data", data)

    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
       this.toast.close()
      if (resData.status == '100') {
        let compData = resData.result
        let compId = compData.compId
        let compCode = compData.compCode
        if (compDataArr.length == 0) {
          compDataArr.push(compData)
          compIdArr.push(compId)
          this.setState({
            compDataArr: compDataArr,
            compIdArr: compIdArr,
            compData: compData,
            compId: "",
            compCode: compCode,
            isGetStoreComponentInfo: true
          })
          tmpstr = tmpstr + compId + ' '
        } else {
          if (tmpstr.indexOf(compId) == -1) {
            compIdArr.push(compId)
            compId = this.state.compId + ',' + compId
            compDataArr.push(compData)
            this.setState({
              compDataArr: compDataArr,
              compIdArr: compIdArr,
              compData: compData,
              compId: "",
              compCode: compCode,
              isGetStoreComponentInfo: true
            })
            tmpstr = tmpstr + JSON.stringify(compId) + ' '
          } else {
            this.toast.show('已经扫瞄过此构件')
          }
        }
        this.setState({
          compId: ""
        })
      } else {
        Alert.alert('错误', resData.message)
        this.setState({
          compId: ""
        })
        //this.input1.focus()
      }

    }).catch(err => {
    })
  }

  GetPalleTracking = (id) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getpalletracking",
      "servicetype": "pdaservice",
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        "_bizid": Url.PDAFid,
        "handlingToolId": id
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
       this.toast.close()
      if (resData.status == '100') {
        let handlingToolId = resData.result.handlingToolId
        let handingToolName = resData.result.handingToolName
        let handingUniqueCode = resData.result.handingUniqueCode
        this.setState({
          handlingToolId: handlingToolId,
          handingToolName: handingToolName,
          handingUniqueCode: handingUniqueCode,
          isGettrackingID: true
        })
      } else {
        Alert.alert('ERROR', resData.message)
      }

    }).catch(err => {
    })
  }

  comIDItem = () => {
    const { isGetcomID, buttondisable, compCode } = this.state
    return (
      <View>
        {
          !isGetcomID ?
            <View>
              <ScanButton

                onPress={() => {
                  this.setState({
                    iscomIDdelete: false,
                    buttondisable: true,
                    isUpdate: true
                  })
                  setTimeout(() => {
                    this.setState({ GetError: false, buttondisable: false })
                    varGetError = false
                  }, 1500)
                  this.props.navigation.navigate('QRCode', {
                    type: 'compid',
                    page: 'ChangeLocation'
                  })
                }}
              />
            </View>
            :
            <TouchableOpacity
              onPress={() => {
                console.log('onPress')
              }}
              onLongPress={() => {
                console.log('onLongPress')
                Alert.alert(
                  '提示信息',
                  '是否删除构件信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      this.setState({
                        compData: [],
                        compCode: '',
                        compId: '',
                        iscomIDdelete: true,
                        isGetcomID: false,
                      })
                    }
                  }])
              }} >
              <Text>{compCode}</Text>
            </TouchableOpacity>
        }
      </View>

    )
  }

  trackingIDItem = (index) => {
    const { isGettrackingID, buttondisable } = this.state
    return (
      <View>
        {
          !isGettrackingID ?
            <View>
              <ScanButton
                onPress={() => {
                  this.setState({
                    istrackingIDdelete: false,
                    buttondisable: true,
                    isUpdate: true,
                    focusIndex: index
                  })
                  this.props.navigation.navigate('QRCode', {
                    type: 'trackingid',
                    page: 'ChangeLocation'
                  })
                }}
                onLongPress={() => {
                  this.setState({
                    type: 'trackingid',
                    focusIndex: index
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}
              />
            </View>
            :
            <TouchableOpacity
              onPress={() => {
                console.log('onPress')
              }}
              onLongPress={() => {
                console.log('onLongPress')
                Alert.alert(
                  '提示信息',
                  '是否删除工装编码',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      this.setState({
                        handingToolName: '',
                        handingUniqueCode: '',
                        handlingToolId: '',
                        istrackingIDdelete: true,
                        buttondisable: false,
                        isGettrackingID: false,
                      })
                    }
                  }])
              }} >
              <Text numberOfLines={1}>{this.state.handingUniqueCode}</Text>
            </TouchableOpacity>
        }
      </View>

    )
  }

  PostData = () => {
    const { formCode, ServerTime, rowguid, compId, compIdArr, libraryId, handlingToolId, roomName } = this.state
    const { navigation } = this.props;
    const QRid = navigation.getParam('QRid') || '';

    console.log("🚀 ~ file: change_location.js ~ line 491 ~ ChangeLocation ~ compIdArr", compIdArr)
    let compIdArray = compIdArr

    if (libraryId.length == 0) {
      this.toast.show('库位不能为空')
      return
    } else if (compIdArray.length == 0) {
      this.toast.show('入库集合不能为空')
      return
    } else if (roomName.length == 0) {
      this.toast.show('库房不能为空')
      return
    }

    this.toast.show(saveLoading, 0)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "savestoragelocationchange",
      "servicetype": "pdaservice",
      "express": "54FA70C0",
      "ciphertext": "042b311dd63cfff370a4230b6412a775",
      "data": {
        "userName": Url.PDAusername,
        "compIdArray": compIdArray,
        "StorageLactionId": libraryId,
        "ChangeTime": ServerTime,
        "_bizid": Url.PDAFid,
        "rowguid": rowguid,
        "handlingToolId": handlingToolId,
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: change_location.js ~ line 516 ~ ChangeLocation ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('保存成功');
        setTimeout(() => {
          this.props.navigation.replace(this.props.navigation.getParam("page"), {
            title: this.props.navigation.getParam("title"),
            pageType: this.props.navigation.getParam("pageType"),
            page: this.props.navigation.getParam("page"),
            isAppPhotoSetting: this.props.navigation.getParam("isAppPhotoSetting"),
            isAppDateSetting: this.props.navigation.getParam("isAppDateSetting"),
            isAppPhotoAlbumSetting: this.props.navigation.getParam("isAppPhotoAlbumSetting"),
          })
          //this.props.navigation.navigate('PDAMainStack')
        }, 400)
      } else {
        this.toast.show('保存失败')
        Alert.alert('保存失败', resData.message)
      }
    }).catch((error) => {
      this.toast.show('保存失败')
      if (error.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('保存失败', "响应内容不是合法JSON格式")
        return
      }
      if (error.toString().indexOf('Network request faile') != -1) {
        Alert.alert('保存失败', "网络请求错误")
        return
      }
      Alert.alert('保存失败', error.toString())
      console.log("🚀 ~ file: change_location.js ~ line 409 ~ ChangeLocation ~ error", error)
    });
  }

  cardListContont = comp => {
    if (visibleArr.indexOf(comp.compCode) == -1) {
      visibleArr.push(comp.compCode);
      this.setState({ visibleArr: visibleArr });
    } else {
      if (visibleArr.length == 1) {
        visibleArr.splice(0, 1);
        this.setState({ visibleArr: visibleArr }, () => {
          this.forceUpdate;
        });
      } else {
        visibleArr.map((item, index) => {
          if (item == comp.compCode) {
            visibleArr.splice(index, 1);
            this.setState({ visibleArr: visibleArr }, () => {
              this.forceUpdate;
            });
          }
        });
      }
    }
  };

  CardList = (compData) => {
    return (
      compData.map((item, index) => {
        let comp = item
        return (
          <View style={styles.CardList_main}>
            <TouchableCustom
              onPress={() => {
                this.cardListContont(comp);
              }}
              onLongPress={() => {
                Alert.alert(
                  '提示信息',
                  '是否删除构件信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      compDataArr.splice(index, 1)
                      compIdArr.splice(index, 1)
                      tmpstr = compIdArr.toString()
                      this.setState({
                        compDataArr: compDataArr,
                        compIdArr: compIdArr
                      })
                    }
                  }])

              }}>
              <View style={styles.CardList_title_main}>
                <View style={styles.title_num}>
                  <Text >{index + 1}</Text>
                </View>
                <View style={styles.title_title}>
                  <Text >{item.compCode}</Text>
                </View>
                <View style={styles.CardList_at_icon}>
                  <IconDown />
                </View>
              </View>
            </TouchableCustom>
            {
              visibleArr.indexOf(item.compCode) != -1 ?
                <View key={index} style={{ backgroundColor: 'transparent' }} >
                  <ListItemScan_child
                    title='产品编号'
                    rightTitle={comp.compCode}
                  />
                  <ListItemScan_child
                    title='项目名称'
                    rightTitle={comp.projectName}
                  />
                  <ListItemScan_child
                    title='构件类型'
                    rightTitle={comp.compTypeName}
                  />
                  <ListItemScan_child
                    title='设计型号'
                    rightTitle={comp.designType}
                  />
                  <ListItemScan_child
                    title='楼号'
                    rightTitle={comp.floorNoName}
                  />
                  <ListItemScan_child
                    title='层号'
                    rightTitle={comp.floorName}
                  />
                  <ListItemScan_child
                    title='砼方量'
                    rightTitle={comp.volume}
                  />
                  <ListItemScan_child
                    title='重量'
                    rightTitle={comp.weight}
                  />
                  <ListItemScan_child
                    title='生产单位'
                    rightTitle={Url.PDAFname}
                  />
                </View> : <View></View>
            }

          </View>
        )
      })
    )
  }

  _DatePicker = (index) => {
    const { ServerTime } = this.state
    return (
      <DatePicker
        customStyles={{
          dateInput: styles.dateInput,
          dateTouchBody: styles.dateTouchBody,
          dateText: styles.dateText,
        }}
        iconComponent={<Icon name='caretdown' color='#419fff' iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
        }
        showIcon={true}
        mode='datetime'
        date={ServerTime}
        format="YYYY-MM-DD HH:mm"
        onDateChange={(value) => {
          this.setState({
            ServerTime: value,
            focusIndex: index
          })
        }}
      />
    )
  }

  isBottomVisible = (data, focusIndex) => {
    this.setState({ bottomVisible: true, bottomData: data, focusIndex: focusIndex })
  }

  render() {
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || '';
    const type = navigation.getParam('type') || '';

    const { resData, focusIndex, isGettrackingID, isLoading, formCode, ServerTime, teamselect, roomselect, library, isUsedPRacking, libraryselect, libraryName, bottomVisible, bottomData, compData, compId, compCode } = this.state

    if (isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          <NavigationEvents
            onWillFocus={this.UpdateControl} />
          <ScrollView ref={ref => this.scoll = ref} style={styles.mainView} showsVerticalScrollIndicator={false} >
            <View style={styles.listView}>
              <ListItemScan
                title='表单编号'
                rightTitle={formCode}
              />
              <TouchableCustom underlayColor={'lightgray'} onPress={() => { this.isBottomVisible(Url.PDAnews, '0') }} >
                <ListItemScan
                  focusStyle={focusIndex == '0' ? styles.focusColor : {}}
                  title='库房'
                  rightElement={
                    <TouchableOpacity onPress={() => { this.setState({ bottomVisible: true, bottomData: Url.PDAnews }) }} >
                      <IconDown text={roomselect} />
                    </TouchableOpacity>
                  }
                />
              </TouchableCustom>
              <ListItemScan
                isButton={true}
                title='库位'
                onPress={() => {
                  this.setState({
                    type: 'libraryid',
                    focusIndex: 1
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressIn={() => {
                  this.setState({
                    type: 'libraryid',
                    focusIndex: 1
                  })
                  NativeModules.PDAScan.onScan();
                }}
                focusStyle={focusIndex == '1' ? styles.focusColor : {}}
                rightElement={
                  <View style={{ flexDirection: 'row' }}>
                    <TouchableCustom style={{ height: 30 }} onPress={() => {
                      if (library.length != 0) {
                        this.setState({ bottomVisible: true, bottomData: library, focusIndex: 1 })
                      } else {
                        this.toast.show('请先选库房')
                      }
                    }} >
                      <View style={{ marginRight: 16, top: 9 }}>
                        <IconDown text={libraryselect} />
                      </View>
                    </TouchableCustom>
                    <ScanButton
                      onPress={() => {
                        this.setState({ focusIndex: 1, isGetStoreroom: false })
                        this.props.navigation.navigate('QRCode', {
                          type: 'libraryid',
                          page: 'ChangeLocation'
                        })
                      }}
                      onLongPress={() => {
                        this.setState({
                          type: 'libraryid',
                          focusIndex: 1
                        })
                        NativeModules.PDAScan.onScan();
                      }}
                      onPressOut={() => {
                        NativeModules.PDAScan.offScan();
                      }}
                    />
                  </View>
                }
              />
              {
                isUsedPRacking == '使用' ?
                  <View>
                    <ListItemScan
                      focusStyle={focusIndex == '2' ? styles.focusColor : {}}
                      isButton={!isGettrackingID}
                      title='工装唯一码'
                      onPressIn={() => {
                        this.setState({
                          type: 'trackingid',
                          focusIndex: 2
                        })
                        NativeModules.PDAScan.onScan();
                      }}
                      onPress={() => {
                        this.setState({
                          type: 'trackingid',
                          focusIndex: 2
                        })
                        NativeModules.PDAScan.onScan();
                      }}
                      onPressOut={() => {
                        NativeModules.PDAScan.offScan();
                      }}
                      rightElement={this.trackingIDItem(2)}
                    />
                    <ListItemScan
                      title='托盘/货架'
                      rightTitle={this.state.handingToolName}
                    />
                  </View> : <View></View>
              }

              <ListItemScan
                focusStyle={focusIndex == '3' ? styles.focusColor : {}}
                title='变更日期'
                rightTitle={this._DatePicker(3)}
              />
              <ListItemScan
                title='登陆人'
                rightTitle={this.state.EditName}
              />
              <ListItemScan
                isButton={true}
                focusStyle={focusIndex == '4' ? styles.focusColor : {}}
                title='变更明细'
                onPress={() => {
                  this.setState({
                    type: 'Stockid',
                    focusIndex: 4
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressIn={() => {
                  this.setState({
                    type: 'Stockid',
                    focusIndex: 4
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}
                rightElement={
                  <View style={{ flexDirection: 'row' }}>
                    <View>
                      <Input
                        ref={ref => { this.input1 = ref }}
                        containerStyle={styles.scan_input_container}
                        inputContainerStyle={styles.scan_inputContainerStyle}
                        inputStyle={[styles.scan_input]}
                        placeholder='请输入'
                        keyboardType='numeric'
                        value={compId}
                        onChangeText={(value) => {
                          value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
                          value = value.replace(/^\./g, ""); //验证第一个字符是数字
                          value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
                          //value = value.replace(/\b(0+)/gi, ""); //清楚开头的0
                          this.setState({
                            compId: value
                          })
                        }}
                        onFocus={() => {
                          this.setState({
                            focusIndex: 3
                          })
                        }}
                        onSubmitEditing={() => {
                          let inputComId = this.state.compId
                          console.log("🚀 ~ file: qualityinspection.js ~ line 468 ~ QualityInspection ~ inputComId", inputComId)
                          inputComId = inputComId.replace(/\b(0+)/gi, "")
                          this.GetStoreComponentInfo(inputComId)
                        }}
                      />
                    </View>
                    <ScanButton
                      onPress={() => {
                        this.setState({
                          isGetStoreComponentInfo: false,
                          isUpdate: true,
                          focusIndex: 4
                        }, () => {
                          this.props.navigation.navigate('QRCode', {
                            type: 'Stockid',
                            page: 'ChangeLocation',
                            isUpdateFun: (isUpdate, type, page, QRid) => {
                              this.setState({
                                isUpdate: isUpdate,
                                type: type,
                                page: page,
                                QRid: QRid,
                              })
                            }
                          })
                        })
                      }}
                      onLongPress={() => {
                        this.setState({
                          type: 'Stockid',
                          focusIndex: 4
                        })
                        NativeModules.PDAScan.onScan();
                      }}
                      onPressOut={() => {
                        NativeModules.PDAScan.offScan();
                      }}
                    />
                  </View>
                }
              />

              {
                compDataArr.length > 0 ? this.CardList(compDataArr) : <View></View>
              }

            </View>

            {this.state.isCheckPage ? <View></View> :
              <FormButton
                backgroundColor='#17BC29'
                title='保存'
                onPress={() => {
                  this.PostData()
                }}
              />

            }


            <BottomSheet
              isVisible={bottomVisible}
              onRequestClose={() => {
                this.setState({
                  bottomVisible: false
                })
              }}
              onBackdropPress={() => {
                this.setState({
                  bottomVisible: false
                })
              }}
            >
              <ScrollView style={styles.bottomsheetScroll}>
                {
                  bottomData.map((item, index) => {
                    let title = ''
                    if (item.roomName) {
                      title = item.roomName
                    } else if (item.libraryName) {
                      title = item.libraryName
                    }
                    return (
                      <TouchableCustom
                        onPress={() => {
                          if (item.roomId) {
                            this.setState({
                              roomId: item.roomId,
                              roomselect: item.roomName,
                              roomName: item.roomName,
                              library: item.library
                            })
                          } else if (item.libraryId) {
                            this.setState({
                              libraryId: item.libraryId,
                              libraryName: item.libraryName,
                              libraryselect: item.libraryName
                            })
                          }
                          this.setState({
                            bottomVisible: false
                          })
                        }}
                      >
                        <BottomItem backgroundColor='white' color="#333" title={title} />

                      </TouchableCustom>
                    )

                  })
                }
              </ScrollView>
              <TouchableCustom
                onPress={() => {
                  this.setState({ bottomVisible: false })
                }}>
                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
              </TouchableCustom>

            </BottomSheet>

          </ScrollView>

        </View>
      )
    }
  }

}

