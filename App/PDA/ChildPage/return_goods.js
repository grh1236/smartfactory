import React from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, ListItem, BottomSheet, Overlay, Header, CheckBox } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { ToDay, YesterDay, BeforeYesterDay } from '../../CommonComponents/Data';
import { RFT } from '../../Url/Pixal';
import { ScrollView } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker'
import { NavigationEvents } from 'react-navigation';
import { styles } from '../Componment/PDAStyles';
import ScanButton from '../Componment/ScanButton';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import ListItemScan_child from '../Componment/ListItemScan_child';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import TouchableCustom from '../Componment/TouchableCustom';
import { saveLoading } from '../../Url/Pixal';
import CheckBoxScan from '../Componment/CheckBoxScan';
import AvatarAdd from '../Componment/AvatarAdd';
import AvatarImg from '../Componment/AvatarImg';
import { DeviceEventEmitter, NativeModules } from 'react-native';
import overlayImg from '../Componment/OverlayImg';
import OverlayImgZoomPicker from '../Componment/OverlayImgZoomPicker';
class BackIcon extends React.PureComponent {
  render() {
    return (
      <TouchableOpacity
        onPress={this.props.onPress} >
        <Icon
          name='arrow-back'
          color='#419FFF' />
      </TouchableOpacity>
    )
  }
}

const { height, width } = Dimensions.get('window') //获取宽高

let fileflag = "12";

let imageArr = [], imageFileArr = [];

let compDataArr = [], compIdArr = [], QRid2 = '', tmpstr = '',
  questionChickIdArr = [], questionChickArr = [], //子表问题数组
  isCheckSecondList = [], //是否展开二级
  isCheckedAllList = [], //二级菜单是否被全选
  visibleArr = []

export default class ReturnGoods extends React.Component {
  constructor() {
    super();
    this.state = {
      monitorSetup: '',
      type: 'Stockid',
      isroom: false,
      isteam: false,
      iskeeper: false,
      bottomData: [],
      bottomVisible: false,
      resData: [],
      formCode: '',
      rowguid: '',
      steelCageIDs: '',
      ServerTime: '',
      QRid: '',
      news: '',
      room: [],
      roomId: '',
      roomselect: '请选择',
      roomName: '',
      library: [],
      libraryId: '',
      libraryselect: '请选择',
      libraryName: '',
      compDataArr: [],
      compData: '',
      compId: '',
      compIdArr: [],
      compCode: '',
      isGetRefundComponentInfo: false,
      isGetStoreroom: false,
      //CardList
      hidden: -1,
      isVisible: false,
      //问题缺陷
      dictionary: [],
      problemDefect: '',
      measure: '',
      problemId: '',
      questionChick: -1,
      questionChickIdArr: [],
      questionChickArr: [],
      problemDefects: [],
      isCheckSecondList: [],
      isCheckedAllList: [],
      isQuestionOverlay: false,
      reasons: [
        { "reason": "损坏/质量问题" },
        { "reason": "损坏" },
      ],
      reasonselect: '请选择',
      reason: '',
      projectId: '',
      projectName: '',
      //照片
      imageArr: [],
      imageFileArr: [],
      pictureSelect: false,
      imageOverSize: false,
      pictureUri: '',
      fileid: "",
      recid: "",
      isPhotoUpdate: false,
      //承运单位
      SupplierInfo: [],
      supId: '',
      supName: '',
      supSelect: '请选择',
      remark: '',
      carnum: '',
      focusIndex: 5,
      isCheckPage: false,
      isLoading: true,
      //控制app拍照，日期，相册选择功能
      isAppPhotoSetting: false,
      currShowImgIndex: 0,
      isAppDateSetting: false,
      isAppPhotoAlbumSetting: false
    }
    //this.requestCameraPermission = this.requestCameraPermission.bind(this)
  }

  componentDidMount() {
    const { navigation } = this.props;
    let guid = navigation.getParam('guid') || ''
    let pageType = navigation.getParam('pageType') || ''
    let isAppPhotoSetting = navigation.getParam('isAppPhotoSetting')
    let isAppDateSetting = navigation.getParam('isAppDateSetting')
    let isAppPhotoAlbumSetting = navigation.getParam('isAppPhotoAlbumSetting')
    this.setState({
      isAppPhotoSetting: isAppPhotoSetting,
      isAppDateSetting: isAppDateSetting,
      isAppPhotoAlbumSetting: isAppPhotoAlbumSetting
    })
    if (pageType == 'CheckPage') {
      this.checkResData(guid)
      this.setState({ isCheckPage: true })
    } else {
      this.resData()
    }

    //通过使用DeviceEventEmitter模块来监听事件
    this.iDataScan = DeviceEventEmitter.addListener('iDataScan', (Event) => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 115 ~ SteelCage ~ Event.ScanResult", Event.ScanResult)
      if (typeof Event.ScanResult != undefined) {
        let data = Event.ScanResult
        let arr = data.split("=");
        let id = ''
        id = arr[arr.length - 1]
        console.log("🚀 ~ file: concrete_pouring.js ~ line 118 ~ SteelCage ~ id", id)
        if (this.state.type == 'libraryid') {
          this.GetStoreroom(id)
        } else if (this.state.type == 'Stockid') {
          this.GetRefundComponentInfo(id)
        }

      }
    });
  }

  UpdateControl = () => {
    if (typeof this.props.navigation.getParam('QRid') != "undefined") {
      if (this.props.navigation.getParam('QRid') != "") {
        this.toast.show(Url.isLoadingView, 0)
      }
    }
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || ''
    const type = navigation.getParam('type') || '';

    if (type == 'libraryid') {
      this.GetStoreroom(QRid)

    } else if (type == 'Stockid') {
      this.GetRefundComponentInfo(QRid)
    }


  }

  componentWillUnmount() {
    compDataArr = [], QRid2 = '', tmpstr = '', compIdArr = [],
      questionChickIdArr = [], questionChickArr = [], //子表问题数组
      isCheckSecondList = [], //是否展开二级
      isCheckedAllList = [] //二级菜单是否被全选
      , visibleArr = []
    imageArr = [], imageFileArr = [];
    this.iDataScan.remove()
  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  checkResData = (guid) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "ObtainRefund",
      "servicetype": "pda",
      "express": "8AC4C0B0",
      "ciphertext": "7df83f712027c63f147f6c2d4a43f08d",
      "data": {
        "guid": guid,
        "factoryId": Url.PDAFid,
      }
    }
    formData.append('jsonParam', JSON.stringify(data))

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: return_goods.js ~ line 209 ~ ReturnGoods ~ resData", resData)
      let rowguid = resData.result.guid
      let formCode = resData.result.formCode
      let libraryName = resData.result.libraryName
      let roomName = resData.result.roomName
      let projectId = resData.result.projectId
      let projectName = resData.result.projectName
      let supId = resData.result.supId
      let supName = resData.result.supName
      let carNumber = resData.result.carNumber
      let remark = resData.result.remark
      let ImageUrls = resData.result.ImageUrls

      ImageUrls.map((item, index) => {
        let tmp = {}
        tmp.fileid = ""
        tmp.uri = item.componentImageUrl
        tmp.url = item.componentImageUrl
        imageFileArr.push(tmp)
        imageArr.push(item.componentImageUrl)
      })
      this.setState({
        imageFileArr: imageFileArr
      })

      let dictionary = []
      if (typeof (resData.result.dictionary) != "undefined") {
        dictionary = resData.result.dictionary
        dictionary.map((item, index) => {
          item.problemDefects.map((defectitem, index) => {
            if (defectitem.ischecked == true) {
              questionChickArr.push(defectitem)
              questionChickIdArr.push(defectitem.rowguid)

            }
          })
        })
      }
      let reason = resData.result.reason
      let ServerTime = resData.result.time
      let component = resData.result.component
      let compCode = component[0].compCode
      let compDataArr = []
      component.map((item, index) => {
        compDataArr.push(item.comp[0])
      })
      this.setState({
        resData: resData,
        rowguid: rowguid,
        ServerTime: ServerTime,
        formCode: formCode,
        libraryName: libraryName,
        libraryselect: libraryName,
        projectId: projectId,
        projectName: projectName,
        supId: supId,
        supName: supName,
        supSelect: supName,
        carnum: carNumber,
        remark: remark,
        roomName: roomName,
        roomselect: roomName,
        dictionary: dictionary,
        questionChickArr: questionChickArr,
        questionChickIdArr: questionChickIdArr,
        reason: reason,
        reasonselect: reason,
        compCode: compCode,
        compDataArr: compDataArr,
        imageArr: imageArr,
        pictureSelect: true
      })
      this.setState({
        isLoading: false,
      })
    }).catch((error) => {
    });

  }

  resData = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "GetRefundFormCodeAndServerTime",
      "servicetype": "pda",
      "express": "B34B2B21",
      "ciphertext": "219527459fa29fa23c7d7ef001165d2b",
      "data": { "factoryId": Url.PDAFid }
    }

    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      //初始化错误提示
      if (resData.status == 100) {
        let formCode = resData.result.formCode
        let ServerTime = resData.result.ServerTime
        let rowguid = resData.result.rowguid
        let dictionary = resData.result.dictionary
        let SupplierInfo = resData.result.SupplierInfo
        this.setState({
          resData: resData,
          formCode: formCode,
          ServerTime: ServerTime,
          rowguid: rowguid,
          dictionary: dictionary,
          SupplierInfo: SupplierInfo,
          isLoading: false
        })
      }
      else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }

    }).catch((error) => {
    });
  }

  GetStoreroom = (id) => {
    let compCode = '111'
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getStoreroom",
      "servicetype": "pda",
      "express": "162C33CC",
      "ciphertext": "3b81e5182d128db9333f0fec4d3703a5",
      "data": {
        "LibraryId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
       this.toast.close()
      if (resData.status == '100') {
        let libraryName = resData.result.libraryName
        let libraryId = resData.result.libraryId
        let roomName = resData.result.roomName
        QRid2 = id
        this.setState({
          libraryName: libraryName,
          libraryId: libraryId,
          roomName: roomName,
          libraryselect: libraryName,
          roomselect: roomName,
          isGetStoreroom: true
        })
      } else {
        Alert.alert('ERROR', resData.message)
      }

    }).catch(err => {
    })
  }

  GetRefundComponentInfo = (id) => {
    console.log("🚀 ~ file: return_goods.js ~ line 318 ~ ReturnGoods ~ id", id)
    let formData = new FormData();
    let data = {};
    data = {
      "action": "GetRefundComponentInfo",
      "servicetype": "pda",
      "express": "227EB695",
      "ciphertext": "de110251c4c1faf0038a3416d728a830",
      "data": {
        "compId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: return_goods.js:436 ~ ReturnGoods ~ formData:", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
       this.toast.close()
      if (resData.status == '100') {
        let compData = resData.result
        let compId = compData.compId
        let compCode = compData.compCode
        let projectId = compData.projectId
        let projectName = compData.projectName
        if (compDataArr.length == 0) {
          compDataArr.push(compData)
          compIdArr.push(compId)
          this.setState({
            compDataArr: compDataArr,
            compIdArr: compIdArr,
            compData: compData,
            compId: "",
            compCode: compCode,
            projectName: projectName,
            projectId: projectId,
            isGetRefundComponentInfo: true
          })
          tmpstr = tmpstr + compId + ','
          console.log("🚀 ~ file: return_goods.js ~ line 357 ~ ReturnGoods ~ tmpstr", tmpstr)
        } else {
          if (tmpstr.indexOf(compId) == -1) {
            if (this.state.projectId != projectId) {
              this.toast.show("项目名称与退货项目不符合")
            } else {
              compIdArr.push(compId)
              console.log("🚀 ~ file: return_goods.js ~ line 363 ~ ReturnGoods ~ compIdArr", compIdArr)
              compDataArr.push(compData)
              console.log("🚀 ~ file: return_goods.js ~ line 364 ~ ReturnGoods ~ compDataArr", compDataArr)
              this.setState({
                compDataArr: compDataArr,
                compIdArr: compIdArr,
                compData: compData,
                compId: "",
                compCode: compCode,
                isGetRefundComponentInfo: true
              })
              tmpstr = tmpstr + compId + ','
              console.log("🚀 ~ file: return_goods.js ~ line 374 ~ ReturnGoods ~ tmpstr", tmpstr)
            }
          } else {
            this.toast.show('已经扫瞄过此构件')
          }

        }

      } else {
        Alert.alert('错误', resData.message)
        this.setState({
          compId: ""
        })
        //this.input1.focus()
      }

    }).catch(err => {
    })
  }

  comIDItem = () => {
    const { isGetcomID, buttondisable, compCode, compId } = this.state
    return (
      <View>
        {
          !isGetcomID ?
            <View>
              <View>
                <Input
                  ref={ref => { this.input1 = ref }}
                  containerStyle={styles.scan_input_container}
                  inputContainerStyle={styles.scan_inputContainerStyle}
                  inputStyle={[styles.scan_input]}
                  placeholder='请输入'
                  keyboardType='numeric'
                  value={compId}
                  onChangeText={(value) => {
                    value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
                    value = value.replace(/^\./g, ""); //验证第一个字符是数字
                    value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
                    //value = value.replace(/\b(0+)/gi, ""); //清楚开头的0
                    this.setState({
                      compId: value
                    })
                  }}
                  //onSubmitEditing={() => { this.input1.focus() }}
                  //onFocus={() => { this.setState({ focusIndex: '5' }) }}
                  /*  */
                  //returnKeyType = 'previous'
                  onSubmitEditing={() => {
                    let inputComId = this.state.compId
                    console.log("🚀 ~ file: qualityinspection.js ~ line 468 ~ QualityInspection ~ inputComId", inputComId)
                    inputComId = inputComId.replace(/\b(0+)/gi, "")
                    this.GetcomID(inputComId)
                  }}
                />
              </View>
              <ScanButton

                onPress={() => {
                  this.setState({
                    iscomIDdelete: false,
                    buttondisable: true
                  })
                  setTimeout(() => {
                    this.setState({ GetError: false, buttondisable: false })
                    varGetError = false
                  }, 1500)
                  this.props.navigation.navigate('QRCode', {
                    type: 'compid',
                    page: 'ReturnGoods'
                  })
                }}
              />
            </View>
            :
            <TouchableOpacity
              onPress={() => {
                console.log('onPress')
              }}
              onLongPress={() => {
                console.log('onLongPress')
                Alert.alert(
                  '提示信息',
                  '是否删除构件信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      this.setState({
                        compData: [],
                        compCode: '',
                        compId: '',
                        iscomIDdelete: true,
                        isGetcomID: false,
                      })
                    }
                  }])
              }} >
              <Text>{compCode}</Text>
            </TouchableOpacity>
        }
      </View>

    )
  }

  PostData = () => {
    const { formCode, projectId, projectName, ServerTime, reason, remark, carnum, supId, supName, rowguid, compId, compIdArr, libraryId, libraryName, roomId, roomName, questionChickArr, recid } = this.state
    const { navigation } = this.props;
    const QRid = navigation.getParam('QRid') || '';

    let compIdstr = compIdArr.toString()

    if (compIdstr == '') {
      this.toast.show('退库明细不能为空')
      return
    }
    if (questionChickArr.length == 0) {
      this.toast.show('退库原因不能为空')
      return
    }
    if (supId.length == 0) {
      this.toast.show('承运单位不能为空')
      return
    }
    if (roomName == '') {
      this.toast.show('库房不能为空')
      return
    }
    if (libraryName == '') {
      this.toast.show('库位不能为空')
      return
    }
    if (this.state.isAppPhotoSetting) {
      if (imageArr.length == 0) {
        this.toast.show('请先拍摄构件照片');
        return
      }
    }

    this.toast.show(saveLoading, 0)

    let formData = new FormData();
    let data = {};
    data = {

      "action": "SaveBatchRefundComponentInfo",
      "servicetype": "pda",
      "express": "54FA70C0",
      "ciphertext": "042b311dd63cfff370a4230b6412a775",
      "data": {
        "libraryName": libraryName,
        "Recipient": Url.PDAEmployeeName,
        "supId": supId,
        "supName": supName,
        "remark": remark,
        "carNumber": carnum,
        "strcompId": compIdstr,
        "problemDefects": questionChickArr,
        "editEmployeeId": Url.PDAEmployeeId,
        "editEmployeeName": Url.PDAusername,
        "libraryId": libraryId,
        "RefundTime": ServerTime,
        "factoryId": Url.PDAFid,
        "roomId": roomId,
        "roomName": roomName,
        "rowguid": rowguid,
        "formCode": formCode,
        "projectId": projectId,
        "projectName": projectName,
        "recid": recid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    if (!Url.isAppNewUpload) {
      imageArr.map((item, index) => {
        formData.append('img_' + index, {
          uri: item,
          type: 'image/jpeg',
          name: 'img_' + index
        })
      })
    }
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('保存成功');
        setTimeout(() => {
          this.props.navigation.replace(this.props.navigation.getParam("page"), {
            title: this.props.navigation.getParam("title"),
            pageType: this.props.navigation.getParam("pageType"),
            page: this.props.navigation.getParam("page"),
            isAppPhotoSetting: this.props.navigation.getParam("isAppPhotoSetting"),
            isAppDateSetting: this.props.navigation.getParam("isAppDateSetting"),
            isAppPhotoAlbumSetting: this.props.navigation.getParam("isAppPhotoAlbumSetting"),
          })
          //this.props.navigation.navigate('PDAMainStack')
        }, 400)
      } else {
        this.toast.show('保存失败')
        Alert.alert('保存失败', resData.message)
      }
    }).catch((error) => {
      this.toast.show('保存失败')
      if (error.toString().indexOf('not valid JSON') != -1) {
        Alert.alert('保存失败', "响应内容不是合法JSON格式")
        return
      }
      if (error.toString().indexOf('Network request faile') != -1) {
        Alert.alert('保存失败', "网络请求错误")
        return
      }
      Alert.alert('保存失败', error.toString())
    });
  }

  ReviseData = () => {
    const { formCode, projectId, projectName, ServerTime, reason, remark, carnum, supId, supName, rowguid, compId, compIdArr, libraryId, libraryName, roomName, questionChickArr } = this.state
    let formData = new FormData();
    let data = {};
    data = {
      "action": "ReviseRefund",
      "servicetype": "pda",
      "express": "3604CA4D",
      "ciphertext": "5132f139763f2356d5e63e28d3b5a76b",
      "data": {
        "libraryName": libraryName,
        "factoryId": Url.PDAFid,
        "libraryId": libraryId,
        "guid": rowguid,
        "Time": ServerTime,
        "roomName": roomName,
        "problemDefects": questionChickArr
      }
    }

    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('修改成功');
        this.props.navigation.navigate('MainCheckPage')
      } else {
        Alert.alert('修改失败', resData.message)
      }
    }).catch((error) => {
      console.log("🚀 ~ file: steel_cage_storage.js ~ line 75 ~ SteelCage ~ error", error)
    });
  }

  cardListContont = comp => {
    if (visibleArr.indexOf(comp.compCode) == -1) {
      visibleArr.push(comp.compCode);
      this.setState({ visibleArr: visibleArr });
    } else {
      if (visibleArr.length == 1) {
        visibleArr.splice(0, 1);
        this.setState({ visibleArr: visibleArr }, () => {
          this.forceUpdate;
        });
      } else {
        visibleArr.map((item, index) => {
          if (item == comp.compCode) {
            visibleArr.splice(index, 1);
            this.setState({ visibleArr: visibleArr }, () => {
              this.forceUpdate;
            });
          }
        });
      }
    }
  };

  CardList = (compData) => {
    return (
      compData.map((item, index) => {
        let comp = item
        return (
          <View style={styles.CardList_main}>
            <TouchableCustom
              onPress={() => {
                this.cardListContont(comp);
              }}
              onLongPress={() => {
                Alert.alert(
                  '提示信息',
                  '是否删除构件信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      compDataArr.splice(index, 1)
                      compIdArr.splice(index, 1)
                      tmpstr = compIdArr.toString()
                      console.log("🚀 ~ file: return_goods.js ~ line 559 ~ ReturnGoods ~ compData.map ~ tmpstr", tmpstr)
                      this.setState({
                        compDataArr: compDataArr,
                        compIdArr: compIdArr
                      })
                    }
                  }])
              }}>
              <View style={styles.CardList_title_main}>
                <View style={styles.title_num}>
                  <Text >{index + 1}</Text>
                </View>
                <View style={styles.title_title}>
                  <Text numberOfLines={1} >{item.compCode}</Text>
                </View>
                <View style={styles.CardList_at_icon}>
                  <IconDown />
                </View>
              </View>
            </TouchableCustom>

            {
              visibleArr.indexOf(item.compCode) != -1 ?
                <View key={index} style={{ backgroundColor: '#F9F9F9' }} >
                  <ListItemScan_child
                    title='产品编号'
                    rightTitle={comp.compCode}
                  />
                  <ListItemScan_child
                    title='项目名称'
                    rightTitle={comp.projectName}
                  />
                  <ListItemScan_child
                    title='构件类型'
                    rightTitle={comp.compTypeName}
                  />
                  <ListItemScan_child
                    title='设计型号'
                    rightTitle={comp.designType}
                  />
                  <ListItemScan_child
                    title='楼号'
                    rightTitle={comp.floorNoName}
                  />
                  <ListItemScan_child
                    title='层号'
                    rightTitle={comp.floorName}
                  />
                  <ListItemScan_child
                    title='砼方量'
                    rightTitle={comp.volume}
                  />
                  <ListItemScan_child
                    title='重量'
                    rightTitle={comp.weight}
                  />
                  <ListItemScan_child
                    title='生产单位'
                    rightTitle={Url.PDAFname}
                  />
                </View> : <View></View>
            }

          </View>
        )
      })
    )
  }

  isBottomVisible = (data, focusIndex) => {
    if (typeof (data) != "undefined") {
      if (data.length > 0) {
        this.setState({ bottomVisible: true, bottomData: data, focusIndex: focusIndex })
      }
    } else {
      this.toast.show("无数据")
    }
  }

  cameraFunc = () => {
    launchCamera(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: parseInt(Url.isResizePhoto),
        maxWidth: parseInt(Url.isResizePhoto),
        saveToPhotos: true,

      },
      (response) => {
        if (response.uri == undefined) {
          return
        }
        imageArr.push(response.uri)
        this.setState({
          //pictureSelect: true,
          pictureUri: response.uri,
          imageArr: imageArr
        })
        if (Url.isAppNewUpload) {
          this.cameraPost(response.uri)
        } else {
          this.setState({
            pictureSelect: true,
          })
        }
        console.log(response)
      },
    )
  }

  camLibraryFunc = () => {
    launchImageLibrary(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: parseInt(Url.isResizePhoto),
        maxWidth: parseInt(Url.isResizePhoto),
      },
      (response) => {
        if (response.uri == undefined) {
          return
        }
        imageArr.push(response.uri)
        this.setState({
          //pictureSelect: true,
          pictureUri: response.uri,
          imageArr: imageArr
        })
        if (Url.isAppNewUpload) {
          this.cameraPost(response.uri)
        } else {
          this.setState({
            pictureSelect: true,
          })
        }
        console.log(response)
      },
    )
  }

  camandlibFunc = () => {
    Alert.alert(
      '提示信息',
      '选择拍照方式',
      [{
        text: '取消',
        style: 'cancel'
      }, {
        text: '拍照',
        onPress: () => {
          this.cameraFunc()
        }
      }, {
        text: '相册',
        onPress: () => {
          this.camLibraryFunc()
        }
      }])
  }

  CameraView = () => {
    return (
      <ListItem
        containerStyle={{ paddingBottom: 6 }}
        leftAvatar={
          <View >
            <View style={{ flexDirection: 'column' }} >
              {
                this.state.isCheckPage ? <View></View> :
                  <AvatarAdd
                    pictureSelect={this.state.pictureSelect}
                    backgroundColor='rgba(77,142,245,0.20)'
                    color='#4D8EF5'
                    title="构"
                    onPress={() => {
                      if (this.state.isAppPhotoAlbumSetting) {
                        this.camandlibFunc()
                      } else {
                        this.cameraFunc()
                      }
                    }} />
              }
              {this.imageView()}
            </View>
          </View>
        }
      />
    )
  }

   // 展示/隐藏 放大图片
  handleZoomPicture = (flag, index) => {
    this.setState({
      imageOverSize: false,
      currShowImgIndex: index || 0
    })
  }

  // 加载放大图片弹窗
  renderZoomPicture = () => {
    const { imageOverSize, currShowImgIndex, imageFileArr } = this.state
    return (
      <OverlayImgZoomPicker
        isShowImage={imageOverSize}
        currShowImgIndex={currShowImgIndex}
        zoomImages={imageFileArr}
        callBack={(flag) => this.handleZoomPicture(flag)}
      ></OverlayImgZoomPicker>
    )
  }

  cameraPost = (uri) => {
    const { recid } = this.state
    this.setState({
      isPhotoUpdate: true
    })
    this.toast.show('图片上传中', 2000)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "savephote",
      "servicetype": "photo_file",
      "express": "D2979AB2",
      "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
      "data": {
        "fileflag": fileflag,
        "username": Url.PDAusername,
        "recid": recid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    formData.append('img', {
      uri: uri,
      type: 'image/jpeg',
      name: 'img'
    })
    console.log("🚀 ~ file: concrete_pouring.js ~ line 672 ~ SteelCage ~ formData", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 690 ~ SteelCage ~ resData", resData)
       this.toast.close()
      if (resData.status == '100') {
        let fileid = resData.result.fileid;
        let recid = resData.result.recid;

        let tmp = {}
        tmp.fileid = fileid
        tmp.uri = uri
        tmp.url = uri
        imageFileArr.push(tmp)
        this.setState({
          fileid: fileid,
          recid: recid,
          pictureSelect: true,
          imageFileArr: imageFileArr
        })
        console.log("🚀 ~ file: concrete_pouring.js ~ line 704 ~ SteelCage ~ imageFileArr", imageFileArr)
        this.toast.show('图片上传成功');
        this.setState({
          isPhotoUpdate: false
        })
      } else {
        Alert.alert('图片上传失败', resData.message)
        this.toast.close(1)
      }
    }).catch((error) => {
      console.log("🚀 ~ file: concrete_pouring.js ~ line 692 ~ SteelCage ~ error", error)
    });
  }

  cameraDelete = (item) => {
    let i = imageArr.indexOf(item)

    if (Url.isAppNewUpload) {
      let formData = new FormData();
      let data = {};
      data = {
        "action": "deletephote",
        "servicetype": "photo_file",
        "express": "D2979AB2",
        "ciphertext": "36ed74b262293b3ebb9c29d16486f7ea",
        "data": {
          "fileid": imageFileArr[i].fileid,
        }
      }
      formData.append('jsonParam', JSON.stringify(data))
      fetch(Url.PDAurl, {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        body: formData
      }).then(res => {
        console.log(res.statusText);
        console.log(res);
        return res.json();
      }).then(resData => {
        if (resData.status == '100') {
          if (i > -1) {
            imageArr.splice(i, 1)
            imageFileArr.splice(i, 1)
            this.setState({
              imageArr: imageArr,
              imageFileArr: imageFileArr
            }, () => {
              this.forceUpdate()
            })
          }
          if (imageArr.length == 0) {
            this.setState({
              pictureSelect: false
            })
          }
          this.toast.show('图片删除成功');
        } else {
          Alert.alert('图片删除失败', resData.message)
          this.toast.close(1)
        }
      }).catch((error) => {
        console.log("🚀 ~ file: concrete_pouring.js ~ line 761 ~ SteelCage ~ cameraDelete ~ error", error)
      });
    } else {
      if (i > -1) {
        imageArr.splice(i, 1)
        this.setState({
          imageArr: imageArr,
        }, () => {
          this.forceUpdate()
        })
      }
      if (imageArr.length == 0) {
        this.setState({
          pictureSelect: false
        })
      }
    }
  }

  imageView = () => {
    return (
      <View style={{ flexDirection: 'row' }}>
        {
          this.state.pictureSelect ?
            this.state.imageArr.map((item, index) => {
              return (
                <AvatarImg
                  item={item}
                  index={index}
                  onPress={() => {
                    this.setState({
                      imageOverSize: true,
                      pictureUri: item
                    })
                  }}
                  onLongPress={() => {
                    Alert.alert(
                      '提示信息',
                      '是否删除构件照片',
                      [{
                        text: '取消',
                        style: 'cancel'
                      }, {
                        text: '删除',
                        onPress: () => {
                          this.cameraDelete(item)
                        }
                      }])
                  }} />
              )
            }) : <View></View>
        }
      </View>
    )
  }

  _DatePicker = () => {
    const { ServerTime } = this.state
    return (
      <DatePicker
        customStyles={{
          dateInput: styles.dateInput,
          dateTouchBody: styles.dateTouchBody,
          dateText: this.state.isAppDateSetting ? styles.dateText : styles.dateDisabled,
        }}
        iconComponent={<Icon name='caretdown' color={this.state.isAppDateSetting ? '#419fff' : "#666"} iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
        }
        showIcon={true}
        mode='datetime'
        date={ServerTime}
        format="YYYY-MM-DD HH:mm"
        onOpenModal={() => {
          this.setState({
            focusIndex: '4'
          })
        }}
        disabled={!this.state.isAppDateSetting}
        onDateChange={(value) => {
          this.setState({
            ServerTime: value
          })
        }}
      />
    )
  }

  render() {
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const QRurl = navigation.getParam('QRurl') || '';
    const QRid = navigation.getParam('QRid') || '';
    const type = navigation.getParam('type') || '';

    const { resData, formCode, carnum, projectName, reasons, focusIndex, reasonselect, ServerTime, teamselect, roomselect, library, libraryId, libraryselect, libraryName, bottomVisible, bottomData, compData, compId, compCode, compDataArr, SupplierInfo, supSelect, remark } = this.state

    if (type == 'keeper') {
      keeper.map((item, index) => {
        if (QRid == item.keeperId) {
          QRkeeperID = item.keeperId
          QRkeeperName = item.keeperName
        }
      })
    } else {
      QRStock = QRid
    }

    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          <NavigationEvents
            onWillFocus={this.UpdateControl} />
          <ScrollView ref={ref => this.scoll = ref} style={styles.mainView} showsVerticalScrollIndicator={false} >
            <View style={styles.listView}>

              {this.CameraView()}

              <ListItemScan
                title='表单编号'

                rightTitle={formCode}
              />

              <ListItemScan
                title='项目名称'
                rightTitle={projectName}
              />

              <ListItemScan
                focusStyle={focusIndex == '7' ? styles.focusColor : {}}
                title='车牌号'
                rightElement={
                  <Input
                    containerStyle={[styles.quality_input_container, { top: 2 }]}
                    inputContainerStyle={styles.inputContainerStyle}
                    inputStyle={[styles.quality_input_]}
                    placeholder='请输入'
                    value={carnum}
                    onFocus={() => { this.setState({ focusIndex: 7 }) }}
                    onChangeText={(value) => {
                      this.setState({
                        carnum: value
                      })
                    }} />
                }
              />

              <TouchableCustom underlayColor={'lightgray'} onPress={() => { this.isBottomVisible(SupplierInfo, '8') }} >
                <ListItemScan
                  title='承运单位'
                  focusStyle={focusIndex == '8' ? styles.focusColor : {}}
                  rightElement={
                    <TouchableOpacity onPress={() => { this.setState({ bottomVisible: true, bottomData: SupplierInfo, focusIndex: '8' }) }} >
                      <IconDown text={supSelect} />
                    </TouchableOpacity>
                  }
                />
              </TouchableCustom>

              <TouchableCustom onPress={() => { this.isBottomVisible(Url.PDAnews, 1) }} >
                <ListItemScan
                  focusStyle={focusIndex == '1' ? styles.focusColor : {}}
                  title='库房'
                  rightElement={
                    <IconDown text={roomselect} />
                  }
                />
              </TouchableCustom>

              <ListItemScan
                focusStyle={focusIndex == '2' ? styles.focusColor : {}}
                title='库位'
                onLongPress={() => {
                  this.setState({
                    type: 'libraryid',
                    focusIndex: 2
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}
                rightElement={
                  <View style={{ flexDirection: 'row' }}>
                    <TouchableCustom style={{ height: 30 }} onPress={() => {
                      if (library.length != 0) {
                        this.isBottomVisible(library, 2)
                      } else {
                        this.toast.show('请先选库房')
                      }
                    }} >
                      <View style={{ marginRight: 16, top: 9 }}>
                        <IconDown text={libraryselect} />
                      </View>
                    </TouchableCustom>
                    <ScanButton
                      onPress={() => {
                        this.setState({ focusIndex: '2' })
                        this.props.navigation.navigate('QRCode', {
                          type: 'libraryid',
                          page: 'ReturnGoods'
                        })
                      }}
                      onLongPress={() => {
                        this.setState({
                          type: 'libraryid',
                          focusIndex: 2
                        })
                        NativeModules.PDAScan.onScan();
                      }}
                      onPressOut={() => {
                        NativeModules.PDAScan.offScan();
                      }}
                    />
                  </View>
                }
              />

              <ListItemScan
                focusStyle={focusIndex == '3' ? styles.focusColor : {}}
                title='退库原因'
                rightElement={
                  <IconDown text={'请选择'} />
                  /* <TouchableOpacity onPress={() => { this.setState({ isQuestionOverlay: false, focusIndex: '3' }) }} >
                    <View
                      style={{ flexDirection: "row" }}
                    >
                      <Text>{reasonselect}</Text>
                      <Icon name='downsquare' color='#419fff' iconStyle={{ fontSize: 17, marginTop: -1, marginLeft: 2 }} type='antdesign' ></Icon>
                    </View>
                  </TouchableOpacity> */
                }
              />

              <View style={{ marginHorizontal: width * 0.05, backgroundColor: "#f8f8f8", borderRadius: 5, marginTop: 10 }}>
                <FlatList
                  data={this.state.dictionary}
                  renderItem={this._renderItem.bind(this)}
                  extraData={this.state} />
              </View>

              <ListItemScan
                focusStyle={focusIndex == '4' ? styles.focusColor : {}}
                title='退库日期'
                rightTitle={this._DatePicker()}
                bottomDivider
              />
              <ListItemScan
                title='操作人'
                rightTitle={Url.PDAEmployeeName}
                rightTitleStyle={{ width: width / 1.5, textAlign: 'right', fontSize: 15 }}
                rightTitleProps={{ numberOfLines: 1 }}
                bottomDivider
              />

              <ListItemScan
                focusStyle={focusIndex == '6' ? styles.focusColor : {}}
                title='备注'
                rightElement={
                  <Input
                    containerStyle={[styles.quality_input_container, { top: 2 }]}
                    inputContainerStyle={styles.inputContainerStyle}
                    inputStyle={[styles.quality_input_]}
                    placeholder='请输入'
                    value={remark}
                    onFocus={() => { this.setState({ focusIndex: 6 }) }}
                    onChangeText={(value) => {
                      this.setState({
                        remark: value
                      })
                    }} />
                }
              />

              <ListItemScan
                isButton={true}
                onPressIn={() => {
                  this.setState({
                    type: 'Stockid',
                    focusIndex: 5
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPress={() => {
                  this.setState({
                    type: 'Stockid',
                    focusIndex: 5
                  })
                  NativeModules.PDAScan.onScan();
                }}
                onPressOut={() => {
                  NativeModules.PDAScan.offScan();
                }}
                focusStyle={focusIndex == '5' ? styles.focusColor : {}}
                title='退库明细'
                rightElement={
                  <View style={{ flexDirection: 'row' }}>
                    <View>
                      <Input
                        ref={ref => { this.input1 = ref }}
                        containerStyle={styles.scan_input_container}
                        inputContainerStyle={styles.scan_inputContainerStyle}
                        inputStyle={[styles.scan_input]}
                        placeholder='请输入'
                        keyboardType='numeric'
                        value={compId}
                        onChangeText={(value) => {
                          value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
                          value = value.replace(/^\./g, ""); //验证第一个字符是数字
                          value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
                          //value = value.replace(/\b(0+)/gi, ""); //清楚开头的0
                          this.setState({
                            compId: value
                          })
                        }}
                        //onSubmitEditing={() => { this.input1.focus() }}
                        //onFocus={() => { this.setState({ focusIndex: '5' }) }}
                        /*  */
                        //returnKeyType = 'previous'
                        onSubmitEditing={() => {
                          let inputComId = this.state.compId
                          console.log("🚀 ~ file: qualityinspection.js ~ line 468 ~ QualityInspection ~ inputComId", inputComId)
                          inputComId = inputComId.replace(/\b(0+)/gi, "")
                          this.GetRefundComponentInfo(inputComId)
                        }}
                      />
                    </View>
                    <ScanButton
                      onPress={() => {
                        this.setState({
                          isGetRefundComponentInfo: false,
                          focusIndex: '5'
                        })
                        this.props.navigation.navigate('QRCode', {
                          type: 'Stockid',
                          page: 'ReturnGoods'
                        })
                      }}
                      onLongPress={() => {
                        this.setState({
                          type: 'Stockid',
                          focusIndex: 5
                        })
                        NativeModules.PDAScan.onScan();
                      }}
                      onPressOut={() => {
                        NativeModules.PDAScan.offScan();
                      }}
                    />
                  </View>
                }

              />

              {
                compCode ? this.CardList(compDataArr) : <View></View>
              }

            </View>

            {
              this.state.isCheckPage ? <FormButton
                backgroundColor='#EB5D20'
                onPress={() => {
                  this.ReviseData()
                }}
                title='修改'
              /> :
                <FormButton
                  backgroundColor='#17BC29'
                  title='保存'
                  onPress={() => {
                    this.PostData()
                  }}
                  disabled={this.state.isPhotoUpdate}
                />
            }

             {this.renderZoomPicture()}

            {/* {overlayImg(obj = {
              onRequestClose: () => {
                this.setState({ imageOverSize: !this.state.imageOverSize }, () => {
                  console.log("🚀 ~ file: equipment_maintenance.js:1696 ~ render ~ imageOverSize:", this.state.imageOverSize)
                })
              },
              onPress: () => {
                this.setState({
                  imageOverSize: !this.state.imageOverSize
                })
              },
              uri: this.state.pictureUri,
              isVisible: this.state.imageOverSize
            })
            }*/}

            <Overlay
              fullScreen={true}
              animationType='fade'
              overlayStyle={{ backgroundColor: '#f9f9f9' }}
              isVisible={this.state.isQuestionOverlay}
              onBackdropPress={() => { this.setState({ isQuestionOverlay: !this.state.isQuestionOverlay }); }}
              onRequestClose={() => {
                this.setState({ isQuestionOverlay: !this.state.isQuestionOverlay });
              }}
            >
              <Header
                centerComponent={{ text: '问题缺陷', style: { color: 'gray', fontSize: 20 } }}
                rightComponent={<BackIcon
                  onPress={() => {
                    this.setState({
                      isQuestionOverlay: !this.state.isQuestionOverlay
                    });
                  }} />}
                backgroundColor='white'
              />
              <FlatList
                data={this.state.dictionary}
                renderItem={this._renderItem.bind(this)}
                extraData={this.state} />
            </Overlay>

            <BottomSheet
              isVisible={bottomVisible && !this.state.isCheckPage}
              onRequestClose={() => {
                this.setState({
                  bottomVisible: false
                })
              }}
              onBackdropPress={() => {
                this.setState({
                  bottomVisible: false
                })
              }}
            >
              <ScrollView style={styles.bottomsheetScroll}>
                {
                  bottomData.map((item, index) => {
                    let title = ''
                    if (item.roomId) {
                      title = item.roomName
                    } else if (item.libraryId) {
                      title = item.libraryName
                    } else if (item.driver) {
                      title = item.driver
                    } else if (item.reason) {
                      title = item.reason
                    } else if (item.supId) {
                      title = item.supName
                    }
                    return (
                      <TouchableCustom
                        onPress={() => {
                          if (item.roomId) {
                            this.setState({
                              roomId: item.roomId,
                              roomselect: item.roomName,
                              roomName: item.roomName,
                              library: item.library
                            })
                          } else if (item.libraryId) {
                            this.setState({
                              libraryId: item.libraryId,
                              libraryName: item.libraryName,
                              libraryselect: item.libraryName
                            })
                          } else if (item.reason) {
                            this.setState({
                              reason: item.reason,
                              reasonselect: item.reason,
                            })
                          } else if (item.supId) {
                            this.setState({
                              supId: item.supId,
                              supName: item.supName,
                              supSelect: item.supName
                            })
                          }
                          this.setState({
                            bottomVisible: false
                          })
                        }}
                      >
                        <BottomItem backgroundColor='white' color="#333" title={title} />
                      </TouchableCustom>
                    )

                  })
                }
              </ScrollView>

              <TouchableCustom
                onPress={() => {
                  this.setState({ bottomVisible: false })
                }}>
                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
              </TouchableCustom>
            </BottomSheet>
          </ScrollView>
        </View>
      )
    }
  }

  checkListSecondisVisible = (item) => {
    if (isCheckSecondList.indexOf(item.defectTypeId) == -1) {
      isCheckSecondList.push(item.defectTypeId);
      this.setState({ isCheckSecondList: isCheckSecondList });
    } else {
      if (isCheckSecondList.length == 1) {
        isCheckSecondList.splice(0, 1);
        this.setState({ isCheckSecondList: isCheckSecondList }, () => {
          this.forceUpdate;
        });
      } else {
        isCheckSecondList.map((item1, index) => {
          if (item1 == item.defectTypeId) {
            isCheckSecondList.splice(index, 1);
            this.setState({ isCheckSecondList: isCheckSecondList }, () => {
              this.forceUpdate;
            });
          }
        });
      }
    }
  }

  //主列全选功能
  allMainCheck = (item) => {
    //主列全选
    if (isCheckedAllList.indexOf(item.defectTypeId) == -1) {
      //遍历子列，全部勾选
      item.problemDefects.map((defectitem, defectindex) => {
        if (questionChickIdArr.indexOf(defectitem.rowguid) == -1) {
          questionChickArr.push(defectitem)
          questionChickIdArr.push(defectitem.rowguid)
          this.setState({
            questionChickArr: questionChickArr,
            questionChickIdArr: questionChickIdArr,
          })
        }
      })
      //子列全部勾选就勾选上主列
      isCheckedAllList.push(item.defectTypeId)
      this.setState({ isCheckedAllList: isCheckedAllList })
    }
    //主列取消全选
    else {
      //遍历子列，依次取消勾选
      item.problemDefects.map((defectitem, defectindex) => {
        if (questionChickIdArr.indexOf(defectitem.rowguid) != -1) {
          let num = questionChickIdArr.indexOf(defectitem.rowguid)
          questionChickIdArr.splice(num, 1)
          questionChickArr.splice(num, 1)
          this.setState({
            questionChickArr: questionChickArr,
            questionChickIdArr: questionChickIdArr,
          })
        }
      })
      //把主列数据取消勾选
      let frinum = isCheckedAllList.indexOf(item.defectTypeId)
      isCheckedAllList.splice(frinum, 1)
      this.setState({ isCheckedAllList: isCheckedAllList })
    }
  }

  sonCheck = (item, defectitem) => {
    //点击勾选☑️
    if (questionChickIdArr.indexOf(defectitem.rowguid) == -1) {
      //数组中添加勾选的子列数据
      questionChickArr.push(defectitem)
      questionChickIdArr.push(defectitem.rowguid)
      this.setState({
        questionChickArr: questionChickArr,
        questionChickIdArr: questionChickIdArr,
      })
      //记录子列是否全部勾选，用数量对比
      let count = 0
      item.problemDefects.map((judgeitem) => {
        if (questionChickIdArr.indexOf(judgeitem.rowguid) != -1) {
          count += 1
        }
      })
      //子列全部勾选，就勾选主列
      if (count == item.problemDefects.length) {
        isCheckedAllList.push(item.defectTypeId)
        this.setState({ isCheckedAllList: isCheckedAllList })
      }

    }
    //反勾选 
    else {
      //查出取消勾选项的角标
      let num = questionChickIdArr.indexOf(defectitem.rowguid)
      //删掉！
      questionChickIdArr.splice(num, 1)
      questionChickArr.splice(num, 1)
      this.setState({
        questionChickArr: questionChickArr,
        questionChickIdArr: questionChickIdArr
      })
    }
    //如果子列取消全部勾选，主列取消勾选
    if (questionChickIdArr.indexOf(defectitem.rowguid) == -1) {
      let frinum = isCheckedAllList.indexOf(item.defectTypeId)
      isCheckedAllList.splice(frinum, 1)
      this.setState({ isCheckedAllList: isCheckedAllList })
    }
  }

  _renderItem = ({ item, index }) => {
    const { isCheckSecondListVisible, isCheckSecondList } = this.state
    return (
      <View style={{}}>
        <ListItem
          title={
            <View style={{ flexDirection: 'row' }}>
              {/* <Icon name={isCheckSecondList.indexOf(item.defectTypeId) != -1 ? 'downcircle' : 'rightcircle'} size={18} type='antdesign' color='#419FFF' /> */}
              <Text style={{ fontSize: 14, marginLeft: 4 }}>{item.defectTypeName}</Text>
            </View>
          }
          underlayColor={'lightgray'}
          containerStyle={{ marginVertical: 0, paddingVertical: 10, borderColor: 'transparent', borderWidth: 0, backgroundColor: "transparent" }}
          onPress={() => {
            this.checkListSecondisVisible(item)
          }}
          rightElement={
            <CheckBoxScan
              key={index}
              //checkedColor='red'
              checked={this.state.isCheckedAllList.indexOf(item.defectTypeId) != -1}
              onPress={() => {
                this.allMainCheck(item)
              }
              } />
          } />
        {
          isCheckSecondList.indexOf(item.defectTypeId) != -1 ?
            (
              <View>
                {item.problemDefects.map((defectitem, defectindex) => {
                  return (
                    <View>
                      <ListItem
                        title={defectitem.defect + ": " + defectitem.measure}
                        titleStyle={{ fontSize: 13 }}
                        containerStyle={{ marginVertical: 0, paddingVertical: 10, borderRadius: 0, borderColor: 'lightgray', borderWidth: 0, backgroundColor: "rgba(77,142,245,0.10)" }}
                        onPress={() => {
                          this.sonCheck(item, defectitem)
                        }
                        }
                        underlayColor='lightgray'
                        rightElement={
                          <CheckBoxScan
                            key={index}
                            title=''
                            //checkedColor='red'
                            checked={this.state.questionChickIdArr.indexOf(defectitem.rowguid) != -1}
                          />
                        } />
                    </View>

                  )
                })}
                <View style={{ height: 5, backgroundColor: "rgba(77,142,245,0.10)", borderBottomLeftRadius: 10, borderBottomRightRadius: 10 }}></View>
              </View>
            ) : <View></View>
        }

      </View>
    )
  }



}