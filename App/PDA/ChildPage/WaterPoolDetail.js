import React, { Component } from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, Icon, Card, ListItem, BottomSheet, CheckBox, Avatar, Overlay, Header, Input } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { RFT } from '../../Url/Pixal';
import { saveLoading } from '../../Url/Pixal';
import { ScrollView } from 'react-native-gesture-handler';
import CardList from "../Componment/CardList";
import DatePicker from 'react-native-datepicker'
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { Image } from 'react-native';
import { NavigationEvents } from 'react-navigation';
import ScanButton from '../Componment/ScanButton';
import { styles } from '../Componment/PDAStyles';
import FormButton from '../Componment/formButton';
import ListItemScan from '../Componment/ListItemScan';
import ListItemScan_child from '../Componment/ListItemScan_child';
import IconDown from '../Componment/IconDown';
import BottomItem from '../Componment/BottomItem';
import TouchableCustom from '../Componment/TouchableCustom';
import CheckBoxScan from '../Componment/CheckBoxScan';
import AvatarAdd from '../Componment/AvatarAdd';
import AvatarImg from '../Componment/AvatarImg';
import CheckBoxchild from '../Componment/CheckBoxchild';
import DeviceStorage from '../../Url/DeviceStorage';

const { height, width } = Dimensions.get('window') //获取宽高

let compDataArr = [], compIdArr = [], QRid2 = '', tmpstr = '', visibleArr = [];

export default class WaterPoolDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      rowguid: '',
      ServerTime: '2000-01-01 00:00',
      startTime: '',
      endTime: '',
      poolList: [],
      formcode: '',

      poolid: '',
      poolname: '',
      poolselect: '',
      poolcapacity: '',
      remaincapacity: '',

      compDataArr: [],
      compData: '',
      compId: '',
      compIdArr: [],
      compCode: '',
      isGetcomID: false,

      isLoading: true,
      isGetPoolID: false,
      conserveDay: "7",
      //底栏控制
      bottomData: [],
      bottomVisible: false,
      focusIndex: 1,
      isCheckPage: false,
      maintainstatus: "",
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    let guid = navigation.getParam('guid') || ''
    let pageType = navigation.getParam('pageType') || ''
    let maintainstatus = navigation.getParam('maintainstatus') || ''

    if (pageType == 'CheckPage') {
      this.checkResData(guid)
      this.setState({ isCheckPage: true, maintainstatus: maintainstatus })
    } else {
      this.resData()
      //this.GetPoolID()
    }
  }

  componentWillUnmount() {
    compDataArr = [], compIdArr = [], QRid2 = '', tmpstr = '', visibleArr = [];

  }

  UpdateControl = () => {
    if (typeof this.props.navigation.getParam('QRid') != "undefined") {
      if (this.props.navigation.getParam('QRid') != "") {
        this.toast.show(Url.isLoadingView, 0)
      }
    }
    const { navigation } = this.props;
    const QRid = navigation.getParam('QRid') || ''
    let type = navigation.getParam('type') || '';
    if (type == 'poolid') {
      this.GetPoolID(QRid)
    }
    if (type == 'compid') {
      this.GetcomID(QRid)
    }
  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  getNewData = (dateProps, days) => {
    let nDate = new Date(dateProps); //转换为MM-DD-YYYY格式    
    let millSeconds = nDate.getTime() + (days * 24 * 60 * 60 * 1000);
    console.log("🚀 ~ file: WaterPoolDetail.js:113 ~ WaterPoolDetail ~ millSeconds:", millSeconds)
    let rDate = new Date(millSeconds);
    console.log("🚀 ~ file: WaterPoolDetail.js:115 ~ WaterPoolDetail ~ rDate:", rDate)
    let year = rDate.getFullYear();
    let month = rDate.getMonth() + 1;
    if (month < 10) month = "0" + month;
    let date = rDate.getDate();
    if (date < 10) date = "0" + date;
    return (year + "-" + month + "-" + date);
  }

  checkResData = (guid) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getwaterpooldetail",
      "servicetype": "pda",
      "express": "8AC4C0B0",
      "ciphertext": "7df83f712027c63f147f6c2d4a43f08d",
      "data": {
        "_rowguid": guid,
        "factoryId": Url.PDAFid,
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: WaterPoolDetail.js:134 ~ WaterPoolDetail ~ formData:", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: WaterPoolDetail.js:147 ~ WaterPoolDetail ~ resData:", resData)

      let rowguid = resData.result._rowguid
      let ServerTime = resData.result.editDate
      let poolName = resData.result.poolName
      let poolid = resData.result.poolId
      let formcode = resData.result.formCode
      let startDate = resData.result.startDate
      let endDate = resData.result.endDate
      let maintainDays = resData.result.maintainDays
      let sumCount = resData.result.sumCount
      let editDate = resData.result.editDate
      let editEmployeeName = resData.result.editEmployeeName
      let subs = resData.result.subs
      subs.map((item, index) => {
        compDataArr.push(item)
        compIdArr.push(item.compcode)
        tmpstr = tmpstr + JSON.stringify(item.compcode) + ','
      })
      let compCode = resData.result.subs[0].compcode

      this.setState({
        resData: resData,
        rowguid: rowguid,
        ServerTime: ServerTime,
        poolname: poolName,
        poolselect: poolName,
        startTime: startDate,
        endTime: endDate,
        poolid: poolid,
        formcode: formcode,
        conserveDay: maintainDays,
        editEmployeeName: editEmployeeName,
        compDataArr: compDataArr,
        compIdArr: compIdArr,
        compCode: compCode,
        sumCount: sumCount,
        isGetPoolID: true,
        isGetcomID: true
      })
      this.setState({
        isLoading: false,
      })
    }).catch((error) => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 215 ~ QualityInspection ~ error", error)
    });
  }

  resData = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "initwaterpoollist",
      "servicetype": "pda",
      "express": "8AC4C0B0",
      "ciphertext": "7df83f712027c63f147f6c2d4a43f08d",
      "data": {
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: WaterPoolDetail.js:219 ~ WaterPoolDetail ~ resData:", resData)
      //初始化错误提示
      if (resData.status == 100) {
        let ServerTime = resData.result.dateTime
        let startTime = resData.result.dateTime
        let timeArr = ServerTime.split(" ")
        let tmpdate = 7
        let endDate = this.getNewData(timeArr[0], tmpdate)
        let endTime = endDate + ' ' + timeArr[1]
        console.log("🚀 ~ file: WaterPoolDetail.js:217 ~ WaterPoolDetail ~ endTime:", endTime)
        console.log("🚀 ~ file: WaterPoolDetail.js:217 ~ WaterPoolDetail ~ endDate:", endDate)
        let rowguid = resData.result._rowguid
        let poolList = resData.result.poolList
        this.setState({
          resData: resData,
          rowguid: rowguid,
          ServerTime: ServerTime,
          startTime: startTime,
          endTime: endTime,
          poolList: poolList,
          isLoading: false,
        })
      }
      else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }

    }).catch((error) => {
      console.log("🚀 ~ file: WaterPoolDetail.js:247 ~ WaterPoolDetail ~ error:", error)
      Alert.alert("错误提示", '服务器查询错误', [{
        text: '取消',
        style: 'cancel'
      }, {
        text: '返回上一级',
        onPress: () => {
          this.props.navigation.goBack()
        }
      }])
    });

  }

  PostData = () => {
    const { rowguid, ServerTime, poolid, poolname, formcode, endTime, startTime, conserveDay } = this.state

    if (poolid.length == 0) {
      this.toast.show('水养池信息不能为空')
      return
    }
    if (compDataArr.length == 0) {
      this.toast.show('没有扫描构件')
      return
    }

    if (conserveDay.length == 0) {
      this.toast.show('没有填写养护周期')
      return
    }

    this.toast.show(saveLoading, 0)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "savewaterpooldetail",
      "servicetype": "pda",
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        "saveType": 'add',
        "_bizid": Url.PDAFid,
        "_rowguid": rowguid,
        "poolId": poolid,
        "poolName": poolname,
        "formCode": formcode,
        "userName": Url.PDAusername,
        "editDate": ServerTime,
        "startDate": startTime,
        "endDate": endTime,
        "maintainDays": conserveDay.toString(),
        "sumCount": compDataArr.length,
        "subs": compDataArr
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: WaterPoolPH.js:232 ~ WaterPoolPH ~ formData:", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('保存成功');
        setTimeout(() => {
          this.props.navigation.replace(this.props.navigation.getParam("page"), {
            title: this.props.navigation.getParam("title"),
            pageType: this.props.navigation.getParam("pageType"),
            page: this.props.navigation.getParam("page"),
          })
        }, 400)
      } else {
        this.toast.close()
        Alert.alert('保存失败', resData.message)

      }
      console.log("🚀 ~ file: steel_cage_storage.js ~ line 195 ~ SteelCage ~ resData", resData)
    }).catch((error) => {
      this.toast.show('保存失败')
      console.log("🚀 ~ file: steel_cage_storage.js ~ line 75 ~ SteelCage ~ error", error)
    });
  }

  ReviseData = () => {
    const { rowguid, ServerTime, poolid, poolname, formcode, endTime, startTime, conserveDay } = this.state

    if (poolid.length == 0) {
      this.toast.show('水养池信息不能为空')
      return
    }
    if (compDataArr.length == 0) {
      this.toast.show('没有扫描构件')
      return
    }

    if (conserveDay.length == 0) {
      this.toast.show('没有填写养护周期')
      return
    }

    this.toast.show(saveLoading, 0)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "savewaterpooldetail",
      "servicetype": "pda",
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        "saveType": 'update',
        "_bizid": Url.PDAFid,
        "_rowguid": rowguid,
        "poolId": poolid,
        "poolName": poolname,
        "formCode": formcode,
        "userName": Url.PDAusername,
        "editDate": ServerTime,
        "startDate": startTime,
        "endDate": endTime,
        "maintainDays": conserveDay.toString(),
        "sumCount": compDataArr.length,
        "subs": compDataArr
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: WaterPoolPH.js:232 ~ WaterPoolPH ~ formData:", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('保存成功');
        this.props.navigation.goBack()
      } else {
        this.toast.close()
        Alert.alert('保存失败', resData.message)

      }
      console.log("🚀 ~ file: steel_cage_storage.js ~ line 195 ~ SteelCage ~ resData", resData)
    }).catch((error) => {
      this.toast.show('保存失败')
      console.log("🚀 ~ file: steel_cage_storage.js ~ line 75 ~ SteelCage ~ error", error)
    });
  }

  DeleteData = () => {

    this.toast.show('删除中', 10000);

    let formData = new FormData();
    let data = {};
    data = {
      "action": "deletewaterpooldetail",
      "servicetype": "projectsystem",
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        "_rowguid": guid,
        "factoryId": Url.PDAFid
      }
    }

    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: main_compreceive.js ~ line 358 ~ miain_compreceive ~ DeleteData ~ formData", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('删除成功');
        this.props.navigation.goBack()
      } else {
        Alert.alert('删除失败', resData.message)
      }
    }).catch((error) => {
    });
  }

  GetPoolID = (id) => {
    const { poolid } = this.state
    let formData = new FormData();
    let data = {};
    data = {
      "action": "initwaterpoolinfo",
      "servicetype": "pda",
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        //"poolId": "FDC76BA329B7401B97C43D382EE6A056",
        "poolId": id,
        "factoryId": Url.PDAFid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: WaterPoolPH.js:286 ~ WaterPoolPH ~ formData:", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      this.toast.close()
      if (resData.status == '100') {
        console.log("🚀 ~ file: equipment_maintenance.js ~ line 360 ~ EquipmentMaintenance ~ resData", resData)
        let rowguid = resData.result._rowguid
        let ServerTime = resData.result.dateTime
        let poolInfo = resData.result.poolInfo
        let poolcapacity = poolInfo.poolcapacity
        console.log("🚀 ~ file: WaterPoolDetail.js:492 ~ WaterPoolDetail ~ poolcapacity:", poolcapacity)
        let poolId = poolInfo.poolid
        let formCode = poolInfo.formcode
        let poolName = poolInfo.poolname
        let remaincapacity = poolInfo.remaincapacity
        console.log("🚀 ~ file: WaterPoolPH.js:367 ~ WaterPoolPH ~ poolName:", poolName)
        this.setState({
          rowguid: rowguid,
          // ServerTime: ServerTime,
          poolid: poolId,
          formcode: formCode,
          poolname: poolName,
          poolselect: poolName,
          poolcapacity: poolcapacity,
          remaincapacity: remaincapacity,
          isGetPoolID: true
        }, () => {
          this.forceUpdate
        })
      } else {
        console.log('resData', resData)
        Alert.alert('ERROR', resData.message)
      }

    }).catch(err => {
      console.log("🚀 ~ file: equipment_maintenance.js ~ line 400 ~ EquipmentMaintenance ~ err", err)

    })
  }

  GetcomID = (id) => {
    const { rowguid } = this.state
    let formData = new FormData();
    let data = {};
    data = {
      "action": "initductpieceinfo",
      "servicetype": "pda",
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        "_rowguid": rowguid,
        "factoryId": Url.PDAFid,
        "compId": id
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: storage.js ~ line 241 ~ PDAStorage ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: WaterPoolDetail.js:384 ~ WaterPoolDetail ~ resData:", resData)
      this.toast.close()
      if (resData.status == '100') {
        let compData = resData.result
        compData._rowguid = ""
        let compCode = compData.compCode
        if (compDataArr.length == 0) {
          compDataArr.push(compData)
          compIdArr.push(compCode)
          this.setState({
            compDataArr: compDataArr,
            compIdArr: compIdArr,
            compData: compData,
            compId: "",
            compCode: compCode,
            isGetcomID: true
          })
          tmpstr = tmpstr + JSON.stringify(compCode) + ','
        } else {
          if (tmpstr.indexOf(compCode) == -1) {
            compIdArr.push(compCode)
            compId = this.state.compCode + ',' + compCode
            compDataArr.push(compData)
            this.setState({
              compDataArr: compDataArr,
              compIdArr: compIdArr,
              compData: compData,
              compId: "",
              compCode: compCode,
              isGetcomID: true
            })
            tmpstr = tmpstr + JSON.stringify(compCode) + ','
          } else {
            this.toast.show('已经扫瞄过此构件')
          }

        }


        console.log("🚀 ~ file: storage.js ~ line 292 ~ PDAStorage ~ compDataArr", compDataArr)

      } else {
        Alert.alert('错误', resData.message)
        this.setState({
          compId: ""
        })
        //this.input1.focus()
      }

    }).catch(err => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 210 ~ SteelCage ~ err", err)
    })
  }

  comIDItem = () => {
    const { isGetPoolID, poolList, poolselect, isCheckPage } = this.state
    return (
      <View>
        <View style={{ flexDirection: 'row' }}>
          {
            !isCheckPage ? <TouchableCustom
              style={{ height: 30 }}
              onPress={() => {
                this.setState({ bottomVisible: true, bottomData: poolList, focusIndex: 1 })
              }} >
              <View style={{ marginRight: 16, top: 9 }}>
                <IconDown text={'请选择'} />
              </View>
            </TouchableCustom> : <View></View>
          }

          {
            !isGetPoolID ?
              <ScanButton
                onPress={() => {
                  this.setState({ focusIndex: 1, isGetStoreroom: false })
                  this.props.navigation.navigate('QRCode', {
                    type: 'poolid',
                    page: 'WaterPoolDetail'
                  })

                }}
              /> :
              <View style={{ top: 8 }}>
                <TouchableOpacity
                  style={{ height: 30 }}
                  onLongPress={() => {
                    Alert.alert(
                      '提示信息',
                      '是否删除水养池信息',
                      [{
                        text: '取消',
                        style: 'cancel'
                      }, {
                        text: '删除',
                        onPress: () => {
                          this.setState({
                            isGetPoolID: false,
                          })
                        }
                      }])
                  }} >
                  <Text allowFontScaling={false}>{poolselect}</Text>
                </TouchableOpacity>
              </View>
          }
        </View>
      </View>

    )
  }

  _DatePicker = (index) => {
    const { ServerTime, startTime, endTime } = this.state
    let Time = ''
    if (index == 3) {
      Time = startTime
    }
    if (index == 4) {
      Time = endTime
    }
    if (index == 5) {
      Time = ServerTime
    }
    let disable = false
    if (index == 4) {
      disable = true
    }
    if (index == 5) {
      disable = true
    }
    if (this.state.isCheckPage && index == 3) {
      disable = true
    }
    return (
      <DatePicker
        customStyles={{
          dateInput: styles.dateInput,
          dateTouchBody: styles.dateTouchBody,
          dateText: !disable ? styles.dateText : styles.dateDisabled,
        }}
        iconComponent={<Icon name='caretdown' color={!disable ? '#419fff' : "#666"} iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
        }
        allowFontScaling={false}
        onOpenModal={() => {
          this.setState({ focusIndex: index })
        }}
        showIcon={true}
        mode='datetime'
        date={Time}
        format="YYYY-MM-DD HH:mm"
        disabled={disable}
        onDateChange={(value) => {
          console.log("🚀 ~ file: WaterPoolDetail.js:593 ~ WaterPoolDetail ~ value:", value)

          if (index == 5) {
            this.setState({
              ServerTime: value
            })
          }
          if (index == 3) {
            let timeArr = value.split(" ")
            let tmpdate = this.state.conserveDay
            let endDate = this.getNewData(timeArr[0], tmpdate)
            let endTime = endDate + ' ' + timeArr[1]
            this.setState({
              startTime: value,
              endTime: endTime
            })
          }
          if (index == 4) {
            this.setState({
              endTime: value
            })
          }

        }}
      />
    )
  }

  cardListContont = comp => {
    if (typeof comp.compCode == 'undefined') {
      comp.compCode = comp.compcode
    }
    if (visibleArr.indexOf(comp.compCode) == -1) {
      visibleArr.push(comp.compCode);
      this.setState({ visibleArr: visibleArr });
    } else {
      if (visibleArr.length == 1) {
        visibleArr.splice(0, 1);
        this.setState({ visibleArr: visibleArr }, () => {
          this.forceUpdate;
        });
      } else {
        visibleArr.map((item, index) => {
          if (item == comp.compCode) {
            visibleArr.splice(index, 1);
            this.setState({ visibleArr: visibleArr }, () => {
              this.forceUpdate;
            });
          }
        });
      }
    }
  };

  CardList = (compData) => {
    return (
      compData.map((item, index) => {
        let comp = item
        return (
          <View style={styles.CardList_main}>
            <TouchableCustom
              onPress={() => {
                this.cardListContont(comp);
              }}
              onLongPress={() => {
                Alert.alert(
                  '提示信息',
                  '是否删除构件信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      compDataArr.splice(index, 1)
                      compIdArr.splice(index, 1)
                      tmpstr = compIdArr.toString()

                      this.setState({
                        compDataArr: compDataArr,
                        compIdArr: compIdArr
                      })
                    }
                  }])

              }}>
              <View style={styles.CardList_title_main}>
                <View style={styles.title_num}>
                  <Text >{index + 1}</Text>
                </View>
                <View style={styles.title_title}>
                  <Text numberOfLines={1}>{item.compCode || comp.compcode}</Text>
                </View>
                <View style={styles.CardList_at_icon}>
                  <IconDown />
                </View>
              </View>
            </TouchableCustom>
            {
              visibleArr.indexOf(item.compCode) != -1 ?
                <View key={index} style={{ backgroundColor: 'transparent' }} >
                  <ListItemScan_child
                    title='产品编号'
                    rightTitle={comp.compCode || comp.compcode}
                  />
                  <ListItemScan_child
                    title='项目名称'
                    rightTitle={comp.projectName || comp.projectname}
                  />
                  <ListItemScan_child
                    title='设计型号'
                    rightTitle={comp.designType || comp.designtype}
                  />
                  {
                    typeof (comp.buildingUnit) == 'undefined' ? <View></View> :
                      <ListItemScan_child
                        title='单元号'
                        rightTitle={comp.buildingUnit}
                      />
                  }
                  <ListItemScan_child
                    title='楼号'
                    rightTitle={comp.floorNoName || comp.floornoname}
                  />
                  <ListItemScan_child
                    title='层号'
                    rightTitle={comp.floorName || comp.floorname}
                  />
                  <ListItemScan_child
                    title='生产单位'
                    rightTitle={Url.PDAFname}
                  />
                  <View style={{ height: 10 }}></View>
                </View> : <View></View>
            }

          </View>
        )
      })
    )
  }

  render() {
    const { isLoading, conserveDay, focusIndex, isCheckPage, bottomVisible, bottomData, compId, compCode } = this.state
    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          <NavigationEvents
            onWillFocus={this.UpdateControl} />
          <ScrollView style={styles.mainView} showsVerticalScrollIndicator={false} >
            <View style={styles.listView}>
              <ListItemScan
                isButton={true}
                focusStyle={focusIndex == '1' ? styles.focusColor : {}}
                title='水养池名称'
                rightElement={
                  this.comIDItem()
                }
              />
              <ListItemScan
                containerStyle={focusIndex == '2' ? { backgroundColor: '#A9E2F3' } : {}}
                bottomDivider
                title='养护时长'
                rightElement={
                  <View style={{ flexDirection: 'row' }}>
                    <Input
                      containerStyle={{
                        width: width * 0.15,
                        height: 18,
                        marginVertical: 0,
                        paddingVertical: 0
                      }}
                      inputContainerStyle={{
                        height: 18
                      }}
                      inputStyle={{
                        fontSize: 16,
                        textAlign: "center"
                      }}
                      keyboardType='number-pad'
                      allowFontScaling={false}
                      value={conserveDay}
                      onChangeText={(value) => {
                        value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
                        value = value.replace(/^\./g, ""); //验证第一个字符是数字
                        value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
                        value = value.replace(/\b(0+)/gi, ""); //清楚开头的0
                        this.setState({
                          conserveDay: value,
                        })
                        if (value.length > 0) {
                          let timeArr = this.state.startTime.split(" ")
                          let tmpdate = parseInt(value)
                          let endDate = this.getNewData(timeArr[0], tmpdate)
                          let endTime = endDate + ' ' + timeArr[1]
                          this.setState({
                            endTime: endTime
                          })
                        }
                      }} />
                    <Text allowFontScaling={false}>天</Text>
                  </View>
                }
              />
              <ListItemScan
                focusStyle={focusIndex == '3' ? styles.focusColor : {}}
                title='养护开始时间'
                rightElement={
                  this._DatePicker('3')
                }
              />
              <ListItemScan
                focusStyle={focusIndex == '4' ? styles.focusColor : {}}
                title='养护结束时间'
                rightElement={
                  this._DatePicker('4')
                }
              />
              <ListItemScan
                focusStyle={focusIndex == '5' ? styles.focusColor : {}}
                title='编制时间'
                rightElement={
                  this._DatePicker('5')
                }
              />
              <ListItemScan
                title='编制人'
                rightTitle={Url.PDAEmployeeName}
              />

              <ListItemScan
                isButton={true}
                focusStyle={focusIndex == '6' ? styles.focusColor : {}}
                title='水养明细'
                rightElement={
                  <View style={{ flexDirection: 'row' }}>
                    <View>
                      <Input
                        ref={ref => { this.input1 = ref }}
                        containerStyle={styles.scan_input_container}
                        inputContainerStyle={styles.scan_inputContainerStyle}
                        inputStyle={[styles.scan_input]}
                        allowFontScaling={false}
                        placeholder='请输入'
                        keyboardType="numbers-and-punctuation"
                        value={compId}
                        onChangeText={(value) => {
                          value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
                          value = value.replace(/^\./g, ""); //验证第一个字符是数字
                          value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
                          //value = value.replace(/\b(0+)/gi, ""); //清楚开头的0
                          this.setState({
                            compId: value
                          })
                        }}
                        onSubmitEditing={() => {
                          let inputComId = this.state.compId
                          console.log("🚀 ~ file: qualityinspection.js ~ line 468 ~ QualityInspection ~ inputComId", inputComId)
                          inputComId = inputComId.replace(/\b(0+)/gi, "")
                          this.GetcomID(inputComId)
                        }}
                      />
                    </View>
                    <ScanButton
                      //disabled={pageType == 'CheckPage'}
                      onPress={() => {
                        this.setState({
                          isGetcomID: false,
                          focusIndex: 6
                        })
                        this.props.navigation.navigate('QRCode', {
                          type: 'compid',
                          page: 'WaterPoolDetail'
                        })
                      }}
                    />
                  </View>
                }
              />
              {
                compCode ? this.CardList(compDataArr) : <View></View>
              }
            </View>

            {
              this.state.maintainstatus != '养护完成' ?
                <FormButton
                  onPress={isCheckPage ? this.ReviseData : this.PostData}
                  title={isCheckPage ? '修改' : '保存'}
                  backgroundColor={isCheckPage ? '#EB5D20' : '#17BC29'}
                /> : <></>
            }



            <BottomSheet
              isVisible={bottomVisible}
              onRequestClose
            >
              <ScrollView style={styles.bottomsheetScroll}>
                {
                  bottomData.map((item, index) => {
                    let title = ''
                    if (item.poolid) {
                      title = item.poolname
                    }
                    return (
                      <TouchableCustom
                        onPress={() => {
                          if (item.poolid) {
                            this.setState({
                              poolid: item.poolid,
                              poolselect: item.poolname,
                              poolname: item.poolname,
                              poolcapacity: item.poolcapacity,
                              remaincapacity: item.remaincapacity,
                              formcode: item.formcode,
                              isGetPoolID: true
                            })
                          }
                          this.setState({
                            bottomVisible: false
                          })
                        }}
                      >
                        <BottomItem backgroundColor='white' color="#333" title={title} />

                      </TouchableCustom>
                    )

                  })
                }
              </ScrollView>
              <TouchableCustom
                onPress={() => {
                  this.setState({ bottomVisible: false })
                }}>
                <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
              </TouchableCustom>

            </BottomSheet>
          </ScrollView>
        </View>
      );
    }
  }
}
