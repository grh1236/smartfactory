import React from 'react';
import { View, StyleSheet } from 'react-native';
import { deviceHeight, deviceWidth, RFT } from '../../Url/Pixal';

let width = deviceWidth

export const styles = StyleSheet.create({
  loading: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  mainView: {
    backgroundColor: '#F0F1F7',
    flex: 1,
    //minHeight: deviceHeight * 2
  },
  listView: {
    marginHorizontal: width * 0.06,
    marginTop: width * 0.04,
    paddingHorizontal: 0,
    paddingTop: 5,
    paddingBottom: 15,
    backgroundColor: 'white',
    borderRadius: 10
  },
  focusColor: {
    backgroundColor: 'rgba(254,211,48,1.0)'
  },
  //安卓ios不同
  dateInput: {
    borderColor: 'transparent',
    right: width * 0.05
    //height: 16,
    // marginLeft: 17,
    //backgroundColor: 'red'
  },
  dateTouchBody: {
    height: 14,
    right: -8,
    //width: width / 2.52,
    //backgroundColor: 'yellow'
  },
  dateText: {
    width: width * 0.4,
    textAlign: 'right',
    // marginRight: 20,
    color: '#4D8EF5',
    //backgroundColor: 'blue'
  },
  dateDisabled: {
    textAlign: 'right',
    width: width * 0.4,
    color: '#666'
  },
  title_child: {
    fontSize: 13,
    color: '#999'
  },
  rightTitle_child: {
    color: '#333',
    fontSize: 13,
    textAlign: 'right',
    lineHeight: 14,
    flexWrap: 'wrap',
    width: deviceWidth / 2
  },
  downsquareTitle: {
    fontSize: 13,
    textAlignVertical: 'center',
    textAlign: 'right',
    width: width * 0.45
  },
  //安卓ios不同
  bottomsheetScroll: {
    height: 290
  },
  loading_input_view: { flexDirection: "row", marginRight: -5, },
  //list_container_style: { height: 14 },
  list_container_style: { marginVertical: 0, paddingVertical: 10, backgroundColor: 'transparent' },
  input_container: {
    width: deviceWidth / 2.3,
    height: 14,
    marginTop: -12,
  },
  input_: {
    fontSize: RFT * 3.4,
    textAlign: 'right'
  },
  inputContainerStyle: { backgroundColor: 'transparent', borderColor: 'transparent' },
  text_beforeinput: { width: deviceWidth / 8, textAlign: 'left', fontSize: RFT * 3.6 },
  quality_input_container: {
    width: width / 2.4,
    height: 18,
    marginTop: -25,
    marginRight: -16,
  },
  quality_input_: {
    fontSize: 14,
    textAlign: "right"
  },
  scan_input_container: {
    height: 18,
    top: -3,
    left: -10,
    width: width / 2.8
  },
  scan_inputContainerStyle: {
    backgroundColor: 'transparent', borderColor: 'transparent'
  },
  scan_input: {
    fontSize: 14,
    textAlign: "right"
  },
  CardList_main: {
    width: width * 0.8,
    marginLeft: width * 0.05,
    marginTop: 10,
    borderRadius: 10,
    backgroundColor: '#f8f8f8'
  },
  CardList_title_main: {
    backgroundColor: 'transparent',
    color: "#333",
    paddingVertical: 15,
    paddingHorizontal: 15,
    flexDirection: 'row'
  },
  title_num: {
    flex: 1
  },
  title_title: {
    flex: 6
  },
  CardList_at_icon: {
    flex: 1
  },
  ProblemTitle: {
    fontSize: 14,
    marginTop: 10,
    width: width * 0.3,
    marginLeft: width * 0.03,
    textAlignVertical: 'center',
    //textAlign: 'center',
    borderRightColor: 'gray',
    borderRightWidth: 0.3
  },
  rightProblemTitle: {
    fontSize: 14,
    marginTop: 10,
    width: width * 0.4,
    textAlignVertical: 'center',
    textAlign: 'center',
    borderLeftColor: 'gray',
    borderLeftWidth: 0.3
  },
  problemList_container_style: {
    flexDirection: 'row',
    height: 30,
    justifyContent: 'center',
    borderLeftColor: 'gray',
    borderLeftWidth: 0.3,
    borderRightColor: 'gray',
    borderRightWidth: 0.3,
    borderTopColor: 'gray',
    borderTopWidth: 0.3
  },
  text: {
    fontSize: 14,
    marginBottom: 5,
    marginTop: 5,
    flex: 1
  },
  textright: {
    textAlign: 'right',
    color: 'gray'
  },
  textfir: {
    fontSize: 18,
    flex: 1.5
  },
  textthi: {
    fontSize: 15
  },
  textname: {
    fontSize: 14,
    marginBottom: 8,
    marginTop: 8,
    color: 'gray'
  },
  textView: {
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  card_containerStyle: {
    borderRadius: 10,
    shadowOpacity: 0,
    backgroundColor: '#f8f8f8',
    borderWidth: 0,
    paddingVertical: 13,
    elevation: 0
  },
  card_titleStyle: {
    fontSize: 13,
    color: '#999'
  },
  SN_View: {
    flex: 1
  },
  SN_Text_View: {
    top: 7
  },
  SN_Text: {
    textAlignVertical: 'center',
    fontSize: 16,
    color: '#535c68'
  },
  SN_Text_Comp: {
    fontSize: 16,
  },
})

if (deviceWidth <= 330) {
  //安卓ios不同
  styles.dateInput = {
    borderColor: 'transparent',
    //height: 16,
    //width: width,
    // marginLeft: 17,
    //backgroundColor: 'red'
  }
  styles.dateTouchBody = {
    height: 14,
    right: -8,
    //width: width / 2.52,
    //backgroundColor: 'yellow'
  }
  styles.dateText = {
    //width: width / 3,
    textAlign: 'right',
    // marginRight: 20,
    color: '#4D8EF5',
    //backgroundColor: 'blue'
  }
  styles.CardList_title_main = {
    backgroundColor: 'transparent',
    color: "#333",
    paddingVertical: 15,
    paddingHorizontal: 15,
    flexDirection: 'row'
  }
  styles.title_num = {
    flex: 1
  }
  styles.title_title = {
    flex: 3.3
  }
  styles.CardList_at_icon = {
    flex: 1
  }
  styles.card_containerStyle = {
    borderRadius: 10,
    shadowOpacity: 0,
    backgroundColor: '#f8f8f8',
    borderWidth: 0,
    paddingVertical: 8,
    paddingHorizontal: 5,
    elevation: 0
  }
  styles.title_child = {
    fontSize: 13,
    color: '#999'
  }
  styles.rightTitle_child = {
    color: '#333',
    fontSize: 13,
    textAlign: 'right',
    lineHeight: 18,
    flexWrap: 'wrap',
    width: deviceWidth * 0.45
  }
  styles.downsquareTitle = {
    fontSize: 13,
    textAlign: 'right',
    textAlignVertical: 'center',
    width: width * 0.45
  }
  styles.quality_input_container= {
    width: width * 0.6,
    height: 18,
    marginTop: -25,
    marginRight: -16,
  }
}
