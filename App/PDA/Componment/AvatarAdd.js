import React from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Avatar, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, ListItem, BottomSheet } from 'react-native-elements';
import Url from '../../Url/Url';
import { RFT } from '../../Url/Pixal';
import { styles } from './PDAStyles';

const { height, width } = Dimensions.get('window') //获取宽高

export default class AvatarAdd extends React.Component {
  render() {
    const { pictureSelect, title, backgroundColor, color } = this.props
    return (
      <View>
        <Avatar
          {...this.props}
          size={pictureSelect ? 38 : 56}
          rounded
          containerStyle={[pictureSelect ? { marginBottom: 10 } : {}, { marginLeft: 7, }]}
          overlayContainerStyle={{ backgroundColor: backgroundColor, }}
          title={title}
          titleStyle={[pictureSelect ? { fontSize: 18 } : { fontSize: 24 }, { color: color }]}
          activeOpacity={0.7} />
      </View>
    )
  }
}

