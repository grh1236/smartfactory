/*
 * @Author: 高睿豪 1375308739@qq.com
 * @Date: 2023-04-28 08:49:15
 * @LastEditors: error: error: git config user.name & please set dead value or install git && error: git config user.email & please set dead value or install git & please set dead value or install git
 * @LastEditTime: 2023-07-11 08:50:35
 * @FilePath: /SmartFactory/App/PDA/Componment/OverlayImg.js
 * @Description: 
 * 
 * Copyright (c) 2023 by ${git_name_email}, All Rights Reserved. 
 */
import React from 'react';
import { ScrollView, Text, TouchableHighlight, View, PanResponder, LayoutChangeEvent } from 'react-native';
import { Header, Icon, Image, Overlay } from 'react-native-elements';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { deviceHeight, deviceWidth } from '../../Url/Pixal';
import Animated from 'react-native-reanimated';

const overlayImg = (props) => {
  const { onRequestClose, uri, isVisible, onPress } = props;
  return (
    <View>
      <Overlay
        fullScreen={true}
        animationType='fade'
        isVisible={isVisible}
        onRequestClose={onRequestClose}>
        <Header
          containerStyle={{ height: 45, paddingTop: 0, top: -5 }}
          centerComponent={{ text: '图片缩放', style: { color: 'gray', fontSize: 20 } }}
          rightComponent={
            <View >
              <TouchableHighlight
                onPress={onRequestClose} >
                <Icon
                  name='arrow-back'
                  color='#419FFF'
                  onPress={onRequestClose} />
              </TouchableHighlight>
            </View>}
          backgroundColor='white' />
        <ScrollView
          maximumZoomScale={3}
          minimumZoomScale={0.8}
          centerContent={true} >
          <Image
            source={{ uri: uri }}
            resizeMethod='resize'
            resizeMode='contain'
            style={{ height: deviceHeight * 0.8 }}
          />
        </ScrollView>

      </Overlay>
    </View>
  )
}

const BackIcon = (onPress) => {
  return (
    <TouchableOpacity
      onPress={onPress} >
      <Icon
        name='arrow-back'
        color='#419FFF' />
    </TouchableOpacity>
  )
}

export default overlayImg;
