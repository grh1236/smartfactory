import React, { PureComponent } from 'react';
import {
  StatusBar,
  StyleSheet,
  FlatList,
  Modal,
  Vibration,
  SectionList,
  TouchableOpacity,
  Animated,
  PermissionsAndroid,
  default as Easing,
  ImageBackground,
  View
} from 'react-native';
import { withNavigation } from 'react-navigation';
import { RNCamera } from 'react-native-camera';
import { Button, Text } from 'react-native-elements';


const PendingView = () => (
  <View
    style={{
      flex: 1,
      backgroundColor: 'lightgreen',
      justifyContent: 'center',
      alignItems: 'center',
    }}
  >
    <Text>Waiting</Text>
  </View>
);

class QRcode extends PureComponent {
  constructor() {
    super()
    this.state = {
      //中间横线动画初始值
      moveAnim: new Animated.Value(0),
      isScan: false
    }
  }

  componentDidMount() {
    this.startAnimation();
    console.log('QRCode')
  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: "扫码",
      
    }
  }


  /** 扫描框动画*/
  startAnimation = () => {
    this.state.moveAnim.setValue(0);
    Animated.timing(
      this.state.moveAnim,
      {
        toValue: -200,
        duration: 500,
        easing: Easing.linear
      }
    ).start(() => this.startAnimation());

  }

  onBarCodeRead = (result) => {
    this.setState({
      isScan: true
    }, () => {
      const { navigation } = this.props;
      const type1 = navigation.getParam('type') || '';
      const page = navigation.getParam('page') || '';
      console.log("🚀 ~ file: PDACamera.js ~ line 66 ~ QRcode ~ page", page)
      console.log('onBarCodeRead')
      const { data, type, bounds } = result; //只要拿到data就可以了
      console.log('type', type)
      console.log('bounds', bounds)
      //扫码后的操作
      console.log(data)
      // alert(data)
      let arr = data.split("=");
      let id = ''
      id = arr[arr.length -1]
      console.log("🚀 ~ file: PDACamera.js ~ line 81 ~ QRcode ~ id.length", id.length)
      if (type1 == 'compid' || type1 == 'Stockid' || type == 'trackingid') {
        if (id.length == '16') {
          id = id.substring(3)
          //id = id.replace(/\b(0+)/gi, "")
        }
      }
      
      console.log("🚀 ~ file: PDACamera.js ~ line 68 ~ QRcode ~ id", id)
      this.props.navigation.navigate(page, {
        QRurl: data,
        QRid: id,
        type: type1,
        QRID: (arr1) => {
          this.setState({QRID : arr1})
        }
      })
    })


  };

  takePicture = async () => {
    if (this.camera) {
      const options = { quality: 0.5, base64: true };
      const data = await this.camera.takePictureAsync(options);
      console.log(data.uri);
    }
  };

  render() {
    const { navigation } = this.props;
    const type = navigation.getParam('type') || '';
    return (
      <View style={styles.container}>
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          autoFocus={RNCamera.Constants.AutoFocus.on}/*自动对焦*/
          style={[styles.preview,]}

          type={RNCamera.Constants.Type.back}/*切换前后摄像头 front前back后*/
          flashMode={RNCamera.Constants.FlashMode.off}/*相机闪光模式*/
          onBarCodeRead={(result) => {
            console.log('QR1')
            // if (!this.state.isScan) {
            this.onBarCodeRead(result)
            //}
          }}
          onCameraReady={() => {
            console.log('ready')
          }}
        >
          <View style={styles.rectangleContainer}>
            <View style={styles.rectangle} />
            <Animated.View style={[
              styles.border,
              { transform: [{ translateY: this.state.moveAnim }] }]} />
            <Text style={styles.rectangleText}>将二维码/条码放入框内，即可自动扫描</Text>
          </View>
        </RNCamera>
      </View>
    )
  }


}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    width: '100%',
    height: '100%'
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  rectangleContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  rectangle: {
    height: 200,
    width: 200,
    borderWidth: 1,
    borderColor: '#419FFF',
    backgroundColor: 'transparent'
  },
  rectangleText: {
    flex: 0,
    color: '#fff',
    marginTop: 10
  },
  border: {
    flex: 0,
    width: 200,
    height: 2,
    backgroundColor: '#419FFF',
  }
});

export default withNavigation(QRcode)

