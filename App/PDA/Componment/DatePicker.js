import React from 'react';
import DatePicker from 'react-native-datepicker';
import { Button, Icon } from 'react-native-elements';
import { styles } from './PDAStyles';

export default DatePickerComp = (ServerTime, onDateChange, isAppDateSetting) => {
  return (
    <DatePicker
      customStyles={{
        dateInput: styles.dateInput,
        dateTouchBody: styles.dateTouchBody,
        dateText: isAppDateSetting ? styles.dateText : styles.dateDisabled,
      }}
      iconComponent={<Icon name='caretdown' color={isAppDateSetting ? '#419fff' : "#666"} iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
      }
      showIcon={true}
      mode='datetime'
      date={ServerTime}
      onOpenModal={() => { this.setState({ focusIndex: 4 }) }}
      format="YYYY-MM-DD HH:mm"
      disabled={!isAppDateSetting}
      onDateChange={onDateChange}
    />
  )
}