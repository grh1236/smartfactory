import React from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, ListItem, BottomSheet } from 'react-native-elements';
import Url from '../../Url/Url';
import { RFT } from '../../Url/Pixal';
import { styles } from './PDAStyles';

const { height, width } = Dimensions.get('window') //获取宽高

export default class ListItemScan extends React.Component {
  render() {
    const { title, rightTitle, focusStyle, containerStyle, rightTitleStyle } = this.props
    return (
      <View style={[focusStyle, { paddingHorizontal: 10 }]}>
        <ListItem
          {...this.props}
          containerStyle={[containerStyle,{ backgroundColor: 'transparent', paddingVertical: 18 }]}
          title={title}
          titleStyle={{ fontSize: 14 }}
          rightTitle={rightTitle}
          rightTitleProps={{ numberOfLines: 1 }}
          rightTitleStyle={[ rightTitleStyle,{ width: width / 1.5, textAlign: 'right', fontSize: 14 }]}
          bottomDivider
        />
      </View>
    )
  }
}

