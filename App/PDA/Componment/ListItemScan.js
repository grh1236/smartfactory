/*
 * @Author: error: error: git config user.name & please set dead value or install git && error: git config user.email & please set dead value or install git & please set dead value or install git
 * @Date: 2021-07-06 17:32:54
 * @LastEditors: error: error: git config user.name & please set dead value or install git && error: git config user.email & please set dead value or install git & please set dead value or install git
 * @LastEditTime: 2023-06-05 15:35:10
 * @FilePath: /SmartFactory/App/PDA/Componment/ListItemScan.js
 * @Description: 
 * 
 * Copyright (c) 2023 by ${git_name_email}, All Rights Reserved. 
 */
import React from 'react';
import { View, ScrollView, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, ListItem, BottomSheet } from 'react-native-elements';
import Url from '../../Url/Url';
import { deviceWidth, RFT } from '../../Url/Pixal';
import { styles } from '../Componment/PDAStyles';

const { height, width } = Dimensions.get('window') //获取宽高

export default class ListItemScan extends React.Component {
  render() {
    const { title, rightTitle, focusStyle, containerStyle, rightTitleStyle, isButton, monitorSetup } = this.props
      return (
          <ListItem
            {...this.props}
            ref = {ref => this.list = ref}
            containerStyle={[isButton ? { paddingVertical: 11 } : { paddingVertical: 18 }, { backgroundColor: 'transparent' }]}
            title={title}
            style={[focusStyle, { paddingHorizontal: 10 }]}
            titleStyle={{ fontSize: 14, width: deviceWidth * 0.3 }}
            titleProps={{ numberOfLines: 1 }}
            rightTitle={rightTitle}
            rightTitleProps={{ numberOfLines: 1 }}
            rightTitleStyle={[rightTitleStyle, { width: width * 0.5, textAlign: 'right', fontSize: 14 }]}
            bottomDivider
            underlayColor={"lightgray"}
          />
      )
  }
}

