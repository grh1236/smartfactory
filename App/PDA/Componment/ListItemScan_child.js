import React from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, ListItem, BottomSheet } from 'react-native-elements';
import Url from '../../Url/Url';
import { RFT } from '../../Url/Pixal';
import { styles } from './PDAStyles';

const { height, width } = Dimensions.get('window') //获取宽高

export default class ListItemScan_child extends React.Component {
  render() {
    return (
      <View>
        <ListItem
          {...this.props}
          containerStyle={styles.list_container_style}
          titleStyle={styles.title_child}
          rightTitleProps={{ numberOfLines: 1 }}
          rightTitleStyle={styles.rightTitle_child}
        />
      </View>
    )
  }
}

