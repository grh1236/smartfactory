import React from 'react';
import { View, StyleSheet } from 'react-native';
import { deviceHeight, deviceWidth, RFT } from '../../Url/Pixal';

let width = deviceWidth

export const styles = StyleSheet.create({
  loading: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  mainView: {
    marginHorizontal: width * 0.015,
    //minHeight: deviceHeight * 2
  },
  title: {
    fontSize: 13,
  },
  rightTitle: {
    fontSize: 13,
    textAlign: 'right',
    lineHeight: 14,
    flexWrap: 'wrap',
    width: deviceWidth / 2
  },
  //list_container_style: { height: 14 },
  list_container_style: { marginVertical: 0, paddingVertical: 6, backgroundColor: 'transparent' },
  input_container: {
    width: deviceWidth / 2.8,
    height: 14,
    marginTop: -8,
  },
  input_: {
    fontSize: 14,
    textAlign: 'right'
  },
  inputContainerStyle: { backgroundColor: 'transparent', borderColor: 'transparent' },
  text_beforeinput: { width: deviceWidth / 5, textAlign: 'center', fontSize: RFT * 4 },
  quality_input_container: {
    width: width / 5,
    height: 18,
    marginTop: -25
  },
  quality_input_: {
    fontSize: 15
  },
  ProblemTitle: {
    fontSize: 14,
    width: width * 0.25,
    marginLeft: width * 0.1,
    textAlignVertical: 'center',
    //textAlign: 'center',
    borderRightColor: 'gray',
    borderRightWidth: 0.3
  },
  rightProblemTitle: {
    fontSize: 14,
    width: width * 0.4,
    textAlignVertical: 'center',
    textAlign: 'center'
  },
  problemList_container_style: {
    flexDirection: 'row',
    height: 30,
    justifyContent: 'center',
    borderLeftColor: 'gray',
    borderLeftWidth: 0.3,
    borderRightColor: 'gray',
    borderRightWidth: 0.3,
    borderTopColor: 'gray',
    borderTopWidth: 0.3
  }
})
