import React from 'react';
import { PanResponder, View, ScrollView, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert, TouchableHighlight } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, ListItem, BottomSheet } from 'react-native-elements';
import Url from '../../Url/Url';
import { RFT } from '../../Url/Pixal';
import ActionButton from 'react-native-action-button';


const { height, width } = Dimensions.get('window') //获取宽高
let leftInit = 0
let topInit = 0

let _previousLeft = 0;
let _previousTop = 0;

let lastLeft = 0;
let lastTop = 0;
let CIRCLE_SIZE = 60;

export default class PDASuspension extends React.Component {
    constructor() {
        super();

        this.state = {
            init: false,
            refresh: true,
            _previousLeft: 0,
            _previousTop: 0,
            lastLeft: 0,
            lastTop: 0,
            selectedIndex: 0,
            style: {
                backgroundColor: '#FDBF3A',
                left: 0,
                top: 0
            }
        }
        this.onStartShouldSetPanResponder = this.onStartShouldSetPanResponder.bind(this);
        this.onMoveShouldSetPanResponder = this.onMoveShouldSetPanResponder.bind(this);
        this.onPanResponderGrant = this.onPanResponderGrant.bind(this);
        this.onPanResponderMove = this.onPanResponderMove.bind(this);
        this.onPanResponderRelease = this.onPanResponderRelease.bind(this);

        this._panResponder = PanResponder.create({
            //用户开始触摸屏幕的时候，是否愿意成为响应者；默认返回false，无法响应，
            // 当返回true的时候则可以进行之后的事件传递。
            onStartShouldSetPanResponder: this.onStartShouldSetPanResponder,

            onStartShouldSetPanResponderCapture: this.onStartShouldSetPanResponderCapture,


            //在每一个触摸点开始移动的时候，再询问一次是否响应触摸交互；
            onMoveShouldSetPanResponder: this.onMoveShouldSetPanResponder,

            onMoveShouldSetPanResponderCapture: this.onMoveShouldSetPanResponder,

            //开始手势操作，也可以说按下去。给用户一些视觉反馈，让他们知道发生了什么事情！（如：可以修改颜色）
            onPanResponderGrant: this.onPanResponderGrant,

            //最近一次的移动距离.如:(获取x轴y轴方向的移动距离 gestureState.dx,gestureState.dy)
            onPanResponderMove: this.onPanResponderMove,

            //用户放开了所有的触摸点，且此时视图已经成为了响应者。
            onPanResponderRelease: this.onPanResponderRelease,

            //另一个组件已经成为了新的响应者，所以当前手势将被取消。
            onPanResponderTerminate: this.onPanResponderEnd,
        });
    }

    //用户开始触摸屏幕的时候，是否愿意成为响应者；
    onStartShouldSetPanResponder(evt, gestureState) {
        return true;
    }

    onStartShouldSetPanResponderCapture(evt, gestureState) {
        return !(gestureState.dx <= 1000 || gestureState.dy <= 1000)
        return true;
    }

    //在每一个触摸点开始移动的时候，再询问一次是否响应触摸交互；
    onMoveShouldSetPanResponder(evt, gestureState) {
        return !(gestureState.dx === 0 || gestureState.dy === 0)
    }

    onMoveShouldSetPanResponderCapture(evt, gestureState) {
        return !(gestureState.dx === 0 || gestureState.dy === 0)
    }

    // 开始手势操作。给用户一些视觉反馈，让他们知道发生了什么事情！
    onPanResponderGrant(evt, gestureState) {

        this.setState({
            style: {
                backgroundColor: 'red',
                left: _previousLeft,   //_previousLeft和_previousTop是两个变量，用来记录小球移动坐标
                top: _previousTop,
            }
        });
    }

    // 最近一次的移动距离为gestureState.move{X,Y}
    onPanResponderMove(evt, gestureState) {
        _previousLeft = lastLeft + gestureState.dx;
        _previousTop = lastTop + gestureState.dy;
        this.setState({
            _previousLeft: _previousLeft,
            _previousTop: _previousTop
        })

        //主要是限制小球拖拽移动的时候不许出屏幕外部
        if (_previousLeft <= 0) {
            //_previousLeft = 0;
            _previousLeft = leftInit;
            _previousTop = topInit;
        }
        if (_previousTop <= 0) {
            //_previousTop = 0;
            _previousLeft = leftInit;
            _previousTop = topInit;
        }

        if (_previousLeft >= Dimensions.get('window').width - CIRCLE_SIZE) {
            //_previousLeft = Dimensions.get('window').width - CIRCLE_SIZE;
            _previousLeft = leftInit;
            _previousTop = topInit;
        }
        if (_previousTop >= Dimensions.get('window').height - CIRCLE_SIZE - 120) {
            //_previousTop = Dimensions.get('window').height - CIRCLE_SIZE;
            _previousLeft = leftInit;
            _previousTop = topInit;
        }

        //实时更新
        this.setState({
            _previousLeft: _previousLeft,
            _previousTop: _previousTop,
            style: {
                backgroundColor: 'red',
                left: _previousLeft,
                top: _previousTop,
            }
        });
    }

    // 用户放开了所有的触摸点，且此时视图已经成为了响应者。
    // 一般来说这意味着一个手势操作已经成功完成。
    onPanResponderRelease(evt, gestureState) {
        lastLeft = _previousLeft;
        lastTop = _previousTop;
        this.setState({
            lastLeft: lastLeft,
            lastTop: lastTop
        })

        this.changePosition();
    }

    //根据位置做出相应处理
    changePosition() {
        this.setState({
            style: {
                left: _previousLeft,
                top: _previousTop,
            }
        });

    }

    componentDidMount() {
        if (typeof this.props.dateFocusButton != 'undefined') {
            this.setState({
                selectedIndex: this.props.dateFocusButton
            })
        }
    }

    render() {
        const { circleSize, offsetX, offsetY, iconName, title, onPress } = this.props
        CIRCLE_SIZE = circleSize != null ? circleSize : CIRCLE_SIZE
        leftInit = offsetX != null ? width - 85 - offsetX : width - 85
        topInit = offsetY != null ? height - 185 - offsetY : height - 185

        if (!this.state.init) {
            _previousLeft = leftInit
            _previousTop = topInit
            lastLeft = leftInit
            lastTop = topInit
            this.setState({
                init: true,
                _previousLeft: _previousLeft,
                _previousTop: _previousTop,
                lastLeft: lastLeft,
                lastTop: lastTop,
                style: {
                    left: leftInit,
                    top: topInit
                }
            })
        } else {
            //console.log('......' + _previousLeft + '......' + _previousTop + '......' + lastLeft + '......' + lastTop);
            _previousLeft = this.state._previousLeft
            _previousTop = this.state._previousTop
            lastLeft = this.state.lastLeft
            lastTop = this.state.lastTop
            //console.log('xxxxxx' + _previousLeft + 'xxxxxx' + _previousTop + 'xxxxxx' + lastLeft + 'xxxxxx' + lastTop);
        }


        let dayList = ['今日', '昨日', '本周', '本月']

        if (typeof this.props.isChildPage != 'undefined') {
            if (this.props.isChildPage == true) {
                if (Url.isNewProductionCard) {
                    dayList = ['今日', '昨日', '本周', '本月', '自定义']
                }
            }
        }
        return (
            <View {...this._panResponder.panHandlers}
                style={[this.state.style, { position: 'absolute', width: 100, backgroundColor: 'transparent', alignContent: 'center' }]}>
                <View style={{
                    width: RFT * 15,
                    height: RFT * 15,
                    left: 25,
                    borderRadius: 90,
                    backgroundColor: "#419FFF"
                }} >
                    <TouchableOpacity onPress={() => {this.props.onPress()}}>
                        <View style={styles.actionButtonView}>
                            <Icon type='material-community' name='home' color='#ffffff' />
                            <Text style={styles.actionButtonText}>返回</Text>
                        </View>
                    </TouchableOpacity>

                </View>
            </View >



        )
    }

}
const styles = StyleSheet.create({
    cycle: {
        justifyContent: 'center',
        width: 60,
        height: 200,
        borderRadius: CIRCLE_SIZE / 2,
        backgroundColor: '#FDBF3A',
        position: 'absolute'
    },
    actionButtonView: {
        top: 5
    },
    actionButtonText: {
        color: 'white',
        fontSize: 14,
        textAlign: 'center'
    }
})
