import React from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, ListItem, BottomSheet, CheckBox } from 'react-native-elements';
import Url from '../../Url/Url';
import { RFT } from '../../Url/Pixal';
import { styles } from '../Componment/PDAStyles';

const { height, width } = Dimensions.get('window') //获取宽高

export default class ListItemScan extends React.Component {
  render() {
    const { title, backgroundColor, color } = this.props
    return (
      <View style={{
        paddingVertical: 18,
        backgroundColor: backgroundColor,
        alignItems: 'center',
        borderBottomColor: '#DDDDDD',
        borderBottomWidth: 0.6,
      }}>
        
        <Text style={{color: color, textAlign: 'center', fontSize: 14, textAlignVertical: 'center' }}>{title}</Text>
      </View>
    )
  }
}

