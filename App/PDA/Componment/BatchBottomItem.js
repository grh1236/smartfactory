/*
 * @Author: error: error: git config user.name & please set dead value or install git && error: git config user.email & please set dead value or install git & please set dead value or install git
 * @Date: 2022-03-07 14:34:55
 * @LastEditors: error: error: git config user.name & please set dead value or install git && error: git config user.email & please set dead value or install git & please set dead value or install git
 * @LastEditTime: 2023-06-06 09:08:13
 * @FilePath: /SmartFactory/App/PDA/Componment/BatchBottomItem.js
 * @Description: 
 * 
 * Copyright (c) 2023 by ${git_name_email}, All Rights Reserved. 
 */
import React from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, ListItem, BottomSheet } from 'react-native-elements';
import Url from '../../Url/Url';
import { RFT } from '../../Url/Pixal';
import { styles } from './PDAStyles';

const { height, width } = Dimensions.get('window') //获取宽高

export default class BatchListItem extends React.Component {

  render() {
    const { dataItem, backgroundColor, color } = this.props
    return (
      <View style={{
        paddingTop: 12,
        paddingHorizontal: 25,
        backgroundColor: backgroundColor,
        borderBottomColor: '#DDDDDD',
        borderBottomWidth: 1,
      }}>
        <View style={stylesItem.rowView}>
          <Text style={stylesItem.Title} numberOfLines={1}>{dataItem.Title}</Text>
          <Text style={stylesItem.textright}>{dataItem.rightTitle}</Text>
        </View>
        <View style={stylesItem.rowView}>
          <Text style={stylesItem.subTitle}>{dataItem.subTitle}</Text>
          <Text style={stylesItem.textright}>{dataItem.subRightTitle}</Text>
        </View>

        {/* <Text style={{color: color, textAlign: 'center', fontSize: 14, textAlignVertical: 'center' }}>{title}</Text> */}
      </View>
    )
  }
}

const stylesItem = StyleSheet.create({
  rowView: {
    flex: 1, 
    //backgroundColor: 'red', 
    flexDirection: 'row', 
    justifyContent: "space-between",
    paddingBottom: 12
  },
  Title: {
    fontSize: 16,
    /* marginBottom: 5,
    marginTop: 5, */
    //flex: 1
  },
  subTitle: {
    color: 'gray'
  },
  textright: {
    //flex: 2,
    textAlign: 'right',
    color: 'gray',
    //backgroundColor: 'blue'
  },

})


