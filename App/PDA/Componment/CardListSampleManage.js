import React from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, ListItem, BottomSheet, CheckBox } from 'react-native-elements';
import Url from '../../Url/Url';
import { ToDay, YesterDay, BeforeYesterDay } from '../../CommonComponents/Data';
import { deviceWidth, RFT } from '../../Url/Pixal';
import { ScrollView } from 'react-native-gesture-handler';

const { height, width } = Dimensions.get('window') //获取宽高


export default class CardListSampleManage extends React.Component {
  constructor() {
    super();
    this.state = {

    }
  }

  render() {
    const { compData } = this.props
    let result =
          console.log("🚀 ~ file: CardListSampleManage.js ~ line 23 ~ CardList ~ render ~ compData", compData)
      console.log("🚀 ~ file: CardListSampleManage.js ~ line 24 ~ CardList ~ render ~ compData.result", compData.result)
    if (compData.result instanceof Array) {
      result = compData.result[0]
    } else {
      result = compData.result
      }
     
    return (
        result == null ? <View /> : <View>
        <Card containerStyle={styles.card_containerStyle}>
          <ListItem
            containerStyle={styles.list_container_style}
            title='项目名称'
            rightTitle={result.projectName || result.ProjectName}
            titleStyle={styles.title}
            rightTitleProps = {{numberOfLines: 1}}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='构件类型'
            rightTitle={result.compTypeName || result.CompTypeName}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='设计型号'
            rightTitle={result.designType || result.DesignType}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='楼号'
            rightTitle={result.floorNoName || result.FloorNoName}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='楼层'
            rightTitle={result.floorName || result.FloorName}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          
          <ListItem
            containerStyle={styles.list_container_style}
            title='构件图纸'
            rightTitle={"查看图纸"}
            titleStyle={styles.title}
            onPress = {() => {
              try {
                this.props.func(true)
              } catch(err) {
                  console.log("🚀 ~ file: CardListSampleManage.js ~ line 105 ~ CardList ~ render ~ err", err)
              }
              
            }}
            underlayColor={'lightgray'}
            rightTitleStyle={[styles.rightTitle, {color: "#419fff"}]}
          />
            </Card>
            <Text />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  card_containerStyle: {
    borderRadius: 10,
    shadowOpacity: 0,
    backgroundColor: '#f8f8f8',
    borderWidth: 0,
    paddingVertical: 13,
    elevation: 0
  },
  title: {
    fontSize: 13,
    color: '#999'
  },
  rightTitle: {
    fontSize: 13,
    textAlign: 'right',
    lineHeight: 14,
    flexWrap: 'wrap',
    width: width / 2,
    color: '#333'
  },
  list_container_style: { marginVertical: 0, paddingVertical: 9, backgroundColor: 'transparent' },

})

if (deviceWidth <= 330) {
  styles.card_containerStyle = {
    borderRadius: 10,
    shadowOpacity: 0,
    backgroundColor: '#f8f8f8',
    borderWidth: 0,
    paddingVertical: 10,
    paddingHorizontal: 5,
    elevation: 0
  }
  styles.list_container_style = {
    marginVertical: 0,
    paddingVertical: 5,
    backgroundColor: 'transparent'
  }
  styles.rightTitle = {
    fontSize: 13,
    textAlign: 'right',
    lineHeight: 16,
    width: deviceWidth * 0.4,
    color: '#333'
  }
}