import React from 'react';
import { Button, Icon } from 'react-native-elements';


export default class formButton extends React.Component {
  render() {
    const { title, backgroundColor } = this.props
    return (
      <Button
        {...this.props}
        title={title}
        titleStyle={{ color: "white", fontSize: 17 }}
        type='solid'
        style={{ marginVertical: 10 }}
        containerStyle={{ marginVertical: 10, marginHorizontal: 22 }}
        buttonStyle={{ backgroundColor: backgroundColor, borderRadius: 10, paddingVertical: 16 }}
      ></Button>
    )
  }
}