/*
 * @Author: error: error: git config user.name & please set dead value or install git && error: git config user.email & please set dead value or install git & please set dead value or install git
 * @Date: 2023-07-10 13:53:45
 * @LastEditors: error: error: git config user.name & please set dead value or install git && error: git config user.email & please set dead value or install git & please set dead value or install git
 * @LastEditTime: 2023-07-12 08:56:15
 * @FilePath: /SmartFactory/App/PDA/Componment/OverlayImgClass.js
 * @Description: 
 * 
 * Copyright (c) 2023 by ${git_name_email}, All Rights Reserved. 
 */
import React, { Component } from 'react';
import { ScrollView, Text, TouchableHighlight, View, PanResponder, LayoutChangeEvent, TouchableOpacity } from 'react-native';
import { Header, Icon, Image, Overlay } from 'react-native-elements';
import { deviceHeight, deviceWidth } from '../../Url/Pixal';
import Animated from 'react-native-reanimated';
// import {  ImageZoomProps, ImageZoomState } from './image-zoom.type';

export default class OverlayImgClass extends Component {
  constructor(props) {
    super(props);
    // this.state = {
    // };
    //state = new ImageZoomState();
    this.scale = 1;
    this.animatedScale = new Animated.Value(1);
    this.zoomLastDistance = null;
    this.zoomCurrentDistance = 0;

    this.horizontalWholeCounter = 0;
    this.verticalWholeCounter = 0;

    this.doubleClickX = 0;
    this.doubleClickY = 0;

    this.isDoubleClick = false;

    // 上次/当前/动画 x 位移
    this.lastPositionX = null;
    this.positionX = 0;
    this.animatedPositionX = new Animated.Value(0);

    // 上次/当前/动画 y 位移
    this.lastPositionY = null;
    this.positionY = 0;
    this.animatedPositionY = new Animated.Value(0);
    this.imagePanResponder = PanResponder.create({
      // 要求成为响应者：
      onStartShouldSetPanResponder: () => true,
      onPanResponderTerminationRequest: () => false,
      onMoveShouldSetPanResponder: () => true,

      onPanResponderGrant: (evt) => {
        console.log("🚀 ~ file: OverlayImgClass.js:46 ~ OverlayImgClass ~ constructor ~ evt:", evt)
        // 开始手势操作
        this.lastPositionX = null;
        this.lastPositionY = null;
        this.zoomLastDistance = null;
        this.horizontalWholeCounter = 0;
        this.verticalWholeCounter = 0;
        this.lastTouchStartTime = new Date().getTime();
        this.isDoubleClick = false;
        this.isLongPress = false;
        this.isHorizontalWrap = false;
        // 任何手势开始，都清空单击计时器
        if (this.singleClickTimeout) {
          clearTimeout(this.singleClickTimeout);
        }

        if (evt.nativeEvent.changedTouches.length > 1) {
          const centerX = (evt.nativeEvent.changedTouches[0].pageX + evt.nativeEvent.changedTouches[1].pageX) / 2;
          console.log("🚀 ~ file: OverlayImgClass.js:64 ~ OverlayImgClass ~ constructor ~ centerX:", centerX)
          this.centerDiffX = centerX - this.props.cropWidth / 2;

          const centerY = (evt.nativeEvent.changedTouches[0].pageY + evt.nativeEvent.changedTouches[1].pageY) / 2;
          console.log("🚀 ~ file: OverlayImgClass.js:68 ~ OverlayImgClass ~ constructor ~ centerY:", centerY)
          this.centerDiffY = centerY - this.props.cropHeight / 2;
        }
        // 计算长按
        if (this.longPressTimeout) {
          clearTimeout(this.longPressTimeout);
        }
        const { locationX, locationY, pageX, pageY } = evt.nativeEvent;
        this.longPressTimeout = setTimeout(() => {
          this.isLongPress = true;
          if (this.props.onLongPress) {
            this.props.onLongPress({ locationX, locationY, pageX, pageY });
          }
        }, this.props.longPressTime);
        if (evt.nativeEvent.changedTouches.length <= 1) {
          // 一个手指的情况
          if (new Date().getTime() - this.lastClickTime < (this.props.doubleClickInterval || 0)) {
            // 认为触发了双击
            this.lastClickTime = 0;

            // 因为可能触发放大，因此记录双击时的坐标位置
            this.doubleClickX = evt.nativeEvent.changedTouches[0].pageX;
            this.doubleClickY = evt.nativeEvent.changedTouches[0].pageY;

            if (this.props.onDoubleClick) {
              this.props.onDoubleClick({
                locationX: evt.nativeEvent.changedTouches[0].locationX,
                locationY: evt.nativeEvent.changedTouches[0].locationY,
                pageX: this.doubleClickX,
                pageY: this.doubleClickY,
              });
            }

            // 取消长按
            clearTimeout(this.longPressTimeout);

            // 缩放
            this.isDoubleClick = true;
            //能否双指放大
            //if (this.props.enableDoubleClickZoom) {
            if (true) {
              if (this.scale > 1 || this.scale < 1) {
                // 回归原位
                this.scale = 1;

                this.positionX = 0;
                this.positionY = 0;
              } else {
                // 开始在位移地点缩放
                // 记录之前缩放比例
                // 此时 this.scale 一定为 1
                const beforeScale = this.scale;

                // 开始缩放
                this.scale = 2;

                // 缩放 diff
                const diffScale = this.scale - beforeScale;
                // 找到两手中心点距离页面中心的位移
                // 移动位置
                this.positionX = ((this.props.cropWidth / 2 - this.doubleClickX) * diffScale) / this.scale;

                this.positionY = ((this.props.cropHeight / 2 - this.doubleClickY) * diffScale) / this.scale;
              }

              //this.imageDidMove('centerOn');

              Animated.parallel([
                Animated.timing(this.animatedScale, {
                  toValue: this.scale,
                  duration: 100,
                  useNativeDriver: !!this.props.useNativeDriver,
                }),
                Animated.timing(this.animatedPositionX, {
                  toValue: this.positionX,
                  duration: 100,
                  useNativeDriver: !!this.props.useNativeDriver,
                }),
                Animated.timing(this.animatedPositionY, {
                  toValue: this.positionY,
                  duration: 100,
                  useNativeDriver: !!this.props.useNativeDriver,
                }),
              ]).start();
            }
          } else {
            this.lastClickTime = new Date().getTime();
          }
        }
      },
      onPanResponderMove: (evt, gestureState) => {
        console.log("🚀 ~ file: OverlayImgClass.js:156 ~ OverlayImgClass ~ constructor ~ gestureState:", gestureState)
        console.log("🚀 ~ file: OverlayImgClass.js:1561 ~ evt.nativeEvent.changedTouches.length:", evt.nativeEvent.changedTouches.length)

        if (this.isDoubleClick) {
          // 有时双击会被当做位移，这里屏蔽掉
          return;
        }
        if (evt.nativeEvent.changedTouches.length <= 1) {
          // x 位移
          let diffX = gestureState.dx - (this.lastPositionX || 0);
          console.log("🚀 ~ file: OverlayImgClass.js:172 ~ OverlayImgClass ~ constructor ~ diffX:", diffX)
          if (this.lastPositionX === null) {
            diffX = 0;
          }
          // y 位移
          let diffY = gestureState.dy - (this.lastPositionY || 0);
          console.log("🚀 ~ file: OverlayImgClass.js:172 ~ OverlayImgClass ~ constructor ~ diffY:", diffY)
          if (this.lastPositionY === null) {
            diffY = 0;
          }

          // 保留这一次位移作为下次的上一次位移
          this.lastPositionX = gestureState.dx;
          this.lastPositionY = gestureState.dy;

          this.horizontalWholeCounter += diffX;
          this.verticalWholeCounter += diffY;

          if (Math.abs(this.horizontalWholeCounter) > 5 || Math.abs(this.verticalWholeCounter) > 5) {
            // 如果位移超出手指范围，取消长按监听
            clearTimeout(this.longPressTimeout);
          }
        } else {
          // 多个手指的情况
          // 取消长按状态
          if (this.longPressTimeout) {
            clearTimeout(this.longPressTimeout);
          }
          //多手指是否能缩放
          //if (this.props.pinchToZoom) {
          if (true) {
            // 找最小的 x 和最大的 x
            let minX;
            let maxX;
            if (evt.nativeEvent.changedTouches[0].locationX > evt.nativeEvent.changedTouches[1].locationX) {
              minX = evt.nativeEvent.changedTouches[1].pageX;
              maxX = evt.nativeEvent.changedTouches[0].pageX;
            } else {
              minX = evt.nativeEvent.changedTouches[0].pageX;
              maxX = evt.nativeEvent.changedTouches[1].pageX;
            }

            let minY;
            let maxY;
            if (evt.nativeEvent.changedTouches[0].locationY > evt.nativeEvent.changedTouches[1].locationY) {
              minY = evt.nativeEvent.changedTouches[1].pageY;
              maxY = evt.nativeEvent.changedTouches[0].pageY;
            } else {
              minY = evt.nativeEvent.changedTouches[0].pageY;
              maxY = evt.nativeEvent.changedTouches[1].pageY;
            }

            const widthDistance = maxX - minX;
            console.log("🚀 ~ file: OverlayImgClass.js:229 ~ widthDistance:", widthDistance)
            const heightDistance = maxY - minY;
            console.log("🚀 ~ file: OverlayImgClass.js:231 ~ heightDistance:", heightDistance)
            const diagonalDistance = Math.sqrt(widthDistance * widthDistance + heightDistance * heightDistance);
            this.zoomCurrentDistance = Number(diagonalDistance.toFixed(1));

            if (this.zoomLastDistance !== null) {
              const distanceDiff = (this.zoomCurrentDistance - this.zoomLastDistance) / 200;
              let zoom = this.scale + distanceDiff;

              if (zoom < (this.props.minScale || 0)) {
                zoom = this.props.minScale || 0;
              }
              if (zoom > (this.props.maxScale || 0)) {
                zoom = this.props.maxScale || 0;
              }

              // 记录之前缩放比例
              const beforeScale = this.scale;

              // 开始缩放
              this.scale = zoom;
              this.animatedScale.setValue(this.scale);

              // 图片要慢慢往两个手指的中心点移动
              // 缩放 diff
              const diffScale = this.scale - beforeScale;
              // 找到两手中心点距离页面中心的位移
              // 移动位置
              this.positionX -= (this.centerDiffX * diffScale) / this.scale;
              this.positionY -= (this.centerDiffY * diffScale) / this.scale;
              this.animatedPositionX.setValue(this.positionX);
              this.animatedPositionY.setValue(this.positionY);
            }
            this.zoomLastDistance = this.zoomCurrentDistance;
          }
        }
      },
      onPanResponderRelease: (evt, gestureState) => {
        // 取消长按
        if (this.longPressTimeout) {
          clearTimeout(this.longPressTimeout);
        }

        // 双击结束，结束尾判断
        if (this.isDoubleClick) {
          return;
        }

        // 长按结束，结束尾判断
        if (this.isLongPress) {
          return;
        }

        // 如果是单个手指、距离上次按住大于预设秒、滑动距离小于预设值, 则可能是单击（如果后续双击间隔内没有开始手势）
        // const stayTime = new Date().getTime() - this.lastTouchStartTime!
        const moveDistance = Math.sqrt(gestureState.dx * gestureState.dx + gestureState.dy * gestureState.dy);
        const { locationX, locationY, pageX, pageY } = evt.nativeEvent;

        if (evt.nativeEvent.changedTouches.length === 1 && moveDistance < (this.props.clickDistance || 0)) {
          this.singleClickTimeout = setTimeout(() => {
            if (this.props.onClick) {
              this.props.onClick({ locationX, locationY, pageX, pageY });
            }
          }, this.props.doubleClickInterval);
        } else {
          // 多手势结束，或者滑动结束
          if (this.props.responderRelease) {
            this.props.responderRelease(gestureState.vx, this.scale);
          }
          this.panResponderReleaseResolve();
        }
      },
      onPanResponderTerminate: () => {
        //
      },
    })
  }

  resetScale = () => {
    this.positionX = 0;
    this.positionY = 0;
    this.scale = 1;
    this.animatedScale.setValue(1);
  };

  panResponderReleaseResolve = () => {
    // 判断是否是 swipeDown
    if (this.props.enableSwipeDown && this.props.swipeDownThreshold) {
      if (this.swipeDownOffset > this.props.swipeDownThreshold) {
        if (this.props.onSwipeDown) {
        }
        // Stop reset.
        return;
      }
    }

    if (this.props.imageWidth * this.scale <= this.props.cropWidth) {
      // 如果图片宽度小于盒子宽度，横向位置重置
      this.positionX = 0;
      Animated.timing(this.animatedPositionX, {
        toValue: this.positionX,
        duration: 100,
        useNativeDriver: !!this.props.useNativeDriver,
      }).start();
    }

  }


  render() {
    const { onRequestClose, uri, isVisible, onPress } = this.props;
    const animateConf = {
      transform: [
        {
          scale: this.animatedScale,
        },
        {
          translateX: this.animatedPositionX,
        },
        {
          translateY: this.animatedPositionY,
        },
      ],
    };
    return (
      <View>
        <Overlay
          fullScreen={true}
          animationType='fade'
          isVisible={isVisible}
          onRequestClose={onRequestClose}>
          <Header
            containerStyle={{ height: 45, paddingTop: 0, top: -5 }}
            centerComponent={{ text: '图片缩放', style: { color: 'gray', fontSize: 20 } }}
            rightComponent={
              <View >
                <TouchableHighlight
                  onPress={onRequestClose} >
                  <Icon
                    name='arrow-back'
                    color='#419FFF'
                    onPress={onRequestClose} />
                </TouchableHighlight>
              </View>}
            backgroundColor='white' />
          <Animated.View
            // maximumZoomScale={3}
            // minimumZoomScale={0.8}
            // centerContent={true}
            style={animateConf}
            {...this.imagePanResponder.panHandlers} >
            <Image
              source={{ uri: uri }}
              resizeMethod='resize'
              resizeMode='contain'
              style={{ height: deviceHeight * 0.8 }}
            />
          </Animated.View>

        </Overlay>
      </View>
    );
  }
}
