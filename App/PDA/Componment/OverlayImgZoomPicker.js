/*
 * @Author: 高睿豪 1375308739@qq.com
 * @Date: 2023-04-28 08:49:15
 * @LastEditors: 高睿豪 1375308739@qq.com
 * @LastEditTime: 2023-08-04 08:51:18
 * @FilePath: /SmartFactory/App/PDA/Componment/OverlayImgZoomPicker.js
 * @Description: 
 * 
 * Copyright (c) 2023 by ${git_name_email}, All Rights Reserved. 
 */
import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Modal,
  ActivityIndicator,
  Dimensions
} from 'react-native';
import { Header, Icon, Image, Overlay } from 'react-native-elements';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { deviceHeight, deviceWidth } from '../../Url/Pixal';
import ImageViewer from 'react-native-image-zoom-viewer';

const { width, height } = Dimensions.get('window');
const Width = () => { return width };
const Height = () => { return height };

export default class OverlayImgZoomPicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowImage: this.props.isShowImage,
      loadingAnimating: true,    
      zoomImages: this.props.zoomImages,
      currShowImgIndex: this.props.currShowImgIndex
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.isShowImage != nextProps.isShowImage) {
      this.setState({
        isShowImage: nextProps.isShowImage
      })
    }
    if (this.state.zoomImages != nextProps.zoomImages) {
      this.setState({
        zoomImages: nextProps.zoomImages
      })
    }
    if (this.state.currShowImgIndex != nextProps.currShowImgIndex) {
      this.setState({
        currShowImgIndex: nextProps.currShowImgIndex
      })
    }
  }

  handleZoomPicture = (flag) => {
    this.setState({
      isShowImage: flag
    })
    this.props.callBack(flag)
  }

  // 图片加载
  renderImageLoad = () => {
    let { loadingAnimating } = this.state
    return (
      <View style={styles.img_load}>
        <ActivityIndicator animating={loadingAnimating} size={"large"} />
      </View>
    )
  }

  render() {
    let { isShowImage, zoomImages, currShowImgIndex } = this.state
    return (
      <Modal
        visible={isShowImage}
        animationType={"slide"}
        transparent={true}
        onRequestClose={() => {
          this.handleZoomPicture(false)
        }}
      >
        <ImageViewer style={styles.zoom_pic_img}
          enableImageZoom={true}
          saveToLocalByLongPress={false}
          // menuContext={{ "saveToLocal": "保存图片", "cancel": "取消" }}
          // onSave={(url) => { this.savePhoto(url) }}
          // failImageSource={{ 
          //     url: 'https://avatars2.githubusercontent.com/u/7970947?v=3&s=460', // 不能加载本地图片
          //     width: Width(),
          //     height: 300
          // }}
          loadingRender={() => this.renderImageLoad()}
          enableSwipeDown={true}
          imageUrls={zoomImages}
          index={currShowImgIndex}
          onCancel={() => {
            this.handleZoomPicture(false)
          }}
          onClick={() => this.handleZoomPicture(false)}
        />
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  zoom_pic_img: {
    maxWidth: Width()
  },
  img_load: {
    marginBottom: (Height() / 2) - 40
  }
})

const BackIcon = (onPress) => {
  return (
    <TouchableOpacity
      onPress={onPress} >
      <Icon
        name='arrow-back'
        color='#419FFF' />
    </TouchableOpacity>
  )
}

