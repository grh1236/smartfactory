import React from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, CheckBox, Avatar, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, ListItem, BottomSheet } from 'react-native-elements';

const { height, width } = Dimensions.get('window') //获取宽高

export default class CheckBoxchild extends React.Component {
  render() {
    const { title, checked, containerStyle } = this.props
    return (
      <CheckBox
      {...this.props}
      containerStyle={[containerStyle,{ backgroundColor: 'transparent', borderColor: 'transparent', padding: 0}]}
      title= {title}
      icon
      uncheckedIcon={<Image source={require('../img/unchecked.png')} style={{ height: 14, width: 14 }} resizeMode='contain'></Image>}
      checkedIcon={<Image source={require('../img/checked.png')} style={{ height: 14, width: 14 }} resizeMode='contain'></Image>}
      checked={checked}
      size={14}
    />

    )
  }
}

