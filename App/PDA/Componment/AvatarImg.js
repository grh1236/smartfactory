import React from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Avatar, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, ListItem, BottomSheet } from 'react-native-elements';
import Url from '../../Url/Url';
import { RFT } from '../../Url/Pixal';
import { styles } from './PDAStyles';

const { height, width } = Dimensions.get('window') //获取宽高

export default class AvatarImg extends React.Component {
  render() {
      const { item, index, onPress, onLongPress, isRounded } = this.props
      let myRounded = true
      if (isRounded == "false") { myRounded = false }
      return (
          <View style={{ marginLeft: 15 }}>
              {
                  (myRounded) ? <Avatar
                      containerStyle={[{ left: -52 * index, backgroundColor: 'transparent', borderColor: 'white', borderWidth: 2 }]}
                      rounded
                      source={{ uri: item }}
                      size={60}
                      onPress={onPress}
                      onLongPress={onLongPress}
                  /> : <Avatar
                      containerStyle={[{ left: -52 * index, backgroundColor: 'transparent', borderColor: 'white', borderWidth: 2 }]}
                      source={{ uri: item }}
                      size={60}
                      onPress={onPress}
                      onLongPress={onLongPress}
                  />
              }

              <Badge
                  value={<Icon size={16} type='antdesign' name='closecircle' color='lightgray' />}
                  containerStyle={{ position: 'absolute', top: -4, right: -3 + 52 * index, }}
                  badgeStyle={{ backgroundColor: "transparent", borderColor: 'transparent' }}
                  onPress={onLongPress} />
          </View>

      )
  }
}

