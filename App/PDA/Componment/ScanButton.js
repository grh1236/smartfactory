/*
 * @Author: 高睿豪 1375308739@qq.com
 * @Date: 2021-07-06 17:32:54
 * @LastEditors: 高睿豪 1375308739@qq.com
 * @LastEditTime: 2023-04-26 10:00:00
 * @FilePath: /SmartFactory/App/PDA/Componment/ScanButton.js
 * @Description: 
 * 
 * Copyright (c) 2023 by ${git_name_email}, All Rights Reserved. 
 */
import React from 'react';
import { Button, Icon } from 'react-native-elements';

export default class ScanButton extends React.Component {
  render() {
    return (
      <Button
        {...this.props}
        titleStyle={{ fontSize: 14 }}
        title={this.props.title ? this.props.title : '扫码'}
        //iconRight={true}
        //icon={<Icon type='antdesign' name='scan1' color='white' size={14} />}
        type='solid'
        buttonStyle={{ paddingVertical: 6, backgroundColor: '#4D8EF5', marginVertical: 0 }} />
    )
  }
}