import React from 'react';
import { View, TouchableOpacity, TouchableHighlight, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, ListItem, BottomSheet } from 'react-native-elements';
import Url from '../../Url/Url';
import { RFT } from '../../Url/Pixal';
import { styles } from './PDAStyles';

const { height, width } = Dimensions.get('window') //获取宽高

export default class TouchableCustom extends React.Component {
  render() {
    return (
      <View>
        <TouchableHighlight
          underlayColor={'lightgray'}
          {...this.props}
        >

        </TouchableHighlight>
      </View>
    )
  }
}

