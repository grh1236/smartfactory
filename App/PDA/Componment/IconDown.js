import React from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, ListItem, BottomSheet } from 'react-native-elements';
import Url from '../../Url/Url';
import { RFT } from '../../Url/Pixal';
import { styles } from './PDAStyles';

const { height, width } = Dimensions.get('window') //获取宽高

export default class IconDown extends React.Component {
  render() {
    const { text } = this.props
    return (
        <View style={{ flexDirection: "row", marginRight: -5}}>
          <Text numberOfLines={1} style={styles.downsquareTitle}>{text}</Text>
          <Icon name='caretdown' color='#419fff' iconStyle={{ fontSize: 13, marginTop: 1, marginLeft: 8, marginRight: -1 }} type='antdesign' ></Icon>
        </View>
    )
  }
}

