
import React from 'react';
import { View, ScrollView, TouchableOpacity, StyleSheet, Dimensions, Alert, StatusBar, ActivityIndicator } from 'react-native';
import { Text, Input, Card, Image, Icon, Header } from 'react-native-elements';
import Url1 from '../Url/Url';
import { ToDay } from '../CommonComponents/Data'
import { deviceWidth, RFT } from '../Url/Pixal';
import { NavigationEvents } from 'react-navigation';
import ActionButton from 'react-native-action-button';
import PushUtil from '../CommonComponents/PushUtil'
import PDASuspension from './Componment/PDASuspension';

const { height, width } = Dimensions.get('window')

export default class PDACheckView extends React.Component {
  constructor() {
    super();
    this.state = {

    }
  }

  componentDidMount() {
    StatusBar.setBarStyle('light-content')

  }

  setTimer = () => {
    let timer = setTimeout(() => {
      clearTimeout(timer)
      this.setState({
        isLoading: false
      })
    }, 1000)
  }

  cardView = (Source) => {
    if (Url1.PDAauthorgray[1] == '1') {
      Source[4].main = 'HideInspection'
    }
    return (
      <View style={styles.ViewCard}>
        {
          Source.map((u, i) => {
            if (u.i == Url1.PDAauthorgray[i] && u.page != "") {
              return (
                <View style={{ marginVertical: 5 }}>
                  <TouchableOpacity
                    onPress={() => {
                      this.props.navigation.navigate(u.page, {
                        title: u.title,
                        sonTitle: u.sonTitle,
                        main: u.main
                      })
                    }}>
                    <View
                      style={[styles.img, {}]}>
                      <Image
                        source={u.img}
                        containerStyle={styles.img}
                        resizeMethod='resize'
                        resizeMode='contain'
                        style={{
                          height: width / 8,
                          width: width / 8,
                          flex: 1,
                          marginVertical: RFT * 6,
                          marginHorizontal: RFT * 5.4,
                        }}
                      ></Image>
                    </View>
                  </TouchableOpacity>
                  <Text style={styles.text}> {u.text} </Text>
                </View>
              )
            }
          })
        }
      </View>
    )
  }

  render() {
    const Url = Url1
    const CardSource = [
      { i: 0, img: require('./img/steel_cage_storage.png'), page: 'MainCheckPage', text: '钢筋笼入库', title: '钢筋笼入库', sonTitle: '钢筋笼入库修改', main: "SteelCageStorage", },
      { i: 1, img: require('./img/production_start.png'), page: 'MainCheckPage', text: '生产开始', title: '生产开始', sonTitle: '生产开始', main: "ProductionStart" },
      { i: 2, img: require('./img/production_process.png'), page: 'MainCheckPage', text: '生产工序', title: '生产工序', sonTitle: '生产工序', main: "ProductionProcess" },
      { i: 3, img: require('./img/steel_cage_die.png'), page: 'MainCheckPage', text: '钢筋笼入模', title: '钢筋笼入模', sonTitle: '钢筋笼入模', main: "SteelCageDie" },
      { i: 4, img: require('./img/quality_inspection.png'), page: 'MainCheckPage', text: '质量隐检', title: '质量隐检', sonTitle: '质量隐检修改', main: "QualityInspection" },
      { i: 5, img: require('./img/buried_card.png'), page: 'MainCheckPage', text: '埋卡附码', title: '埋卡附码', sonTitle: '埋卡附码修改', main: "BuriedCard" },
      { i: 6, img: require('./img/concrete_pouring.png'), page: 'MainCheckPage', text: '混凝土浇筑', title: '混凝土浇筑', sonTitle: '混凝土浇筑修改', main: "ConcretePouring" },
      { i: 7, img: require('./img/steel_cage_storage.png'), page: 'SteelCageStorage', text: '构件蒸养', title: '构件蒸养', sonTitle: '构件蒸养', url: Url.url + Url.Produce + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 8, img: require('./img/stripping_inspection.png'), page: 'MainCheckPage', text: '脱模待检', title: '脱模待检', sonTitle: '脱模待检修改', main: "StrippingInspection" },
      { i: 9, img: require('./img/quality_product_inspection.png'), page: 'MainCheckPage', text: '质量成品检', title: '质量成品检', sonTitle: '质量成品检修改', main: "QualityProductInspection" },
      { i: 10, img: require('./img/quality_product_inspection_review.png'), page: 'MainCheckPage', text: '成品检复检', title: '成品检复检', sonTitle: '成品检复检修改', main: "QualityProductInspectionReview" },
      { i: 11, img: require('./img/storage.png'), page: 'MainCheckPage', text: '入库管理', title: '入库管理', sonTitle: '入库管理修改', main: "PDAStorage" },
      { i: 12, img: require('./img/loading.png'), page: 'MainCheckPage', text: '装车出库', title: '装车出库', sonTitle: '装车出库修改', main: "PDALoading" },
      { i: 13, img: require('./img/loading.png'), page: 'MainCheckPage', text: '扫码装车', title: '扫码装车', sonTitle: '扫码装车修改', main: "PDASacnLoading" },
      { i: 14, img: require('./img/unload.png'), page: 'MainCheckPage', text: '卸车管理', title: '卸车管理', sonTitle: '卸车管理修改', main: "Unload" },
      { i: 15, img: require('./img/install.png'), page: 'MainCheckPage', text: '构件安装', title: '构件安装', sonTitle: '构件安装修改', main: "PDAInstall" },
      { i: 16, img: require('./img/return_goods.png'), page: 'MainCheckPage', text: '退库管理', title: '退库管理', sonTitle: '退库管理修改', main: "ReturnGoods" },
      { i: 17, img: require('./img/equipment_maintenance.png'), page: 'MainCheckPage', text: '设备保养', title: '设备保养', sonTitle: '设备保养修改', main: "EquipmentMaintenance" },
      { i: 18, img: require('./img/equipment_inspection.png'), page: 'MainCheckPage', text: '设备点检', title: '设备点检', sonTitle: '设备点检修改', main: "EquipmentInspection" },
      { i: 19, img: require('./img/wantconcrete.png'), page: 'MainCheckPage', text: '叫料单', title: '叫料单', sonTitle: '叫料单修改', main: "Wantconcrete" },
      { i: 20, img: require('./img/concrete_pouring.png'), page: 'MainCheckPage', text: '混凝土浇筑(批)', title: '混凝土浇筑(批)', sonTitle: '混凝土浇筑(批)', url: Url.url + Url.Quality + Url.Fid + 'GetQualityHidcheckProject', main: "ConcretePouring" },
      //{ i: 20, img: require('./img/concrete_pouring.png'), page: 'MainCheckPage', text: '混凝土浇筑(批)', title: '混凝土浇筑(批)', sonTitle: '混凝土浇筑(批)', url: Url.url + Url.Quality + Url.Fid + 'GetQualityHidcheckProject', main: "ConcretePouring" },
      { i: 21, img: require('./img/inventory_check.png'), page: 'MainCheckPage', text: '构件盘点', title: '堆场盘点', sonTitle: '堆场盘点修改', main: "InventoryCheck" },
      { i: 22, img: require('./img/quality_inspection.png'), page: 'MainCheckPage', text: '综合隐检(批)', title: '综合隐检(批)', sonTitle: '综合隐检(批)', url: Url.url + Url.Quality + Url.Fid + 'GetQualityHidcheckProject', main: "HideInspection" },
      { i: 23, img: require('./img/stripping_inspection.png'), page: 'MainCheckPage', text: '脱模待检(批)', title: '脱模待检(批)', sonTitle: '脱模待检(批)', url: Url.url + Url.Quality + Url.Fid + 'GetQualityHidcheckProject', main: "StrippingInspection" },
      { i: 24, img: require('./img/wantconcrete.png'), page: 'MainCheckPage', text: '叫料单(批)', title: '叫料单(批)', sonTitle: '叫料单(批)', url: Url.url + Url.Quality + Url.Fid + 'GetQualityHidcheckProject', main: "Wantconcrete" },
      { i: 25, img: require('./img/change_location.png'), page: 'MainCheckPage', text: '库位变更', title: '库位变更', sonTitle: '库位变更修改', main: "ChangeLocation" },
      { i: 26, img: require('./img/steel_cage_storage.png'), page: 'MainCheckPage', text: '钢筋笼入库(批)', title: '钢筋笼入库(批)', sonTitle: '钢筋笼入库(批)', url: Url.url + Url.Quality + Url.Fid + 'GetQualityHidcheckProject', main: "SteelCageStorage" },
      { i: 27, img: require('./img/equipment_repair.png'), page: 'MainCheckPage', text: '设备报修', title: '设备报修', sonTitle: '设备报修修改', main: "EquipmentRepair" },
      { i: 28, img: require('./img/equipment_service.png'), page: 'MainCheckPage', text: '设备维修', title: '设备维修', sonTitle: '设备维修修改', main: "EquipmentService" },
      { i: 29, img: require('./img/storage.png'), page: 'MainCheckPage', text: '预入库管理', title: '预入库管理', sonTitle: '预入库管理修改', main: "AdvanceStorage", url: Url.url + Url.Quality + Url.Fid + 'GetQualityHidcheckProject' },
      //{ i: 30, img: require('./img/quality_inspection.png'), page: 'MainCheckPage', text: '构件抽检', title: '构件抽检', sonTitle: '构件抽检', main: "SpotCheck", url: Url.url + Url.Quality + Url.Fid + 'GetQualityHidcheckProject' },
      { i: 30, img: require('./img/safe_rectify_notice.png'), page: 'MainCheckPage', text: '安全整改通知单', title: '安全整改通知单', sonTitle: '安全整改通知单', main: 'SafeManageNotice', url: Url.url + Url.Produce + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 31, img: require('./img/safe_rectify_receipt.png'), page: 'MainCheckPage', text: '安全整改回执单', title: '安全整改回执单', sonTitle: '安全整改回执单', main: 'SafeManageReceipt', url: Url.url + Url.Produce + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 32, img: require('./img/post_risk_inspection.png'), page: 'MainCheckPage', text: '岗位风险检查表', title: '岗位风险检查表', sonTitle: '岗位风险检查表', main: 'PostRiskInspection', url: Url.url + Url.Produce + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 33, img: require('./img/sample_manage.png'), page: 'MainCheckPage', text: '质量抽检管理', title: '质量抽检管理', sonTitle: '质量抽检管理', main: 'SampleManage', url: Url.url + Url.Produce + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 34, img: require('./img/sample_recheck.png'), page: 'MainCheckPage', text: '抽检整改单', title: '抽检整改单', sonTitle: '抽检整改单', main: 'SampleManage', url: Url.url + Url.Produce + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 35, img: require('./img/sample_rectify.png'), page: 'MainCheckPage', text: '整改复检', title: '整改复检', sonTitle: '整改复检', main: 'SampleManage', url: Url.url + Url.Produce + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 36, img: require('./img/inventory_check.png'), page: 'MainCheckPage', text: '堆场盘点单', title: '堆场盘点单', sonTitle: '堆场盘点单', pageType: 'CheckPage', main: 'Inveck', url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 37, img: require('./img/material_in_storage.png'), page: 'MaterialCheckPage', text: '采购入库', title: '采购入库', sonTitle: '采购入库', pageType: 'CheckPage', main: 'MaterialInStorage', url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 38, img: require('./img/material_project_picking.png'), page: 'MaterialCheckPage', text: '项目领料', title: '项目领料', sonTitle: '项目领料', pageType: 'CheckPage', main: 'MaterialProjectPicking', url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 39, img: require('./img/material_project_picking.png'), page: 'MaterialCheckPage', text: '部门领料', title: '部门领料', sonTitle: '部门领料', pageType: 'CheckPage', main: 'MaterialProjectPicking', url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 40, img: require('./img/material_out_storage.png'), page: 'MaterialCheckPage', text: '材料出库', title: '材料出库', sonTitle: '材料出库', pageType: 'CheckPage', main: 'MaterialOutStorage', url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 41, img: require('./img/material_out_storage_return.png'), page: 'MaterialCheckPage', text: '材料退库', title: '材料退库', sonTitle: '材料退库', pageType: 'CheckPage', main: 'MaterialOutStorageReturn', url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 42, img: require('./img/material_inventory.png'), page: '', text: '材料盘点', title: '材料盘点', sonTitle: '材料盘点', pageType: 'CheckPage', main: 'MaterialInventory', url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 43, img: require('./img/material_stock.png'), page: 'MaterialStock', text: '库存查询', title: '库存查询', sonTitle: '库存查询', pageType: 'Scan', main: 'MaterialStock', url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 44, img: require('./img/equipment_service.png'), page: 'MainCheckPage', text: '设备维修确认', title: '设备维修确认', sonTitle: '设备维修确认', pageType: 'CheckPage', main: 'EquipmentService', url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 45, img: require('./img/baoxiuchuli.png'), page: 'MainCheckPage', text: '报修处理', title: '报修处理', sonTitle: '报修处理', main: 'CompRepair', url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 46, img: require('./img/goujianweixiu.png'), page: 'MainCheckPage', text: '构件维修', title: '构件维修', sonTitle: '构件维修', main: 'CompMaintain', url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 47, img: require('./img/qualityScrap.png'), page: 'MainCheckPage', text: '构件报废', title: '构件报废', sonTitle: '构件报废', pageType: 'CheckPage', main: 'QualityScrap', url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 48, img: require('./img/handlingToolBaskets.png'), page: 'MainCheckPage', text: '构件装筐', title: '构件装筐', sonTitle: '构件装筐确认', pageType: 'CheckPage', main: 'HandlingToolBaskets', url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 49, img: require('./img/WaterPoolPH.png'), page: 'MainCheckPage', text: '水养池记录', title: '水养池温度/PH记录', sonTitle: '水养池温度/PH记录查看', pageType: 'CheckPage', main: 'WaterPoolPH', url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 50, img: require('./img/WaterPoolDetail.png'), page: 'MainCheckPage', text: '管片水养录入', title: '管片水养录入', sonTitle: '管片水养录入查看', pageType: 'CheckPage', main: 'WaterPoolDetail', url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },

    ]

    if (this.state.isLoading) {
      return (
        <View style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#F5FCFF',
        }}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )

    } else {
      return (
        <View>
          <ScrollView style={{ backgroundColor: 'white', width: width * 0.94, marginLeft: width * 0.03 }} >
            <View style={{ marginVertical: width * 0.03 }}>
              <View style={{ marginLeft: width * 0.02 }}>
                {this.cardView(CardSource)}
              </View>
            </View>
          </ScrollView>
          <PDASuspension
            onPress={() => {
              PushUtil.deleteTag(Url.ID + "_" + Url.deptCode, (code, remain) => {
                if (code == 200) {
                  console.log("🚀 ~ file: PDAMainView.js ~ line 257 ~ Mine_Screen ~ PushUtil.deleteTag ~ remain", remain)
                }
              })
              PushUtil.deleteTag(Url.ID, (code, remain) => {
                if (code == 200) {
                  console.log("🚀 ~ file: PDAMainView.js ~ line 262 ~ Mine_Screen ~ PushUtil.deleteTag ~ remain", remain)
                }
              })
              PushUtil.deleteTag(Url.deptCode, (code, remain) => {
                if (code == 200) {
                  console.log("🚀 ~ file: PDAMainView.js ~ line 267 ~ Mine_Screen ~ PushUtil.deleteTag ~ remain", remain)
                }
              })
              PushUtil.deleteAlias(Url.employeeIdAlias, "employeeId", (code) => {
                if (code == 200) {
                  console.log("🚀 ~ file: PDAMainView.js ~ line 272 ~ Mine_Screen ~ PushUtil.deleteAlias ~ code", code)
                }
              })
              Url1.appPhotoSetting = []
              Url1.appPhotoAlbumSetting = []
              Url1.appDateSetting = []
              this.props.navigation.navigate('Login');
            }}></PDASuspension>
          {/* <ActionButton
            buttonColor="#419FFF"
            onPress={() => {
              PushUtil.deleteTag(Url.ID + "_" + Url.deptCode, (code, remain) => {
                if (code == 200) {
                  console.log("🚀 ~ file: PDAMainView.js ~ line 257 ~ Mine_Screen ~ PushUtil.deleteTag ~ remain", remain)
                }
              })
              PushUtil.deleteTag(Url.ID, (code, remain) => {
                if (code == 200) {
                  console.log("🚀 ~ file: PDAMainView.js ~ line 262 ~ Mine_Screen ~ PushUtil.deleteTag ~ remain", remain)
                }
              })
              PushUtil.deleteTag(Url.deptCode, (code, remain) => {
                if (code == 200) {
                  console.log("🚀 ~ file: PDAMainView.js ~ line 267 ~ Mine_Screen ~ PushUtil.deleteTag ~ remain", remain)
                }
              })
              PushUtil.deleteAlias(Url.employeeIdAlias, "employeeId", (code) => {
                if (code == 200) {
                  console.log("🚀 ~ file: PDAMainView.js ~ line 272 ~ Mine_Screen ~ PushUtil.deleteAlias ~ code", code)
                }
              })
              Url1.appPhotoSetting = []
              Url1.appPhotoAlbumSetting = []
              Url1.appDateSetting = []
              this.props.navigation.navigate('Login');
            }}
            // offsetY={130}
            renderIcon={() => (<View style={styles.actionButtonView}><Icon type='material-community' name='home' color='#ffffff' />
              <Text style={styles.actionButtonText}>返回</Text>
            </View>)}
          /> */}
          {/* <ActionButton
              buttonColor="#419FFF"
              onPress={() => {
                this.props.navigation.navigate('Camera1')
              }}
              offsetY={60}
              renderIcon={() => (<View style={styles.actionButtonView}><Icon type='material-community' name='qrcode-scan' color='#ffffff' />
                <Text style={styles.actionButtonText}>扫码</Text>
              </View>)}
            /> */}
        </View>

      )
    }

  }

}

const styles = StyleSheet.create({
  ViewCard: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    marginTop: -5,
    justifyContent: 'flex-start',
    flex: 1
  },
  img: {
    height: width / 5,
    width: width / 3.5,
    marginLeft: width * 0.016,
    justifyContent: "center",
    alignContent: 'center',
    //backgroundColor: 'blue'
  },
  text: {
    color: 'black',
    textAlign: 'center',
    fontSize: RFT * 3.2,
    width: deviceWidth / 3.5
  },
  backgroundVideo: {
    position: 'absolute',
    color: 'red',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
  actionButtonText: {
    color: 'white',
    fontSize: 14,
  }
})