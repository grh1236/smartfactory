import React from 'react';
import { View, ScrollView, TouchableOpacity, StyleSheet, Dimensions, Alert, StatusBar, ActivityIndicator, DeviceEventEmitter } from 'react-native';
import { Text, Input, Card, Image, Icon, Header } from 'react-native-elements';
import Url1 from '../Url/Url';
import { ToDay } from '../CommonComponents/Data'
import { deviceWidth, RFT } from '../Url/Pixal';
import ActionButton from 'react-native-action-button';
import Daily from '../MainView/Views/Daily'
import DeviceStorage from '../Url/DeviceStorage';
import Url from '../Url/Url';
import PushUtil from '../CommonComponents/PushUtil'
import PDASuspension from './Componment/PDASuspension';

const { height, width } = Dimensions.get('window')

export default class PDAMainView extends React.Component {
  constructor() {
    super();
    this.state = {

    }
  }

  // 消息相关处理
  addUMNotificationListener = (result) => {
    console.log("🚀 ~ file: PDAMainView.js ~ line 24 ~ DailyScreen ~ PushUtil.result", result)
  }

  clickUMNotificationListener = (result) => {
    console.log("🚀 ~ file: PDAMainView.js ~ line 28 ~ PDAMainView ~ PushUtil.clickUMNotificationListener.result", result)
    let resultObj = JSON.parse(result)
    console.log("🚀 ~ file: PDAMainView.js ~ line 30 ~ PDAMainView ~ PushUtil.clickUMNotificationListener.resultObj", resultObj)
    DeviceStorage.save("newMessage", true)
    DeviceStorage.save("resultBody", JSON.stringify(resultObj.body))
    if (resultObj.body.title == "随身工厂-质量抽检消息") {
      this.props.navigation.navigate('MainCheckPage', {
        title: '抽检整改单',
        pageType: 'Scan',
        sonTitle: '抽检整改单',
        page: 'MainCheckPage',
        url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject',
        sonUrl: '',
        main: 'SampleManage'
      })
    } else if (resultObj.body.title == "随身工厂-抽检整改复检消息") {
      this.props.navigation.navigate('MainCheckPage', {
        title: '整改复检',
        pageType: 'Scan',
        sonTitle: '整改复检',
        page: 'MainCheckPage',
        url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject',
        sonUrl: '',
        main: 'SampleManage'
      })
    } else if (resultObj.body.title == "随身工厂-抽检整改复检合格消息") {
      this.props.navigation.navigate('MainCheckPage', {
        title: '整改复检',
        pageType: 'CheckPage',
        sonTitle: '整改复检',
        page: 'MainCheckPage',
        url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject',
        sonUrl: '',
        main: 'SampleManage'
      })
    }
    //this.setState({ newMessage: true, navi: JSON.stringify(resultObj.body) })
  }

  UMNoticeEventListener = (callback) => {
    this.UMNoticeListener = DeviceEventEmitter.addListener("UMNoticeListener", result => {
      callback(result)
    })
  }

  UMClickEventListener = (callback) => {
    this.UMClickListener = DeviceEventEmitter.addListener("UMNoticeClick", result => {
      callback(result)
    })
  }

  componentDidMount() {
    StatusBar.setBarStyle('light-content')
    console.log("🚀 ~ file: PDAMainView.js:223 ~ PDAMainView ~ render ~ Url1:", Url1)
    // 消息监听处理
    this.UMNoticeEventListener(this.addUMNotificationListener)
    this.UMClickEventListener(this.clickUMNotificationListener)
    // 验证是否智慧看板跳转过来
    DeviceStorage.get('changeModelPDA').then(res => {
      if (res) {
        DeviceStorage.save('changeModelPDA', false);
        DeviceStorage.get('resultObj').then(res2 => {
          if (res2.body.title == "随身工厂-质量抽检消息") {
            this.props.navigation.navigate('MainCheckPage', {
              title: '抽检整改单',
              pageType: 'Scan',
              sonTitle: '抽检整改单',
              page: 'MainCheckPage',
              url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject',
              sonUrl: '',
              main: 'SampleManage'
            })
          } else if (res2.body.title == "随身工厂-抽检整改复检消息") {
            this.props.navigation.navigate('MainCheckPage', {
              title: '整改复检',
              pageType: 'Scan',
              sonTitle: '整改复检',
              page: 'MainCheckPage',
              url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject',
              sonUrl: '',
              main: 'SampleManage'
            })
          } else if (res2.body.title == "随身工厂-抽检整改复检合格消息") {
            this.props.navigation.navigate('MainCheckPage', {
              title: '整改复检',
              pageType: 'CheckPage',
              sonTitle: '整改复检',
              page: 'MainCheckPage',
              url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject',
              sonUrl: '',
              main: 'SampleManage'
            })
          }
        })
      }
    })
  }

  setTimer = () => {
    let timer = setTimeout(() => {
      clearTimeout(timer)
      this.setState({
        isLoading: false
      })
    }, 1000)
  }

  cardView = (Source) => {
    if (Url1.PDAauthorgray[1] == '1') {
      Source[4].page = 'HideInspection'
    }
    return (
      <View style={styles.ViewCard}>
        {
          Source.map((u, i) => {
            if (u.i == Url1.PDAauthorgray[i] && u.page != "") {
              return (
                <View style={{ marginVertical: 5 }}>
                  <TouchableOpacity
                    onPress={() => {
                      let isAppPhotoSetting = false
                      let isAppPhotoSetting1 = false
                      let isAppDateSetting = true
                      let isAppDateSetting1 = true
                      let isAppPhotoAlbumSetting = true
                      let isAppPhotoAlbumSetting1 = true

                      let text = u.text + '0'
                      let text1 = u.text + '10'

                      if (Url1.appDateSetting.length != 0) {
                        isAppPhotoSetting = false
                        isAppPhotoSetting1 = false
                        isAppDateSetting = false
                        isAppDateSetting1 = false
                        isAppPhotoAlbumSetting = false
                        isAppPhotoAlbumSetting1 = false
                        if (Url1.appPhotoSetting.indexOf(text) != -1) {
                          isAppPhotoSetting = true
                        }
                        if (Url1.appPhotoSetting.indexOf(text1) != -1) {
                          isAppPhotoSetting1 = true
                        }
                        if (Url1.appDateSetting.indexOf(text) != -1) {
                          isAppDateSetting = true
                        }
                        if (Url1.appPhotoAlbumSetting.indexOf(text) != -1) {
                          isAppPhotoAlbumSetting = true
                        }
                        if (Url1.appPhotoAlbumSetting.indexOf(text1) != -1) {
                          isAppPhotoAlbumSetting1 = true
                        }
                      }

                      this.props.navigation.navigate(u.page, {
                        title: u.title,
                        pageType: u.pageType,
                        sonTitle: u.sonTitle,
                        page: u.page,
                        url: u.url,
                        sonUrl: u.sonUrl,
                        main: u.main,
                        isAppPhotoSetting: isAppPhotoSetting,
                        isAppPhotoSetting1: isAppPhotoSetting1,
                        isAppDateSetting: isAppDateSetting,
                        isAppPhotoAlbumSetting: isAppPhotoAlbumSetting,
                        isAppPhotoAlbumSetting1: isAppPhotoAlbumSetting1,
                      })
                    }}>
                    <View
                      style={[styles.img, {}]}>
                      <Image
                        source={u.img}
                        containerStyle={styles.img}
                        resizeMethod='resize'
                        resizeMode='contain'
                        style={{
                          height: width / 8,
                          width: width / 8,
                          flex: 1,
                          marginVertical: RFT * 6,
                          marginHorizontal: RFT * 5.4,
                        }}
                      ></Image>
                    </View>
                  </TouchableOpacity>
                  <Text numberOfLines={1} style={styles.text}> {u.text}</Text>
                </View>
              )
            }
          })
        }
      </View>
    )
  }

  render() {
    //Url1.PDAurl = 'http://10.100.132.97:8090/pcservice/V1/pdaservice.ashx'
    const Url = this.props.navigation.getParam('Url') ? this.props.navigation.getParam('Url') : Url1
    if (Url1.PDAurl.indexOf('V2') != -1) {
      Url1.isAppNewUpload = true
    } else {
      Url1.isAppNewUpload = false
    }
    const CardSource = [
      { i: 0, img: require('./img/steel_cage_storage.png'), page: 'SteelCageStorage', text: '钢筋笼入库', title: '钢筋笼入库', sonTitle: '钢筋笼入库', pageType: 'Scan', url: Url.url + Url.Produce + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 1, img: require('./img/production_start.png'), page: 'ProductionStart', text: '生产开始', title: '生产开始', sonTitle: '生产开始', pageType: 'Scan', url: Url.url + Url.Produce + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 2, img: require('./img/production_process.png'), page: 'ProductionProcess', text: '生产工序', title: '生产工序', sonTitle: '生产工序', pageType: 'Scan', url: Url.url + Url.Produce + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 3, img: require('./img/steel_cage_die.png'), page: 'SteelCageDie', text: '钢筋笼入模', title: '钢筋笼入模', sonTitle: '钢筋笼入模', pageType: 'Scan', url: Url.url + Url.Produce + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 4, img: require('./img/quality_inspection.png'), page: 'QualityInspection', text: '质量隐检', title: '质量隐检', sonTitle: '质量隐检', pageType: 'Scan', url: Url.url + Url.Produce + Url.Fid + 'GetProduceDailyRecordProject/' + ToDay, sonUrl: '' },
      { i: 5, img: require('./img/buried_card.png'), page: 'BuriedCard', text: '埋卡附码', title: '埋卡附码', sonTitle: '埋卡附码', pageType: 'Scan', url: Url.url + Url.Produce + Url.Fid + 'GetProduceRelease/40/', sonUrl: '' },
      { i: 6, img: require('./img/concrete_pouring.png'), page: 'ConcretePouring', text: '混凝土浇筑', title: '混凝土浇筑', sonTitle: '混凝土浇筑', pageType: 'Scan', url: Url.url + Url.Produce + Url.Fid + 'GetProduceRelease/40/', sonUrl: '' },
      { i: 7, img: require('./img/steel_cage_storage.png'), page: 'SteelCageStorage', text: '构件蒸养', title: '构件蒸养', sonTitle: '构件蒸养', pageType: 'Scan', url: Url.url + Url.Produce + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 8, img: require('./img/stripping_inspection.png'), page: 'StrippingInspection', text: '脱模待检', title: '脱模待检', sonTitle: '脱模待检', pageType: 'Scan', url: Url.url + Url.Produce + Url.Fid + 'GetProduceRelease/40/', sonUrl: '' },
      { i: 9, img: require('./img/quality_product_inspection.png'), page: 'QualityProductInspection', text: '质量成品检', title: '质量成品检', sonTitle: '质量成品检', pageType: 'Scan', url: Url.url + Url.Produce + Url.Fid + 'GetProduceProject', sonUrl: Url.url + Url.Produce + Url.Fid + 'GetProjectProducedComponents/' },
      { i: 10, img: require('./img/quality_product_inspection_review.png'), page: 'QualityProductInspectionReview', text: '成品检复检', title: '成品检复检', sonTitle: '成品检复检', pageType: 'Scan', url: Url + 'GetProduceTask', sonUrl: 'http://47.94.205.217:910/api/Produce/683BF474B82A4D33890299608C885330/GetProduceTaskCompDetails' },
      { i: 11, img: require('./img/storage.png'), page: 'PDAStorage', text: '入库管理', title: '入库管理', sonTitle: '入库管理', pageType: 'Scan', url: Url.url + Url.Quality + Url.Fid + 'GetQualityHidcheckProject' },
      { i: 12, img: require('./img/loading.png'), page: 'PDALoading', text: '装车出库', title: '装车出库', sonTitle: '装车出库', pageType: 'Scan', url: Url.url + Url.Quality + Url.Fid + 'GetQualityHidcheckProject' },
      { i: 13, img: require('./img/loading.png'), page: 'PDASacnLoading', text: '扫码装车', title: '扫码装车', sonTitle: '扫码装车', pageType: 'Scan', url: Url.url + Url.Quality + Url.Fid + 'GetQualityHidcheckProject' },
      { i: 14, img: require('./img/unload.png'), page: 'Unload', text: '卸车管理', title: '卸车管理', sonTitle: '卸车管理', pageType: 'Scan', url: Url.url + Url.Quality + Url.Fid + 'GetQualityHidcheckProject' },
      { i: 15, img: require('./img/install.png'), page: 'PDAInstall', text: '构件安装', title: '构件安装', sonTitle: '构件安装', pageType: 'Scan', url: Url.url + Url.Quality + Url.Fid + 'GetQualityHidcheckProject' },
      { i: 16, img: require('./img/return_goods.png'), page: 'ReturnGoods', text: '退库管理', title: '退库管理', sonTitle: '退库管理', pageType: 'Scan', url: Url.url + Url.Quality + Url.Fid + 'GetQualityHidcheckProject' },
      { i: 17, img: require('./img/equipment_maintenance.png'), page: 'EquipmentMaintenance', text: '设备保养', title: '设备保养', sonTitle: '设备保养', pageType: 'Scan', url: Url.url + Url.Quality + Url.Fid + 'GetQualityHidcheckProject' },
      { i: 18, img: require('./img/equipment_inspection.png'), page: 'EquipmentInspection', text: '设备点检', title: '设备点检', sonTitle: '设备点检', pageType: 'Scan', url: Url.url + Url.Quality + Url.Fid + 'GetQualityHidcheckProject' },
      { i: 19, img: require('./img/wantconcrete.png'), page: 'Wantconcrete', text: '叫料单', title: '叫料单', sonTitle: '叫料单', pageType: 'Scan', url: Url.url + Url.Quality + Url.Fid + 'GetQualityHidcheckProject' },
      { i: 20, img: require('./img/concrete_pouring.png'), page: 'ConcretePouring', text: '混凝土浇筑(批)', title: '混凝土浇筑(批)', sonTitle: '混凝土浇筑(批)', pageType: 'BatchScan', url: Url.url + Url.Quality + Url.Fid + 'GetQualityHidcheckProject' },
      //{ i: 21, img: require('./img/inventory_check.png'), page: 'InventoryCheck', text: '堆场盘点', title: '堆场盘点', sonTitle: '堆场盘点', pageType: 'Scan', url: Url.url + Url.Quality + Url.Fid + 'GetQualityHidcheckProject' },
      { i: 21, img: require('./img/inventory_check.png'), page: 'CompInveck', text: '构件盘点', title: '堆场盘点', sonTitle: '堆场盘点', pageType: 'Scan', url: Url.url + Url.Quality + Url.Fid + 'GetQualityHidcheckProject' },
      { i: 22, img: require('./img/quality_inspection.png'), page: 'HideInspection', text: '综合隐检(批)', title: '综合隐检(批)', sonTitle: '综合隐检(批)', pageType: 'BatchScan', url: Url.url + Url.Quality + Url.Fid + 'GetQualityHidcheckProject' },
      { i: 23, img: require('./img/stripping_inspection.png'), page: 'StrippingInspection', text: '脱模待检(批)', title: '脱模待检(批)', sonTitle: '脱模待检(批)', pageType: 'BatchScan', url: Url.url + Url.Quality + Url.Fid + 'GetQualityHidcheckProject' },
      { i: 24, img: require('./img/wantconcrete.png'), page: 'Wantconcrete', text: '叫料单(批)', title: '叫料单(批)', sonTitle: '叫料单(批)', pageType: 'BatchScan', url: Url.url + Url.Quality + Url.Fid + 'GetQualityHidcheckProject' },
      { i: 25, img: require('./img/change_location.png'), page: 'ChangeLocation', text: '库位变更', title: '库位变更', sonTitle: '库位变更', pageType: 'Scan', url: Url.url + Url.Quality + Url.Fid + 'GetQualityHidcheckProject' },
      { i: 26, img: require('./img/steel_cage_storage.png'), page: 'SteelCageStorage', text: '钢筋笼入库(批)', title: '钢筋笼入库(批)', sonTitle: '钢筋笼入库(批)', pageType: 'BatchScan', url: Url.url + Url.Quality + Url.Fid + 'GetQualityHidcheckProject' },
      { i: 27, img: require('./img/equipment_repair.png'), page: 'EquipmentRepair', text: '设备报修', title: '设备报修', sonTitle: '设备报修', pageType: 'Scan', url: Url.url + Url.Quality + Url.Fid + 'GetQualityHidcheckProject' },
      { i: 28, img: require('./img/equipment_service.png'), page: 'EquipmentService', text: '设备维修', title: '设备维修', sonTitle: '设备维修', pageType: 'Scan', url: Url.url + Url.Quality + Url.Fid + 'GetQualityHidcheckProject' },
      { i: 29, img: require('./img/storage.png'), page: 'AdvanceStorage', text: '预入库管理', title: '预入库管理', sonTitle: '预入库管理', pageType: 'Scan', url: Url.url + Url.Quality + Url.Fid + 'GetQualityHidcheckProject' },
      //{ i: 30, img: require('./img/quality_inspection.png'), page: 'SpotCheck', text: '构件抽检', title: '构件抽检', sonTitle: '构件抽检', pageType: 'Scan', url: Url.url + Url.Quality + Url.Fid + 'GetQualityHidcheckProject' },
      { i: 30, img: require('./img/safe_rectify_notice.png'), page: 'SafeManageNotice', text: '安全整改通知单', title: '安全整改通知单', sonTitle: '安全整改通知单', pageType: 'Scan', url: Url.url + Url.Quality + Url.Fid + 'GetQualityHidcheckProject' },
      { i: 31, img: require('./img/safe_rectify_receipt.png'), page: 'MainCheckPage', text: '安全整改回执单', title: '安全整改回执单', sonTitle: '安全整改回执单', pageType: 'Scan', main: 'SafeManageReceipt', url: Url.url + Url.Produce + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 32, img: require('./img/post_risk_inspection.png'), page: 'PostRiskInspection', text: '岗位风险检查表', title: '岗位风险检查表', sonTitle: '岗位风险检查表', pageType: 'Scan', url: Url.url + Url.Quality + Url.Fid + 'GetQualityHidcheckProject' },
      { i: 33, img: require('./img/sample_manage.png'), page: 'SampleManage', text: '质量抽检管理', title: '质量抽检管理', sonTitle: '质量抽检管理', pageType: 'Scan', url: Url.url + Url.Quality + Url.Fid + 'GetQualityHidcheckProject' },
      { i: 34, img: require('./img/sample_recheck.png'), page: 'MainCheckPage', text: '抽检整改单', title: '抽检整改单', sonTitle: '抽检整改单', pageType: 'Scan', main: 'SampleManage', url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 35, img: require('./img/sample_rectify.png'), page: 'MainCheckPage', text: '整改复检', title: '整改复检', sonTitle: '整改复检', pageType: 'Scan', main: 'SampleManage', url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 36, img: require('./img/inventory_check.png'), page: 'Inveck', text: '堆场盘点单', title: '堆场盘点单', sonTitle: '堆场盘点单', pageType: 'Scan', main: '', url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 37, img: require('./img/material_in_storage.png'), page: 'MaterialInStorage', text: '采购入库', title: '采购入库', sonTitle: '采购入库', pageType: 'Scan', main: 'MaterialInStorage', url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 38, img: require('./img/material_project_picking.png'), page: 'MaterialProjectPicking', text: '项目领料', title: '项目领料', sonTitle: '项目领料', pageType: 'Scan', main: 'MaterialProjectPicking', url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 39, img: require('./img/material_project_picking.png'), page: 'MaterialProjectPicking', text: '部门领料', title: '部门领料', sonTitle: '部门领料', pageType: 'Scan', main: 'MaterialProjectPicking', url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 40, img: require('./img/material_out_storage.png'), page: 'MaterialOutStorage', text: '材料出库', title: '材料出库', sonTitle: '材料出库', pageType: 'Scan', main: 'MaterialOutStorage', url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 41, img: require('./img/material_out_storage_return.png'), page: 'MaterialOutStorageReturn', text: '材料退库', title: '材料退库', sonTitle: '材料退库', pageType: 'Scan', main: 'MaterialOutStorageReturn', url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 42, img: require('./img/material_inventory.png'), page: '', text: '材料盘点', title: '材料盘点', sonTitle: '材料盘点', pageType: 'Scan', main: 'MaterialInventory', url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 43, img: require('./img/sample_rectify.png'), page: '', text: '库存查询', title: '库存查询', sonTitle: '库存查询', pageType: 'Scan', main: 'MaterialStock', url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 44, img: require('./img/equipment_service.png'), page: 'MainCheckPage', text: '设备维修确认', title: '设备维修确认', sonTitle: '设备维修确认', pageType: 'Scan', main: 'EquipmentService', url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 45, img: require('./img/baoxiuchuli.png'), page: 'MainCheckPage', text: '报修处理', title: '报修处理', sonTitle: '报修处理', pageType: 'Scan', main: 'CompRepair', url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 46, img: require('./img/goujianweixiu.png'), page: 'CompMaintain', text: '构件维修', title: '构件维修', sonTitle: '构件维修', pageType: 'Scan', main: 'CompMaintain', url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 47, img: require('./img/qualityScrap.png'), page: 'QualityScrap', text: '构件报废', title: '构件报废', sonTitle: '构件报废', pageType: 'Scan', main: 'xxx', url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 48, img: require('./img/handlingToolBaskets.png'), page: 'HandlingToolBaskets', text: '构件装筐', title: '构件装筐', sonTitle: '构件装筐', pageType: 'Scan', main: 'xxx', url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 49, img: require('./img/WaterPoolPH.png'), page: 'WaterPoolPH', text: '水养池记录', title: '水养池温度/PH记录', sonTitle: '水养池温度/PH记录', pageType: 'Scan', main: 'xxx', url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },
      { i: 50, img: require('./img/WaterPoolDetail.png'), page: 'WaterPoolDetail', text: '管片水养录入', title: '管片水养录入', sonTitle: '管片水养录入', pageType: 'Scan', main: 'xxx', url: Url.url + Url.Quality + Url.Fid + 'GetWaitingPoolProject', sonUrl: '' },

    ]

    if (this.state.isLoading) {
      return (
        <View style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#F5FCFF',
        }}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )

    } else {
      return (
        <View style={{ flex: 1 }}>
          <ScrollView style={{ backgroundColor: 'white', width: width * 0.94, marginLeft: width * 0.03 }} >
            <View style={{ marginVertical: width * 0.03 }}>
              <View style={{ marginLeft: width * 0.02 }}>
                {this.cardView(CardSource)}
              </View>
            </View>
          </ScrollView>
          <PDASuspension
            onPress={() => {
              PushUtil.deleteTag(Url.ID + "_" + Url.deptCode, (code, remain) => {
                if (code == 200) {
                  console.log("🚀 ~ file: PDAMainView.js ~ line 257 ~ Mine_Screen ~ PushUtil.deleteTag ~ remain", remain)
                }
              })
              PushUtil.deleteTag(Url.ID, (code, remain) => {
                if (code == 200) {
                  console.log("🚀 ~ file: PDAMainView.js ~ line 262 ~ Mine_Screen ~ PushUtil.deleteTag ~ remain", remain)
                }
              })
              PushUtil.deleteTag(Url.deptCode, (code, remain) => {
                if (code == 200) {
                  console.log("🚀 ~ file: PDAMainView.js ~ line 267 ~ Mine_Screen ~ PushUtil.deleteTag ~ remain", remain)
                }
              })
              PushUtil.deleteAlias(Url.employeeIdAlias, "employeeId", (code) => {
                if (code == 200) {
                  console.log("🚀 ~ file: PDAMainView.js ~ line 272 ~ Mine_Screen ~ PushUtil.deleteAlias ~ code", code)
                }
              })
              Url1.appPhotoSetting = []
              Url1.appPhotoAlbumSetting = []
              Url1.appDateSetting = []
              this.props.navigation.navigate('Login');
            }}></PDASuspension>
          {/* <ActionButton
            buttonColor="#419FFF"
            onPress={() => {
              PushUtil.deleteTag(Url.ID + "_" + Url.deptCode, (code, remain) => {
                if (code == 200) {
                  console.log("🚀 ~ file: PDAMainView.js ~ line 257 ~ Mine_Screen ~ PushUtil.deleteTag ~ remain", remain)
                }
              })
              PushUtil.deleteTag(Url.ID, (code, remain) => {
                if (code == 200) {
                  console.log("🚀 ~ file: PDAMainView.js ~ line 262 ~ Mine_Screen ~ PushUtil.deleteTag ~ remain", remain)
                }
              })
              PushUtil.deleteTag(Url.deptCode, (code, remain) => {
                if (code == 200) {
                  console.log("🚀 ~ file: PDAMainView.js ~ line 267 ~ Mine_Screen ~ PushUtil.deleteTag ~ remain", remain)
                }
              })
              PushUtil.deleteAlias(Url.employeeIdAlias, "employeeId", (code) => {
                if (code == 200) {
                  console.log("🚀 ~ file: PDAMainView.js ~ line 272 ~ Mine_Screen ~ PushUtil.deleteAlias ~ code", code)
                }
              })
              Url1.appPhotoSetting = []
              Url1.appPhotoAlbumSetting = []
              Url1.appDateSetting = []
              this.props.navigation.navigate('Login');
            }}
            //offsetY={130}
            renderIcon={() => (<View style={styles.actionButtonView}><Icon type='material-community' name='home' color='#ffffff' />
              <Text style={styles.actionButtonText}>返回</Text>
            </View>)}
          /> */}
          {/*  <ActionButton
            buttonColor="#419FFF"
            onPress={() => {
              this.props.navigation.navigate('Camera1')
            }}
            offsetY={60}
            renderIcon={() => (<View style={styles.actionButtonView}><Icon type='material-community' name='qrcode-scan' color='#ffffff' />
              <Text style={styles.actionButtonText}>扫码</Text>
            </View>)}
          /> */}
        </View>
      )
    }

  }

}

const styles = StyleSheet.create({
  ViewCard: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    marginTop: -5,
    justifyContent: 'flex-start',
    flex: 1
  },
  img: {
    height: width / 5,
    width: width / 3.5,
    marginLeft: width * 0.016,
    justifyContent: "center",
    alignContent: 'center',
    //backgroundColor: 'blue'
  },
  text: {
    color: 'black',
    textAlign: 'center',
    fontSize: RFT * 3.2,
    width: deviceWidth / 3.5
  },
  backgroundVideo: {
    position: 'absolute',
    color: 'red',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
  actionButtonText: {
    color: 'white',
    fontSize: 14,
  }
})