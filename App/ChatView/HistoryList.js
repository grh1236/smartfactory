import React from 'react'
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, DeviceEventEmitter } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge } from 'react-native-elements';
import Url from '../Url/Url';
import { ToDay, YesterDay, BeforeYesterDay } from '../CommonComponents/Data';
import { RFT, deviceWidth } from '../Url/Pixal';
import DeviceStorage from '../Url/DeviceStorage';

let isReadArr = []

export default class HistoryList extends React.Component {
  constructor() {
    super();
    this.state = {
      isLoading: true,
      resData: [],
      isRead: true,
      isReadArr: [],
      noticeBadge: '',
      historyData: [],
      idObj: {},
      resData: [
        {
          reciver: 'reciver',
          time: 'time',
          content: 'content'
        },
        {
          reciver: 'reciver',
          time: 'time',
          content: 'content'
        },
        {
          reciver: 'reciver',
          time: 'time',
          content: 'content'
        }
      ]
    }
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('status') == 'now' ? '未读消息' : '历史消息',
    }
  }

  componentDidMount() {
    //this.setTimer();
    Url.Login()
    this.requestData();
  }

  componentWillUnmount() {
    isReadArr = []
  }

  _separator = () => {
    return <View style={{ height: 2, backgroundColor: '#fffffc' }} />;
  }

  setTimer = () => {
    let timer = setTimeout(() => {
      clearTimeout(timer)
      this.setState({
        isLoading: false
      })
    }, 100)
  }

  requestData = () => {
    const status = this.props.navigation.getParam('status')
    const urlnow = Url.url + Url.factory + 'facId/' + Url.Fid + 'recId/' + Url.EmployeeId + '/GetUnSeenMsgListApp'
    console.log("🚀 ~ file: HistoryList.js ~ line 72 ~ HistoryList ~ urlnow", urlnow)
    const urlhis = Url.url + "StatisticFactory/facId/" + Url.Fid + "/Operator/" + Url.employeeName + "/GetNotification";
    if (status == 'now') {
      fetch(urlnow, {
        credentials: 'include',
      }).then(res => {
        console.log(res.status);
        return res.json()
      }).then(resData => {
        console.log("🚀 ~ file: HistoryList.js ~ line 79 ~ HistoryList ~ resData.map ~ resData", resData)
        resData.map((item) => {
          let newtime = this.getTimeStr(item.sendTime)
          item.sendTime = newtime
        })
        this.setState({
          historyData: resData,
          isLoading: false
        })
      }).catch((err) => {
        console.log("Login -> err", err)
      })
    } else {
      fetch(urlhis, {
        credentials: 'include',
      })
        .then((res) => res.json())
        .then((resData) => {
          resData.map((item) => {
            let newtime = this.getTimeStr(item._createtime)
            item.sendTime = newtime
            item.messageTitle = item.bizName
            item.senderName = item.username
            item.messageInfo = item.content
          })
          console.log(resData)
          this.setState({
            historyData: resData,
            isLoading: false
          });
        }).catch(e => console.log(e));
    }

  }

  componentWillUnmount() {
    //DeviceEventEmitter.emit('noticecount', this.state.historyData.length);
  }

  _footer = () => {
    return (
      <Text style={[{ fontSize: 18, textAlign: 'center', marginHorizontal: 30 }]}>第 {this.state.page} 页</Text>
    );
  }

  getTimeStr = (date) => {
    let DateString = date
    var myDate = new Date(DateString);
    /* var year = myDate.getFullYear();    //获取完整的年份(4位,1970-????)
    var month = (Array(2).join(0) + (myDate.getMonth() + 1)).slice(-2);       //获取当前月份(01-12)
    var day = (Array(2).join(0) + myDate.getDate()).slice(-2);        //获取当前日(01-31)
    var hour = (Array(2).join(0) + myDate.getHours()).slice(-2);        //获取当前日(01-31)
    var minute = (Array(2).join(0) + myDate.getMinutes()).slice(-2);        //获取当前日(01-31)
    var second = (Array(2).join(0) + myDate.getSeconds()).slice(-2);        //获取当前日(01-31)
    // 注意js里面的getMonth是从0开始的
    let FormattedDateTime =
      year + '-' +
      month + '-' +
      day + ' ' +
      hour + ':' +
      minute + ':' +
      second; */
    /* let FormattedDateTime =
      myDate.getFullYear() + '-' +
      (Array(2).join(0) + (myDate.getMonth() + 1)).slice(-2) + '-' +
      (Array(2).join(0) + myDate.getDate()).slice(-2) + ' ' +
      (Array(2).join(0) + myDate.getHours()).slice(-2)  + ':' +
      (Array(2).join(0) + myDate.getMinutes()).slice(-2) + ':' +
        myDate.getSeconds(); */
    let length = date.length
    //let FormattedDateTime = date.replace(/T/g, " ").substring(0, length - 4)
    let FormattedDateTime = date.replace(/T/g, " ")

    return FormattedDateTime;
  }

  readSet = (item, index) => {
    const url = Url.url + Url.factory + '5'
    console.log("🚀 ~ file: HistoryList.js:160 ~ HistoryList ~ url", url)
    const urlMessage = Url.url + 'Message/' + '5'
    console.log("🚀 ~ file: HistoryList.js ~ line 155 ~ HistoryList ~ urlMessage", urlMessage)
    const urlMessageTest = "http://10.100.131.222:5001/api/Message/5"

    if (item.bizRangeId.length == 0) {
      let tmp = {}
      tmp.MessageId = item._rowguid + ','
      tmp.ReceiverId = item.receiverId
      console.log("🚀 ~ file: HistoryList.js ~ line 154 ~ HistoryList ~ tmp", JSON.stringify(tmp))
      fetch(urlMessage, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(tmp)
      }).then(res => {
        //console.log("🚀 ~ file: HistoryList.js ~ line 170 ~ HistoryList ~ res", res.text)
        return res.json();
      }).then(resDataMessage => {
        console.log("🚀 ~ file: HistoryList.js ~ line 173 ~ HistoryList ~ resDataMessage", resDataMessage)
        if (resDataMessage.length != 0) {
          isReadArr.push(item._rowguid)
          console.log("🚀 ~ file: HistoryList.js ~ line 163 ~ HistoryList ~ isReadArr", isReadArr)
          this.setState({
            isReadArr: isReadArr
          }, () => {
            this.forceUpdate()
          })
        }
      }).catch((errormess) => {
        console.log("🚀 ~ file: HistoryList.js ~ line 180 ~ HistoryList ~ errormess", errormess)
      });
    } else {
      let tmp = {}
      tmp._rowguid = item._rowguid + ','
      tmp.ReceiverId = item.receiverId
      console.log("🚀 ~ file: HistoryList.js:19971 ~ HistoryList ~ tmp", tmp)
      console.log("🚀 ~ file: HistoryList.js:19971 ~ HistoryList ~ JSON.stringify(tmp)", JSON.stringify(tmp))

      fetch(url, {
        method: 'POST',
        credentials: 'include',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(tmp)
      }).then(res => {
        return res.json();
      }).then(resData => {
        console.log("🚀 ~ file: HistoryList.js:209 ~ HistoryList ~ resData", resData)
        isReadArr.push(item._rowguid)
        console.log("🚀 ~ file: HistoryList.js ~ line 1633 ~ HistoryList ~ isReadArr", isReadArr)
        this.setState({
          isReadArr: isReadArr
        }, () => {
          this.forceUpdate()
        })
      }).catch((error) => {
      });
    }
  }

  render() {
    const status = this.props.navigation.getParam('status')
    console.log(this.state.resData)
    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (
        <View style={{ backgroundColor: '#F9f9f9' }}>
          <View >

            <FlatList
              data={this.state.historyData}
              renderItem={this.renderItem}
              showsVerticalScrollIndicator={false}
              extraData={this.state}
            />

            {/*  <FlatList
              style={{ flex: 1, borderRadius: 10 }}
              ref={(flatList) => this._flatList = flatList}
              //ItemSeparatorComponent={this._separator}
              data={this.state.resData}
              renderItem={this.renderItem}
            // onRefresh={this.refreshing}
            // refreshing={false}
            //控制上拉加载，两个平台需要区分开
            //onEndReachedThreshold={(Platform.OS === 'ios') ? -0.3 : 0.0001}
            //onEndReached={this._onload}
            //numColumns={1}
            // ListFooterComponent={this._footer}
            //columnWrapperStyle={{borderWidth:2,borderColor:'black',paddingLeft:20}}
            //horizontal={true}

            /> */}
          </View>

        </View>
      )
    }

  }

  renderItem = ({ item, index }) => {
    //console.log("🚀 ~ file: HistoryList.js ~ line 184 ~ HistoryList ~ item._createtime", item._createtime)
    const status = this.props.navigation.getParam('status')
    const { isRead, isReadArr } = this.state
    return (
      <Card containerStyle={styles.card_containerStyle}>
        <TouchableOpacity
          onPress={() => {
            if (status == 'now') {
              this.readSet(item, index)
            }
            if (item.messageTitle.indexOf('流程') != -1) {
              DeviceStorage.get('password')
                .then(respass => {
                  let url = Url.urlsys + '/appviews/flowcenter/index.html?s_c_bizrangeid=' + Url.Fidweb + '&treeno=0181002001&s_employeeid=' + Url.EmployeeId + '&s_username=' + Url.username + '&s_userpwd=' + respass
                  this.props.navigation.navigate('Process', {
                    url: url
                  })
                })
              //DeviceStorage.save("newMessage", false)
            } else if (item.messageInfo.indexOf('日报') != -1) {
              this.props.navigation.navigate('DayStatistic', {
                title: '每日统计分析',
                badge: '生产',
                buttonID: 0
              })
            } else if (item.messageInfo.indexOf('周报') != -1) {
              this.props.navigation.navigate('DayStatistic', {
                title: '每日统计分析',
                badge: '生产',
                buttonID: 2
              })
            } else if (item.messageInfo.indexOf('月报') != -1) {
              this.props.navigation.navigate('DayStatistic', {
                title: '每日统计分析',
                badge: '生产',
                buttonID: 3
              })
            }
          }}>
          <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
            <View style={[styles.SN_View]}>
              <View style={[styles.SN_Text_View]}>
                <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
              </View>
            </View>
            <View style={styles.text_View}>
              <View style={{ flexDirection: 'row' }}>
                <View style={{ flexDirection: 'row', flex: 3 }}>
                  <Text style={styles.textname}>标题：</Text>
                  <Text style={[styles.textname, { color: status == 'now' ? ((isReadArr.indexOf(item._rowguid) != -1) ? '#64B1FC' : 'orange') : '#64B1FC', width: deviceWidth / 2 }]} numberOfLines={1}>{item.messageTitle}</Text>
                  <View style={{ flex: 1 }}>
                    <View style={[styles.badgeView, { backgroundColor: status == 'now' ? ((isReadArr.indexOf(item._rowguid) != -1) ? 'rgba(98, 178, 250, 0.22)' : 'rgba(255, 122, 56, 0.22)') : 'rgba(98, 178, 250, 0.22)' }]}>
                      <Badge
                        badgeStyle={{ backgroundColor: status == 'now' ? ((isReadArr.indexOf(item._rowguid) != -1) ? '#64B1FC' : 'orange') : '#64B1FC' }}
                        containerStyle={{ marginRight: 8, top: 10 }}
                      />
                      <Text style={[styles.text, { color: status == 'now' ? ((isReadArr.indexOf(item._rowguid) != -1) ? '#64B1FC' : 'orange') : '#64B1FC', fontWeight: 'bold' }]}>{status == 'now' ? ((isReadArr.indexOf(item._rowguid) != -1) ? '已读' : '未读') : '已读'}</Text>
                    </View>
                  </View>
                </View>

              </View>
              <View style={{ flexDirection: 'row' }}>
                <Text style={styles.textname}>发送人：</Text>
                <Text style={[styles.text]}>{item.senderName}</Text>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <Text style={styles.textname}>时间：</Text>
                <Text style={styles.text}>{item.sendTime}</Text>
              </View>
              <View style={{ flexDirection: 'row', width: deviceWidth * 0.7 }}>
                <Text style={styles.textname}>内容：</Text>
                <Text style={styles.text}>{item.messageInfo}</Text>
              </View>
            </View>

          </View>
        </TouchableOpacity>
      </Card>

    )
  }



}

const styles = StyleSheet.create({
  loading: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  contrain: {
    flexDirection: 'column',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  card_containerStyle: {
    borderRadius: 5,
    shadowOpacity: 0,
    borderWidth: 0,
    elevation: 0,
  },
  SN_View: {
    flex: 1
  },
  SN_Text_View: {
    top: 4
  },
  SN_Text: {
    textAlignVertical: 'center',
    fontSize: 16,
    color: '#535c68'
  },
  SN_Text_Comp: {
    fontSize: 13,
  },
  text_View: {
    flexDirection: 'column',
    backgroundColor: '#FFFFFB',
    flex: 13,
  },
  txt: {

    fontSize: 16,
    marginBottom: 8,
    marginTop: 8,
    marginHorizontal: 8
  },
  text: {
    fontSize: 14,
    marginBottom: 4,
    marginTop: 4,
  },
  textname: {
    fontSize: 14,
    marginBottom: 4,
    marginTop: 4,
    color: 'gray'
  },
  badgeView: {
    //height: 30,
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    borderRadius: 20,
    flex: 1
  },
  textView: {

    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  // txt: {

  //   fontSize: 16,
  //   marginBottom: 8,
  //   marginTop: 8,
  //   marginHorizontal: 8
  // },
  // text: {
  //   fontSize: 14,
  //   marginBottom: 8,
  //   marginTop: 8,
  // },
  // textname: {
  //   fontSize: 14,
  //   marginBottom: 8,
  //   marginTop: 8,
  //   color: 'gray'
  // },
  // title: {
  //   fontSize: 18,
  //   marginBottom: 8,
  //   marginTop: 8,
  // },
  // titlename: {
  //   fontSize: 18,
  //   marginBottom: 8,
  //   marginTop: 8,
  //   color: 'gray'
  // },
  // textView: {

  //   flexDirection: 'row',
  //   justifyContent: 'flex-start'
  // },
  // badgeView: {
  //   //height: 30,
  //   flexDirection: 'row',
  //   justifyContent: 'center',
  //   alignContent: 'center',
  //   borderRadius: 20,
  //   height: 30,
  //   flex: 1
  // },
  // textBadge: {
  //   fontSize: 14,
  //   marginBottom: 8,
  //   marginTop: 5,
  // },
  hidden: {
    flex: 0,
    height: 0,
    width: 0,
  }
})