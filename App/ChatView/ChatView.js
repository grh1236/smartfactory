import React from 'react'
import { View, TouchableOpacity, AppState, DeviceEventEmitter, ActivityIndicator, StyleSheet, StatusBar } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, Header } from 'react-native-elements';
import Url from '../Url/Url';
import { ToDay, YesterDay, BeforeYesterDay } from '../CommonComponents/Data';
import { RFT, deviceWidth, deviceHeight } from '../Url/Pixal';
import { Alert } from 'react-native';
import { NavigationEvents } from 'react-navigation';
import JPush from 'jpush-react-native';
import ActionButton from 'react-native-action-button';


var countg = 0, isSend = 0

export default class ChatView extends React.Component {
  constructor() {
    super();
    this.state = {
      count: null,
      idArrayNotice: {},
      idArrayMessage: {},
      isLoading: true,
      isSend: 0
    }
  }

  setTimer = () => {
    let timer = setTimeout(() => {
      clearTimeout(timer)
      this.setState({
        isLoading: false
      })
    }, 400)
  }

  componentDidMount() {
    Url.Login()
    this.deEmitter1 = DeviceEventEmitter.addListener('noticecount',
      (result) => {
        console.log(result)
        countg = result
        this.setState({
          count: countg
        })
      });

    this.GetUnreadMassage();

    JPush.addLocalNotificationListener((result) => {
      console.log("🚀 ~ file: ChatView.js ~ line 47 ~ ChatView ~ JPush.addLocalNotificationListener ~ result", result)
      if (AppState.currentState === 'active' && result.notificationEventType.indexOf('Opened') != -1) {
        this.props.navigation.navigate('HistoryList', {
          status: 'now'
        })
      }

    })




    //this.setTimer();
  }

  GetUnreadMassage = () => {
    this.setState({ isLoading: true })
    const url = Url.url + Url.factory + 'facId/' + Url.Fid + 'recId/' + Url.EmployeeId + '/GetUnSeenMsgListApp'
    console.log("🚀 ~ file: ChatView.js ~ line 122 ~ ChatView ~ url", url)
    fetch(url, {
      credentials: 'include',
    }).then(res => {
      console.log(res.status);
      return res.json()
    }).then(resData => {
      let idArrayNotice = {}, _rowguidNotice = '', tmpNotice = {}
      let idArrayMessage = {}, _rowguidMessage = '', tmpMessage = {}
      console.log("🚀 ~ file: ChatView.js ~ line 128 ~ ChatView ~ fetch ~ resData", resData)
      isSend += 1
      resData.map((item, index) => {
        if (item.bizRangeId.length == 0) {
          _rowguidMessage = item._rowguid + ',' + _rowguidMessage
          tmpMessage.MessageId = _rowguidMessage
          tmpMessage.ReceiverId = item.receiverId
          let autoid = item.autoid.toString()
          /* if (isSend == 1) {
            JPush.addLocalNotification({
              "messageID": autoid,
              "title": item.messageTitle,
              "content": item.messageInfo,
            })
          } */
        } else {
          _rowguidNotice = item._rowguid + ',' + _rowguidNotice
          tmpNotice._rowguid = _rowguidNotice
          tmpNotice.ReceiverId = item.receiverId
        }
      })
      idArrayNotice = tmpNotice
      idArrayMessage = tmpMessage
      console.log("🚀 ~ file: ChatView.js ~ line 78 ~ ChatView ~ fetch ~ idArrayMessage", idArrayMessage)
      countg = resData.length

      this.setState({
        Unread: resData,
        idArrayNotice: idArrayNotice,
        idArrayMessage: idArrayMessage,
        isLoading: false,
        count: countg,
        isSend: isSend
      }, () => { this.forceUpdate() })
      DeviceEventEmitter.emit('noticecount', resData.length);

    }).catch((err) => {
      console.log("🚀 ~ file: ChatView.js ~ line 86 ~ ChatView ~ fetch ~ err", err)
    })
  }

  componentWillUnmount() {
    this.deEmitter1.remove();
    console.log('remove: ' + this.state.count)
    JPush.removeLocalNotification()

  }

  allReadSet = () => {
    const urlNotice = Url.url + Url.factory + '5'
    const urlMessage = Url.url + 'Message/' + '5'
    const { idArrayNotice, idArrayMessage } = this.state
    console.log("🚀 ~ file: ChatView.js ~ line 95 ~ ChatView ~ idArrayMessage", idArrayMessage)
    console.log("🚀 ~ file: ChatView.js ~ line 95 ~ ChatView ~ idArrayNotice", idArrayNotice)

    let count = 0

    if (JSON.stringify(idArrayNotice).length > 2) {
      fetch(urlNotice, {
        credentials: 'include',
      }, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(idArrayNotice)
      }).then(res => {
        return res.json();
        console.log("🚀 ~ file: ChatView.js ~ line 99 ~ ChatView ~ res", res.status)
      }).then(resDataNotice => {
        console.log("🚀 ~ file: ChatView.js ~ line 126 ~ ChatView ~ resDataNotice", resDataNotice)
        count += 1
        if (count == 2) {
          this.GetUnreadMassage()
          console.log('ChatView ~ count  GetUnreadMassage')
        }
      }).catch((error) => {
        console.log("🚀 ~ file: ChatView.js ~ line 125 ~ ChatView ~ error", error)
      });
    } else { count += 1 }

    if (JSON.stringify(idArrayMessage).length > 2) {
      fetch(urlMessage, {
        credentials: 'include',
      }, {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(idArrayMessage)
      }).then(res => {
        return res.json();
      }).then(resDataMessage => {
        console.log("🚀 ~ file: ChatView.js ~ line 122 ~ ChatView ~ resDataMessage", resDataMessage)
        count += 1
        if (count == 2) {
          this.GetUnreadMassage()
          console.log('ChatView ~ count  GetUnreadMassage')
        }
      }).catch((errormess) => {
        console.log("🚀 ~ file: ChatView.js ~ line 123 ~ ChatView ~ errormess", errormess)
      });
    } else { count += 1 }
    console.log("🚀 ~ file: ChatView.js ~ line 168 ~ ChatView ~ count", count)



  }


  render() {
    const { count } = this.state;

    if (this.state.isLoading) {
      return (
        <View style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#F5FCFF',
        }}>
          <StatusBar translucent={false} />
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
    } else {
      return (
        <View style={{ backgroundColor: '#F9F9F9', flex: 1 }}>
          <View style={{ flexDirection: 'row', }}>
            <StatusBar translucent={false} />
            <NavigationEvents
              onWillFocus={this.GetUnreadMassage} />
            <View style={[styles.img, {}]}>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('HistoryList', {
                    status: 'now'
                  })
                }}
                /* onLongPress={() => {
                  Alert.alert(
                    '提示信息',
                    '是否全部已读',
                    [{
                      text: '取消',
                      style: 'cancel'
                    }, {
                      text: '全部已读',
                      onPress: () => this.allReadSet()
                    }])

                }} */ >
                <Image
                  source={require('./img/unread.png')}
                  containerStyle={styles.img_container}
                  resizeMethod='resize'
                  resizeMode='contain'
                  style={styles.img_style}
                >{this.state.count == 0 || null ? <View></View> :
                  <Badge
                    status="error"
                    containerStyle={{ position: 'absolute', top: -4, right: -4 }}
                    value={this.state.count}

                  />}</Image>
                <Text style={styles.underText}>未读消息</Text>
                {/*  <Header
                statusBarProps={{ barStyle: 'light-content' }}
                barStyle="light-content"
                placement="left"
                leftComponent={<Icon
                  name='work'
                  type='material'
                  color={'#32D185'}
                  onPress={() => {
                    this.props.navigation.navigate('HistoryList', {
                      status: 'now'
                    })
                  }} />}
                centerComponent={{ text: '您有' + (count || '0') + '条未读消息', style: { color: 'gray', fontSize: 24 } }}
                rightComponent={
                  count == 0 ? <View></View> :
                    <Badge
                      value={count}
                      status='error'
                      badgeStyle={{ height: 20 }}
                    ></Badge>
                }
                containerStyle={{
                  backgroundColor: 'white',
                  justifyContent: 'space-around',
                }}
              /> */}
              </TouchableOpacity>
            </View>

            <View style={[styles.img, {}]}>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('HistoryList', {
                    status: 'history'
                  })
                }} >
                <Image
                  source={require('./img/history.png')}
                  containerStyle={styles.img_container}
                  resizeMethod='resize'
                  resizeMode='contain'
                  style={styles.img_style}
                >
                </Image>
                <Text style={styles.underText}>历史消息</Text>
                {/* <Header
                statusBarProps={{ barStyle: 'light-content' }}
                barStyle="light-content"
                placement="left"
                leftComponent={<Icon
                  name='history'
                  type='font-awesome'
                  color={'#FFCF18'}
                  onPress={() => {
                    this.props.navigation.navigate('HistoryList', {
                      status: 'history'
                    })
                  }} />}
                centerComponent={{ text: '历史消息', style: { color: 'gray', fontSize: 24 } }}
                containerStyle={{
                  backgroundColor: 'white',
                  justifyContent: 'space-around',
                }}
              /> */}
              </TouchableOpacity>
            </View>
          </View>

          <ActionButton
            buttonColor="#419FFF"
            onPress={() => {
              Alert.alert(
                '提示信息',
                '是否全部已读',
                [{
                  text: '取消',
                  style: 'cancel'
                }, {
                  text: '全部已读',
                  onPress: () => this.allReadSet()
                }])

            }}
            //offsetY = {110}
            size={RFT * 18}
            renderIcon={() => (<View style={styles.actionButtonView}><Icon type='entypo' name='unread' color='#ffffff' />
              <Text style={styles.actionButtonText}>全部已读</Text>
            </View>)}
          />

          {/*  <ActionButton
            buttonColor="#419FFF"
            onPress={() => {
              this.props.navigation.navigate('Camera', {
                mainpage: "工作台",
                title: '构件扫码'
              })
            }}
            size={RFT * 18}
            renderIcon={() => (<View style={styles.actionButtonView}><Icon type='material-community' name='qrcode-scan' color='#ffffff' />
              <Text style={styles.actionButtonText}>扫  码</Text>
            </View>)}
          /> */}
        </View>
      )
    }

  }

}

const styles = StyleSheet.create({
  ViewCard: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    marginTop: 20,
    justifyContent: 'flex-start',
    flex: 1
  },
  img: {
    height: deviceWidth / 2.3,
    width: deviceWidth / 2.3,
    marginTop: RFT * 6,
    marginLeft: deviceWidth * 0.05,
    justifyContent: "center",
    alignContent: 'center',
    borderRadius: 20,
    backgroundColor: 'white'
  },
  img_container: {
    height: deviceWidth / 4,
    width: deviceWidth / 2,
  },
  img_style: {
    height: deviceWidth / 5.5,
    width: deviceWidth / 5.5,
    flex: 1,
    marginBottom: RFT * 1.5,
    marginTop: RFT * 1,
    marginHorizontal: RFT * 12.5,
  },
  text: {
    color: 'black',
    textAlign: 'center',
  },
  underText: {
    color: 'black',
    textAlign: 'center',
    fontSize: 18
  },
  actionButtonIcon: {
    fontSize: 22,
    height: 22,
    color: 'white',
  },
  actionButtonText: {
    color: 'white',
    fontSize: 13,
  }
})