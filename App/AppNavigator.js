import React from "react";
import { Image, View, DeviceEventEmitter, StatusBar, Text, ImageBackground } from 'react-native';
import { Icon, Badge } from 'react-native-elements'
import { createStackNavigator, createBottomTabNavigator, createAppContainer, createSwitchNavigator, createMaterialTopTabNavigator, BottomTabBar } from "react-navigation";
import Loading from './Loading';
import MainView from './MainView/MainScreen';
import ChatStack from './ChatView/ChatStack';
import Process from './webProcess/Process';
import WorkSpaceView from './WorkSpace/Stack';
import MineView from './MineView/MineView';
import Login from './Login/Login';
import Register from './Login/Register';
import Camera from './CommonComponents/Camera';
import Web from './CommonComponents/Web';
import PDAMainView from './PDA/PDAMainView';
import PDACheckView from './PDA/PDACheckView';
import PDANavigator from "./PDA/PDANavigator";
import MainXMCX from "./XMCX/main";
import MineXMCX from "./XMCX/MineView";
import XMCXStack from "./XMCX/XMCXStack";
import CameraXMCX from "./XMCX/Components/CameraXMCX";

import Url from './Url/Url'
import DeviceStorage from "./Url/DeviceStorage";
import { deviceWidth } from "./Url/Pixal";

class ChatIcon extends React.Component {
  constructor() {
    super();
    this.state = {
      count: 0
    }
  }

  componentDidMount() {
    this.deEmitter = DeviceEventEmitter.addListener('noticecount',
      (result) => {
        this.setState({
          count: result
        })
      });

  }

  componentWillUnmount() {
    this.deEmitter.remove();
  }

  render() {
    return (
      <View>
        <Icon name='message' type='message-processing' size={this.props.focused ? 32 : 28} color={this.props.focused ? '#419FFF' : 'white'}
        />
        {this.state.count == 0 ? <View></View> :
          <Badge
            status="error"
            containerStyle={{ position: 'absolute', top: -4, right: -4 }}
            value={this.state.count}
          />}
      </View>
    )
  }
}

const FactoryTabNavigator = {
  lazy: true,
  //initialRouteName: '首页',
  animationEnabled: true,
  tabBarOptions: {
    activeTintColor: '#419FFF',
    inactiveTintColor: 'gray',
    backgroundColor: 'black',
    headerTitleAllowFontScaling: false,

    style: {
      height: 60,
      backgroundColor: 'black',
    },
  },
  resetOnBlur: true
}

/* <Image source={focused ? require('./tabImg/Main.png') : require('./tabImg/Main1.png')} 
        style={{height: 34, width: 34 }} /> */
const BottomTab = createBottomTabNavigator({
  首页: {
    screen: MainView,
    navigationOptions: {
      headerTitle: '首页',
      tabBarIcon: ({ tintColor, focused }) => {
        return (<Icon name='home' type='entypo' size={focused ? 32 : 28} color={focused ? '#419FFF' : 'white'} />)
      },
    }
  },
  工作台: {
    screen: WorkSpaceView,
    navigationOptions: {
      title: '工作台',
      //tabBarVisible: false,
      tabBarIcon: ({ tintColor, focused }) => {
        return (<Icon name='ios-settings' type='ionicon' size={focused ? 32 : 28} color={focused ? '#419FFF' : 'white'} />)
      },
      tabBarOnPress: (obj) => {
        Url.Login()
        obj.navigation.navigate('Route', {
          Url: Url
        })
      }
    }
  },
  消息: {
    screen: ChatStack,
    navigationOptions: {
      tabBarLabel: '消息',
      tabBarIcon: ({ tintColor, focused }) => {
        return (
          <ChatIcon focused={focused}></ChatIcon>
        )
      },
      tabBarOnPress: (obj) => {
        Url.Login()
        obj.navigation.navigate('ChatView')
        const url = Url.url + Url.factory + 'facId/' + Url.Fid + 'recId/' + Url.EmployeeId + '/GetUnSeenMsgListApp'
        fetch(url).then(res => {
          console.log(res.status);
          return res.json()
        }).then(resData => {
          console.log("🚀 ~ file: AppNavigator.js ~ line 99 ~ fetch ~ resData", resData)
          DeviceEventEmitter.emit('noticecount', resData.length);
        }).catch((err) => {
        })
      }
    }
  },
  /* ,
  通讯录: {
    screen: ContactsView,
    navigationOptions: {
      tabBarLabel: '通讯录',
      tabBarIcon: ({tintColor, focused}) => {
        return( <Icon name = 'contacts'  type = 'antdesign'  size = {focused ? 36 : 32} color = { focused ? '#419FFF' : '#94BAEF' } />)
      },
    }
  }, */
  我的: {
    screen: MineView,
    navigationOptions: {
      headerTitle: '我的',
      tabBarIcon: ({ tintColor, focused }) => {
        return (<Icon name='user' type='font-awesome' size={focused ? 32 : 28} color={focused ? '#419FFF' : 'white'} />)//<Image source={focused ? require('./tabImg/Mine.png') : require('./tabImg/Mine1.png')} style={{height: 34, width: 30 }} />)
      },
      tabBarOnPress: (obj) => {
        Url.Login()
        obj.navigation.navigate('我的', {
          Url: Url
        })
      }
    }
  }
}, FactoryTabNavigator);

const BottomTabProcess = createBottomTabNavigator({
  首页: {
    screen: MainView,
    navigationOptions: {
      headerTitle: '首页',
      tabBarIcon: ({ tintColor, focused }) => {
        return (<Icon name='home' type='entypo' size={focused ? 32 : 28} color={focused ? '#419FFF' : 'white'} />)
      },
    }
  },
  工作台: {
    screen: WorkSpaceView,
    navigationOptions: {
      title: '工作台',
      //tabBarVisible: false,
      tabBarIcon: ({ tintColor, focused }) => {
        return (<Icon name='ios-settings' type='ionicon' size={focused ? 32 : 28} color={focused ? '#419FFF' : 'white'} />)
      },
      tabBarOnPress: (obj) => {
        obj.navigation.navigate('Route', {
          Url: Url
        })
      }
    }
  },
  消息: {
    screen: ChatStack,
    navigationOptions: {
      tabBarLabel: '消息',
      tabBarIcon: ({ tintColor, focused }) => {
        return (
          <ChatIcon focused={focused}></ChatIcon>
        )
      },
      tabBarOnPress: (obj) => {
        obj.navigation.navigate('ChatView')
        const url = Url.url + Url.factory + 'facId/' + Url.Fid + 'recId/' + Url.EmployeeId + '/GetUnSeenMsgListApp'
        fetch(url).then(res => {
          console.log(res.status);
          return res.json()
        }).then(resData => {
          DeviceEventEmitter.emit('noticecount', resData.length);
        }).catch((err) => {
        })
      }
    }
  },
  Process: {
    screen: Process,
    navigationOptions: {
      tabBarLabel: '流程',
      tabBarIcon: ({ tintColor, focused }) => {
        return (<Icon name='share-alt-square' type='font-awesome' size={focused ? 36 : 32} color={focused ? '#419FFF' : 'white'} />)
      },
      tabBarOnPress: (obj) => {
        console.log("🚀 ~ file: AppNavigator.js ~ line 203 ~ AppNavigator", "AppNavigator")
        let username = '', password = '';
        DeviceStorage.get('username')
          .then(resuser => {
            if (resuser) {
              username = Url.ID == 'pkpmfb' ? 'pkpm' : resuser
              DeviceStorage.get('password')
                .then(respass => {
                  if (respass) {
                    password = respass
                    let url = Url.urlsys + '/appviews/flowcenter/index.html?s_c_bizrangeid=' + Url.Fidweb + '&treeno=0181002001&s_employeeid=' + Url.EmployeeId + '&s_username=' + username + '&s_userpwd=' + password + '&platForm=' + "android"
                    console.log("🚀 ~ file: AppNavigator.js ~ line 189 ~ url", url)
                    obj.navigation.navigate('Process', {
                      url: url
                    })
                  }
                }).catch(err => { })
            }
          }).catch(err => { });
      },
      tabBarOnLongPress: (obj) => {
        console.log("🚀 ~ file: AppNavigator.js ~ line 203 ~ AppNavigator", "AppNavigator")
        let username = '', password = '';
        DeviceStorage.get('username')
          .then(resuser => {
            if (resuser) {
              username = Url.ID == 'pkpmfb' ? 'pkpm' : resuser
              DeviceStorage.get('password')
                .then(respass => {
                  if (respass) {
                    password = respass
                    let url = Url.urlsys + '/appviews/flowcenter/index.html?s_c_bizrangeid=' + Url.Fidweb + '&treeno=0181002001&s_employeeid=' + Url.EmployeeId + '&s_username=' + username + '&s_userpwd=' + password + '&t=' + Math.ceil(Math.random() * 10)
                    console.log("🚀 ~ file: AppNavigator.js ~ line 189 ~ url", url)
                    obj.navigation.navigate('Process', {
                      url: url
                    })
                  }
                }).catch(err => { })
            }
          }).catch(err => { });
      },
    },
  },
  /* ,
  通讯录: {
    screen: ContactsView,
    navigationOptions: {
      tabBarLabel: '通讯录',
      tabBarIcon: ({tintColor, focused}) => {
        return( <Icon name = 'contacts'  type = 'antdesign'  size = {focused ? 36 : 32} color = { focused ? '#419FFF' : '#94BAEF' } />)
      },
    }
  }, */
  我的: {
    screen: MineView,
    navigationOptions: {
      resetOnBlur: true,

      headerTitle: '我的',
      tabBarIcon: ({ tintColor, focused }) => {
        return (<Icon name='user' type='font-awesome' size={focused ? 32 : 28} color={focused ? '#419FFF' : 'white'} />)//<Image source={focused ? require('./tabImg/Mine.png') : require('./tabImg/Mine1.png')} style={{height: 34, width: 30 }} />)
      }
    }
  }
}, FactoryTabNavigator);

const BottomTabXMCX = createBottomTabNavigator({
  首页: {
    screen: XMCXStack,
    navigationOptions: {
      tabBarLabel: '首页',
      tabBarIcon: ({ tintColor, focused }) => {
        return (<Icon name='home' type='antdesign' size={36} color={focused ? '#FFD000' : 'white'} />)
      },
    }
  },
  扫码: {
    screen: CameraXMCX,
    navigationOptions: {
      title: '',
      tabBarIcon: ({ tintColor, focused }) => {
        return (<Image source={require('./XMCX/img/Scan3x.png')} style={{ width: 60, height: 60, top: 6 }} />)
      },
      tabBarOnPress: (obj) => {
        obj.navigation.navigate('CameraXMCX', {
          mainpage: "首页",
          title: '构件扫码'
        })
      }
    }
  },
  我的: {
    screen: MineXMCX,
    navigationOptions: {
      headerTitle: '我的',
      tabBarIcon: ({ tintColor, focused }) => {
        return (<Icon name='user' type='antdesign' size={36} color={focused ? '#FFD000' : 'white'} />)//<Image source={focused ? require('./tabImg/Mine.png') : require('./tabImg/Mine1.png')} style={{height: 34, width: 30 }} />)
      }
    }
  }
}, {
  lazy: false,
  tabBarComponent: props => <CustomtabBarComponent {...props} style={{ backgroundColor: 'transparent', borderTopWidth: 0, height: 80 }}
  />,
  //initialRouteName: '首页',
  animationEnabled: true,
  tabBarOptions: {
    activeTintColor: 'white',
    inactiveTintColor: 'white',
    backgroundColor: 'transparent',
    headerTitleAllowFontScaling: false,
    imgSource: require('./XMCX/img/tabBar3x.png'),
    labelStyle: {
      fontSize: 15,
      top: -6
    },
    style: {
      height: 80,
      backgroundColor: 'transparent',
      borderRadius: 80,
      elevation: 0,
      borderTopWidth: 0,

      //width: deviceWidth * 2
    },
  },
  resetOnBlur: true
});


const BottomTab1 = createBottomTabNavigator({
  扫码: {
    screen: PDANavigator,
    navigationOptions: {
      tabBarLabel: '扫码',
      tabBarIcon: ({ tintColor, focused }) => {
        return (
          <ChatIcon focused={focused}></ChatIcon>
        )
      },
    }
  }/* ,
  查询: {
    screen: PDAMainView,
    navigationOptions: {
      tabBarLabel: '查询',
      tabBarIcon: ({ tintColor, focused }) => {
        return (<Icon name='contacts' type='antdesign' size={focused ? 36 : 32} color={focused ? '#419FFF' : '#94BAEF'} />)
      },
    }
  }, */
}, {
  lazy: true,
  //initialRouteName: '首页',
  animationEnabled: true,
  tabBarOptions: {
    activeTintColor: '#419FFF',
    inactiveTintColor: 'gray',
    backgroundColor: 'white',
    style: {
      height: 60,
    },
  },
  resetOnBlur: true
})

const PDATopBottom = createMaterialTopTabNavigator({
  扫码: {
    screen: PDAMainView,
  },
  查询: {
    screen: PDACheckView,
  },
}, {
  tabBarPosition: 'bottom',
  animationEnabled: true
})

const AppNavigator = createSwitchNavigator({
  Login: {
    screen: Login,
  },
  Main: {
    screen: BottomTab,
  },
  Main1: {
    screen: PDANavigator,
  },
  MainProcess: BottomTabProcess,
  MainXMCX: BottomTabXMCX,
  Loading: {
    screen: Loading,
  },
  Camera: Camera,
  Web: {
    screen: Web,
  },
  Register: {
    screen: Register,
    navigationOptions: {
      headerTitle: '注册'
    }
  },
}, {
  lazy: true,

  navigationOptions: {
    headerTransparent: false,
  },
  /* defaultNavigationOptions: {
    headerStyle: {
      //marginTop: 100
    }
  } */
});

const CustomtabBarComponent = (props) => (
  <ImageBackground source={require('./XMCX/img/tabBar3x.png')} style={{ width: deviceWidth }}>
    <BottomTabBar {...props} />
  </ImageBackground>
);

// const CustomtabBarComponent = (props) => (
//   <LinearGradient
//     start={{ x: 0, y: 1 }} end={{ x: 1, y: 0 }}
//     colors={['#F4BC2D', '#F4BC3d']}
//     locations={[0.15, 0.85]} >
//     <MaterialTopTabBar {...props} />
//   </LinearGradient>

// );

export default createAppContainer(AppNavigator);
