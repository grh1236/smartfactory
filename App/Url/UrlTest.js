const Url = {
  url: 'http://47.94.205.217:910/api/',
  project: 'StatisticProject/',
  factory: 'StatisticFactory/',
  Produce: 'Produce/',
  Quality: 'Quality/',
  Fid: '74e1cbc6-2cfc-4d6f-bcd1-739d28e4c74d/',
  Card: 'http://119.3.25.101:910/api/StatisticFactory/',
  Today: 'GetFactoryComponentStatisticToday',
  Yesterday: 'GetFactoryComponentStatisticYesterday',
  Week: 'GetFactoryComponentStatisticWeek',
  Month: 'GetFactoryComponentStatisticMonth',
  Imgurl: 'http://119.3.25.101:81/',
}
export default Url;