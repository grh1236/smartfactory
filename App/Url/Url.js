import { View, ActivityIndicator, Text } from 'react-native'

import React from 'react';

var Url = {
  PDAurl: '',
  url: '',
  urlsys: '',
  StaticType: '0/',
  isStaticTypeOpen: true,
  Imgurl: '',
  project: 'StatisticProject/',
  factory: 'StatisticFactory/',
  Produce: 'Produce/',
  Quality: 'Quality/',
  Stock: 'Stock/',
  PDAFid: '',
  PDAFname: '',
  Fid: 'fa1119c1-ba8e-442c-b2e8-334fafbdae9e/',
  Fidweb: 'fa1119c1-ba8e-442c-b2e8-334fafbdae9e',
  Card: 'https://smart.pkpm.cn/api/StatisticFactory/',
  Today: 'GetFactoryComponentStatisticToday',
  Yesterday: 'GetFactoryComponentStatisticYesterday',
  Week: 'GetFactoryComponentStatisticWeek',
  Month: 'GetFactoryComponentStatisticMonth',
  Imgurl: 'http://smart.pkpm.cn:81',
  EmployeeId: '04c41369-8737-4151-84b6-5da37d32196e',
  ID: '',
  employeeIdAlias: '',
  deptCode: '',
  employeeName: '',
  compTypeNameList: [],
  bigStateList: [],
  productLineNameList: [],
  compRoomNameList: [],
  employeeList: [],
  //XMCX
  ProjectId: '',
  ProjectIdweb: '',
  ProjectName: '',
  ProjectAbb: '',
  ProjectCode: '',
  XMCXauthorgray: [],
  //PDA
  PDAEmployeeId: '',
  PDAEmployeeName: '',
  PDAusername: '',
  PDAauthorgray: '',
  PDAusername: '',
  isAppNewUpload: false,
  isUsedDeliveryPlan: "",
  chengJianPaiZhao: "YES",
  jiaoZhuPaiZhao: "YES",
  isUsePhotograph: "YES",
  isUseTaiWanID: "YES",
  isResizePhoto: "600",
  factoryCode: "",
  isUsedSpareParts: "",
  appPhotoSetting: [],
  appDateSetting: [],
  appPhotoAlbumSetting: [],
  PDAnews: [
    {
      "roomId": "9557F5AFD55D44A39AEC6475C76BEBE5",
      "roomName": "北堆场",
      "library": [
        {
          "libraryId": "4E778F0DCAB142C380AA6801A5E74C8B",
          "libraryName": "A1"
        },
        {
          "libraryId": "D5B42D6AB225477D85AFEEB0A4B22713",
          "libraryName": "A2"
        },
      ]
    }
  ],
  Menu: [],
  Daily: false,
  Project: false,
  Stocks: false,
  Business: false,
  Model: false,
  ChangeFac: false,
  username: '',
  password: '',
  NoticeList: [],
  isLoutuError: false,
  isNewProductionCard: true,
  iRdataResult: "",
  Login: () => {
    let encodePassword = encodeURIComponent(Url.password);
    const accountUrl = Url.url + '/User/' + Url.username + '/CheckLogin/' + encodePassword;
    console.log("🚀 ~ file: Login.js:331 ~ Url ~ fetch ~ accountUrl:", accountUrl)
    fetch(accountUrl).then(res => {
      if (res.ok) {
        return res.text();
      }
    }).then(resData => {
      console.log("🚀 ~ file: Login.js:1624 ~ Login ~ fetch ~ resData", resData)
    })
  },
  isLoadingView:
  <View style={{
    width: 65, height: 65
  }}>
    <ActivityIndicator
      animating={true}
      color="white"
      size="large"
    />
    <Text style={{ color: 'white', marginTop: 10, textAlign: 'center' }}>加载中</Text>
  </View>
}



export default Url;

