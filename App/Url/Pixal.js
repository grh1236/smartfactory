import { Dimensions, PixelRatio, View, ActivityIndicator, Text } from 'react-native';

import React from 'react';

export const saveLoading =
  <View style={{
    width: 65, height: 65
  }}>
    <ActivityIndicator
      animating={true}
      color="white"
      size="large"
    />
    <Text style={{ color: 'white', marginTop: 10, textAlign: 'center' }}>保存中</Text>
  </View>


const fontScale = PixelRatio.getFontScale(); // 返回字体大小缩放比例
const pixelRatio = PixelRatio.get(); // 当前设备的像素密度
export const deviceWidth = Dimensions.get('window').width; // 设备的宽度
export const deviceHeight = Dimensions.get('window').height; // 设备的高度


export const RVW = deviceWidth / 100;
export const RVH = deviceHeight / 100;
export const RFT = RVW / fontScale;
export const RPX = 1 / pixelRatio;

export const color =
  [
    '#EE5A24',
    '#009432',
    '#FFCF18',
    '#7158e2',
    '#17c0eb',
    '#ffb8b8',
    '#c56cf0',
    '#ff3838',
    '#C4E538',
    '#FFC312',
    '#67e6dc',
    '#ED4C67',
    '#FDA7DF',
    '#0652DD',
    '#833471',
    '#12CBC4',
    '#D980FA',
    '#ff9f1a',
    '#1289A7',
    '#A3CB38',
    '#F79F1F',
    '#9980FA',
    '#B53471',
    '#EE5A24',
    '#009432',
    '#fff200',
    '#7158e2',
    '#ff9f1a',
    '#17c0eb',
    '#ff3838',
    '#ffb8b8',
    '#c56cf0',
    '#C4E538',
    '#FFC312',
    '#67e6dc',
    '#ED4C67',
    '#FDA7DF',
    '#0652DD',
    '#833471',
    '#12CBC4',
    '#D980FA',
    '#1289A7',
    '#A3CB38',
    '#F79F1F',
    '#9980FA',
    '#B53471',
    '#EE5A24',
    '#009432',
    '#fff200',
    '#7158e2',
    '#ff9f1a',
    '#17c0eb',
    '#ff3838',
    '#ffb8b8',
    '#c56cf0',
    '#C4E538',
    '#FFC312',
    '#67e6dc',
    '#ED4C67',
    '#FDA7DF',
    '#0652DD',
    '#833471',
    '#12CBC4',
    '#D980FA',
    '#1289A7',
    '#A3CB38',
    '#F79F1F',
    '#9980FA',
    '#B53471',
  ] 
