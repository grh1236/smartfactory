import React from 'react';
import { View, ScrollView, TouchableOpacity, StyleSheet, Dimensions } from 'react-native';
import { Text, Input, Card, Image, Icon } from 'react-native-elements';
import ButtonGroup from '../CommonComponents/ButtonGroupTest'
import Url from '../Url/Url';
import Url1 from '../Url/UrlTest';
import ActionButton from 'react-native-action-button';

const { height, width } = Dimensions.get('window')

const CardSource = [
  { img: require('../viewImg/ZhiLiangYinJian.png'), text: '质量隐检', title: '构件隐蔽检查', sonTitle: '构件隐蔽检查', url: Url.url + Url.Quality + Url.Fid + 'GetQualityHidcheck/10/' },
  { img: require('../viewImg/ChengPinJianCha.png'), text: '成品检查', title: '成品检查', sonTitle: '成品检查', url: Url.url + Url.Quality + Url.Fid + 'GetQualityFinProducts/10/' },
  { img: require('../viewImg/ChengPinFuCha.png'), text: '成品复检', title: '成品复检', sonTitle: '成品复检', url: Url.url + Url.Quality + Url.Fid + 'GetQualityReFinProducts' },
  { img: require('../viewImg/ZhiLiangWenTiJiLu.png'), text: '质量问题记录', title: '质量问题记录', sonTitle: '质量问题记录', url: Url.url + Url.Quality + Url.Fid + 'GetProduceDailyRecord' },
  { img: require('../viewImg/GouJianBaoFei.png'), text: '构件报废', title: '构件报废', sonTitle: '构件报废', url: Url.url + Url.Quality + Url.Fid + 'GetQualityScrap' },
]

export default class LogInScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      title: {
        first: '已检',
        second: '合格',
        third: '不合格',
        fourth: '报废'
      },
      text: null,
      resDataToday: {
        firstNumber: 0,
        firstVolume: 0,
        secondNumber: 0,
        secondVolume: 0,
        thirdNumber: 0,
        thirdVolume: 0,
        fourthNumber: 0,
        fourthVolume: 0,
      }, resDataYesterday: {
        firstNumber: 0,
        firstVolume: 0,
        secondNumber: 0,
        secondVolume: 0,
        thirdNumber: 0,
        thirdVolume: 0,
        fourthNumber: 0,
        fourthVolume: 0,
      }, resDataWeek: {
        firstNumber: 0,
        firstVolume: 0,
        secondNumber: 0,
        secondVolume: 0,
        thirdNumber: 0,
        thirdVolume: 0,
        fourthNumber: 0,
        fourthVolume: 0,
      }, resDataMonth: {
        firstNumber: 0,
        firstVolume: 0,
        secondNumber: 0,
        secondVolume: 0,
        thirdNumber: 0,
        thirdVolume: 0,
        fourthNumber: 0,
        fourthVolume: 0,
      },
    }
    this.requestDataToday = this.requestDataToday.bind(this);
    this.requestDataYesterday = this.requestDataYesterday.bind(this);
    this.requestDataWeek = this.requestDataWeek.bind(this);
    this.requestDataMonth = this.requestDataMonth.bind(this);
  }

  requestDataToday = () => {
    const url = Url.url + Url.factory + Url.Fid + Url.Today;
    fetch(url).then(res => {
      return res.json()
    }).then(resData => {
      this.setState({
        resDataToday: resData,
      });
      const resDataToday = JSON.parse(JSON.stringify(this.state.resDataToday)
        .replace(/finNumber/g, "firstNumber")
        .replace(/finVolume/g, "firstVolume")
        .replace(/finPassNumber/g, "secondNumber")
        .replace(/finPassVolume/g, "secondVolume")
        .replace(/finNotPassNumber/g, "thirdNumber")
        .replace(/finNotPassVolume/g, "thirdVolume")
        .replace(/scrapNumber/g, "fourthNumber")
        .replace(/scrapVolume/g, "fourthVolume"));
      this.setState({
        resDataToday: resDataToday
      })
      console.log(resData)
    }).catch((error) => {
      console.log(error);
    });
  }

  requestDataYesterday = () => {
    const url = Url.url + Url.factory + Url.Fid + Url.Yesterday;
    fetch(url).then(resYesterday => {
      return resYesterday.json()
    }).then(resDataYesterday => {
      this.setState({
        resDataYesterday: resDataYesterday,
      });
      const resDataYesterdaynew = JSON.parse(JSON.stringify(this.state.resDataYesterday)
        .replace(/finNumber/g, "firstNumber")
        .replace(/finVolume/g, "firstVolume")
        .replace(/finPassNumber/g, "secondNumber")
        .replace(/finPassVolume/g, "secondVolume")
        .replace(/finNotPassNumber/g, "thirdNumber")
        .replace(/finNotPassVolume/g, "thirdVolume")
        .replace(/scrapNumber/g, "fourthNumber")
        .replace(/scrapVolume/g, "fourthVolume"));
      this.setState({
        resDataYesterday: resDataYesterdaynew
      })
      console.log(this.state.resDataYesterday)
    }).catch((error) => {
      console.log(error);
    });
  }

  requestDataWeek = () => {
    const url = Url.url + Url.factory + Url.Fid + Url.Week;
    fetch(url).then(resWeek => {
      return resWeek.json()
    }).then(resDataWeek => {
      this.setState({
        resDataWeek: resDataWeek,
      });
      const resDataWeeknew = JSON.parse(JSON.stringify(this.state.resDataWeek)
        .replace(/finNumber/g, "firstNumber")
        .replace(/finVolume/g, "firstVolume")
        .replace(/finPassNumber/g, "secondNumber")
        .replace(/finPassVolume/g, "secondVolume")
        .replace(/finNotPassNumber/g, "thirdNumber")
        .replace(/finNotPassVolume/g, "thirdVolume")
        .replace(/scrapNumber/g, "fourthNumber")
        .replace(/scrapVolume/g, "fourthVolume"));
      this.setState({
        resDataWeek: resDataWeeknew
      })
      console.log(this.state.resDataWeek)
    }).catch((error) => {
      console.log(error);
    });
  }

  requestDataMonth = () => {
    const url = Url.url + Url.factory + Url.Fid + Url.Month;
    fetch(url).then(resMonth => {
      return resMonth.json()
    }).then(resDataMonth => {
      this.setState({
        resDataMonth: resDataMonth,
      });
      const resDataMonthnew = JSON.parse(JSON.stringify(this.state.resDataMonth)
        .replace(/finNumber/g, "firstNumber")
        .replace(/finVolume/g, "firstVolume")
        .replace(/finPassNumber/g, "secondNumber")
        .replace(/finPassVolume/g, "secondVolume")
        .replace(/finNotPassNumber/g, "thirdNumber")
        .replace(/finNotPassVolume/g, "thirdVolume")
        .replace(/scrapNumber/g, "fourthNumber")
        .replace(/scrapVolume/g, "fourthVolume"));
      this.setState({
        resDataMonth: resDataMonthnew
      })
      console.log(this.state.resDataMonth)
    }).catch((error) => {
      console.log(error);
    });
  }

 componentDidMount() {
    this.requestDataToday();
    this.requestDataYesterday();
    this.requestDataWeek();
    this.requestDataMonth();
  }



  render() {
    const { resDataToday, resDataYesterday, resDataWeek, resDataMonth, title } = this.state;
    return (
      <ScrollView >

        <ButtonGroup
          testDataToday={resDataToday}
          testDataYesterday={resDataYesterday}
          testDataSevenDay={resDataWeek}
          testDataMonth={resDataMonth}
          title={title} />

        <View style={styles.ViewCard}>
          {
            CardSource.map((u, i) => {
              return (
                <View style={{ marginVertical: 20 }}>
                  <TouchableOpacity 
                  onPress={() => {
                    this.props.navigation.navigate('ChildOne', {
                      title: u.title,
                      sonTitle: u.sonTitle,
                      data: this.state.dataTest,
                      url: u.url,
                      sonUrl: u.sonUrl
                    })
                    console.log(u.sonTitle)
                  }}>
                    <Image
                      source={u.img}
                      style={styles.img}
                    ></Image>
                  </TouchableOpacity>
                  <Text style={{ color: 'black', textAlign: 'center' }}> {u.text} </Text>
                </View>
              );
            })
          }

        </View>
        <View style={{ height: 80 }}>
          <ActionButton
            buttonColor='#00CCFF'
            renderIcon={() => {
              return (
                <Icon
                  name='refresh'
                  color='white' />
              )
            }}
            onPress={this.requestData} />
        </View>
      </ScrollView>


    );
  }
}

const styles = StyleSheet.create({
  ViewCard: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    marginTop: 20,
    justifyContent: 'flex-start',
    flex: 1
  },
  img: {
    height: width / 3,
    width: width / 3
  }
})