import React from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, ListItem, BottomSheet } from 'react-native-elements';

const { height, width } = Dimensions.get('window') //获取宽高

export default class ListItemScan extends React.Component {

  render() {
    const { titlename, title, color, bottomDivider, onPress } = this.props;
    let bottomDivider1 = bottomDivider
    if (typeof (bottomDivider) == "undefined") {
      bottomDivider1 = true
    }
    return (
      <ListItem
        containerStyle={{ paddingVertical: 18, backgroundColor: 'transparent' }}
        leftElement={
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ fontWeight: "300" }}>{titlename}</Text>
            <Text style={{ color: color, fontWeight: "300" }} >{title}</Text>
          </View>}
        underlayColor = {"lightgray"}
        titleStyle={{ fontSize: 14 }}
        bottomDivider={bottomDivider1}
        onPress = {onPress}
      />
    )
  }
}