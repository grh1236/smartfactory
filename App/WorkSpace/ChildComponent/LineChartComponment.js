import React from 'react';
import {
  StyleSheet,
  Text,
  Button,
  View, processColor, ActivityIndicator
} from 'react-native';

 import {LineChart} from 'react-native-charts-wrapper';

 export default class lineChartScreen extends React.Component {

  constructor() {
    super();
    this.state = {
      chartData: {},
      chartXAxis: {},
      data: {},
      chartLoading: true,
      marker: {
        enabled: true,
        digits: 1,
        markerColor: processColor('#999999'),
        textColor: processColor('white'),
        markerFontSize: 16,
      },
      xAxis: {
        granularityEnabled: true,
        granularity: 1,
      },
      yAxis:{
        left: {
          enabled: true,
          granularity: 1,
          axisMinimum: 0,
          
        },
        right: {
          enabled: false,
          granularity: 1,
          axisMinimum: 0,
        }
      },
      legend: {
        enabled: true,
        // textSize: 14,
        form: "SQUARE",
        //formSize: 14,
        xEntrySpace: 10,
        yEntrySpace: 5,
        wordWrapEnabled: true,
        horizontalAlignment: 'RIGHT',
        verticalAlignment: 'TOP'
      },
      // visibleRange: {x: {min: 1, max: 2}}
    };
  }

  componentDidMount() {
    //this.refs.chart.moveViewToAnimated(100, 0, 'left', 10000)
  }

  handleSelect(event) {
    let entry = event.nativeEvent
    if (entry == null) {
      this.setState({...this.state, selectedEntry: null})
    } else {
      this.setState({...this.state, selectedEntry: JSON.stringify(entry)})
    }

    console.log(event.nativeEvent)
  }

  setStateLoading = () => {
    this.setState({
      chartLoading: true
    });
  }

  ReFlash (chartData, chartXAxis)  {
    this.setState({
      chartData: chartData,
      chartXAxis: chartXAxis,
      chartLoading: false
    })
    this.refs.chart.moveViewToAnimated(10, 0, 'left', 2000)
  }

  render() {
    if (this.state.chartLoading === true) {
      return (
        <View style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#F5FCFF',
        }}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
    } else {
      return (
        <View style={{flex: 1}}>
          <View style={styles.container}>
            <LineChart
              style={styles.chart}
              data={this.state.chartData}
              chartDescription={{text: ''}}
              legend={this.state.legend}
              marker={this.state.marker}
              xAxis={this.state.chartXAxis}     
              yAxis = {this.state.yAxis}       
              autoScaleMinMaxEnabled={false}
              touchEnabled={true}
              dragEnabled={true}
              scaleEnabled={false}
              pinchZoom={false}
              doubleTapToZoomEnabled={false}
              highlightPerTapEnabled={true}
              highlightPerDragEnabled={false}
              // visibleRange={this.state.visibleRange}
              dragDecelerationEnabled={true}
              dragDecelerationFrictionCoef={0.99}
              keepPositionOnRotation={false}
              animation={{
                durationX: 1000,
                durationY: 1500,
                easingY: 'EaseInOutQuart'
              }}
              visibleRange={{
                x: { min: 4, max: 4 }
              }}
              onSelect={this.handleSelect.bind(this)}
              onChange={(event) => console.log(event.nativeEvent)}
              ref="chart"
            />
          </View>
        </View>
      );
    }
   
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //backgroundColor: '#F5FCFF'
  },
  chart: {
    flex: 1
  }
});


