import React from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, ListItem, BottomSheet } from 'react-native-elements';

const { height, width } = Dimensions.get('window') //获取宽高

export default class SideSearchItem extends React.Component {
  render() {
    const { searchname, Picker, color, bottomDivider } = this.props;
    return (
      <View style = {{marginBottom: 10}}>
        <View style={{ marginLeft: 6, marginTop: 8 }} >
          <Text style={{ fontSize: 16 }}>{searchname}</Text>
        </View>
        <View style={{ marginLeft: width * 0.005, marginRight: width * 0.08, backgroundColor: "#f8f8f8", borderRadius: 5, marginTop: 10 }}>
          <ListItem
            title={
              <View style={{ flexDirection: 'row' }}>
                {/* <Icon name={isCheckSecondList.indexOf(item.defectTypeId) != -1 ? 'downcircle' : 'rightcircle'} size={18} type='antdesign' color='#419FFF' /> */}
                <Text style={{ color: '#666', fontSize: 14, marginLeft: 4 }}>{Picker}</Text>
              </View>
            }
            underlayColor={'lightgray'}
            containerStyle={{ marginVertical: 0, paddingVertical: 10, borderColor: 'transparent', borderWidth: 0, backgroundColor: "transparent" }}
            /* onPress={() => {
              this.setState({ isCompTypePickerVisible: !isCompTypePickerVisible })
            }} */
            rightElement={
              <Icon size={18} color='#666' type='antdesign' name='down-square-o' />
            } />
        </View>
      </View>
    )
  }
}