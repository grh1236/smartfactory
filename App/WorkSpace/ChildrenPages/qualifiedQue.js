import React from 'react';
import { View, StyleSheet, Platform, Dimensions, processColor, ScrollView } from 'react-native';
import { Card, Text } from 'react-native-elements'
import { HorizontalBarChart, LineChart } from 'react-native-charts-wrapper';
import PieChart from '../../MainView/Components/PieChartCommon'
import Url from '../../Url/Url'

const { height, width } = Dimensions.get('screen')
var resDataBuildLineNew = [], resDataWorkQvalue = [], resDataWorkQvaluelabel = [], resDataProjectQualityvalue = [], resDataProjectQualityXaxis = [];

export default class ZhiliangQuestion extends React.Component {
  constructor() {
    super();
    this.state = {
      isLoading: true,
      resDataBuildLine: [
        { value: 14, label: "一号流水线" },
        { value: 16, label: "一号流水线" },
        { value: 18, label: "一号流水线" },
        { value: 13, label: "一号流水线" }
      ],
      resDataBuildLineNew: [],
      resDataWorkQvalue: [],
      resDataWorkQvaluelabel: [],
      resDataProjectQualityvalue: [],
      resDataProjectQualityXaxis: [],
      legend: {
        enabled: true,
        textSize: 16,
        form: 'CIRCLE',
        wordWrapEnabled: true,
        xEntrySpace: 13,
        yEntrySpace: 6,
      },
      description: {
        text: ' ',
        textSize: 15,
        textColor: processColor('darkgray'),
      },
      marker: {
        enabled: true,
        markerColor: processColor('#999999'),
        textColor: processColor('white'),
        markerFontSize: 12,
      }
    }
    this.requestDataBuildLine = this.requestDataBuildLine.bind(this);
    this.requestDataWorkQ = this.requestDataWorkQ.bind(this);
    this.requestDataProjectQuality = this.requestDataProjectQuality.bind(this);
  }

  componentWillMount() {
    this.requestDataBuildLine();
    this.requestDataWorkQ();
    this.requestDataProjectQuality();
  }

//顶栏
static navigationOptions = ({ navigation }) => {
  return {
    title: navigation.getParam('title')
  }
}

  handleSelect(event) {
    let entry = event.nativeEvent
    if (entry == null) {
      this.setState({ ...this.state, selectedEntry: null })
    } else {
      this.setState({ ...this.state, selectedEntry: JSON.stringify(entry) })
    }

    console.log(event.nativeEvent)
  }

  requestDataBuildLine = () => {
    const url = Url.url + Url.Quality + Url.Fid + '/GetQualityIssuesFacLineStatistic';
    console.log(url)
    fetch(url, {
      credentials: 'include',
    }).then(resBuildLine => {
      return resBuildLine.json()
    }).then(resDataBuildLine => {
      this.setState({
        isLoading: false,
      }, () => {
        //let i = 0;
        resDataBuildLine.map((item, index) => {
          let tmp = {};
          tmp.value = item.qualityIssuesNumber;
          tmp.label = item.productLineName;
          console.log(tmp, index);
          resDataBuildLineNew.push(tmp);
        });
        this.setState({
          resDataBuildLine: resDataBuildLineNew
        });
      });
      console.log(resDataBuildLineNew);
    }).catch((error) => {
      console.log(error);
    });
  }

  requestDataWorkQ = () => {
    const url = Url.url + Url.Quality + Url.Fid + '/GetQualityIssuesFacLabourStatistic';
    console.log(url)
    fetch(url, {
      credentials: 'include',
    }).then(resWorkQ => {
      return resWorkQ.json()
    }).then(resDataWorkQ => {
      this.setState({
        isLoading: false,
      }, () => {
        //let i = 0;
        resDataWorkQ.map((item, index) => {
          let tmp = {};
          tmp.y = item.qualityIssuesNumber;
          resDataWorkQvalue.push(tmp);
          resDataWorkQvaluelabel.push(item.labourName);
          console.log(tmp, index);
        });
        this.setState({
          resDataWorkQvalue: resDataWorkQvalue,
          resDataWorkQvaluelabel: resDataWorkQvaluelabel,
        });
      });
      console.log(resDataWorkQ);
    }).catch((error) => {
      console.log(error);
    });
  }

  requestDataBuildLine = () => {
    const url = Url.url + Url.Quality + Url.Fid + '/GetQualityIssuesFacLineStatistic';
    console.log(url)
    fetch(url, {
      credentials: 'include',
    }).then(resBuildLine => {
      return resBuildLine.json()
    }).then(resDataBuildLine => {
      this.setState({
        isLoading: false,
      }, () => {
        //let i = 0;
        resDataBuildLine.map((item, index) => {
          let tmp = {};
          tmp.value = item.qualityIssuesNumber;
          tmp.label = item.productLineName;
          console.log(tmp, index);
          resDataBuildLineNew.push(tmp);
        });
        this.setState({
          resDataBuildLine: resDataBuildLineNew
        });
      });
      console.log(resDataBuildLineNew);
    }).catch((error) => {
      console.log(error);
    });
  }

  requestDataProjectQuality = () => {
    const url = Url.url + Url.Quality + Url.Fid + '/GetQualityIssuesFacProjectStatistic';
    console.log(url);
    fetch(url, {
      credentials: 'include',
    }).then(resProjectQuality => {
      return resProjectQuality.json()
    }).then(resDataProjectQuality => {
      this.setState({
        isLoading: false,
      }, () => {
        //let i = 0;
        resDataProjectQuality.map((item, index) => {
          let tmp = {};
          tmp.y = item.qualityIssuesNumber;
          tmp.x = index;
          tmp.marker = item.projectAbb;
          resDataProjectQualityvalue.push(tmp);
          resDataProjectQualityXaxis.push(item.projectAbb);
          console.log(tmp, index);
        });
        this.setState({
          resDataProjectQualityvalue: resDataProjectQualityvalue,
          resDataProjectQualityXaxis: resDataProjectQualityXaxis,
        });
      });
      console.log(this.state.resDataProjectQualityXaxis);
    }).catch((error) => {
      console.log(error);
    });
  }

  render() {
    const { resDataBuildLine, resDataWorkQvalue, resDataWorkQvaluelabel, resDataProjectQualityvalue, resDataProjectQualityXaxis } = this.state;
    console.log(resDataBuildLine);
    return (
      <ScrollView>
        <View style={{
          height: width / 1.3, width: width / 1.3, alignContent: 'center', justifyContent: 'center'
        }}>
          <Text style={{ flex: 1, fontSize: 20, paddingLeft: 15, paddingTop: 15, color: 'gray' }}>生产线质量问题分布</Text>
          <View style={{
            height: width / 1.5,
            width: width / 1.15
          }} >
            <View style={styles.contianer}>
              <PieChart
                noDataText='NO DATA'
                style={styles.chart}
                data={{
                  dataSets: [{
                    values: resDataBuildLine,
                    label: ' ',
                    config: {
                      colors: [processColor('#8579E8'),
                      processColor('#9b59b6'),
                      processColor('#419FFF'),
                      processColor('#41CCFF'),
                      processColor('#FF9D18'),
                      processColor('#FFCF18'),
                      processColor('#1abc9c'),
                      processColor('#2ecc71'),
                      ],
                      valueTextSize: 10,
                      valueTextColor: processColor('green'),
                      sliceSpace: 5,
                      selectionShift: 13,
                      xValuePosition: "OUTSIDE_SLICE",
                      yValuePosition: "OUTSIDE_SLICE",
                      valueFormatter: "#.#'%'",
                      valueLineColor: processColor('green'),
                      valueLinePart1Length: 0.5
                    }
                  }],
                }}
                legend={this.state.legend}
              />
            </View>
          </View>
        </View>

        <View style={{ height: width + 10 }}>
          <Text style={{ fontSize: 20, paddingLeft: 15, paddingTop: 15, color: 'gray' }}>劳务队质量问题统计</Text>
          <HorizontalBarChart
          scaleYEnabled={false}
            style={styles.chart}
            data={{
              dataSets: [{
                values: resDataWorkQvalue,
                label: ' ',
                config: {
                  colors: processColor('teal'),
                  barShadowColor: processColor('lightgrey'),
                  highlightAlpha: 90,
                  highlightColor: processColor('red'),
                }
              }],
            }}
            xAxis={{
              valueFormatter: resDataWorkQvaluelabel,
              position: 'BOTTOM',
              granularityEnabled: true,
              granularity: 1,
              labelCount: 10,
            }}
            yAxis={{
              left: {
                axisMinimum: 0
              },
              right: {
                axisMinimum: 0
              }
            }}
            animation={{ durationX: 2000 }}
            legend={this.state.legend}
            gridBackgroundColor={processColor('#ffffff')}
            drawBarShadow={false}
            drawValueAboveBar={true}
            drawHighlightArrow={true}
            onSelect={this.handleSelect.bind(this)}
            onChange={(event) => console.log(event.nativeEvent)}
          />
        </View>

        <View style={{ height: width + 10 }}>
          <Text style={{ fontSize: 20, paddingLeft: 15, paddingTop: 15, color: 'gray' }}>项目质量问题频次</Text>
          <LineChart
            scaleYEnabled={false}
            style={styles.chart}
            data={{
              dataSets: [
                {
                  values: resDataProjectQualityvalue,
                  label: "劳务队问题记录",
                  config: {
                    mode: "CUBIC_BEZIER",
                    drawValues: false,
                    lineWidth: 2,
                    drawCircles: true,
                    circleColor: processColor('#449BCE'),
                    drawCircleHole: false,
                    circleRadius: 5,
                    highlightColor: processColor("transparent"),
                    color: processColor('#449BCE'),
                    drawFilled: false,
                    fillGradient: {
                      colors: [processColor('#449BCE'), processColor('#449BCE')],
                      positions: [0, 0.5],
                      angle: 90,
                      orientation: "TOP_BOTTOM"
                    },
                    fillAlpha: 1000,
                    valueTextSize: 15
                  }
                }
              ]
            }}
            xAxis={{
              enabled: true,
              valueFormatter: resDataProjectQualityXaxis,
              granularity: 1,
              drawLabels: true,
              position: "BOTTOM",
              drawAxisLine: false,
              drawGridLines: false,
              fontFamily: "HelveticaNeue-Medium",
              fontWeight: "bold",
              textSize: 12,
              textColor: processColor("black"),
              labelRotationAngle: -30
            }}
            yAxis={{
              left: {
                enabled: true,
                axisMinimum: 0
              },
              right: {
                enabled: false
              }
            }}
            animation={{
              durationX: 0,
              durationY: 1500,
              easingY: 'EaseInOutQuart'
            }}
            onSelect={this.handleSelect.bind(this)}
            onChange={(event) => console.log(event.nativeEvent)}
            marker={this.state.marker}
            visibleRange={{
              x: { min: 6 },
              y: { min: 1 }

            }}
            autoScaleMinMaxEnabled={true}
            drawValueAboveBar={true} />
        </View>



      </ScrollView >
    )
  }

}

const styles = StyleSheet.create({
  contianer: {
    flex: 1,

  },
  chart: {
    flex: 1
  }
})