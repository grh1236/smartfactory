import React from 'react'
import { View, StyleSheet, Dimensions, processColor, FlatList, ScrollView, ActivityIndicator } from 'react-native'
import { Card, Text, Image, Button, Badge, ListItem, Overlay, Header, Icon } from 'react-native-elements'
import LinearGradient from 'react-native-linear-gradient';
import ListItemTwo from "../ChildComponent/ListItemTwo";
import ListItemTwoThree from "../ChildComponent/ListItemTwoThree";
import { RFT } from '../../Url/Pixal';
import Url from '../../Url/Url'
import Pdf from 'react-native-pdf';
import TouchableCustom from '../../PDA/Componment/TouchableCustom';


const color =
  [
    '#f1c40f',
    '#f39c12',
    '#41CCFF',
    '#2ecc71',
    '#9b59b6',
    '#449BCE',
    '#e67e22',
    '#d35400',
    '#22BC77',
    '#229DFF',
    '#e74c3c',
    '#27ae60',
    '#f1c40f',
  ]

//定义屏幕宽高
const { height, width } = Dimensions.get('window');

class BackIcon extends React.PureComponent {
  render() {
    return (
      <TouchableCustom
        onPress={this.props.onPress} >
        <Icon
          name='arrow-back'
          color='#419FFF' />
      </TouchableCustom>
    )
  }
}

export default class PageTwo extends React.Component {
  constructor() {
    super()
    this.state = {
      resData: [], //接口返回的数据
      produceComp: {},
      isMediaVisible: false,
      paperUrl: '',
      hidImgUrl: '',
      finImgUrl: '',
      mediaType: '',
      imgUrlList: [],
      isLoading: true,
    }
    this.requestData = this.requestData.bind(this);
  }

  //TopBar
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('sonTitle')
    }
  }

  //执行函数
  componentDidMount() {
    const { navigation } = this.props;
    const url = navigation.getParam('sonUrl');
    const faUrl = navigation.getParam('url');
    console.log("🚀 ~ file: ChildrenPageTwo.js ~ line 75 ~ PageTwo ~ componentDidMount ~ faUrl", faUrl)
    if (faUrl.indexOf("PostWaitingPoolCompPage") == -1) {
      this.requestData();
    } else {
      this.setState({
        isLoading: false
      })
    }

  }

  //接口数据请求，子页面要求新的链接
  requestData = () => {
    //将子页面链接提取
    const { navigation } = this.props;
    const url = navigation.getParam('sonUrl');
    const faUrl = navigation.getParam('url');
    //const url = "https://smart.pkpm.cn:910/api/Produce/6181220600594C28A97724501F5AB975/GetProjectProducedComponentsSingle/";
    console.log("🚀 ~ file: ChildrenPageTwo.js ~ line 59 ~ PageTwo ~ url", url)
    //请求数据
    fetch(url, {
      credentials: 'include',
    }).then(res => {
      return res.json()
    }).then(resData => {
      console.log("🚀 ~ file: ChildrenPageTwo.js ~ line 86 ~ PageTwo ~ fetch ~ resData", resData)
      if (url.indexOf("GetProjectProducedComponentsSingle") != -1) {
        this.setState({ produceComp: resData[0] })
      }
      this.setState({
        resData: resData,
        isLoading: false
      });
    }).catch((error) => {
      console.log(error);
    });
  }

  //长列表头部
  _header = (item) => {
    return (
      <View>
        <View style={{ justifyContent: 'center', alignItems: 'center', borderRadius: 12, marginTop: 8 }}>
          <LinearGradient style={{ width: width - 26, borderRadius: 5 }} colors={['#97BBFF', '#419FFF',]}>

            {/* 成品入库头部 */}
            <View style={item.roomName ? { flex: 1 } : { flex: 0, height: 0 }}>
              <Text style={styles.topbartext}>表单编号：{item.formCode}</Text>
              <Text style={styles.topbartext}>库房库位：{item.roomName} </Text>
              <Text style={styles.topbartext}>制单人：{item.stopeople}</Text>
              <Text style={styles.topbartext}>入库日期：{item.stotime}</Text>
              <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                <View style={{ flexDirection: 'column' }}>
                  <Text style={styles.topbartopcenter}>{item.compNumber}件</Text>
                  <Text style={styles.topbarbottomcenter}>构件数量</Text>
                </View>
                <View style={{ flexDirection: 'column' }}>
                  <Text style={styles.topbartopcenter}>{item.compVolume}m³</Text>
                  <Text style={styles.topbarbottomcenter}>总方量</Text>
                </View>
              </View>
            </View>

            {/* 成品出库头部 */}
            <View style={item.carnumbe ? { flex: 1, flexDirection: 'column' } : { flex: 0, height: 0 }}>
              <View style={{ flexDirection: 'column' }}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ flexDirection: 'column', width: width / 1.3 }}>
                    <Text style={styles.topbartext}>计划单号：{item.planformCode}</Text>
                    <Text style={styles.topbartext}>运单编号：{item.formCode} </Text>
                    <Text style={styles.topbartext}>项目名称：{item.projectName}</Text>
                    <Text style={styles.topbartext}>楼号楼层：{item.buildingNo}</Text>
                    <Text style={styles.topbartext}>计划发货日期：{item.planLibTime}</Text>
                    <Text style={styles.topbartext}>实际发货日期：{item.libTime}</Text>
                  </View>
                  <Button
                    titleStyle={{ color: 'white', fontSize: 16 }}
                    title={item.state}
                    type='clear' />
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                  <View style={{ flexDirection: 'column' }}>
                    <Text style={styles.topbartopcenter}>{item.compNumber}件</Text>
                    <Text style={styles.topbarbottomcenter}>构件数量</Text>
                  </View>
                  <View style={{ flexDirection: 'column' }}>
                    <Text style={styles.topbartopcenter}>{item.compVolume}m³</Text>
                    <Text style={styles.topbarbottomcenter}>总方量</Text>
                  </View>
                </View>
              </View>

            </View>
          </LinearGradient>
        </View>

        <View style={item.carnumbe ? { flex: 1, flexDirection: 'column' } : { flex: 0, height: 0 }}>
          <View style={{ marginTop: 12 }}>
            <Text style={{ color: 'gray', fontSize: 16, marginHorizontal: 16 }}>客户信息</Text>
          </View>
          <Card containerStyle={{ borderRadius: 20 }}>
            <View style={styles.textView}>
              <Text style={styles.textname}>客户名称：</Text>
              <Text style={[styles.text]}>{item.customer} </Text>
            </View>
          </Card>

          <View style={{ marginTop: 12 }}>
            <Text style={{ color: 'gray', fontSize: 16, marginHorizontal: 16 }}>车辆信息</Text>
          </View>
          <Card containerStyle={{ borderRadius: 20 }}>
            <View style={styles.textView}>
              <Text style={styles.textname}>车牌号码：</Text>
              <Text style={[styles.text]}>{item.carnumbe} </Text>
            </View>
            <View style={styles.textView}>
              <Text style={styles.textname}>司机姓名：</Text>
              <Text style={[styles.text]}>{item.driver} </Text>
            </View>
          </Card>
        </View>

      </View >
    )
  }

  //刷新和加载
  refreshing() {
    let timer = setTimeout(() => {
      clearTimeout(timer)
      alert('刷新成功')
    }, 1500)
  }

  _onload() {
    let timer = setTimeout(() => {
      clearTimeout(timer)
      console.log('加载成功')
    }, 1500)
  }

  //构件报废详情注解
  remark = (item) => {
    return (
      <View style={{ flex: 3 }} >
        <View style={{ marginTop: 12 }}>
          <Text style={{ color: 'gray', fontSize: 16, marginHorizontal: 16 }}>报废原因</Text>
        </View>
        <Card>
          <Text> {item.remark} </Text>
        </Card>
      </View>
    )
  }

  pdfSourse = (item) => {
    if (item.pdfUrl == null) {
      return
    } else {
      return (
        <View style={{ flex: 1 }}>
          <View style={{ marginTop: 12 }}>
            <Text style={{ color: 'gray', fontSize: 14, marginHorizontal: 16 }}>构件图纸</Text>
          </View>
          <View style={styles.listView} >
            <View style={{ justifyContent: 'center', alignContent: 'center', paddingVertical: 14 }}>
              <Pdf
                source={{
                  uri: Url.urlsys + '/' + item.pdfUrl,
                  //method: 'GET', //默认 'GET'，请求 url 的方式
                }}
                fitWidth={true} //默认 false，若为 true 则不能将 fitWidth = true 与 scale 一起使用
                fitPolicy={0} // 0:宽度对齐，1：高度对齐，2：适合两者（默认）
                page={1}
                //scale={1}
                onLoadComplete={(numberOfPages, filePath, width, height, tableContents) => {
                  console.log(`number of pages: ${numberOfPages}`); //总页数
                  console.log(`number of filePath: ${filePath}`); //本地返回的路径
                  console.log(`number of width: `, JSON.stringify(width));
                  console.log(`number of height: ${JSON.stringify(height)}`);
                  console.log(`number of tableContents: ${tableContents}`);
                }}
                onError={(error) => {
                  console.log(error);
                }}
                minScale={1} //最小模块
                maxScale={3}

                enablePaging={true} //在屏幕上只能显示一页
                style={{
                  backgroundColor: "white",
                  flex: 1,
                  height: width - 80,
                  width: width - 60,
                  marginLeft: width * 0.03
                }}
              />
              {/*  <Image
                source={{ uri: Url.urlsys + '/' + item.hidImgUrl }}
                style={{ width: width - 80, height: width - 80, marginLeft: width * 0.08, justifyContent: 'center', alignContent: 'center', }}
                PlaceholderContent={<ActivityIndicator color={'#419fff'} />} /> */}
            </View>
          </View>
        </View>
      )
    }
  }

  //构件图片函数，因为需要隐藏，所以独立出来
  hidImgSourse = (item) => {
    console.log("🚀 ~ file: ChildrenPageTwo.js ~ line 220 ~ PageTwo ~ Url.Imgurl + item.hidImgUrl", Url.urlsys + '/' + item.hidImgUrl)

    if (item.hidImgUrl == null) {
      return
    } else {
      return (
        <View style={{ flex: 1 }}>
          <View style={{ marginTop: 12 }}>
            <Text style={{ color: 'gray', fontSize: 14, marginHorizontal: 16 }}>隐检照片</Text>
          </View>
          <View style={styles.listView} >
            <View style={{ justifyContent: 'center', alignContent: 'center', paddingVertical: 14 }}>
              <Image
                source={{ uri: Url.urlsys + '/' + item.hidImgUrl }}
                style={{ width: width - 80, height: width - 80, marginLeft: width * 0.08, justifyContent: 'center', alignContent: 'center', }}
                PlaceholderContent={<ActivityIndicator color={'#419fff'} />} />
            </View>
          </View>
        </View>
      )
    }

  }

  //构件报废图片，或者其他？？图片？
  scrapImgSourse = (item) => {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ marginTop: 12 }}>
          <Text style={{ color: 'gray', fontSize: 16, marginHorizontal: 16 }}>报废构件信息</Text>
        </View>
        <Card >
          <View style={{ justifyContent: 'center', alignContent: 'center' }}>
            <Image
              source={{ uri: Url.Imgurl + item.compImgUrl }}
              style={{ width: width - 60, height: width - 60, justifyContent: 'center', alignContent: 'center', }}
              PlaceholderContent={<ActivityIndicator />} />
            <Image
              source={{ uri: Url.Imgurl + item.reportImgUrl }}
              style={{ width: width - 60, height: width - 60, justifyContent: 'center', alignContent: 'center', }}
              PlaceholderContent={<ActivityIndicator />} />
          </View>
        </Card>
      </View>
    )
  }

  finImgSourse = (item) => {
    if (item.finImgUrl == null) {
      return
    } else {
      return (
        <View style={{ flex: 1 }}>
          <View style={{ marginTop: 12 }}>
            <Text style={{ color: 'gray', fontSize: 14, marginHorizontal: 16 }}>成检照片</Text>
          </View>
          <View style={styles.listView} >
            <View style={{ justifyContent: 'center', alignContent: 'center', paddingVertical: 14 }}>
              <Image
                source={{ uri: Url.urlsys + '/' + item.finImgUrl }}
                style={{ width: width - 80, height: width - 80, marginLeft: width * 0.08, justifyContent: 'center', alignContent: 'center', }}
                PlaceholderContent={<ActivityIndicator color={'#419fff'} />} />
            </View>
          </View>
        </View>
      )
    }

  }

  //日期安排子卡片
  CardSource2 = (item) => {
    console.log(123)
    let hidImgUrlList = []
    let finImgUrlList = []
    let finReportImgIDUrlList = []
    if (!!item.hidImgUrl) {
      hidImgUrlList = item.hidImgUrl.split(",");
    }
    if (!!item.finImgUrl) {
      finImgUrlList = item.finImgUrl.split(",");
    }

    if (!!item.finReportImgIDUrl) {
      finReportImgIDUrlList = item.finReportImgIDUrl.split(",");
    }


    console.log("🚀 ~ file: ChildrenPageTwo.js ~ line 344 ~ PageTwo ~ finImgUrlList", finImgUrlList)
    console.log("🚀 ~ file: ChildrenPageTwo.js ~ line 343 ~ PageTwo ~ hidImgUrlList", hidImgUrlList)
    return (
      <View>
        <View style={styles.listView} >
          {
            item.jhtime != null ? <ListItemTwoThree titlename="计划生产：" title={item.jhtime} /> : <View></View>
          }
          {
            item.sjtime != null ? <ListItemTwoThree titlename="开始生产：" title={item.sjtime} /> : <View></View>
          }
          {
            item.putMoldTime != null ? <ListItemTwoThree titlename="钢筋笼入模：" title={item.putMoldTime} righttitle={item.putMoldName} /> : <View></View>
          }
          {
            item.hidTime != null ? <ListItemTwoThree titlename="隐蔽检查：" title={item.hidTime} righttitle={item.hidEmpName} /> : <View></View>
          }

          {item.hidImgUrl != null ? <ListItemTwoThree titlename="隐检照片：" title={" 点击查看"} color={"#419FFF"} onPress={() => {
            this.setState({
              isMediaVisible: true,
              mediaType: 'hidimg',
              paperUrl: Url.urlsys + '/' + item.hidImgUrl,
              imgUrlList: hidImgUrlList
            })
          }} /> : <View></View>}

          {
            item.demouldTime != null ? <ListItemTwoThree titlename="脱模待检：" title={item.demouldTime} righttitle={item.demouldName} /> : <View></View>
          }
          {
            item.finChkTime != null ? <ListItemTwoThree titlename="成品检验：" title={item.finChkTime} righttitle={item.finChkEmpName} /> : <View></View>
          }

          {item.finImgUrl != null ? <ListItemTwoThree titlename="成检照片：" title={" 点击查看"} color={"#419FFF"} onPress={() => {
            this.setState({
              isMediaVisible: true,
              mediaType: 'finimg',
              paperUrl: Url.urlsys + '/' + item.finImgUrl,
              imgUrlList: finImgUrlList
            })
          }} /> : <View></View>}

          {item.finReportImgIDUrl != null ? <ListItemTwoThree titlename="检查报告照片：" title={" 点击查看"} color={"#419FFF"} onPress={() => {
            this.setState({
              isMediaVisible: true,
              mediaType: 'finreportimg',
              paperUrl: "",
              imgUrlList: finReportImgIDUrlList
            })
          }} /> : <View></View>}

          {
            item.reFinChkTime != null ? <ListItemTwoThree titlename="成品复检：" title={item.reFinChkTime} righttitle={item.reFinChkEmpName} /> : <View></View>
          }
          {
            item.warehouTime != null ? <ListItemTwoThree titlename="成品入库：" title={item.warehouTime} righttitle={item.warehouEmpName} /> : <View></View>
          }
          {
            item.warehouRomName != null ? <ListItemTwoThree titlename="库房库位：" title={item.warehouRomName + '(' + item.warehouLibName + ')'} /> : <View></View>
          }
          {
            item.loaoutTime != null ? <ListItemTwoThree titlename="成品出库：" title={item.loaoutTime} righttitle={item.loaoutEmpName} /> : <View></View>
          }
          {
            item.unloadTime != null ? <ListItemTwoThree titlename="成品卸车：" title={item.unloadTime} /> : <View></View>
          }
          {
            item.installTime != null ? <ListItemTwoThree titlename="成品安装：" title={item.installTime} /> : <View></View>
          }

        </View>
      </View>

    )
  }

  //顶部蓝栏
  TopBar = (item) => {
    const url = this.props.navigation.getParam('url');
    const sonTitle = this.props.navigation.getParam('sonTitle');
    //计划生产日期
    if (url.indexOf("PostProduceTaskPage") != -1) {
      return (
        <LinearGradient style={{ width: width - 26, borderRadius: 5, marginTop: 8 }} colors={['#97BBFF', '#4AA4FE']}>
          <View style={item.planDate ? { flex: 1 } : { height: 0 }}>
            <Text style={[styles.topbartext, { marginTop: 14 }]}>任务编号：{item.formCode}</Text>
            <Text style={styles.topbartext}>计划生产日期：{item.planDate}</Text>
            <Text style={styles.topbartext}>生产线：{item.productLineName}</Text>
            <Text style={styles.topbartext}>劳务队：{item.labourName}</Text>
            <Text style={styles.topbartext}>负责人：{item.editEmployeeName}</Text>
            <View style={{ flexDirection: 'row', marginVertical: 8, backgroundColor: 'rgba(55,134,241)' }}>
              <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}>
                <Text style={styles.topbarnum}>{item.compNum}件</Text>
                <Text style={styles.topbarnum}>构件数量</Text>
              </View>
              <View style={{ flex: 1, borderLeftColor: 'white', borderLeftWidth: 0.5 }}>
                <Text style={styles.topbarnum}>{item.compTotalVolume}m³</Text>
                <Text style={styles.topbarnum}>总方量</Text>
              </View>
            </View>
          </View>
        </LinearGradient >
      )

    }
    else if (url.indexOf("PostStockLoaoutWarePage") != -1) {
      return (
        <LinearGradient style={{ width: width - 26, borderRadius: 5, marginTop: 8 }} colors={['#97BBFF', '#4AA4FE']}>

          <Text style={[styles.topbartext, { marginTop: 14 }]}>计划单号：{item.planformCode}</Text>
          <Text style={styles.topbartext}>运单编号：{item.formCode}</Text>
          <Text style={styles.topbartext}>项目名称：{item.projectName}</Text>
          <Text style={styles.topbartext}>楼号楼层：{item.buildingNo}</Text>
          <Text style={styles.topbartext}>客户名称：{item.customer}</Text>
          {
            sonTitle.indexOf('运单详情') == -1 ?
              <View>
                <Text style={styles.topbartext}>车辆号牌：{item.carnumbe}</Text>
                <Text style={styles.topbartext}>司机姓名：{item.driver}</Text>
              </View>
              :
              <View></View>
          }
          <Text style={styles.topbartext}>计划发货时间：{item.planLibTime}</Text>
          <Text style={styles.topbartext}>实际发货时间：{item.libTime}</Text>
          <View style={{ flexDirection: 'row', marginVertical: 8, backgroundColor: 'rgba(55,134,241)' }}>
            <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}>
              <Text style={styles.topbarnum}>{item.compNumber}件</Text>
              <Text style={styles.topbarnum}>构件数量</Text>
            </View>
            <View style={{ flex: 1, borderLeftColor: 'white', borderLeftWidth: 0.5 }}>
              <Text style={styles.topbarnum}>{item.compVolume}m³</Text>
              <Text style={styles.topbarnum}>总方量</Text>
            </View>
          </View>
        </LinearGradient >
      )

    } //计划生产日期
    // else if (item.planDate) {
    //   return (
    //     <LinearGradient style={{ width: width - 26, borderRadius: 5, marginTop: 8 }} colors={['#97BBFF', '#419FFF',]}>
    //       <View style={item.planDate ? { flex: 1 } : { height: 0 }}>
    //         <Text style={styles.topbartext}>任务编号：{item.compCode}</Text>
    //         <Text style={styles.topbartext}>计划生产日期：{item.planDate}</Text>
    //       </View>
    //     </LinearGradient >
    //   )
    // } // 剩余信息都在这里，还需要优化
    else if (item.produceDate || item.releaseDate) {
      return (
        <LinearGradient style={{ width: width - 26, borderRadius: 5, marginTop: 8 }} colors={['#97BBFF', '#419FFF',]}>
          {/* 操作人与生产日期，有就显示，没有不显示，很多界面通用 */}
          <View style={item.produceDate || item.releaseDate ? { flex: 1 } : styles.hidden}>
            <Text style={styles.topbartext}>操作人：{item.warehouEmpName ? item.warehouEmpName : (item.releaseName ? item.releaseName : item.editEmployeeName || "")}</Text>
            <Text style={styles.topbartext}>生产日期：{item.produceDate ? item.produceDate : item.releaseDate}</Text>
          </View>
          {/* 负责人班组长质检员，日生产记录页面 */}
          <View style={item.castingTime && item.editEmployeeName ? { flex: 1 } : styles.hidden}>
            <Text style={styles.topbartext}>负责人：{item.editEmployeeName}</Text>
            <Text style={styles.topbartext}>班组名称：{item.teamName}</Text>
            <Text style={styles.topbartext}>质检员：{item.member}</Text>
            <Text style={styles.topbartext}>生产日期：{item.produceDate}</Text>
          </View>
          {/* 报废孙子页面，使用报废详情说明判断 */}
          <View style={item.operatorUser ? { flex: 1 } : styles.hidden}>
            <Text style={styles.topbartext}>报废单号：{item.compCode}</Text>
            <Text style={styles.topbartext}>报废来源：</Text>
            <Text style={styles.topbartext}>经办人：{item.operatorUser}</Text>
            <Text style={styles.topbartext}>报废日期：{item.scrapTime}</Text>
          </View>
          {/* 成品检查页面头部，可能和成品复检通用。 */}
          <View style={item.finChkTime ? { flex: 1 } : styles.hidden}>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ flexDirection: 'column', width: width / 1.5 }}>
                <Text style={styles.topbartext}>质检员：{item.member}</Text>
                <Text style={styles.topbartext}>成检日期：{item.finChkTime}</Text>
              </View>
              <Button
                titleStyle={{ color: 'white', fontSize: 18 }}
                containerStyle={{ justifyContent: 'space-around', alignContent: 'flex-end' }}
                title={item.insresults}
                icon={<Badge
                  status={item.insresults == '合格' ? 'success' : null}
                  containerStyle={{ marginHorizontal: 4 }}
                />}
                iconContainerStyle={{ marginHorizontal: 4 }}
                type='clear' />
            </View>
          </View>
        </LinearGradient>
      )
    }
  }

  //分隔栏
  _separator = () => {
    return <View style={{ height: 2, backgroundColor: '#fffffc' }} />;
  }

  CardMain = (item) => {
    console.log("🚀 ~ file: ChildrenPageTwo.js ~ line 572 ~ PageTwo ~ item", item)
    return (
      <View>
        {/* 构件类型 */}
        <View style={{ marginTop: 12 }}>
          <Text style={{ color: 'gray', fontSize: 14, marginHorizontal: 16 }}>{item.compTypeName}</Text>
        </View>
        <View style={styles.listView} >
          <ListItemTwo titlename="项目：" title={item.projectName} color={"#419FFF"} />
          <ListItemTwo titlename="产品编号：" title={item.compCode} />
          <ListItemTwo titlename="设计型号：" title={item.designType} />
          <ListItemTwo titlename="类型：" title={item.compTypeName} />
          <ListItemTwo titlename="子型号：" title={""} />
          <ListItemTwo titlename="版本号：" title={item.versionName} />
          <ListItemTwo titlename="楼号：" title={item.floorName} />
          <ListItemTwo titlename="层号：" title={item.floorNoName} />
          <ListItemTwo titlename="体积：" title={item.volume + 'm³'} />
          <ListItemTwo titlename="重量：" title={item.weight + '吨'} />
          <ListItemTwo titlename="砼强度：" title={item.concreteGrade} bottomDivider={item.productLineName ? true : false} />
          {/* 需不需要显示生产日期和生产厂家 */}
          {/* <View style={item.releaseDate ? { flex: 1 } : { height: 0, flex: 0 }}>
            <ListItemTwo titlename="生产日期：" title={item.releaseDate} bottomDivider={item.releaseDate ? true : false} />
            <ListItemTwo titlename="生产厂家：" title={item.factoryName} bottomDivider={item.releaseDate ? true : false} />
          </View> */}
          {/* 有没有生产线信息 */}
          {
            item.productLineName ? <View >
              <ListItemTwo titlename="生产线：" title={item.productLineName} />
              <ListItemTwo titlename="专用模具：" title={item.spetaiWanCode} />
              <ListItemTwo titlename="模台：" title={item.taiWanName} />
              <ListItemTwo titlename="钢筋笼编号：" title={item.steelCageCode} />
              <ListItemTwo titlename="承包队：" title={item.labourName} />
              <ListItemTwo titlename="生产厂家：" title={item.labourName} />
              <ListItemTwo titlename="合格证：" title={""} />
            </View> : <View></View>
          }

          {item.pdfUrl != null ? <ListItemTwo titlename="构件图纸：" title={" 点击查看"} color={"#419FFF"} onPress={() => {
            this.setState({
              isMediaVisible: true,
              mediaType: 'paper',
              paperUrl: Url.urlsys + '/' + item.pdfUrl
            })
          }} /> : <View></View>}

        </View>

      </View>

    )
  }

  CardList = () => {
    const { resData } = this.state
    return (
      <View style={{ marginTop: 10 }}>
        <Text style={{ color: '#535c68', fontSize: RFT * 3.6, paddingLeft: 15 }}>构件列表</Text>
        {
          resData.map((item, index) => {
            return (
              <View style={styles.listView} >
                <ListItemTwo titlename="项目：" title={item.projectName} color={"#419FFF"} />
                <ListItemTwo titlename="类型：" title={item.compTypeName} />
                <ListItemTwo titlename="型号：" title={item.designType} />
                <ListItemTwo titlename="子型号：" title={""} />
                <ListItemTwo titlename="版本号：" title={item.versionName} />
                <ListItemTwo titlename="楼号：" title={item.floorName} />
                <ListItemTwo titlename="层号：" title={item.floorNoName} />
                <ListItemTwo titlename="体积：" title={item.concretrVolume} />
                <ListItemTwo titlename="重量：" title={item.weight} />
                <ListItemTwo titlename="生产数量：" title={item.compNum} bottomDivider={false} />
              </View>
            )
          })
        }

      </View>
    )
  }

  CarditemTask = (compitem) => {
    const { resData } = this.state
    return (
      <View style={{ marginTop: 10 }}>
        <Text style={{ color: '#535c68', fontSize: RFT * 3.6, paddingLeft: 15 }}>构件列表</Text>
        {
          <View style={styles.listView} >
            <ListItemTwo titlename="构件编码：" title={compitem.compCode} color={"#419FFF"} />
            <ListItemTwo titlename="构件类型：" title={compitem.compTypeName} />
            <ListItemTwo titlename="项目名称：" title={compitem.projectName} />
            <ListItemTwo titlename="设计型号：" title={compitem.designType} />
            <ListItemTwo titlename="子型号：" title={""} />
            <ListItemTwo titlename="版本号：" title={compitem.versionName} />
            <ListItemTwo titlename="楼号：" title={compitem.floorName} />
            <ListItemTwo titlename="层号：" title={compitem.floorNoName} />
            <ListItemTwo titlename="体积：" title={compitem.concretrVolume} />
            <ListItemTwo titlename="重量：" title={compitem.weight} />
            <ListItemTwo titlename="长：" title={compitem.length} />
            <ListItemTwo titlename="宽：" title={compitem.width} />
            <ListItemTwo titlename="高：" title={compitem.heihgt} />
          </View>

        }

      </View>
    )
  }

  CardListTaskState = (itemmain) => {
    console.log("🚀 ~ file: ChildrenPageTwo.js ~ line 681 ~ PageTwo ~ itemmain", itemmain)
    const { resData } = this.state
    let resDataNew = []
    resData.map((item, index) => {
      if (item.state == 14) {
        resDataNew.splice(3, 0, item)

      } else {
        resDataNew.push(item)
      }
    })
    return (
      <View style={{ marginTop: 10 }}>
        <Text style={{ color: '#535c68', fontSize: RFT * 3.6, paddingLeft: 15 }}>构件列表</Text>
        <View style={styles.listView} >
          {resDataNew.map((item, index) => {
            item.color = color[index]
            return (
              <ListItem
                title={item.statecn + ':'}
                titleStyle={{ fontSize: 15 }}
                rightTitle={item.compNum + '件' + '(' + item.compVol + 'm³)'}
                //titleStyle={styles.text}
                rightTitleStyle={{ width: width / 2, textAlign: 'right', fontSize: 15 }}
                bottomDivider
                chevron
                //contentContainerStyle={styles.content}
                containerStyle={styles.ListItem_container}
                leftElement={<Badge badgeStyle={{ backgroundColor: item.color, top: 0 }} />}
                onPress={() => {
                  this.props.navigation.push('ChildOne', {
                    title: '任务单',
                    sonTitle: '构件详情',
                    length: item.compNum,
                    item: itemmain,
                    state: item.state,
                    taskid: item.taskid,
                    sonUrl: Url.url + Url.Produce + '/PostProduceTaskCompStatelistPage',
                    url: Url.url + Url.Produce + '/PostProduceTaskCompStatelistPage'
                  })
                }}
              // chevron
              />
            )
          })}
        </View>
      </View>
    )
  }

  CardListLoaout = (itemmain) => {
    const { resData } = this.state
    return (
      <View style={{ marginTop: 10 }}>
        {/* <View>
          <Text style={{ color: '#535c68', fontSize: RFT * 3.6, paddingLeft: 15 }}>客户信息</Text>
          {

            <View style={styles.listView} >
              <ListItemTwo titlename="客户名称：" title={itemmain.customer} bottomDivider={false} />
            </View>

          }
        </View>
        <View style={{ marginTop: 15 }}>
          <Text style={{ color: '#535c68', fontSize: RFT * 3.6, paddingLeft: 15 }}>车辆信息</Text>
          {

            <View style={styles.listView} >
              <ListItemTwo titlename="车辆号牌：" title={itemmain.carnumbe} bottomDivider={false} />
              <ListItemTwo titlename="司机姓名：" title={itemmain.driver} bottomDivider={false} />
            </View>

          }
        </View> */}
        <View style={{ marginTop: 15 }}>
          <Text style={{ color: '#535c68', fontSize: RFT * 3.6, paddingLeft: 15 }}>构件列表</Text>
          <FlatList
            data={resData}
            renderItem={this.LoaoutItem} />
          {/* 
            resData.map((item, index) => {
              return (
                <View style={styles.listView} >
                  <ListItemTwo titlename="" title={item.compCode} color={"#419FFF"} bottomDivider={false} />
                  <ListItemTwo titlename="" title={item.compTypeName} bottomDivider={false} />
                  <ListItemTwo titlename="" title={item.designType} bottomDivider={false} />
                  <ListItemTwo titlename="" title={item.floorNoName + " " + item.floorName} bottomDivider={false} />
                  <ListItemTwo titlename="" title={item.volume + "m³" + " " + item.weight + "吨"} bottomDivider={false} />
                </View>
              )

            })
           */}
        </View>
      </View>
    )
  }

  LoaoutItem = ({ item, index }) => {
    return (
      <View style={styles.listView} >
        <ListItemTwo titlename="" title={item.compCode} color={"#419FFF"} bottomDivider={false} />
        <ListItemTwo titlename="" title={item.compTypeName} bottomDivider={false} />
        <ListItemTwo titlename="" title={item.designType} bottomDivider={false} />
        <ListItemTwo titlename="" title={item.floorNoName + " " + item.floorName} bottomDivider={false} />
        <ListItemTwo titlename="" title={item.volume + "m³" + " " + item.weight + "吨"} bottomDivider={false} />
      </View>
    )
  }

  mediaTypeJudge = () => {
    const { mediaType, paperUrl, imgUrlList } = this.state;
    if (mediaType == 'paper') {
      return (
        <Pdf
          source={{
            uri: this.state.paperUrl,
            //method: 'GET', //默认 'GET'，请求 url 的方式
          }}
          fitWidth={true} //默认 false，若为 true 则不能将 fitWidth = true 与 scale 一起使用
          fitPolicy={0} // 0:宽度对齐，1：高度对齐，2：适合两者（默认）
          page={1}
          //scale={1}
          onLoadComplete={(numberOfPages, filePath, width, height, tableContents) => {
            console.log(`number of pages: ${numberOfPages}`); //总页数
            console.log(`number of filePath: ${filePath}`); //本地返回的路径
            console.log(`number of width: `, JSON.stringify(width));
            console.log(`number of height: ${JSON.stringify(height)}`);
            console.log(`number of tableContents: ${tableContents}`);
          }}
          //activityIndicator = {{color: '#419FFF'}}
          onError={(error) => {
            console.log(error);
          }}
          minScale={1} //最小模块
          maxScale={3}
          enablePaging={true} //在屏幕上只能显示一页
          style={{
            flex: 1,
            //height: height * 0.9,
            width: width
          }}
        />
      )
    } else if (mediaType == 'hidimg' || mediaType == 'finimg') {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}>
          <ScrollView>
            {
              this.state.imgUrlList.map((item, index) => {
                if (item.length > 0) {
                  return (
                    <Image
                      source={{ uri: Url.urlsys + '/' + item }}
                      resizeMethod='resize'
                      resizeMode='contain'
                      style={{ height: height * 0.8 }}

                    />
                  )
                }
              })
            }
          </ScrollView>
        </View>
      )
    } else {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}>
          {
            this.state.imgUrlList.map((item, index) => {
              if (item.length > 0) {
                if (item.indexOf('.pdf') != -1) {
                  let itemArr = item.split("+");
                  console.log("🚀 ~ file: ChildrenPageTwo.js ~ line 841 ~ PageTwo ~ this.state.imgUrlList.map ~ Url.urlsys + '/sys/common/FileDownload.aspx?fileid=' + itemArr[0]", Url.urlsys + '/sys/common/FileDownload.aspx?fileid=' + itemArr[0])
                  return (
                    <Pdf
                      source={{
                        uri: Url.urlsys + '/sys/common/FileDownload.aspx?fileid=' + itemArr[0],
                        //method: 'GET', //默认 'GET'，请求 url 的方式
                      }}
                      fitWidth={true} //默认 false，若为 true 则不能将 fitWidth = true 与 scale 一起使用
                      fitPolicy={0} // 0:宽度对齐，1：高度对齐，2：适合两者（默认）
                      page={1}
                      //scale={1}
                      onLoadComplete={(numberOfPages, filePath, width, height, tableContents) => {
                        console.log(`number of pages: ${numberOfPages}`); //总页数
                        console.log(`number of filePath: ${filePath}`); //本地返回的路径
                        console.log(`number of width: `, JSON.stringify(width));
                        console.log(`number of height: ${JSON.stringify(height)}`);
                        console.log(`number of tableContents: ${tableContents}`);
                      }}
                      //activityIndicator = {{color: '#419FFF'}}
                      onError={(error) => {
                        console.log(error);
                      }}
                      minScale={1} //最小模块
                      maxScale={3}
                      enablePaging={true} //在屏幕上只能显示一页
                      style={{
                        flex: 1,
                        //height: height * 0.9,
                        width: width
                      }}
                    />
                  )
                } else {
                  let itemArr = item.split("+");

                  return (
                    <Image
                      source={{ uri: Url.urlsys + '/' + itemArr[1] }}
                      resizeMethod='resize'
                      resizeMode='contain'
                      style={{ height: height * 0.8 }}

                    />
                  )
                }
              }

            })
          }
        </View>
      )


    }
  }

  //主页面渲染
  render() {
    //获取孙子数据
    const { navigation } = this.props;
    const sonTitle = navigation.getParam('sonTitle')
    console.log("🚀 ~ file: ChildrenPageTwo.js ~ line 916 ~ PageTwo ~ render ~ sonTitle", sonTitle)
    const item = navigation.getParam('item')
    console.log("🚀 ~ file: ChildrenPageTwo.js ~ line 900 ~ PageTwo ~ render ~ item", item)
    const url = navigation.getParam('url');
    console.log("🚀 ~ file: ChildrenPageTwo.js ~ line 921 ~ PageTwo ~ render ~ url", url)
    const sonurl = navigation.getParam('sonUrl');
    console.log("🚀 ~ file: ChildrenPageTwo.js ~ line 919 ~ PageTwo ~ render ~ sonurl", sonurl)
    const stateurl = navigation.getParam('stateUrl');
    const compitem = navigation.getParam('compitem') || {};
    const { mediaType } = this.state;
    let mediaTypeTitle = ''
    if (mediaType == 'paper') {
      mediaTypeTitle = '图纸预览'
    } else if (mediaType == 'hidimg') {
      mediaTypeTitle = '隐检图片预览'
    } else if (mediaType == 'finimg') {
      mediaTypeTitle = '成检图片预览'
    } else {
      mediaTypeTitle = '检查报告图片预览'
    }
    // 正常通用界面
    if (this.state.isLoading) {
      return (
        <View style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#F5FCFF',
        }}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )

    } else {
      return (
        <View style={{ backgroundColor: '#f9f9f9' }}>
          <ScrollView showsVerticalScrollIndicator={false}>
            {/* 顶部组件 */}
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', borderRadius: 12 }}>
              {this.TopBar(item)}
            </View>
            {/* 构件基本信息，大概所有页面通用 */}
            <View style={{ flex: 3 }}>

              {sonurl.indexOf("GetProduceTaskCompDetails") != -1 ? this.CarditemTask(compitem) : <View></View>}

              {/*  {sonurl.indexOf("GetProduceTaskCompDetails") != -1 ? this.CardList(compitem) : <View></View>} */}

              {sonurl.indexOf("GetProduceTaskCompState") != -1 ? this.CardListTaskState(item) : <View></View>}

              {sonurl.indexOf("GetStockLoaoutWareCompDetails") != -1 ? this.CardListLoaout(item) : <View></View>}

              {sonurl.indexOf("GetProjectProducedComponentsSingle") != -1 && url.indexOf("Pool") == -1 ? this.CardMain(this.state.produceComp) : <View></View>}

              {sonurl.indexOf("GetProduceTaskCompDetails") == -1 && sonurl.indexOf("GetStockLoaoutWareCompDetails") == -1 && sonurl.indexOf("GetProduceTaskCompState") == -1 && sonurl.indexOf("GetProjectProducedComponentsSingle") == -1 ? this.CardMain(item) : <View></View>}

              {url.indexOf("Pool") != -1 ? this.CardMain(item) : <View></View>}
            </View>

            {/* 判断是否显示构件报废记录 */}
            {item.remark ? this.remark(item) : <View></View>}

            {/* 构件图片是否显示 */}
            {/*  <View style={item.imgUrl ? { flex: 4 } : { height: 0 }}>
            {item.imgUrl ?  : <View></View>}
          </View> */}

            {/*
            {sonurl.indexOf("GetProjectProducedComponentsSingle") != -1 ? this.hidImgSourse(this.state.produceComp) : <View></View>}

            
            {sonurl.indexOf("GetProjectProducedComponentsSingle") != -1 ? this.hidImgSourse(this.state.produceComp) : <View></View>}

            {sonurl.indexOf("GetProjectProducedComponentsSingle") != -1 ? this.finImgSourse(this.state.produceComp) : <View></View>}

            {sonurl.indexOf("GetProjectProducedComponentsSingle") != -1 ? this.pdfSourse(this.state.produceComp) : <View></View>}
            */}
            {/* 构件报废图片，但是看情况要改，具体需要看情况 */}
            {/* <View style={item.remark ? { flex: 4 } : { height: 0 }}>
            {item.compImgUrl ? this.scrapImgSourse(item) : <View></View>}
          </View> */}

            {/*  成品检查构件图片  */}
            {/* <View style={url.indexOf('GetQualityFinProducts') != -1 || url.search('GetQualityFinProducts') != -1 ? { flex: 4 } : { height: 0 }}>
            {url.search('GetQualityFinProducts') != -1 || url.search('GetQualityFinProducts') != -1 ? this.FinProductsImgSourse(item) : <View></View>}
          </View> */}

            {/* 子卡片2，控制是否显示 */}
            <View style={{ flex: 4 }}>
              {
                sonurl.indexOf("GetProjectProducedComponentsSingle") != -1  && url.indexOf("Pool") == -1 ?
                  <View>
                    <View style={{ marginTop: 12 }}>
                      <Text style={{ color: 'gray', fontSize: 16, marginHorizontal: 16 }}>生产过程记录</Text>
                    </View>
                    <View style={{ flex: 3 }}>
                      {
                        sonurl.indexOf("GetProjectProducedComponentsSingle") != -1 ? this.CardSource2(this.state.produceComp) : this.CardSource2(item)
                      }
                    </View>
                  </View> : <View></View>
              }

            </View>
            <View style={{ height: 30 }}></View>

            <Overlay
              fullScreen={true}
              animationType='fade'
              isVisible={this.state.isMediaVisible}
              onRequestClose={() => {
                this.setState({ isMediaVisible: !this.state.isMediaVisible });
              }} >
              <Header
                containerStyle={{ height: 45, paddingTop: 0, top: -5 }}
                centerComponent={{ text: mediaTypeTitle, style: { color: 'gray', fontSize: 20 } }}
                rightComponent={<BackIcon
                  onPress={() => {
                    this.setState({
                      isMediaVisible: !this.state.isMediaVisible
                    });
                  }} />}
                backgroundColor='white'
              />
              <View style={{ flex: 1 }}>
                {this.mediaTypeJudge()}
              </View>
            </Overlay>
          </ScrollView>
        </View>
      )
    }

  }
}

const styles = StyleSheet.create({
  contrain: {
    flex: 4,
    flexDirection: 'column',
    //alignContent: 'center',
    //alignItems: 'center',
    //justifyContent: 'center',
    borderRadius: 15
  },
  text2: {
    flex: 1,
    fontSize: 14,
    marginEnd: 12,
    marginBottom: 12,
    color: '#454545'
    //fontFamily:'STKaiti',
    //fontWeight: 'bold',
  },
  text: {
    flex: 2,
    fontSize: 15,
    marginEnd: 12,
    marginBottom: 12,
    color: '#454545'
    //fontFamily:'STKaiti',
    //fontWeight: 'bold',
  },
  textname: {
    flex: 1.3,
    fontSize: 14,
    marginEnd: 12,
    marginBottom: 12,
    color: 'gray'
  },
  topbartext: {
    flex: 1,
    fontSize: 14,
    fontWeight: "300",
    marginTop: 8,
    marginBottom: 8,
    marginHorizontal: 10,
    color: 'white'
  },
  topbarnum: {
    fontSize: 14,
    fontWeight: "300",
    marginBottom: 8,
    marginHorizontal: 10,
    color: 'white',
    textAlign: "center"
  },
  topbartopcenter: {
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
    color: 'white',
    marginTop: 4,
    marginBottom: 4
  },
  topbarbottomcenter: {
    textAlign: 'center',
    fontSize: 14,
    color: 'white',
    marginTop: 4,
    marginBottom: 4
  },
  CardTitle: {
    fontSize: 20,
  },
  textView: {
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  hidden: {
    flex: 0,
    height: 0,
    width: 0,
  },
  listView: {
    marginHorizontal: width * 0.02,
    marginTop: 10,
    paddingHorizontal: 0,
    paddingTop: 5,
    paddingBottom: 0,
    backgroundColor: 'white',
    borderRadius: 10,
  },
  ListItem_container: {
    paddingVertical: 14,
  }
})