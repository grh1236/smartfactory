import React from 'react'
import { View, TouchableOpacity, TouchableWithoutFeedback, StyleSheet, FlatList, ActivityIndicator, Platform, ScrollView, Dimensions, StatusBar } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, Slider } from 'react-native-elements';
import Url from '../../../Url/Url';
import { RFT, deviceWidth, deviceHeight } from '../../../Url/Pixal';
import Video from 'react-native-video';
//import Slider from '@react-native-community/slider';
import Orientation from '@emanon_/react-native-orientation';
import { SafeAreaView } from 'react-navigation';

const screenWidth = Dimensions.get('window').width;

formatTime = (second) => {
  let h = 0, i = 0, s = parseInt(second);
  if (s > 60) {
    i = parseInt(s / 60);
    s = parseInt(s % 60);
  }
  // 补零
  let zero = function (v) {
    return (v >> 0) < 10 ? "0" + v : v;
  };
  return [zero(h), zero(i), zero(s)].join(":");
}

export default class landspaceVideo extends React.Component {
  constructor() {
    super();
    this.state = {
      data: [],
      isLoading: true,

      videoWidth: screenWidth,
      videoHeight: screenWidth * 9 / 16, // 默认16：9的宽高比
      showVideoCover: true,    // 是否显示视频封面
      showVideoControl: false, // 是否显示视频控制组件
      isPlaying: false,        // 视频是否正在播放
      currentTime: 0,        // 视频当前播放的时间
      duration: 0,           // 视频的总时长
      isFullScreen: false,     // 当前是否全屏显示
      playFromBeginning: false, // 是否从头开始播放
    }
    this.changePausedState = this.changePausedState.bind(this);
    this.customerSliderValue = this.customerSliderValue.bind(this);
    this._changePauseSliderFullState = this._changePauseSliderFullState.bind(this);
    this._onStartShouldSetResponder = this._onStartShouldSetResponder.bind(this);
    this.requestData = this.requestData.bind(this);
  }

  static navigationOptions = ({ navigation }) => {
    return {
      header: null
    }
  }

  componentWillMount() {
    Orientation.lockToLandscape();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
    this._navListener.remove();
  }

  requestData = () => {
    const url = 'https://smart.pkpm.cn:910/api/User/PKPM/GetDEMOVideo'
    const urltest = "https://raw.githubusercontent.com/facebook/react-native/0.51-stable/docs/MoviesExample.json";
    const category = this.props.navigation.getParam('category');
    let categoryData = [];
    fetch(url)
      .then((res) => res.json())
      .then((resData) => {
        resData.map((item, index) => {
          if (category == item.category) {
            categoryData.push(item);
          }
        })
        this.setState({
          data: categoryData,
          isLoading: false
        });
      }).catch(e => console.log(e));
  }

  changePausedState = () => { //控制按钮显示播放，要显示进度条3秒钟，之后关闭显示
    this.setState({
      isPaused: this.state.isPaused ? false : true,
      isVisiblePausedSliderFullScreen: true
    })
    //这个定时调用失去了this指向
    let that = this;
    setTimeout(() => {
      that.setState({
        isVisiblePausedSliderFullScreen: false
      })
    }, 30000)
  }

  _changePauseSliderFullState = () => { // 单击事件，是否显示 “暂停、进度条、全屏按钮 盒子”
    let flag = this.state.isVisiblePausedSliderFullScreen ? false : true;
    this.setState({
      isVisiblePausedSliderFullScreen: flag
    })
    //这个定时调用失去了this指向
    let that = this;
    setTimeout(() => {
      that.setState({
        isVisiblePausedSliderFullScreen: false
      })
    }, 30000)
  }

  _onStartShouldSetResponder(e) {
    console.log(e);
  }

  //格式化音乐播放的时间为0：00。借助onProgress的定时器调用，更新当前时间
  formatMediaTime(time) {
    let minute = Math.floor(time / 60);
    let second = parseInt(time - minute * 60);
    minute = minute >= 10 ? minute : "0" + minute;
    second = second >= 10 ? second : "0" + second;
    return minute + ":" + second;

  }

  //加载视频调用，主要是拿到 “总时间”，并格式化
  customerOnload(e) {
    let time = e.duration;
    this.setState({
      duration: time
    })
  }

  // 获得当前的，播放时间数，但这个数是0.104，需要处理
  customerOnprogress(e) {
    let time = e.currentTime;   // 获取播放视频的秒数       
    this.setState({
      currentTime: time,
      sliderValue: time
    })
  }

  // 移动滑块，改变视频播放进度
  customerSliderValue(value) {
    this.player.seek(value);
  }


  componentDidMount() {
    this.requestData();
    this._navListener = this.props.navigation.addListener('didFocus', () => {
      //StatusBar.setBarStyle('dark-content');
      StatusBar.setBackgroundColor('#419FFF');
    });
  }

  render1() {
    const { navigation } = this.props;
    const videopath = navigation.getParam('videopath');
    const viedoname = navigation.getParam('viedoname');
    console.log(RFT)
    let playButtonComponent = (
      <TouchableWithoutFeedback
        onPress={this.changePausedState}
      >
        <View style={styles.playBtn}>
        </View>
      </TouchableWithoutFeedback>
    );

    let pausedBtn = this.state.isPaused ? playButtonComponent : null;

    // 暂停按钮、进度条、全屏按钮 是否显示
    let pausedSliderFullComponent = (
      <View style={{ position: "absolute", bottom: 0 }}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          {/* 进度条按钮  */}
          <View style={styles.sliderBox}>
            <View >
              <Icon
                containerStyle={{ marginLeft: 3 }}
                type='antdesign'
                name={this.state.isPaused ? 'pause' : 'caretright'}
                size={RFT * 5}
                color='white'
                onPress={() => {
                  this.setState({
                    isPaused: !this.state.isPaused
                  })
                }}
              ></Icon>
            </View>
            <Text style={{ color: 'white', fontSize: RFT * 2.5, marginLeft: 6 }}>{this.formatMediaTime(this.state.currentTime)}</Text>
            <Slider
              style={{ marginLeft: 10, width: height * 0.85, height: 40 }}
              value={this.state.sliderValue}
              maximumValue={this.state.duration}
              thumbTintColor="#fff" //开关夹点的yanse              
              minimumTrackTintColor="red"
              maximumTrackTintColor="#ccc"
              step={1}
              onValueChange={this.customerSliderValue}
            />
            <Text style={{ color: 'white', fontSize: RFT * 2.5 }}>{this.formatMediaTime(this.state.duration)}</Text>
          </View>
          {/* 全屏按钮 */}



        </View>
      </View>
    );

    let pausedSliderFull = this.state.isVisiblePausedSliderFullScreen ? pausedSliderFullComponent : null;

    return (
      <SafeAreaView>
        <StatusBar backgroundColor={'black'} />
        <View >
          <TouchableWithoutFeedback
            onPress={this._changePauseSliderFullState}
            onResponderMove={this._onStartShouldSetResponder}
          >
            <Video source={{ uri: videopath }}
              ref={(ref) => {
                this.player = ref
              }}
              onBuffer={() => {
                return (
                  <View style={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    // backgroundColor: '#F5FCFF',
                  }}>
                    <ActivityIndicator
                      animating={true}
                      color='#419FFF'
                      size="large" />
                  </View>
                )
              }}
              style={{ backgroundColor: 'black', width: height + 20, height: width - RFT - 30 }}
              allowsExternalPlayback={false} // 不允许导出 或 其他播放器播放
              paused={this.state.isPaused} // 控制视频是否播放
              resizeMode="cover"
              onLoad={(e) => this.customerOnload(e)}
              onProgress={(e) => this.customerOnprogress(e)}
              fullscreenAutorotate={true}
              fullscreenOrientation='landscape'
              fullscreen={true}
              repeat={true}
            />
          </TouchableWithoutFeedback>
          {/* 播放的按钮：点击之后需要消失 */}
          {pausedBtn}
          {/* 暂停按钮，进度条，全屏按钮 */}
          {pausedSliderFull}
        </View>


      </SafeAreaView>

    )
  }

  render() {
    const { navigation } = this.props;
    const videopath = navigation.getParam('videopath');
    const viedoname = navigation.getParam('viedoname');
    return (
      <View style={styles.container} onLayout={this._onLayout}>
        <View style={{ width: this.state.videoWidth, height: this.state.videoHeight, backgroundColor: '#000000' }}>
          <Video
            source={{ uri: videopath }}
            rate={1.0}
            volume={1.0}
            muted={false}
            paused={!this.state.isPlaying}
            resizeMode={'contain'}
            playWhenInactive={false}
            playInBackground={false}
            ignoreSilentSwitch={'ignore'}
            progressUpdateInterval={250.0}
            onLoadStart={this._onLoadStart}
            onLoad={this._onLoaded}
            onProgress={this._onProgressChanged}
            onEnd={this._onPlayEnd}
            onError={this._onPlayError}
            onBuffer={this._onBuffering}
            style={{ width: this.state.videoWidth, height: this.state.videoHeight }}
          ></Video>
          {
            this.state.showVideoCover ?
              <Image
                style={{
                  position: 'absolute',
                  top: 0,
                  left: 0,
                  width: this.state.videoWidth,
                  height: this.state.videoHeight
                }}
                resizeMode={'cover'}
                source={require('../../../viewImg/black.jpeg')}
              /> : null
          }<TouchableWithoutFeedback onPress={() => { this.hideControl() }}>
            <View
              style={{
                position: 'absolute',
                top: 0,
                left: 0,
                width: this.state.videoWidth,
                height: this.state.videoHeight,
                backgroundColor: this.state.isPlaying ? 'transparent' : 'rgba(0, 0, 0, 0.2)',
                alignItems: 'center',
                justifyContent: 'center'
              }}>
              {
                this.state.isPlaying ? null :
                  <TouchableWithoutFeedback onPress={() => { this.onPressPlayButton() }}>
                    <Icon
                      style={styles.playButton}
                      type = 'antdesign'
                      name = 'caretright'
                      size={RFT * 5}
                      color='white'
                    />
                  </TouchableWithoutFeedback>
              }
            </View>
          </TouchableWithoutFeedback>
          {
            this.state.showVideoControl ?
              <View style={[styles.control, { width: this.state.videoWidth }]}>
                <TouchableOpacity activeOpacity={0.3} onPress={() => { this.onControlPlayPress() }}>
                  <Icon
                    style={styles.playControl}
                    type = 'antdesign'
                    name = {this.state.isPlaying ? 'pause' : 'caretright'} 
                    size={RFT * 5}
                    color='white'
                      />
                </TouchableOpacity>
                <Text style={styles.time}>{formatTime(this.state.currentTime)}</Text>
                <Slider
                  style={{ flex: 1 }}
                  maximumTrackTintColor={'#999999'}
                  minimumTrackTintColor={'#00c06d'}
                  value={this.state.currentTime}
                  minimumValue={0}
                  maximumValue={this.state.duration}
                  onValueChange={(currentTime) => { this.onSliderValueChanged(currentTime) }}
                />
                <Text style={styles.time}>{formatTime(this.state.duration)}</Text>
                <TouchableOpacity activeOpacity={0.3} onPress={() => { this.onControlShrinkPress() }}>
                 <Text style = {{color: 'white'}} >全屏</Text>
                </TouchableOpacity>
              </View> : null
          }
        </View>
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <Button title={'开始播放'} onPress={() => { this.playVideo() }} />
          <Button title={'暂停播放'} onPress={() => { this.pauseVideo() }} />


        </View>
      </View>

    )
  }

  /// -------Video组件回调事件-------

  _onLoadStart = () => {
    console.log('视频开始加载');
  };

  _onBuffering = () => {
    console.log('视频缓冲中...')
  };

  _onLoaded = (data) => {
    console.log('视频加载完成');
    this.setState({
      duration: data.duration,
    });
  };

  _onProgressChanged = (data) => {
    console.log('视频进度更新');
    if (this.state.isPlaying) {
      this.setState({
        currentTime: data.currentTime,
      })
    }
  };

  _onPlayEnd = () => {
    console.log('视频播放结束');
    this.setState({
      currentTime: 0,
      isPlaying: false,
      playFromBeginning: true
    });
  };

  _onPlayError = () => {
    console.log('视频播放失败');
  };

  ///-------控件点击事件-------

  /// 控制播放器工具栏的显示和隐藏
  hideControl() {
    if (this.state.showVideoControl) {
      this.setState({
        showVideoControl: false,
      })
    } else {
      this.setState(
        {
          showVideoControl: true,
        },
        // 5秒后自动隐藏工具栏
        () => {
          setTimeout(
            () => {
              this.setState({
                showVideoControl: false
              })
            }, 5000
          )
        }
      )
    }
  }

  /// 点击了播放器正中间的播放按钮
  onPressPlayButton() {
    let isPlay = !this.state.isPlaying;
    this.setState({
      isPlaying: isPlay,
      showVideoCover: false
    });
    if (this.state.playFromBeginning) {
      this.videoPlayer.seek(0);
      this.setState({
        playFromBeginning: false,
      })
    }
  }

  /// 点击了工具栏上的播放按钮
  onControlPlayPress() {
    this.onPressPlayButton();
  }

  /// 点击了工具栏上的全屏按钮
  onControlShrinkPress() {
    if (this.state.isFullScreen) {
      Orientation.lockToPortrait();
    } else {
      Orientation.lockToLandscape();
    }
  }

  /// 进度条值改变
  onSliderValueChanged(currentTime) {
    this.videoPlayer.seek(currentTime);
    if (this.state.isPlaying) {
      this.setState({
        currentTime: currentTime
      })
    } else {
      this.setState({
        currentTime: currentTime,
        isPlaying: true,
        showVideoCover: false
      })
    }
  }

  /// 屏幕旋转时宽高会发生变化，可以在onLayout的方法中做处理，比监听屏幕旋转更加及时获取宽高变化
  _onLayout = (event) => {
    //获取根View的宽高
    let { width, height } = event.nativeEvent.layout;
    console.log('通过onLayout得到的宽度：' + width);
    console.log('通过onLayout得到的高度：' + height);

    // 一般设备横屏下都是宽大于高，这里可以用这个来判断横竖屏
    let isLandscape = (width > height);
    if (isLandscape) {
      this.setState({
        videoWidth: width,
        videoHeight: height,
        isFullScreen: true,
      })
    } else {
      this.setState({
        videoWidth: width,
        videoHeight: width * 9 / 16,
        isFullScreen: false,
      })
    }
    Orientation.unlockAllOrientations();
  };

  /// -------外部调用事件方法-------

  ///播放视频，提供给外部调用
  playVideo() {
    this.setState({
      isPlaying: true,
      showVideoCover: false
    })
  }

  /// 暂停播放，提供给外部调用
  pauseVideo() {
    this.setState({
      isPlaying: false,
    })
  }

  /// 切换视频并可以指定视频开始播放的时间，提供给外部调用
  switchVideo(videoURL, seekTime) {
    this.setState({
      videoUrl: videoURL,
      currentTime: seekTime,
      isPlaying: true,
      showVideoCover: false
    });
    this.videoPlayer.seek(seekTime);
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f0f0f0'
  },
  playButton: {
    width: 50,
    height: 50,
  },
  playControl: {
    width: 24,
    height: 24,
    marginLeft: 15,
  },
  shrinkControl: {
    width: 15,
    height: 15,
    marginRight: 15,
  },
  time: {
    fontSize: 12,
    color: 'white',
    marginLeft: 10,
    marginRight: 10
  },
  control: {
    flexDirection: 'row',
    height: 44,
    alignItems:'center',
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    position: 'absolute',
    bottom: 0,
    left: 0
  },
})