import React from 'react'
import { View, TouchableOpacity, TouchableWithoutFeedback, StyleSheet, FlatList, ActivityIndicator, Platform, ScrollView } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, Slider } from 'react-native-elements';
import Url from '../../../Url/Url';
import { RFT, deviceWidth, deviceHeight } from '../../../Url/Pixal';
import Video from 'react-native-video';
//import Slider from '@react-native-community/slider';
import Orientation from '@emanon_/react-native-orientation';


export default class videoPage extends React.Component {
  constructor() {
    super();
    this.state = {
      data: [],
      isLoading: true,

      isPaused: true,  //是暂停
      duration: 0,      //总时长
      currentTime: 0, //当前播放时间
      sliderValue: 0,   //进度条的进度 

      //用来控制进入全屏的属性
      videoWidth: deviceWidth,
      videoHeight: 226,
      isFullScreen: false,
      isVisiblePausedSliderFullScreen: false
    }
    this.changePausedState = this.changePausedState.bind(this);
    this.customerSliderValue = this.customerSliderValue.bind(this);
    this.enterFullScreen = this.enterFullScreen.bind(this);
    this._changePauseSliderFullState = this._changePauseSliderFullState.bind(this);
    this._onStartShouldSetResponder = this._onStartShouldSetResponder.bind(this);
    this.requestData = this.requestData.bind(this);
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('viedoname'),

    }
  }



  requestData = () => {
    const url = 'https://smart.pkpm.cn:910/api/User/PKPM/GetDEMOVideo'
    const urltest = "https://raw.githubusercontent.com/facebook/react-native/0.51-stable/docs/MoviesExample.json";
    const category = this.props.navigation.getParam('category');
    let categoryData = [];
    fetch(url)
      .then((res) => res.json())
      .then((resData) => {
        resData.map((item, index) => {
          if (category == item.category) {
            categoryData.push(item);
          }
        })
        this.setState({
          data: categoryData,
          isLoading: false
        });
      }).catch(e => console.log(e));
  }

  changePausedState = () => { //控制按钮显示播放，要显示进度条3秒钟，之后关闭显示
    this.setState({
      isPaused: this.state.isPaused ? false : true,
      isVisiblePausedSliderFullScreen: true
    })
    //这个定时调用失去了this指向
    let that = this;
    setTimeout(() => {
      that.setState({
        isVisiblePausedSliderFullScreen: false
      })
    }, 30000)
  }

  _changePauseSliderFullState = () =>  { // 单击事件，是否显示 “暂停、进度条、全屏按钮 盒子”
    let flag = this.state.isVisiblePausedSliderFullScreen ? false : true;
    this.setState({
      isVisiblePausedSliderFullScreen: flag
    })
    //这个定时调用失去了this指向
    let that = this;
    setTimeout(() =>  {
      that.setState({
        isVisiblePausedSliderFullScreen: false
      })
    }, 30000)
  }

  //格式化音乐播放的时间为0：00。借助onProgress的定时器调用，更新当前时间
  formatMediaTime(time) {
    let minute = Math.floor(time / 60);
    let second = parseInt(time - minute * 60);
    minute = minute >= 10 ? minute : "0" + minute;
    second = second >= 10 ? second : "0" + second;
    return minute + ":" + second;

  }

  //加载视频调用，主要是拿到 “总时间”，并格式化
  customerOnload(e) {
    let time = e.duration;
    this.setState({
      duration: time
    })
  }

  // 获得当前的，播放时间数，但这个数是0.104，需要处理
  customerOnprogress(e) {
    let time = e.currentTime;   // 获取播放视频的秒数       
    this.setState({
      currentTime: time,
      sliderValue: time
    })
  }

  // 移动滑块，改变视频播放进度
  customerSliderValue(value) {
    this.player.seek(value);
  }

  enterFullScreen() { //1.改变宽高  2.允许进入全屏模式  3.如何配置屏幕旋转,不需要改变进度条盒子的显示和隐藏
    this.setState({
      // videoWidth: deviceHeight,
      //videoHeight: deviceWidth,
      isFullScreen: !this.state.isFullScreen
    })
    // 直接设置方向
    //Orientation.lockToLandscape();
  }
  _onStartShouldSetResponder(e) {
    console.log(e);
  }

  componentDidMount() {
    var initial = Orientation.getInitialOrientation();
    if (initial === 'PORTRAIT') {
      console.log('是竖屏');
    } else {
      console.log('如果是横屏，就将其旋转过来');
      Orientation.lockToPortrait();
    }
    this.requestData();
  }

  render() {
    const { navigation } = this.props;
    const videopath = navigation.getParam('videopath');
    const viedoname = navigation.getParam('viedoname');
    const category = navigation.getParam('category');
    let playButtonComponent = (
      <TouchableWithoutFeedback
        onPress={this.changePausedState}
      >
        <View style={styles.playBtn}>
        </View>
      </TouchableWithoutFeedback>
    );

    let pausedBtn = this.state.isPaused ? playButtonComponent : null;

    // 暂停按钮、进度条、全屏按钮 是否显示
    let pausedSliderFullComponent = (
      <View style={{ position: "absolute", bottom: 0 }}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          {/* 进度条按钮  */}
          <View style={styles.sliderBox}>
            <View >
              <Icon
                containerStyle={{ marginLeft: 3 }}
                type='antdesign'
                name={this.state.isPaused ? 'pause' : 'caretright'}
                size={RFT * 5}
                color='white'
                onPress={() => {
                  this.setState({
                    isPaused: !this.state.isPaused
                  })
                }}
              ></Icon>
            </View>
            <Text style={{ color: 'white', fontSize: RFT * 2.5, marginLeft: 6 }}>{this.formatMediaTime(this.state.currentTime)}</Text>
            <Slider
              style={{ width: deviceWidth * 0.68, height: 40 }}
              value={this.state.sliderValue}
              maximumValue={this.state.duration}
              thumbTintColor="#fff" //开关夹点的yanse              
              minimumTrackTintColor="red"
              maximumTrackTintColor="#ccc"
              step={1}
              onValueChange={this.customerSliderValue}
            />
            <Text style={{ color: 'white', fontSize: RFT * 2.5 }}>{this.formatMediaTime(this.state.duration)}</Text>
          </View>
          {/* 全屏按钮 */}
          <View>
            <TouchableOpacity
              onPress={
                //this.enterFullScreen
                () => {
                  this.setState({
                    isPaused: true
                  })
                  this.props.navigation.navigate('landspaceVideo', {
                    videopath: videopath,
                    viedoname: viedoname
                  })
                }

              }
            >
              <Text style={{ color: 'white', margin: 5, marginLeft: 8 }}>全屏</Text>
            </TouchableOpacity>
          </View>


        </View>
      </View>
    );

    let pausedSliderFull = this.state.isVisiblePausedSliderFullScreen ? pausedSliderFullComponent : null;

    return (
      <ScrollView>
        <Text style={{ color: '#535c68', fontSize: 20, marginTop: 18, marginLeft: 18 }}>{viedoname}</Text>
        <View style={{ marginVertical: 14 }}>
          <TouchableWithoutFeedback
            onPress={this._changePauseSliderFullState}
            onResponderMove={this._onStartShouldSetResponder}
          >
            <Video source={{ uri: videopath }}
              ref={(ref) => {
                this.player = ref
              }}
              onBuffer={() => {
                return (
                  <View style={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    // backgroundColor: '#F5FCFF',
                  }}>
                    <ActivityIndicator
                      animating={true}
                      color='#419FFF'
                      size="large" />
                  </View>
                )
              }}
              style={{ width: this.state.videoWidth, height: this.state.videoHeight, backgroundColor: 'black' }}
              allowsExternalPlayback={false} // 不允许导出 或 其他播放器播放
              paused={this.state.isPaused} // 控制视频是否播放
              resizeMode="cover"
              onLoad={(e) => this.customerOnload(e)}
              onProgress={(e) => this.customerOnprogress(e)}
              fullscreenAutorotate={true}
              fullscreenOrientation='landscape'
              fullscreen={this.state.isFullScreen}
              repeat={true}
            />
          </TouchableWithoutFeedback>
          {/* 播放的按钮：点击之后需要消失 */}
          {pausedBtn}
          {/* 暂停按钮，进度条，全屏按钮 */}
          {pausedSliderFull}
        </View>


        <Button buttonStyle={{
          marginRight: 30,
          borderRadius: 50,
          width: 70,
          height: 36,
          marginLeft: 18,
        }}
          containerStyle={{
            borderRadius: 1,
          }}
          title={category}
          titleStyle={{ fontFamily: 'STHeitiSC-Light', fontSize: RFT * 3.2 }}
          type='outline'></Button>

        <ScrollView style={{ width: deviceWidth * 0.8 }}>
          <FlatList
            data={this.state.data}
            renderItem={this._renderItem}
            style={styles.list}
          //keyExtractor={item => item.id}
          />
        </ScrollView>
      </ScrollView>

    )
  }

  _renderItem = ({ item, index }) => {
    const { navigation } = this.props;
    return (
      <View>
        <TouchableOpacity
          onPress={() => {
            navigation.push('VideoPage', {
              videopath: 'http://smart.pkpm.cn:82/' + item.videopath,
              viedoname: item.viedoname,
              category: item.category,
            })
          }} >
          <View
            style={styles.container}
          >
            <View style={{ borderRadius: 20 }}>
              <Image
                source={require('../../../viewImg/video.png')}
                style={styles.thumbnail}
                resizeMethod='scale'
                resizeMode='contain'
              />
            </View>
            <View style={styles.rightContainer}>
              <Text style={styles.title}>{item.viedoname}</Text>
              <Text style={styles.year}>{item.category}</Text>
            </View>

          </View>
        </TouchableOpacity>
      </View >
    );
  }

}

const styles = StyleSheet.create({
  backgroundVideo: {
    position: 'absolute',
    color: 'red',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  myVideo: {
    width: 340,
    height: 240
  },
  playBtn: {
    width: 50,
    height: 50,
    backgroundColor: 'red',
    borderRadius: 50,
    position: "absolute",
    top: "50%",
    left: "50%",
    marginLeft: -25,
    marginTop: -25,
    zIndex: 999
  },
  sliderBox: {
    flex: 0,
    flexDirection: 'row',
    alignItems: 'center'
  },
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    //backgroundColor: '#F5FCFF',
  },
  thumbnail: {
    width: 64,
    height: 64,
    borderRadius: 20
  },
  rightContainer: {
    flex: 1,

  },
  title: {
    fontSize: 16,
    marginLeft: 6,
    textAlignVertical: 'center',
    //textAlign: 'center',
  },
  year: {
    marginLeft: 6,
    marginTop: 5,
    fontSize: 12
    //textAlign: 'center',
  },
  list: {
    paddingTop: 14,
    width: deviceWidth * 0.9,
    marginLeft: deviceWidth * 0.05
    //backgroundColor: '#F5FCFF',
  },
})