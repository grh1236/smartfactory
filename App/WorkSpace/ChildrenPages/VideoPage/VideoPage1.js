import React from 'react'
import { View, TouchableOpacity, TouchableWithoutFeedback, StyleSheet, FlatList, ActivityIndicator, Platform, ScrollView, Dimensions } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, Slider } from 'react-native-elements';
import Url from '../../../Url/Url';
import { RFT, deviceWidth, deviceHeight } from '../../../Url/Pixal';
import Video from 'react-native-video';
//import Slider from '@react-native-community/slider';
import Orientation from '@emanon_/react-native-orientation';

const screenWidth = Dimensions.get('window').width;

formatTime = (second) => {
  let h = 0, i = 0, s = parseInt(second);
  if (s > 60) {
    i = parseInt(s / 60);
    s = parseInt(s % 60);
  }
  // 补零
  let zero = function (v) {
    return (v >> 0) < 10 ? "0" + v : v;
  };
  return [zero(h), zero(i), zero(s)].join(":");
}

export default class videoPage extends React.Component {
  constructor() {
    super();
    this.state = {
      data: [],
      isLoading: true,

      isBuffering: false,

      videoWidth: screenWidth,
      videoHeight: screenWidth * 9 / 16, // 默认16：9的宽高比
      showVideoCover: true,    // 是否显示视频封面
      showVideoControl: false, // 是否显示视频控制组件
      isPlaying: false,        // 视频是否正在播放
      currentTime: 0,        // 视频当前播放的时间
      duration: 0,           // 视频的总时长
      isFullScreen: false,     // 当前是否全屏显示
      playFromBeginning: false, // 是否从头开始播放
    }

    this.requestData = this.requestData.bind(this);
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('viedoname'),
      header: null
    }
  }



  requestData = () => {
    const url = 'https://smart.pkpm.cn:910/api/User/PKPM/GetDEMOVideo'
    const urltest = "https://raw.githubusercontent.com/facebook/react-native/0.51-stable/docs/MoviesExample.json";
    const category = this.props.navigation.getParam('category');
    let categoryData = [];
    fetch(url)
      .then((res) => res.json())
      .then((resData) => {
        resData.map((item, index) => {
          if (category == item.category) {
            categoryData.push(item);
          }
        })
        this.setState({
          data: categoryData,
          isLoading: false
        });
      }).catch(e => console.log(e));
  }

  componentDidMount() {

    this.requestData();
  }

  render() {
    const { navigation } = this.props;
    const videopath = navigation.getParam('videopath');
    const viedoname = navigation.getParam('viedoname');
    const category = navigation.getParam('category');
    
    return (
      <ScrollView onLayout={this._onLayout}>
        {/* <Text style={{ color: '#535c68', fontSize: 20, marginTop: 18, marginLeft: 18 }}>{viedoname}</Text> */}

        <View style={{ width: this.state.videoWidth, height: this.state.videoHeight, backgroundColor: '#000000' }}>
          <Video
            ref={(ref) => this.videoPlayer = ref}
            source={{ uri: videopath }}
            rate={1.0}
            volume={1.0}
            muted={false}
            paused={!this.state.isPlaying}
            resizeMode={'contain'}
            playWhenInactive={false}
            playInBackground={false}
            ignoreSilentSwitch={'ignore'}
            progressUpdateInterval={1000.0}
            onLoadStart={this._onLoadStart}
            onLoad={this._onLoaded}
            onProgress={this._onProgressChanged}
            onEnd={this._onPlayEnd}
            onError={this._onPlayError}
            onBuffer={this._onBuffering}
            style={{ width: this.state.videoWidth, height: this.state.videoHeight }}
          ></Video>
          {
            this.state.isBuffering ?
              <View style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                // backgroundColor: '#F5FCFF',
              }}>
                <ActivityIndicator
                  animating={true}
                  color='#419FFF'
                  size="large" />
              </View> : null
          }<TouchableWithoutFeedback onPress={() => { this.hideControl() }}>
            <View
              style={{
                position: 'absolute',
                top: 0,
                left: 0,
                width: this.state.videoWidth,
                height: this.state.videoHeight,
                backgroundColor: this.state.isPlaying ? 'transparent' : 'rgba(0, 0, 0, 0.2)',
                alignItems: 'center',
                justifyContent: 'center'
              }}>
              {
                this.state.isPlaying ? null :
                  <TouchableWithoutFeedback onPress={() => { this.onPressPlayButton() }}>
                    <Icon
                      style={styles.playButton}
                      type='antdesign'
                      name='caretright'
                      size={RFT * 5}
                      color='white'
                    />
                  </TouchableWithoutFeedback>
              }
            </View>
          </TouchableWithoutFeedback>
          {
            this.state.showVideoControl ?
              <View style={[styles.control, { width: this.state.videoWidth }]}>
                <TouchableOpacity activeOpacity={0.3} onPress={() => { this.onControlPlayPress() }}>
                  <Icon
                    style={styles.playControl}
                    type='antdesign'
                    name={this.state.isPlaying ? 'pause' : 'caretright'}
                    size={RFT * 5}
                    color='white'
                  />
                </TouchableOpacity>
                <Text style={styles.time}>{formatTime(this.state.currentTime)}</Text>
                <Slider
                  style={{ flex: 1 }}
                  maximumTrackTintColor={'#999999'}
                  minimumTrackTintColor={'red'}
                  value={this.state.currentTime}
                  minimumValue={0}
                  maximumValue={this.state.duration}
                  onValueChange={(currentTime) => { this.onSliderValueChanged(currentTime) }}
                />
                <Text style={styles.time}>{formatTime(this.state.duration)}</Text>
                <TouchableOpacity activeOpacity={0.3} onPress={() => { this.onControlShrinkPress() }}>
                  <Text style={{ color: 'white' }} >全屏</Text>
                </TouchableOpacity>
              </View> : null
          }
        </View>

        <Text style={{ color: '#535c68', fontSize: 20, marginTop: 18, marginLeft: 18 }}>{viedoname}</Text>

        <Button buttonStyle={{
          marginRight: 30,
          borderRadius: 50,
          width: 70,
          height: 36,
          marginLeft: 18,
          marginTop: 18,
        }}
          containerStyle={{
            borderRadius: 1,
          }}
          title={category}
          titleStyle={{ fontFamily: 'STHeitiSC-Light', fontSize: RFT * 3.2 }}
          type='outline'></Button>

        <ScrollView style={{ width: deviceWidth * 0.8 }}>
          <FlatList
            data={this.state.data}
            renderItem={this._renderItem}
            style={styles.list}
          //keyExtractor={item => item.id}
          />
        </ScrollView>
      </ScrollView>

    )
  }

  _renderItem = ({ item, index }) => {
    const { navigation } = this.props;
    return (
      <View>
        <TouchableOpacity
          onPress={() => {
            this.pauseVideo();
            navigation.push('VideoPage', {
              videopath: 'http://smart.pkpm.cn:82/' + item.videopath,
              viedoname: item.viedoname,
              category: item.category,
            })
          }} >
          <View
            style={styles.container}
          >
            <View style={{ borderRadius: 20 }}>
              <Image
                source={require('../../../viewImg/video.png')}
                style={styles.thumbnail}
                resizeMethod='scale'
                resizeMode='contain'
              />
            </View>
            <View style={styles.rightContainer}>
              <Text style={styles.title}>{item.viedoname}</Text>
              <Text style={styles.year}>{item.category}</Text>
            </View>

          </View>
        </TouchableOpacity>
      </View >
    );
  }

  /// -------Video组件回调事件-------

  _onLoadStart = () => {
    console.log('视频开始加载');
  };

  _onBuffering = (isBuffering) => {
    this.setState({ isBuffering: isBuffering });
    console.log('加载中')
  }

  _onLoaded = (data) => {
    console.log('视频加载完成');
    
    this.setState({
      duration: data.duration,
    });
  };

  _onProgressChanged = (data) => {
    console.log('视频进度更新');
    if (this.state.isPlaying) {
      this.setState({
        currentTime: data.currentTime,
      })
    }
  };

  _onPlayEnd = () => {
    console.log('视频播放结束');
    this.setState({
      currentTime: 0,
      isPlaying: false,
      playFromBeginning: true
    });
  };

  _onPlayError = () => {
    console.log('视频播放失败');
  };

  ///-------控件点击事件-------

  /// 控制播放器工具栏的显示和隐藏
  hideControl() {
    if (this.state.showVideoControl) {
      this.setState({
        showVideoControl: false,
      })
    } else {
      this.setState(
        {
          showVideoControl: true,
        },
        // 5秒后自动隐藏工具栏
        () => {
          setTimeout(
            () => {
              this.setState({
                showVideoControl: false
              })
            }, 5000
          )
        }
      )
    }
  }

  /// 点击了播放器正中间的播放按钮
  onPressPlayButton() {
    let isPlay = !this.state.isPlaying;
    this.setState({
      isPlaying: isPlay,
      showVideoCover: false
    });
    if (this.state.playFromBeginning) {
      this.videoPlayer.seek(0);
      this.setState({
        playFromBeginning: false,
      })
    }
  }

  /// 点击了工具栏上的播放按钮
  onControlPlayPress() {
    this.onPressPlayButton();
  }

  /// 点击了工具栏上的全屏按钮
  onControlShrinkPress() {
    if (this.state.isFullScreen) {
      Orientation.lockToPortrait();
    } else {
      Orientation.lockToLandscape();
    }
  }

  /// 进度条值改变
  onSliderValueChanged(currentTime) {
    this.videoPlayer.seek(currentTime);
    if (this.state.isPlaying) {
      this.setState({
        currentTime: currentTime
      })
    } else {
      this.setState({
        currentTime: currentTime,
        isPlaying: true,
        showVideoCover: false
      })
    }
  }

  /// 屏幕旋转时宽高会发生变化，可以在onLayout的方法中做处理，比监听屏幕旋转更加及时获取宽高变化
  _onLayout = (event) => {
    //获取根View的宽高
    let { width, height } = event.nativeEvent.layout;
    console.log('通过onLayout得到的宽度：' + width);
    console.log('通过onLayout得到的高度：' + height);

    // 一般设备横屏下都是宽大于高，这里可以用这个来判断横竖屏
    let isLandscape = (width > height);
    if (isLandscape) {
      this.setState({
        videoWidth: width,
        videoHeight: height,
        isFullScreen: true,
      })
    } else {
      this.setState({
        videoWidth: width,
        videoHeight: width * 9 / 16,
        isFullScreen: false,
      })
    }
    Orientation.unlockAllOrientations();
  };

  /// -------外部调用事件方法-------

  ///播放视频，提供给外部调用
  playVideo() {
    this.setState({
      isPlaying: true,
      showVideoCover: false
    })
  }

  /// 暂停播放，提供给外部调用
  pauseVideo() {
    this.setState({
      isPlaying: false,
    })
  }

  /// 切换视频并可以指定视频开始播放的时间，提供给外部调用
  switchVideo(videoURL, seekTime) {
    this.setState({
      videoUrl: videoURL,
      currentTime: seekTime,
      isPlaying: true,
      showVideoCover: false
    });
    this.videoPlayer.seek(seekTime);
  }

}

const styles = StyleSheet.create({
  backgroundVideo: {
    position: 'absolute',
    color: 'red',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  myVideo: {
    width: 340,
    height: 240
  },
  playBtn: {
    width: 50,
    height: 50,
    backgroundColor: 'red',
    borderRadius: 50,
    position: "absolute",
    top: "50%",
    left: "50%",
    marginLeft: -25,
    marginTop: -25,
    zIndex: 999
  },
  sliderBox: {
    flex: 0,
    flexDirection: 'row',
    alignItems: 'center'
  },
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    //backgroundColor: '#F5FCFF',
  },
  thumbnail: {
    width: 64,
    height: 64,
    borderRadius: 20
  },
  rightContainer: {
    flex: 1,

  },
  title: {
    fontSize: 16,
    marginLeft: 6,
    textAlignVertical: 'center',
    //textAlign: 'center',
  },
  year: {
    marginLeft: 6,
    marginTop: 5,
    fontSize: 12
    //textAlign: 'center',
  },
  list: {
    paddingTop: 14,
    width: deviceWidth * 0.9,
    marginLeft: deviceWidth * 0.05
    //backgroundColor: '#F5FCFF',
  },

  playButton: {
    width: 50,
    height: 50,
  },
  playControl: {
    width: 24,
    height: 24,
    marginLeft: 15,
  },
  shrinkControl: {
    width: 15,
    height: 15,
    marginRight: 15,
  },
  time: {
    fontSize: 12,
    color: 'white',
    marginLeft: 10,
    marginRight: 10
  },
  control: {
    flexDirection: 'row',
    height: 44,
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    position: 'absolute',
    bottom: 0,
    left: 0
  },
})