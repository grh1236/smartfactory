import React from 'react'
import { View, TouchableOpacity, StyleSheet, FlatList, ActivityIndicator, Platform } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge } from 'react-native-elements';
import Url from '../../../Url/Url';
import { RFT, deviceWidth } from '../../../Url/Pixal';

let category = [];

class videoCategory extends React.Component {
  constructor() {
    super();
    this.state = {
      data: [],
      isLoading: true,
      isCategory: true,
    };
    this.requestData = this.requestData.bind(this);
  }

  componentDidMount() {
    this.requestData();
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: '视频分类'
    }
  }

  requestData = () => {
    const url = 'https://smart.pkpm.cn:910/api/User/PKPM/GetDEMOVideo'
    const urltest = 'https://smart.pkpm.cn:910/api/User/PKPM/GetDEMOVideo';

    fetch(url)
      .then((res) => res.json())
      .then((resData) => {
        this.setCategory(resData)
        this.setState({
          data: resData,
          isLoading: false
        });
        
        // 注意，这里使用了this关键字，为了保证this在调用时仍然指向当前组件，我们需要对其进行“绑定”操作

      }).catch(e => console.log(e));
  }

  setCategory = (data) => {
    let tmpcategory = '';
    data.map((item, index) => {
      if (tmpcategory != item.category && category.indexOf(item.category) == -1) {
        category.push(item.category)
        tmpcategory = item.category
      }
    })
  }

  componentWillUnmount() {
    category = [];
  }

  render() {
    const { isLoading } = this.state;
    if (isLoading) {
      return (
        <View style={styles.container}>
          <Text>
            正在加载视频列表……
        </Text>
        </View>
      )
    } else {
      return (
        <View>
          <FlatList
            data={category}
            renderItem={this._renderItem}
            style={styles.list}
          //keyExtractor={item => item.id}
          />
        </View>
      )
    }
  }

  _renderItem = ({ item, index }) => {
    const { navigation } = this.props;
    return (
      <View>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('VideoList', {
              category: item,
            })
          }} >
          <View
            style={styles.container}
          >
            <Image
              source={require('../../../viewImg/workspace.jpeg')}
              style={styles.thumbnail}
              resizeMethod='scale'
              resizeMode='contain'
              containerStyle = {{borderRadius: 20}}
            />
            <View style={styles.rightContainer}>
              <Text style={styles.title}>{item}</Text>
            </View>

          </View>
        </TouchableOpacity>
      </View >
    );
  }

}

export default videoCategory;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    //backgroundColor: '#F5FCFF',
  },
  thumbnail: {
    marginLeft: 8,
    width: 81,
    height: 64,
    borderRadius: 20,
    //backgroundColor: 'white'
  },
  rightContainer: {
    flex: 1,
   // borderRightWidth: 10,
    //borderRightColor: 'red'
  },
  title: {
    fontSize: 16,
    marginLeft: 6,
    textAlignVertical: 'center',
    //textAlign: 'center',
  },
  year: {
    marginLeft: 6
    //textAlign: 'center',
  },
  list: {
    paddingBottom: 20,
    //marginLeft: deviceWidth * 0.02,
    //width: deviceWidth * 0.96
    //backgroundColor: '#F5FCFF',
  },
});