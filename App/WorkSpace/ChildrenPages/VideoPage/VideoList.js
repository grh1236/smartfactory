import React from 'react'
import { View, TouchableOpacity, StyleSheet, FlatList, ActivityIndicator, Platform } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge } from 'react-native-elements';
import Url from '../../../Url/Url';
import { RFT, deviceWidth, deviceHeight } from '../../../Url/Pixal';
import { withNavigation } from 'react-navigation';

//import fakedata from './fakedata';
//const { navigation } = this.props;
let category = [];

class videoList extends React.Component {
  constructor() {
    super();
    this.state = {
      data: [],
      movies: {
        "id": "11494",
        "title": "Chain Reaction",
        "year": 1996,
        "mpaa_rating": "PG-13",
        "runtime": 106,
        "release_dates": {
          "theater": "1996-08-02",
          "dvd": "2001-05-22"
        },
        "ratings": {
          "critics_rating": "Rotten",
          "critics_score": 16,
          "audience_rating": "Spilled",
          "audience_score": 27
        },
        "synopsis": "",
        "posters": {
          "thumbnail": "http://resizing.flixster.com/DeLpPTAwX3O2LszOpeaMHjbzuAw=/53x77/dkpu1ddg7pbsk.cloudfront.net/movie/11/16/47/11164719_ori.jpg",
          "profile": "http://resizing.flixster.com/DeLpPTAwX3O2LszOpeaMHjbzuAw=/53x77/dkpu1ddg7pbsk.cloudfront.net/movie/11/16/47/11164719_ori.jpg",
          "detailed": "http://resizing.flixster.com/DeLpPTAwX3O2LszOpeaMHjbzuAw=/53x77/dkpu1ddg7pbsk.cloudfront.net/movie/11/16/47/11164719_ori.jpg",
          "original": "http://resizing.flixster.com/DeLpPTAwX3O2LszOpeaMHjbzuAw=/53x77/dkpu1ddg7pbsk.cloudfront.net/movie/11/16/47/11164719_ori.jpg"
        },
        "abridged_cast": [
          {
            "name": "Keanu Reeves",
            "id": "162654049",
            "characters": [
              "Eddie Kasalivich"
            ]
          },
          {
            "name": "Morgan Freeman",
            "id": "162652224",
            "characters": [
              "Paul Shannon"
            ]
          },
          {
            "name": "Rachel Weisz",
            "id": "162653682",
            "characters": [
              "Dr. Lily Sinclair"
            ]
          },
          {
            "name": "Fred Ward",
            "id": "162667867",
            "characters": [
              "Agt. Leon Ford"
            ]
          },
          {
            "name": "Kevin Dunn",
            "id": "162664658",
            "characters": [
              "Agt. Doyle"
            ]
          }
        ],
        "alternate_ids": {
          "imdb": "0115857"
        },
        "links": {
          "self": "http://api.rottentomatoes.com/api/public/v1.0/movies/11494.json",
          "alternate": "http://www.rottentomatoes.com/m/1072457-chain_reaction/",
          "cast": "http://api.rottentomatoes.com/api/public/v1.0/movies/11494/cast.json",
          "reviews": "http://api.rottentomatoes.com/api/public/v1.0/movies/11494/reviews.json",
          "similar": "http://api.rottentomatoes.com/api/public/v1.0/movies/11494/similar.json"
        }
      },
      isLoading: true,
      isCategory: true,
    };
    this.requestData = this.requestData.bind(this);
  }

  componentDidMount() {
    this.requestData();
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: '视频列表'
    }
  }

  requestData = () => {
    const url = 'https://smart.pkpm.cn:910/api/User/PKPM/GetDEMOVideo'
    const urltest = "https://raw.githubusercontent.com/facebook/react-native/0.51-stable/docs/MoviesExample.json";
    const category = this.props.navigation.getParam('category');
    let categoryData = [];
    fetch(url)
      .then((res) => res.json())
      .then((resData) => {
        resData.map((item, index) => {
          if (category == item.category) {
            categoryData.push(item);
          }
        })
        this.setState({
          data: categoryData,
          isLoading: false
        });
        // 注意，这里使用了this关键字，为了保证this在调用时仍然指向当前组件，我们需要对其进行“绑定”操作
       
      }).catch(e => console.log(e));
  }

  
  render() {
    const { isLoading, data, isCategory } = this.state;
    const { navigation } = this.props;
    if (isLoading) {
      return (
        <View style={styles.container}>
          <Text>
            正在加载视频数据……
        </Text>
        </View>
      )
    } 
    else {
      return (
        <View>
          <FlatList
            data={this.state.data}
            renderItem={this._renderItem}
            style={styles.list}
          //keyExtractor={item => item.id}
          />
        </View>
      )
    }



  }

  _renderItemCategory = ({item, index}) => {
    const { navigation } = this.props;
    return (
      <View>
         <TouchableOpacity
          onPress={() => {
            this.setState({
              isCategory: false
            })
            navigation.push('VideoList',)
          }} ></TouchableOpacity>
      </View>
    )
  }

  _renderItem = ({ item, index }) => {
    const { navigation } = this.props;
    return (
      <View>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('VideoPage', {
              videopath: 'http://smart.pkpm.cn:82/' + item.videopath,
              viedoname: item.viedoname,
              category: item.category,
            })
          }} >
          <View
            style={styles.container}
          >
            <Image
              source={require('../../../viewImg/video.png')}
              style={styles.thumbnail}
              resizeMethod='scale'
              resizeMode='contain'
            />
            <View style={styles.rightContainer}>
              <Text style={styles.title}>{item.viedoname}</Text>
              <Text style={styles.year}>{item.category}</Text>
            </View>

          </View>
        </TouchableOpacity>
      </View >
    );
  }

  renderLoadingView() {
    return (
      <View style={styles.container}>
        <Text>
          正在加载视频数据……
        </Text>
      </View>
    );
  }

}

export default videoList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    //backgroundColor: '#F5FCFF',
  },
  thumbnail: {
    marginLeft: 8,
    width: 81,
    height: 64
  },
  rightContainer: {
    flex: 1,
  },
  title: {
    fontSize: 16,
    marginLeft: 6,
    textAlignVertical: 'center',
    //textAlign: 'center',
  },
  year: {
    marginLeft: 6,
    marginTop: 5,
    fontSize: 12
    //textAlign: 'center',
  },
  list: {
    paddingBottom: 20,
    //marginLeft: deviceWidth * 0.01,
   // width: deviceWidth * 0.98
    //backgroundColor: '#F5FCFF',
  },
});