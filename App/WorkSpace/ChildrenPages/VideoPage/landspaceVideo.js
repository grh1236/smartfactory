import React from 'react'
import { View, TouchableOpacity, TouchableWithoutFeedback, StyleSheet, FlatList, ActivityIndicator, Platform, ScrollView, Dimensions, StatusBar } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, Slider } from 'react-native-elements';
import Url from '../../../Url/Url';
import { RFT, deviceWidth, deviceHeight } from '../../../Url/Pixal';
import Video from 'react-native-video';
//import Slider from '@react-native-community/slider';
import Orientation from '@emanon_/react-native-orientation';
import { SafeAreaView } from 'react-navigation';

const { width, height } = Dimensions.get('window')

export default class landspaceVideo extends React.Component {
  constructor() {
    super();
    this.state = {
      data: [],
      isLoading: true,

      isPaused: true,  //是暂停
      duration: 0,      //总时长
      currentTime: 0, //当前播放时间
      sliderValue: 0,   //进度条的进度 
    }
    this.changePausedState = this.changePausedState.bind(this);
    this.customerSliderValue = this.customerSliderValue.bind(this);
    this._changePauseSliderFullState = this._changePauseSliderFullState.bind(this);
    this._onStartShouldSetResponder = this._onStartShouldSetResponder.bind(this);
    this.requestData = this.requestData.bind(this);
  }

  static navigationOptions = ({ navigation }) => {
    return {
      header: null
    }
  }

  componentWillMount() {
    Orientation.lockToLandscape();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
    this._navListener.remove();
  }

  requestData = () => {
    const url = 'https://smart.pkpm.cn:910/api/User/PKPM/GetDEMOVideo'
    const urltest = "https://raw.githubusercontent.com/facebook/react-native/0.51-stable/docs/MoviesExample.json";
    const category = this.props.navigation.getParam('category');
    let categoryData = [];
    fetch(url)
      .then((res) => res.json())
      .then((resData) => {
        resData.map((item, index) => {
          if (category == item.category) {
            categoryData.push(item);
          }
        })
        this.setState({
          data: categoryData,
          isLoading: false
        });
      }).catch(e => console.log(e));
  }

  changePausedState = () => { //控制按钮显示播放，要显示进度条3秒钟，之后关闭显示
    this.setState({
      isPaused: this.state.isPaused ? false : true,
      isVisiblePausedSliderFullScreen: true
    })
    //这个定时调用失去了this指向
    let that = this;
    setTimeout(() => {
      that.setState({
        isVisiblePausedSliderFullScreen: false
      })
    }, 30000)
  }

  _changePauseSliderFullState = () => { // 单击事件，是否显示 “暂停、进度条、全屏按钮 盒子”
    let flag = this.state.isVisiblePausedSliderFullScreen ? false : true;
    this.setState({
      isVisiblePausedSliderFullScreen: flag
    })
    //这个定时调用失去了this指向
    let that = this;
    setTimeout(() => {
      that.setState({
        isVisiblePausedSliderFullScreen: false
      })
    }, 30000)
  }

  _onStartShouldSetResponder(e) {
    console.log(e);
  }

  //格式化音乐播放的时间为0：00。借助onProgress的定时器调用，更新当前时间
  formatMediaTime(time) {
    let minute = Math.floor(time / 60);
    let second = parseInt(time - minute * 60);
    minute = minute >= 10 ? minute : "0" + minute;
    second = second >= 10 ? second : "0" + second;
    return minute + ":" + second;

  }

  //加载视频调用，主要是拿到 “总时间”，并格式化
  customerOnload(e) {
    let time = e.duration;
    this.setState({
      duration: time
    })
  }

  // 获得当前的，播放时间数，但这个数是0.104，需要处理
  customerOnprogress(e) {
    let time = e.currentTime;   // 获取播放视频的秒数       
    this.setState({
      currentTime: time,
      sliderValue: time
    })
  }

  // 移动滑块，改变视频播放进度
  customerSliderValue(value) {
    this.player.seek(value);
  }


  componentDidMount() {
    this.requestData();
    this._navListener = this.props.navigation.addListener('didFocus', () => {
      //StatusBar.setBarStyle('dark-content');
      StatusBar.setBackgroundColor('#419FFF');
    });
  }

  render() {
    const { navigation } = this.props;
    const videopath = navigation.getParam('videopath');
    const viedoname = navigation.getParam('viedoname');
    console.log(RFT)
    let playButtonComponent = (
      <TouchableWithoutFeedback
        onPress={this.changePausedState}
      >
        <View style={styles.playBtn}>
        </View>
      </TouchableWithoutFeedback>
    );

    let pausedBtn = this.state.isPaused ? playButtonComponent : null;

    // 暂停按钮、进度条、全屏按钮 是否显示
    let pausedSliderFullComponent = (
      <View style={{ position: "absolute", bottom: 0 }}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          {/* 进度条按钮  */}
          <View style={styles.sliderBox}>
            <View >
              <Icon
                containerStyle={{ marginLeft: 3 }}
                type='antdesign'
                name={this.state.isPaused ? 'pause' : 'caretright'}
                size={RFT * 5}
                color='white'
                onPress={() => {
                  this.setState({
                    isPaused: !this.state.isPaused
                  })
                }}
              ></Icon>
            </View>
            <Text style={{ color: 'white', fontSize: RFT * 2.5, marginLeft: 6 }}>{this.formatMediaTime(this.state.currentTime)}</Text>
            <Slider
              style={{ marginLeft: 10, width: height * 0.85, height: 40 }}
              value={this.state.sliderValue}
              maximumValue={this.state.duration}
              thumbTintColor="#fff" //开关夹点的yanse              
              minimumTrackTintColor="red"
              maximumTrackTintColor="#ccc"
              step={1}
              onValueChange={this.customerSliderValue}
            />
            <Text style={{ color: 'white', fontSize: RFT * 2.5 }}>{this.formatMediaTime(this.state.duration)}</Text>
          </View>
          {/* 全屏按钮 */}



        </View>
      </View>
    );

    let pausedSliderFull = this.state.isVisiblePausedSliderFullScreen ? pausedSliderFullComponent : null;

    return (
      <SafeAreaView>
        <StatusBar backgroundColor={'black'} />
        <View >
          <TouchableWithoutFeedback
            onPress={this._changePauseSliderFullState}
            onResponderMove={this._onStartShouldSetResponder}
          >
            <Video source={{ uri: videopath }}
              ref={(ref) => {
                this.player = ref
              }}
              onBuffer={() => {
                return (
                  <View style={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    // backgroundColor: '#F5FCFF',
                  }}>
                    <ActivityIndicator
                      animating={true}
                      color='#419FFF'
                      size="large" />
                  </View>
                )
              }}
              style={{ backgroundColor: 'black', width: height + 20, height: width - RFT - 30 }}
              allowsExternalPlayback={false} // 不允许导出 或 其他播放器播放
              paused={this.state.isPaused} // 控制视频是否播放
              resizeMode="cover"
              onLoad={(e) => this.customerOnload(e)}
              onProgress={(e) => this.customerOnprogress(e)}
              fullscreenAutorotate = {true}
              
              //fullscreenOrientation = 'landscape'
              fullscreen={true}
              repeat={true}
            />
          </TouchableWithoutFeedback>
          {/* 播放的按钮：点击之后需要消失 */}
          {pausedBtn}
          {/* 暂停按钮，进度条，全屏按钮 */}
          {pausedSliderFull}
        </View>


      </SafeAreaView>

    )
  }

}

const styles = StyleSheet.create({
  backgroundVideo: {
    position: 'absolute',
    color: 'red',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  myVideo: {
    width: 340,
    height: 240
  },
  playBtn: {
    width: 50,
    height: 50,
    backgroundColor: 'red',
    borderRadius: 50,
    position: "absolute",
    top: "50%",
    left: "50%",
    marginLeft: -25,
    marginTop: -25,
    zIndex: 999
  },
  sliderBox: {
    flex: 0,
    flexDirection: 'row',
    alignItems: 'center'
  },
})