import React from 'react';
import { View, ScrollView, StyleSheet, processColor, ActivityIndicator } from 'react-native';
import { Button, Text, } from 'react-native-elements';
import Url from '../../Url/Url';
import LineChartComp from '../ChildComponent/LineChartComponment';
import { RFT, deviceWidth } from '../../Url/Pixal';
import { withNavigation } from 'react-navigation';

class QualifiedRate extends React.Component {
  constructor() {
    super();
    this.state =
      {
        urlCT: '',
        CTList: [],
        compTypeID: '',
        CTData: {},
        urlTeam: '',
        teamList: [],
        teamID: '',
        teamData: {},
        focusButtonCT: 0,
        focusButtonTeam: 0,
        chartDataCT: {},
        isLoading: true
      }
    this.requestUrl = this.requestUrl.bind(this);
  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  componentDidMount() {
    this.requestUrl();
  }

  requestUrl = () => {
    const urlCTList = Url.url + Url.factory + Url.Fid + 'GetFacCompTypeList';

    this.requestCTList(urlCTList);

    const urlTeamList = Url.url + Url.factory + Url.Fid + 'GetFacTeamList';

    this.requestTeamList(urlTeamList);

  }

  requestCTList = (url) => {
    let CTList = []
    fetch(url, {
      credentials: 'include',
    }).then(res => {
      return res.json()
    }).then(resData => {
      resData.map((item, index) => {
        let tmp = {}
        tmp.ID = item.compTypeId
        tmp.Name = item.compTypeName
        CTList.push(tmp);
      })

      const urlCT = Url.url + Url.factory + 'Fid/' + Url.Fid + 'CTid/' + CTList[0].ID + '/GetFacCompTypeQualifiedRateStatisticYear';

      this.requestCTData(urlCT);

      this.setState({
        CTList: CTList,
        compTypeID: resData[0].compTypeID,
        urlCT: urlCT,
        isLoading: false
      })
    }).catch((error) => {
      console.log(error);
    });
  }

  requestCTData = (url) => {
    let YearMonth = [], finPassRate = [], CTData = {}, chartData = {}, xAxis = {};
    fetch(url, {
      credentials: 'include',
    }).then(res => {
      return res.json()
    }).then(resData => {
      resData.map((item, index) => {
        YearMonth.push(item.yearMonth);
        finPassRate.push(item.finPassRate * 100);
      })

      CTData.Name = YearMonth;
      CTData.Value = finPassRate;

      chartData = {
        dataSets: [
          {
            values: CTData.Value || [0, 0, 0, 0, 0],
            label: '产品合格率',
            config: {
              /* mode: "CUBIC_BEZIER", */
              drawValues: false,
              lineWidth: 2,
              drawCircles: true,
              circleColor: processColor('#FF7A38'),
              drawCircleHole: false,
              circleRadius: 5,
              highlightColor: processColor("transparent"),
              color: processColor('#FF7A38'),
              drawFilled: false,
              fillAlpha: 1000,
              valueTextSize: 15,
              
            }
          }],
      };

      let length = CTData.Name.length

      xAxis = {
        enabled: true,
        granularity: 1,
        drawLabels: true,
        position: "BOTTOM",
        axisMaximum: length,
        axisMinimum: -0.5,
        drawAxisLine: false,
        drawGridLines: false,
        fontFamily: "HelveticaNeue-Medium",
        fontWeight: "bold",
        textSize: RFT * 3,
        textColor: processColor("gray"),
        valueFormatter: CTData.Name,
      }

      this.refComp.ReFlash(chartData, xAxis)

      this.setState({
        CTData: CTData,
        chartDataCT: chartData
      })
    }).catch((error) => {
      console.log(error);
    });
  }

  requestTeamList = (url) => {
    let teamList = []
    console.log(url)
    fetch(url, {
      credentials: 'include',
    }).then(res => {
      return res.json()
    }).then(resData => {
      resData.map((item, index) => {
        let tmp = {}
        tmp.ID = item.teamId
        tmp.Name = item.teamName
        teamList.push(tmp);
      })
      this.setState({
        teamList: teamList,
        teamID: resData[0].teamId,
        isLoading: false,
        urlTeam: Url.url + Url.factory + 'Fid/' + Url.Fid + 'Tid/' + teamList[0].ID + '/GetFacTeamQualifiedRateStatisticYear'
      }, () => {
        console.log(this.state.urlTeam)
        this.requestTeamData(this.state.urlTeam);
      })
    }).catch((error) => {
      console.log(error);
    });
  }

  requestTeamData = (url) => {
    let YearMonth = [], finPassRate = [], teamData = {};
    fetch(url, {
      credentials: 'include',
    }).then(res => {
      return res.json()
    }).then(resData => {
      resData.map((item, index) => {
        YearMonth.push(item.yearMonth);
        finPassRate.push(item.finPassRate  * 100);
      })
      teamData.Name = YearMonth;
      teamData.Value = finPassRate;

      //console.log(YearMonth)
      //console.log(teamData.Name)

      chartData = {
        dataSets: [
          {
            values: teamData.Value || [0, 0, 0, 0, 0],
            label: '产品合格率',
            config: {
              /* mode: "CUBIC_BEZIER", */
              drawValues: false,
              lineWidth: 2,
              drawCircles: true,
              circleColor: processColor('#FF7A38'),
              drawCircleHole: false,
              circleRadius: 5,
              highlightColor: processColor("transparent"),
              color: processColor('#FF7A38'),
              drawFilled: false,
              fillAlpha: 1000,
              valueTextSize: 15
            }
          }],
      };

      let length = teamData.Name.length

      xAxis = {
        enabled: true,
        granularity: 1,
        drawLabels: true,
        position: "BOTTOM",
        axisMaximum: length,
        axisMinimum: -0.5,
        drawAxisLine: false,
        drawGridLines: false,
        fontFamily: "HelveticaNeue-Medium",
        fontWeight: "bold",
        textSize: RFT * 3,
        textColor: processColor("gray"),
        valueFormatter: teamData.Name,
      }

      this.refTeam.ReFlash(chartData, xAxis);

      this.setState({
        teamData: teamData
      })
    }).catch((error) => {
      console.log(error);
    });
  }

  chartDataSet = (Data) => {
    console.log(1)


  }

  CTButton = () => {
    const { CTList, focusButtonCT } = this.state;
    return (
      <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{ margin: deviceWidth * 0.02 }}>
        {
          CTList.map((item, i) => {
            return (
              <Button
                key={i}
                type='clear'
                title={item.Name}
                buttonStyle={{ backgroundColor: focusButtonCT == i ? '#419FFF' : '#F9F9F9', borderRadius: 50 }}
                titleStyle={{ fontSize: RFT * 3.6, color: focusButtonCT == i ? 'white' : '#419FFF' }}
                onPress={() => {
                  this.setState({
                    focusButtonCT: i,
                    urlCT: Url.url + Url.factory + 'Fid/' + Url.Fid + 'CTid/' + item.ID + '/GetFacCompTypeQualifiedRateStatisticYear'
                  }, () => {
                    console.log(this.state.urlCT)
                    this.refComp.setStateLoading();
                    this.requestCTData(this.state.urlCT);
                  });
                }}
              />
            )
          })
        }
      </ScrollView>
    )
  }

  TeamButton = () => {
    const { teamList, focusButtonTeam } = this.state;
    return (
      <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{ margin: deviceWidth * 0.02 }}>
        {
          teamList.map((item, i) => {
            return (
              <Button
                key={i}
                type='clear'
                title={item.Name}
                buttonStyle={{ backgroundColor: focusButtonTeam == i ? '#419FFF' : '#F9F9F9', borderRadius: 50 }}
                titleStyle={{ fontSize: RFT * 3.6, color: focusButtonTeam == i ? 'white' : '#419FFF' }}
                onPress={() => {
                  this.setState({
                    focusButtonTeam: i,
                    urlTeam: Url.url + Url.factory + 'Fid/' + Url.Fid + 'Tid/' + item.ID + '/GetFacTeamQualifiedRateStatisticYear'
                  }, () => {
                    this.refTeam.setStateLoading();
                    this.requestTeamData(this.state.urlTeam)
                  });
                }}
              />
            )
          })
        }
      </ScrollView>
    )

  }

  render() {
    return (
      <ScrollView style={styles.view}>

        <View>
          <View style={{ flexDirection: 'row', marginTop: 10 }}>
            <View style={{ justifyContent: 'center', alignContent: 'center' }}>
              <Text style={{ textAlign: 'center', textAlignVertical: 'center', color: '#535c68', fontSize: RFT * 3.6, paddingLeft: 15, paddingRight: 15 }}>构件类型</Text>
            </View>
            {this.CTButton()}
          </View>
          <View style={styles.chartview}>
            <View style={styles.chart}>
              <LineChartComp
                ref={refComp => { this.refComp = refComp }}
                chartData={this.state.chartDataCT}
              //onRef={refcomptype => { this.refcomptype = refcomptype }}
              />
            </View>
          </View>
        </View>

        <View>
          <View style={{ flexDirection: 'row', marginTop: 10  }}>
            <View style={{ justifyContent: 'center', alignContent: 'center' }}>
              <Text style={{ textAlign: 'center', textAlignVertical: 'center', color: '#535c68', fontSize: RFT * 3.6, paddingLeft: 15, paddingRight: 15 }}>班组</Text>
            </View>
            {this.TeamButton()}
          </View>
          <View style={styles.chartview}>
            <View style={styles.chart}>
              <LineChartComp
                ref={refTeam => { this.refTeam = refTeam }}
              // chartData={this.state.chartDataCT}
              //onRef={refcomptype => { this.refcomptype = refcomptype }}
              />
            </View>
          </View>
        </View>

      </ScrollView >
    );

  }
}

const styles = StyleSheet.create({
  view: {
    flex: 1,
    backgroundColor: '#F9F9F9'
  },
  buttonview: {
    height: 40,
    flexDirection: "row",
    justifyContent: 'space-around',
    marginTop: 3,
  },
  text: {
    color: '#535c68',
    fontSize: RFT * 3.5,
    paddingLeft: 15,
    marginTop: 10,
  },
  chartview: {
    width: deviceWidth * 0.94,
    height: deviceWidth * 0.9,
    marginTop: 10,
    marginLeft: deviceWidth * 0.03,
    backgroundColor: '#FFFFFF',
    borderRadius: 10
  },
  chart: {
    width: deviceWidth * 0.94,
    height: deviceWidth * 0.87,
  }
})

export default QualifiedRate;