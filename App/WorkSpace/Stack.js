import React from 'react'
import { StatusBar, View, Text,  } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import WorkSpaceView from './View';
import ChildOne from './ChildrenPages/ChildOneFlat'
import ChildTwo from './ChildrenPages/ChildrenPageTwo'
import qualifiedQue from './ChildrenPages/qualifiedQue'
import qualifiedRate from './ChildrenPages/qualifiedRate'
import VideoList from './ChildrenPages/VideoPage/VideoList'
import VideoPage from './ChildrenPages/VideoPage/VideoPage1'
import VideoCategory from './ChildrenPages/VideoPage/VideoCategory'
import landspaceVideo from './ChildrenPages/VideoPage/landspaceVideo1';
import { RFT, deviceWidth, deviceHeight } from '../Url/Pixal';
import { Icon } from 'react-native-elements';

//LOGO的题头
class LogoTitle extends React.Component {
  constructor() {
    super();
    this.state = {
      Factoryname: '',
      route: '日常',
      colors: ['#F4BC2D', 'rgba(246,172,34,0.88)']
    }
  }


  componentDidMount() {
  }

  componentWillUnmount() {
  }

  render() {
    console.log("🚀 ~ file: MainScreen.js ~ line 74 ~ LogoTitle ~ render ~ render", this.props)
    return (
      <View
        style={{ width: deviceWidth }}
      >
        <View style={{ flex: 1, left: deviceWidth * 0.1 }}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 3 }}>
              <Text style={{ marginLeft: 10, marginTop: 15, fontSize: 18, color: '#333', fontWeight: 'bold', textAlign: 'center' }} >{'工作台'}</Text>
            </View>
            <View style={{ flex: 1, marginTop: 16, left: -(RFT * 4) }}>
              <Icon onPress={() => {
                this.props.navigation.navigate('CameraMain', {
                  mainpage: "首页",
                  title: '构件扫码'
                })
              }}
                type='ant-design' name='scan1' size={24} color='#333' />
            </View>

          </View>
        </View >
      </View>

      /* <View style={{ flexDirection: 'row', height: 28 }}>
        <Image
          source={require('../../logo.png')}
          style={{ width: 15, height: 26, marginLeft: 15 }}
        />
        <Text style={{ marginLeft: 10, fontSize: 18, color: 'black' }}>构力科技</Text>
      </View> */
    );
  }
}

const WorkSpaceStack = createStackNavigator({
  Route: {
    screen: WorkSpaceView,
    navigationOptions: props => {
      return {
        headerTitle: <LogoTitle {...props} />,
      }
    }
  },
  ChildOne: {
    screen: ChildOne,
    navigationOptions: {
      /*headerTitle: '任务单',*/ headerTintColor: 'black',
    }
  },
  ChildTwo: {
    screen: ChildTwo,
    navigationOptions: {
      headerTintColor: 'black'
    }
  },
  qualifiedQue: {
    screen: qualifiedQue,
    navigationOptions: {
      headerTintColor: 'black'
    }
  },
  QualifiedRate: {
    screen: qualifiedRate,
    navigationOptions: {
      headerTintColor: 'black'
    }
  },
  VideoPage: {
    screen: VideoPage,
    navigationOptions: {
      //title: '设备总览',
      tabBarVisible: false,
      headerTintColor: 'black'
    }
  },
  VideoList: {
    screen: VideoList,
    navigationOptions: {
      //title: '设备总览',
      tabBarVisible: false,
      headerTintColor: 'black'
    }
  },
  VideoCategory: {
    screen: VideoCategory,
    navigationOptions: {
      //title: '设备总览',
      tabBarVisible: false,
      headerTintColor: 'black'
    }
  },
  landspaceVideo: {
    screen: landspaceVideo,
    navigationOptions: {
      //title: '设备总览',
      tabBarVisible: false,
      headerTintColor: 'black'
    }
  },
},

  {
    navigationOptions: ({
      navigation
    }) => ({
      tabBarVisible: navigation.state.index > 0 ? false : true,
    }),
    defaultNavigationOptions: {
      headerStyle: {//设置导航条的样式。背景色，宽高等
        backgroundColor: 'white',
        //marginTop: StatusBar.currentHeight
      },
      headerTintColor: '#eee',
      headerTitleStyle: {//设置导航条文字样式
        fontWeight: 'normal'
      }
    }
  });

export default WorkSpaceStack;