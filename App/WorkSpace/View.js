import React from 'react';
import { View, ScrollView, TouchableOpacity, StyleSheet, Dimensions, Alert, StatusBar, ActivityIndicator } from 'react-native';
import { Text, Input, Card, Image, Icon, Header } from 'react-native-elements';
import ButtonGroup from '../CommonComponents/ButtonGroupTest'
import UrlReal from '../Url/Url';
import Url1 from '../Url/UrlTest';
import ActionButton from 'react-native-action-button';
import DeviceStorage from '../Url/DeviceStorage';
import { ToDay } from '../CommonComponents/Data'


import JPush from 'jpush-react-native';


const { height, width } = Dimensions.get('window')




const menuIcon = () => ({
  //type: '',
  size: 18,
  name: 'close',
  color: 'white',
});

export default class WorkSpaceScreen extends React.Component {

  constructor() {
    super();
    this.state = {
      Url: '',
      hidden1: true,
      hidden2: true,
      hidden3: true,
      title: {
        first: '已检',
        second: '合格',
        third: '不合格',
        fourth: '报废'
      },
      text: null,
      isLoading: false,
    }
  }

  componentDidMount() {
    //this.setTimer();
    this._navListener = this.props.navigation.addListener('didFocus', () => {
      //StatusBar.setBarStyle('dark-content');
      StatusBar.setBackgroundColor('black');
      StatusBar.setTranslucent(false)
    });
  }

  componentWillUnmount() {
    this._navListener.remove();
  }

  hidden = () => {
    this.setState({ hidden: !this.state.hidden })
  }

  cardView = (Source) => {

    return (
      <View style={styles.ViewCard}>
        {
          Source.map((u, i) => {
            if (UrlReal.Menu.indexOf('APP' + u.text) != -1) {
              return (
                <View style={{ marginLeft: 6, marginBottom: 30, marginTop: 15 }}>
                  <TouchableOpacity
                    onPress={() => {
                      if (UrlReal.Menu.indexOf('APP' + u.text) != -1) {
                        if (u.text === '质量问题记录') {
                          this.props.navigation.navigate('qualifiedQue', {
                            title: u.title,
                            sonTitle: u.sonTitle,
                            url: u.url,
                            sonUrl: u.sonUrl
                          })
                        } else if (u.text === '产品合格率') {
                          this.props.navigation.navigate('QualifiedRate', {
                            title: u.title,
                          })
                        } else {
                          this.props.navigation.navigate('ChildOne', {
                            title: u.title,
                            sonTitle: u.sonTitle,
                            data: this.state.dataTest,
                            url: u.url,
                            sonUrl: u.sonUrl
                          })
                        }
                      } else {
                        Alert.alert('权限提醒', '您还没有权限查看' + u.text + '模块。')
                      }
                    }}>
                    <Image
                      source={u.img}
                      containerStyle={styles.img}
                      resizeMethod='resize'
                      resizeMode='contain'
                      style={{
                        marginHorizontal: width * 0.09,
                        marginVertical: width * 0.04,
                        width: width / 10,
                        height: width / 10,
                      }}
                    ></Image>
                  </TouchableOpacity>
                  <Text style={styles.text}> {u.text} </Text>
                </View>
              );
            }
          })
        }
      </View>
    );
  }

  setTimer = () => {
    let timer = setTimeout(() => {
      clearTimeout(timer)
      this.setState({
        isLoading: false
      })
    }, 1000)
  }

  render() {
    const Url = this.props.navigation.getParam('Url') ? this.props.navigation.getParam('Url') : UrlReal
    console.log('I m workplace')
    const CardSourceShengChan = [
      { img: require('../viewImg/DaiChanchi1.png'), text: '待产池', title: '待产池', sonTitle: '待产池构件信息', main: "Pool", url: Url.url + Url.Produce + 'PostWaitingPoolProjectPage2', sonUrl: '' },
      { img: require('../viewImg/RiShengChanJiLu1.png'), text: '日生产记录', title: '日生产记录', sonTitle: '构件生产信息', url: Url.url + Url.Produce + 'PostProduceDailyRecordProjectPage2', sonUrl: '' },
      { img: require('../viewImg/TuoMoDaiJian1.png'), text: '脱模待检', title: '脱模待检', sonTitle: '脱模待检', url: Url.url + Url.Produce + 'PostProduceReleasePage2', sonUrl: '' },
      { img: require('../viewImg/YiShengChanGouJian1.png'), text: '已生产构件明细', title: '已生产构件明细', sonTitle: '构件列表', url: Url.url + Url.Produce + 'PostProduceProjectPage', sonUrl: '' },
      { img: require('../viewImg/RenWuDan1.png'), text: '任务单', title: '任务单', sonTitle: '任务单', url: Url.url + Url.Produce + 'PostProduceTaskPage', sonUrl: '' },
      { img: require('../viewImg/GouJianZhuanTai1.png'), text: '质量隐检', title: '构件隐蔽检查', sonTitle: '构件隐蔽检查', url: Url.url + Url.Quality + 'PostQualityHidcheckProjectPage2', sonUrl: '' },
    ]
    const CardSourceZhiLiang = [
      { img: require('../viewImg/ChengPinJianCha1.png'), text: '成品检查', title: '成品检查', sonTitle: '成品检查', url: Url.url + Url.Quality + 'PostQualityFinProductsProjectPage', sonUrl: '' },
      { img: require('../viewImg/ChengPinFuCha1.png'), text: '成品复检', title: '成品复检', sonTitle: '成品复检', url: Url.url + Url.Quality + 'PostQualityReFinProductsProjectPage', sonUrl: '' },
      //{ img: require('../viewImg/ZhiLiangWenTiJiLu.png'), text: '质量问题记录', title: '质量问题记录', sonTitle: '质量问题记录', url: Url.url + Url.Quality + Url.Fid + '/GetQualityFinProductsProject' },
      { img: require('../viewImg/GouJianBaoFei1.png'), text: '构件报废', title: '构件报废', sonTitle: '构件报废', url: Url.url + Url.Quality + 'PostQualityScrapProjectPage', sonUrl: '' },
      { img: require('../viewImg/ChanPinHeGeLv1.png'), text: '产品合格率', title: '产品合格率分析', sonTitle: '产品合格率分析', url: '' },

    ]
    const CardSourceChengPin = [
      { img: require('../viewImg/YunDanChaXun1.png'), text: '发货计划', title: '发货计划', sonTitle: '运单详情', url: Url.url + Url.Stock + 'PostStockLoaoutWarePage', sonUrl: '' },
      { img: require('../viewImg/ChengPinChuKu1.png'), text: '成品出库', title: '成品出库', sonTitle: '构件详情', url: Url.url + Url.Stock + 'PostStockLoaoutWarePage', sonUrl: '' },
    ]
    console.log(Url.Fid)
    if (this.state.isLoading) {
      return (
        <View style={{
          flex: 1,
          //flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#F5FCFF',
        }}>
          <StatusBar translucent={false} />
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )

    } else {
      return (
        <View style={{ flex: 1, backgroundColor: '#f9f9f9' }}>
          <StatusBar translucent={false} />
          <ScrollView >
            {
              UrlReal.Menu.indexOf('APP培训视频') != -1 ?

                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('VideoCategory')
                  }} >
                  <View>
                    <Image
                      source={require('../tabImg/pkpm.jpg')}
                      containerStyle={{ marginHorizontal: width * 0.15, marginTop: 10, shadowColor: 'gary', shadowOpacity: 10, shadowOffset: { width: 1, height: 1 }, }}
                      resizeMethod='scale'
                      resizeMode='contain'
                      style={{ height: 320 / 1.5, width: width * 0.7, shadowOffset: { width: 1, height: 1 }, shadowColor: 'gary', shadowOpacity: 1 }}
                    ></Image>
                    <Text style={{ color: '#535c68', fontSize: 18, textAlign: 'center' }}>畅轻版系统使用教学视频</Text>
                  </View>
                </TouchableOpacity> :
                <View></View>
            }

            {
              UrlReal.Menu.indexOf('APP待产池') != -1 || UrlReal.Menu.indexOf('APP日生产记录') != -1 || UrlReal.Menu.indexOf('APP脱模待检') != -1 || UrlReal.Menu.indexOf('APP已生产构件明细') != -1 || UrlReal.Menu.indexOf('APP质量隐检') != -1 ?
                <View>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({ hidden1: !this.state.hidden1 })
                    }} >
                    <Header
                      statusBarProps={{ translucent: false }}
                      placement="left"
                      leftComponent={<MenuIcon
                        color={'#419FFF'}
                        onPress={() => {
                          this.setState({ hidden1: !this.state.hidden1 })
                        }} />}
                      centerComponent={{ text: '生产管理', style: { color: 'gray', fontSize: 24 } }}
                      containerStyle={{
                        backgroundColor: 'transparent',
                        justifyContent: 'space-around',
                        paddingTop: 0,
                        height: 60
                      }}
                    />
                  </TouchableOpacity>
                  {this.state.hidden1 ? <View></View> : this.cardView(CardSourceShengChan)}
                </View> :
                <View></View>

            }

            {
              UrlReal.Menu.indexOf('APP成品检查') != -1 || UrlReal.Menu.indexOf('APP成品复检') != -1 || UrlReal.Menu.indexOf('APP构件报废') != -1 || UrlReal.Menu.indexOf('APP产品合格率') != -1 ?
                <View>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({ hidden2: !this.state.hidden2 })
                    }} >
                    <Header
                      statusBarProps={{ translucent: false }}
                      placement="left"
                      leftComponent={<MenuIcon
                        color={'#32D185'}
                        onPress={() => {
                          this.setState({ hidden2: !this.state.hidden2 })
                        }} />}
                      centerComponent={{ text: '质量管理', style: { color: 'gray', fontSize: 24 } }}
                      containerStyle={{
                        backgroundColor: 'transparent',
                        justifyContent: 'space-around',
                        paddingTop: 0,
                        height: 60
                      }}
                    />
                  </TouchableOpacity>
                  {this.state.hidden2 ?
                    <View></View> :
                    this.cardView(CardSourceZhiLiang)
                  }

                </View> :
                <View></View>

            }

            {
              UrlReal.Menu.indexOf('APP运单查询') != -1 || UrlReal.Menu.indexOf('APP成品出库') != -1 ?
                <View>
                  <TouchableOpacity
                    onPress={() => {

                      this.setState({ hidden3: !this.state.hidden3 })

                    }} >
                    <Header
                      statusBarProps={{ translucent: false }}
                      placement="left"
                      leftComponent={<MenuIcon
                        color={'#F49136'}
                        onPress={() => {
                          this.setState({ hidden3: !this.state.hidden3 })
                        }} />}
                      centerComponent={{ text: '成品管理', style: { color: 'gray', fontSize: 24 } }}
                      containerStyle={{
                        backgroundColor: 'transparent',
                        justifyContent: 'space-around',
                        paddingTop: 0,
                        height: 60
                      }}
                    />
                  </TouchableOpacity>
                  {this.state.hidden3 ?
                    <View></View> :
                    this.cardView(CardSourceChengPin)
                  }
                </View> :
                <View></View>
            }

          </ScrollView>

        </View>
      );
    }

  }
}

class MenuIcon extends React.Component {
  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress}>
        <Icon
          name='menu'
          color={this.props.color} />
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  ViewCard: {
    backgroundColor: 'white',
    width: width * 0.9,
    marginLeft: width * 0.05,
    borderRadius: 20,
    flexWrap: 'wrap',
    flexDirection: 'row',
    //marginBottom: 20,
    justifyContent: 'flex-start',
    flex: 1

  },
  img: {
    height: width / 5,
    width: width / 3.6,
    //backgroundColor: 'yellow'
  },
  text: {
    color: 'black',
    textAlign: 'center',
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
  actionButtonText: {
    color: 'white',
    fontSize: 14,
  }
})