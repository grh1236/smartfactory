import React from 'react';
import {
  ActivityIndicator,
  StyleSheet,
  View,
} from 'react-native';
import DeviceStorage from './Url/DeviceStorage';
import Url from './Url/Url';

export default class AuthLoadingScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userToken: true,
    }
    this._bootstrapAsync();
  }


  componentWillMount() {
    this._bootstrapAsync();
  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    const { userToken } = this.state;
    await DeviceStorage.get('isLoading')
      .then(res => {
        if (res) {
          this.setState({
            userToken: res
          })
        }
      }).catch(err => { })

    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    console.log("🚀 ~ file: Loading.js ~ line 39 ~ AuthLoadingScreen ~ _bootstrapAsync= ~ Url.Menu.indexOf('APP流') != -1", Url.Menu.indexOf('APP流程') != -1)
    let isProcess = Url.Menu.indexOf('APP流程') != -1
    if (isProcess) {
      this.props.navigation.navigate(userToken ? 'MainProcess' : 'Loading');
    } else {
      this.props.navigation.navigate(userToken ? 'Main' : 'Loading');
    }
    
  };

  // Render any loading content that you like here
  render() {
    return (
      <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
    );
  }
}

const styles = StyleSheet.create({
  loading: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  }
})