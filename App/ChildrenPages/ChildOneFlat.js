import React from 'react'
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge } from 'react-native-elements';
import Url from '../Url/Url';
import { ToDay, YesterDay, BeforeYesterDay } from '../CommonComponents/Data';
import { RFT, deviceWidth } from '../Url/Pixal';

const Urlnew = Url.url + Url.Produce;
let pageNo = 1;   //翻页设置,有问题
const { height, width } = Dimensions.get('window') //获取宽高

//搜索栏搜索图标
const defaultSearchIcon = () => ({
  //type: 'antdesign',
  size: 20,
  name: 'search',
  color: 'white',
});

//搜索栏清理图标
const defaultClearIcon = () => ({
  //type: '',
  size: 20,
  name: 'close',
  color: 'white',
});

export default class ChildOne extends React.Component {
  constructor() {
    super();
    this.state = {
      search: '', //搜索关键字保存
      resData: [], //接口数据保存
      item: [], //好像没用
      isLoading: true,  //判断是否加载 加载页
      isRefreshing: false,  //是否刷新
      page: '1', //页码
      open: false, //
      selectedIndex: 0,  //按钮组选择
      pageLoading: true,
      urlnew: '',
    }
    //函数绑定
    this.requestData = this.requestData.bind(this);
    this.refreshing = this.refreshing.bind(this);
    this._onload = this._onload.bind(this);
    this._footer = this._footer.bind(this);
  }

  //长列表头部
  _header = () => {
    return <Text style={[styles.txt, { backgroundColor: 'black' }]}>这是头部</Text>;
  }

  //长列表尾部
  _footer = () => {
    return (
      /* <View style={{ flexDirection: 'row', justifyContent: 'center', alignContent: 'center' }}>
        <Text style={[styles.txt, { textAlign: 'right', flex: 1 }]}>第</Text>
        <Input
          containerStyle={{ width: width / 10 }}
          placeholder=' '
          onChangeText={text => this.setState({ page: text })}
          onChange={text => {
            this.setState({ page: text })
            this.requestData();
          }} >
        </Input>
        <Text style={[styles.txt, { flex: 1 }]}> 页</Text>
      </View> */
      <Text style={[{ fontSize: 18, textAlign: 'center', marginHorizontal: 30 }]}>第 {this.state.page} 页</Text>
    );
  }

  //长列表分隔栏
  _separator = () => {
    return <View style={{ height: 2, backgroundColor: '#fffffc' }} />;
  }

  //刷新列表
  refreshing() {
    this.requestData();
    console.log('page: ')
    console.log(this.state.page)
  }

  //上拉加载下一页
  _onload() {
    if (this.state.resData.length === 40) {
      /* this.setState({
        isLoading: true,
      }) */
      this.state.page++;
      this.requestData();
      console.log('page: ')
      console.log(this.state.page)
    }

  }

  //执行函数
  componentDidMount() {
    this.requestData();
  }

  componentDidUpdate() {
    return true;
  }

  //返回接口数据
  requestData = (urlprops) => {
    const { navigation } = this.props;
    const url = navigation.getParam('url'); //从主界面获取链接
    //需要翻页的链接返回
    if (url.indexOf('/40/') != -1) {
      //接口链接需要加上页数，页数上拉会加载下一页
      fetch(url + this.state.page).then(res => {
        return res.json()
      }).then(resData => {
        this.setState({
          resData: resData.concat(this.state.resData),
          isLoading: false,
          pageLoading: false,
        });
        //console.log(this.state.resData);
      }).catch((error) => {
        console.log(error);
      });
    } //需要按日期记录的页面数据，日期用按钮控制，从render函数传过来
    else if (url.search('DailyRecord') != -1) {
      //点按钮时传当日数据，否则传今天的
      fetch(urlprops ? urlprops : url).then(res => {
        return res.json()
      }).then(resData => {
        this.setState({
          resData: resData,
          isLoading: false,
          pageLoading: false,
        });
        console.log(this.state.resData);
      }).catch((error) => {
        console.log(error);
      });
    } //通用数据接口
    else {
      fetch(url).then(res => {
        return res.json()
      }).then(resData => {
        this.setState({
          resData: resData,
          isLoading: false,
          pageLoading: false,
        });
        //console.log(this.state.resData);
      }).catch((error) => {
        console.log(error);
      });
    }

  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  //搜索栏搜索功能
  TextChange = (text) => {
    let i = 0;
    let searchData = [];
    const { navigation } = this.props;
    const url = navigation.getParam('url');
    console.log(url);
    this.setState({ search: text });
    if (url.indexOf('Project') == -1) {
      this.state.resData.map((item, index) => {
        if (item.compCode.indexOf(text) != -1) {
          searchData[i++] = item;
        }
      })
    } else {
      this.state.resData.map((item, index) => {
        if (item.projectName.indexOf(text) != -1) {
          searchData[i++] = item;
        }
      })
    }
    if (text) {
      this.setState({ resData: searchData })
    } else {
      this.setState({ pageLoading: !this.state.pageLoading })
      this.requestData();
    }
  }

  //日期按钮组顶栏
  ButtonGroupday = () => {
    const buttons = ['今日', '昨日', '前日']
    const { selectedIndex } = this.state
    return (
      <View style={{ flex: 1.3, flexDirection: 'row' }}>
        <ButtonGroup
          onPress={this.updateIndex.bind(this)}
          selectedIndex={selectedIndex}
          buttons={buttons}
          buttonStyle={{ borderWidth: 0, }}
          containerStyle={{ borderWidth: 0, flex: 1 }}
          selectedButtonStyle={{ backgroundColor: '#419FFF', }}
        />
        <Button
          title='筛选'
          type='clear'
          style={{ flex: 1, justifyContent: 'center', alignContent: 'center', }}
          titleStyle={{ color: 'gray' }}
          iconRight={true}
          icon={<Icon
            name='filter'
            type='antdesign'
            color='#419FFF' />} />
      </View>
    )
  }

  //顶部记录与筛选按钮
  Top = () => {
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const url = navigation.getParam('url')
    let length = 0;
    this.state.resData.map((item, index) => {
      if (item.compNum !== 0) {
        length++
      }
    })
    return (
      <View style={{ flex: 1.2, flexDirection: 'row', justifyContent: 'flex-end' }}>
        <Button
          type='clear'
          title={length + '条记录'}
          titleStyle={{ color: 'gray' }}
          style={{ width: width / 3, flex: 1 }}
        />
        {url.indexOf('Daily') != -1 ?
          <View></View> :
          <Button
            title='筛选'
            type='clear'
            style={{ width: width / 3, flex: 1 }}
            titleStyle={{ color: 'gray' }}
            iconRight={true}
            icon={<Icon
              name='filter'
              type='antdesign'
              color='#419FFF' />}
          />
        }

      </View>
    )
  }

  //按钮组触发函数
  updateIndex(selectedIndex) {
    const { navigation } = this.props;
    const url = navigation.getParam('url')
    //console.log(url.substring(0, url.length - 10));
    this.setState({
      selectedIndex: selectedIndex,
      pageLoading: true
    })
    if (selectedIndex == 0) {
      const urlnew = url.substring(0, url.length - 10) + ToDay
      console.log(urlnew)
      this.requestData(urlnew);
    } else if (selectedIndex == 1) {
      const urlnew = url.substring(0, url.length - 10) + YesterDay
      this.requestData(urlnew);
      console.log(urlnew)
    } else if (selectedIndex == 2) {
      const urlnew = url.substring(0, url.length - 10) + BeforeYesterDay
      console.log(urlnew)
      this.requestData(urlnew);
    }
  }

  //渲染页面函数
  render() {
    console.log(ToDay, YesterDay, BeforeYesterDay)
    const { navigation } = this.props;
    //从主页面传url, 未来集成到一起
    const url = navigation.getParam('url')
    console.log(url)
    //从主页面接收孙子页面标题
    var sonTitletest = navigation.getParam('sonTitle');
    // 是否显示加载页，由接口返回数据判断
    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (
        <View style={{ flexDirection: 'column', flex: 1 }}>
          {/* 搜索栏 */}
          <SearchBar
            platform='android'
            placeholder={url.indexOf('Project') == -1 ? '按编号进行搜索...' : '按项目名进行搜索...'}
            placeholderTextColor='white'
            // style={{ backgroundColor: 'red', borderColor: 'red', borderWidth: 10 }}
            //containerStyle={{ flex: 1 }}
            inputContainerStyle={{ backgroundColor: '#419FFF', width: deviceWidth * 0.94, marginLeft: deviceWidth * 0.03, borderRadius: 45}}
            inputStyle={{ color: '#FFFFFF' }}
            searchIcon={defaultSearchIcon()}
            clearIcon={defaultClearIcon()}
            cancelIcon = {defaultClearIcon()}
            round={true}
            value={this.state.search}
            onChangeText={this.TextChange}
            input={(input) => { this.setState({ search: input }) }} />
          {/* 是否显示日期选择按钮组 */}
          {url.indexOf('Daily') != -1 ? this.ButtonGroupday() : <View style={{ backgroundColor: 'white' }}></View>}
          {/* 是否显示筛选顶栏 */}
          {this.Top()}
          <View style={{ flex: 14.5 }}>
            {/* 长列表 */}
            {
              this.state.pageLoading ?
                <View style={styles.loading}>
                  <ActivityIndicator
                    animating={true}
                    color='#419FFF'
                    size="large" />
                </View> :
                <FlatList
                  style={{ flex: 1, borderRadius: 10 }}
                  ref={(flatList) => this._flatList = flatList}
                  ItemSeparatorComponent={this._separator}
                  renderItem={this._renderItem}
                  onRefresh={this.refreshing}
                  refreshing={false}
                  //控制上拉加载，两个平台需要区分开
                  onEndReachedThreshold={(Platform.OS === 'ios') ? -0.3 : 0.0001}
                  onEndReached={this._onload}
                  numColumns={1}
                  ListFooterComponent={this._footer}
                  //columnWrapperStyle={{borderWidth:2,borderColor:'black',paddingLeft:20}}
                  //horizontal={true}
                  data={this.state.resData}
                />
            }

          </View>
        </View>
      )
    }

  }



  //接口返回的对象数组，长列表对数据遍历
  _renderItem = ({ item, index }) => {
    //console.log(item);
    const { navigation } = this.props;
    const url = navigation.getParam('url')
    let sonUrl = navigation.getParam('sonUrl');
    let sonTitletest = item.bigState ? item.bigState : navigation.getParam('sonTitle');
    //待产池项目, 日生产记录项目, 隐检项目页面（通用项目页面）
    if ((item.compNum || item.compNumPass) && !item.projectState && item.projectId) {
      return (
        <Card containerStyle={{ borderRadius: 10 }}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.push('ChildOne', {
                title: '构件列表',
                sonTitle: sonTitletest,
                url:
                  url.indexOf('Pool') != -1 ?
                    Url.url + Url.Produce + Url.Fid + '/GetWaitingPoolComp/' + item.projectId :
                    (url.indexOf('DailyRecordProject') != -1 ?
                      Url.url + Url.Produce + Url.Fid + item.projectId + '/GetProduceDailyRecord/' + ToDay :
                      (url.indexOf('Hidcheck') != -1 ?
                        Url.url + Url.Quality + Url.Fid + '/GetQualityHidcheck/' + item.projectId + '/40/' :
                        (url.indexOf('ReFinProducts') != -1 ?
                          Url.url + Url.Quality + Url.Fid + '/GetQualityReFinProducts/' + item.projectId :
                          (url.indexOf('Scrap') != -1 ?
                            Url.url + Url.Quality + Url.Fid + '/GetQualityScrap/' + item.projectId :
                            //这个是成品检查
                            Url.url + Url.Quality + Url.Fid + '/GetQualityFinProducts/' + item.projectId + '/40/')))
                    ),
              })
            }}>
            <View style={{ flexDirection: 'row', }}>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 1, }}>
                {/* 项目名 */}
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.textname, { fontSize: 17, fontWeight: 'bold' }]}>项目名：</Text>
                  <Text style={[styles.text, { fontSize: 17, fontWeight: 'bold' }]}> {item.projectAbb}</Text>
                </View>
                {/* 开工时间，日生产记录没有,所以只要排除他就好 */}
                <View style={url.indexOf('GetProduceDailyRecordProject') == -1 ? { flexDirection: 'row' } : styles.hidden}>
                  <Text style={styles.textname}>开工日期：</Text>
                  <Text style={styles.text}> {item.startTime}</Text>
                </View>
                {/* 数量，根据页面选择说明 */}
                <View style={url.indexOf('GetQualityFinProductsProject') != -1 ? styles.hidden : { flexDirection: 'row' }}>
                  <Text style={styles.textname}>{url.indexOf('GetWaitingPoolProject') != -1 ? '待产池数量：' : (url.indexOf('GetProduceDailyRecordProject') != -1 ? '生产数量：' : (url.indexOf('GetQualityScrapProject') != -1 ? '报废数量：' : '已检数量：'))}</Text>
                  <Text style={styles.text}> {item.compNum}件</Text>
                </View>
                {/* 生产方量，目前只有日生产记录有。 */}
                <View style={url.indexOf('GetProduceDailyRecordProject') != -1 ? { flexDirection: 'row' } : styles.hidden}>
                  <Text style={styles.textname}>生产方量：</Text>
                  <Text style={styles.text}> {item.compVolume}m³</Text>
                </View>
                {/* 成品检查模块， */}
                <View style={url.indexOf('GetQualityFinProductsProject') != -1 ? { flexDirection: 'column' } : styles.hidden}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.textname}>已检数量：</Text>
                    <Text style={styles.text}> {item.compNumPass + item.compNumNotPass}件</Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.textname}>合格数量：</Text>
                    <Text style={styles.text}> {item.compNumPass}件</Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.textname}>不合格数量：</Text>
                    <Text style={styles.text}> {item.compNumNotPass}件</Text>
                  </View>
                </View>
              </View>
              {/* <Button
                type='clear'
                title={item.state}
                titleStyle={{ fontSize: 14 }}
                containerStyle={{ borderRadius: 60 }}
                style={[item.state ? { flex: 0.1 } : { flex: 0 }, { borderRadius: 60, alignContent: 'flex-start' }]} /> */}
            </View>
          </TouchableOpacity>
        </Card>
      )
    } //已生产构件项目页面
    else if (item.projectState == '生产中') {
      return (
        <Card containerStyle={{ borderRadius: 10 }}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.push('ChildOne', {
                sonTitle: sonTitletest,
                url: Url.url + Url.Produce + Url.Fid + 'GetProjectProducedComponents/' + item.projectId,
              })
            }}>
            <View style={{ flexDirection: 'row', }}>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 1, }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>项目名称：</Text>
                  <Text style={[styles.text, { color: '#419FFF', width: width / 2 }]}> {item.projectName}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>开工日期：</Text>
                  <Text style={styles.text}> {item.startTime}</Text>
                </View><View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>已生产构件：</Text>
                  <Text style={styles.text}> {item.compNum}件</Text>
                </View><View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textname}>已生产方量：</Text>
                  <Text style={styles.text}> {item.compVolume}m³</Text>
                </View>
              </View>
              <Button
                type='clear'
                title={item.projectState}
                titleStyle={{ fontSize: 14 }}
                containerStyle={{ borderRadius: 60 }}
                style={{ flex: 0.1, borderRadius: 60, alignContent: 'flex-start' }}
                icon={<Badge
                  status={item.projectState == '生产中' ? 'warning' : (item.projectState == null ? null : 'error')}
                  containerStyle={{ marginHorizontal: 4 }}
                />} />
              <View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    } //通用页面 估计很快就不通用了？？
    else if (item.compTypeName) {
      return (
        <Card containerStyle={{ borderRadius: 10 }}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('ChildTwo', {
                sonTitle: sonTitletest,
                item: item,
                sonUrl: Url.url + Url.Produce + Url.Fid + 'GetProjectProducedComponents/' + item.projectId,
                url: url
              })
              console.log(Url.url + Url.Produce + Url.Fid + 'etProjectProducedComponents/' + item.projectId);
            }} >
            <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', width: width / 1.5 }}>
                <Text style={[styles.txt, { color: 'orange' }]}>{item.compCode}</Text>
                <Text style={styles.txt} >{item.projectName}</Text>
                <Text style={styles.txt} >{item.compTypeName}</Text>
                <Text style={styles.txt}>{item.floorNoName}({item.floorName})</Text>
                <Text style={item.planDate || item.produceDate ? [styles.txt, { color: 'gray' }] : styles.hidden}>{item.finChkTime ? '成检日期：' : '计划生产日期：'}
                  <Text style={styles.txt}>{item.planDate ? item.planDate : (item.finChkTime ? item.finChkTime : item.produceDate)}</Text>
                </Text>
              </View>
              <View>
                <Button
                  type='clear'
                  title={item.bigState}
                  titleStyle={{ fontSize: 14 }}
                  containerStyle={{ borderRadius: 60 }}
                  style={[item.bigState ? { flex: 1 } : styles.hidden, { borderRadius: 60, alignContent: 'flex-start' }]}
                  disabled={true}
                  disabledTitleStyle={{ color: '#419FFF' }}
                  icon={<Badge
                    //入库蓝色，脱模待检黄色，出库绿色，剩下黄色
                    status={item.bigState == '脱模待检' ? 'warning' : (item.bigState == '出库' ? 'success' : (item.bigState == '入库' ? 'primary' : (item.bigState == null ? null : 'warning')))}
                    containerStyle={{ marginHorizontal: 4 }}
                  />} />
                <Button
                  type='clear'
                  title={item.insresults}
                  titleStyle={{ fontSize: 15 }}
                  style={[item.insresults ? { flex: 1 } : styles.hidden, { borderRadius: 60, alignContent: 'flex-start' }]}
                  disabled={true}
                  disabledTitleStyle={{ color: '#419FFF' }}
                  icon={<Badge
                    status={item.insresults == '合格' ? 'success' : (item.insresults == null ? null : 'error')}
                    containerStyle={{ marginHorizontal: 4 }}
                  />} />
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    }
  }
}

const styles = StyleSheet.create({
  loading: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  contrain: {
    flexDirection: 'column',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  txt: {

    fontSize: 16,
    marginBottom: 8,
    marginTop: 8,
    marginHorizontal: 8
  },
  text: {
    fontSize: 14,
    marginBottom: 8,
    marginTop: 8,
  },
  textname: {

    fontSize: 14,
    marginBottom: 8,
    marginTop: 8,
    color: 'gray'
  },
  textView: {

    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  hidden: {
    flex: 0,
    height: 0,
    width: 0,
  }
})