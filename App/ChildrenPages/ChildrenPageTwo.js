import React from 'react'
import { View, StyleSheet, Dimensions, processColor, FlatList, ScrollView, ActivityIndicator } from 'react-native'
import { Card, Text, Image, Button, Badge } from 'react-native-elements'
import LinearGradient from 'react-native-linear-gradient';
import Url from '..//Url/Url'
import { isTSEnumMember } from '@babel/types';

//定义屏幕宽高
const { height, width } = Dimensions.get('window');

export default class PageTwo extends React.Component {
  constructor() {
    super()
    this.state = {
      resData: [] //接口返回的数据
    }
    this.requestData = this.requestData.bind(this);
  }

  //TopBar
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('sonTitle')
    }
  }

  //执行函数
  componentDidMount() {
    this.requestData();
  }

  //接口数据请求，子页面要求新的链接
  requestData = () => {
    //将子页面链接提取
    const { navigation } = this.props;
    const url = navigation.getParam('sonUrl');
    console.log(url);
    //请求数据
    fetch(url).then(res => {
      return res.json()
    }).then(resData => {
      this.setState({
        resData: resData,
      });
      console.log(this.state.resData);
    }).catch((error) => {
      console.log(error);
    });
  }

  //长列表头部
  _header = (item) => {
    return (
      <View>
        <View style={{ justifyContent: 'center', alignItems: 'center', borderRadius: 12, marginTop: 8 }}>
          <LinearGradient style={{ width: width - 26, borderRadius: 5 }} colors={['#97BBFF', '#419FFF',]}>

            {/* 成品入库头部 */}
            <View style={item.roomName ? { flex: 1 } : { flex: 0, height: 0 }}>
              <Text style={styles.topbartext}>表单编号：{item.formCode}</Text>
              <Text style={styles.topbartext}>库房库位：{item.roomName} </Text>
              <Text style={styles.topbartext}>制单人：{item.stopeople}</Text>
              <Text style={styles.topbartext}>入库日期：{item.stotime}</Text>
              <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                <View style={{ flexDirection: 'column' }}>
                  <Text style={styles.topbartopcenter}>{item.compNumber}件</Text>
                  <Text style={styles.topbarbottomcenter}>构件数量</Text>
                </View>
                <View style={{ flexDirection: 'column' }}>
                  <Text style={styles.topbartopcenter}>{item.compVolume}m³</Text>
                  <Text style={styles.topbarbottomcenter}>总方量</Text>
                </View>
              </View>
            </View>

            {/* 成品出库头部 */}
            <View style={item.carnumbe ? { flex: 1, flexDirection: 'column' } : { flex: 0, height: 0 }}>
              <View style={{ flexDirection: 'column' }}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ flexDirection: 'column', width: width / 1.3 }}>
                    <Text style={styles.topbartext}>计划单号：{item.planformCode}</Text>
                    <Text style={styles.topbartext}>运单编号：{item.formCode} </Text>
                    <Text style={styles.topbartext}>项目名称：{item.projectName}</Text>
                    <Text style={styles.topbartext}>楼号楼层：{item.buildingNo}</Text>
                    <Text style={styles.topbartext}>计划发货日期：{item.planLibTime}</Text>
                    <Text style={styles.topbartext}>实际发货日期：{item.libTime}</Text>
                  </View>
                  <Button
                    titleStyle={{ color: 'white', fontSize: 16 }}
                    title={item.state}
                    type='clear' />
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                  <View style={{ flexDirection: 'column' }}>
                    <Text style={styles.topbartopcenter}>{item.compNumber}件</Text>
                    <Text style={styles.topbarbottomcenter}>构件数量</Text>
                  </View>
                  <View style={{ flexDirection: 'column' }}>
                    <Text style={styles.topbartopcenter}>{item.compVolume}m³</Text>
                    <Text style={styles.topbarbottomcenter}>总方量</Text>
                  </View>
                </View>
              </View>

            </View>
          </LinearGradient>
        </View>

        <View style={item.carnumbe ? { flex: 1, flexDirection: 'column' } : { flex: 0, height: 0 }}>
          <View style={{ marginTop: 12 }}>
            <Text style={{ color: 'gray', fontSize: 16, marginHorizontal: 16 }}>客户信息</Text>
          </View>
          <Card containerStyle={{ borderRadius: 20 }}>
            <View style={styles.textView}>
              <Text style={styles.textname}>客户名称：</Text>
              <Text style={[styles.text]}>{item.customer} </Text>
            </View>
          </Card>

          <View style={{ marginTop: 12 }}>
            <Text style={{ color: 'gray', fontSize: 16, marginHorizontal: 16 }}>车辆信息</Text>
          </View>
          <Card containerStyle={{ borderRadius: 20 }}>
            <View style={styles.textView}>
              <Text style={styles.textname}>车牌号码：</Text>
              <Text style={[styles.text]}>{item.carnumbe} </Text>
            </View>
            <View style={styles.textView}>
              <Text style={styles.textname}>司机姓名：</Text>
              <Text style={[styles.text]}>{item.driver} </Text>
            </View>
          </Card>
        </View>

      </View >
    )
  }

  //刷新和加载
  refreshing() {
    let timer = setTimeout(() => {
      clearTimeout(timer)
      alert('刷新成功')
    }, 1500)
  }
  _onload() {
    let timer = setTimeout(() => {
      clearTimeout(timer)
      console.log('加载成功')
    }, 1500)
  }

  //构件报废详情注解
  remark = (item) => {
    return (
      <View style={{ flex: 3 }} >
        <View style={{ marginTop: 12 }}>
          <Text style={{ color: 'gray', fontSize: 16, marginHorizontal: 16 }}>报废原因</Text>
        </View>
        <Card>
          <Text> {item.remark} </Text>
        </Card>
      </View>
    )
  }

  //构件图片函数，因为需要隐藏，所以独立出来
  ImgSourse = (item) => {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ marginTop: 12 }}>
          <Text style={{ color: 'gray', fontSize: 16, marginHorizontal: 16 }}>构件照片</Text>
        </View>
        <Card >
          <View style={{ justifyContent: 'center', alignContent: 'center' }}>
            <Image
              source={{ uri: Url.Imgurl + item.imgUrl }}
              style={{ width: width - 60, height: width - 60, justifyContent: 'center', alignContent: 'center', }}
              PlaceholderContent={<ActivityIndicator />} />
          </View>
        </Card>
      </View>
    )
  }

  //构件报废图片，或者其他？？图片？
  scrapImgSourse = (item) => {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ marginTop: 12 }}>
          <Text style={{ color: 'gray', fontSize: 16, marginHorizontal: 16 }}>报废构件信息</Text>
        </View>
        <Card >
          <View style={{ justifyContent: 'center', alignContent: 'center' }}>
            <Image
              source={{ uri: Url.Imgurl + item.compImgUrl }}
              style={{ width: width - 60, height: width - 60, justifyContent: 'center', alignContent: 'center', }}
              PlaceholderContent={<ActivityIndicator />} />
            <Image
              source={{ uri: Url.Imgurl + item.reportImgUrl }}
              style={{ width: width - 60, height: width - 60, justifyContent: 'center', alignContent: 'center', }}
              PlaceholderContent={<ActivityIndicator />} />
          </View>
        </Card>
      </View>
    )
  }

  FinProductsImgSourse = (item) => {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ marginTop: 12 }}>
          <Text style={{ color: 'gray', fontSize: 16, marginHorizontal: 16 }}>构件信息</Text>
        </View>
        <Card >
          <View style={{ justifyContent: 'center', alignContent: 'center' }}>
            <Image
              source={{ uri: Url.Imgurl + item.compImgUrl }}
              style={{ width: width - 60, height: width - 60, justifyContent: 'center', alignContent: 'center', }}
              PlaceholderContent={<ActivityIndicator />} />
            <Image
              source={{ uri: Url.Imgurl + item.reportImgUrl }}
              style={{ width: width - 60, height: width - 60, justifyContent: 'center', alignContent: 'center', }}
              PlaceholderContent={<ActivityIndicator />} />
          </View>
        </Card>
      </View>
    )
  }

  //日期安排子卡片
  CardSource2 = (item) => {
    console.log(123)
    return (
      <Card
        containerStyle={styles.contrain}>
        <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
          <Text style={styles.textname}>计划生产：</Text>
          <Text style={styles.text}> </Text>
          <Text style={styles.text2}> </Text>
        </View>
        <View style={{ flexDirection: 'row' }}>
          <Text style={styles.textname}>开始生产：</Text>
          <Text style={styles.text}> </Text>
          <Text style={styles.text2}> </Text>
        </View>
        <View style={{ flexDirection: 'row' }}>
          <Text style={styles.textname}>钢筋笼入模：</Text>
          <Text style={styles.text}> {item.putMoldTime}</Text>
          <Text style={styles.text2}> {item.putMoldName}</Text>
        </View>
        <View style={{ flexDirection: 'row' }}>
          <Text style={styles.textname}>隐蔽检查：</Text>
          <Text style={styles.text}> {item.hidTime}</Text>
          <Text style={styles.text2}> {item.hidEmpName}</Text>
        </View>
        <View style={{ flexDirection: 'row' }}>
          <Text style={styles.textname}>脱模待检：</Text>
          <Text style={styles.text}> {item.demouldTime}</Text>
          <Text style={styles.text2}> {item.demouldName}</Text>
        </View>
        <View style={{ flexDirection: 'row' }}>
          <Text style={styles.textname}>成品检验：</Text>
          <Text style={styles.text}> {item.finChkTime}</Text>
          <Text style={styles.text2}> {item.finChkEmpName}</Text>
        </View>
        <View style={{ flexDirection: 'row' }}>
          <Text style={styles.textname}>成品复检：</Text>
          <Text style={styles.text}> {item.reFinChkTime}</Text>
          <Text style={styles.text2}> {item.reFinChkEmpName}</Text>
        </View>
        <View style={{ flexDirection: 'row' }}>
          <Text style={styles.textname}>成品入库：</Text>
          <Text style={styles.text}> {item.warehouTime}</Text>
          <Text style={styles.text2}> {item.warehouEmpName}</Text>
        </View>
        <View style={{ flexDirection: 'row' }}>
          <Text style={styles.textname}>成品出库：</Text>
          <Text style={styles.text}> {item.loaoutTime}</Text>
          <Text style={styles.text2}> {item.loaoutEmpName}</Text>
        </View>
      </Card>
    )
  }

  //顶部蓝栏
  TopBar = (item) => {
    //计划生产日期
    if (item.planDate) {
      return (
        <LinearGradient style={{ width: width - 26, borderRadius: 5, marginTop: 8 }} colors={['#97BBFF', '#419FFF',]}>
          <View style={item.planDate ? { flex: 1 } : { height: 0 }}>
            <Text style={styles.topbartext}>任务编号：{item.compCode}</Text>
            <Text style={styles.topbartext}>计划生产日期：{item.planDate}</Text>
          </View>
        </LinearGradient >
      )
    } // 剩余信息都在这里，还需要优化
    else if (item.produceDate || item.releaseDate) {
      return (
        <LinearGradient style={{ width: width - 26, borderRadius: 5, marginTop: 8 }} colors={['#97BBFF', '#419FFF',]}>
          {/* 操作人与生产日期，有就显示，没有不显示，很多界面通用 */}
          <View style={item.planDate || item.releaseDate ? { flex: 1 } : styles.hidden}>
            <Text style={styles.topbartext}>操作人：{item.warehouEmpName ? item.warehouEmpName : item.releaseName}</Text>
            <Text style={styles.topbartext}>生产日期：{item.produceDate ? item.produceDate : item.releaseDate}</Text>
          </View>
          {/* 负责人班组长质检员，日生产记录页面 */}
          <View style={item.hidTime && item.editEmployeeName ? { flex: 1 } : styles.hidden}>
            <Text style={styles.topbartext}>负责人：{item.editEmployeeName}</Text>
            <Text style={styles.topbartext}>班组长：{item.teamleader}</Text>
            <Text style={styles.topbartext}>质检员：{item.member}</Text>
            <Text style={styles.topbartext}>生产日期：{item.produceDate}</Text>
          </View>
          {/* 报废孙子页面，使用报废详情说明判断 */}
          <View style={item.remark ? { flex: 1 } : styles.hidden}>
            <Text style={styles.topbartext}>报废单号：{item.compCode}</Text>
            <Text style={styles.topbartext}>报废来源：</Text>
            <Text style={styles.topbartext}>经办人：{item.operatorUser}</Text>
            <Text style={styles.topbartext}>报废日期：{item.scrapTime}</Text>
          </View>
          {/* 成品检查页面头部，可能和成品复检通用。 */}
          <View style={item.finChkTime ? { flex: 1 } : styles.hidden}>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ flexDirection: 'column', width: width / 1.5 }}>
                <Text style={styles.topbartext}>质检员：{item.member}</Text>
                <Text style={styles.topbartext}>成检日期：{item.finChkTime}</Text>
              </View>
              <Button
                titleStyle={{ color: 'white', fontSize: 18 }}
                containerStyle={{ justifyContent: 'space-around', alignContent: 'flex-end' }}
                title={item.insresults}
                icon = {<Badge
                  status = {item.insresults == '合格' ? 'success' : null}
                  containerStyle = {{marginHorizontal: 4}} 
                   />}
                iconContainerStyle = {{marginHorizontal: 4}}
                type='clear' />
            </View>
          </View>
        </LinearGradient>
      )
    }
  }

  //分隔栏
  _separator = () => {
    return <View style={{ height: 2, backgroundColor: '#fffffc' }} />;
  }

  //主页面渲染
  render() {
    //获取孙子数据
    const { navigation } = this.props;
    const item = navigation.getParam('item')
    const url = navigation.getParam('url');
    console.log(url)
    // 正常通用界面
    return (
      <ScrollView >
        {/* 顶部组件 */}
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', borderRadius: 12 }}>
          {this.TopBar(item)}
        </View>
        {/* 构件类型 */}
        <View style={{ marginTop: 12 }}>
          <Text style={{ color: 'gray', fontSize: 16, marginHorizontal: 16 }}>{item.compTypeName}</Text>
        </View>
        {/* 构件基本信息，大概所有页面通用 */}
        <View style={{ flex: 3 }}>
          <Card
            title={'项目:' + item.projectName}
            titleStyle={styles.CardTitle}
            containerStyle={styles.contrain}>
            <Text style={styles.textname}>产品编号：
              <Text style={styles.text}> {item.compCode}</Text>
            </Text>
            <Text style={styles.textname}>设计型号：
            <Text style={styles.text}>{item.designType}</Text>
            </Text>
            <Text style={styles.textname}>楼号：
            <Text style={styles.text}>{item.floorNoName}</Text>
            </Text>
            <Text style={styles.textname}>层号：
            <Text style={styles.text}>{item.floorName}</Text>
            </Text>
            <Text style={styles.textname}>体积：
            <Text style={styles.text}>{item.volume}m³</Text>
            </Text>
            <Text style={styles.textname}>重量：
            <Text style={styles.text}>{item.weight}吨</Text>
            </Text>
            <Text style={styles.textname}>尺寸：
            <Text style={styles.text}>{item.size}</Text>
            </Text>
            <Text style={styles.textname}>砼强度：
            <Text style={styles.text}>{item.concreteGrade}</Text>
            </Text>

            {/* 需不需要显示生产日期和生产厂家 */}
            <View style={item.releaseDate ? { flex: 1 } : { height: 0 }}>
              <Text style={styles.textname}>生产日期：
            <Text style={styles.text}>{item.releaseDate}</Text>
              </Text>
              <Text style={styles.textname}>生产厂家：
            <Text style={styles.text}>{item.factoryName}</Text>
              </Text>
            </View>

            {/* 有没有生产线信息 */}
            <View style={item.productLineName ? { flex: 1 } : { height: 0 }}>
              <Text style={styles.textname}>生产线：
            <Text style={styles.text}>{item.productLineName}</Text>
              </Text>
              <Text style={styles.textname}>专用模具：
            <Text style={styles.text}>{item.spetaiWanCode}</Text>
              </Text>
              <Text style={styles.textname}>模台：
            <Text style={styles.text}>{item.taiWanName}</Text>
              </Text>
              <Text style={styles.textname}>钢筋笼编号：
            <Text style={styles.text}></Text>
              </Text>
              <Text style={styles.textname}>承包队：
            <Text style={styles.text}>{item.labourName}</Text>
              </Text>
              <Text style={styles.textname}>生产厂家：
            <Text style={styles.text}></Text>
              </Text>
              <Text style={styles.textname}>合格证：
            <Text style={styles.text}></Text>
              </Text>
            </View>
          </Card>
        </View>

        {/* 判断是否显示构件报废记录 */}
        {item.remark ? this.remark(item) : <View></View>}

        {/* 构件图片是否显示 */}
      {/*   <View style={item.imgUrl ? { flex: 4 } : { height: 0 }}>
          {item.imgUrl ? this.ImgSourse(item) : <View></View>}
        </View> */}

        {/* 构件报废图片，但是看情况要改，具体需要看情况 */}
       {/*  <View style={item.remark ? { flex: 4 } : { height: 0 }}>
          {item.compImgUrl ? this.scrapImgSourse(item) : <View></View>}
        </View> */}
        {/*  成品检查构件图片  */}
       {/*  <View style={url.indexOf('GetQualityFinProducts') != -1 || url.search('GetQualityFinProducts') != -1 ? { flex: 4 } : { height: 0 }}>
          {url.search('GetQualityFinProducts') != -1 || url.search('GetQualityFinProducts') != -1 ? this.FinProductsImgSourse(item) : <View></View>}
        </View> */}

        {/* 子卡片2，控制是否显示 */}
        <View style={(item.hidTime && item.putMoldTime) ? { flex: 4 } : { height: 0 }}>
          <View style={{ marginTop: 12 }}>
            <Text style={{ color: 'gray', fontSize: 16, marginHorizontal: 16 }}>生产过程记录</Text>
          </View>
          <View style={{ flex: 3 }}>
            {item.hidTime ? this.CardSource2(item) : <View></View>}
          </View>
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  contrain: {
    flex: 4,
    flexDirection: 'column',
    //alignContent: 'center',
    //alignItems: 'center',
    //justifyContent: 'center',
    borderRadius: 15
  },
  text2: {
    flex: 1,
    fontSize: 14,
    marginEnd: 12,
    marginBottom: 12,
    color: '#454545'
    //fontFamily:'STKaiti',
    //fontWeight: 'bold',
  },
  text: {
    flex: 2,
    fontSize: 15,
    marginEnd: 12,
    marginBottom: 12,
    color: '#454545'
    //fontFamily:'STKaiti',
    //fontWeight: 'bold',
  },
  textname: {
    flex: 1.3,
    fontSize: 14,
    marginEnd: 12,
    marginBottom: 12,
    color: 'gray'
  },
  topbartext: {
    flex: 1,
    fontSize: 16,
    marginTop: 12,
    marginBottom: 12,
    marginHorizontal: 10,
    color: 'white'
  },
  topbartopcenter: {
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
    color: 'white',
    marginTop: 4,
    marginBottom: 4
  },
  topbarbottomcenter: {
    textAlign: 'center',
    fontSize: 14,
    color: 'white',
    marginTop: 4,
    marginBottom: 4
  },
  CardTitle: {
    fontSize: 20,
  },
  textView: {
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  hidden: {
    flex: 0,
    height: 0,
    width: 0,
  }
})