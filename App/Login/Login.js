import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Alert,
  Dimensions,
  Text,
  AsyncStorage,
  Platform,
  Linking,
  ImageBackground,
  TouchableOpacity,
  ActivityIndicator,
  KeyboardAvoidingView,
  StatusBar,
  PermissionsAndroid,
  status,
  TouchableHighlight,
  TouchableNativeFeedback,
  DeviceEventEmitter
} from 'react-native';
import {
  isFirstTime,
  isRolledBack,
  packageVersion,
  currentVersion,
  checkUpdate,
  downloadUpdate,
  switchVersion,
  switchVersionLater,
  markSuccess,
} from 'react-native-update';
import { Button, Input, Image, Icon, Card, Overlay, Header, BottomSheet, ListItem } from 'react-native-elements'
//import Button from './Components/Button';
import { inject, observer } from 'mobx-react';
import NetInfo from '@react-native-community/netinfo'
import { WebView } from 'react-native-webview'
import Toast from 'react-native-easy-toast';
import TextButton from './Components/TextButton';
import TextField from './Components/TextField';
import Url from '../Url/Url';
import DeviceStorage from '../Url/DeviceStorage';
import _updateConfig from '../../update.json';
import request from '../Url/request';
import navigationUtil from '../Url/navigation';
import Wave from './Components/Wave'
import JPush from 'jpush-react-native';
import { RVW, RVH } from '../Url/Pixal';
import { RFT, deviceWidth } from '../Url/Pixal';

import PushUtil from '../CommonComponents/PushUtil'
import { ScrollView } from 'react-navigation';


const { appKey } = _updateConfig[Platform.OS];
const { height, width } = Dimensions.get('window');
let userInfo = {};
const statuscolor = 'rgba(52,158,246)'
console.log(Url.url)

let count = 0;

let loadingText = ''

function HashMap() {
  this.map = {};
}

HashMap.prototype = {
  put: function (key, value) {// 向Map中增加元素（key, value) 
    this.map[key] = value;
  },
  get: function (key) { //获取指定Key的元素值Value，失败返回Null 
    if (this.map.hasOwnProperty(key)) {
      return this.map[key];
    }
    return null;
  },
  remove: function (key) { // 删除指定Key的元素，成功返回True，失败返回False
    if (this.map.hasOwnProperty(key)) {
      return delete this.map[key];
    }
    return false;
  },
  removeAll: function () {  //清空HashMap所有元素
    this.map = {};
  },
  keySet: function () { //获取Map中所有KEY的数组（Array） 
    var _keys = [];
    for (var i in this.map) {
      _keys.push(i);
    }
    return _keys;
  }
};

HashMap.prototype.constructor = HashMap;

class Login extends Component {
  constructor() {
    super();
    this.state = {
      logourl: '',
      sitename: '智慧工厂管理系统',
      userInfo: [],
      resDataID: '',
      ID: '',
      isVisible: true,
      isLoading: true,
      logLoading: false,
      isLogin: false,
      username: '',
      password: '',
      deptId: '',
      deptName: '',
      isTrue: false,
      resData: null,
      FacName: '',
      lastUserInfo: false,
      secretVisible: false,
      selVisible: false,
      press: false,
      isDoubleAuthor: false,
      APPAuthor: false,
      PDAAuthor: false,
      PDAaddress: '',
      bottomFacVisible: false,
      PDAselFac: [],
      PDAFacArr: [],
      bottomGlorySFFacVisible: false,
      isXMCXapp: false,
      isErrorID: false
    }
    //this.login = this.login.bind(this);
    this.getInfo = this.getInfo.bind(this);
  }

  //页面预加载信息
  getInfo = () => {
    DeviceStorage.get('count')
      .then(res => {
        if (res) {
          count = res
          //console.log(res)
        }
      }).catch(err => {
        Alert.alert('错误', 'count' + err.toString())
      })
    DeviceStorage.get('logourl')
      .then(res => {
        if (res) {
          this.setState({
            logourl: res
          })
          //console.log(res)
        }
      }).catch(err => {
        Alert.alert('错误', 'logourl' + err.toString())
      })
    DeviceStorage.get('sitename')
      .then(res => {
        if (res) {
          this.setState({
            sitename: res
          })
          //console.log(res)
        }
      }).catch(err => {
        Alert.alert('错误', 'sitename' + err.toString())
      })
    DeviceStorage.get('ID')
      .then(res => {
        if (res) {
          this.setState({
            ID: res,
            IDLast: res
          })
          Url.ID = res
          //console.log(res)
        }
      }).catch(err => {
        Alert.alert('错误', 'ID' + err.toString())
      })
    DeviceStorage.get('employeeIdAlias')
      .then(res => {
        if (res) {
          Url.employeeIdAlias = res
          //console.log(res)
        }
      }).catch(err => {
        Alert.alert('错误', 'employeeIdAlias' + err.toString())
      })
    DeviceStorage.get('employeeName')
      .then(res => {
        if (res) {
          Url.employeeName = res
          //console.log(res)
        }
      }).catch(err => {
        Alert.alert('错误', 'employeeName' + err.toString())
      })
    DeviceStorage.get('username')
      .then(resuser => {
        if (resuser) {
          this.setState({
            username: resuser
          })
          Url.username = resuser
          //console.log(this.state.username)
        }
      }).catch(err => {
        Alert.alert('错误', 'username' + err.toString())
      });
    DeviceStorage.get('password')
      .then(respass => {
        if (respass) {
          this.setState({
            password: respass
          })
          Url.password = respass
          //console.log(respass)
        }
      }).catch(err => {
        Alert.alert('错误', 'password' + err.toString())
      })
    DeviceStorage.get('FacName')
      .then(resfac => {
        if (resfac) {
          this.setState({
            FacName: resfac
          })
          //console.log(respass)
        }
      }).catch(err => {
        Alert.alert('错误', 'FacName' + err.toString())

      })
    DeviceStorage.get('url')
      .then(resUrl => {
        if (resUrl) {
          Url.url = resUrl
          //Url.url = 'http://121.36.55.207:9102/GlorySF_GP/api/'
          //console.log(respass)
        }
      }).catch(err => {
        Alert.alert('错误', 'url' + err.toString())
      })
    DeviceStorage.get('urlsys')
      .then(res => {
        if (res) {
          Url.urlsys = res
        }
      }).catch(err => {
        Alert.alert('错误', 'urlsys' + err.toString())
      })
    DeviceStorage.get('Fid')
      .then(res => {
        if (res) {
          Url.Fid = res
        }
      }).catch(err => {
        Alert.alert('错误', 'Fid' + err.toString())
      })
    DeviceStorage.get('Fidweb')
      .then(res => {
        if (res) {
          Url.Fidweb = res
        }
      }).catch(err => {
        Alert.alert('错误', 'Fidweb' + err.toString())

      })
    DeviceStorage.get('deptCode')
      .then(res => {
        if (res) {
          Url.deptCode = res
        }
      }).catch(err => {
        Alert.alert('错误', 'deptCode' + err.toString())
      })
    DeviceStorage.get('Factoryname')
      .then(res => {
        if (res) {
          Url.Factoryname = res
        }
      }).catch(err => {
        Alert.alert('错误', 'Factoryname' + err.toString())

      })
    DeviceStorage.get('EmployeeId')
      .then(res => {
        if (res) {
          Url.EmployeeId = res
        }
      }).catch(err => {
        Alert.alert('错误', 'EmployeeId' + err.toString())
      })
    DeviceStorage.get('Menu')
      .then(res => {
        if (res) {
          Url.Menu = res
          if (Url.Menu.indexOf('APP日常') != -1) {
            DeviceStorage.save('daily', true);
            Url.Daily = true
          } else {
            DeviceStorage.save('daily', false);
            Url.Daily = false
          }
          if (Url.Menu.indexOf('APP项目') != -1) {
            DeviceStorage.save('project', true);
            Url.Project = true
          } else {
            DeviceStorage.save('project', false);
            Url.Project = false
          }
          if (Url.Menu.indexOf('APP堆场') != -1) {
            DeviceStorage.save('stocks', true)
            Url.Stocks = true
          } else {
            DeviceStorage.save('stocks', false);
            Url.Stocks = false
          }
          if (Url.Menu.indexOf('APP经营') != -1) {
            DeviceStorage.save('business', true);
            Url.Business = true
          } else {
            DeviceStorage.save('business', false);
            Url.Business = false
          }
          if (Url.Menu.indexOf('APP模型') != -1) {
            DeviceStorage.save('model', true);
            Url.Model = true
          } else {
            DeviceStorage.save('model', false);
            Url.Model = false
          }
          if (Url.Menu.indexOf('APP切换工厂') != -1 || Url.Menu.indexOf('APP切换业务范围') != -1) {
            DeviceStorage.save('changeFac', true);
            Url.ChangeFac = true
          } else {
            DeviceStorage.save('changeFac', false);
            Url.ChangeFac = false
          }
        }
      }).catch(err => {
        Alert.alert('错误', 'Menu' + err.toString())
      })
    DeviceStorage.get('PDAurl')
      .then(resUrl => {
        if (resUrl) {
          Url.PDAurl = resUrl
          //console.log(respass)
        }
      }).catch(err => {
        Alert.alert('错误', 'PDAurl' + err.toString())
      })
    DeviceStorage.get('workSpaceSearchList')
      .then(resData => {
        if (resData) {
          Url.employeeList = resData.lstEmployee
          Url.compTypeNameList = resData.lstCompType
          Url.compRoomNameList = resData.lstLibrary
          Url.bigStateList = resData.lstCompState
          Url.productLineNameList = resData.lstProductionLine
        }
      }).catch(err => {
        Alert.alert('错误', 'workSpaceSearchList' + err.toString())
      })
    DeviceStorage.get('isLogin')
      .then(resisLogin => {
        if (resisLogin) {
          let encodePassword = encodeURIComponent(this.state.password);
          const accountUrl = Url.url + '/User/' + this.state.username + '/CheckLogin/' + encodePassword;
          console.log("🚀 ~ file: Login.js:331 ~ Login ~ fetch ~ accountUrl:", accountUrl)
          fetch(accountUrl).then(res => {
            if (res.ok) {
              return res.text();
            }
          }).then(resData => {
            console.log("🚀 ~ file: Login.js:1624 ~ Login ~ fetch ~ resData", resData)
            if (resData === 'true') {
              DeviceStorage.get('isStaticTypeOpen')
                .then(res => {
                  if (res == true) {
                    Url.isStaticTypeOpen = true
                    DeviceStorage.get('staticType')
                      .then(resType => {
                        if (typeof resType != 'undefined') {
                          if (resType == '房建') {
                            Url.StaticType = '0/'
                          }
                          if (resType == '管片') {
                            Url.StaticType = '1/'
                          }
                          if (resType == '批量') {
                            Url.StaticType = '2/'
                          }
                        } else {
                          Url.StaticType = '0/'
                        }
                      })
                      .catch(err => {
                        Alert.alert('错误', '获取数据统计类型' + err.toString())
                      })
                  } else {
                    Url.isStaticTypeOpen = false
                    Url.StaticType = ''
                    console.log("🚀 ~ file: Login.js:392 ~ Login ~ Url.StaticType:", Url.StaticType)
                    console.log("🚀 ~ file: Login.js:391 ~ Login ~ Url.isStaticTypeOpen:", Url.isStaticTypeOpen)
                  }
                }).catch(err => {
                  Alert.alert('错误', '获取数据统计类型' + err.toString())
                })
              this.setState({
                isLogin: resisLogin
              })
              this.setState({ logLoading: false })
              this.GetUnreadMassage();
              let isProcess = Url.Menu.indexOf('APP流程') != -1
              if (isProcess && this.state.ID != 'pkpmfb') {
                this.props.navigation.navigate('MainProcess');
              } else {
                this.props.navigation.navigate('Main');
              }
            }
          })
        }
      }).catch(err => { })
  }

  componentWillMount() {

    this.getInfo();
    console.log("🚀 ~ file: Login.js:360 ~ Login ~ componentWillMount ~ count:", count)
    console.log("🚀 ~ file: Login.js:360 ~ Login ~ componentWillMount ~ this.state.ID != '':", this.state.ID != '')

    if (this.state.ID != '' && count == 0) {
      this.GetIPcustom()
      count++
      DeviceStorage.save('count', count);
    }
    if (this.state.ID != '' && (Url.url.length == 0 || Url.PDAurl.length == 0)) {
      this.GetIPcustom()
    }
  }

  //网易云信
  checkAccount = () => {
    let { account } = this.state;
    if (account.trim() === '' || account.length > 20) {
      this.toast.show('账号长度错误');
      return
    }
    return true
  }

  //网易云信
  checkPwd = () => {
    let { password } = this.state;
    if (password.trim() === '' || password.length < 6 || password.length > 20) {
      this.toast.show('密码长度错误');
      return
    }
    return true
  }


  componentDidMount() {
    PushUtil.listTag((code, tagList) => {
      if (code == 200) {
        let tags = "tags:";
        console.log("PushUtil xxxxxx length=" + tagList.length)
        for (var i = 0; i < tagList.length; i++) {
          tags = tags + tagList[i] + "\n";
          console.log("PushUtil xxxxxx tags=" + tags)
        }
      }
    })

    PushUtil.appInfo((result) => {
    })

    try {
      const granted = PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: '申请摄像头权限',
          message:
            '智慧工厂app需要使用你的摄像头，' +
            '用于扫描构件信息，构件拍摄等功能',
          buttonNeutral: '下次再开启',
          buttonNegative: '拒绝',
          buttonPositive: '同意',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('现在你获得摄像头权限了');
      } else {
        console.log('用户并不屌你');
        this.props.navigation.goBack()
      }
    } catch (err) {
      console.warn(err);
    }

    this.checkUpdate();

    DeviceStorage.get('isFirst')
      .then(res => {
        console.log("Login -> componentDidMount -> res", res)
        if (res == false) {
          this.setState({
            secretVisible: false
          })
          //console.log(res)
        } else {
          this.setState({
            secretVisible: true
          })
        }
      }).catch(err => { })

    DeviceStorage.save('daily', false);
    DeviceStorage.save('project', false);
    DeviceStorage.save('stocks', false);
    DeviceStorage.save('business', false);
    DeviceStorage.save('model', false);
    DeviceStorage.save('changeFac', false);

    // 智慧看板收到随身工厂消息跳转登录
    DeviceStorage.get('changeModelPDA').then(res => {
      if (res) {
        this.login1
      }
    }).catch(err => {
      Alert.alert('错误', err.toString())
      console.log("🚀 ~ file: Login.js:460 ~ Login ~ DeviceStorage.get ~ err:", err)
    })
  }

  doUpdate = info => {
    downloadUpdate(info).then(hash => {
      Alert.alert('提示', '下载完毕,是否重启应用?', [
        { text: '是', onPress: () => { switchVersion(hash); } },
        { text: '否', },
        { text: '下次启动时', onPress: () => { switchVersionLater(hash); } },
      ]).catch(err => {
        Alert.alert('提示', '更新失败.');
      });
    })
  }

  checkUpdate = () => {
    //console.log(packageVersion)
    //console.log(currentVersion)
    checkUpdate(appKey).then(info => {
      if (info.expired && Platform.OS === 'android') {
        Alert.alert('提示', '您的应用版本已更新,请前往应用商店下载新的版本', [
          {
            text: '确定', onPress: () => {
              console.log(info.downloadUrl)
              info.downloadUrl && Linking.openURL(info.downloadUrl).catch(err => console.log('An error occurred', err));
            }
          },
          { text: '下次更新' },
        ]);
      } else if (info.upToDate) {
        //Alert.alert('提示', '您的应用版本已是最新.');
      } else if (Platform.OS === 'android') {
        Alert.alert('提示', '检查到新的版本' + info.name + ',是否下载?\n' + info.description, [
          { text: '是', onPress: () => { this.doUpdate(info) } },
          { text: '否', },
        ]);
      }
    }).catch(err => {
      //Alert.alert('提示', '更新失败.');
    });
  };


  render() {
    const { navigation } = this.props;
    const { username, password, ID, logLoading, logourl, sitename } = this.state;
    //console.log(ID)
    /* if (logLoading) {
      return (
        <ImageBackground style={styles.loading}
          source={require('../viewImg/Login_1248_2208.jpg')}>
          <ActivityIndicator
            animating={true}
            color='#FFFFFF'
            size="large" />
          <Toast ref="Visiter" position='center' />
        </ImageBackground>
      )
    } else { */
    return (
      <ImageBackground style={{ flex: 1 }}
        source={require('../viewImg/Login_1080_2340_new.jpg')} >
        <StatusBar backgroundColor={'black'} />
        <Toast ref={(ref) => { this.toast = ref; }} position="center" />
        <Toast ref="Visiter" position='center' />

        <KeyboardAvoidingView behavior="padding" style={styles.container}>
          <View style={styles.topFlex}></View>
          <View style={styles.imageFlex}>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ flex: 1 }}></View>
              <View style={{ flex: 1 }}>
                <Image
                  containerStyle={{}}
                  resizeMethod='resize'
                  resizeMode='contain'
                  style={{ height: 320 / 3, width: width * 0.33 }}
                  source={this.state.logourl == '' ? require('../viewImg/LOGO.png') : { uri: this.state.logourl }}

                />
              </View>
              <View style={{ flex: 1 }}></View>
            </View>
          </View>
          <Text style={[styles.titleFlex]} numberOfLines={1}>{sitename}</Text>
          <Text style={styles.engTitleFlex}>PKPM Smart Factory</Text>
          <View style={{ flex: 15, marginLeft: 25, marginRight: 25, paddingHorizontal: 16, }}>
            <TextField
              ref={(ref) => { this.ID = ref; }}
              leftIcon={<Icon name='solution1' color='rgba(255,255,255,0.9)' type='antdesign' />}
              labelStyle={{ textAlign: 'center', fontSize: 16 }}
              placeholder='编号'
              value={ID}
              returnKeyType='search'
              renderErrorMessage={false}
              rightIcon={
                <TouchableOpacity
                  onPress={() => {
                    this.GetIPcustom()
                  }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Icon
                      name='search1'
                      color='rgba(255,255,255,0.9)'
                      type='antdesign'
                      iconStyle={{ top: 3, }}
                      size={16}
                    />
                    <Text style={{ color: 'white', fontSize: 16, marginLeft: 10 }}>搜索</Text>
                  </View>
                </TouchableOpacity>
              }
              onChange={value => {
                this.setState({
                  ID: value,
                })
              }}
              onSubmitEditing={() => {
                this.GetIPcustom()
              }}
            //errorMessage = "未获取现场地址"
            />
            <TextField
              leftIcon={<Icon name='user' color='rgba(255,255,255,0.9)' type='antdesign' />}
              labelStyle={{ textAlign: 'center', fontSize: 16 }}
              placeholder='用户名'
              returnKeyType='next'
              value={username}
              onChange={value => this.setState({ username: value })}
            />
            <TextField
              leftIcon={<Icon name='key' color='rgba(255,255,255,0.9)' type='antdesign' />}
              type='password'
              placeholder='请输入登录密码'
              value={password}
              returnKeyType='next'
              onChange={value => this.setState({ password: value })}
            />

            <Button
              disabled={!username || !password}
              //isDisabled={!username || !password}
              onPress={this.login1}
              title='登录'
              type='outline'
              titleStyle={{ fontSize: 18 }}
              style={{ marginVertical: 6 }}
              containerStyle={{ marginVertical: 10 }}
              buttonStyle={{ backgroundColor: '#FFFFFF', height: 50 }}
            ></Button>

            <View style={styles.textBtnContainer}>
              <TextButton
                disabled={false}
                onPress={() => navigation.navigate('Register')}
              >新用户注册</TextButton>
              <TextButton disabled={true}>忘记密码？</TextButton>
            </View>
          </View>
        </KeyboardAvoidingView >


        <Overlay
          fullScreen={false}
          overlayStyle={{
            width: deviceWidth * 0.7,
            height: deviceWidth * 0.9
          }}
          animationType='fade'
          isVisible={this.state.secretVisible}
          onRequestClose={() => {
            // this.setState({ secretVisible: !this.state.secretVisible });
          }} >
          {/* <Header
              centerComponent={{ text: '免责声明', style: { color: 'gray', fontSize: 20 } }}
              rightComponent={<BackIcon
                onPress={() => {
                  this.setState({
                    secretVisible: !this.state.secretVisible
                  });
                }} />}
              backgroundColor='white'
            /> */}

          <SecretPage />
          <View style={{ flexDirection: 'row', marginBottom: RFT * 2, marginTop: RFT * 2, justifyContent: 'flex-end' }}>
            <Icon
              name={this.state.press ? 'checkcircle' : 'checkcircleo'}
              color={this.state.press ? '#419FFF' : '#999999'}
              type='antdesign'
              size={16}
              containerStyle={{ justifyContent: 'center' }}
              onPress={() => {
                this.setState({ press: !this.state.press })
              }} />
            <Text
              style={[styles.secret, this.state.press ? { color: '#419FFF' } : { color: '#999999' }]}
              onPress={() => {
                this.setState({ press: !this.state.press })
              }}>我已阅读并同意此协议</Text>

          </View>
          <Button
            disabled={!this.state.press}
            onPress={() => {
              this.setState({ secretVisible: !this.state.secretVisible })
              DeviceStorage.save('isFirst', false);
            }}
            title='同意'
            type='outline'
            style={{ marginVertical: 10 }}
            containerStyle={{ marginVertical: 10 }}
            buttonStyle={{ backgroundColor: '#FFFFFF', borderRadius: 60 }}
          ></Button>

        </Overlay>

        <Overlay
          fullScreen={true}
          overlayStyle={{
            //width: width * 0.55,
            //height: width * 1.1,
            backgroundColor: 'transparent',
            shadowColor: 'transparent',
            shadowOpacity: 0,
            shadowOffset: 0,
            borderColor: 'transparent',
            borderWidth: 0
          }}
          style={{
            backgroundColor: 'transparent',
            shadowColor: 'transparent',
            shadowOpacity: 0,
            shadowOffset: 0,
            backfaceVisibility: 'hidden'
          }}

          onBackdropPress={() => { this.setState({ selVisible: !this.state.selVisible }); }}
          animationType='fade'
          isVisible={this.state.selVisible}
          onRequestClose={() => {
            this.setState({ selVisible: !this.state.selVisible });
          }} >
          <View style={styles.loginSelView}>
            <TouchableOpacity
              onPress={() => {
                if (Url.url.length > 0) {
                  this.setState({
                    logLoading: true,
                    selVisible: false
                  })
                  let encodePassword = encodeURIComponent(this.state.password);

                  const accountUrl = Url.url + '/User/' + this.state.username + '/CheckLogin/' + encodePassword;
                  console.log("🚀 ~ file: Login.js ~ line 684 ~ Login ~ render ~ accountUrl", accountUrl)

                  this.accountLogin(accountUrl);
                } else {
                  const IDurl = 'https://smart.pkpm.cn:910/api/FactoryIPMange/' + this.state.ID + '/GetBaseUrl';
                  this.IDLogin(IDurl);
                }
              }}
            >
              <Card
                containerStyle={styles.loginSelCard}
              >
                <Image
                  resizeMethod='scale'
                  resizeMode='contain'
                  containerStyle={{ justifyContent: 'center', alignContent: 'center' }}
                  style={styles.loginSelCard_Image}
                  source={require('./img/smart.png')}
                />
                <Text style={{ fontSize: 18, textAlign: 'center', marginBottom: 20 }}>智慧看板</Text>

              </Card>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                this.setState({
                  logLoading: true,
                  selVisible: false
                }, () => {
                  if (Url.PDAurl.length > 0) {
                    this.setState({
                      logLoading: true
                    })
                    this.PDALoginNEW()
                  } else {
                    this.PDAIDget()
                  }

                })
              }}
            >
              <Card
                containerStyle={styles.loginSelCard}
              >
                <Image
                  resizeMethod='scale'
                  resizeMode='contain'
                  containerStyle={{ justifyContent: 'center', alignContent: 'center' }}
                  style={styles.loginSelCard_Image}
                  source={require('./img/factory.png')}
                />
                <Text style={{ fontSize: 18, textAlign: 'center', marginBottom: 20 }}>随身工厂</Text>

              </Card>
            </TouchableOpacity>
          </View>
        </Overlay>

        <Overlay
          fullScreen={true}
          overlayStyle={{
            //width: width * 0.55,
            //height: width * 1.1,
            backgroundColor: 'transparent',
            shadowColor: 'transparent',
            shadowOpacity: 0,
            shadowOffset: 0,
            borderColor: 'transparent',
            borderWidth: 0
          }}
          style={{
            backgroundColor: 'transparent',
            shadowColor: 'transparent',
            shadowOpacity: 0,
            shadowOffset: 0,
            backfaceVisibility: 'hidden'
          }}
          animationType='fade'
          isVisible={this.state.logLoading}
        >
          <ActivityIndicator
            animating={true}
            color='#FFFFFF'
            size="large"
            style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }} />
        </Overlay>

        {/* 智慧看板选择工厂 */}
        <BottomSheet isVisible={this.state.bottomGlorySFFacVisible}>
          <ScrollView style={{ height: 290 }}>
            {
              this.state.userInfo.map((item, index) => {
                if (item.deptType == 40) {
                  return (
                    <TouchableHighlight
                      onPress={() => {

                        DeviceStorage.save('user', item.userName);
                        DeviceStorage.save('FacName', item.deptName);
                        DeviceStorage.save('employeeName', item.employeeName);

                        const sitecodeUrl = Url.url + 'User/' + 'GetSiteCode';

                        Url.employeeName = item.employeeName

                        this.GetSiteCode(sitecodeUrl, item.employeeId, item.deptCode);

                        authorityUrl = Url.url + '/User/' + '/Employeeid/' + item.employeeId + '/Deptid/' + item.deptId + '/GetFacMenu';

                        DeviceStorage.save('isLogin', true);
                        this.authorityLogin(authorityUrl);

                        Url.Fid = item.deptId + '/';
                        Url.Fidweb = item.deptId;
                        Url.Factoryname = item.deptName;
                        Url.EmployeeId = item.employeeId;
                        this.GetCompInfo()
                        DeviceStorage.save('Fid', item.deptId + '/');
                        DeviceStorage.save('Fidweb', item.deptId);
                        DeviceStorage.save('Factoryname', item.deptName);
                        DeviceStorage.save('EmployeeId', item.employeeId);
                        this.setState({
                          bottomGlorySFFacVisible: false
                        })

                        this.requestLibraryDetail()

                        this.setState({
                          isLoading: false,
                          isLogin: true
                        })
                      }}
                    >
                      <View style={{
                        paddingVertical: 18,
                        backgroundColor: 'white',
                        alignItems: 'center',
                        borderBottomColor: '#DDDDDD',
                        borderBottomWidth: 1,
                      }}>
                        <Text style={{ color: 'black', textAlign: 'center', fontSize: 14, textAlignVertical: 'center' }}>{item.deptName}</Text>
                      </View>
                    </TouchableHighlight>
                  )
                }
              })
            }
          </ScrollView>
          <TouchableHighlight onPress={() => {
            this.setState({ bottomGlorySFFacVisible: false })
          }} >
            <View style={{
              paddingVertical: 18,
              backgroundColor: '#e74c3c',
              alignItems: 'center',
              borderBottomColor: '#DDDDDD',
              borderBottomWidth: 1,
            }}>
              <Text style={{ color: 'white', textAlign: 'center', fontSize: 14, textAlignVertical: 'center' }}>{'关闭'}</Text>
            </View>
          </TouchableHighlight>
        </BottomSheet>

        {/* 随身工厂选择工厂 */}
        <BottomSheet isVisible={this.state.bottomFacVisible}>
          <ScrollView style={{ height: 290 }}>
            {
              this.state.PDAFacArr.map((item, index) => {
                return (
                  <TouchableHighlight
                    onPress={() => {
                      Url.PDAFid = item.factoryId
                      Url.PDAFname = item.factoryName
                      Url.PDAEmployeeId = item.EmployeeId
                      Url.PDAEmployeeName = this.state.ID == 'pkpmfb' ? this.state.username : item.EmployeeName
                      Url.PDAusername = this.state.username
                      Url.chengJianPaiZhao = item.chengJianPaiZhao
                      Url.isUsedDeliveryPlan = item.isUsedDeliveryPlan
                      Url.jiaoZhuPaiZhao = item.jiaoZhuPaiZhao || "YES"
                      Url.yinJianPaiZhao = item.yinJianPaiZhao || "YES"
                      Url.isUseTaiWanID = item.isUseTaiWanID || "YES"
                      Url.isResizePhoto = item.isResizePhoto || "600"
                      Url.factoryCode = item.factoryCode || ""

                      if (typeof item.isUsedSpareParts != 'undefined') {
                        Url.isUsedSpareParts = item.isUsedSpareParts.split(",") || ""
                      }

                      if (typeof item.appPhotoSetting != 'undefined') {
                        let appPhotoSetting = []
                        Url.appPhotoSetting = []
                        appPhotoSetting = item.appPhotoSetting.split(",") || []
                        appPhotoSetting.map(item => {
                          Url.appPhotoSetting.push(item + '0')
                        })
                        let appDateSetting = []
                        Url.appDateSetting = []
                        appDateSetting = item.appDateSetting.split(",") || []
                        appDateSetting.map(item => {
                          Url.appDateSetting.push(item + '0')
                        })
                        let appPhotoAlbumSetting = []
                        Url.appPhotoAlbumSetting = []
                        appPhotoAlbumSetting = item.appPhotoAlbumSetting.split(",") || []
                        appPhotoAlbumSetting.map(item => {
                          Url.appPhotoAlbumSetting.push(item + '0')
                        })
                      }
                      if (typeof item.isUsePhotograph !== undefined) {
                        Url.isUsePhotograph = item.isUsePhotograph
                      }

                      let PDAauthorgray = [], gary = ''
                      gary = item.gray
                      PDAauthorgray = gary.split(",");
                      Url.PDAauthorgray = PDAauthorgray
                      Url.PDAnews = item.news
                      this.setState({
                        bottomFacVisible: false
                      })
                      // 接收消息相关配置
                      const sitecodeUrl = Url.url + 'User/' + 'GetSiteCode';
                      this.GetSiteCode(sitecodeUrl, Url.PDAEmployeeId, Url.factoryCode);
                      this.props.navigation.navigate('Main1');
                      //this.PDALogin(l.FactoryId)
                    }}
                  >
                    <View style={{
                      paddingVertical: 18,
                      backgroundColor: 'white',
                      alignItems: 'center',
                      borderBottomColor: '#DDDDDD',
                      borderBottomWidth: 1,
                    }}>
                      <Text style={{ color: 'black', textAlign: 'center', fontSize: 14, textAlignVertical: 'center' }}>{item.factoryName}</Text>
                    </View>
                  </TouchableHighlight>
                )

              })
            }
          </ScrollView>

          <TouchableHighlight onPress={() => {
            this.setState({ bottomFacVisible: false })
          }} >
            <View style={{
              paddingVertical: 18,
              backgroundColor: '#e74c3c',
              alignItems: 'center',
              borderBottomColor: '#DDDDDD',
              borderBottomWidth: 1,
            }}>
              <Text style={{ color: 'white', textAlign: 'center', fontSize: 14, textAlignVertical: 'center' }}>{'关闭'}</Text>
            </View>
          </TouchableHighlight>
        </BottomSheet>

      </ImageBackground>
    );
    //}

  }

  //用来验证是否为带管片统计的新版本
  requestLibraryDetail = () => {
    const url = Url.url + Url.Stock + Url.Fid + '0/' + 'GetLibraryDetail';
    console.log("🚀 ~ file: Login.js:1044 ~ Login ~ url:", url)
    fetch(url, {
      credentials: 'include',
    }).then(res => {
      if (res.status == 404) {
        return res.status
      }
      return res.json()
    }).then(resData => {
      console.log("🚀 ~ file: Login.js:10521 ~ _StockScreen ~ resData:", resData)
      if (resData == 404) {
        Url.StaticType = ''
        Url.isStaticTypeOpen = false
        DeviceStorage.save('isStaticTypeOpen', false);
      } else {
        DeviceStorage.save('isStaticTypeOpen', true);
        Url.isStaticTypeOpen = true
        DeviceStorage.get('staticType')
          .then(resType => {
            if (typeof resType != 'undefined') {
              if (resType == '房建') {
                Url.StaticType = '0/'
              }
              if (resType == '管片') {
                Url.StaticType = '1/'
              }
              if (resType == '批量') {
                Url.StaticType = '2/'
              }
            } else {
              Url.StaticType = '0/'
            }
          })
          .catch(err => {
            Alert.alert('错误', '获取数据统计类型' + err.toString())
          })
      }
    }).catch(err => {
      console.log("🚀 ~ file: Login.js:1052 ~ Login ~ err:", err)
    })
  }

  //获取现场地址
  GetIPcustom = () => {
    if (this.state.ID.length == 0) {
      this.toast.show('客户编码为空')
      return
    }
    this.setState({
      logLoading: true
    })
    //智慧看板
    const IDurl = 'https://smart.pkpm.cn:910/api/FactoryIPMange/' + this.state.ID + '/GetBaseUrl';
    console.log("🚀 ~ file: Login.js:932 ~ Login ~ IDurl", IDurl)
    const Sysurl = 'https://smart.pkpm.cn:910/api/FactoryIPMange/' + this.state.ID + '/GetSysUrl';
    fetch(IDurl, {
      credentials: 'include',
    }).then(resid => {
      console.log(resid.ok)
      return resid.text()
    }).catch((err) => {
      Alert.alert("error", err)
    }).then((text1) => {
      let text = text1
      console.log("🚀 ~ file: Login.js ~ line 873 ~ Login ~ fetch ~ text", text)
      this.setState({ resDataID: text });
      if (text.indexOf('请输入正确的用户编码') == -1) {

        Url.Imgurl = 'http://' + text
        if (text.indexOf('smart.pkpm.cn') != -1) {
          Url.url = 'https://' + text + '/api/';
        } else if (text.indexOf('https://') != -1) {
          Url.url = text + '/api/';
        } else {
          Url.url = 'http://' + text + '/api/';
        }
        this.GetSysLogo1()
      }
      else {
        this.setState({
          logLoading: false
        })
        this.setState({ ID: '' })
        //this.ID.focus()
        this.toast.show("请输入正确的用户编码", 1000)
      }
      DeviceStorage.save('url', Url.url || '');
      DeviceStorage.save('ID', this.state.ID || '');

    }).catch((err) => {
      this.setState({
        logLoading: false
      })
      Alert.alert('错误', err.toString())
      this.setState({
        logLoading: false
      })
      console.log("🚀 ~ file: Login.js:963 ~ Login ~ fetch ~ err", err)
    })

    //随身工厂
    let formData = new FormData();
    let data = {};
    // data = {
    //   "action": "GetSystemAddress",
    //   "servicetype": "pda",
    //   "express": "EAB711EA",
    //   "ciphertext": "22d37237b0d87bc362e198e9aec3f728",
    //   "data": {
    //     "customerCode": this.state.ID,
    //   }
    // }
    formData.append("action", "GetSystemAddress")
    formData.append("customerCode", this.state.ID)
    formData.append("express", "EAB711EA")
    formData.append("ciphertext", "22d37237b0d87bc362e198e9aec3f728")
    console.log("🚀 ~ file: Login.js:982 ~ Login ~ formData", formData)
    fetch('http://smart.pkpm.cn:81/pcservice/pdaservice.ashx', {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.status);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        Url.PDAurl = resData.result.systemAddress
        DeviceStorage.save('PDAurl', resData.result.systemAddress);
        console.log("🚀 ~ file: Login.js ~ line 1080 ~ Login ~ Url.PDAurl", Url.PDAurl)
        this.setState({
          PDAaddress: Url.PDAurl,
        })
        DeviceStorage.save('ID', this.state.ID);
      }
    }).catch((error) => {
      this.setState({
        logLoading: false
      })
      console.log("🚀 ~ file: Login.js:1006 ~ Login ~ error", error)
      Alert.alert("error", error)
    });
  }

  authorJudge = (status) => {
    const IDurl = 'https://smart.pkpm.cn:910/api/FactoryIPMange/' + this.state.ID + '/GetBaseUrl';

    fetch(IDurl).then(resid => {
      console.log(resid.ok)
      return resid.text()
    }).then((text) => {
      this.setState({ resDataID: text });
      console.log(text);
      console.log(this.state.resDataID)
      if (text.indexOf('正确') == -1) {
        Url.Imgurl = 'http://' + text
        if (text.indexOf('smart.pkpm.cn') != -1) {
          Url.url = 'https://' + text + '/api/';
        } else {
          Url.url = 'http://' + text + '/api/';
        }

        userInfoUrl = Url.url + '/User/' + this.state.username + '/GetUserInfo';
        console.log('userInfoUrl', userInfoUrl)
        fetch(userInfoUrl).then(res => {
          return res.json()
        }).then(resuserInfo => {
          let factoryList = resuserInfo
          if (factoryList) {
            factoryList.map((item, index) => {
              if (item.deptType == 40) {
                authorityUrl = Url.url + '/User/' + '/Employeeid/' + item.employeeId + '/Deptid/' + item.deptId + '/GetFacMenu';

                fetch(authorityUrl).then(resMenu => {
                  console.log('resMenu');
                  console.log(resMenu.ok);
                  return resMenu.text()
                }).then(resDataMenu => {
                  let MenuArray = [];
                  MenuArray = resDataMenu.split(",");
                  Url.Menu = MenuArray;
                  console.log("🚀 ~ file: Login.js ~ line 434 ~ Login ~ fetch ~ MenuArray.indexOf('随身工厂') != -1", MenuArray.indexOf('随身工厂') != -1)
                  console.log("🚀 ~ file: Login.js ~ line 441 ~ Login ~ fetch ~ MenuArray.indexOf('智慧看板') != -1", MenuArray.indexOf('智慧看板') != -1)

                  if (MenuArray.indexOf('智慧看板') != -1 && MenuArray.indexOf('随身工厂') != -1) {
                    this.setState({
                      isDoubleAuthor: true,
                      //selVisible: true
                    })
                    if (status == 'login') {
                      this.setState({
                        selVisible: true
                      })
                    }
                  } else if (MenuArray.indexOf('随身工厂') != -1) {
                    this.setState({
                      PDAAuthor: true
                    })
                  } else
                    if (MenuArray.indexOf('智慧看板') != -1) {
                      this.setState({
                        APPAuthor: true
                      })
                    }

                }).catch((err) => {
                  console.log("Login -> authorJudge -> authorityList -> err", err)
                })


              }
            })
          }
        })
      }

    }).catch((err) => {
      console.log("Login -> authorJudge -> err", err)
    })
  }

  authorJudgeNoFac = (status) => {
    this.setState({
      logLoading: true
    })

    //进入项目级系统
    if (this.state.username.toLowerCase().indexOf("xmcx") != -1) {
      this.setState({ isXMCXapp: true })
      //密码加密
      let formDataAuthor = new FormData();
      let dataAuthor = {};
      dataAuthor = {
        "action": "getmanyprojectapplimit",
        "servicetype": "pda",
        "express": "E2BCEB1E",
        "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
        "data": {
          "userName": this.state.username,
          "pwd": this.state.password
        }
      }
      formDataAuthor.append('jsonParam', JSON.stringify(dataAuthor))
      console.log("🚀 ~ file: Login.js ~ line 1142 ~ Login ~ formDataAuthor", formDataAuthor)
      fetch(Url.PDAurl, {
        method: 'POST',
        credentials: 'omit',
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        body: formDataAuthor
      }).then(res => {
        console.log(res.status);
        return res.json();
      }).then(resData => {
        console.log("🚀 ~ file: Login.js ~ line 1152 ~ Login ~ resData", resData)
        if (resData.status == '100') {
          if (resData.result.length == 1) {
            let result = resData.result[0]
            Url.EmployeeId = result.EmployeeId
            Url.employeeName = result.EmployeeName
            Url.Fid = result.factoryId
            Url.Fidweb = result.factoryId
            Url.FacName = result.factoryName
            Url.username = this.state.username
            Url.ProjectId = result.projectId
            Url.ProjectIdweb = result.projectId
            Url.ProjectName = result.projectName
            Url.ProjectAbb = result.projectAbb
            Url.ProjectCode = result.projectCode
            Url.chengJianPaiZhao = result.chengJianPaiZhao
            Url.isUsedDeliveryPlan = result.isUsedDeliveryPlan
            Url.jiaoZhuPaiZhao = result.jiaoZhuPaiZhao || "YES"
            Url.yinJianPaiZhao = result.yinJianPaiZhao || "YES"
            Url.isUseTaiWanID = result.isUseTaiWanID || "YES"
            Url.isResizePhoto = result.isResizePhoto || "600"
            if (result.isUsePhotograph !== undefined) {
              Url.isUsePhotograph = result.isUsePhotograph
            }
            //项目权限
            let XMCXauthorgray = [], gary = ''
            gary = result.gray
            XMCXauthorgray = gary.split(",");
            console.log("🚀 ~ file: Login.js ~ line 1177 ~ Login ~ XMCXauthorgray", XMCXauthorgray)
            Url.XMCXauthorgray = XMCXauthorgray
            if (XMCXauthorgray.indexOf("APP项目级管理系统") != -1) {
              DeviceStorage.save('isLoginXMCX', true);
              DeviceStorage.save('Menu', XMCXauthorgray);
              DeviceStorage.save('ProjectName', Url.ProjectName);
              DeviceStorage.save('ProjectAbb', Url.ProjectAbb);
              this.props.navigation.navigate('MainXMCX');
              this.setState({
                logLoading: false
              })
            } else {
              this.toast.show('没有权限')
            }

            Url.XMCXnews = result.news
            // 接收消息相关配置
            const sitecodeUrl = Url.url + 'User/' + 'GetSiteCode';
            console.log("🚀 ~ file: Login.js ~ line 1198 ~ Login ~ sitecodeUrl", sitecodeUrl)
            this.GetSiteCode(sitecodeUrl, result.EmployeeId, result.factoryCode);
            //this.props.navigation.navigate('Main1');
          } else if (resData.result.length > 1) {
            this.setState({
              logLoading: false,
              bottomFacVisible: true,
              PDAFacArr: resData.result
            })
          }
        } else {
          Alert.alert('登陆错误', resData.message)
        }
      }).catch(err => {
        Alert.alert('登陆错误', "随身工厂登陆验证错误" + err.toString())
        this.setState({ logLoading: false })
      })

      return
    }

    let formDataAuthor = new FormData();
    let dataAuthor = {};
    dataAuthor = {
      "action": "getmanyfactorypdalimit",
      "servicetype": "pda",
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        "userName": this.state.username,
        "pwd": this.state.password,
        "customerCode": this.state.ID
      }
    }
    formDataAuthor.append('jsonParam', JSON.stringify(dataAuthor))
    console.log("🚀 ~ file: Login.js:1298 ~ Login ~ formDataAuthor", formDataAuthor)

    fetch(Url.PDAurl, {
      method: 'POST',
      cache: 'no-store',
      credentials: "omit",
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formDataAuthor
    }).then(res => {
      console.log("🚀 ~ file: Login.js:1420 ~ Login ~ res:", res)
      console.log(res.status);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: Login.js ~ line 744 ~ Login ~ resData.status == '1'", resData.status == '1')
      if (resData.status == '1') {
        //密码加密
        let encodePassword = encodeURIComponent(this.state.password);

        const accountUrl = Url.url + '/User/' + this.state.username + '/CheckLogin/' + encodePassword;
        console.log("🚀 ~ file: Login.js ~ line 1140 ~ Login ~ accountUrl", accountUrl)
        this.accountLogin(accountUrl);
        return
      }
      if (resData.status != '100') {
        if (resData.message != '此账户在所有工厂都没有PDA权限') {
          Alert.alert('登陆错误', resData.message)
          this.setState({ logLoading: false })
        } else {
          this.loginJudge(status)
        }
      } else {
        this.loginJudge(status)
      }

    }).catch(err => {
      Alert.alert('登陆错误', '随身工厂登陆验证错误 ' + err.toString())
      this.setState({ logLoading: false })
      console.log("🚀 ~ file: Login.js:1336 ~ Login ~ err", err)
    })

  }

  loginJudge = (status) => {
    let userInfoUrl = Url.url + '/User/' + this.state.username + '/GetUserInfo';
    if (this.state.ID == 'pkpmfb') {
      userInfoUrl = Url.url + '/User/pkpm/GetUserInfo';
    }
    console.log("🚀 ~ file: Login.js ~ line 1415 ~ Login ~ userInfoUrl", userInfoUrl)
    fetch(userInfoUrl, {
      credentials: 'include',
      cache: 'no-cache'
    }).then(res2 => {
      return res2.json()
    }).then(resuserInfo => {
      if (typeof resuserInfo == "undefined") {
        Alert.alert('登陆错误')
        this.setState({ logLoading: false })
        return
      }
      Url.EmployeeId = resuserInfo[0].employeeId
      Url.employeeName = resuserInfo[0].employeeName

      //let menuurl = Url.url + '/User/' + resuserInfo[0].employeeName + '/GetMenu';
      let menuurl = Url.url + 'User/' + resuserInfo[0].employeeId + '/GetMenuID';
      console.log("🚀 ~ file: Login.js ~ line 1423 ~ Login ~ fetch ~ menuurl", menuurl)

      this.menuJudge(menuurl)

    }).catch(err => {
      Alert.alert('登陆错误', '智慧看板获取用户ID ' + err.toString())
      this.setState({ logLoading: false })
      console.log("🚀 ~ file: Login.js ~ line 1538 ~ Login ~ fetch ~ err", err)
    })
  }

  menuJudge = (menuUrl) => {
    fetch(menuUrl, {
      credentials: 'include',
      cache: 'no-cache'
    }).then(resMenu => {
      console.log("🚀 ~ file: Login.js ~ line 1488 ~ Login ~ fetch ~ resMenu.status", resMenu.status)
      console.log("🚀 ~ file: Login.js ~ line 1488 ~ Login ~ fetch ~ resMenu.ok", resMenu.ok)
      if (resMenu.status == 200) {
        return resMenu.text()
      } else {
        let menuurl = Url.url + 'User/' + Url.employeeName + '/GetMenu';
        this.menuJudge(menuurl)
        return
      }
    }).then(resDataMenu => {
      if (typeof resDataMenu == 'undefined') {
        Alert.alert('登陆错误')
        this.setState({ logLoading: false })
        return
      }
      console.log("🚀 ~ file: Login.js ~ line 1490 ~ Login ~ fetch ~ resDataMenu", resDataMenu)
      let MenuArray = [];
      MenuArray = resDataMenu.split(",");
      Url.Menu = MenuArray;

      if (MenuArray.indexOf('PDA功能') != -1 && MenuArray.indexOf('APP功能') != -1) {
        this.setState({
          logLoading: false,
          isDoubleAuthor: true,
          selVisible: true
        })

        DeviceStorage.save('MenuPDA', true);
        DeviceStorage.save('MenuAPP', true);
        // 验证智慧看板跳转随身工厂
        DeviceStorage.get('changeModelPDA').then(res => {
          if (res) {
            if (Url.PDAurl.length > 0) {
              this.PDALoginNEW()
            } else {
              this.PDAIDget()
            }
          }
        })
      } else if (MenuArray.indexOf('PDA功能') != -1) {
        DeviceStorage.save('MenuPDA', true);
        DeviceStorage.save('MenuAPP', false);
        this.setState({
          PDAAuthor: true
        })
        this.PDALoginNEW();
      } else if (MenuArray.indexOf('APP功能') != -1) {
        DeviceStorage.save('MenuPDA', false);
        DeviceStorage.save('MenuAPP', true);
        this.setState({
          APPAuthor: true
        })
        let encodePassword = encodeURIComponent(this.state.password);

        const accountUrl = Url.url + '/User/' + this.state.username + '/CheckLogin/' + encodePassword;
        console.log("🚀 ~ file: Login.js ~ line 1303 ~ Login ~ fetch ~ accountUrl", accountUrl)

        this.accountLogin(accountUrl);
      } else {
        Alert.alert('登陆错误', '无APP权限')
        this.setState({ logLoading: false })
        this.setState({ noAuthor: true })
      }
    }).catch(err => {

      Alert.alert('登陆错误', '智慧看板获取权限信息' + err.toString())
      this.setState({ logLoading: false })
      console.log("🚀 ~ file: Login.js ~ line 1535 ~ Login ~ fetch ~ err", err)
    })
  }

  //登陆开始，验证是否输入ID。无ID进入游客模式, PDA
  login1 = async () => {
    console.log("🚀 ~ file: Login.js ~ line 1390 ~ Login ~ login1= ~ Url.url.length", Url.url.length)
    console.log("🚀 ~ file: Login.js ~ line 1390 ~ Login ~ login1= ~ Url.url.length", Url.url)
    console.log("🚀 ~ file: Login.js ~ line 1390 ~ Login ~ login1= ~ Url.PDAurl.length == 0", Url.PDAurl.length == 0)
    console.log("🚀 ~ file: Login.js ~ line 1390 ~ Login ~ login1= ~ Url.PDAurl.length == 0", Url.PDAurl)

    if (this.state.ID.length == 0) {
      this.toast.show('请输入客户编码')
      this.setState({ isErrorID: true })
      return
    }

    if (this.state.ID.length != 0) {
      if (Url.url.length == 0 && Url.PDAurl.length == 0) {
        console.log("🚀 ~ file: Login.js ~ line 1396 ~ Login ~ login1= ~ this.ID.focus()")
        this.toast.show('请输入客户编码，并点击搜索键', 3000)
        this.setState({ isErrorID: true })
        return
      }
    }

    if (this.state.ID == '' && Url.url.length == 0 && Url.PDAurl.length == 0) {
      Url.url = 'https://smart.pkpm.cn:910/api/';
      Url.username = 'pkpm';
      Url.Fid = '74e1cbc6-2cfc-4d6f-bcd1-739d28e4c74d/';
      Url.Factoryname = '北京构力科技';
      Url.Daily = true;
      Url.Project = true;
      Url.Stocks = true;
      Url.Business = true;
      Url.Model = true;
      Url.ChangeFac = true;
      this.setState({
        logLoading: false,
      })
      navigationUtil.reset(this.props.navigation, 'Main')
    } else {
      //密码加密
      let encodePassword = encodeURIComponent(this.state.password);
      const accountUrl = Url.url + '/User/' + this.state.username + '/CheckLogin/' + encodePassword;
      console.log("🚀 ~ file: Login.js:1625 ~ Login ~ login1= ~ accountUrl", accountUrl)
      fetch(accountUrl, {
        cache: 'no-cache'
      }).then(res => {
        if (res.ok) {
          return res.text();
        }
      }).then(resData => {
        if (resData == 'true') {
          this.authorJudgeNoFac('login');
        }
        else {
          Alert.alert('登录失败', resData || '账号密码无效');
          this.setState({ logLoading: false })
        }
        console.log("🚀 ~ file: Login.js:1624 ~ Login ~ fetch ~ resData", resData)
      }).catch(err => {
        console.log("🚀 ~ file: Login.js:1561 ~ Login ~ fetch ~ typeof err:", typeof err)
        Alert.alert('登陆错误', '登陆开始' + err.toString())
        this.setState({ logLoading: false })
        console.log("🚀 ~ file: Login.js:1634 ~ Login ~ fetch ~ err", err)
      })
      console.log("🚀 ~ file: Login.js ~ line 529 ~ Login ~ login= ~ this.state.isDoubleAuthor", this.state.isDoubleAuthor)

    }
    Url.password = this.state.password
    await DeviceStorage.save('username', this.state.username);
    await DeviceStorage.save('password', this.state.password);
  }

  //验证ID获取现场接口服务
  IDLogin = (IDurl) => {
    fetch(IDurl, {
      credentials: 'include',
    }).then(resid => {
      console.log(resid.ok)
      return resid.text()
    }).then(text => {
      this.setState({ resDataID: text });
      console.log(text);
      console.log(this.state.resDataID)
      if (text.indexOf('正确') == -1) {

        Url.Imgurl = 'http://' + text
        if (text.indexOf('smart.pkpm.cn') != -1) {
          Url.url = 'https://' + text + '/api/';
        } else {
          Url.url = 'http://' + text + '/api/';
        }

        DeviceStorage.save('url', Url.url);


        //密码加密
        let encodePassword = encodeURIComponent(this.state.password);

        const accountUrl = Url.url + '/User/' + this.state.username + '/CheckLogin/' + encodePassword;
        console.log("🚀 ~ file: Login.js ~ line 1445 ~ Login ~ fetch ~ accountUrl", accountUrl)
        console.log(accountUrl)

        this.accountLogin(accountUrl);

      } else {
        Alert.alert('登录失败', text)
        this.setState({ logLoading: false });
      }
    }).catch((error) => {
      console.log(resid.status);
      Alert.alert('登录失败', error);
      this.setState({ logLoading: false })
    });
  }

  //验证账号密码获取用户信息
  accountLogin = (accountUrl) => {
    console.log("🚀 ~ file: Login.js ~ line 1463 ~ Login ~ accountUrl", accountUrl)
    fetch(accountUrl, {
      credentials: 'include',
    }).then(res => {
      if (res.ok) {
        return res.text();
      }
    }).then(resData => {
      if (this.state.ID == 'pkpmfb') resData = 'true'
      console.log(resData)
      if (resData === 'true') {

        this.setState({
          resData: resData,
          isTrue: resData,
        });

        Url.username = this.state.username;

        let userInfoUrl = Url.url + '/User/' + Url.username + '/GetUserInfo';
        if (this.state.ID == 'pkpmfb') userInfoUrl = Url.url + '/User/pkpm/GetUserInfo';
        console.log("🚀 ~ file: Login.js ~ line 1481 ~ Login ~ fetch ~ userInfoUrl", userInfoUrl)

        this.userInfoLogin(userInfoUrl);

      } else {
        Alert.alert('登录失败', resData || '账号密码无效');
        this.setState({ logLoading: false })
      }
    }).catch((error) => {
      Alert.alert('登陆错误', 'GetUserInfo' + err.toString())
      this.setState({ logLoading: false })
      console.log(error);
    });
  }

  //验证用户信息获取工厂权限
  userInfoLogin = (userInfoUrl) => {
    console.log("🚀 ~ file: Login.js ~ line 1504 ~ Login ~ userInfoUrl", userInfoUrl)
    console.log("Login -> userInfoUrl", userInfoUrl)

    let lastUserInfo = [{
      "id": '',
      "userName": "",
      "employeeName": "",
      "employeeId": "",
      "roleId": "",
      "roleLevel": 0,
      "deptId": "",
      "deptName": "",
      "deptCode": "",
      "deptType": 0
    },]
    fetch(userInfoUrl, {
      credentials: 'include',
    }).then(res2 => {
      return res2.json()
    }).then(resuserInfo => {
      if (typeof resuserInfo == 'undefined') {
        Alert.alert('登录失败');
        this.setState({ logLoading: false })
      }
      let obj = {};
      let userInfo = resuserInfo.reduce((item, next) => {
        obj[next.deptId] ? '' : obj[next.deptId] = true && item.push(next);
        return item;
      }, []);
      console.log("🚀 ~ file: Login.js ~ line 1625 ~ Login ~ userInfo ~ userInfo", userInfo)
      console.log("🚀 ~ file: Login.js ~ line 1521 ~ Login ~ fetch ~ resuserInfo", resuserInfo)
      //如果有上次登录的工厂名字，判断
      let facCount = 0

      userInfo.map((item) => {
        if (item.deptType == 40) {
          facCount++
        }
      })
      console.log("🚀 ~ file: Login.js ~ line 1634 ~ Login ~ userInfo.map ~ facCount", facCount)

      if (facCount == 1) {
        userInfo.map((item) => {
          if (item.deptType == 40) {
            DeviceStorage.save('user', this.state.ID == 'pkpmfb' ? this.state.username : item.userName);
            DeviceStorage.save('FacName', item.deptName);
            DeviceStorage.save('employeeName', this.state.ID == 'pkpmfb' ? this.state.username : item.employeeName);
            Url.employeeName = item.employeeName

            const sitecodeUrl = Url.url + 'User/' + 'GetSiteCode';
            this.GetSiteCode(sitecodeUrl, item.employeeId, item.deptCode);

            authorityUrl = Url.url + '/User/' + '/Employeeid/' + item.employeeId + '/Deptid/' + item.deptId + '/GetFacMenu';
            console.log("🚀 ~ file: Login.js ~ line 1528 ~ Login ~ fetch ~ authorityUrl", authorityUrl)
            DeviceStorage.save('isLogin', true);
            this.authorityLogin(authorityUrl);

            Url.Fid = item.deptId + '/';
            Url.Fidweb = item.deptId;
            Url.Factoryname = item.deptName;
            Url.EmployeeId = item.employeeId;
            this.GetCompInfo()
            DeviceStorage.save('Fid', item.deptId + '/');
            DeviceStorage.save('Fidweb', item.deptId);
            DeviceStorage.save('Factoryname', item.deptName);
            DeviceStorage.save('EmployeeId', item.employeeId);

            this.setState({
              isLoading: false,
              isLogin: true
            })
          }
        })
        return
      }

      this.setState({
        userInfo: userInfo,
        logLoading: false,
        bottomGlorySFFacVisible: true,
      });

    }).catch((error) => {
      console.log(error)
      Alert.alert('登录失败');
      this.setState({ logLoading: false })
    });
  }

  //验证用户信息获取项目权限
  userInfoLoginXMCX = (userInfoUrl) => {
    console.log("🚀 ~ file: Login.js ~ line 1504 ~ Login ~ userInfoUrl", userInfoUrl)

    fetch(userInfoUrl, {
      credentials: 'include',
    }).then(res2 => {
      return res2.json()
    }).then(resuserInfo => {
      console.log("🚀 ~ file: Login.js ~ line 1521 ~ Login ~ fetch ~ resuserInfo", resuserInfo)
      resuserInfo = resuserInfo
      this.setState({
        userInfo: resuserInfo,
      });

      if (resuserInfo) {

        for (let i = 0; i < resuserInfo.length; i++) {
          const item = {}
          /* if (this.state.FacName != '' && ((this.state.ID == this.state.IDLast) && this.state.IDLast != '' && lastUserInfo[i].deptName == this.state.FacName)) {
            item = lastUserInfo[i];
          } else {
            item = resuserInfo[i];
          } */

          item = resuserInfo[i];

          if (item.deptType == 40) {
            Url.Fid = item.deptId + '/';
            console.log("🚀 ~ file: Login.js ~ line 1638 ~ Login ~ fetch ~ Url.Fid", Url.Fid)
            Url.Fidweb = item.deptId;
            Url.Factoryname = item.deptName;
            Url.EmployeeId = item.employeeId;
            DeviceStorage.save('FacName', item.deptName);
            DeviceStorage.save('employeeName', item.employeeName);
            DeviceStorage.save('Fid', item.deptId + '/');
            DeviceStorage.save('Fidweb', item.deptId);
            DeviceStorage.save('Factoryname', item.deptName);
            DeviceStorage.save('EmployeeId', item.employeeId);
          }

          if (item.deptType != 40 && item.deptType != 0) {
            console.log("🚀 ~ file: Login.js ~ line 1652 ~ Login ~ fetch ~ item", item)
            Url.ProjectId = item.deptId
            Url.ProjectName = item.deptName
            DeviceStorage.save('user', item.userName);
            DeviceStorage.save('ProjectId', item.deptId + '/');
            DeviceStorage.save('ProjectIdweb', item.deptId);
            DeviceStorage.save('ProjectName', item.deptName);
            DeviceStorage.save('EmployeeId', item.employeeId);

            const sitecodeUrl = Url.url + 'User/' + 'GetSiteCode';

            Url.employeeName = item.employeeName

            this.GetSiteCode(sitecodeUrl, item.employeeId, item.deptCode);

            authorityUrl = Url.url + '/User/' + '/Employeeid/' + item.employeeId + '/Deptid/' + Url.Fid + '/GetFacMenu';
            console.log("🚀 ~ file: Login.js ~ line 1667 ~ Login ~ fetch ~ authorityUrl", authorityUrl)

            this.authorityLoginXMCX(authorityUrl);
            this.setState({
              isLoading: false,
            })

            break;
          }
        }

      } else {
        Alert.alert('无项目查看权限')
        this.setState({ logLoading: false })
      }
    }).catch((error) => {
      Alert.alert('登录失败', error);
      console.log(error)
      this.setState({ logLoading: false })
    });
  }

  //获取真实的客户编码
  GetSiteCode = (sitecodeUrl, employeeId, deptCode) => {
    console.log("Login -> GetSiteCode -> sitecodeUrl", sitecodeUrl)
    console.log('sitecodeUrl, employeeId, deptCode', sitecodeUrl, employeeId, deptCode)
    fetch(sitecodeUrl, {
      credentials: 'include',
    }).then(res => {
      return res.json()
    }).then(ressitecode => {
      console.log("Login -> GetSiteCode -> ressitecode", ressitecode)
      console.log('sitecode:' + ressitecode[0].value)
      console.log(ressitecode[0].value.toLowerCase() == this.state.ID.toLowerCase())
      let isMatch = false
      isMatch = ressitecode[0].value.toLowerCase() == Url.ID.replace(/\s+/g, "").toLowerCase() || ressitecode[0].value.toLowerCase() == this.state.ID.replace(/\s+/g, "").toLowerCase()
      if (isMatch) {
        DeviceStorage.save('ID', this.state.ID == 'pkpmfb' ? 'pkpmfb' : ressitecode[0].value);
        const ID = ressitecode[0].value;
        console.log('updateTags')
        Url.ID = this.state.ID == 'pkpmfb' ? 'pkpmfb' : ID
        Url.deptCode = deptCode
        PushUtil.addTag(ID, (code, remain) => {
          if (code == 200) {
            this.setState({ result: remain });
          }
        })
        PushUtil.addTag(deptCode, (code, remain) => {
          if (code == 200) {
            this.setState({ result: remain });
          }
        })
        PushUtil.addTag(ID + "_" + deptCode, (code, remain) => {
          if (code == 200) {
            this.setState({ result: remain });
          }
        })
        PushUtil.listTag((code, tagList) => {
          if (code == 200) {
            let tags = "tags:";
            console.log("PushUtil xxxxxx length=" + tagList.length)
            for (var i = 0; i < tagList.length; i++) {
              tags = tags + tagList[i] + "\n";
              console.log("PushUtil xxxxxx tags=" + tags)
            }
          }
        })
        employeeId = employeeId.replace(/-/g, "")
        let employeeIdAlias = ID + '_' + deptCode + '_' + employeeId
        employeeIdAlias = employeeIdAlias.toLowerCase()
        console.log("🚀 ~ file: Login.js ~ line 1784 ~ Login ~ fetch ~ employeeIdAlias", employeeIdAlias)
        Url.employeeIdAlias = employeeIdAlias
        console.log("🚀 ~ file: Login.js ~ line 1785 ~ Login ~ fetch ~ Url.employeeIdAlias", Url.employeeIdAlias)
        PushUtil.addAlias(employeeIdAlias, "employeeId", (code) => {
          if (code == 200) {
          }
        })

        this.setState({
          ID: ressitecode[0].value
        })
      } else {
        if (this.state.ID != 'pkpmfb') {
          Alert.alert('系统编码错误', '系统编码和用户输入ID不一致，请检查是否有误。')
        }
      }
    }).catch(err => {
      Alert.alert('系统编码错误', err)
    })
  }

  //获取系统地址
  GetSysInfo = (Sysurl) => {
    console.log("Login -> GetSysInfo -> Sysurl", Sysurl)
    fetch(Sysurl, {
      credentials: 'include',
    }).then(resid => {
      return resid.json()
    }).then(resdataID => {
      console.log("Login -> GetSysInfo -> resdataID", resdataID)
      let arr = []
      arr = resdataID[0].systemAddress.split("/")
      console.log("Login -> GetSysInfo -> arr", arr)
      Url.urlsys = 'http://' + arr[2]
    }).catch((error) => {
      console.log(resid.status);
      Alert.alert('登录失败', '');
      this.setState({ logLoading: false })
    });
  }

  //验证工厂权限获取业务权限信息
  authorityLogin = (authorityUrl) => {
    fetch(authorityUrl, {
      credentials: 'include',
    }).then(resMenu => {
      console.log('resMenu');
      console.log(resMenu.ok);
      return resMenu.text()
    }).then(resDataMenu => {
      console.log("🚀 ~ file: Login.js ~ line 1628 ~ Login ~ fetch ~ resDataMenu", resDataMenu)
      let MenuArray = [];
      MenuArray = resDataMenu.split(",");
      //console.log(MenuArray);
      DeviceStorage.save('Menu', MenuArray);
      Url.Menu = MenuArray;

      if (Url.Menu.indexOf('APP日常') != -1) {
        DeviceStorage.save('daily', true);
        Url.Daily = true
      } else {
        DeviceStorage.save('daily', false);
        Url.Daily = false
      }
      if (Url.Menu.indexOf('APP项目') != -1) {
        DeviceStorage.save('project', true);
        Url.Project = true
      } else {
        DeviceStorage.save('project', false);
        Url.Project = false
      }
      if (Url.Menu.indexOf('APP堆场') != -1) {
        DeviceStorage.save('stocks', true)
        Url.Stocks = true
      } else {
        DeviceStorage.save('stocks', false);
        Url.Stocks = false
      }
      if (Url.Menu.indexOf('APP经营') != -1) {
        DeviceStorage.save('business', true);
        Url.Business = true
      } else {
        DeviceStorage.save('business', false);
        Url.Business = false
      }
      if (Url.Menu.indexOf('APP模型') != -1) {
        DeviceStorage.save('model', true);
        Url.Model = true
      } else {
        DeviceStorage.save('model', false);
        Url.Model = false
      }
      if (Url.Menu.indexOf('APP切换工厂') != -1 || Url.Menu.indexOf('APP切换业务范围') != -1) {
        DeviceStorage.save('changeFac', true);
        Url.ChangeFac = true
      } else {
        DeviceStorage.save('changeFac', false);
        Url.ChangeFac = false
      }
      if (this.state.isLoading == false) {
        this.setState({ logLoading: false })
        //this.doLogin();
        // DeviceStorage.save('isLoading', this.state.isLoading);
        //this.NIMLogin();
        this.GetUnreadMassage();

        this.requestLibraryDetail()
        let isProcess = Url.Menu.indexOf('APP流程') != -1
        if (isProcess && this.state.ID != 'pkpmfb') {
          this.props.navigation.navigate('MainProcess');
        } else {
          this.props.navigation.navigate('Main');
        }

        // this.props.navigation.navigate('Loading');
      }
    }).catch((error) => {
      Alert.alert('登录失败', error.toString())
      console.log(error)
    });
  }

  authorityLoginXMCX = (authorityUrl) => {
    fetch(authorityUrl, {
      credentials: 'include',
    }).then(resMenu => {
      console.log('resMenu');
      console.log(resMenu.ok);
      return resMenu.text()
    }).then(resDataMenu => {
      console.log("🚀 ~ file: Login.js ~ line 1734 ~ Login ~ fetch ~ resDataMenu", resDataMenu)
      let MenuArray = [];
      MenuArray = resDataMenu.split(",");
      if (MenuArray.indexOf("APP项目级管理系统") != -1) {
        DeviceStorage.save('isLoginXMCX', true);
        DeviceStorage.save('Menu', MenuArray);
        this.props.navigation.navigate('MainXMCX');
      } else {
        Alert.alert("登录失败", "无APP登陆权限")
        this.setState({
          logLoading: false
        })
      }
      //console.log(MenuArray);

    })
  }

  GetSysLogo = () => {
    if (Url.ID.toLowerCase() == this.state.ID.toLowerCase()) {
      const IDurl = 'https://smart.pkpm.cn:910/api/FactoryIPMange/' + this.state.ID + '/GetBaseUrl';
      fetch(IDurl).then(resid => {
        console.log(resid.ok)
        return resid.text()
      }).then(text => {
        console.log("Login -> GetSysLogo -> text", text)
        this.setState({ resDataID: text });

        if (text.indexOf('正确') == -1) {
          Url.Imgurl = 'http://' + text
          if (text.indexOf('smart.pkpm.cn') != -1) {
            Url.url = 'https://' + text + '/api/';
          } else {
            Url.url = 'http://' + text + '/api/';
          }

          const sitenameurl = Url.url + 'User/GetSysSiteName'

          fetch(sitenameurl).then(res => {
            if (res.ok) {
              return res.json()
            } else {
              return
            }
          }).then(resSiteName => {
            if (resSiteName == undefined) {
              this.setState({
                sitename: '智慧工厂管理系统'
              })
              DeviceStorage.save('sitename', '智慧工厂管理系统')
              return
            }
            this.setState({
              sitename: resSiteName[0].value
            })
            DeviceStorage.save('sitename', resSiteName[0].value)
          }).catch(err => {
            Alert.alert("Login -> GetSysLogo -> sitename -> err", err)
          })

          const logourl = Url.url + 'User/GetSysAppLogo'
          //获取系统设置
          fetch(logourl).then(resid => {
            console.log("Login -> GetSysLogo -> resid", resid.ok)
            if (resid.ok) {
              return resid.json()
            } else {
              this.setState({
                logourl: ''
              })
              return
            }
          }).then(resdataRecID => {
            if (resdataRecID == undefined) {
              DeviceStorage.save('logourl', '')
              return
            }

            //获取系统地址
            const Sysurl = 'https://smart.pkpm.cn:910/api/FactoryIPMange/' + this.state.ID + '/GetSysUrl';
            fetch(Sysurl).then(resid => {
              return resid.json()
            }).then(resdataID => {
              console.log("Login -> GetSysLogo -> resdataID", resdataID)
              let arr = []
              arr = resdataID[0].systemAddress.split("/")

              Url.urlsys = 'http://' + arr[2]

              DeviceStorage.save('urlsys', Url.urlsys)

              const urllogo = Url.urlsys + '/' + resdataRecID[0].attachment
              console.log("Login -> GetSysLogo -> urllogo", urllogo)

              this.setState({
                logourl: urllogo
              })

              DeviceStorage.save('logourl', urllogo)

            }).catch((error) => {
              console.log("Login -> GetSysLogo -> sys -> error", error)
            });

          }).catch(err => {
            Alert.alert("Login -> GetSysLogo -> fetch1 -> err", err)
          })

        }
      }).catch((error) => {
        Alert.alert("Login -> GetSysLogo -> error", error)
      });
    } else {

      return
    }

  }

  GetSysLogo1 = () => {
    const sitenameurl = Url.url + 'User/GetSysSiteName'

    fetch(sitenameurl, {
      credentials: 'include',
    }).then(res => {
      if (res.ok) {
        return res.json()
      } else {
        return
      }
    }).then(resSiteName => {
      if (typeof resSiteName == "undefined") {
        this.setState({
          sitename: '智慧工厂管理系统'
        })
        this.setState({
          logLoading: false
        })
        Alert.alert('错误', "系统名获取失败")
        DeviceStorage.save('sitename', '智慧工厂管理系统')
        return
      }
      this.setState({
        sitename: resSiteName[0].value
      })
      this.setState({
        logLoading: false
      })
      DeviceStorage.save('sitename', resSiteName[0].value)
    }).catch(err => {
      Alert.alert('错误', 'sitename' + err.toString())
      this.setState({
        logLoading: false
      })
      Alert.alert("Login -> GetSysLogo -> sitename -> err", err)
    })

    const logourl = Url.url + 'User/GetSysAppLogo'
    console.log("🚀 ~ file: Login.js ~ line 2042 ~ Login ~ logourl", logourl)
    //获取系统设置
    fetch(logourl, {
      credentials: 'include',
    }).then(resid => {
      console.log("Login -> GetSysLogo -> resid", resid.ok)
      if (resid.ok) {
        return resid.json()
      } else {
        this.setState({
          logourl: ''
        })
        return
      }
    }).then(resdataRecID => {
      console.log("🚀 ~ file: Login.js ~ line 2055 ~ Login ~ fetch ~ resdataRecID", typeof resdataRecID)
      console.log("🚀 ~ file: Login.js ~ line 2055 ~ Login ~ fetch ~ resdataRecID", resdataRecID)
      if (typeof resdataRecID == "undefined") {
        this.setState({
          logLoading: false
        })
        Alert.alert('错误', "系统logo获取失败")
        DeviceStorage.save('logourl', '')
        return
      }

      //获取系统地址
      const Sysurl = 'https://smart.pkpm.cn:910/api/FactoryIPMange/' + this.state.ID + '/GetSysUrl';
      fetch(Sysurl).then(resid => {
        return resid.json()
      }).then(resdataID => {
        if (typeof resdataRecID == "undefined") {
          this.setState({
            logLoading: false
          })
          Alert.alert('错误', "系统地址获取失败")
          return
        }
        console.log("Login -> GetSysLogo -> resdataID", resdataID)

        let arr = []
        arr = resdataID[0].systemAddress.split("/")

        if (resdataID[0].systemAddress.indexOf('https') != -1) {
          Url.urlsys = 'https://' + arr[2]
        } else {
          Url.urlsys = 'http://' + arr[2]
        }

        
        DeviceStorage.save('urlsys', Url.urlsys)

        const urllogo = Url.urlsys + '/' + resdataRecID[0].attachment
        console.log("Login -> GetSysLogo -> urllogo", urllogo)

        this.setState({
          logourl: urllogo
        })

        this.setState({
          logLoading: false
        })

        DeviceStorage.save('logourl', urllogo)

      }).catch((error) => {
        Alert.alert('错误', 'sysurl' + error.toString())
        this.setState({
          logLoading: false
        })
      });

    }).catch(err => {
      Alert.alert('错误', 'Logo' + err.toString())
      this.setState({
        logLoading: false
      })
    })


  }

  GetUnreadMassage = () => {
    const url = Url.url + Url.factory + 'facId/' + Url.Fid + 'recId/' + Url.EmployeeId + '/GetUnSeenMsgListApp'
    console.log("Login -> url", url)
    fetch(url, {
      credentials: 'include',
    }).then(res => {
      console.log(res.status);
      return res.json()
    }).then(resData => {
      console.log("App -> resData.length", resData.length)
      DeviceEventEmitter.emit('noticecount', resData.length);
    }).catch((err) => {
      console.log("Login -> err", err)
    })
  }

  GetCompInfo = () => {
    const url = Url.url + 'Factories/' + Url.Fid + 'GetSearchCondition'
    console.log("🚀 ~ file: Login.js ~ line 1888 ~ Login ~ url", url)
    fetch(url, {
      credentials: 'include',
    }).then(res => {
      console.log(res.status);
      return res.json()
    }).then(resData => {
      console.log("🚀 ~ file: Login.js ~ line 1894 ~ Login ~ fetch ~ resData", resData)
      console.log("🚀 ~ file: Login.js ~ line 1896 ~ Login ~ fetch ~ resData.length", resData.length)
      DeviceStorage.save('workSpaceSearchList', resData);
      if (resData.toString().length > 0) {
        Url.employeeList = resData.lstEmployee
        Url.compTypeNameList = resData.lstCompType
        Url.compRoomNameList = resData.lstLibrary
        Url.bigStateList = resData.lstCompState
        Url.productLineNameList = resData.lstProductionLine
      }
      console.log("🚀 ~ file: Login.js ~ line 1894 ~ Login ~ fetch ~ Url", Url)

    }).catch((err) => {
      console.log("🚀 ~ file: Login.js ~ line 1901 ~ Login ~ fetch ~ err", err)
    })
  }

  //PDA登陆站点获取
  PDAIDgetAuto = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "GetSystemAddress",
      "servicetype": "pda",
      "express": "EAB711EA",
      "ciphertext": "22d37237b0d87bc362e198e9aec3f728",
      "data": {
        "customerCode": this.state.ID,
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.status);
      return res.json();
    }).then(resData => {
      Url.PDAurl = resData.result.systemAddress
      console.log("🚀 ~ file: Login.js ~ line 1080 ~ Login ~ Url.PDAurl", Url.PDAurl)
      this.setState({
        PDAaddress: Url.PDAurl,
      })

    }).catch((error) => {
      Alert.alert("Login -> GetSysLogo -> error", error)
    });
  }

  //PDA登陆
  PDAIDget = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "GetSystemAddress",
      "servicetype": "pda",
      "express": "EAB711EA",
      "ciphertext": "22d37237b0d87bc362e198e9aec3f728",
      "data": {
        "customerCode": this.state.ID,
      }
    }
    formData.append("action", "GetSystemAddress")
    formData.append("customerCode", this.state.ID)
    formData.append("express", "EAB711EA")
    formData.append("ciphertext", "22d37237b0d87bc362e198e9aec3f728")
    fetch('http://smart.pkpm.cn:81/pcservice/pdaservice.ashx', {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.status);
      return res.json();
    }).then(resData => {
      Url.PDAurl = resData.result.systemAddress
      DeviceStorage.save('PDAurl', resData.result.systemAddress);
      console.log("🚀 ~ file: Login.js ~ line 1080 ~ Login ~ Url.PDAurl", Url.PDAurl)
      this.setState({
        PDAaddress: Url.PDAurl,
      })

      this.PDALoginNEW()

    }).catch((error) => {
      Alert.alert("Login -> GetSysLogo -> error", error)
    });
  }

  //PDA获取多工厂权限(自动，用于权限判断)
  PDASelFacAuto = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getfactorybyusername",
      "servicetype": "pda",
      "express": "EAB711EA",
      "ciphertext": "22d37237b0d87bc362e198e9aec3f728",
      "data": {
        "userName": this.state.username,
        "customerCode": this.state.ID,
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      credentials: 'omit',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.status);
      return res.json();
    }).then(resData => {

      //缺少判断语句， 当PDA无权限没开发
      let PDAselFac = resData.result

      if (PDAselFac.length == 1) {

        return
      } else {
        //默认PDA有权限
        this.setState({
          PDAselFac: PDAselFac,
          PDAAuthor: true
        })
      }

    }).catch((error) => {
      Alert.alert("Login -> GetSysLogo -> error", error)
    });
  }

  //PDA获取多工厂权限
  PDASelFac = () => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "getfactorybyusername",
      "servicetype": "pda",
      "express": "EAB711EA",
      "ciphertext": "22d37237b0d87bc362e198e9aec3f728",
      "data": {
        "userName": this.state.username,
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      credentials: 'omit',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.status);
      return res.json();
    }).then(resData => {

      //缺少判断语句， 当PDA无权限没开发
      let PDAselFac = resData.result

      if (PDAselFac.length == 1) {
        this.PDALogin(PDAselFac[0].FactoryId)
        return
      }

      //默认PDA有权限
      this.setState({
        PDAselFac: PDAselFac,
        logLoading: false,
        bottomFacVisible: true,
      })

    }).catch((error) => {
      Alert.alert("Login -> GetSysLogo -> error", error)
    });
  }

  //PDA登陆验证，用于权限管理
  PDALoginAuto = (FactoryId) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "loginChk",
      "servicetype": "pda",
      "express": "A45162C0",
      "ciphertext": "770fdb27cc9d018dc2d6c5d64ce5ff23",
      "data": {
        "userName": this.state.username,
        "pwd": this.state.password,
        "factoryId": FactoryId
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      credentials: 'omit',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.status);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        Url.PDAEmployeeId = resData.result.EmployeeId
        Url.PDAEmployeeName = resData.result.EmployeeName
        Url.PDAusername = this.state.username
        let PDAauthorgray = [], gary = ''
        gary = resData.result.gray
        PDAauthorgray = gary.split(",");
        console.log("🚀 ~ file: Login.js ~ line 1093 ~ Login ~ resData.result.gray", resData.result.gray)
        Url.PDAauthorgray = PDAauthorgray
        console.log("🚀 ~ file: Login.js ~ line 1094 ~ Login ~ Url.PDAauthorgray", Url.PDAauthorgray)
        Url.PDAnews = resData.result.news
        console.log("🚀 ~ file: Login.js ~ line 1170 ~ Login ~ Url.PDAnews", Url.PDAnews)
        console.log("🚀 ~ file: Login.js ~ line 1173 ~ Login ~ Url.PDAEmployeeName", Url.PDAEmployeeName)
        this.setState({
          bottomFacVisible: false,
        })
        this.props.navigation.navigate('Main1');
      } else {
        this.setState({
          bottomFacVisible: false
        })
        Alert.alert('登录失败', resData.message)
      }
    }).catch((error) => {
      Alert.alert("Login -> GetSysLogo -> error", error)
    });

  }

  // 随身工厂登录后接收消息配置
  userInfoLogin2 = (userInfoUrl) => {
    console.log("Login -> userInfoUrl", userInfoUrl)
    let lastUserInfo = [{
      "id": '',
      "userName": "",
      "employeeName": "",
      "employeeId": "",
      "roleId": "",
      "roleLevel": 0,
      "deptId": "",
      "deptName": "",
      "deptCode": "",
      "deptType": 0
    },]
    fetch(userInfoUrl).then(res2 => {
      return res2.json()
    }).then(resuserInfo => {
      this.setState({
        userInfo: resuserInfo,
      });

      //如果有上次登录的工厂名字，判断
      if (this.state.FacName != '') {
        resuserInfo.map((item) => {
          if (item.deptName == this.state.FacName) {
            lastUserInfo.push(item)
          }
        })
      }

      if (userInfo) {
        for (let i = 0; i < resuserInfo.length; i++) {
          const item = {}
          if (this.state.FacName != '' && ((this.state.ID == this.state.IDLast) && this.state.IDLast != '' && lastUserInfo[i].deptName == this.state.FacName)) {
            item = lastUserInfo[i];
          } else {
            item = resuserInfo[i];
          }

          if (item.deptType == 40) {
            const sitecodeUrl = Url.url + 'User/' + 'GetSiteCode';
            this.GetSiteCode(sitecodeUrl, item.employeeId, item.deptCode);
            break;
          }
        }
      } else {
        Alert.alert('无工厂查看权限')
        this.setState({ logLoading: false })
      }
    }).catch((error) => {
      Alert.alert('登录失败', error);
      console.log(error)
      this.setState({ logLoading: false })
    });
  }

  //PDA登陆新接口 🆕
  PDALoginNEW = () => {
    // 接收消息相关配置
    //userInfoUrl = Url.url + '/User/' + Url.username + '/GetUserInfo';
    //this.userInfoLogin2(userInfoUrl);

    let formDataAuthor = new FormData();
    let dataAuthor = {};
    dataAuthor = {
      "action": "getmanyfactorypdalimit",
      "servicetype": "pda",
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        "userName": this.state.username,
        "pwd": this.state.password,
        "customerCode": this.state.ID
      }
    }
    formDataAuthor.append('jsonParam', JSON.stringify(dataAuthor))
    console.log("🚀 ~ file: Login.js ~ line 2424 ~ Login ~ formDataAuthor", formDataAuthor)
    fetch(Url.PDAurl, {
      method: 'POST',
      credentials: 'omit',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formDataAuthor
    }).then(res => {
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: Login.js ~ line 2433 ~ Login ~ resData", resData)
      if (resData.status == '100') {
        if (resData.result.length == 1) {
          let result = resData.result[0]
          Url.PDAEmployeeId = result.EmployeeId
          Url.PDAEmployeeName = this.state.ID == 'pkpmfb' ? this.state.username : result.EmployeeName
          Url.PDAFid = result.factoryId
          Url.PDAFname = result.factoryName
          Url.PDAusername = this.state.username
          Url.chengJianPaiZhao = result.chengJianPaiZhao
          Url.isUsedDeliveryPlan = result.isUsedDeliveryPlan
          Url.jiaoZhuPaiZhao = result.jiaoZhuPaiZhao || "YES"
          Url.yinJianPaiZhao = result.yinJianPaiZhao || "YES"
          Url.isUseTaiWanID = result.isUseTaiWanID || "YES"
          Url.isResizePhoto = result.isResizePhoto || "600"
          Url.factoryCode = result.factoryCode || ""
          if (typeof result.isUsedSpareParts != 'undefined') {
            Url.isUsedSpareParts = result.isUsedSpareParts || ""
          }
          if (typeof result.appPhotoSetting != 'undefined') {
            let appPhotoSetting = []
            Url.appPhotoSetting = []
            appPhotoSetting = result.appPhotoSetting.split(",") || []
            appPhotoSetting.map(item => {
              Url.appPhotoSetting.push(item + '0')
            })
            let appDateSetting = []
            Url.appDateSetting = []
            appDateSetting = result.appDateSetting.split(",") || []
            appDateSetting.map(item => {
              Url.appDateSetting.push(item + '0')
            })
            let appPhotoAlbumSetting = []
            Url.appPhotoAlbumSetting = []
            appPhotoAlbumSetting = result.appPhotoAlbumSetting.split(",") || []
            appPhotoAlbumSetting.map(item => {
              Url.appPhotoAlbumSetting.push(item + '0')
            })
          }
          if (typeof result.isUsePhotograph !== 'undefined') {
            Url.isUsePhotograph = result.isUsePhotograph
          }
          let PDAauthorgray = [], gary = ''
          gary = result.gray
          PDAauthorgray = gary.split(",");
          Url.PDAauthorgray = PDAauthorgray
          Url.PDAnews = result.news
          this.setState({
            bottomFacVisible: false,
            logLoading: false
          })
          // 接收消息相关配置
          const sitecodeUrl = Url.url + 'User/' + 'GetSiteCode';
          this.GetSiteCode(sitecodeUrl, Url.PDAEmployeeId, Url.factoryCode);
          this.props.navigation.navigate('Main1');
        } else if (resData.result.length > 1) {
          this.setState({
            logLoading: false,
            bottomFacVisible: true,
            PDAFacArr: resData.result
          })
        }
      } else {
        Alert.alert('登录错误', resData.message)
      }
    }).catch(err => {
      Alert.alert('登录错误', 'getmanyfactorypdalimit' + err.toString())

    })

  }

  //PDA登陆
  PDALogin = (FactoryId) => {
    let formData = new FormData();
    let data = {};
    data = {
      "action": "loginChk",
      "servicetype": "pda",
      "express": "A45162C0",
      "ciphertext": "770fdb27cc9d018dc2d6c5d64ce5ff23",
      "data": {
        "userName": this.state.username,
        "pwd": this.state.password,
        "factoryId": FactoryId
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    fetch(Url.PDAurl, {
      method: 'POST',
      credentials: 'omit',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.status);
      return res.json();
    }).then(resData => {

      if (resData.status == '100') {
        Url.PDAEmployeeId = resData.result.EmployeeId
        Url.PDAEmployeeName = resData.result.EmployeeName
        Url.PDAusername = this.state.username
        let PDAauthorgray = [], gary = ''
        gary = resData.result.gray
        PDAauthorgray = gary.split(",");
        Url.PDAauthorgray = PDAauthorgray
        Url.PDAnews = resData.result.news
        this.setState({
          bottomFacVisible: false
        })
        this.props.navigation.navigate('Main1');
      } else {
        this.setState({
          bottomFacVisible: false
        })
        Alert.alert('登录失败', resData.message)
      }
    }).catch((error) => {
      Alert.alert("Login -> GetSysLogo -> error", error)
    });

  }

}

const styles = StyleSheet.create({
  topFlex: {
    flex: 4
  },
  imageFlex: {
    flex: 5,
    justifyContent: 'center',
    alignContent: 'center'
  },
  titleFlex: {
    flex: 1.5,
    fontSize: 24,
    textAlign: 'center',
    color: '#FFFFFF'
  },
  engTitleFlex: {
    flex: 1.3,
    fontSize: 20,
    textAlign: 'center',
    color: 'rgba(255,255,255,0.8)'
  },
  container: {
    justifyContent: 'center',
    alignContent: 'center',
    flex: 1,

    //backgroundColor: '#fff'
  },
  textBtnContainer: {
    marginVertical: 5,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  loading: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    //backgroundColor: '#F5FCFF',
  },
  secret: {
    textAlignVertical: 'center',
    color: '#419FFF',
    fontSize: RFT * 3
  },
  loginSelView: {
    width: width * 0.48,
    height: width * 1.1,
    marginLeft: width * 0.24,
    marginTop: width * 0.3
  },
  loginSelCard: {
    marginVertical: 8,
    borderRadius: 20,
    borderColor: 'transparent',
    opacity: 0.8,
    marginBottom: 20
  },
  loginSelCard_Image: {
    height: 50,
    width: 50,
    marginLeft: width * 0.1,
    marginVertical: 20
  }
});

if (deviceWidth <= 330) {
  styles.topFlex = {
    flex: 2,
  }
  styles.imageFlex = {
    flex: 7,
    justifyContent: 'center',
    alignContent: 'center'
  }
  styles.titleFlex = {
    flex: 1.5,
    fontSize: 20,
    textAlign: 'center',
    color: '#FFFFFF'
  }
  styles.engTitleFlex = {
    flex: 1.3,
    fontSize: 18,
    textAlign: 'center',
    color: 'rgba(255,255,255,0.8)'
  }
  styles.loginSelView = {
    width: width * 0.53,
    height: width * 0.5,
    marginLeft: width * 0.2,
    marginTop: width * 0.3
  }
  styles.loginSelCard = {
    marginVertical: 8,
    borderRadius: 20,
    borderColor: 'transparent',
    opacity: 0.8,
    marginBottom: 20
  }
  styles.loginSelCard_Image = {
    height: 50,
    width: 50,
    marginLeft: RFT * 9,
    marginVertical: 20
  }
}

class SecretPage extends React.Component {
  render() {
    return (
      <WebView
        source={{ uri: 'http://smart.pkpm.cn:81/secret.html' }}
      // style={{ marginTop: 20 }}
      />
    );
  }
}

class BackIcon extends React.Component {
  render() {
    return (
      <TouchableOpacity
        onPress={this.props.onPress} >
        <Icon
          name='arrow-back'
          color='#419FFF' />
      </TouchableOpacity>
    )
  }
}

export default Login;
