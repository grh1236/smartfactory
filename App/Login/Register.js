import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Alert,
  TouchableOpacity,
  ImageBackground,
  KeyboardAvoidingView
} from 'react-native';
import { Button, Icon, Text, Overlay, Header } from 'react-native-elements';
import { WebView } from 'react-native-webview';
//import Button from './Components/Button';
import TextField from './Components/TextField';
import request from '../Url/request';
import TextButton from './Components/TextButton';
import Url from '../Url/Url';

class Register extends Component {
  constructor() {
    super();
    this.state = {
      loginName: '',
      pwd: '',
      press: false,
      secretVisible: false
    }
  }

  render() {
    const { loginName, pwd, press } = this.state;
    return (
      <ImageBackground
        source={require('../viewImg/Login_1248_2208.jpg')} style={styles.container}>
        <KeyboardAvoidingView behavior='padding'>
          <Text style={{fontSize:17,color:'white'}}>用户名 </Text>
          <TextField
            placeholder='请设置用户名'
            onChange={value => this.setState({ loginName: value })}
          />
          <Text style={{ fontSize: 17, color: 'white' }}>密码 </Text>
          <TextField
            type='pwd'
            placeholder='请设置密码'
            onChange={value => this.setState({ pwd: value })}
          />

          <View style={{ flexDirection: 'row', marginBottom: 13, marginTop: 3, justifyContent: 'flex-end' }}>
            <Icon
              name={this.state.press ? 'checkcircle' : 'checkcircleo'}
              color={this.state.press ? '#FFFFFF' : '#535c68'}
              type='antdesign'
              size={16}
              containerStyle={{ justifyContent: 'center' }}
              onPress={() => {
                this.setState({ press: !this.state.press })
              }} />
            <Text
              style={[styles.secret, {}]}
              onPress={() => {
                this.setState({ press: !this.state.press })
              }}>我已阅读并同意PKPM智慧工厂</Text>
            <Text style={[styles.secret, { color: '#2c3e50' }]}
              onPress={() => {
                this.setState({ secretVisible: !this.state.secretVisible })
              }} >用户协议及隐私策略</Text>
          </View>

          <Button
            disabled={!loginName || !pwd || !press}
            onPress={this.register}
            title='注册'
            titleStyle={{ color: '#419FFF' }}
            buttonStyle={{ backgroundColor: 'white' }}
          ></Button>

          <View style = {{marginVertical: 8}}>
            <TextButton disabled={false}
              onPress={() => this.props.navigation.navigate('Login')}
            >
              已有账号？返回登陆
          </TextButton>
          </View>

          <Overlay
            fullScreen={true}
            animationType='fade'
            onBackdropPress = {() => {
              this.setState({ secretVisible: !this.state.secretVisible });
            }}
            isVisible={this.state.secretVisible}
            onRequestClose={() => {
              this.setState({ secretVisible: !this.state.secretVisible });
            }} >
            <Header
              centerComponent={{ text: '免责声明', style: { color: 'gray', fontSize: 20 } }}
              rightComponent={<BackIcon
                onPress={() => {
                  this.setState({
                    secretVisible: !this.state.secretVisible
                  });
                }} />}
              backgroundColor='white'
            />
            <SecretPage />
            <View style={{ flexDirection: 'row' }}>
            </View>
          </Overlay>
        </KeyboardAvoidingView>
      </ImageBackground>
    );
  }

  register = () => {
    //const loginurl = 'https://smart.pkpm.cn:910/api/FactoryIPMange'
    const loginurl = 'http://smart.pkpm.cn:9100/pcservice/V1/pdaservice.ashx'
    const { loginName, pwd } = this.state;
      const data = {
          "action": "registeruser",
          "servicetype": "pda",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
              "userName": loginName,
              "pwd": pwd
          }
      };
    console.log(data)
    console.log(JSON.stringify(data))
    //Alert.alert('注册成功')
    //this.props.navigation.navigate('Login')
      let formData = new FormData();
      formData.append('jsonParam', JSON.stringify(data))
    fetch(loginurl, {
      method: 'POST',
      headers: {
         'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.status);
      return res.json();
    }).then(resData => {
        console.log(resData)
        if (resData.status == '100') {
            Alert.alert('注册成功')
            this.props.navigation.navigate('Login')
        } else {
            Alert.alert(resData.message)
        }
    }).catch(err => {
      console.log(err);
      console.log(res.status);
    })
    /* request({
      method: 'POST',
      url: 'http://47.94.205.217:910/api/FactoryIPMange',
      data: {loginName: loginName,pwd: pwd }
    })
      .then(() => {
        Alert.alert('注册成功');
      })
      .catch(err => {
        console.log(err)
        Alert.alert('注册失败', err);
      });*/
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 16,
    backgroundColor: '#fff'
  },
  textBtnContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  secret: {
    textAlignVertical: 'center',
    color: '#FFFFFF'
  }
});

class BackIcon extends React.Component {
  render() {
    return (
      <TouchableOpacity
        onPress={this.props.onPress} >
        <Icon
          name='arrow-back'
          color='#419FFF' />
      </TouchableOpacity>
    )
  }
}

class RefreshIcon extends React.Component {
  render() {
    return (
      <TouchableOpacity
        onPress={this.props.onPress} >
        <Icon
          type='simple-line-icon'
          name='refresh'
          color='#419FFF' />
      </TouchableOpacity>
    )
  }
}

class SecretPage extends React.Component {
  render() {
    return (
      <WebView
        source={{ uri: 'http://smart.pkpm.cn:81/secret.html' }}
      // style={{ marginTop: 20 }}
      />
    );
  }
}

export default Register;
