import React, { Component } from 'react';
import {
  View,
  TextInput,
  Text,
  StyleSheet
} from 'react-native';
import { Input } from 'react-native-elements';
import { deviceWidth } from '../../Url/Pixal';

export default class TextField extends Component {
  render() {
    const { label, placeholder, type, onChange, icon } = this.props;
    return (
      <View style={styles.textField}>
        {/* <Text style={[styles.label,{color: 'gray'}]}>{label}</Text> */}
        <Input
          {... this.props}
          allowFontScaling={false}
          containerStyle={styles.containerStyle}
          labelStyle={{ color: 'rgba(255,255,255,0.9)' }}
          leftIconContainerStyle={{ marginHorizontal: 8 }}
          //autoCapitalize= 'characters'
          style={styles.edit}
          secureTextEntry={type === 'password'}
          keyboardType='ascii-capable'
          selectTextOnFocus={true}
          onChangeText={onChange}
          inputStyle={{ color: 'rgba(255,255,255,0.9)' }}
          inputContainerStyle={{ backgroundColor: 'transparent', borderColor: 'transparent' }}
          placeholderTextColor='rgba(255,255,255,0.9)'
          //numberOfLines = {0}
          // underlineColorAndroid = 'rgba(255,255,255,0.9)'
          underlineColorAndroid="transparent"
          autoCorrect={false}
          renderErrorMessage={true}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  textField: {
    padding: 0,
    flexDirection: 'row',
    alignItems: 'center',
  },
  label: {
    textAlign: 'center',
    fontSize: 17,
    width: 85,
    paddingVertical: 2
  },
  edit: {
    flex: 1,
    fontSize: 17,
  },
  containerStyle: {
    marginVertical: 10,
    backgroundColor: 'rgba(255,255,255,0.3)',
    height: 50
  }
});

if (deviceWidth <= 330) {
  styles.containerStyle = {
    marginVertical: 6,
    backgroundColor: 'rgba(255,255,255,0.3)',
    height: 47
  }
}


