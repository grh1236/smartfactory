/*
 * @Author: 高睿豪 1375308739@qq.com
 * @Date: 2023-04-14 11:25:26
 * @LastEditors: 高睿豪 1375308739@qq.com
 * @LastEditTime: 2023-04-14 11:25:37
 * @FilePath: /SmartFactory/App/Login/Components/fetchPromise.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
/**
 * 实现fetch的timeout 功能
 * @param {object} fecthPromise fecth
 * @param {Number} timeout 超时设置，默认5000ms
 * */
export function fetch_timeout(fecthPromise, timeout = 5000, controller) {
  let abort = null;
  let abortPromise = new Promise((resolve, reject) => {
    abort = () => {
      return reject({
        code: 504,
        message: '请求超时!',
      });
    };
  });
  // 最快出结果的promise 作为结果
  let resultPromise = Promise.race([fecthPromise, abortPromise]);
  setTimeout(() => {
    abort();
    controller.abort();
  }, timeout);

  return resultPromise.then((res) => {
    clearTimeout(timeout);
    return res;
  });
}