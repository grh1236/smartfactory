import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Text
} from 'react-native';

class TextButton extends Component {
  render() {
    const { children, onPress, disabled } = this.props;
    return (
      <TouchableOpacity
      disabled = {disabled}
        onPress={onPress}
        
      >
        <Text style={disabled ? {color: '#2c3e50'} : styles.buttonText}>{children}</Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  buttonText: {
    color: '#FFFFFF'
  }
});

export default TextButton;
