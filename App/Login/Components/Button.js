import React, { Component } from 'react';
import {
  StyleSheet
} from 'react-native';
import { Button } from 'react-native-elements';

export default class ButtonC extends Component {
  render() {
    const { style, textStyle, children } = this.props;
    return (
      <Button
        {...this.props}
        style={[styles.button, style]}
        textStyle={[styles.buttonText, textStyle]}
        type= 'clear'
        
      >{children}</Button>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    flex:1,
    marginVertical: 16,
    backgroundColor: '#1890ff',
    borderRadius: 5,
    borderWidth: 0
  },
  buttonText: {
    color: '#fff',
    fontSize: 16
  }
});
