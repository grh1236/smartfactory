import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Platform,
  Alert,
  Linking,
  Dimensions,
  StatusBar,
  DeviceEventEmitter
} from 'react-native';
import {
  isFirstTime,
  isRolledBack,
  packageVersion,
  currentVersion,
  checkUpdate,
  downloadUpdate,
  switchVersion,
  switchVersionLater,
  markSuccess,
} from 'react-native-update';
import { Avatar, Button, Icon, Overlay, Card, Header } from 'react-native-elements';
import { SwitchActions, NavigationActions, StackActions } from 'react-navigation'
import { WebView } from 'react-native-webview';
import LinearGradient from 'react-native-linear-gradient';
import Url from '../Url/Url';
import DeviceStorage from '../Url/DeviceStorage'
import _updateConfig from '../../update.json';
import navigationUtil from '../Url/navigation'
import { FlatList } from 'react-native-gesture-handler';
import JPush from 'jpush-react-native';
import Wave from '../CommonComponents/Wave';
import { Surface, Shape, Path, } from '@react-native-community/art';
import { RFT } from '../Url/Pixal';


import PushUtil from '../CommonComponents/PushUtil'
import { deviceWidth } from '../Url/Pixal';


const { appKey } = _updateConfig[Platform.OS];
let changeFac = false;

const { height, width } = Dimensions.get('screen');

class SecretPage extends React.Component {
  render() {
    return (
      <WebView
        source={{ uri: 'http://smart.pkpm.cn:81/secret.html' }}
      // style={{ marginTop: 20 }}
      />
    );
  }
}

//头部
class HeaderTitle extends React.Component {
  render() {
    return (
      <LinearGradient
        start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
        colors={['rgba(37, 116, 255, 0.8)', 'rgba(65, 255, 255, 0.8)']}
        style={{ width: width, height: width * 0.6, }}
      >
        <View style={styles.headerTitleContainer}>

          <View style={{ marginTop: 16 + '%' }}>
            <TouchableOpacity style={styles.headerAvatarContent} onPress={() => this.onPress()}>
              {/* <Image style={styles.headerRing} source={ImgConfig.default_transparent}/> */}
              <Avatar
                style={styles.headerAvatar} rounded
                icon={{ name: 'user', type: 'antdesign', size: 56 }}
                showEditButton
              />
            </TouchableOpacity>
            <Text style={styles.headerName}>{this.props.text}</Text>
          </View>
          <View style={{ width: width, marginTop: 20 }}>
            <Wave
              waveHeight={60}
              waveWidth={width}
              wave1Color='#F9F9F9'
              wave2Color='rgba(255,255,255,0.8)' />
          </View>
        </View>
      </LinearGradient>
    );
  }
  onPress() {
    console.log('hello');
  }
}

class InfoContent extends Component {
  render() {
    let { top = 0, text = "", name, color, type } = this.props;
    return (
      <View style={[styles.infoContent, { marginTop: top }]}>
        <TouchableOpacity style={styles.infoTouchable} onPress={this.props.onPress} >
          <View style={styles.infoFlex}>
            <Icon
              type={type}
              size={20}
              name={name}
              color={color}
              containerStyle={{ marginHorizontal: 15 }} />
            <Text style={styles.infoName}>{text}</Text>
          </View>
          <View style={[styles.infoFlex, styles.infoIconContent]}>

          </View>
        </TouchableOpacity>
      </View>
    )
  }
}

export default class Mine_Screen extends React.Component {
  constructor() {
    super()
    this.state = {
      isVisible: false,
      userInfo: [],
      userName: 'pkpm',
      secretVisible: false,
      ID: '',
    }
    this.changeFactory = this.changeFactory.bind(this);
  }

  componentWillMount() {
    if (isRolledBack) {
      Alert.alert('提示', '刚刚更新失败了,版本被回滚.');
    }
  }

  componentDidMount() {
    Url.Login()
    DeviceStorage.get('ID')
      .then(res => {
        if (res) {
          this.setState({
            ID: Url.ID == 'pkpmfb' ? 'pkpmfb' : res
          })
          //console.log(res)
        }
      }).catch(err => { })

    this.changeFactory();
    DeviceStorage.get('changeFac').then(res => {
      changeFac = res
    })
    this._navListener = this.props.navigation.addListener('didFocus', () => {
      //StatusBar.setBarStyle('dark-content');
      StatusBar.setBackgroundColor('black');
    });
  }


  changeFactory = () => {
    let url = ''
    if (Url.ID != 'pkpmfb') {
      url = Url.url + '/User/' + Url.username + '/GetUserInfo'
    } else {
      url = Url.url + '/User/pkpm/GetUserInfo'
    }
    fetch(url, {
      credentials: 'include',
    }).then(res2 => {
      return res2.json()
    }).then(userInfo => {
      let obj = {};
      userInfo = userInfo.reduce((item, next) => {
        obj[next.deptName] ? '' : obj[next.deptName] = true && item.push(next);
        return item;
      }, []);
      this.setState({
        userInfo: userInfo,
        userName: Url.ID == 'pkpmfb' ? Url.username : userInfo[0].employeeName
      })

      console.log(userInfo)
    }).catch((error) => {
      console.log(error);
    });
  }

  /* userGet = () => {
    DeviceStorage.get('user')
    .then(res => {
        this.setState({
          username: res
        })
    }).catch(err => {
      console.log(err);
    })
  } */

  doUpdate = info => {
    downloadUpdate(info).then(hash => {
      Alert.alert('提示', '下载完毕,是否重启应用?', [
        { text: '是', onPress: () => { switchVersion(hash); } },
        { text: '否', },
        { text: '下次启动时', onPress: () => { switchVersionLater(hash); } },
      ]).catch(err => {
        Alert.alert('提示', err);
      });
    })
  }

  checkUpdate = () => {
    checkUpdate(appKey).then(info => {
      console.log(info)
      if (info.expired) {
        Alert.alert('提示', '您的应用版本已更新,请前往应用商店下载新的版本', [
          { text: '确定', onPress: () => { info.downloadUrl && Linking.openURL(info.downloadUrl) } },
        ]);
      } else if (info.upToDate) {
        Alert.alert('提示', '您的应用版本是' + packageVersion + '\n已是最新版本');
      } else {
        Alert.alert('提示', '检查到新的版本' + info.name + ',是否下载?\n' + info.description, [
          { text: '是', onPress: () => { this.doUpdate(info) } },
          { text: '否', },
        ]);
      }
    }).catch(err => {
      Alert.alert('提示', JSON.stringify(err));
    });
  };

  deleteUser = () => {
    Alert.alert('提示', '注销账号后将无法再使用该账号，确定注销账号？', [
      {
        text: '确定', onPress: () => {
          const url = 'http://smart.pkpm.cn:9100/pcservice/V1/pdaservice.ashx'
          const data = {
            "action": "deleteuser",
            "servicetype": "pda",
            "express": "E2BCEB1E",
            "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
            "data": {
              "userName": Url.username,
            }
          };
          console.log(data)
          console.log(JSON.stringify(data))
          //Alert.alert('注册成功')
          //this.props.navigation.navigate('Login')
          let formData = new FormData();
          formData.append('jsonParam', JSON.stringify(data))
          fetch(url, {
            method: 'POST',
            headers: {
              'Content-Type': 'multipart/form-data'
            },
            body: formData
          }).then(res => {
            console.log(res.status);
            return res.json();
          }).then(resData => {
            console.log(resData)
            if (resData.status == '100') {
              Alert.alert('注销成功', '该账号已注销，即将返回登录界面', [{ text: '确定', onPress: () => { this.logout() } }])

            } else {
              Alert.alert(resData.message)
            }
          }).catch(err => {
            console.log(err);
            console.log(res.status);
          })
        }
      },
      { text: '取消', }
    ]);
  };

  componentWillUnmount() {
    this._navListener.remove();
  }

  render() {
    console.log(Url)
    return (
      <View >
        <View style={{ flex: 1 }}>
          <HeaderTitle text={this.state.userName}></HeaderTitle>
          {/* <InfoContent text="个人设置" name='setting' top={16} /> */}
          <InfoContent text="切换业务范围" name='exchange' type="font-awesome" color={"#419fff"} top={16} onPress={() => {
            this.setState({
              isVisible: !this.state.isVisible,
            });
          }} />
          {/*  <InfoContent text="修改密码" name='unlock' top={16} /> */}
          <InfoContent text="重新登录" name='user' type="font-awesome" color={"#419fff"} top={16} onPress={this.logout} />
          {/* <InfoContent text="清除缓存" name='delete' type="material" color={"#419fff"} top={16} onPress={this.getCache} /> */}
          <InfoContent text="检查更新" name='download' type="font-awesome" color={"#41DFA2"} top={16} onPress={this.checkUpdate} />
          {
            this.state.ID == 'pkpmfb' ? <InfoContent text="账号注销" name='sign-out' type="font-awesome" color={"#41DFA2"} top={16} onPress={this.deleteUser} /> : <View></View>
          }


          {/* <View style = {{marginTop: RFT * 60}}>
            <Text onPress={() => { this.setState({ secretVisible: !this.state.secretVisible }) }} style={[styles.bottomText1]}>免责声明</Text>
            <Text style={[styles.bottomText2]}>{'当前版本: V ' + packageVersion}</Text>
          </View> */}
        </View>


        <Overlay
          fullScreen={true}
          animationType='fade'
          isVisible={this.state.secretVisible}
          onRequestClose={() => {
            this.setState({ secretVisible: !this.state.secretVisible });
          }} >
          <Header
            centerComponent={{ text: '免责声明', style: { color: 'gray', fontSize: 20 } }}
            rightComponent={<BackIcon
              onPress={() => {
                this.setState({
                  secretVisible: !this.state.secretVisible
                });
              }} />}
            backgroundColor='white'
          />
          <SecretPage />
          <View style={{ flexDirection: 'row' }}>
          </View>
        </Overlay>

        <Overlay
          fullScreen={true}
          animationType='fade'
          isVisible={this.state.isVisible}
          onRequestClose={() => {
            this.setState({ isVisible: !this.state.isVisible });
          }}>
          <Header
            leftComponent={<RefreshIcon
              onPress={this.changeFactory.bind(this)} />}
            centerComponent={{ text: '切换业务范围', style: { color: 'gray', fontSize: 20 } }}
            rightComponent={<BackIcon
              onPress={() => {
                this.setState({
                  isVisible: !this.state.isVisible
                });
              }} />}
            backgroundColor='white'
          />
          <FlatList
            renderItem={this._renderItem}
            data={this.state.userInfo} />
        </Overlay>
      </View>
    );
  }

  _renderItem = ({ item, index }) => {
    if (item.deptType === 40) {
      return (
        <TouchableOpacity
          onPress={() => {
            if (Url.ChangeFac) {
              Url.Fid = item.deptId + '/';
              Url.Fidweb = item.deptId;
              Url.Factoryname = item.deptName;
              Url.EmployeeId = item.employeeId;
              console.log(Url.url + '/User/' + '/Employeeid/' + item.employeeId + '/Deptid/' + item.deptId + '/GetFacMenu');
              fetch(Url.url + '/User/' + '/Employeeid/' + item.employeeId + '/Deptid/' + item.deptId + '/GetFacMenu',
                {
                  credentials: 'include',
                }
              ).then(resMenu => {
                console.log('resMenu');
                console.log(resMenu.ok);
                return resMenu.text()
              }).then(resDataMenu => {
                console.log(resDataMenu)
                let MenuArray = [];
                MenuArray = resDataMenu.split(",");
                //console.log(MenuArray);
                DeviceStorage.save('Menu', MenuArray);
                Url.Menu = MenuArray;
                if (Url.Menu.indexOf('APP日常') != -1) {
                  DeviceStorage.save('daily', true);
                  Url.Daily = true
                } else {
                  DeviceStorage.save('daily', false);
                  Url.Daily = false
                }
                if (Url.Menu.indexOf('APP项目') != -1) {
                  DeviceStorage.save('project', true);
                  Url.Project = true
                } else {
                  DeviceStorage.save('project', false);
                  Url.Project = false
                }
                if (Url.Menu.indexOf('APP堆场') != -1) {
                  DeviceStorage.save('stocks', true)
                  Url.Stocks = true
                } else {
                  DeviceStorage.save('stocks', false);
                  Url.Stocks = false
                }
                if (Url.Menu.indexOf('APP经营') != -1) {
                  DeviceStorage.save('business', true);
                  Url.Business = true
                } else {
                  DeviceStorage.save('business', false);
                  Url.Business = false
                }
                if (Url.Menu.indexOf('APP模型') != -1) {
                  DeviceStorage.save('model', true);
                  Url.Model = true
                } else {
                  DeviceStorage.save('model', false);
                  Url.Model = false
                }
                if (Url.Menu.indexOf('APP切换工厂') != -1 || Url.Menu.indexOf('APP切换业务范围') != -1) {
                  DeviceStorage.save('changeFac', true);
                  Url.ChangeFac = true
                } else {
                  DeviceStorage.save('changeFac', false);
                  Url.ChangeFac = false
                }
              }).catch((error) => {
                console.log(error)
              });
              console.log(Url.changeFac)
              this.GetUnreadMassage();

              this.GetCompInfo()

              PushUtil.deleteTag(Url.deptCode, (code, remain) => {
                if (code == 200) {
                  console.log("🚀 ~ file: MineView.js ~ line 368 ~ Mine_Screen ~ PushUtil.deleteTag ~ remain", remain)
                }
              })
              PushUtil.deleteTag(Url.ID + "_" + Url.deptCode, (code, remain) => {
                if (code == 200) {
                  console.log("🚀 ~ file: MineView.js ~ line 368 ~ Mine_Screen ~ PushUtil.deleteTag ~ remain", remain)
                }
              })
              PushUtil.addTag(item.deptCode, (code, remain) => {
                if (code == 200) {
                  this.setState({ result: remain });
                }
              })
              PushUtil.addTag(Url.ID + "_" + item.deptCode, (code, remain) => {
                if (code == 200) {
                  this.setState({ result: remain });
                }
              })
              PushUtil.listTag((code, tagList) => {
                if (code == 200) {
                  let tags = "tags:";
                  console.log("PushUtil xxxxxx length=" + tagList.length)
                  for (var i = 0; i < tagList.length; i++) {
                    tags = tags + tagList[i] + "\n";
                    console.log("PushUtil xxxxxx tags=" + tags)
                  }
                }
              })
              Url.deptCode = item.deptCode

              PushUtil.deleteAlias(Url.employeeIdAlias, "employeeId", (code) => {
                console.log("🚀 ~ file: MineView.js ~ line 400 ~ Mine_Screen ~ PushUtil.deleteAlias ~ code", code)
                if (code == 200) {
                  console.log("🚀 ~ file: MineView.js ~ line 391 ~ Mine_Screen ~ PushUtil.deleteAlias ~ code", code)
                }
              })

              let employeeId = item.employeeId.replace(/-/g, "")
              console.log("🚀 ~ file: Login.js ~ line 1253 ~ Login ~ fetch ~ PushUtilemployeeId", employeeId)
              let employeeIdAlias = Url.ID + '_' + item.deptCode + '_' + employeeId
              employeeIdAlias = employeeIdAlias.toLowerCase()
              console.log("🚀 ~ file: Login.js ~ line 1266 ~ Login ~ fetch ~ PushUtilemployeeIdAlias", employeeIdAlias)
              Url.employeeIdAlias = employeeIdAlias

              PushUtil.addAlias(employeeIdAlias, "employeeId", (code) => {
                console.log("🚀 ~ file: Login.js ~ line 1255 ~ Login ~ PushUtil.addAlias ~ code", code)
                if (code == 200) {
                  console.log("🚀 ~ file: Login.js ~ line 1254 ~ Login ~ PushUtil.addAlias ~ code", code)
                }
              })


              JPush.updateTags({ "sequence": 1, "tags": [this.state.ID, item.employeeName, item.deptCode] })
              DeviceStorage.save('Fid', item.deptId + '/');
              DeviceStorage.save('Fidweb', item.deptId);
              DeviceStorage.save('FacName', item.deptName);
              DeviceStorage.save('Factoryname', item.deptName);
              //Url.NoticeList = []
              navigationUtil.reset(this.props.navigation, 'Main');
              this.setState({ isVisible: !this.state.isVisible })
            } else {
              Alert.alert('权限提醒', '您没有切换业务范围的权限。')
            }
          }} >
          <Card
            containerStyle={{ marginVertical: 8, borderRadius: 8 }} >
            <Text style={{ textAlign: 'center', color: '#419FFF' }}>{item.deptName}</Text>
          </Card>
        </TouchableOpacity>
      )
    }
  }

  logout = () => {
    //navigationUtil.reset(this.props.navigation, 'Login'); 
    DeviceStorage.save('isLogin', false);
    PushUtil.deleteTag(Url.ID + "_" + Url.deptCode, (code, remain) => {
      if (code == 200) {
        console.log("🚀 ~ file: MineView.js ~ line 368 ~ Mine_Screen ~ PushUtil.deleteTag ~ remain", remain)
      }
    })
    PushUtil.deleteTag(Url.ID, (code, remain) => {
      if (code == 200) {
        console.log("🚀 ~ file: MineView.js ~ line 368 ~ Mine_Screen ~ PushUtil.deleteTag ~ remain", remain)
      }
    })
    PushUtil.deleteTag(Url.deptCode, (code, remain) => {
      if (code == 200) {
        console.log("🚀 ~ file: MineView.js ~ line 368 ~ Mine_Screen ~ PushUtil.deleteTag ~ remain", remain)
      }
    })
    PushUtil.deleteAlias(Url.employeeIdAlias, "employeeId", (code) => {
      console.log("🚀 ~ file: MineView.js ~ line 400 ~ Mine_Screen ~ PushUtil.deleteAlias ~ code", code)
      if (code == 200) {
        console.log("🚀 ~ file: MineView.js ~ line 391 ~ Mine_Screen ~ PushUtil.deleteAlias ~ code", code)
      }
    })
    Url.employeeIdAlias = ""
    JPush.deleteTags({ sequence: 1 })
    Url.NoticeList = []
    Url.Fid = ''
    Url.Fidweb = ''
    DeviceStorage.save('daily', false);
    DeviceStorage.save('project', false);
    DeviceStorage.save('stocks', false);
    DeviceStorage.save('business', false);
    DeviceStorage.save('model', false);
    DeviceStorage.save('changeFac', false);
    fetch(Url.url + '/User/LogOut', {
      method: 'Post'
    }).then(res => {
      return res.text()
    }).then(resData => {
      console.log("🚀 ~ file: MineView.js:550 ~ Mine_Screen ~ resData:", resData)
    }).catch((error) => {
      console.log(error);
    });
    // const resetAction = StackActions.reset({
    //   index: 0,
    //   actions: [
    //     NavigationActions.navigate({ routeName: 'Login' })//要跳转到的页面名字
    //   ]
    // });
    // this.props.navigation.dispatch(resetAction);
    this.props.navigation.dispatch(SwitchActions.jumpTo({ routeName: 'Login' }));
    //this.props.navigation.reset([NavigationActions.navigate({ routeName: 'Login' })], 0);
  }

  GetUnreadMassage = () => {
    const url = Url.url + Url.factory + 'facId/' + Url.Fid + 'recId/' + Url.EmployeeId + '/GetUnSeenMsgListApp'
    console.log("Login -> url", url)
    fetch(url, {
      credentials: 'include',
    }).then(res => {
      console.log(res.status);
      return res.json()
    }).then(resData => {
      console.log("App -> resData.length", resData.length)
      DeviceEventEmitter.emit('noticecount', resData.length);
    }).catch((err) => {
      console.log("Login -> err", err)
    })
  }

  GetCompInfo = () => {
    const url = Url.url + 'Factories/' + Url.Fid + 'GetSearchCondition'
    console.log("🚀 ~ file: Login.js ~ line 1888 ~ Login ~ url", url)
    fetch(url, {
      credentials: 'include',
    }).then(res => {
      console.log(res.status);
      return res.json()
    }).then(resData => {
      console.log("🚀 ~ file: Login.js ~ line 1894 ~ Login ~ fetch ~ resData", resData)
      console.log("🚀 ~ file: Login.js ~ line 1896 ~ Login ~ fetch ~ resData.length", resData.length)
      DeviceStorage.save('workSpaceSearchList', resData);
      if (resData.toString().length > 0) {
        Url.employeeList = resData.lstEmployee
        Url.compTypeNameList = resData.lstCompType
        Url.compRoomNameList = resData.lstLibrary
        Url.bigStateList = resData.lstCompState
        Url.productLineNameList = resData.lstProductionLine
      }
      console.log("🚀 ~ file: Login.js ~ line 1894 ~ Login ~ fetch ~ Url", Url)

    }).catch((err) => {
      console.log("🚀 ~ file: Login.js ~ line 1901 ~ Login ~ fetch ~ err", err)
    })
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
  },
  headerTitleContainer: {
    width: width,
    height: width * 0.6,
    //backgroundColor: '#419FFF',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerRing: {
    width: 79,
    height: 79,
    top: 0,
    left: 0,
    right: 0,
    borderWidth: 0.5,
    borderRadius: 39.5,
    borderColor: '#FFFFFF',
    position: 'absolute',
  },
  headerAvatar: {
    width: 70,
    height: 70,
    borderRadius: 35,
  },
  headerName: {
    textAlign: "center",
    fontSize: 16,
    marginTop: 5,
    color: '#fff'
  },
  headerAvatarContent: {
    width: 79,
    height: 79,
    marginTop: 49,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  infoContent: {
    width: deviceWidth * 0.94,
    marginLeft: deviceWidth * 0.03,
    height: 44,
    position: 'relative',
    backgroundColor: '#F9FFFF',
    borderRadius: 15
  },
  infoTouchable: {
    paddingLeft: 16,
    width: null,
    height: 44,
    flexDirection: 'row',
    alignItems: 'center',
  },
  infoFlex: {
    flexDirection: 'row',
    flex: 1,
  },
  infoName: {
    fontSize: 14,
    color: '#333333'
  },
  infoIconContent: {
    alignItems: 'flex-end',
    marginRight: 15,
  },
  name: {
    fontSize: 14,
  },
  user: {
    textAlign: 'center',
    justifyContent: 'center'
  },
  bottomText1: {
    //marginTop: height * 0.3,
    textAlignVertical: 'bottom',
    textAlign: 'center',
    fontSize: RFT * 4.5,
    color: '#419FFF'
  },
  bottomText2: {
    marginTop: 8,
    textAlignVertical: 'bottom',
    textAlign: 'center',
    fontSize: RFT * 3.5,
  },
})

class BackIcon extends React.Component {
  render() {
    return (
      <TouchableOpacity
        onPress={this.props.onPress} >
        <Icon
          name='arrow-back'
          color='#419FFF' />
      </TouchableOpacity>
    )
  }
}

class RefreshIcon extends React.Component {
  render() {
    return (
      <TouchableOpacity
        onPress={this.props.onPress} >
        <Icon
          type='simple-line-icon'
          name='refresh'
          color='#419FFF' />
      </TouchableOpacity>
    )
  }
}