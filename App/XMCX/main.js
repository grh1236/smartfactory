import React, { Component } from 'react';

import {
  View, Text, StyleSheet, ScrollView, Alert, AppState, ActivityIndicator,
  Linking, Animated, DeviceEventEmitter, TouchableOpacity, Dimensions, StatusBar, NativeModules, ImageBackground, TouchableHighlight
} from 'react-native';
import { Button, ListItem, Badge, Image } from 'react-native-elements';
import { RFT, deviceWidth, deviceHeight, RVW } from '../Url/Pixal';
import Url from '../Url/Url';
import commonStyle from './Components/commonStyle';


let card = [
  { "index": 0, "title": "生产", "page": "ShengChan", "img": require("./img/shengchan3x.png") },
  { "index": 1, "title": "库存", "page": "KuCun", "img": require("./img/kucun3x.png") },
  { "index": 2, "title": "发货", "page": "FaHuo", "img": require("./img/fahuo3x.png") },
  { "index": 3, "title": "楼图", "page": "LouTu", "img": require("./img/loutu3x.png") },
  { "index": 4, "title": "分析", "page": "FenXi", "img": require("./img/fenxi3x.png") },
  { "index": 5, "title": "汇总", "page": "HuiZong", "img": require("./img/huizong3x.png") },
]

let operationCard = [
  { "index": 0, "title": "收货确认单", "page": "MainCheckPageXMCX", "mainpage": "Main_CompReceive", "pagetype": "operation", "img": require("./img/goujianshouhuodan.png") },
  { "index": 1, "title": "构件报修", "page": "MainCheckPageXMCX", "mainpage": "Main_CompRepair", "pagetype": "operation", "img": require("./img/goujianbaoxiu.png") },
  //{ "index": 2, "title": "报修确认", "page": "MainCheckPageXMCX", "mainpage": "Main_CompRepair", "pagetype": "operation", "img": require("./img/baoxiuqueren.png") },
  { "index": 2, "title": "维修确认", "page": "MainCheckPageXMCX", "mainpage": "Main_CompMaintain", "pagetype": "operation", "img": require("./img/goujianweixiu.png") },
]

let checkCard = [
  { "index": 0, "title": "收货确认单", "page": "MainCheckPageXMCX", "mainpage": "Main_CompReceive", "pagetype": "check", "img": require("./img/goujianshouhuodan.png") },
  { "index": 1, "title": "构件报修", "page": "MainCheckPageXMCX", "mainpage": "Main_CompRepair", "pagetype": "check", "img": require("./img/goujianbaoxiu.png") },
  { "index": 2, "title": "报修确认", "page": "MainCheckPageXMCX", "mainpage": "Main_CompRepair", "pagetype": "check", "img": require("./img/baoxiuqueren.png") },
  { "index": 3, "title": "维修确认", "page": "MainCheckPageXMCX", "mainpage": "Main_CompMaintain", "pagetype": "check", "img": require("./img/goujianweixiu.png") },
]

export default class MainXMCXScreen extends React.Component {
  constructor() {
    super()
    this.state = {
      dayList: [
        { name: '今日', day: 'today' },
        { name: '昨日', day: 'yesterday' },
        { name: '本周', day: 'week' },
        { name: '本月', day: 'month' },
      ],
      resDataDayCard: {
        "proNumber": 0,
        "proVolume": 0.000,
        "storageNumber": 0,
        "storageVolume": 0.000,
        "outNumber": 0,
        "outVolume": 0.000,
        "planNumber": 0,
        "planVolume": 0.000
      },
      typeFocusButton: 0,
      isHidden1: false,
      isHidden2: false,
      isHidden3: false
    }
  }

  componentDidMount() {
    this.GetDailyStaistic('today')
  }

  GetDailyStaistic_old = (day) => {
    const url = Url.url + Url.project + Url.Fid + Url.ProjectId + '/GetProjectComponentStatistic' + day;

    console.log("🚀 ~ file: main.js ~ line 47 ~ MainXMCXScreen ~ url", url)
    fetch(url, {
      credentials: 'include',
    }).then(res => {
      return res.json()
    }).then(resData => {
      console.log("🚀 ~ file: main.js ~ line 50 ~ MainXMCXScreen ~ fetch ~ resData", resData)
      this.setState({
        resDataDayCard: resData,
      });
    }).catch((error) => {
      console.log(error);
    });
  }

  GetDailyStaistic = (day) => {
    const url = Url.url + Url.project + Url.Fid + Url.ProjectId + '/GetProjectComponentStatistic' + day;

    let formData = new FormData();
    let data = {};

    data = {
      "action": "getprojectcomponentstatistic" + day,
      "servicetype": "projectsystem",
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        "factoryId": Url.Fidweb,
        "projectId": Url.ProjectId,
        "isprj": 1
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: main.js ~ line 100 ~ MainXMCXScreen ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: main.js ~ line 96 ~ MainXMCXScreen ~ resData", resData)
      if (resData.status == '100') {
        let result = resData.result
        this.setState({
          resDataDayCard: result,
        });
      } else {
        Alert.alert(resData.message)
        this.toast.show(resData.message)
      }
    }).catch((error) => {
      console.log("🚀 ~ file: main.js ~ line 103 ~ MainXMCXScreen ~ error", error)
    });
  }



  render() {
    const { resDataDayCard } = this.state

    return (
      <View>
        <ScrollView>
          <StatusBar backgroundColor={'#656FF6'} />
          <View style={{ flex: 1 }}>
            <View style={{ flexDirection: 'row', backgroundColor: rgb(87, 101, 255, 0.1) }}>
              {
                this.state.dayList.map((item, index) => {
                  return (
                    <Button
                      buttonStyle={{
                        borderRadius: -10,
                        borderBottomColor: this.state.typeFocusButton == index ? '#6777EF' : '#999997',
                        borderBottomWidth: 3,
                        // width: 48,
                        height: 50,
                        marginLeft: 25
                      }}
                      containerStyle={{
                        borderColor: this.state.typeFocusButton == index ? '#6777EF' : '#999997',
                      }}
                      title={item.name}
                      titleStyle={{
                        fontFamily: 'STHeitiSC-Light',
                        textAlign: 'center',
                        fontSize: 17,
                        fontWeight: "600",
                        //fontSize: RFT * 3.3,
                        color: this.state.typeFocusButton == index ? '#6777EF' : '#333333',
                      }}
                      type="clear"
                      onPress={() => {
                        this.GetDailyStaistic(item.day)
                        this.setState({
                          typeFocusButton: index
                        })
                      }}
                    />
                  )
                })
              }
            </View>

            <View style={{ flex: 1 }}>
              <View style={{ flexDirection: "row" }}>
                <ImageBackground
                  imageStyle={styles.ImageBackground_ImageStyle}
                  style={[styles.ImageBackground_Style, { marginLeft: 8 }]}
                  resizeMode='stretch'
                  source={require('./img/1.png')}>
                  <View style={commonStyle.main_cardContentView}>
                    <View style={commonStyle.main_cardContent_textView}>
                      <Text style={commonStyle.main_cardContent_text}>生产量</Text>
                    </View>
                    <Image
                      source={require('./img/shengchanliang.png')}
                      containerStyle={commonStyle.main_cardContent_imageContainerStyle}
                      style={commonStyle.main_cardContent_imageStyle}
                    />
                  </View>
                  <View>
                    <View style={commonStyle.main_cardContentNumView}>
                      <Text style={commonStyle.main_cardContentNum1}>{resDataDayCard.proNumber}</Text>
                      <Text style={commonStyle.main_cardContentNum2}>件</Text>
                    </View>
                  </View>
                </ImageBackground>
                <ImageBackground
                  imageStyle={styles.ImageBackground_ImageStyle}
                  style={[styles.ImageBackground_Style, { left: -11, }]}
                  resizeMode='stretch'
                  source={require('./img/2.png')}>
                  <View style={commonStyle.main_cardContentView}>
                    <View style={commonStyle.main_cardContent_textView}>
                      <Text style={commonStyle.main_cardContent_text}>生产方量</Text>
                    </View>
                    <Image
                      source={require('./img/shengchanfangliang.png')}
                      containerStyle={[commonStyle.main_cardContent_imageContainerStyle, { left: RFT * 16.8 }]}
                      style={commonStyle.main_cardContent_imageStyle}
                    />
                  </View>
                  <View>
                    <View style={commonStyle.main_cardContentNumView}>
                      <Text style={commonStyle.main_cardContentNum1}>{resDataDayCard.proVolume}</Text>
                      <Text style={commonStyle.main_cardContentNum2}>m³</Text>
                    </View>
                  </View>
                </ImageBackground>
              </View>
              <View style={{ flexDirection: "row", top: -10 }}>
                <ImageBackground
                  imageStyle={styles.ImageBackground_ImageStyle}
                  style={[styles.ImageBackground_Style, { marginLeft: 8, }]}
                  resizeMode='stretch'
                  source={require('./img/3.png')}>
                  <View style={commonStyle.main_cardContentView}>
                    <View style={commonStyle.main_cardContent_textView}>
                      <Text style={commonStyle.main_cardContent_text}>发货量</Text>
                    </View>
                    <Image
                      source={require('./img/fahuoliang.png')}
                      containerStyle={commonStyle.main_cardContent_imageContainerStyle}
                      style={commonStyle.main_cardContent_imageStyle}
                    />
                  </View>
                  <View>
                    <View style={commonStyle.main_cardContentNumView}>
                      <Text style={commonStyle.main_cardContentNum1}>{resDataDayCard.outNumber}</Text>
                      <Text style={commonStyle.main_cardContentNum2}>件</Text>
                    </View>
                  </View>
                </ImageBackground>
                <ImageBackground
                  imageStyle={styles.ImageBackground_ImageStyle}
                  style={[styles.ImageBackground_Style, { left: -11, }]}
                  resizeMode='stretch'
                  source={require('./img/4.png')}>
                  <View style={commonStyle.main_cardContentView}>
                    <View style={commonStyle.main_cardContent_textView}>
                      <Text style={commonStyle.main_cardContent_text}>发货方量</Text>
                    </View>
                    <Image
                      source={require('./img/fahuofangliang.png')}
                      containerStyle={[commonStyle.main_cardContent_imageContainerStyle, { left: RFT * 16.8 }]}
                      style={commonStyle.main_cardContent_imageStyle}
                    />
                  </View>
                  <View>
                    <View style={commonStyle.main_cardContentNumView}>
                      <Text style={commonStyle.main_cardContentNum1}>{resDataDayCard.outVolume}</Text>
                      <Text style={commonStyle.main_cardContentNum2}>m³</Text>
                    </View>
                  </View>
                </ImageBackground>
              </View>
            </View>

            <TouchableHighlight
              underlayColor={"lightgray"}
              onPress={() => {
                this.setState({ isHidden1: !this.state.isHidden1 })
              }}
            >
              <View >
                <View style={[commonStyle.row, commonStyle.viewLeft, { marginBottom: 15 }]}>
                  <View style={[commonStyle.mainPurpleColor, commonStyle.titleBadge]}></View>
                  <Text style={[commonStyle.titleText]}>生产情况分析</Text>
                </View>
              </View>
            </TouchableHighlight>

            {
              !this.state.isHidden1 ?
                <View style={commonStyle.main_FuncList}>
                  {
                    card.map((item, index) => {
                      return (
                        <TouchableHighlight
                          underlayColor="lightgray"
                          onPress={() => {
                            this.props.navigation.navigate(item.page, {
                              title: item.title,
                            })
                          }}>
                          <View style={commonStyle.main_FuncItemView}>
                            <Image
                              source={item.img}
                              style={commonStyle.main_FuncItemImageStyle} />
                            <Text style={commonStyle.main_FuncItemText}>{item.title}</Text>
                          </View>
                        </TouchableHighlight>
                      )
                    })
                  }
                </View> : <View></View>
            }

            {
              Url.XMCXauthorgray.indexOf("APP收货确认单") != -1 || Url.XMCXauthorgray.indexOf("APP构件报修") != -1 || Url.XMCXauthorgray.indexOf("APP维修确认") != -1 ?
                <TouchableHighlight
                  underlayColor={"lightgray"}
                  onPress={() => {
                    this.setState({ isHidden2: !this.state.isHidden2 })
                  }}
                >
                  <View >
                    <View style={[commonStyle.row, commonStyle.viewLeft, { marginBottom: 15 }]}>
                      <View style={[commonStyle.mainPurpleColor, commonStyle.titleBadge]}></View>
                      <Text style={[commonStyle.titleText]}>业务操作</Text>
                    </View>
                  </View>
                </TouchableHighlight> : <View></View>
            }

            {
              !this.state.isHidden2 ?
                <View style={commonStyle.main_FuncList}>
                  {
                    operationCard.map((item, index) => {
                      if (Url.XMCXauthorgray.indexOf('APP' + item.title) != -1) {
                        return (
                          <TouchableHighlight
                            underlayColor="lightgray"
                            onPress={() => {
                              this.props.navigation.navigate(item.page, {
                                title: item.title,
                                mainpage: item.mainpage,
                                pagetype: item.pagetype
                              })
                            }}>
                            <View style={commonStyle.main_FuncItemView}>
                              <Image
                                source={item.img}
                                style={commonStyle.main_FuncItemImageStyle} />
                              <Text style={commonStyle.main_FuncItemText}>{item.title}</Text>
                            </View>
                          </TouchableHighlight>
                        )
                      }

                    })
                  }
                </View> : <View></View>
            }


            {/* <TouchableHighlight
              underlayColor={"lightgray"}
              onPress={() => {
                this.setState({ isHidden3: !this.state.isHidden3 })
              }}
            >
              <View >
                <View style={[commonStyle.row, commonStyle.viewLeft, {marginBottom: 15}]}>
                  <View style={[commonStyle.mainPurpleColor, commonStyle.titleBadge]}></View>
                  <Text style={[commonStyle.titleText]}>业务查看</Text>
                </View>
              </View>
            </TouchableHighlight>

            {
              !this.state.isHidden3 ? <View style={commonStyle.main_FuncList}>
                {
                  checkCard.map((item, index) => {
                    return (
                      <TouchableHighlight
                        underlayColor="lightgray"
                        onPress={() => {
                          this.props.navigation.navigate(item.page, {
                            title: item.title,
                            pagetype: item.pagetype,
                            mainpage: item.mainpage
                          })
                        }}>
                        <View style={commonStyle.main_FuncItemView}>
                          <Image
                            source={item.img}
                            style={commonStyle.main_FuncItemImageStyle} />
                          <Text style={commonStyle.main_FuncItemText}>{item.title}</Text>
                        </View>
                      </TouchableHighlight>
                    )
                  })
                }
              </View> : <View></View>
            } */}

          </View>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  ImageBackground_Style: {
    //height: deviceWidth / 2.8,
    // width: deviceWidth / 2.2,
    height: deviceWidth / 3.6,
    //width: deviceWidth / 2,
    paddingRight: 0,
    flex: 1,
    //backgroundColor: 'red'

    // borderRadius: 20,
  },
  ImageBackground_ImageStyle: {
    // height: deviceWidth / 3,
    // width: deviceWidth / 1.8,
    //padding: 0,
    // margin: 0,
    borderRadius: 20,
    //backgroundColor: 'blue'
  }
});