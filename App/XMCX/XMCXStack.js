import React from "react";
import { Image, View, DeviceEventEmitter, StatusBar, Text } from 'react-native';
import { Icon, Badge, Button } from 'react-native-elements'
import { createStackNavigator, createBottomTabNavigator, createAppContainer, createSwitchNavigator, createMaterialTopTabNavigator } from "react-navigation";
import DeviceStorage from "../Url/DeviceStorage";
import MainXMCX from "./main";
import ShengChan from './ChildPage/shengchan';
import KuCun from './ChildPage/kucun';
import FaHuo from './ChildPage/fahuo';
import LouTu from './ChildPage/loutu';
import FenXi from './ChildPage/fenxi';
import HuiZong from './ChildPage/huizong';
import Main_CompReceive from './ChildPage/main_compreceive';
import Main_CompRepair from './ChildPage/main_comprepair';
import Main_CompMaintain from './ChildPage/main_compmaintain';
import Main_CompRepair_Confirm from './ChildPage/main_comprepair_confirm';
import MainCheckPage from './ChildPage/mainCheckPage';
import PDACamera from './Components/PDACamera';
import CameraXMCX from './Components/CameraXMCX';
import ChildOneFlatXMCX from './ChildPage/ChildOneFlatXMCX'
import ChildrenPageTwoXMCX from './ChildPage/ChildrenPageTwo'
import WebXMCX from './ChildPage/Web';
import TouchableCustom from "../PDA/Componment/TouchableCustom";

class LogoTitle extends React.Component {
  constructor() {
    super();
    this.state = {
      ProjectName: '',
    }
  }

  componentDidMount() {
    DeviceStorage.get('ProjectAbb')
      .then(respro => {
        this.setState({
          ProjectName: respro.substring(0, 10)
        })
      }).catch(err => { })
  }

  render() {

    return (
      <View>
        <Text style={{ marginLeft: 10, fontSize: 20, color: 'white', fontWeight: 'bold' }} >{this.state.ProjectName}</Text>
      </View >

      /* <View style={{ flexDirection: 'row', height: 28 }}>
        <Image
          source={require('../../logo.png')}
          style={{ width: 15, height: 26, marginLeft: 15 }}
        />
        <Text style={{ marginLeft: 10, fontSize: 18, color: 'black' }}>构力科技</Text>
      </View> */
    );
  }
}

const MainStack = createStackNavigator({
  MainXMCX: {
    screen: MainXMCX,
    navigationOptions: {
      headerTitle: <LogoTitle />,
    }
  },
  ShengChan: ShengChan,
  KuCun: KuCun,
  FaHuo: FaHuo,
  LouTu: LouTu,
  FenXi: FenXi,
  HuiZong: HuiZong,
  Main_CompReceive: Main_CompReceive,
  Main_CompRepair: Main_CompRepair,
  Main_CompMaintain: Main_CompMaintain,
  Main_CompRepair_Confirm: {
    screen: Main_CompRepair_Confirm,
    // navigationOptions: ({ navigation }) => {
    //   return {
    //     headerRight:
    //       <TouchableCustom onPress={navigation.state.params.PostData()}>
    //         <View style={{ left: -10 }}>
    //           <Button
    //             titleStyle={{ fontSize: 19, color: 'white' }}
    //             title="确认"
    //             buttonStyle={{ color: 'white' }}
    //             icon={<Icon type='antdesign' name='checkcircleo' color='white' size={20} style={{ width: 34 }} />}
    //             iconContainerStyle={{ backgroundColor: 'red', width: 50 }}
    //             type='clear'
    //           />
    //         </View>
    //       </TouchableCustom>
    //   }
    // }
  },
  MainCheckPageXMCX: MainCheckPage,
  ChildOneFlatXMCX: ChildOneFlatXMCX,
  ChildrenPageTwoXMCX: ChildrenPageTwoXMCX,
  CameraXMCX: CameraXMCX,
  WebXMCX: WebXMCX,
  XMCXCamera: PDACamera,
},
  {
    defaultNavigationOptions: {
      headerStyle: {//设置导航条的样式。背景色，宽高等
        backgroundColor: '#656FF6',
        elevation: 0,
        //marginTop: StatusBar.currentHeight
      },
      headerTintColor: 'white',

      headerTitleStyle: {//设置导航条文字样式
        fontWeight: 'normal',
        textAlign: 'center'
      }
    },
    navigationOptions: ({
      navigation
    }) => ({
      tabBarVisible: navigation.state.index > 0 ? false : true,
    }),
  }
)


export default MainStack