import React from 'react'
import { View, Text, Dimensions, StatusBar } from 'react-native'
import { WebView } from 'react-native-webview';
import { ScrollView } from 'react-native-gesture-handler';

const { height, width } = Dimensions.get('window')

export default class webView extends React.Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title'),
    };
  };
  render() {
    let url = this.props.navigation.getParam('url')
    console.log("render -> url", url)
    return (
      <ScrollView
        scrollEnabled={false}
      >
        <StatusBar
          barStyle='default'
          hidden={false}
          translucent = {false}
        />
        <View style={{ height: height * 0.96 }}>
          <WebView
            source={{ uri: url }}
          ></WebView>
        </View>
      </ScrollView>
    )
  }
}