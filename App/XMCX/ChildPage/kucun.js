import React, { Component } from 'react';
import { View, Text, ActivityIndicator, ScrollView } from 'react-native';
import { Card, ListItem, Badge, Icon } from 'react-native-elements';
import commonStyle from '../Components/commonStyle';
import { Echarts, echarts } from 'react-native-secharts';
import { color, deviceWidth, RFT } from '../../Url/Pixal';
import Url from '../../Url/Url';
import { styles } from '../Components/XMCXStyles';

export default class kucun extends Component {
  constructor(props) {
    super(props);
    this.state = {
      chartData: {},
      chartLoading: true,
      CompTypeStock: [],
      CompTypeStockList: []
    };
  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  componentDidMount() {
    this.GetProjectCompTypeStockSaturation()
  }

  GetProjectCompTypeStockSaturation = () => {

    let formData = new FormData();
    let data = {};

    let Name = [],
      Num = [],
      Vol = [],
      count = 0,
      CompTypeStock = [];
    CompTypeStockList = [];

    data = {
      "action": "getprojectcomptypestocksaturation",
      "servicetype": "projectsystem",
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        "factoryId": Url.Fidweb,
        "projectId": Url.ProjectId,
        "isprj": "1"
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: kucun.js ~ line 56 ~ kucun ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: kucun.js ~ line 66 ~ kucun ~ resData", resData)
      if (resData.status == '100') {
        let result = resData.result.data
        result.map((item, index) => {
          item.color = color[count++]
          let tmp = {}, tmpList = {}
          //图表
          tmp.name = item.compTypeName
          tmp.value = item.volume
          Name.push(item.compTypeName);
          Vol.push(item.volume);
          CompTypeStock.push(tmp)

          //表格
          tmpList.color = item.color
          tmpList.name = item.compTypeName
          tmpList.volume = item.volume
          tmpList.ratio = item.ratio
          CompTypeStockList.push(tmpList)
        })

        this.setState({
          CompTypeStock: CompTypeStock,
          CompTypeStockList: CompTypeStockList,
          chartLoading: false
        })
      } else {
        Alert.alert(resData.message)
        this.toast.show(resData.message)
      }
    }).catch((error) => {
      console.log("🚀 ~ file: kucun.js ~ line 97 ~ kucun ~ error", error)
    });
  }

  GetProjectCompTypeStockSaturation_old = () => {
    const url = Url.url + Url.project + Url.Fid + '/' + Url.ProjectId + '/GetProjectCompTypeStockSaturation';
    let Name = [],
      Num = [],
      Vol = [],
      count = 0,
      CompTypeStock = [];
    CompTypeStockList = [];
    fetch(url).then(res => {
      return res.json()
    }).then(resData => {
      let result = resData.result
      console.log("🚀 ~ file: kucun.js ~ line 111 ~ kucun ~ fetch ~ resData", resData)
      resData.map((item, index) => {
        item.color = color[count++]
        let tmp = {}, tmpList = {}
        //图表
        tmp.name = item.compTypeName
        tmp.value = item.volume
        Name.push(item.compTypeName);
        Vol.push(item.volume);
        CompTypeStock.push(tmp)

        //表格
        tmpList.color = item.color
        tmpList.name = item.compTypeName
        tmpList.volume = item.volume
        tmpList.ratio = item.ratio
        CompTypeStockList.push(tmpList)
      })

      this.setState({
        CompTypeStock: CompTypeStock,
        CompTypeStockList: CompTypeStockList,
        chartLoading: false
      })
    }).catch((error) => {
      console.log(error);
    });
  }

  render() {
    const { CompTypeStock, CompTypeStockList, chartLoading } = this.state
    return (
      <View style={{ flex: 1, backgroundColor: "#f9f9f9" }}>
        <ScrollView>
          <View style={commonStyle.blankView}></View>
          <View style={{}}>
            <View style={[commonStyle.row, commonStyle.viewLeft]}>
              <View style={[commonStyle.mainPurpleColor, commonStyle.titleBadge]}></View>
              <Text style={[commonStyle.titleText]}>库存情况</Text>
            </View>
          </View>
          {
            chartLoading ?
              <View style={{ height: 300 }}>
                <View style={styles.loading}>
                  <ActivityIndicator
                    animating={true}
                    color='#656FF6'
                    size="large" />
                </View>
              </View> :
              <Card containerStyle={commonStyle.card_containerStyle}>
                <Echarts
                  //width={deviceWidth * 0.93}
                  height={300}
                  renderLoading={() => {
                    <View style={{
                      flex: 1,
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                      backgroundColor: '#F5FCFF',
                    }}>
                      <ActivityIndicator
                        animating={true}
                        color='#419F'
                        size="large" />
                    </View>
                  }}
                  option={{
                    tooltip: {
                      trigger: 'item',
                      formatter: "{a}: <br/>{b}: {c}m³ ({d}%)",
                      position: 'bottom'
                    },
                    legend: {
                      show: true,
                      bottom: 'bottom',
                      bottom: 3,
                      //type: 'scroll',
                      itemWidth: 14
                    },
                    grid: {
                      left: '10%',
                      right: '10%'
                    },
                    series: [
                      {
                        name: '构件类型',
                        type: 'pie',
                        radius: '50%',
                        colorBy: 'data',
                        minShowLabelAngle: 20,
                        //yAxisIndex: 1,
                        data: CompTypeStock,
                        avoidLabelOverlap: true,
                        top: "top",
                        bottom: "30%",
                        emphasis: {
                          itemStyle: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                          }
                        },
                        label: {
                          formatter: "{b}",
                          color: "#111"
                        },
                        labelLine: {
                          show: true
                        },
                      }
                    ],
                    color: color,

                  }} />
              </Card>
          }
          <View style={commonStyle.blankView}></View>
          <View style={{}}>
            <View style={[commonStyle.row, commonStyle.viewLeft]}>
              <View style={[commonStyle.mainPurpleColor, commonStyle.titleBadge]}></View>
              <Text style={[commonStyle.titleText]}>库存情况统计</Text>
            </View>
          </View>
          <View style={commonStyle.blankView}></View>
          {
            chartLoading ?
              <View style={{ height: 300 }}>
                <View style={styles.loading}>
                  <ActivityIndicator
                    animating={true}
                    color='#656FF6'
                    size="large" />
                </View>
              </View> :
              <View style={commonStyle.View_ListItem}>

                {
                  CompTypeStockList.map((item, index) => {
                    return (
                      <ListItem
                        title={item.name + ':  ' + item.volume + 'm³ ' + '(' + item.ratio.toFixed(3) + '%)'}
                        titleStyle={commonStyle.ListItem_title}
                        bottomDivider={(index + 1) == CompTypeStockList.length ? false : true}
                        leftElement={<Badge badgeStyle={{ backgroundColor: item.color }} />}
                      // chevron
                      />
                    )
                  })
                }
              </View>
          }
          <View style={commonStyle.blankView}></View>
        </ScrollView>
      </View>
    );
  }
}
