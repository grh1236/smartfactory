import React, { Component } from 'react';
import { View, Text, processColor, ActivityIndicator } from 'react-native';
import { Card } from 'react-native-elements';
import commonStyle from '../Components/commonStyle';
import { Echarts, echarts } from 'react-native-secharts';
import { deviceWidth, RFT } from '../../Url/Pixal';
import { styles } from '../Components/XMCXStyles';
import Url from '../../Url/Url';

export default class shengchan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Production: [],
      ProcessAnaysis: [],
      chartData: {},
      chart1Loading: true,
      chart2Loading: true,
    };
  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  componentDidMount() {
    this.GetProjectProduction()
    this.GetFacProjectProcessAnaysis()
  }

  GetProjectCompTypeStockSaturation = () => {
    const url = Url.url + Url.project + Url.Fid + '/' + Url.ProjectId + '/GetProjectProduction';
    console.log("🚀 ~ file: fenxi.js MainXMCXScreen ~ line 99 ~ fenxi ~ url", url)
    let Name = [],
      Num = [],
      Vol = [],
      outVolume = [],
      count = 0,
      Production = {};

    fetch(url).then(res => {
      return res.json()
    }).then(resData => {
      console.log("🚀 ~ file: main.js ~ line 50 ~ MainXMCXScreen ~ fetch ~ resData", resData)
      Num.push(resData.fincnt)
      Num.push(resData.outcnt)
      Num.push(resData.workcnt)
      Num.push(resData.precnt)

      Vol.push(resData.outvol)
      Vol.push(resData.finvol)
      Vol.push(resData.workvol)
      Vol.push(resData.prevol)
      Production.Num = Num
      Production.Vol = Vol
      console.log("🚀 ~ file: fenxi.js ~ line 119 ~ fenxi ~ fetch ~ Production", Production)
      this.setState({ Production: Production, chart1Loading: false })
    }).catch((error) => {
      console.log(error);
    });
  }

  GetProjectProduction = () => {
    //const url = Url.url + Url.project + Url.Fid + Url.ProjectId + '/GetProjectComponentStatistic' + day;

    let formData = new FormData();
    let data = {};

    let Name = [],
      Num = [],
      Vol = [],
      outVolume = [],
      count = 0,
      Production = {};

    data = {
      "action": "getprojectproduction",
      "servicetype": "projectsystem",
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        "factoryId": Url.Fidweb,
        "projectId": Url.ProjectId,
        "isprj": "1"
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: shengchan.js ~ line 84 ~ shengchan ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: shengchan.js ~ line 94 ~ shengchan ~ resData", resData)
      if (resData.status == '100') {
        let result = resData.result
        Num.push(result.fincnt)
        Num.push(result.outcnt)
        Num.push(result.workcnt)
        Num.push(result.precnt)

        Vol.push(result.outvol)
        Vol.push(result.finvol)
        Vol.push(result.workvol)
        Vol.push(result.prevol)
        Production.Num = Num
        Production.Vol = Vol
        console.log("🚀 ~ file: fenxi.js ~ line 119 ~ fenxi ~ fetch ~ Production", Production)
        this.setState({ Production: Production, chart1Loading: false })
      } else {
        Alert.alert(resData.message)
        this.toast.show(resData.message)
      }
    }).catch((error) => {
      console.log("🚀 ~ file: shengchan.js ~ line 105 ~ shengchan ~ error", error)
    });
  }

  GetFacProjectProcessAnaysis = () => {
    //const url = Url.url + Url.project + Url.Fid + Url.ProjectId + '/GetProjectComponentStatistic' + day;

    let formData = new FormData();
    let data = {};

    let Name = [],
      preVolumeArr = [],
      proVolumeArr = [],
      probabilityArr = [],
      outVolume = [],
      count = 0,
      ProcessAnaysis = {};

    data = {
      "action": "getfacprojectprocessanaysis",
      "servicetype": "projectsystem",
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        "factoryId": Url.Fidweb,
        "projectId": Url.ProjectId,
        "isprj": "1"
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: shengchan.js ~ line 152 ~ shengchan ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: shengchan.js ~ line 162 ~ shengchan ~ resData", resData)
      if (resData.status == '100') {
        let result = resData.result.data
        result.map((item, index) => {
          Name.push(item.comptypename)
          preVolumeArr.push(item.prevolume)
          proVolumeArr.push(item.provolume)
          let probability = item.provolume / (item.prevolume == "0.000" ? "1.000" : item.prevolume)
          probability = probability * 100
          probabilityArr.push(probability.toFixed(0))
        })
        ProcessAnaysis.Name = Name
        ProcessAnaysis.preVolume = preVolumeArr
        ProcessAnaysis.proVolume = proVolumeArr
        ProcessAnaysis.probability = probabilityArr
        console.log("🚀 ~ file: shengchan.js ~ line 84 ~ shengchan ~ fetch ~ ProcessAnaysis", ProcessAnaysis)
        this.setState({ ProcessAnaysis: ProcessAnaysis, chart2Loading: false })
      } else {
        Alert.alert(resData.message)
        this.toast.show(resData.message)
      }
    }).catch((error) => {
      console.log("🚀 ~ file: shengchan.js ~ line 105 ~ shengchan ~ error", error)
    });
  }

  GetFacProjectProcessAnaysis_old = () => {
    const url = Url.url + Url.project + Url.Fid + '/' + Url.ProjectId + '/GetFacProjectProcessAnaysis'
    console.log("🚀 ~ file: shengchan.js ~ line 128 ~ shengchan ~ url", url)
    let Name = [],
      preVolumeArr = [],
      proVolumeArr = [],
      probabilityArr = [],
      outVolume = [],
      count = 0,
      ProcessAnaysis = {};
    fetch(url).then(res => {
      return res.json()
    }).then(resData => {
      resData.map((item, index) => {
        Name.push(item.compTypeName)
        preVolumeArr.push(item.preVolume)
        proVolumeArr.push(item.proVolume)
        let probability = item.proVolume / (item.preVolume == 0 ? 1 : item.preVolume)
        probability = probability * 100
        probabilityArr.push(probability.toFixed(0))
      })
      ProcessAnaysis.Name = Name
      ProcessAnaysis.preVolume = preVolumeArr
      ProcessAnaysis.proVolume = proVolumeArr
      ProcessAnaysis.probability = probabilityArr
      console.log("🚀 ~ file: shengchan.js ~ line 84 ~ shengchan ~ fetch ~ ProcessAnaysis", ProcessAnaysis)
      this.setState({ ProcessAnaysis: ProcessAnaysis, chart2Loading: false })
    }).catch((error) => {
      console.log(error);
    });
  }

  render() {
    const { Production, ProcessAnaysis, chart1Loading, chart2Loading } = this.state
    return (
      <View style={{ flex: 1, backgroundColor: "#f9f9f9" }}>
        <View style={commonStyle.blankView}></View>

        <View >
          <View style={[commonStyle.row, commonStyle.viewLeft]}>
            <View style={[commonStyle.mainPurpleColor, commonStyle.titleBadge]}></View>
            <Text style={[commonStyle.titleText]}>项目生产情况</Text>
          </View>
        </View>
        {
          chart1Loading ? <View style={styles.loading}>
            <ActivityIndicator
              animating={true}
              color='#656FF6'
              size="large" />
          </View> :
            <View>
              <Card containerStyle={commonStyle.card_containerStyle}>
                <Echarts
                  height={200}
                  renderLoading={() => {
                    <View style={{
                      flex: 1,
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                      backgroundColor: '#F5FCFF',
                    }}>
                      <ActivityIndicator
                        animating={true}
                        color='#419F'
                        size="large" />
                    </View>
                  }}
                  option={{
                    tooltip: {
                      trigger: 'axis',
                      axisPointer: {
                        crossStyle: {
                          color: '#999'
                        }
                      }
                    },
                    legend: {
                      data: ['已发货', '待发货', '生产中', '未生产'],
                      left: 'left'
                    },
                    xAxis: [
                      {
                        show: true,
                        type: 'value',
                        label: {
                          show: true,
                          backgroundColor: '#004E52'
                        },
                        axisPointer: {
                          snap: false,
                          type: 'shadow',
                          handle: {
                            show: false,
                            color: '#004E52'
                          },
                        },
                        axisTick: {
                          show: false
                        },
                        axisLine: {
                          show: false
                        },
                        splitLine: {
                          show: false
                        },
                        splitArea: {
                          show: true,
                          interval: 1,
                          areaStyle: {
                            color: ['white', '#f9f9f9']
                          }
                        },
                      }
                    ],
                    yAxis: [
                      {
                        show: true,
                        type: 'category',
                        data: ['已发货', '待发货', '生产中', '未生产',],
                        //interval: 1,
                        axisLabel: {
                          fontSize: 12,
                          color: 'gray'
                        },
                        axisTick: {
                          show: false
                        },
                        axisLine: {
                          lineStyle: {
                            color: 'lightgray'
                          }
                        }
                      },
                    ],
                    grid: {
                      left: '15%',
                      right: '8%',
                      top: '10%',
                      bottom: '20%',
                      show: false
                    },

                    series: [
                      /* {
                        name: '数量',
                        type: 'bar',
                        data: chartData.Num,
                        //barMaxWidth: chartData.Num.length <= 6 && '28%'
                        barMaxWidth: chartData.Num.length > 13 && chartData.Num.length < 20 ? chartData.Num.length - 5 : ((chartData.Num.length < 6 ? chartData.Num.length + 10 : chartData.Num.length + 5))
                      }, */
                      {
                        name: '方量',
                        type: 'bar',
                        //yAxisIndex: 1,
                        data: this.state.Production.Vol,
                        barMaxWidth: 20,
                        itemStyle: {
                          normal: {
                            //每根柱子颜色设置
                            color: function (params) {
                              let color = [
                                '#f1c40f',
                                '#f39c12',
                                '#41CCFF',
                                '#2ecc71',
                                '#9b59b6',
                                '#449BCE',
                                '#e67e22',
                                '#d35400',
                                '#e74c3c',
                                '#27ae60'
                              ];
                              const colorList =
                                [
                                  '#EE5A24',
                                  '#009432',
                                  '#FFCF18',
                                  '#7158e2',
                                  '#17c0eb',
                                  '#ffb8b8',
                                  '#c56cf0',
                                  '#ff3838',
                                  '#C4E538',
                                  '#FFC312',
                                  '#67e6dc',
                                  '#ED4C67',
                                  '#FDA7DF',
                                  '#0652DD',
                                  '#833471',
                                  '#12CBC4',
                                  '#D980FA',
                                  '#ff9f1a',
                                  '#1289A7',
                                  '#A3CB38',
                                  '#F79F1F',
                                  '#9980FA',
                                  '#B53471',
                                  '#EE5A24',
                                  '#009432',
                                  '#fff200',
                                  '#7158e2',
                                  '#ff9f1a',
                                  '#17c0eb',
                                  '#ff3838',
                                  '#ffb8b8',
                                  '#c56cf0',
                                  '#C4E538',
                                  '#FFC312',
                                  '#67e6dc',
                                  '#ED4C67',
                                  '#FDA7DF',
                                  '#0652DD',
                                  '#833471',
                                  '#12CBC4',
                                  '#D980FA',
                                  '#1289A7',
                                  '#A3CB38',
                                  '#F79F1F',
                                  '#9980FA',
                                  '#B53471',
                                  '#EE5A24',
                                  '#009432',
                                  '#fff200',
                                  '#7158e2',
                                  '#ff9f1a',
                                  '#17c0eb',
                                  '#ff3838',
                                  '#ffb8b8',
                                  '#c56cf0',
                                  '#C4E538',
                                  '#FFC312',
                                  '#67e6dc',
                                  '#ED4C67',
                                  '#FDA7DF',
                                  '#0652DD',
                                  '#833471',
                                  '#12CBC4',
                                  '#D980FA',
                                  '#1289A7',
                                  '#A3CB38',
                                  '#F79F1F',
                                  '#9980FA',
                                  '#B53471',
                                ]
                              return colorList[params.dataIndex];
                            }
                          }
                        }
                      }
                    ],
                    //color: ['#2ecc71', '#f1c40f'],

                  }} />
              </Card>
            </View>
        }


        <View style={commonStyle.blankView}></View>

        <View >
          <View style={[commonStyle.row, commonStyle.viewLeft]}>
            <View style={[commonStyle.mainPurpleColor, commonStyle.titleBadge]}></View>
            <Text style={[commonStyle.titleText]}>生产情况分析</Text>
          </View>
        </View>
        {
          chart2Loading ? <View style={styles.loading}>
            <ActivityIndicator
              animating={true}
              color='#656FF6'
              size="large" />
          </View> : <View>
            <Card containerStyle={commonStyle.card_containerStyle}>
              <Echarts
                //width={deviceWidth * 0.93}
                height={300}
                renderLoading={() => {
                  <View style={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: '#F5FCFF',
                  }}>
                    <ActivityIndicator
                      animating={true}
                      color='#419F'
                      size="large" />
                  </View>
                }}
                option={{
                  tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                      crossStyle: {
                        color: '#999'
                      }
                    }
                  },

                  legend: {
                    data: ['预生产量', '生产量', '生产率'],
                    //itemWidth: 12,
                    //itemHeight: 16,
                    //padding: [10, 5, 5, 5],
                    textStyle: {
                      //fontSize: 13,
                      //lineHeight: 15
                    },
                    itemGap: 20,
                    show: true,
                    top: '5%',
                  },
                  xAxis: [
                    {
                      show: true,
                      type: 'category',
                      data: ProcessAnaysis.Name,
                      label: {
                        show: false,
                        backgroundColor: '#004E52'
                      },
                      axisLine: {

                      },
                      axisPointer: {
                        snap: false,
                        type: 'shadow',
                        handle: {
                          show: false,
                          color: '#004E52'
                        },
                      }
                    }
                  ],
                  yAxis: [
                    {
                      show: true,
                      type: 'value',
                      name: 'm³',
                      min: 0,
                      // interval: 1,
                      axisLabel: {
                        formatter: (value) => {
                          if (value >= 1000 && value < 100000) {
                            value = value / 1000 + "k";
                          }
                          return value;
                        }
                      },
                      splitLine: {
                        show: false
                      },
                      splitArea: {
                        show: true,
                        interval: 1,
                        areaStyle: {
                          color: ['white', '#f9f9f9']
                        }
                      },
                    }, {
                      show: true,
                      type: 'value',
                      name: '生产率：%',
                      axisLabel: {
                        formatter: '{value}%' // 格式化标签
                      },
                      min: 0,
                      //max: 100,
                      // interval: 1,
                      splitLine: {
                        show: false
                      }
                    }
                  ],
                  grid: {
                    top: '30%',
                    left: '10%',
                    right: '10%',
                    bottom: '15%'
                  },

                  series: [
                    /* {
                      name: '数量',
                      type: 'bar',
                      data: chartData.Num,
                      //barMaxWidth: chartData.Num.length <= 6 && '28%'
                      barMaxWidth: chartData.Num.length > 13 && chartData.Num.length < 20 ? chartData.Num.length - 5 : ((chartData.Num.length < 6 ? chartData.Num.length + 10 : chartData.Num.length + 5))
                    }, */
                    {
                      name: '预生产量',
                      type: 'bar',
                      yAxisIndex: 0,
                      data: ProcessAnaysis.preVolume,
                      barMaxWidth: 10,
                      itemStyle: {
                        color: '#5765FF'
                      }
                    },
                    {
                      name: '生产量',
                      type: 'bar',
                      yAxisIndex: 0,
                      data: ProcessAnaysis.proVolume,
                      barMaxWidth: 10,
                      itemStyle: {
                        color: '#22D190'
                      }
                    },
                    {
                      name: '生产率',
                      type: 'line',
                      symbol: 'none',
                      yAxisIndex: 1,
                      data: ProcessAnaysis.probability,
                      barMaxWidth: 10,
                      itemStyle: {
                        color: '#F88932'
                      }
                    }
                  ],
                  //color: ['#2ecc71', '#f1c40f'],

                }} />
            </Card>
          </View>
        }


      </View >
    );
  }
}
