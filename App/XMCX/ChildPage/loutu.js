import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableWithoutFeedback } from 'react-native';
import { Card } from 'react-native-elements';
import commonStyle from '../Components/commonStyle';
import { Echarts, echarts } from 'react-native-secharts';
import { color, deviceWidth, RFT, deviceHeight } from '../../Url/Pixal';
import Url from '../../Url/Url';
import WebView from 'react-native-webview';
import { ScrollView } from 'react-navigation';

export default class loutu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      UrlProjectH5Chart: '',
      UrlProjectH5List: '',
      enableScrollViewScroll: false,
    };
  }
  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  componentDidMount() {
    this.setState({
      UrlProjectH5Chart: Url.urlsys + '/assets/projectH5/projectSectionOne.html?isapp=1&isprj=1&' + 'factoryid=' + Url.Fid + '&' + 'projectid=' + Url.ProjectId,
      UrlProjectH5List: Url.urlsys + '/assets/projectH5/projectSectionTwo.html?isapp=1&isprj=1&' + 'factoryid=' + Url.Fid + '&' + 'projectid=' + Url.ProjectId,
    }, () => {
      console.log("🚀 ~ file: loutu.js ~ line 30 ~ loutu ~ componentDidMount ~ UrlProjectH5Chart", this.state.UrlProjectH5Chart)
      console.log("🚀 ~ file: loutu.js ~ line 31 ~ loutu ~ componentDidMount ~ UrlProjectH5List", this.state.UrlProjectH5List)
    })
  }

  render() {
    return (
      <View>
        <ScrollView overScrollMode='never' scrollEnabled={this.state.enableScrollViewScroll}>
          <Text style={styles.text}>形象进度图</Text>
          <View style={{ width: deviceWidth * 0.94, marginLeft: deviceWidth * 0.03, height: deviceHeight * 1.3, backgroundColor: '#FFFFFF', borderRadius: 10 }}>
            <View style={{ height: deviceHeight * 0.7, width: deviceWidth * 0.94 }}>
              <TouchableWithoutFeedback
                onPressIn={() => {
                  this.setState({ enableScrollViewScroll: false })
                }}
                onPressOut={() => {
                  this.setState({ enableScrollViewScroll: true })
                }}
                onLongPress={() => {
                  return true
                }}>
                <WebView
                  source={{ uri: this.state.UrlProjectH5Chart }}
                  onMessage={(e) => {
                    console.log("🚀 ~ file: Project.js ~ line 514 ~ Project_Screen ~ render ~ e.nativeEvent", e.nativeEvent)
                    console.log("🚀 ~ file: Project.js ~ line 514 ~ Project_Screen ~ render ~ e.nativeEvent.data", e.nativeEvent.data)
                    let dataArr = e.nativeEvent.data.split('&')
                    console.log("🚀 ~ file: Project.js:576 ~ Project_Screen ~ render ~ dataArr:", dataArr)
                    let project = dataArr[1].split('=')
                    let projectId = project[1]
                    let floorNoIdArr = dataArr[2].split('=')
                    let floorNoId = floorNoIdArr[1]
                    let floorIdArr = dataArr[3].split('=')
                    let floorId = floorIdArr[1]
                    let compTypeIdArr = dataArr[4].split('=')
                    let compTypeId = compTypeIdArr[1]
                    let compstateArr = dataArr[5].split('=')
                    let compState = compstateArr[1]
                    let floorNoNameArr = dataArr[6].split('=')
                    let floorNoName = floorNoNameArr[1]
                    let floorNameArr = dataArr[8].split('=')
                    let floorName = floorNameArr[1]
                    let compTypeNameArr = dataArr[7].split('=')
                    let compTypeName = compTypeNameArr[1]

                    this.props.navigation.navigate('ChildOneFlatXMCX', {
                      title: '构件明细',
                      sonTitle: "构件详情",
                      url: Url.url + Url.Produce + 'PostProduceCompDetail',
                      sonUrl: '',
                      projectId: projectId,
                      floorNoId: floorNoId,
                      floorNoName: floorNoName,
                      floorId: floorId,
                      floorName: floorName,
                      compTypeId: compTypeId,
                      compTypeName: compTypeName,
                      compState: compState
                    })
                  }}
                ></WebView>
              </TouchableWithoutFeedback>
            </View>
            <View style={{ height: deviceHeight * 0.6, width: deviceWidth * 0.94 }}>
              <WebView
                source={{ uri: this.state.UrlProjectH5List }}
              ></WebView>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  text: {
    fontSize: RFT * 4,
    marginTop: 10,
    marginBottom: 10,
    color: '#535c68',
    marginLeft: deviceWidth * 0.04
  },
})
