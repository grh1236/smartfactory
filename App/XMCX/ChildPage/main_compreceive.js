
import React, { Component } from 'react';
import { View, TouchableOpacity, TouchableHighlight, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, Input, Icon, Badge, Card, ListItem, BottomSheet, CheckBox, Avatar, Overlay, Header } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { deviceWidth, RFT } from '../../Url/Pixal';
import { ScrollView } from 'react-native-gesture-handler';
import CardList from "../../PDA/Componment/CardList";
import DatePicker from 'react-native-datepicker'
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { Image } from 'react-native';
import { NavigationEvents } from 'react-navigation';
import { styles } from '../Components/XMCXStyles';
import ScanButton from '../Components/ScanButton';
import CameraButton from '../Components/CameraButton';
import FormButton from '../../PDA/Componment/formButton';
import ListItemScan from '../Components/ListItemScan';
import ListItemScan_child from '../../PDA/Componment/ListItemScan_child';
import IconDown from '../Components/IconDown';
import BottomItem from '../../PDA/Componment/BottomItem';
import TouchableCustom from '../../PDA/Componment/TouchableCustom';
import CheckBoxScan from '../Components/CheckBoxScan';
import AvatarAdd from '../../PDA/Componment/AvatarAdd';
import AvatarImg from '../Components/AvatarImg';
import DeviceStorage from '../../Url/DeviceStorage';
import Pdf from 'react-native-pdf';
import navigation from '../../Url/navigation';

let imageArr = [], guid = '', subsSelect = []

let compDataArr = [], compIdArr = [], tmpstr = '', visibleArr = [], problemDescriptionArr = []

let pictureSelectArr = []

let deliveryNum = 0, deliveryVol = 0,
  confirmNum = 0, confirmVol = 0,
  noConfirmNum = 0, noConfirmVol = 0

export default class miain_compreceive extends Component {
  constructor(props) {
    super(props);
    this.state = {
      resData: [],
      main: [],
      //构件字表
      subs: [],
      subselecttext: '请选择构件',
      subsSelect: [],
      compDataArr: [],
      compIdArr: [],
      hidden: -1,
      isVisible: false,
      visibleArr: [],
      productCode: '',
      formCode_CK: "",
      //数量
      deliveryNum: 0,
      deliveryVol: 0,
      confirmNum: 0,
      confirmVol: 0,
      noConfirmNum: 0,
      noConfirmVol: 0,
      ServerTime: "",
      receiveEmployeeName: "",
      remark: "",
      problemDescription: '',
      problemDescriptionArr: [],
      isSubmit: 0,
      //照片
      imageArr: [],
      pictureSelect: false,
      pictureSelectArr: [],
      imageOverSize: false,
      pictureUri: '',

      isLoading: true,
      focusIndex: '',
      bottomData: [],
      bottomVisible: false,
      isCheckPage: false,
      isSubsVisible: false
    };
  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  componentDidMount() {
    const { navigation } = this.props;
    const QRid = navigation.getParam('QRid')
    guid = navigation.getParam('guid') || ''
    console.log("🚀 ~ file: miain_compreceive.js ~ line 40 ~ miain_compreceive ~ componentDidMount ~ guid", guid)
    let pageType = navigation.getParam('pageType') || ''

    console.log("🚀 ~ file: main_compreceive.js ~ line 97 ~ miain_compreceive ~ componentDidMount ~ QRid", QRid)
    if (QRid) {
      let type = navigation.getParam('type') || '';
      if (type == 'outStoGuid') {
        this.GetStoID(QRid)
        guid = QRid
        return
      }
      if (type == 'compid') {
        this.GetcomID(QRid)
        return
      }
    }

    if (pageType == 'check') {
      this.checkResData(guid)
      this.setState({ isCheckPage: true })
    } else {
      this.resData()
    }

  }

  UpdateControl = () => {
    const { navigation } = this.props;
    const QRid = navigation.getParam('QRid') || ''
    console.log("🚀 ~ file: main_compreceive.js ~ line 107 ~ miain_compreceive UpdateControl ~ QRid", QRid)
    let type = navigation.getParam('type') || '';
    console.log("🚀 ~ file: main_compreceive.js ~ line 108 ~ miain_compreceive UpdateControl ~ type", type)
    if (type == 'outStoGuid') {
      this.GetStoID(QRid)
    }
    if (type == 'compid') {
      this.GetcomID(QRid)
    }
  }

  componentWillUnmount() {
    imageArr = [], subsSelect = []

    compDataArr = [], compIdArr = [], tmpstr = '', visibleArr = [], problemDescriptionArr = []

    pictureSelectArr = []

    deliveryNum = 0, deliveryVol = 0
    confirmNum = 0, confirmVol = 0
    noConfirmNum = 0, noConfirmVol = 0
  }

  checkResData = (guid) => {
    let url = 'http://10.100.132.129:9100/pcservice/v1/PDAService.ashx'
    let formData = new FormData();
    let data = {};
    data = {
      "action": "initcompconfirmqueryinfo",
      "servicetype": "projectsystem",
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        "rowGuid": guid
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: main_compreceive.js ~ line 120 ~ miain_compreceive ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: main_compreceive.js ~ line 132 ~ miain_compreceive ~ resData", resData)
      let resultcheck = resData.result

      if (resData.status == 100) {
        let main = resultcheck.main
        let subs = resultcheck.subs
        let formCode_CK = main.formCode_CK
        deliveryNum = main.deliveryNum
        deliveryVol = main.deliveryVol
        confirmNum = main.confirmNum
        confirmVol = main.confirmVol
        noConfirmNum = main.noConfirmNum
        noConfirmVol = main.noConfirmVol
        let receiveDate = main.receiveDate
        let isSubmit = main.isSubmit
        let remark = main.remark

        subs.map((item, index) => {
          if (item.confirmResult == '未确认') {
            compIdArr.push(item.compId)
            compDataArr.push(item)
          }
        })

        this.setState({
          resData: resData,
          main: main,
          subs: subs,
          formCode_CK: formCode_CK,
          deliveryNum: deliveryNum,
          deliveryVol: deliveryVol,
          confirmNum: confirmNum,
          confirmVol: confirmVol,
          noConfirmNum: noConfirmNum,
          noConfirmVol: noConfirmVol,
          ServerTime: receiveDate,
          remark: remark,
          isSubmit: isSubmit,
          compDataArr: compDataArr,
          compIdArr: compIdArr,
          pictureSelect: true,
          isEquipVisible: true,
          isEquipCareContentVisible: true,
          isGetcomID: true,
        })
      } else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }


      this.setState({
        isLoading: false,
      })
    }).catch((error) => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 215 ~ QualityInspection ~ error", error)
    });
  }

  resData = () => {
    let url = 'http://10.100.132.129:9100/pcservice/v1/PDAService.ashx'
    let formData = new FormData();
    let data = {};
    data = {
      "action": "initoutstorageinfo",
      "servicetype": "projectsystem",
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        "rowGuid": "",
        "outStoGuid": guid,
        "userName": Url.username,
        "type": "add"
      }
    }
    console.log("🚀 ~ file: miain_compreceive.js ~ line 58 ~ miain_compreceive ~ data", data)
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: miain_compreceive.js ~ line 69 ~ miain_compreceive ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: miain_compreceive.js ~ line 77 ~ miain_compreceive ~ resData", resData)
      let result = resData.result
      //初始化错误提示
      if (resData.status == 100) {
        let main = result.main
        console.log("🚀 ~ file: main_compreceive.js ~ line 142 ~ miain_compreceive ~ main", main)
        let subs = result.subs
        let formCode_CK = main.formCode_CK
        deliveryNum = main.deliveryNum
        deliveryVol = main.deliveryVol
        confirmNum = main.confirmNum
        confirmVol = main.confirmVol
        noConfirmNum = main.noConfirmNum
        noConfirmVol = main.noConfirmVol
        let receiveDate = main.receiveDate
        let receiveEmployeeName = main.receiveEmployeeName
        let remark = main.remark

        this.setState({
          resData: resData,
          main: main,
          subs: subs,
          formCode_CK: formCode_CK,
          deliveryNum: deliveryNum,
          deliveryVol: deliveryVol,
          confirmNum: confirmNum,
          confirmVol: confirmVol,
          noConfirmNum: noConfirmNum,
          noConfirmVol: noConfirmVol,
          ServerTime: receiveDate,
          receiveEmployeeName: receiveEmployeeName,
          remark: remark,
          isLoading: false
        })
      }
      else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }

    }).catch((error) => {
    });

  }

  PostData = () => {


    const { formCode_CK, main, subs, confirmNum, confirmVol, compDataArr, noConfirmNum, noConfirmVol, ServerTime, remark, imageArr, problemDescriptionArr } = this.state
    console.log("🚀 ~ file: main_compreceive.js ~ line 190 ~ miain_compreceive ~ main", main)

    console.log("🚀 ~ file: miain_compreceive.js ~ line 182 ~ miain_compreceive ~ imageArr", imageArr)

    let url = 'http://10.100.132.129:9100/pcservice/v1/PDAService.ashx'

    subs.map((item) => {
      problemDescriptionArr.map((itempro) => {
        if (item.compId == itempro.compId) {
          item.problemDescription = itempro.descript
        }
      })
    })

    main.confirmNum = confirmNum
    main.confirmVol = confirmVol
    main.deliveryNum = deliveryNum
    main.deliveryVol = deliveryVol
    main.noConfirmNum = noConfirmNum
    main.noConfirmVol = noConfirmVol
    main.remark = remark

    if (noConfirmNum == 0) {
      this.setState({
        confirmNum: deliveryNum,
        confirmVol: deliveryVol
      })
      main.confirmNum = deliveryNum
      main.confirmVol = deliveryVol
    }

    let formData = new FormData();
    let data = {};
    data = {
      "action": "savecompconfirm",
      "servicetype": "projectsystem",
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        "type": "add",
        "userName": Url.username,
        "main": main,
        "subs": subs
      }
    }
    console.log("🚀 ~ file: main_compreceive.js ~ line 186 ~ miain_compreceive ~ data", data)
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: miain_compreceive.js ~ line 220 ~ miain_compreceive ~ formData", formData)
    imageArr.map((item, index) => {
      formData.append(item.name, {
        uri: item.value,
        type: 'image/jpeg',
        name: item.name
      })
    })

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('保存成功');
        this.props.navigation.goBack()
      } else {
        Alert.alert('保存失败', resData.message)
        console.log('resData', resData)
      }
    }).catch((error) => {
      console.log("🚀 ~ file: main_compreceive.js ~ line 388 ~ miain_compreceive ~ error", error)
    });

  }

  DeleteData = () => {
    let url = 'http://10.100.132.129:9100/pcservice/v1/PDAService.ashx'

    this.toast.show('删除中', 3000)

    let formData = new FormData();
    let data = {};
    data = {
      "action": "deletecompconfirm",
      "servicetype": "projectsystem",
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        "rowGuid": guid
      }
    }

    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: main_compreceive.js ~ line 358 ~ miain_compreceive ~ DeleteData ~ formData", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('删除成功');
        this.props.navigation.navigate("MainCheckPageXMCX")
      } else {
        Alert.alert('删除失败', resData.message)
      }
    }).catch((error) => {
    });
  }

  GetStoID = (guid) => {
    let url = 'http://10.100.132.129:9100/pcservice/v1/PDAService.ashx'
    let formData = new FormData();
    let data = {};
    data = {
      "action": "initoutstorageinfo",
      "servicetype": "projectsystem",
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        "rowGuid": "",
        "outStoGuid": guid,
        "userName": Url.username,
        "type": "add"
      }
    }
    console.log("🚀 ~ file: miain_compreceive.js ~ line 58 ~ miain_compreceive ~ data", data)
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: miain_compreceive.js ~ line 69 ~ miain_compreceive ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: miain_compreceive.js ~ line 77 ~ miain_compreceive ~ resData", resData)
      let result = resData.result
      //初始化错误提示
      if (resData.status == 100) {
        let main = result.main
        console.log("🚀 ~ file: main_compreceive.js ~ line 142 ~ miain_compreceive ~ main", main)
        let subs = result.subs
        let formCode_CK = main.formCode_CK
        deliveryNum = main.deliveryNum
        deliveryVol = main.deliveryVol
        confirmNum = main.confirmNum
        confirmVol = main.confirmVol
        noConfirmNum = main.noConfirmNum
        noConfirmVol = main.noConfirmVol
        let receiveDate = main.receiveDate
        let receiveEmployeeName = main.receiveEmployeeName
        let remark = main.remark

        this.setState({
          resData: resData,
          main: main,
          subs: subs,
          formCode_CK: formCode_CK,
          deliveryNum: deliveryNum,
          deliveryVol: deliveryVol,
          confirmNum: confirmNum,
          confirmVol: confirmVol,
          noConfirmNum: noConfirmNum,
          noConfirmVol: noConfirmVol,
          ServerTime: receiveDate,
          receiveEmployeeName: receiveEmployeeName,
          remark: remark,
          isLoading: false
        })
      }
      else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }

    }).catch((error) => {
    });
  }

  GetcomID = (id) => {
    console.log("🚀 ~ file: main_compreceive.js ~ line 502 ~ miain_compreceive ~ id", id)
    let formData = new FormData();
    let data = {};
    data = {
      "action": "initdefectcompinfo",
      "servicetype": "projectsystem",
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        "outStoGuid": guid,
        "compId": id,
        "factoryId": Url.Fidweb,
        "projectId": Url.ProjectId
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: main_compreceive.js ~ line 517 ~ miain_compreceive ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: main_compreceive.js ~ line 527 ~ miain_compreceive ~ resData", resData)
      if (resData.status == '100') {
        let compData = {}
        this.state.subs.map((item) => {
          if (resData.result._Rowguid == item.compId) {
            compData = item
          }
        })
        let compId = compData.compId
        let compCode = compData.compCode
        if (compDataArr.length == 0) {
          compData.confirmResult = '未确认'
          subsSelect.push(compData)
          compDataArr.push(compData)
          compIdArr.push(compId)
          this.setState({
            subsSelect: subsSelect,
            compDataArr: compDataArr,
            compIdArr: compIdArr,
            productCode: compCode
          })
          tmpstr = tmpstr + compId + ','
        } else {
          if (tmpstr.indexOf(compId) == -1) {
            subsSelect.push(compData)
            compIdArr.push(compId)
            compDataArr.push(compData)
            compData.confirmResult = '未确认'
            this.setState({
              subsSelect: subsSelect,
              compDataArr: compDataArr,
              compIdArr: compIdArr,
              productCode: compCode
            })
            tmpstr = tmpstr + compId + ','
          } else {
            this.toast.show('已经扫瞄过此构件')
          }
        }
        noConfirmNum = compDataArr.length
        noConfirmVol += compData.compVolume
        confirmNum = deliveryNum - noConfirmNum
        confirmVol = deliveryVol - noConfirmVol
        this.setState({
          confirmNum: confirmNum,
          confirmVol: confirmVol,
          noConfirmNum: noConfirmNum,
          noConfirmVol: noConfirmVol
        }, () => {
          this.forceUpdate()

        })
      } else {
        this.toast.show(resData.message)
      }
    }).catch(err => {
      console.log("🚀 ~ file: main_compreceive.js ~ line 568 ~ miain_compreceive ~ err", err)
    })
  }

  _DatePicker = () => {
    const { ServerTime } = this.state
    return (
      <DatePicker
        customStyles={{
          dateInput: styles.dateInput,
          dateTouchBody: styles.dateTouchBody,
          dateText: styles.dateText,
        }}
        iconComponent={<Icon name='caretdown' color='#656FF6' iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
        }
        showIcon={true}
        mode='datetime'
        date={ServerTime}
        format="YYYY-MM-DD HH:mm"
        onOpenModal={() => {
          this.setState({
            focusIndex: '4'
          })
        }}
        onDateChange={(value) => {
          this.setState({
            ServerTime: value
          })
        }}
      />
    )
  }

  isBottomVisible = (data, focusIndex) => {
    if (typeof (data) != "undefined") {
      if (data.length > 0) {
        this.setState({ bottomVisible: true, bottomData: data, focusIndex: focusIndex })
      }
    } else {
      this.toast.show("无数据")
    }
  }

  cameraFunc = (compId) => {
    launchCamera(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: parseInt(Url.isResizePhoto),
        maxWidth: parseInt(Url.isResizePhoto),
        saveToPhotos: true,

      },
      (response) => {
        if (response.uri == undefined) {
          return
        }
        let tmp = {}
        tmp.name = compId
        tmp.value = response.uri
        imageArr.push(tmp)
        this.setState({
          pictureSelect: true,
          imageArr: imageArr
        })
        pictureSelectArr.push(compId)
        this.setState({
          pictureSelectArr: pictureSelectArr
        })
        console.log(response)
      },
    )
  }

  camLibraryFunc = (compId) => {
    launchImageLibrary(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: parseInt(Url.isResizePhoto),
        maxWidth: parseInt(Url.isResizePhoto),
      },
      (response) => {
        if (response.uri == undefined) {
          return
        }
        let tmp = {}
        tmp.name = compId
        tmp.value = response.uri
        imageArr.push(tmp)
        this.setState({
          pictureSelect: true,
          imageArr: imageArr
        })
        pictureSelectArr.push(compId)
        this.setState({
          pictureSelectArr: pictureSelectArr
        })
        console.log(response)
      },
    )
  }

  camandlibFunc = (compId) => {
    Alert.alert(
      '提示信息',
      '选择拍照方式',
      [{
        text: '取消',
        style: 'cancel'
      }, {
        text: '拍照',
        onPress: () => {
          this.cameraFunc(compId)
        }
      }, {
        text: '相册',
        onPress: () => {
          this.camLibraryFunc(compId)
        }
      }])
  }

  imageView = (compId) => {
    return (
      <View style={{ flexDirection: 'row' }}>
        {
          // this.state.pictureSelect ?
          this.state.imageArr.map((item, index) => {
            if (item.name == compId) {
              return (
                <AvatarImg
                  item={item.value}
                  index={index}
                  onPress={() => {
                    this.setState({
                      imageOverSize: true,
                      pictureUri: item.value
                    })
                  }}
                  onLongPress={() => {
                    Alert.alert(
                      '提示信息',
                      '是否删除构件照片',
                      [{
                        text: '取消',
                        style: 'cancel'
                      }, {
                        text: '删除',
                        onPress: () => {


                          console.log("🚀 ~ file: miain_compreceive.js ~ line 375 ~ miain_compreceive ~ this.state.imageArr.map ~ imageArr", imageArr)
                          let p = pictureSelectArr.indexOf(item.value)
                          imageArr.splice(p, 1)
                          pictureSelectArr.splice(p, 1)
                          console.log("🚀 ~ file: miain_compreceive.js ~ line 375 ~ miain_compreceive ~ this.state.imageArr.map ~ pictureSelectArr", pictureSelectArr)
                          this.setState({
                            imageArr: imageArr,
                            pictureSelectArr: pictureSelectArr
                          }, () => {
                            this.forceUpdate()
                          })
                          // if (i > -1) {

                          // }
                          if (imageArr.length == 0) {
                            this.setState({
                              pictureSelect: false
                            })
                          }
                          console.log("🚀 ~ file: qualityinspection.js ~ line 639 ~ SteelCage ~ this.state.imageArr.map ~ imageArr", imageArr)
                        }
                      }])
                  }} />
              )
            }
            //if (index == 0) {

          })
          // : <View></View>
        }
      </View>
    )
  }

  cardListContont = comp => {
    if (visibleArr.indexOf(comp.compCode) == -1) {
      visibleArr.push(comp.compCode);
      this.setState({ visibleArr: visibleArr });
    } else {
      if (visibleArr.length == 1) {
        visibleArr.splice(0, 1);
        this.setState({ visibleArr: visibleArr }, () => {
          this.forceUpdate;
        });
      } else {
        visibleArr.map((item, index) => {
          if (item == comp.compCode) {
            visibleArr.splice(index, 1);
            this.setState({ visibleArr: visibleArr }, () => {
              this.forceUpdate;
            });
          }
        });
      }
    }
  };

  CardList_old = (compData) => {

    return (
      compData.map((item, index) => {
        let comp = item
        console.log("🚀 ~ file: main_compreceive.js ~ line 463 ~ miain_compreceive ~ compData.map ~ comp", comp)
        return (
          <View style={styles.CardList_main}>
            <TouchableCustom
              onPress={() => {
                this.cardListContont(comp);
              }}
              onLongPress={() => {
                Alert.alert(
                  '提示信息',
                  '是否删除构件信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      compDataArr.splice(index, 1)
                      compIdArr.splice(index, 1)
                      subsSelect.splice(index, 1)
                      tmpstr = compIdArr.toString()
                      console.log("🚀 ~ file: return_goods.js ~ line 559 ~ ReturnGoods ~ compData.map ~ tmpstr", tmpstr)
                      this.setState({
                        compDataArr: compDataArr,
                        compIdArr: compIdArr,
                        subsSelect: subsSelect
                      })
                    }
                  }])
              }}>
              <View style={styles.CardList_title_main}>
                <View style={styles.title_num}>
                  <Text >{index + 1}</Text>
                </View>
                <View style={styles.title_title}>
                  <Text >{item.compCode}</Text>
                </View>
                <View style={styles.CardList_at_icon}>
                  <IconDown />
                </View>
              </View>
            </TouchableCustom>

            {
              visibleArr.indexOf(item.compCode) != -1 ?
                <View key={index} style={{ backgroundColor: '#F9F9F9' }} >
                  <ListItemScan_child
                    title='产品编号'
                    rightTitle={comp.compCode}
                  />
                  <ListItemScan_child
                    title='构件类型'
                    rightTitle={comp.compTypeName}
                  />
                  <ListItemScan_child
                    title='设计型号'
                    rightTitle={comp.designType}
                  />
                  <ListItemScan_child
                    title='楼号'
                    rightTitle={comp.floorNoName}
                  />
                  <ListItemScan_child
                    title='层号'
                    rightTitle={comp.floorName}
                  />
                  <ListItemScan_child
                    title='砼方量'
                    rightTitle={comp.compVolume}
                  />
                  <ListItemScan_child
                    title='重量'
                    rightTitle={comp.compWeight}
                  />
                  <ListItemScan_child
                    title='结果'
                    rightTitle={comp.confirmResult}
                  />

                  <ListItemScan_child
                    title='拍照'
                    rightElement={
                      this.state.isCheckPage ?
                        <AvatarImg
                          index={0}
                          item={item.problemImg}
                          onPress={() => {
                            this.setState({
                              imageOverSize: true,
                              pictureUri: problemImg
                            })
                          }}
                        /> :
                        pictureSelectArr.indexOf(item.compId) == -1 ?
                          <CameraButton
                            onPress={() => {
                              this.camandlibFunc(item.compId)
                            }}
                          /> : this.imageView(item.compId)
                    }
                  />
                  <ListItemScan_child
                    title='问题描述'
                    rightElement={
                      this.state.isCheckPage ? comp.problemDescription :
                        <Input
                          containerStyle={[styles.quality_input_container, { top: 2 }]}
                          inputContainerStyle={styles.inputContainerStyle}
                          inputStyle={[styles.quality_input_]}
                          placeholder='请输入'
                          //value={comp.problemDescription}
                          onFocus={() => { this.setState({ focusIndex: 6 }) }}
                          onChangeText={(value) => {
                            let tmp = {}
                            tmp.compId = comp.compId
                            tmp.descript = value
                            console.log("🚀 ~ file: main_compreceive.js ~ line 888 ~ miain_compreceive ~ compData.map ~ tmp", tmp)
                            problemDescriptionArr.push(tmp)
                            this.setState({
                              problemDescriptionArr: problemDescriptionArr
                            })
                          }} />
                    }
                  />
                </View> : <View></View>
            }

          </View>
        )
      })
    )
  }

  CardList = (compData) => {
    const { pictureSelect } = this.state
    console.log("🚀 ~ file: main_compreceive.js ~ line 946 ~ miain_compreceive ~ compData", compData)
    return (
      compData.map((item, index) => {
        return (
          <TouchableCustom
            onLongPress={() => {
              Alert.alert(
                '提示信息',
                '是否删除构件信息',
                [{
                  text: '取消',
                  style: 'cancel'
                }, {
                  text: '删除',
                  onPress: () => {
                    item.confirmResult = '确认'
                    let i = compIdArr.indexOf(item.compId)
                    compDataArr.splice(i, 1)
                    compIdArr.splice(i, 1)
                    subsSelect.splice(i, 1)
                    tmpstr = compIdArr.toString()
                    console.log("🚀 ~ file: return_goods.js ~ line 559 ~ ReturnGoods ~ compData.map ~ tmpstr", tmpstr)
                    this.setState({
                      compDataArr: compDataArr,
                      compIdArr: compIdArr,
                      subsSelect: subsSelect
                    })
                  }
                }])

            }}>
            <View style={[styles.listView]} key={index}>
              <View>
                <Card containerStyle={styles.card_containerStyle}>
                  <ListItemScan_child
                    title='产品编号'
                    rightTitle={item.compCode}
                  />
                  <ListItemScan_child
                    title='构件类型'
                    rightTitle={item.compTypeName}
                  />
                  <ListItemScan_child
                    title='设计型号'
                    rightTitle={item.designType}
                  />
                  <ListItemScan_child
                    title='楼号（楼层）'
                    rightTitle={item.floorNoName + '(' + item.floorName + ')'}
                  />
                </Card>
                <ListItemScan
                  isButton={true}
                  title='问题照片'
                  rightElement={
                    this.state.isCheckPage ?
                      <AvatarImg
                        index={0}
                        item={item.problemImg}
                        onPress={() => {
                          this.setState({
                            imageOverSize: true,
                            pictureUri: problemImg
                          })
                        }}
                      /> :
                      pictureSelectArr.indexOf(item.compId) == -1 ?
                        <CameraButton
                          onPress={() => {
                            this.camandlibFunc(item.compId)
                          }}
                        /> : this.imageView(item.compId)
                  }
                />
                <ListItemScan
                  title='问题描述'
                  rightElement={
                    this.state.isCheckPage ? item.problemDescription :
                      <Input
                        containerStyle={[styles.quality_input_container, { top: 2 }]}
                        inputContainerStyle={styles.inputContainerStyle}
                        inputStyle={[styles.quality_input_]}
                        placeholder='请输入'
                        //value={comp.problemDescription}
                        onFocus={() => { this.setState({ focusIndex: 6 }) }}
                        onChangeText={(value) => {
                          let tmp = {}
                          tmp.compId = item.compId
                          tmp.descript = value
                          console.log("🚀 ~ file: main_compreceive.js ~ line 888 ~ miain_compreceive ~ compData.map ~ tmp", tmp)
                          problemDescriptionArr.push(tmp)
                          this.setState({
                            problemDescriptionArr: problemDescriptionArr
                          })
                        }} />
                  }
                />
              </View>

            </View>
          </TouchableCustom>

        )
      })
    )

  }

  GetDeliveryGuide = () => {
    this.setState({ isSubsVisible: true })
  }

  render() {
    const { formCode_CK, subs, subselect, focusIndex, remark, bottomData, bottomVisible, subsSelect, subselecttext, isCheckPage } = this.state

    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          <NavigationEvents
            onWillFocus={this.UpdateControl}
          />
          <ScrollView style={styles.mainView} showsVerticalScrollIndicator={false} >
            <View style={styles.listView}>
              <ListItemScan
                title='运单编号'
                rightTitle={formCode_CK}
              />
              <ListItemScan
                title='送货明细'
                rightElement={
                  <View>
                    <Text onPress={() => { this.GetDeliveryGuide() }} style={[styles.rightTitle, { color: '#656FF6', fontSize: 13, textAlign: 'right' }]} numberOfLines={1}>
                      查看详情
                    </Text>
                  </View>
                }
              />
              <ListItemScan
                title='送货数量'
                rightTitle={deliveryNum}
              />
              <ListItemScan
                title='未确认数量'
                rightTitle={noConfirmNum || "0"}
              />
              <ListItemScan
                focusStyle={focusIndex == '1' ? styles.focusColor : {}}
                title='退库日期'
                rightTitle={this._DatePicker()}
                bottomDivider
              />
              <ListItemScan
                title='备注'
                rightElement={
                  <Input
                    containerStyle={[styles.quality_input_container, { top: 2 }]}
                    inputContainerStyle={styles.inputContainerStyle}
                    inputStyle={[styles.quality_input_]}
                    placeholder='请输入'
                    value={remark}
                    onChangeText={(value) => {
                      this.setState({
                        remark: value
                      })
                    }} />
                }
              />
              <ListItemScan
                isButton={true}
                focusStyle={focusIndex == '2' ? styles.focusColor : {}}
                title='问题构件'
                rightElement={
                  <View style={{ flexDirection: 'row' }}>
                    <TouchableCustom style={{ height: 30 }} onPress={() => {

                      this.isBottomVisible(subs, 2)

                    }} >
                      <View style={{ marginRight: 16, top: 9 }}>
                        <IconDown text={subselecttext} />
                      </View>
                    </TouchableCustom>
                    <ScanButton
                      onPress={() => {
                        this.setState({
                          focusIndex: '2'
                        })
                        this.props.navigation.navigate('XMCXCamera', {
                          type: 'compid',
                          page: 'Main_CompReceive'
                        })
                      }}
                    />
                  </View>
                }
              />


            </View>

            {
              compIdArr.length > 0 ? this.CardList(compDataArr) : <View></View>
            }

            <View style={{ height: deviceWidth * 0.02, }}></View>

            <FormButton
              onPress={isCheckPage ? this.DeleteData : this.PostData}
              title={isCheckPage ? '删除' : '保存'}
              backgroundColor={isCheckPage ? '#EB5D20' : '#17BC29'}
            />
          </ScrollView>

          <BottomSheet
            isVisible={bottomVisible && !this.state.isCheckPage}
            onRequestClose={() => {
              this.setState({
                bottomVisible: false
              })
            }}
            onBackdropPress={() => {
              this.setState({
                bottomVisible: false
              })
            }}
          >
            <ScrollView style={styles.bottomsheetScroll}>
              {
                bottomData.map((item, index) => {
                  let title = ''
                  title = item.productCode
                  let dataItem = {}
                  dataItem.Title = item.compCode
                  dataItem.rightTitle = item.floorNoName + '(' + item.floorName + ')'
                  dataItem.subTitle = item.designType
                  dataItem.subRightTitle = item.compTypeName
                  return (
                    <TouchableCustom
                      onPress={() => {
                        if (item.confirmResult = '确认') {
                          item.confirmResult = '未确认'
                        } else {
                          item.confirmResult = '确认'
                        }
                        let compData = item
                        let compId = item.compId
                        if (compDataArr.length == 0) {
                          subsSelect.push(item)
                          compDataArr.push(item)
                          compIdArr.push(item.compId)
                          this.setState({
                            subsSelect: subsSelect,
                            compDataArr: compDataArr,
                            compIdArr: compIdArr,
                            productCode: item.productCode
                          })
                          tmpstr = tmpstr + compId + ','
                        } else {
                          if (tmpstr.indexOf(compId) == -1) {
                            subsSelect.push(item)
                            compDataArr.push(item)
                            compIdArr.push(item.compId)
                            this.setState({
                              subsSelect: subsSelect,
                              compDataArr: compDataArr,
                              compIdArr: compIdArr,
                              productCode: item.productCode
                            })
                            tmpstr = tmpstr + compId + ','
                          } else {
                            let i = compIdArr.indexOf(compId)
                            compDataArr.splice(i, 1)
                            compIdArr.splice(i, 1)
                            subsSelect.splice(i, 1)
                            tmpstr = compIdArr.toString()
                            console.log("🚀 ~ file: return_goods.js ~ line 559 ~ ReturnGoods ~ compData.map ~ tmpstr", tmpstr)
                            this.setState({
                              compDataArr: compDataArr,
                              compIdArr: compIdArr,
                              subsSelect: subsSelect
                            })
                            //this.toast.show('已经添加过此构件')
                          }
                        }
                        noConfirmNum = compDataArr.length
                        noConfirmVol += item.compVolume
                        confirmNum = deliveryNum - noConfirmNum
                        confirmVol = deliveryVol - noConfirmVol
                        console.log("🚀 ~ file: miain_compreceive.js ~ line 461 ~ miain_compreceive ~ bottomData.map ~ deliveryNum", deliveryNum)
                        console.log("🚀 ~ file: miain_compreceive.js ~ line 461 ~ miain_compreceive ~ bottomData.map ~ confirmNum", confirmNum)
                        console.log("🚀 ~ file: miain_compreceive.js ~ line 461 ~ miain_compreceive ~ bottomData.map ~ noConfirmNum", noConfirmNum)

                        this.setState({
                          confirmNum: confirmNum,
                          confirmVol: confirmVol,
                          noConfirmNum: noConfirmNum,
                          noConfirmVol: noConfirmVol
                        }, () => {
                          this.forceUpdate()
                        })

                        // this.setState({
                        //   bottomVisible: false
                        // })
                      }}
                    >
                      <View style={{
                        paddingVertical: 18,
                        backgroundColor: "white",
                        alignItems: 'center',
                        flexDirection: 'row',
                        borderBottomColor: '#DDDDDD',
                        borderBottomWidth: 0.6,

                      }}>
                        <View style={{ flex: 1 }}>
                          <CheckBoxScan
                            onPress={() => {
                              if (item.confirmResult = '确认') {
                                item.confirmResult = '未确认'
                              } else {
                                item.confirmResult = '确认'
                              }
                              let compData = item
                              let compId = item.compId
                              if (compDataArr.length == 0) {
                                subsSelect.push(item)
                                compDataArr.push(item)
                                compIdArr.push(item.compId)
                                this.setState({
                                  subsSelect: subsSelect,
                                  compDataArr: compDataArr,
                                  compIdArr: compIdArr,
                                  productCode: item.productCode
                                })
                                tmpstr = tmpstr + compId + ','
                              } else {
                                if (tmpstr.indexOf(compId) == -1) {
                                  subsSelect.push(item)
                                  compDataArr.push(item)
                                  compIdArr.push(item.compId)
                                  this.setState({
                                    subsSelect: subsSelect,
                                    compDataArr: compDataArr,
                                    compIdArr: compIdArr,
                                    productCode: item.productCode
                                  })
                                  tmpstr = tmpstr + compId + ','
                                } else {
                                  let i = compIdArr.indexOf(compId)
                                  compDataArr.splice(i, 1)
                                  compIdArr.splice(i, 1)
                                  subsSelect.splice(i, 1)
                                  tmpstr = compIdArr.toString()
                                  console.log("🚀 ~ file: return_goods.js ~ line 559 ~ ReturnGoods ~ compData.map ~ tmpstr", tmpstr)
                                  this.setState({
                                    compDataArr: compDataArr,
                                    compIdArr: compIdArr,
                                    subsSelect: subsSelect
                                  })
                                  //this.toast.show('已经添加过此构件')
                                }
                              }
                              noConfirmNum = compDataArr.length
                              noConfirmVol += item.compVolume
                              confirmNum = deliveryNum - noConfirmNum
                              confirmVol = deliveryVol - noConfirmVol
                              console.log("🚀 ~ file: miain_compreceive.js ~ line 461 ~ miain_compreceive ~ bottomData.map ~ deliveryNum", deliveryNum)
                              console.log("🚀 ~ file: miain_compreceive.js ~ line 461 ~ miain_compreceive ~ bottomData.map ~ confirmNum", confirmNum)
                              console.log("🚀 ~ file: miain_compreceive.js ~ line 461 ~ miain_compreceive ~ bottomData.map ~ noConfirmNum", noConfirmNum)

                              this.setState({
                                confirmNum: confirmNum,
                                confirmVol: confirmVol,
                                noConfirmNum: noConfirmNum,
                                noConfirmVol: noConfirmVol
                              }, () => {
                                this.forceUpdate()
                              })
                            }}
                            checked={tmpstr.indexOf(item.compId) != -1} />
                        </View>
                        <View style={{ flex: 6, paddingHorizontal: 30, }}>
                          <View style={stylesItem.rowView}>
                            <Text style={stylesItem.Title}>{dataItem.Title}</Text>
                            <Text style={stylesItem.textright}>{dataItem.rightTitle}</Text>
                          </View>
                          <View style={[stylesItem.rowView, { paddingBottom: 0 }]}>
                            <Text style={stylesItem.subTitle}>{dataItem.subTitle}</Text>
                            <Text style={stylesItem.textright}>{dataItem.subRightTitle}</Text>
                          </View>
                        </View>
                        {/*  <Text style={{ color: '#333', textAlign: 'center', fontSize: 14, textAlignVertical: 'center' }}>{title}</Text> */}
                      </View>
                    </TouchableCustom>
                  )

                })
              }
            </ScrollView>

            <TouchableCustom
              onPress={() => {
                this.setState({ bottomVisible: false })
              }}>
              <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
            </TouchableCustom>
          </BottomSheet>

          <Overlay
            fullScreen={true}
            animationType='fade'
            isVisible={this.state.isSubsVisible}
            onRequestClose={() => {
              this.setState({ isSubsVisible: !this.state.isSubsVisible });
            }}>
            <Header
              containerStyle={{ height: 40, paddingTop: 0 }}
              centerComponent={{ text: '送货明细', style: { color: 'gray', fontSize: 20 } }}
              rightComponent={<BackIcon
                onPress={() => {
                  this.setState({
                    isSubsVisible: !this.state.isSubsVisible
                  });
                }} />}
              backgroundColor='white'
            />
            <ScrollView>
              {
                this.state.subs.map((item, index) => {
                  return (
                    <Card containerStyle={{ borderRadius: 10 }}>
                      <TouchableOpacity >
                        <View style={{ flexDirection: 'row', }}>
                          <View style={[styles.SN_View]}>
                            <View style={[styles.SN_Text_View]}>
                              <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                            </View>
                          </View>
                          <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 14, }}>
                            <View style={{ flexDirection: 'row' }}>
                              <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.compCode}</Text>
                              <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.designType}</Text>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                              <Text style={[styles.text]} numberOfLines={1}> {item.compTypeName}</Text>
                              <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.floorNoName + '(' + item.floorName + ')'}</Text>
                            </View>
                            {/*  <View style={{ flexDirection: 'row' }}>
                              <Text style={[styles.text, styles.textthi]} numberOfLines={1}> {item.compTypeName}</Text>
                              <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.libraryName}</Text>
                            </View> */}
                          </View>
                        </View>
                      </TouchableOpacity>
                    </Card>
                  )
                })
              }
            </ScrollView>

          </Overlay>
        </View>
      );
    }
  }
}

const stylesItem = StyleSheet.create({
  rowView: {
    flex: 1,
    //backgroundColor: 'red', 
    flexDirection: 'row',
    justifyContent: "space-between",
    paddingBottom: 12
  },
  Title: {
    fontSize: 16,
    /* marginBottom: 5,
    marginTop: 5, */
    //flex: 1
  },
  subTitle: {
    color: 'gray'
  },
  textright: {
    //flex: 2,
    textAlign: 'right',
    color: 'gray',
    //backgroundColor: 'blue'
  },

})

class BackIcon extends React.PureComponent {
  render() {
    return (
      <TouchableOpacity
        onPress={this.props.onPress} >
        <Icon
          name='arrow-back'
          color='#656FF6' />
      </TouchableOpacity>
    )
  }
}
