import React, { Component } from 'react';
import { View, TouchableOpacity, TouchableHighlight, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, Input, Icon, Badge, Card, ListItem, BottomSheet, CheckBox, Avatar, Overlay, Header } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Url from '../../Url/Url';
import { deviceHeight, deviceWidth, RFT } from '../../Url/Pixal';
import { ScrollView } from 'react-native-gesture-handler';
import CardList from "../Components/CardList";
import DatePicker from 'react-native-datepicker'
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { Image } from 'react-native';
import { NavigationEvents } from 'react-navigation';
import { styles } from '../../PDA/Componment/PDAStyles';
import ScanButton from '../../PDA/Componment/ScanButton';
import CameraButton from '../Components/CameraButton';
import FormButton from '../../PDA/Componment/formButton';
import ListItemScan from '../../PDA/Componment/ListItemScan';
import ListItemScan_child from '../../PDA/Componment/ListItemScan_child';
import IconDown from '../../PDA/Componment/IconDown';
import BottomItem from '../../PDA/Componment/BottomItem';
import TouchableCustom from '../../PDA/Componment/TouchableCustom';
import CheckBoxScan from '../../PDA/Componment/CheckBoxScan';
import AvatarAdd from '../../PDA/Componment/AvatarAdd';
import AvatarImg from '../Components/AvatarImg_NoEdit';
import DeviceStorage from '../../Url/DeviceStorage';
import Pdf from 'react-native-pdf';
import navigation from '../../Url/navigation';
import CustomButton from '../Components/CustomButton';
import overlayImg from '../../PDA/Componment/OverlayImg';


let imageArr = [], pictureSelectArr = [];

let pageType = ''

class BackIcon extends React.PureComponent {
  render() {
    return (
      <TouchableOpacity
        onPress={this.props.onPress} >
        <Icon
          name='arrow-back'
          color='#419FFF' />
      </TouchableOpacity>
    )
  }
}


export default class comprepair extends Component {
  constructor(props) {
    super(props);
    this.state = {
      resData: [],
      formCode: '',
      compData: {},
      compId: '',
      compCode: '',
      applyDate: '',
      applyEmployeeName: '',
      problemDescription: '',
      repairDate: '',
      repairEmployeeName: '',
      repairImg: '',
      repairResult: '',
      confirmEmployeeName: '',
      confirmDate: '',
      //照片
      problemImg: "",
      repairImg: "",
      imageArr: [],
      pictureSelect: false,
      pictureSelectArr: [],
      imageOverSize: false,
      pictureUri: '',
      isLoading: true,
      focusIndex: '',
      bottomData: [],
      bottomVisible: false,
      isGetcomID: false,
      isCheckPage: false,
      focusIndex: ''
    };
  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    console.log("🚀 ~ file: main_comprepair_confirm.js ~ line 87 ~ comprepair ~ navigation", navigation)
    console.log("🚀 ~ file: main_comprepair_confirm.js ~ line 87 ~ comprepair ~ navigation.state", navigation.state)
    return {
      title: navigation.getParam('title'),
    }
  }

  componentDidMount() {
    const { navigation } = this.props;
    navigation.setParams({
      PostData: this.PostData
    })
    guid = navigation.getParam('guid') || ''
    pageType = navigation.getParam('pageType') || ''
    console.log("🚀 ~ file: main_comprepair.js ~ line 86 ~ comprepair ~ componentDidMount ~ pageType", pageType)
    if (pageType == 'check') {
      this.checkResData(guid)
      this.setState({ isCheckPage: true })
    } else {
      this.resData(guid)
    }
  }

  componentWillUnmount() {
    imageArr = [], pictureSelectArr = [];

    pageType = ''
  }

  UpdateControl = () => {
    const { navigation } = this.props;
    const QRid = navigation.getParam('QRid') || ''
    console.log("🚀 ~ file: main_comprepair.js ~ line 101 ~ comprepair ~ QRid", QRid)
    let type = navigation.getParam('type') || '';
    console.log("🚀 ~ file: main_comprepair.js ~ line 102 ~ comprepair ~ type", type)
    if (type == 'compid') {
      this.GetcomID(QRid)
    }
  }

  checkResData = (guid) => {
    let url = 'http://10.100.132.129:9100/pcservice/v1/PDAService.ashx'
    let formData = new FormData();
    let data = {};
    data = {
      "action": "initcomprepairqueryinfo",
      "servicetype": "projectsystem",
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        "factoryId": Url.Fidweb,
        "projectId": Url.ProjectId,
        "rowGuid": guid,
        "isprj": "1",
        "type": "choose"
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: main_comprepair.js ~ line 110 ~ comprepair ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: main_comprepair.js ~ line 122 ~ comprepair ~ resData", resData)
      if (resData.status == 100) {
        let resultcheck = resData.result.main
        let formCode = resultcheck.formCode
        let applyDate = resultcheck.applyDate
        let problemImg = resultcheck.problemImg
        let applyEmployeeName = resultcheck.applyEmployeeName
        let problemDescription = resultcheck.problemDescription
        let repairEmployeeName = resultcheck.repairEmployeeName
        let repairImg = resultcheck.repairImg
        let repairDate = resultcheck.repairDate
        let repairResult = resultcheck.repairResult
        let confirmDate = resultcheck.confirmDate
        let confirmEmployeeName = resultcheck.confirmEmployeeName
        let confirmEmployeeId = resultcheck.confirmEmployeeId

        let compId = resultcheck.compId
        let compCode = resultcheck.compCode
        let projectName = resultcheck.projectName
        let compTypeName = resultcheck.compTypeName
        let designType = resultcheck.designType
        let floorNoName = resultcheck.floorNoName
        let floorName = resultcheck.floorName
        let volume = resultcheck.compVolume
        let weight = resultcheck.compWeight

        //产品子表
        let compData = {}, result = {}
        result.projectName = projectName
        result.compTypeName = compTypeName
        result.designType = designType
        result.floorNoName = floorNoName
        result.floorName = floorName
        result.volume = volume
        result.weight = weight
        compData.result = result

        this.setState({
          resData: resData,
          formCode: formCode,
          applyDate: applyDate,
          applyEmployeeName: applyEmployeeName,
          problemDescription: problemDescription,
          repairEmployeeName: repairEmployeeName,
          repairDate: repairDate,
          repairResult: repairResult,
          confirmEmployeeName: confirmEmployeeName,
          confirmDate: confirmDate,
          problemImg: problemImg,
          repairImg: repairImg,
          imageArr: imageArr,
          compCode: compCode,
          compId: compId,
          compData: compData,
          pictureSelect: true,
          isGetcomID: true,
        })
      } else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }


      this.setState({
        isLoading: false,
      })
    }).catch((error) => {
      console.log("🚀 ~ file: qualityinspection.js ~ line 215 ~ QualityInspection ~ error", error)
    });
  }

  resData = (guid) => {
    let url = 'http://10.100.132.129:9100/pcservice/v1/PDAService.ashx'
    let formData = new FormData();
    let data = {};
    data = {
      "action": "initcompapplyrepairinfo",
      "servicetype": "projectsystem",
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        "rowGuid": guid,
        "factoryId": Url.Fidweb,
        "projectId": Url.ProjectId,
        "userName": Url.username,
        "type": "confirm"
      }
    }

    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: main_comprepair.js ~ line 89 ~ comprepair ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: miain_compreceive.js ~ line 77 ~ miain_compreceive ~ resData", resData)
      //let result = resData.result
      //初始化错误提示
      if (resData.status == 100) {
        let resultcheck = resData.result
        let formCode = resultcheck.formCode
        let applyDate = resultcheck.applyDate
        let problemImg = resultcheck.problemImg
        let applyEmployeeName = resultcheck.applyEmployeeName
        let problemDescription = resultcheck.problemDescription
        let repairEmployeeName = resultcheck.repairEmployeeName
        let repairImg = resultcheck.repairImg
        let repairDate = resultcheck.repairDate
        let repairResult = resultcheck.repairResult
        let confirmDate = resultcheck.confirmDate
        let confirmEmployeeName = resultcheck.confirmEmployeeName

        let compId = resultcheck.compId
        let compCode = resultcheck.compCode
        let projectName = resultcheck.projectName
        let compTypeName = resultcheck.compTypeName
        let designType = resultcheck.designType
        let floorNoName = resultcheck.floorNoName
        let floorName = resultcheck.floorName
        let volume = resultcheck.compVolume
        let weight = resultcheck.compWeight

        //产品子表
        let compData = {}, result = {}
        result.projectName = projectName
        result.compTypeName = compTypeName
        result.designType = designType
        result.floorNoName = floorNoName
        result.floorName = floorName
        result.volume = volume
        result.weight = weight
        compData.result = result

        this.setState({
          resData: resData,
          formCode: formCode,
          applyDate: applyDate,
          applyEmployeeName: applyEmployeeName,
          problemDescription: problemDescription,
          repairEmployeeName: repairEmployeeName,
          repairDate: repairDate,
          repairResult: repairResult,
          confirmEmployeeName: confirmEmployeeName,
          confirmDate: confirmDate,
          problemImg: problemImg,
          repairImg: repairImg,
          compCode: compCode,
          compId: compId,
          compData: compData,
          pictureSelect: true,
          isGetcomID: true,
          isLoading: false,
        })
      }
      else {
        Alert.alert("错误提示", resData.message, [{
          text: '取消',
          style: 'cancel'
        }, {
          text: '返回上一级',
          onPress: () => {
            this.props.navigation.goBack()
          }
        }])
      }

    }).catch((error) => {
    });

  }

  PostData = () => {
    const { resData, compData, compId, imageArr, applyDate, problemDescription } = this.state
    console.log("🚀 ~ file: main_comprepair.js ~ line 251 ~ comprepair ~ compData", compData)
    console.log("🚀 ~ file: main_comprepair.js ~ line 251 ~ comprepair ~ resData", resData)

    let mainData = resData.result
    let _compData = compData.result

    let url = 'http://10.100.132.129:9100/pcservice/v1/PDAService.ashx'

    let formData = new FormData();
    let data = {};
    data = {
      "action": "savecompapplyrepair",
      "servicetype": "projectsystem",
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        "type": "confirm",
        "userName": Url.username,
        "main": {
          "confirmId": mainData.confirmId,
          "confirmCode": mainData.confirmCode,
          "projectId": mainData.projectId,
          "projectName": mainData.projectName,
          "compId": mainData.projectName,
          "applyEmployeeId": mainData.applyEmployeeId,
          "applyEmployeeName": mainData.applyEmployeeName,
          "applyDate": mainData.applyDate,
          "problemImg": mainData.problemImg,
          "problemDescription": mainData.problemDescription,
          "repairEmployeeId": mainData.repairEmployeeId,
          "repairEmployeeName": mainData.repairEmployeeName,
          "repairDate": mainData.repairDate,
          "repairResult": mainData.repairResult,
          "repairImg": mainData.repairImg,
          "confirmEmployeeId": Url.EmployeeId,
          "confirmEmployeeName": Url.employeeName,
          "confirmDate": mainData.confirmDate,
          "repairState": "已确认",
          "formCode": mainData.formCode,
          "_bizid": Url.Fidweb,
          "_Rowguid": mainData._Rowguid
        }
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: main_comprepair.js ~ line 295 ~ comprepair ~ formData", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('保存成功');
        this.props.navigation.goBack()
      } else {
        Alert.alert('保存失败', resData.message)
        console.log('resData', resData)
      }
    }).catch((error) => {
    });

  }

  DeleteData = () => {
    let url = 'http://10.100.132.129:9100/pcservice/v1/PDAService.ashx'

    let formData = new FormData();
    let data = {};
    data = {
      "action": "deletecomprepair",
      "servicetype": "projectsystem",
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        "rowGuid": guid
      }
    }

    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: main_compreceive.js ~ line 358 ~ miain_compreceive ~ DeleteData ~ formData", formData)

    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      console.log(res.statusText);
      console.log(res);
      return res.json();
    }).then(resData => {
      if (resData.status == '100') {
        this.toast.show('删除成功');
        this.props.navigation.goBack()
      } else {
        Alert.alert('删除失败', resData.message)
      }
    }).catch((error) => {
    });
  }

  GetcomID = (id) => {
    let url = 'http://10.100.132.129:9100/pcservice/v1/PDAService.ashx'

    console.log("🚀 ~ file: main_comprepair.js ~ line 351 ~ comprepair ~ id", id)
    let formData = new FormData();
    let data = {};
    data = {
      "action": "initscandefectcompinfo",
      "servicetype": "projectsystem",
      "express": "0F51EF23",
      "ciphertext": "a91d6dc5c5a20f2a35939b6e2d91a0ef",
      "data": {
        "compId": id,
        "factoryId": Url.Fidweb,
        "projectId": Url.ProjectId
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: main_comprepair.js ~ line 356 ~ comprepair ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: main_comprepair.js ~ line 377 ~ comprepair ~ resData", resData)
      if (resData.status == '100') {
        let compCode = resData.result.compCode
        let compId = resData.result._Rowguid
        this.setState({
          compData: resData,
          compCode: compCode,
          compId: compId,
          isGetcomID: true,
        })
      } else {
        console.log('resData', resData)
        this.toast.show(resData.message)
        this.setState({
          compId: ""
        })
      }

    }).catch(err => {
    })
  }

  comIDItem = (index) => {
    const { isGetcomID, compCode, compId } = this.state
    return (
      <View>
        {
          !isGetcomID ?
            <View style={{ flexDirection: 'row' }}>
              <View>
                <Input
                  ref={ref => { this.input1 = ref }}
                  containerStyle={styles.scan_input_container}
                  inputContainerStyle={styles.scan_inputContainerStyle}
                  inputStyle={[styles.scan_input]}
                  placeholder='请输入'
                  keyboardType='numeric'
                  value={compId}
                  onChangeText={(value) => {
                    value = value.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
                    value = value.replace(/^\./g, ""); //验证第一个字符是数字
                    value = value.replace(/\.{2,}/g, "."); //只保留第一个, 清除多余的
                    //value = value.replace(/\b(0+)/gi, ""); //清楚开头的0
                    this.setState({
                      compId: value
                    })
                  }}
                  //onSubmitEditing={() => { this.input1.focus() }}
                  //onFocus={() => { this.setState({ focusIndex: '5' }) }}
                  /*  */
                  //returnKeyType = 'previous'
                  onSubmitEditing={() => {
                    let inputComId = this.state.compId
                    console.log("🚀 ~ file: qualityinspection.js ~ line 468 ~ QualityInspection ~ inputComId", inputComId)
                    inputComId = inputComId.replace(/\b(0+)/gi, "")
                    this.GetcomID(inputComId)
                  }}
                />
              </View>
              <ScanButton
                onPress={() => {
                  this.setState({
                    focusIndex: 0
                  })
                  this.props.navigation.navigate('XMCXCamera', {
                    type: 'compid',
                    page: 'Main_CompRepair'
                  })
                }}
              />
            </View>
            :
            <TouchableCustom
              onPress={() => {
                console.log('onPress')
              }}
              onLongPress={() => {
                console.log('onLongPress')
                Alert.alert(
                  '提示信息',
                  '是否删除构件信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      this.setState({
                        compData: [],
                        compCode: '',
                        compId: '',
                        iscomIDdelete: true,
                        isGetcomID: false,
                      })
                    }
                  }])
              }} >
              <Text>{compCode}</Text>
            </TouchableCustom>
        }
      </View>

    )
  }

  _DatePicker = () => {
    const { ServerTime } = this.state
    return (
      <DatePicker
        customStyles={{
          dateInput: styles.dateInput,
          dateTouchBody: styles.dateTouchBody,
          dateText: styles.dateText,
        }}
        iconComponent={<Icon name='caretdown' color='#419fff' iconStyle={{ fontSize: 13 }} type='antdesign' ></Icon>
        }
        showIcon={true}
        mode='datetime'
        date={ServerTime}
        format="YYYY-MM-DD HH:mm"
        onOpenModal={() => {
          this.setState({
            focusIndex: '4'
          })
        }}
        onDateChange={(value) => {
          this.setState({
            ServerTime: value
          })
        }}
      />
    )
  }

  isBottomVisible = (data, focusIndex) => {
    if (typeof (data) != "undefined") {
      if (data.length > 0) {
        this.setState({ bottomVisible: true, bottomData: data, focusIndex: focusIndex })
      }
    } else {
      this.toast.show("无数据")
    }
  }

  cameraFunc = (compId) => {
    launchCamera(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: parseInt(Url.isResizePhoto),
        maxWidth: parseInt(Url.isResizePhoto),
        saveToPhotos: true,

      },
      (response) => {
        if (response.uri == undefined) {
          return
        }
        let tmp = {}
        tmp.name = compId
        tmp.value = response.uri
        imageArr.push(tmp)
        this.setState({
          pictureSelect: true,
          imageArr: imageArr
        })
        pictureSelectArr.push(compId)
        this.setState({
          pictureSelectArr: pictureSelectArr
        })
        console.log(response)
      },
    )
  }

  camLibraryFunc = (compId) => {
    launchImageLibrary(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: parseInt(Url.isResizePhoto),
        maxWidth: parseInt(Url.isResizePhoto),
      },
      (response) => {
        if (response.uri == undefined) {
          return
        }
        let tmp = {}
        tmp.name = compId
        tmp.value = response.uri
        imageArr.push(tmp)
        this.setState({
          pictureSelect: true,
          imageArr: imageArr
        })
        pictureSelectArr.push(compId)
        this.setState({
          pictureSelectArr: pictureSelectArr
        })
        console.log(response)
      },
    )
  }

  camandlibFunc = (compId) => {
    Alert.alert(
      '提示信息',
      '选择拍照方式',
      [{
        text: '取消',
        style: 'cancel'
      }, {
        text: '拍照',
        onPress: () => {
          this.cameraFunc(compId)
        }
      }, {
        text: '相册',
        onPress: () => {
          this.camLibraryFunc(compId)
        }
      }])
  }

  imageView = (compId) => {
    return (
      <View style={{ flexDirection: 'row' }}>
        {
          // this.state.pictureSelect ?
          this.state.imageArr.map((item, index) => {
            if (item.name == compId) {
              return (
                <AvatarImg
                  item={item.value}
                  index={index}
                  onPress={() => {
                    this.setState({
                      imageOverSize: true,
                      pictureUri: item.value
                    })
                  }}
                  onLongPress={() => {
                    Alert.alert(
                      '提示信息',
                      '是否删除构件照片',
                      [{
                        text: '取消',
                        style: 'cancel'
                      }, {
                        text: '删除',
                        onPress: () => {


                          console.log("🚀 ~ file: miain_compreceive.js ~ line 375 ~ miain_compreceive ~ this.state.imageArr.map ~ imageArr", imageArr)
                          let p = pictureSelectArr.indexOf(item.value)
                          imageArr.splice(p, 1)
                          pictureSelectArr.splice(p, 1)
                          console.log("🚀 ~ file: miain_compreceive.js ~ line 375 ~ miain_compreceive ~ this.state.imageArr.map ~ pictureSelectArr", pictureSelectArr)
                          this.setState({
                            imageArr: imageArr,
                            pictureSelectArr: pictureSelectArr
                          }, () => {
                            this.forceUpdate()
                          })
                          // if (i > -1) {

                          // }
                          if (imageArr.length == 0) {
                            this.setState({
                              pictureSelect: false
                            })
                          }
                          console.log("🚀 ~ file: qualityinspection.js ~ line 639 ~ SteelCage ~ this.state.imageArr.map ~ imageArr", imageArr)
                        }
                      }])
                  }} />
              )
            }
            //if (index == 0) {

          })
          // : <View></View>
        }
      </View>
    )
  }

  cardListContont = comp => {
    if (visibleArr.indexOf(comp.compCode) == -1) {
      visibleArr.push(comp.compCode);
      this.setState({ visibleArr: visibleArr });
    } else {
      if (visibleArr.length == 1) {
        visibleArr.splice(0, 1);
        this.setState({ visibleArr: visibleArr }, () => {
          this.forceUpdate;
        });
      } else {
        visibleArr.map((item, index) => {
          if (item == comp.compCode) {
            visibleArr.splice(index, 1);
            this.setState({ visibleArr: visibleArr }, () => {
              this.forceUpdate;
            });
          }
        });
      }
    }
  };

  CardList = (compData) => {

    return (
      compData.map((item, index) => {
        let comp = item
        console.log("🚀 ~ file: main_compreceive.js ~ line 463 ~ miain_compreceive ~ compData.map ~ comp", comp)
        return (
          <View style={styles.CardList_main}>
            <TouchableCustom
              onPress={() => {
                this.cardListContont(comp);
              }}
              onLongPress={() => {
                Alert.alert(
                  '提示信息',
                  '是否删除构件信息',
                  [{
                    text: '取消',
                    style: 'cancel'
                  }, {
                    text: '删除',
                    onPress: () => {
                      compDataArr.splice(index, 1)
                      compIdArr.splice(index, 1)
                      subsSelect.splice(index, 1)
                      tmpstr = compIdArr.toString()
                      console.log("🚀 ~ file: return_goods.js ~ line 559 ~ ReturnGoods ~ compData.map ~ tmpstr", tmpstr)
                      this.setState({
                        compDataArr: compDataArr,
                        compIdArr: compIdArr,
                        subsSelect: subsSelect
                      })
                    }
                  }])
              }}>
              <View style={styles.CardList_title_main}>
                <View style={styles.title_num}>
                  <Text >{index + 1}</Text>
                </View>
                <View style={styles.title_title}>
                  <Text >{item.productCode}</Text>
                </View>
                <View style={styles.CardList_at_icon}>
                  <IconDown />
                </View>
              </View>
            </TouchableCustom>

            {
              visibleArr.indexOf(item.compCode) != -1 ?
                <View key={index} style={{ backgroundColor: '#F9F9F9' }} >
                  <ListItemScan_child
                    title='产品编号'
                    rightTitle={comp.compCode}
                  />
                  <ListItemScan_child
                    title='构件类型'
                    rightTitle={comp.compTypeName}
                  />
                  <ListItemScan_child
                    title='设计型号'
                    rightTitle={comp.designType}
                  />
                  <ListItemScan_child
                    title='楼号'
                    rightTitle={comp.floorNoName}
                  />
                  <ListItemScan_child
                    title='层号'
                    rightTitle={comp.floorName}
                  />
                  <ListItemScan_child
                    title='砼方量'
                    rightTitle={comp.compVolume}
                  />
                  <ListItemScan_child
                    title='重量'
                    rightTitle={comp.compWeight}
                  />
                  <ListItemScan_child
                    title='拍照'
                    rightElement={
                      pictureSelectArr.indexOf(item.compId) == -1 ?
                        <CameraButton
                          onPress={() => {
                            this.camandlibFunc(item.compId)
                          }}
                        /> : this.imageView(item.compId)
                    }
                  />
                  <ListItemScan_child
                    title='问题描述'
                    rightElement={
                      <Input
                        containerStyle={[styles.quality_input_container, { top: 2 }]}
                        inputContainerStyle={styles.inputContainerStyle}
                        inputStyle={[styles.quality_input_]}
                        placeholder='请输入'
                        value={comp.problemDescription}
                        onFocus={() => { this.setState({ focusIndex: 6 }) }}
                        onChangeText={(value) => {
                          let tmp = {}
                          tmp.compId = comp.compId
                          tmp.descript = value

                        }} />
                    }
                  />
                </View> : <View></View>
            }

          </View>
        )
      })
    )
  }

  render() {
    const { formCode, compData, compId, compCode, problemDescription, remark, isGetcomID, isCheckPage, focusIndex, bottomData, bottomVisible, pictureSelect, problemImg, repairImg, repairResult, repairEmployeeName, repairDate, applyDate, applyEmployeeName, confirmDate, confirmEmployeeName } = this.state

    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
      //渲染页面
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Toast ref={(ref) => { this.toast = ref; }} position="center" />
          <NavigationEvents
            onWillFocus={this.UpdateControl}
          />
          <ScrollView style={styles.mainView} showsVerticalScrollIndicator={false} >
            <View style={styles.listView}>

              <ListItemScan
                title='报修编号'
                rightTitle={formCode}
              />
              <ListItemScan
                isButton={!isGetcomID}
                focusStyle={focusIndex == '0' ? styles.focusColor : {}}
                title='产品编号'
                rightElement={
                  this.comIDItem()
                }
              />

              {
                isGetcomID ? <CardList compData={compData} /> : <View></View>
              }

              <ListItemScan
                title='报修照片'
                rightElement={
                  <View>
                    <AvatarImg
                      index={0}
                      item={problemImg}
                      onPress={() => {
                        this.setState({
                          imageOverSize: true,
                          pictureUri: problemImg
                        })
                      }}
                    />
                  </View>
                }
              />

              <ListItemScan
                focusStyle={focusIndex == '1' ? styles.focusColor : {}}
                title='问题描述'
                rightTitle={problemDescription}
              />

              <ListItemScan
                focusStyle={focusIndex == '2' ? styles.focusColor : {}}
                title='报修日期'
                rightTitle={applyDate}
                bottomDivider
              />
              <ListItemScan
                title='报修人'
                rightTitleStyle={{ width: deviceWidth / 1.5, textAlign: 'right', fontSize: 15 }}
                rightTitle={applyEmployeeName}
              />

              <ListItemScan
                title='处理结果'
                rightTitle={repairResult}
              />

              <ListItemScan
                title='处理后照片'
                rightElement={
                  <View>
                    <AvatarImg
                      index={0}
                      item={repairImg}
                      onPress={() => {
                        this.setState({
                          imageOverSize: true,
                          pictureUri: repairImg
                        })
                      }}
                    />
                  </View>
                }
              />

              <ListItemScan
                title='处理日期'
                rightTitle={repairDate}
              />

              <ListItemScan
                title='维修人'
                rightTitle={repairEmployeeName}
              />

              <ListItemScan
                title='确认日期'
                rightTitle={confirmDate}
              />

              <ListItemScan
                title='确认人'
                rightTitle={confirmEmployeeName}
              />

            </View>

            <FormButton
              onPress={isCheckPage ? this.DeleteData : this.PostData}
              title={isCheckPage ? '删除' : '确认'}
              backgroundColor={isCheckPage ? '#EB5D20' : '#17BC29'}
            />
          </ScrollView>

          {overlayImg(obj = {
              onRequestClose: () => {
                this.setState({ imageOverSize: !this.state.imageOverSize }, () => {
                  console.log("🚀 ~ file: equipment_maintenance.js:1696 ~ render ~ imageOverSize:", this.state.imageOverSize)
                })
              },
              onPress: () => {
                this.setState({
                  imageOverSize: !this.state.imageOverSize
                })
              },
              uri: this.state.pictureUri,
              isVisible: this.state.imageOverSize
            })
            }

          <BottomSheet
            isVisible={bottomVisible && !this.state.isCheckPage}
            onRequestClose={() => {
              this.setState({
                bottomVisible: false
              })
            }}
            onBackdropPress={() => {
              this.setState({
                bottomVisible: false
              })
            }}
          >
            <ScrollView style={styles.bottomsheetScroll}>
              {
                bottomData.map((item, index) => {
                  let title = ''
                  title = item.productCode
                  return (
                    <TouchableCustom
                      onPress={() => {
                        let compData = item
                        let compId = item.compId
                        if (compDataArr.length == 0) {
                          subsSelect.push(item)
                          compDataArr.push(item)
                          compIdArr.push(item.compId)
                          this.setState({
                            subsSelect: subsSelect,
                            compDataArr: compDataArr,
                            compIdArr: compIdArr,
                            productCode: item.productCode
                          })
                          tmpstr = tmpstr + compId + ','
                        } else {
                          if (tmpstr.indexOf(compId) == -1) {
                            subsSelect.push(item)
                            compDataArr.push(item)
                            compIdArr.push(item.compId)
                            this.setState({
                              subsSelect: subsSelect,
                              compDataArr: compDataArr,
                              compIdArr: compIdArr,
                              productCode: item.productCode
                            })
                            tmpstr = tmpstr + compId + ','
                          } else {
                            this.toast.show('已经t添加过此构件')
                          }
                        }
                        confirmNum = compDataArr.length
                        confirmVol += item.compVolume
                        noConfirmNum = deliveryNum - confirmNum
                        noConfirmVol = deliveryVol - confirmVol
                        console.log("🚀 ~ file: miain_compreceive.js ~ line 461 ~ miain_compreceive ~ bottomData.map ~ deliveryNum", deliveryNum)
                        console.log("🚀 ~ file: miain_compreceive.js ~ line 461 ~ miain_compreceive ~ bottomData.map ~ confirmNum", confirmNum)
                        console.log("🚀 ~ file: miain_compreceive.js ~ line 461 ~ miain_compreceive ~ bottomData.map ~ noConfirmNum", noConfirmNum)

                        this.setState({
                          confirmNum: confirmNum,
                          confirmVol: confirmVol,
                          noConfirmNum: noConfirmNum,
                          noConfirmVol: noConfirmVol
                        }, () => {
                          this.forceUpdate()

                        })

                        this.setState({
                          bottomVisible: false
                        })
                      }}
                    >
                      <BottomItem backgroundColor='white' color="#333" title={title} />
                    </TouchableCustom>
                  )

                })
              }
            </ScrollView>

            <TouchableCustom
              onPress={() => {
                this.setState({ bottomVisible: false })
              }}>
              <BottomItem backgroundColor='#e74c3c' color="white" title={'关闭'} />
            </TouchableCustom>
          </BottomSheet>
        </View>
      );
    }

  }
}
