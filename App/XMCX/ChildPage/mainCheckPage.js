import React, { Component } from 'react'
import { View, StatusBar, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert, PanResponder, ScrollView } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge } from 'react-native-elements';
import { createMaterialTopTabNavigator } from 'react-navigation'
import { NavigationEvents } from 'react-navigation';
import Url from '../../Url/Url';
import { RFT, deviceWidth } from '../../Url/Pixal';
import CustomButton from '../Components/CustomButton';
import Toast from 'react-native-easy-toast';

import Main_CompRepair from './main_comprepair';


let isCheckSecondList = []; //是否展开二级


let pageNum = 1, pageNumMy = 1, page = '', mainNav = '', sontitle = '', text = '', pageType = '', scanQRid = false, mainpage = ''
let listData = [], data_All = [], data_my = []

let type = '', QRid = ''

let selectList = [
  { name: '待确认', day: 'all' },
  { name: '已确认', day: 'all' },
]

//搜索栏搜索图标
const defaultSearchIcon = () => ({
  //type: 'antdesign',
  size: 24,
  name: 'search',
  color: '#656FF6',
});

//搜索栏清理图标
const defaultClearIcon = () => ({
  //type: '',
  size: 20,
  name: 'close',
  color: '#999',
});

//查看页面将全部和我的合为一个
export default class MainCheckPage extends React.Component {
  constructor() {
    super();
    this.state = {
      search: '', //搜索关键字保存
      pageNum: '1', //页码
      pageNumMy: '1',
      tabIndex: '', //tab栏
      resData: {}, //接口数据保存
      listData: [], //列表数据
      data: [],
      data_All: [],
      data_my: [],
      isLoading: true,  //判断是否加载 加载页
      isRefreshing: false,  //是否刷新
      selectedIndex: 0,  //按钮组选择
      page: 0,
      pageLoading: true,
      tabs: [{ title: "待确认" }, { title: "已确认" }],
      isCheckSecondList: [], //二级菜单数组,
      isBatch: false,
      selectList: [
        { name: '待确认', day: 'all' },
        { name: '已确认', day: 'all' },
      ],
      typeFocusButton: 0,
      isScanCode: false
    }
  }

  componentDidMount() {
    const { navigation } = this.props;
    text = navigation.getParam('text') || ''
    page = navigation.getParam('title') || ''
    pageType = navigation.getParam('pagetype') || ''
    console.log("🚀 ~ file: mainCheckPage.js ~ line 66 ~ MainCheckPage ~ componentDidMount ~ pageType", pageType)
    mainpage = navigation.getParam('mainpage')
    console.log("🚀 ~ file: mainCheckPage.js ~ line 68 ~ MainCheckPage ~ componentDidMount ~ mainpage", mainpage)
    this.isScan()
    this.selectListJudge()
    this.requestData(page, "");
  }

  UpdateControl = () => {
    const { navigation } = this.props;
    text = navigation.getParam('text') || ''
    page = navigation.getParam('title') || ''
    console.log("🚀 ~ file: mainCheckPage.js ~ line 82 ~ MainCheckPage ~ page", page)
    pageType = navigation.getParam('pagetype') || ''
    // 扫码跳转回来获取参数
    mainNav = navigation.getParam('main') || ''
    mainpage = navigation.getParam('mainpage')
    console.log("🚀 ~ file: mainCheckPage.js ~ line 68 ~ MainCheckPage ~ componentDidMount ~ mainpage", mainpage)
    this.setState({ isLoading: true })
    this.requestData(page, "");
    this.isScan()

    //扫构件码后进入
    type = navigation.getParam('type') || ''
    QRid = navigation.getParam('QRid') || ''

  }

  componentWillUnmount() {
    mainNav = '', text = '', pageType = '', mainpage = ''
    pageNum = 1
    pageNumMy = 1
    listData = [], data_All = [], data_my = []
    scanQRid = false
    type = '', QRid = ''
    selectList = [
      { name: '待确认', day: 'all' },
      { name: '已确认', day: 'all' },
    ]

  }

  GetcomID = (id, type) => {
    let formData = new FormData();
    let data = {};

    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: MainCheckPage.js ~ line 123 ~ MainCheckPage ~ data:", data)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      //console.log("🚀 ~ file: MainCheckPage.js ~ line 131 ~ MainCheckPage ~ res:", res.json())
      return res.json();
    }).then(resData => {
      if (resData.status == '113') {
        this.toast.show(resData.message);
      } else if (resData.status == '100') {
        let guid = resData.result.guid
        setTimeout(() => {
          this.props.navigation.navigate('SampleManage', {
            guid: guid,
            pageType: 'Scan',
            title: '抽检整改单',
            isCheckComID: true
          })
        }, 1000)
      } else {
        console.log('resData', resData)
        this.toast.show(resData.message)
        this.input1.focus()
        var GetError = true
        this.setState({
          GetError: true
        })
      }

    }).catch(err => {
      console.log("🚀 ~ file: MainCheckPage.js ~ line 156 ~ MainCheckPage ~ err", err)
    })
  }

  requestData = (page, strhazy, allormy) => {
    const { pageNum, tabIndex, typeFocusButton } = this.state
    console.log("🚀 ~ file: mainCheckPage.js ~ line 156 ~ MainCheckPage ~ typeFocusButton", typeFocusButton)
    const { navigation } = this.props;
    text = navigation.getParam('text') || ''
    strhazy = strhazy.toString()

    let formData = new FormData();
    let data = {};

    if (typeFocusButton == 0) {
      if (mainpage == 'Main_CompReceive') {
        data = {
          "action": "initoutstoragelist",
          "servicetype": "projectsystem",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "factoryId": Url.Fidweb,
            "projectId": Url.ProjectId,
            "empId": Url.EmployeeId,
            "keyWord": strhazy || "",
            "pageIndex": 1,
            "count": 10
          }
        }
      }
      if (mainpage == 'Main_CompRepair') {
        data = {
          "action": "initcompmaintainlist",
          "servicetype": "projectsystem",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "factoryId": Url.Fidweb,
            "projectId": Url.ProjectId,
            "empId": Url.EmployeeId,
            "keyWord": "",
            "type": "apply",
            "isprj": "1",
            "pageIndex": 1,
            "count": 10
          }
        }
        if (page == '报修确认') {
          data = {
            "action": "initcomprepairquerylist",
            "servicetype": "projectsystem",
            "express": "E2BCEB1E",
            "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
            "data": {
              "factoryId": Url.Fidweb,
              "projectId": Url.ProjectId,
              "empId": Url.EmployeeId,
              "keyWord": "",
              "isprj": "1",
              "type": "repair",
              "pageIndex": 1,
              "count": 10
            }
          }
        }
      }
      if (mainpage == 'Main_CompMaintain') {
        data = {
          "action": "initcompmaintainquerylist",
          "servicetype": "projectsystem",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "factoryId": Url.Fidweb,
            "projectId": Url.ProjectId,
            "empId": Url.EmployeeId,
            "keyWord": strhazy || "",
            "type": "maintain",
            "isprj": "1",
            "pageIndex": 1,
            "count": 10
          }
        }
      }
    } else if (typeFocusButton == 1) {
      if (mainpage == 'Main_CompReceive') {
        data = {
          "action": "initcompconfirmquerylist",
          "servicetype": "projectsystem",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "empId": Url.EmployeeId,
            "projectId": Url.ProjectId,
            "factoryId": Url.Fidweb,
            "keyWord": strhazy || '',
            "pageIndex": 1,
            "count": 10
          }
        }
      }
      if (mainpage == 'Main_CompRepair') {
        data = {
          "action": "initcomprepairquerylist",
          "servicetype": "projectsystem",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "factoryId": Url.Fidweb,
            "projectId": Url.ProjectId,
            "empId": Url.EmployeeId,
            "keyWord": strhazy || "",
            "isprj": "1",
            "type": "apply",
            "pageIndex": 1,
            "count": 10
          }
        }
        if (page == '报修确认') {
          data = {
            "action": "initcomprepairquerylist",
            "servicetype": "projectsystem",
            "express": "E2BCEB1E",
            "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
            "data": {
              "factoryId": Url.Fidweb,
              "projectId": Url.ProjectId,
              "empId": Url.EmployeeId,
              "keyWord": "",
              "isprj": "1",
              "type": "confirm",
              "pageIndex": 1,
              "count": 10
            }
          }
        }
      }
      if (mainpage == 'Main_CompMaintain') {
        data = {
          "action": "initcompmaintainquerylist",
          "servicetype": "projectsystem",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "factoryId": Url.Fidweb,
            "projectId": Url.ProjectId,
            "empId": Url.EmployeeId,
            "keyWord": strhazy || "",
            "type": "confirm",
            "isprj": "1",
            "pageIndex": 1,
            "count": 10
          }
        }
      }
    } else {
      if (mainpage == 'Main_CompRepair') {
        data = {
          "action": "initcomprepairquerylist",
          "servicetype": "projectsystem",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "factoryId": Url.Fidweb,
            "projectId": Url.ProjectId,
            "empId": Url.EmployeeId,
            "keyWord": "",
            "isprj": "1",
            "type": "repair",
            "pageIndex": 1,
            "count": 10
          }
        }
      }
    }

    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: MainCheckPage.js ~ line 68 ~ MainCheckPage ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: compreceive.js ~ line 191 ~ MainCheckPage ~ resData", resData)
      if (resData.status == '100') {
        listData = resData.result
        if (listData.all) {
          data_All = listData.all
        } else {
          data_All = []
        }
        console.log("🚀 ~ file: mainCheckPage.js ~ line 307 ~ MainCheckPage ~ typeof listData.my", typeof listData.my)

        if (listData.my) {
          data_my = listData.my
        } else {
          data_my = []
        }
        console.log("🚀 ~ file: mainCheckPage.js ~ line 309 ~ MainCheckPage ~ data_All", data_All)

        this.setState({ data: data_All }, () => { this.forceUpdate() })

        this.setState({
          listData: listData,
          data_All: data_All,
          data_my: data_my,
          resData: resData,
          isLoading: false,
          pageLoading: false
        })
        console.log("🚀 ~ file: mainCheckPage.js ~ line 324 ~ MainCheckPage ~ data", this.state.data)

      } else {
        Alert.alert(resData.message)
        this.toast.show(resData.message)
      }
    }).catch((error) => {
      console.log("🚀 ~ file: MainCheckPage.js ~ line 254 ~ MainCheckPageAll ~ error", error)
    });

  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  //搜索栏搜索功能
  TextChange = (text) => {
    text = text.toString()
    this.setState({
      pageLoading: true,
      search: text
    })
    this.requestData(page, text, "")
    //this.requestData(page, text, "my")
  }

  //顶部记录与筛选按钮
  Top = () => {

  }

  isScan = () => {
    if (mainpage == 'Main_CompReceive' && pageType != "check") {
      this.setState({ isScanCode: true })
    }
    if (mainpage == 'Main_CompRepair' && pageType == "check") {
      this.setState({ isScanCode: true })
    }
  }

  selectListJudge = () => {
    if (mainpage == 'Main_CompRepair') {
      selectList = [
        { name: '报修', day: 'all' },
        { name: '已报修', day: 'all' },
        { name: '报修确认', day: 'all' },
      ]
    }
  }

  _panResponder = PanResponder.create({
    onStartShouldSetPanResponder: () => {
      //if (gs.dx < -100 || gs.dx > 100) {
      return false
      //}
    },
    onMoveShouldSetPanResponder: () => {
      //if (gs.dx < -100 || gs.dx > 100) {
      return true
      //}
    },
    onPanResponderGrant: () => {
      // console.log('开始移动：');
    },
    onPanResponderMove: (evt, gs) => {
      // console.log('正在移动：X轴：' + gs.dx + '，Y轴：' + gs.dy);
    },
    onPanResponderRelease: (event, gs) => {
      if (gs.dx < -100) {
        if (this.state.typeFocusButton == 0) {
          this.setState({ typeFocusButton: 1, pageLoading: true }, () => {
            this.requestData(page, "", "");
          })
        }
        if (selectList.length > 2) {
          if (this.state.typeFocusButton == 1) {
            this.setState({ typeFocusButton: 2, pageLoading: true }, () => {
              this.requestData(page, "", "");
            })
          }
        }

      }
      if (gs.dx > 100) {
        if (this.state.typeFocusButton == 1) {
          this.setState({ typeFocusButton: 0, pageLoading: true }, () => {
            this.requestData(page, "", "");
          })
        }
        if (this.state.typeFocusButton == 2) {
          this.setState({ typeFocusButton: 1, pageLoading: true }, () => {
            this.requestData(page, "", "");
          })
        }
      }
      console.log('结束移动：X轴移动了：' + gs.dx + '，Y轴移动了：' + gs.dy);
    }
  })

  _onResponderMove = (event) => {
    console.log("🚀 ~ file: mainCheckPage.js ~ line 389 ~ MainCheckPage ~ event", event)

  }

  _onScroll = (event) => {
    console.log("🚀 ~ file: mainCheckPage.js ~ line 430 ~ MainCheckPage ~ event", event)
    const offset_x = event.nativeEvent.contentOffset;
    console.log("🚀 ~ file: mainCheckPage.js ~ line 431 ~ MainCheckPage ~ offset_x", offset_x)
  }

  render() {
    const { isLoading, isRefreshing, pageLoading, search, resData, listData, data_All, data, tabs } = this.state

    let isCreate = false
    let isCreateView = <View></View>
    if (page == '构件报修' && this.state.typeFocusButton == 0) {
      isCreate = true
      isCreateView = <Main_CompRepair  {...this._panResponder.panHandlers} navigation={this.props.navigation} title={page} mainpage={mainpage} pageType={pageType} type={type} QRid={QRid} />
    }

    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            animating={true}
            color='#656FF6'
            size="large" />
        </View>
      )
      //渲染页面
    }
    else {
      return (
        <View style={{ flex: 1, backgroundColor: '#f9f9f9' }}
        >
          <NavigationEvents
            onWillFocus={this.UpdateControl} />
          <Toast ref={(ref) => { this.toast = ref; }} position="center"></Toast>
          <View style={{ flexDirection: 'row', backgroundColor: rgb(87, 101, 255, 0.1) }}>
            {
              selectList.map((item, index) => {
                return (
                  <Button
                    buttonStyle={{
                      borderRadius: -10,
                      borderBottomColor: this.state.typeFocusButton == index ? '#6777EF' : '#999997',
                      borderBottomWidth: 3,
                      // width: 48,
                      height: 50,
                      marginLeft: 25
                    }}
                    containerStyle={{
                      borderColor: this.state.typeFocusButton == index ? '#6777EF' : '#999997',
                    }}
                    title={item.name}
                    titleStyle={{
                      fontFamily: 'STHeitiSC-Light',
                      textAlign: 'center',
                      fontSize: 17,
                      fontWeight: "600",
                      //fontSize: RFT * 3.3,
                      color: this.state.typeFocusButton == index ? '#6777EF' : '#333333',
                    }}
                    type="clear"
                    onPress={() => {
                      this.setState({ typeFocusButton: index, pageLoading: true }, () => {
                        this.requestData(page, "", "");
                      })

                    }}
                  />
                )
              })
            }
          </View>
          <View style={{ flex: 1 }}>
            {
              isCreate ? <View></View> :
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around' }}>
                  <SearchBar
                    platform='android'
                    placeholder={'按编号进行搜索...'}
                    placeholderTextColor='#999'
                    // style={{ backgroundColor: 'red', borderColor: 'red', borderWidth: 10 }}
                    //containerStyle={{ flex: 1 }}
                    containerStyle={styles.searchContainerStyle}
                    inputContainerStyle={[styles.searchInputContainerStyle, !this.state.isScanCode ? { width: deviceWidth * 0.94 } : {}]}
                    inputStyle={{ color: '#333' }}
                    searchIcon={defaultSearchIcon()}
                    clearIcon={defaultClearIcon()}
                    cancelIcon={defaultSearchIcon()}
                    round={true}
                    value={search}
                    onChangeText={this.TextChange}
                    input={(input) => { this.setState({ search: input }) }} />
                  <View style={{ left: -20 }}>
                    {
                      this.state.isScanCode ?
                        <Icon onPress={() => {
                          if (mainpage == 'Main_CompReceive') {
                            this.props.navigation.navigate('XMCXCamera', {
                              pageType: pageType,
                              page: mainpage,
                              type: 'outStoGuid',
                              title: page,
                              isCheckPage: true
                            })
                          }
                          if (mainpage == 'Main_CompRepair' && pageType == "check") {
                            this.props.navigation.navigate('XMCXCamera', {
                              pageType: pageType,
                              page: mainpage,
                              type: 'checkCompId',
                              title: page,
                              isCheckPage: true
                            })
                          }
                        }} type='material-community' name='qrcode-scan' color='#656FF6' />
                        : <View></View>
                    }
                  </View>
                </View>
            }

            {
              this.state.pageLoading ? <View style={styles.loading}>
                <ActivityIndicator
                  animating={true}
                  color='#656FF6'
                  size="large" />
              </View> :
                isCreate ?
                  <View style={{ flex: 1 }} /* {...this._panResponder.panHandlers} */>
                    <Main_CompRepair navigation={this.props.navigation} title={page} mainpage={mainpage} pageType={pageType} type={type} QRid={QRid} />
                  </View>
                  :
                  <FlatList
                    style={{ flex: 1, backgroundColor: '#f8f8f8' }}
                    ref={(flatList) => this._flatList1 = flatList}
                    renderItem={this._renderItem}
                    onRefresh={this.refreshing}
                    refreshing={false}
                    showsVerticalScrollIndicator={false}
                    //控制上拉加载，两个平台需要区分开
                    onEndReachedThreshold={(Platform.OS === 'ios') ? -0.3 : 0.1}
                    onEndReached={() => {
                      if (data.length >= 5) {
                        this._onload("all")
                      }
                    }
                    }
                    //{...this._panResponder.panHandlers}
                    numColumns={1}
                    data={data}
                    scrollEnabled={true}
                    extraData={this.state}
                  />

            }

          </View>
        </View>

      )

    }
  }

  //接口返回的对象数组，长列表对数据遍历
  _renderItem = ({ item, index }) => {
    let mainNavList2 = ['Wantconcrete', 'ChangeLocation', 'EquipmentRepair', 'EquipmentService', 'PDASacnLoading', 'AdvanceStorage']
    //let CustomButton = <CustomButton title='维修完成' color='#17BC29' />
    const { typeFocusButton } = this.state
    if (mainpage == 'Main_CompReceive') {
      return (
        <Card containerStyle={{ borderRadius: 10 }}>
          <TouchableOpacity
            onPress={() => {
              if (typeFocusButton == 0) {
                let guid = item.outStoGuid.toString()
                this.props.navigation.navigate(mainpage, {
                  guid: guid,
                  pageType: pageType,
                  title: page,
                })
              } else {
                let guid = item._Rowguid.toString()
                this.props.navigation.navigate(mainpage, {
                  guid: guid,
                  pageType: "check",
                  title: page,
                })
              }
            }} >
            <View style={{ flexDirection: 'row', }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.formCode_CK}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> 送货：{item.deliveryNum}件</Text>
                </View>
                {/* <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text]} numberOfLines={1}> {item.noConfirmNum}</Text>
                   {
                    item.projectName == undefined ? <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.ProjectName.toString() || ""}</Text> :
                      <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.projectName.toString() || ""}</Text>
                  } 
  
                </View> */}
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textthi]} numberOfLines={1}> {item.receiveDate}</Text>
                  {this.state.typeFocusButton == 1 ? <Text style={[styles.text, styles.textright]} numberOfLines={1}> 未确认：{item.noConfirmNum}件</Text> : <View></View>}

                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    }
    if (mainpage == 'Main_CompRepair') {
      return (
        <Card containerStyle={{ borderRadius: 10 }}>
          <TouchableOpacity
            onPress={() => {
              if (typeFocusButton == 0) {
                if (page == '报修确认') {
                  let guid = item._Rowguid.toString()
                  this.props.navigation.navigate("Main_CompRepair_Confirm", {
                    guid: guid,
                    pageType: pageType,
                    title: page,
                  })
                  return
                }
                this.props.navigation.navigate(mainpage, {
                  guid: guid,
                  pageType: pageType,
                  title: page,
                })
              } else if (typeFocusButton == 1) {
                let guid = item._Rowguid.toString()
                this.props.navigation.navigate(mainpage, {
                  guid: guid,
                  pageType: "check",
                  title: page,
                })
              } else {
                if (item.repairState == "维修完成") {
                  let guid = item._Rowguid.toString()
                  this.props.navigation.navigate("Main_CompRepair_Confirm", {
                    guid: guid,
                    pageType: "operation",
                    title: page,
                  })
                  return
                }
                let guid = item._Rowguid.toString()
                this.props.navigation.navigate("Main_CompRepair_Confirm", {
                  guid: guid,
                  pageType: "check",
                  title: page,
                })
              }
            }} >
            <View style={{ flexDirection: 'row', }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.compCode}</Text>
                  {
                    item.repairState == "维修完成" ?
                      <CustomButton onPress={() => {
                        let guid = item._Rowguid.toString()
                        this.props.navigation.navigate("Main_CompRepair_Confirm", {
                          guid: guid,
                          pageType: "operation",
                          title: page,
                        })
                      }} title='确认' color='#656FF6' /> :
                      item.repairState == "报修申请" ?
                        <View style={[styles.badgeView, { backgroundColor: 'rgba(119,136,153, 0.22)', width: 100, }]}>
                          <Badge
                            badgeStyle={{ backgroundColor: '#778899' }}
                            containerStyle={{ marginRight: 8, top: 12 }}
                          />
                          <Text style={[styles.badgeText, { color: '#778899', fontWeight: 'bold' }]}>{item.repairState}</Text>
                        </View> :
                        <View style={[styles.badgeView, { backgroundColor: 'rgba(119,136,153, 0.22)', width: 80, }]}>
                          <Badge
                            badgeStyle={{ backgroundColor: '#778899' }}
                            containerStyle={{ marginRight: 8, top: 12 }}
                          />
                          <Text style={[styles.badgeText, { color: '#778899', fontWeight: 'bold' }]}>{item.repairState}</Text>
                        </View>
                  }
                </View>
                {/* <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text]} numberOfLines={1}> {item.noConfirmNum}</Text>
                   {
                    item.projectName == undefined ? <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.ProjectName.toString() || ""}</Text> :
                      <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.projectName.toString() || ""}</Text>
                  } 
  
                </View> */}
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textthi]} numberOfLines={1}> {item.formCode}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.applyDate}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    }

    if (mainpage == 'Main_CompMaintain') {
      return (
        <Card containerStyle={{ borderRadius: 10 }}>
          <TouchableOpacity
            onPress={() => {
              if (typeFocusButton == 0) {
                let guid = item._Rowguid.toString()
                this.props.navigation.navigate(mainpage, {
                  guid: guid,
                  pageType: pageType,
                  title: page,
                })
              } else {
                let guid = item._Rowguid.toString()
                this.props.navigation.navigate(mainpage, {
                  guid: guid,
                  pageType: 'check',
                  title: page,
                })
              }
            }} >
            <View style={{ flexDirection: 'row', }}>
              <View style={[styles.SN_View]}>
                <View style={[styles.SN_Text_View]}>
                  <Text style={[styles.SN_Text, styles.SN_Text_Comp]}>{(index + 1) + '.'}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'column', backgroundColor: '#FFFFFB', flex: 12, }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textfir]} numberOfLines={1}> {item.compCode}</Text>
                  {
                    item.maintainState == '已确认' ? <View style={[styles.badgeView, { backgroundColor: 'rgba(119,136,153, 0.22)', width: 80, }]}>
                      <Badge
                        badgeStyle={{ backgroundColor: '#778899' }}
                        containerStyle={{ marginRight: 8, top: 12 }}
                      />
                      <Text style={[styles.badgeText, { color: '#778899', fontWeight: 'bold' }]}>{item.maintainState}</Text>
                    </View> :
                      <CustomButton onPress={() => {
                        let guid = item._Rowguid.toString()
                        this.props.navigation.navigate(mainpage, {
                          guid: guid,
                          pageType: pageType,
                          title: page,
                        })
                      }} title='确认' color='#656FF6' />
                  }

                  {/* <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.maintainState}</Text> */}
                </View>
                {/* <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text]} numberOfLines={1}> {item.noConfirmNum}</Text>
                   {
                    item.projectName == undefined ? <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.ProjectName.toString() || ""}</Text> :
                      <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.projectName.toString() || ""}</Text>
                  } 
  
                </View> */}
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.text, styles.textthi]} numberOfLines={1}> {item.formCode}</Text>
                  <Text style={[styles.text, styles.textright]} numberOfLines={1}> {item.applyDate}</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Card>
      )
    }


  }

  refreshing = () => {
    this.toast.show('刷新中...', 1000)
    this.setState({
      pageLoading: true
    })
    const { search } = this.state
    let strhazy = search
    const { pageNum } = this.state
    let formData = new FormData();

    if (this.state.typeFocusButton == 0) {
      if (mainpage == 'Main_CompReceive') {
        data = {
          "action": "initoutstoragelist",
          "servicetype": "projectsystem",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "factoryId": Url.Fidweb,
            "projectId": Url.ProjectId,
            "empId": Url.EmployeeId,
            "keyWord": strhazy || "",
            "pageIndex": 1,
            "count": 10
          }
        }
      }
      if (mainpage == 'Main_CompRepair') {
        data = {
          "action": "initcompmaintainlist",
          "servicetype": "projectsystem",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "factoryId": Url.Fidweb,
            "projectId": Url.ProjectId,
            "empId": Url.EmployeeId,
            "keyWord": strhazy || "",
            "type": "apply",
            "isprj": "1",
            "pageIndex": 1,
            "count": 10
          }
        }
      }
      if (mainpage == 'Main_CompMaintain') {
        data = {
          "action": "initcompmaintainquerylist",
          "servicetype": "projectsystem",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "factoryId": Url.Fidweb,
            "projectId": Url.ProjectId,
            "empId": Url.EmployeeId,
            "keyWord": strhazy || "",
            "type": "maintain",
            "isprj": "1",
            "pageIndex": 1,
            "count": 10
          }
        }
      }
    } else if (this.state.typeFocusButton == 1) {
      if (mainpage == 'Main_CompReceive') {
        data = {
          "action": "initcompconfirmquerylist",
          "servicetype": "projectsystem",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "empId": Url.EmployeeId,
            "projectId": Url.ProjectId,
            "factoryId": Url.Fidweb,
            "keyWord": strhazy || '',
            "pageIndex": 1,
            "count": 10
          }
        }
      }
      if (mainpage == 'Main_CompRepair') {
        data = {
          "action": "initcomprepairquerylist",
          "servicetype": "projectsystem",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "factoryId": Url.Fidweb,
            "projectId": Url.ProjectId,
            "empId": Url.EmployeeId,
            "keyWord": strhazy || "",
            "isprj": "1",
            "type": "apply",
            "pageIndex": 1,
            "count": 10
          }
        }
      }
      if (mainpage == 'Main_CompMaintain') {
        data = {
          "action": "initcompmaintainquerylist",
          "servicetype": "projectsystem",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "factoryId": Url.Fidweb,
            "projectId": Url.ProjectId,
            "empId": Url.EmployeeId,
            "keyWord": strhazy || "",
            "type": "confirm",
            "isprj": "1",
            "pageIndex": 1,
            "count": 10
          }
        }
      }
    } else {
      if (mainpage == 'Main_CompRepair') {
        data = {
          "action": "initcomprepairquerylist",
          "servicetype": "projectsystem",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "factoryId": Url.Fidweb,
            "projectId": Url.ProjectId,
            "empId": Url.EmployeeId,
            "keyWord": "",
            "isprj": "1",
            "type": "repair",
            "pageIndex": 1,
            "count": 10
          }
        }
      }
    }

    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: MainCheckPage.js ~ line 68 ~ MainCheckPage ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: compreceive.js ~ line 191 ~ MainCheckPage ~ resData", resData)
      if (resData.status == '100') {
        listData = resData.result
        if (listData.all) {
          data_All = listData.all
        } else {
          data_All = []
        }
        if (listData.my) {
          data_my = listData.my
        } else {
          data_my = []
        }
        this.setState({ data: data_All })
        this.setState({
          listData: listData,
          data_All: data_All,
          data_my: data_my,
          resData: resData,
          isLoading: false,
          pageLoading: false
        })
        this.toast.show('刷新成功', 1000)
      } else {
        Alert.alert(resData.message)
        this.toast.show(resData.message)
      }
    }).catch((error) => {
      console.log("🚀 ~ file: MainCheckPage.js ~ line 254 ~ MainCheckPageAll ~ error", error)
    });
  }

  _onload = (user) => {

    const { search } = this.state
    let strhazy = search
    this.toast.show('加载中...', 1000)
    pageNum += 1
    let formData = new FormData();
    let data = {};
    if (this.state.typeFocusButton == 0) {
      if (mainpage == 'Main_CompReceive') {
        data = {
          "action": "initoutstoragelist",
          "servicetype": "projectsystem",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "factoryId": Url.Fidweb,
            "projectId": Url.ProjectId,
            "empId": Url.EmployeeId,
            "keyWord": strhazy || "",
            "pageIndex": pageNum,
            "count": 10
          }
        }
      }
      if (mainpage == 'Main_CompRepair') {
        data = {
          "action": "initcompmaintainlist",
          "servicetype": "projectsystem",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "factoryId": Url.Fidweb,
            "projectId": Url.ProjectId,
            "empId": Url.EmployeeId,
            "keyWord": strhazy || "",
            "type": "apply",
            "isprj": "1",
            "pageIndex": pageNum,
            "count": 10
          }
        }
      }
      if (mainpage == 'Main_CompMaintain') {
        data = {
          "action": "initcompmaintainquerylist",
          "servicetype": "projectsystem",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "factoryId": Url.Fidweb,
            "projectId": Url.ProjectId,
            "empId": Url.EmployeeId,
            "keyWord": strhazy || "",
            "type": "maintain",
            "isprj": "1",
            "pageIndex": pageNum,
            "count": 10
          }
        }
      }
    } else if (this.state.typeFocusButton == 1) {
      if (mainpage == 'Main_CompReceive') {
        data = {
          "action": "initcompconfirmquerylist",
          "servicetype": "projectsystem",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "empId": Url.EmployeeId,
            "projectId": Url.ProjectId,
            "factoryId": Url.Fidweb,
            "keyWord": strhazy || '',
            "pageIndex": pageNum,
            "count": 10
          }
        }
      }
      if (mainpage == 'Main_CompRepair') {
        data = {
          "action": "initcomprepairquerylist",
          "servicetype": "projectsystem",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "factoryId": Url.Fidweb,
            "projectId": Url.ProjectId,
            "empId": Url.EmployeeId,
            "keyWord": strhazy || "",
            "isprj": "1",
            "type": "apply",
            "pageIndex": pageNum,
            "count": 10
          }
        }
      }
      if (mainpage == 'Main_CompMaintain') {
        data = {
          "action": "initcompmaintainquerylist",
          "servicetype": "projectsystem",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "factoryId": Url.Fidweb,
            "projectId": Url.ProjectId,
            "empId": Url.EmployeeId,
            "keyWord": strhazy || "",
            "type": "confirm",
            "isprj": "1",
            "pageIndex": pageNum,
            "count": 10
          }
        }
      }

    } else {
      if (mainpage == 'Main_CompRepair') {
        data = {
          "action": "initcomprepairquerylist",
          "servicetype": "projectsystem",
          "express": "E2BCEB1E",
          "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
          "data": {
            "factoryId": Url.Fidweb,
            "projectId": Url.ProjectId,
            "empId": Url.EmployeeId,
            "keyWord": strhazy || "",
            "isprj": "1",
            "type": "repair",
            "pageIndex": pageNum,
            "count": 10
          }
        }
      }
    }

    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: MainCheckPage.js ~ line 282 ~ MainCheckPage ~ _onload", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: MainCheckPage.js ~ line 292 ~ MainCheckPage ~ _onload", resData)

      if (resData.status == '100') {
        let listDataLoad = resData.result

        let data_AllLoad = listDataLoad.all
        let data_myLoad = listDataLoad.my
        if (data_AllLoad.length > 0) {
          data_All.push.apply(data_All, data_AllLoad)
        }
        else if (data_myLoad.length > 0) {
          data_my.push.apply(data_my, data_myLoad)
        }
        this.setState({ data: data_All })
        this.setState({
          listData: listData,
          data_All: data_All,
          data_my: data_my,
          resData: resData,
          isLoading: false,
          pageLoading: false
        })
        if (data_AllLoad.length > 0 || data_myLoad.length > 0) {
          this.toast.show('加载完成', 2000);
        } else {
          this.toast.show('无更多数据', 2000);
        }
      } else if (resData.status != '100') {
        this.toast.show(resData.message)
      } else {
        this.toast.show('加载失败')
      }

    }).catch((error) => {
      console.log("🚀 ~ file: MainCheckPage.js ~ line 365 ~ MainCheckPage ~ error", error)
    });


  }

}

//export default XMCXTopBottom

const styles = StyleSheet.create({
  loading: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',

  },
  contrain: {
    flexDirection: 'column',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  txt: {

    fontSize: 16,
    marginBottom: 8,
    marginTop: 8,
    marginHorizontal: 8
  },
  text: {
    fontSize: 14,
    marginBottom: 5,
    marginTop: 5,
    flex: 1
  },
  textright: {
    textAlign: 'right',
    color: 'gray'
  },
  textfir: {
    flex: 1.8,
    fontSize: 18,
    //backgroundColor: 'red'
  },
  textthi: {
    fontSize: 15
  },
  textname: {
    fontSize: 14,
    marginBottom: 8,
    marginTop: 8,
    color: 'gray'
  },
  textView: {

    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  hidden: {
    flex: 0,
    height: 0,
    width: 0,
  },
  searchContainerStyle:
  {
    shadowColor: '#999', shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.3,
    shadowRadius: 6,
    backgroundColor: 'transparent',
    flex: 6
  },
  searchInputContainerStyle: {
    backgroundColor: 'white',
    width: deviceWidth * 0.82,
    marginLeft: deviceWidth * 0.03,
    borderRadius: 45
  },
  SN_View: {
    flex: 1
  },
  SN_Text_View: {
    top: 7
  },
  SN_Text: {
    textAlignVertical: 'center',
    fontSize: 18,
    color: '#535c68'
  },
  SN_Text_Comp: {
    fontSize: 16,
  },
  badgeText: {
    fontSize: 14,
    //marginBottom: 4,
    marginTop: 6,
  },
  badgeView: {
    height: 32,
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    borderRadius: 20,
    //flex: 1
  },
  textView: {
    textAlignVertical: 'center',
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
})


