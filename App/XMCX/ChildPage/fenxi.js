import React, { Component } from 'react';
import { View, Text, ActivityIndicator, ScrollView } from 'react-native';
import { Card, ListItem, Badge, Icon } from 'react-native-elements';
import commonStyle from '../Components/commonStyle';
import ListItemTwoThree from '../Components/ListItemTwoThree';
import { Echarts, echarts } from 'react-native-secharts';
import { color, deviceWidth, RFT } from '../../Url/Pixal';
import Url from '../../Url/Url';
import { styles } from '../Components/XMCXStyles';

export default class fenxi extends Component {
  constructor(props) {
    super(props);
    this.state = {
      chartData: {},
      chartLoading: true,
      ProduceOutYear: {},
      ProduceOutYearList: []
    };
  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  componentDidMount() {
    this.GetFacPrjProduceOutYear()
  }

  GetFacPrjProduceOutYear = () => {
    //const url = Url.url + Url.project + Url.Fid + Url.ProjectId + '/GetProjectComponentStatistic' + day;

    let formData = new FormData();
    let data = {};

    let Name = [],
      proVolume = [],
      storageVolume = [],
      outVolume = [],
      count = 0,
      ProduceOutYear = {};
    ProduceOutYearList = [];

    data = {
      "action": "getfacprjproduceoutyear",
      "servicetype": "projectsystem",
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        "factoryId": Url.Fidweb,
        "projectId": Url.ProjectId,
        "isprj": "1"
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: fenxi.js ~ line 59 ~ fenxi ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: fenxi.js ~ line 69 ~ fenxi ~ resData", resData)
      if (resData.status == '100') {
        let result = resData.result.data
        result.map((item, index) => {
          Name.push(item.yearmonth);
          proVolume.push(item.provolume);
          storageVolume.push(item.storagevolume);
          outVolume.push(item.outvolume);

          let tmp = {}
          tmp.color = color[count++]
          tmp.name = item.yearmonth
          tmp.volume1 = item.provolume//.toFixed(2)
          tmp.volume2 = item.outvolume//.toFixed(2)
          ProduceOutYearList.push(tmp)
        })
        ProduceOutYear.Name = Name;
        ProduceOutYear.proVolume = proVolume;
        ProduceOutYear.storageVolume = storageVolume;
        ProduceOutYear.outVolume = outVolume;
        this.setState({
          ProduceOutYear: ProduceOutYear,
          ProduceOutYearList: ProduceOutYearList,
          chartLoading: false,
        })
      } else {
        Alert.alert(resData.message)
        this.toast.show(resData.message)
      }
    }).catch((error) => {
      console.log("🚀 ~ file: fenxi.js ~ line 98 ~ fenxi ~ error", error)
    });
  }

  GetProjectCompTypeStockSaturation = () => {
    const url = Url.url + Url.project + Url.Fid + '/' + Url.ProjectId + '/GetFacPrjProduceOutYear';
    console.log("🚀 ~ file: fenxi.js MainXMCXScreen ~ line 99 ~ fenxi ~ url", url)
    let Name = [],
      proVolume = [],
      storageVolume = [],
      outVolume = [],
      count = 0,
      ProduceOutYear = {};
    ProduceOutYearList = [];
    fetch(url).then(res => {
      return res.json()
    }).then(resData => {
      console.log("🚀 ~ file: main.js ~ line 50 ~ MainXMCXScreen ~ fetch ~ resData", resData)
      resData.map((item, index) => {
        Name.push(item.yearMonth);
        proVolume.push(item.proVolume);
        storageVolume.push(item.storageVolume);
        outVolume.push(item.outVolume);

        let tmp = {}
        tmp.color = color[count++]
        tmp.name = item.yearMonth
        tmp.volume1 = item.proVolume//.toFixed(2)
        tmp.volume2 = item.outVolume//.toFixed(2)
        ProduceOutYearList.push(tmp)
      })
      ProduceOutYear.Name = Name;
      ProduceOutYear.proVolume = proVolume;
      ProduceOutYear.storageVolume = storageVolume;
      ProduceOutYear.outVolume = outVolume;
      console.log("🚀 ~ file: fenxi.js ~ line 119 ~ fenxi ~ fetch ~ ProduceOutYear", ProduceOutYear)
      this.setState({
        ProduceOutYear: ProduceOutYear,
        ProduceOutYearList: ProduceOutYearList,
        chartLoading: false,
      })
    }).catch((error) => {
      console.log(error);
    });
  }

  render() {
    const { ProduceOutYear, ProduceOutYearList, chartLoading } = this.state
    return (
      <View style={{ flex: 1, backgroundColor: "#f9f9f9" }}>
        <ScrollView>
          <View style={commonStyle.blankView}></View>
          <View >
            <View style={[commonStyle.row, commonStyle.viewLeft]}>
              <View style={[commonStyle.mainPurpleColor, commonStyle.titleBadge]}></View>
              <Text style={[commonStyle.titleText]}>月生产与发货分析</Text>
            </View>
          </View>
          <View>
            {
              chartLoading ?
                <View style={{ height: 300 }}>
                  <View style={styles.loading}>
                    <ActivityIndicator
                      animating={true}
                      color='#656FF6'
                      size="large" />
                  </View>
                </View> : <Card containerStyle={commonStyle.card_containerStyle}>
                  <Echarts
                    //width={deviceWidth * 0.93}
                    height={300}
                    renderLoading={() => {
                      <View style={{
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: '#F5FCFF',
                      }}>
                        <ActivityIndicator
                          animating={true}
                          color='#419F'
                          size="large" />
                      </View>
                    }}
                    option={{
                      tooltip: {
                        trigger: 'axis',
                        axisPointer: {
                          crossStyle: {
                            color: '#999'
                          }
                        }
                      },

                      legend: {
                        data: ['生产量', '发货量'],
                        show: true,
                        top: '5%',
                      },
                      xAxis: [
                        {
                          show: true,
                          type: 'category',
                          data: this.state.ProduceOutYear.Name,
                          axisTick: {
                            alignWithLabel: true
                          },
                          axisLine: {
                            show: true,
                            lineStyle: {
                              color: 'gray'
                            }
                          },
                          axisLabel: {
                            color: '#333'
                          }
                        }
                      ],
                      yAxis: [
                        /* {
                          show: false,
                          type: 'value',
                          name: '数量',
                          min: 0,
                          //interval: 1,
                          axisLabel: {
                            formatter: (value) => {
                              if (value >= 1000 && value < 10000) {
                                value = value / 1000 + "k";
                              }
                              return value;
                            }
                          },
                        }, */
                        {
                          show: true,
                          type: 'value',
                          name: 'm³',
                          min: 0,
                          // interval: 1,
                          axisLine: {
                            show: true,
                            lineStyle: {
                              color: 'gray'
                            },
                            formatter: (value) => {
                              if (value >= 1000 && value < 100000) {
                                value = value / 1000 + "k";
                              }
                              return value;
                            }
                          },
                          axisLabel: {
                            color: '#333'
                          },
                          splitLine: {
                            show: false
                          },
                          splitArea: {
                            show: true,
                            interval: 1,
                            areaStyle: {
                              color: ['white', '#f9f9f9']
                            }
                          },
                        }
                      ],
                      grid: {
                        top: '25%',
                        left: '10%',
                        right: '5%',
                        bottom: '13%'
                      },

                      series: [
                        /* {
                          name: '数量',
                          type: 'bar',
                          data: chartData.Num,
                          //barMaxWidth: chartData.Num.length <= 6 && '28%'
                          barMaxWidth: chartData.Num.length > 13 && chartData.Num.length < 20 ? chartData.Num.length - 5 : ((chartData.Num.length < 6 ? chartData.Num.length + 10 : chartData.Num.length + 5))
                        }, */

                        {
                          name: '生产量',
                          type: 'line',
                          symbol: 'none',
                          smooth: true,
                          //yAxisIndex: 1,
                          data: this.state.ProduceOutYear.proVolume,
                          areaStyle: {
                            opacity: 0.7,
                            color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                              {
                                offset: 0,
                                color: '#00EA9F'
                              },
                              {
                                offset: 1,
                                color: 'white'
                              }
                            ])
                          },
                          itemStyle: {
                            // color: '#F88932'
                          }
                        },
                        {
                          name: '发货量',
                          type: 'line',
                          symbol: 'none',
                          smooth: true,
                          //yAxisIndex: 1,
                          data: this.state.ProduceOutYear.outVolume,
                          areaStyle: {
                            opacity: 0.7,
                            color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                              {
                                offset: 0,
                                color: '#229DFF'
                              },
                              {
                                offset: 1,
                                color: 'white'
                              }
                            ])
                          },
                          itemStyle: {
                            // color: '#F88932'
                          }
                        }
                      ],
                      color: ['#00EA9F', '#229DFF'],

                    }} />
                </Card>}

          </View>
          <View style={commonStyle.blankView}></View>
          <View >
            <View style={[commonStyle.row, commonStyle.viewLeft]}>
              <View style={[commonStyle.mainPurpleColor, commonStyle.titleBadge]}></View>
              <Text style={[commonStyle.titleText]}>月生产与发货统计</Text>
            </View>
          </View>
          <View style={commonStyle.blankView}></View>

          {
            chartLoading ?
              <View style={{ height: 300 }}>
                <View style={styles.loading}>
                  <ActivityIndicator
                    animating={true}
                    color='#656FF6'
                    size="large" />
                </View>
              </View> :
              <View style={commonStyle.View_ListItem}>
                <ListItemTwoThree color={'transparent'} titlename="统计月份" title={"生产量"} righttitle={"发货量"} />
                {
                  ProduceOutYearList.map((item, index) => {
                    return (
                      <ListItemTwoThree
                        color={item.color}
                        titlename={item.name}
                        title={item.volume1 + 'm³'}
                        righttitle={item.volume2 + 'm³'}
                        bottomDivider={(index + 1) == ProduceOutYearList.length ? false : true} />
                    )
                  })
                }
              </View>
          }
          <View style={commonStyle.blankView}></View>
        </ScrollView>
      </View>
    );
  }
}
