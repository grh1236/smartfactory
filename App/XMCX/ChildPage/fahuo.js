import React, { Component } from 'react';
import { View, Text, ActivityIndicator, ScrollView } from 'react-native';
import { Card, ListItem, Badge, Icon } from 'react-native-elements';
import commonStyle from '../Components/commonStyle';
import ListItemTwoThree from '../Components/ListItemTwoThree';
import { Echarts, echarts } from 'react-native-secharts';
import { color, deviceWidth, RFT } from '../../Url/Pixal';
import Url from '../../Url/Url';
import { styles } from '../Components/XMCXStyles';

export default class fahuo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ProcessAnaysis: [],
      ProcessAnaysisList: [],
      chartData: {},
      chart1Loading: true,
    };
  }

  //顶栏
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')
    }
  }

  componentDidMount() {
    this.GetFacProjectProcessAnaysis()
  }

  GetFacProjectProcessAnaysis = () => {
    //const url = Url.url + Url.project + Url.Fid + Url.ProjectId + '/GetProjectComponentStatistic' + day;

    let formData = new FormData();
    let data = {};

    let Name = [],
      preVolumeArr = [],
      proVolumeArr = [],
      probabilityArr = [],
      outVolumeArr = [],
      count = 0,
      ProcessAnaysis = {},
      ProcessAnaysisList = [];

    data = {
      "action": "getfacprojectprocessanaysis",
      "servicetype": "projectsystem",
      "express": "E2BCEB1E",
      "ciphertext": "077d106a940be035e6d80eb61a1a01c3",
      "data": {
        "factoryId": Url.Fidweb,
        "projectId": Url.ProjectId,
        "isprj": "1"
      }
    }
    formData.append('jsonParam', JSON.stringify(data))
    console.log("🚀 ~ file: shengchan.js ~ line 152 ~ shengchan ~ formData", formData)
    fetch(Url.PDAurl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }).then(res => {
      return res.json();
    }).then(resData => {
      console.log("🚀 ~ file: shengchan.js ~ line 162 ~ shengchan ~ resData", resData)
      if (resData.status == '100') {
        let result = resData.result.data
        result.map((item, index) => {
          item.color = color[count++]
          Name.push(item.comptypename)
          preVolumeArr.push(item.prevolume)
          outVolumeArr.push(item.outvolume)
          let probability = item.outvolume / (item.prevolume == 0 ? 1 : item.prevolume)
          probability = probability * 100
          probabilityArr.push(probability.toFixed(0))

          let tmp = {}
          tmp.color = item.color
          tmp.name = item.comptypename
          tmp.volume1 = item.prevolume//.toFixed(2)
          tmp.volume2 = item.outvolume//.toFixed(2)
          tmp.probability = probability.toFixed(0)
          ProcessAnaysisList.push(tmp)
        })
        ProcessAnaysis.Name = Name
        ProcessAnaysis.preVolume = preVolumeArr
        ProcessAnaysis.outVolume = outVolumeArr
        ProcessAnaysis.probability = probabilityArr
        console.log("🚀 ~ file: shengchan.js ~ line 84 ~ shengchan ~ fetch ~ ProcessAnaysis", ProcessAnaysis)
        console.log("🚀 ~ file: fahuo.js ~ line 59 ~ fahuo ~ resData.map ~ ProcessAnaysisList", ProcessAnaysisList)
        this.setState({
          ProcessAnaysis: ProcessAnaysis,
          ProcessAnaysisList: ProcessAnaysisList,
          chart1Loading: false
        })
      } else {
        Alert.alert(resData.message)
        this.toast.show(resData.message)
      }
    }).catch((error) => {
      console.log("🚀 ~ file: shengchan.js ~ line 105 ~ shengchan ~ error", error)
    });
  }

  GetFacProjectProcessAnaysis_old = () => {
    const url = Url.url + Url.project + Url.Fid + '/' + Url.ProjectId + '/GetFacProjectProcessAnaysis'
    let Name = [],
      preVolumeArr = [],
      proVolumeArr = [],
      probabilityArr = [],
      outVolumeArr = [],
      count = 0,
      ProcessAnaysis = {},
      ProcessAnaysisList = [];
    fetch(url).then(res => {
      return res.json()
    }).then(resData => {
      console.log("🚀 ~ file: fahuo.js ~ line 44 ~ fahuo ~ fetch ~ resData", resData)
      resData.map((item, index) => {
        item.color = color[count++]
        Name.push(item.compTypeName)
        preVolumeArr.push(item.preVolume)
        outVolumeArr.push(item.outVolume)
        let probability = item.outVolume / (item.preVolume == 0 ? 1 : item.preVolume)
        probability = probability * 100
        probabilityArr.push(probability.toFixed(0))

        let tmp = {}
        tmp.color = item.color
        tmp.name = item.compTypeName
        tmp.volume1 = item.preVolume//.toFixed(2)
        tmp.volume2 = item.outVolume//.toFixed(2)
        tmp.probability = probability.toFixed(0)
        ProcessAnaysisList.push(tmp)
      })
      ProcessAnaysis.Name = Name
      ProcessAnaysis.preVolume = preVolumeArr
      ProcessAnaysis.outVolume = outVolumeArr
      ProcessAnaysis.probability = probabilityArr
      console.log("🚀 ~ file: shengchan.js ~ line 84 ~ shengchan ~ fetch ~ ProcessAnaysis", ProcessAnaysis)
      console.log("🚀 ~ file: fahuo.js ~ line 59 ~ fahuo ~ resData.map ~ ProcessAnaysisList", ProcessAnaysisList)
      this.setState({
        ProcessAnaysis: ProcessAnaysis,
        ProcessAnaysisList: ProcessAnaysisList,
        chart1Loading: false
      })
    }).catch((error) => {
      console.log(error);
    });
  }

  render() {
    const { ProcessAnaysis, ProcessAnaysisList, chart1Loading } = this.state
    return (
      <View style={{ flex: 1, backgroundColor: "#f9f9f9" }}>
        <ScrollView>
          <View style={commonStyle.blankView}></View>
          <View >
            <View style={[commonStyle.row, commonStyle.viewLeft]}>
              <View style={[commonStyle.mainPurpleColor, commonStyle.titleBadge]}></View>
              <Text style={[commonStyle.titleText]}>发货情况分析</Text>
            </View>
          </View>
          {
            chart1Loading ?
              <View style={{ height: 300 }}>
                <View style={styles.loading}>
                  <ActivityIndicator
                    animating={true}
                    color='#656FF6'
                    size="large" />
                </View>
              </View>
              :
              <View>
                <Card containerStyle={commonStyle.card_containerStyle}>
                  <Echarts
                    //width={deviceWidth * 0.93}
                    height={300}
                    renderLoading={() => {
                      <View style={{
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: '#F5FCFF',
                      }}>
                        <ActivityIndicator
                          animating={true}
                          color='#419F'
                          size="large" />
                      </View>
                    }}
                    option={{
                      tooltip: {
                        trigger: 'axis',
                        axisPointer: {
                          crossStyle: {
                            color: '#999'
                          }
                        }
                      },

                      legend: {
                        data: ['合同量', '发货量', '发货率'],
                        show: true,
                        top: '5%',
                      },
                      xAxis: [
                        {
                          show: true,
                          type: 'category',
                          data: ProcessAnaysis.Name,
                          label: {
                            show: false,
                            backgroundColor: '#004E52'
                          },
                          axisLine: {

                          },
                          axisPointer: {
                            snap: false,
                            type: 'shadow',
                            handle: {
                              show: false,
                              color: '#004E52'
                            },
                          }
                        }
                      ],
                      yAxis: [
                        /* {
                          show: false,
                          type: 'value',
                          name: '数量',
                          min: 0,
                          //interval: 1,
                          axisLabel: {
                            formatter: (value) => {
                              if (value >= 1000 && value < 10000) {
                                value = value / 1000 + "k";
                              }
                              return value;
                            }
                          },
                        }, */
                        {
                          show: true,
                          type: 'value',
                          name: 'm³',
                          min: 0,
                          // interval: 1,
                          axisLabel: {
                            formatter: (value) => {
                              if (value >= 1000 && value < 100000) {
                                value = value / 1000 + "k";
                              }
                              return value;
                            }
                          },
                          splitLine: {
                            show: false
                          },
                          splitArea: {
                            show: true,
                            interval: 1,
                            areaStyle: {
                              color: ['white', '#f9f9f9']
                            }
                          },
                        }, {
                          show: true,
                          type: 'value',
                          name: '发货率：%',
                          min: 0,
                          // interval: 1,
                          splitLine: {
                            show: false
                          }
                        }
                      ],
                      grid: {
                        top: '25%',
                        left: '10%',
                        right: '10%',
                        bottom: '13%'
                      },

                      series: [
                        /* {
                          name: '数量',
                          type: 'bar',
                          data: chartData.Num,
                          //barMaxWidth: chartData.Num.length <= 6 && '28%'
                          barMaxWidth: chartData.Num.length > 13 && chartData.Num.length < 20 ? chartData.Num.length - 5 : ((chartData.Num.length < 6 ? chartData.Num.length + 10 : chartData.Num.length + 5))
                        }, */
                        {
                          name: '合同量',
                          type: 'bar',
                          //yAxisIndex: 1,
                          data: ProcessAnaysis.preVolume,
                          barMaxWidth: 10,
                          itemStyle: {
                            color: '#5765FF'
                          }
                        },
                        {
                          name: '发货量',
                          type: 'bar',
                          //yAxisIndex: 1,
                          data: ProcessAnaysis.outVolume,
                          barMaxWidth: 10,
                          itemStyle: {
                            color: '#22D190'
                          }
                        },
                        {
                          name: '发货率',
                          type: 'line',
                          symbol: 'none',
                          yAxisIndex: 1,
                          data: ProcessAnaysis.probability,
                          barMaxWidth: 10,
                          itemStyle: {
                            color: '#F88932'
                          }
                        }
                      ],
                      //color: ['#2ecc71', '#f1c40f'],

                    }} />
                </Card>
              </View>}
          <View style={commonStyle.blankView}></View>
          <View >
            <View style={[commonStyle.row, commonStyle.viewLeft]}>
              <View style={[commonStyle.mainPurpleColor, commonStyle.titleBadge]}></View>
              <Text style={[commonStyle.titleText]}>发货情况统计</Text>
            </View>
          </View>
          <View style={commonStyle.blankView}></View>
          {
            chart1Loading ?
              <View style={{ height: 300 }}>
                <View style={styles.loading}>
                  <ActivityIndicator
                    animating={true}
                    color='#656FF6'
                    size="large" />
                </View>
              </View> :
              <View style={commonStyle.View_ListItem}>
                <ListItemTwoThree color={'transparent'} titlename="构件类型" title={"合同量"} righttitle={"发货量"} />
                {
                  ProcessAnaysisList.map((item, index) => {
                    return (
                      <ListItemTwoThree
                        color={item.color}
                        titlename={item.name}
                        title={item.volume1 + 'm³'}
                        righttitle={item.volume2 + 'm³'}
                        bottomDivider={(index + 1) == ProcessAnaysisList.length ? false : true} />
                    )
                  })
                }
              </View>
          }
        </ScrollView>
      </View>
    );
  }
}