import React from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, CheckBox, Avatar, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, ListItem, BottomSheet } from 'react-native-elements';

const { height, width } = Dimensions.get('window') //获取宽高

export default class CheckBoxScan extends React.Component {
  render() {
    const { title, checked, containerStyle } = this.props
    return (
      <CheckBox
      {...this.props}
      containerStyle={[containerStyle,{ backgroundColor: 'transparent', borderColor: 'transparent', padding: 0}]}
      title= {title || ''}
      //uncheckedIcon={<Image source={require('../img/unchecked.png')} style={{ height: 16, width: 16 }} resizeMode='contain'></Image>}
      checkedIcon={<Icon  name='check-box' color='#656FF6'></Icon>}
      uncheckedIcon={<Icon  name='check-box-outline-blank' color='#656FF6'></Icon>}
      checked={checked}
      size={14}
    />

    )
  }
}

