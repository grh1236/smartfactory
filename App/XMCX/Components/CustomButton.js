import React from 'react';
import { Button, Icon } from 'react-native-elements';

export default class ScanButton extends React.Component {
  render() {
    return (
      <Button
        {...this.props}
        titleStyle = {{fontSize: 14}}
        //iconRight={true}
        //icon={<Icon type='antdesign' name='scan1' color='white' size={14} />}
        type='solid'
        buttonStyle={{ paddingVertical: 6, backgroundColor: this.props.color, marginVertical: 0 }} />
    )
  }
}