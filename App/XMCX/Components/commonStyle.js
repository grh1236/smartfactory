import { StyleSheet } from 'react-native';
import { RFT, deviceWidth, deviceHeight } from '../../Url/Pixal';

export default styles = StyleSheet.create({
  blankView: {
    height: 20
  },
  row: {
    flexDirection: 'row',
    //marginBottom: 15
  },
  viewLeft: {
    marginLeft: 20
  },
  mainPurpleColor: {
    backgroundColor: '#6777EF'
  },
  titleBadge: {
    width: 4,
    height: 20,
    top: 2,
    marginRight: 12
  },
  titleText: {
    fontWeight: 'bold',
    fontSize: 18
  },
  main_cardContentView: {
    flexDirection: 'row',
    marginTop: RFT * 7.5
  },
  main_cardContent_textView: {
    left: RFT * 8
  },
  main_cardContent_text: {
    color: "white",
    fontWeight: 'bold',
    fontSize: 14
  },
  main_cardContent_imageContainerStyle: {
    top: -(RFT * 1.5),
    left: RFT * 20.5
  },
  main_cardContent_imageStyle: {
    width: RFT * 9,
    height: RFT * 9
  },
  main_cardContentNumView: {
    left: RFT * 8,
    top: -(RFT * 3),
    flexDirection: 'row'
  },
  main_cardContentNum1: {
    color: "white",
    fontWeight: 'bold',
    fontSize: 23,
    fontFamily: 'Arial-BoldMT'
  },
  main_cardContentNum2: {
    color: "white",
    fontSize: 14,
    top: 8,
    left: 8
  },
  main_FuncList: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginLeft: 14
  },
  main_FuncItemView: {
    width: deviceWidth / 3.3,
    height: deviceWidth / 3,
    justifyContent: 'center',
    alignContent: 'center'
  },
  main_FuncItemImageStyle: {
    width: deviceWidth / 5.5,
    height: deviceWidth / 5.5,
    marginLeft: deviceWidth / 16,
    marginBottom: 10
  },
  main_FuncItemText: {
    textAlign: 'center',
    fontSize: 16
  },
  card_containerStyle: {
    borderRadius: 5,
    padding: 0,
    shadowOpacity: 0,
    borderWidth: 0,
    elevation: 0,
  },
  ListItem_title: {
    fontSize: RFT * 3.5,
    textAlignVertical: "center",
    color: '#454545',
  },
  View_ListItem: {
    flex: 1,
    width: deviceWidth * 0.94,
    marginLeft: deviceWidth * 0.03,
    borderRadius: 10,
    backgroundColor: 'white'
  }
});