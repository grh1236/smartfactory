
import React, { Component } from 'react';
import { View, Text, StyleSheet, processColor, ActivityIndicator } from 'react-native';

import { CombinedChart } from 'react-native-charts-wrapper';
import { Echarts, echarts } from 'react-native-secharts';
import { RFT, deviceWidth, color } from '../../Url/Pixal';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
    backgroundColor: 'transparent'
  },
  text: {
    margin: 4,
    color: 'gray',
    fontSize: RFT * 2.6
  }
});

let accont = '\t' + '数量'

export default class PieChart extends Component {

  constructor() {
    super();
    this.state = {
      legend: {
        enabled: true,
        textSize: RFT * 3,
        form: "SQUARE",
        //formSize: RFT * 4,
        xEntrySpace: 10,
        yEntrySpace: 5,
        wordWrapEnabled: true,
        horizontalAlignment: 'RIGHT',
        verticalAlignment: 'TOP',
      },
      xAxis: {
        valueFormatter: [1, 2, 3, 4, 5, 6, 7],
        granularityEnabled: true,
        granularity: 1,
        axisMaximum: 12,
        axisMinimum: -0.5,
        drawAxisLine: false,
        centerAxisLabels: false,
        setDrawGirdLines: false,
        drawGridLines: false,
        gridLineWidth: 1,
        position: "BOTTOM",
      },
      yAxis: {
        left: {
          granularityEnabled: true,
          granularity: 1,
          axisMinimum: 0,
          // drawAxisLine: false,
          drawGridLines: false,
          //valueFormatterPattern: "件",
          // spaceBottom: 0
        },
        right: {
          granularityEnabled: true,
          granularity: 1,
          axisMinimum: 0,
          //drawAxisLine: false,
          //drawGridLines: false,
          //valueFormatterPattern: "#.#'%'",
          //spaceBottom: 0
        }
      },
      marker: {
        enabled: true,
        markerColor: processColor('#999999'),
        textColor: processColor('white'),
        markerFontSize: 14,
        digits: 4,
      },

      chartData: {},
      chartLoading: true,
    };

  }

  componentDidMount() {
    // in this example, there are line, bar, candle, scatter, bubble in this combined chart.
    // according to MpAndroidChart, the default data sequence is line, bar, scatter, candle, bubble.
    // so 4 should be used as dataIndex to highlight bubble data.

    // if there is only bar, bubble in this combined chart.
    // 1 should be used as dataIndex to highlight bubble data.

    this.setState({ ...this.state, highlights: [{ x: 1, y: 150, dataIndex: 4 }, { x: 2, y: 106, dataIndex: 4 }] })
  }

  setStateLoaing = () => {
    this.setState({ chartLoading: true })
  }

  ReFlash = (propsData) => {
    this.setState({
      chartData: propsData,
      chartLoading: false
    });

  }

  static displayName = 'Combined';

  handleSelect(event) {
    let entry = event.nativeEvent
    if (entry == null) {
      this.setState({ ...this.state, selectedEntry: null })
    } else {
      this.setState({ ...this.state, selectedEntry: JSON.stringify(entry) })
    }

    console.log(event.nativeEvent)
  }

  render() {
    const { chartData, chartLoading } = this.state
    if (chartLoading === true) {
      return (
        <View style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#F5FCFF',
        }}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
    } else {
      return (
        <View style={{ flex: 1, zIndex: 3 }}>
          <View style={{ flex: 1 }}>
            <Echarts
              //width={deviceWidth * 0.93}
              height={this.props.height}
              renderLoading={() => {
                <View style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: '#F5FCFF',
                }}>
                  <ActivityIndicator
                    animating={true}
                    color='#419F'
                    size="large" />
                </View>
              }}
              option={{
                tooltip: {
                  trigger: 'item',
                  formatter: "{a}: <br/>{b}: {c}m³ ({d}%)"
                },

                legend: {
                  orient: 'vertical',
                  x: 'left',
                  textStyle: {
                    fontSize: RFT * 2.6,
                  },
                  itemWidth: RFT * 2.6,
                  itemHeight: RFT * 2.6,
                  //data: chartData
                },
                grid: {
                  left: '10%',
                  right: '10%'
                },

                series: [
                  {
                    name: '生产线',
                    type: 'pie',
                    radius: ['50%', '70%'],
                    //yAxisIndex: 1,
                    data: chartData,
                    avoidLabelOverlap: false,
                    label: {
                      show: false,
                      position: 'center'
                    },
                    emphasis: {
                      label: {
                        show: true,
                        fontSize: RFT * 3.4,
                        fontWeight: 'bold'
                      }
                    },
                    labelLine: {
                      show: false
                    },
                  }
                ],
                color: color,

              }} />
            {}


          </View>
        </View>
      );
    }
  }
}

























/* <CombinedChart
              data={{
                barData: {
                  dataSets: [{
                    values: chartData.Num || [0, 0, 0, 0, 0],
                    label: '数量',
                    config: {
                      drawValues: false,
                      colors: [processColor('#2ecc71')],
                      digits: -1,
                      valueFormatter: "#件",
                      valueTextSize: RFT * 2.5
                    }
                  }],
                  config: {
                    barWidth: 0.4,

                    axisDependency: 'LEFT',
                  }
                },
                lineData: {
                  dataSets: [{
                    values: chartData.Vol || [0, 0, 0, 0, 0],
                    label: '方量',
                    config: {
                      drawValues: true,
                      valueTextSize: RFT * 2.5,
                      drawCircles: true,
                      circleColor: processColor('#f1c40f'),
                      circleRadius: 2,
                      colors: [processColor('#f1c40f')],
                      mode: "HORIZONTAL_BEZIER",
                      lineWidth: 2,
                      axisDependency: "RIGHT",
                    }
                  },],
                },
              }}
              dragDecelerationEnabled={true}
              dragDecelerationFrictionCoef={0.99}
              xAxis={{
                valueFormatter: chartData.Name,
                granularityEnabled: true,
                granularity: 1,
                axisMaximum: chartData.Name.length,
                axisMinimum: -0.5,
                drawAxisLine: false,
                centerAxisLabels: false,
                setDrawGirdLines: false,
                drawGridLines: false,
                gridLineWidth: 1,
                position: "BOTTOM",
                //valueFormatterPattern: "件",
              }}
              yAxis = {this.state.yAxis}
              onSelect={this.handleSelect.bind(this)}
              onChange={(event) => console.log(event.nativeEvent)}
              marker={this.state.marker}
              highlights={this.state.highlights}
              highlightFullBarEnabled={false}
              drawOrder={['LINE', 'BAR']}
              autoScaleMinMaxEnabled={false}
              style={styles.container}
              chartDescription={{ text: ' ' }}
              visibleRange={{
                x: { min: 1, max: 4 }
              }}
              animation={{
                durationX: 2000,
                durationY: 1500,
                easingY: 'EaseInOutQuart'
              }}
              legend={this.state.legend}
              scaleEnabled={false}
             // drawValueAboveBar = {false}
               /> */
