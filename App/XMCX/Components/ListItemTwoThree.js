import React from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, ListItem, BottomSheet } from 'react-native-elements';
import { RFT } from '../../Url/Pixal';

const { height, width } = Dimensions.get('window') //获取宽高

export default class ListItemScan extends React.Component {

  render() {
    const { titlename, title, righttitle, color, bottomDivider, onPress } = this.props;
    console.log("🚀 ~ file: ListItemTwo.js ~ line 11 ~ ListItemScan ~ render ~ bottomDivider", bottomDivider)
    console.log("🚀 ~ file: ListItemTwo.js ~ line 13 ~ ListItemScan ~ render ~ typeof(bottomDivider) == undefined", typeof (bottomDivider) == "undefined")
    let bottomDivider1 = bottomDivider
    if (typeof (bottomDivider) == "undefined") {
      bottomDivider1 = true
    } 
    return (
      <ListItem
        containerStyle={{ paddingVertical: 14, backgroundColor: 'transparent' }}
        leftElement={
          <View style={{ flexDirection: 'row', width: width * 0.88 }}>
            <View style = {{flex: 0.2}}>
              <Badge badgeStyle={{ backgroundColor: color, top: 5, left: -7}} />
            </View>
            <Text style={{ fontWeight: "300", fontSize: RFT * 3.5, flex: 1 }}>{titlename}</Text>
            <Text style={{ fontWeight: "300", fontSize: RFT * 3.5, flex: 1.5, textAlign: 'center' }} >{title}</Text>
            <Text style={{ fontWeight: "300", fontSize: RFT * 3.5, flex: 1.3, textAlign: 'center' }} >{righttitle}</Text>
          </View>}
        onPress={onPress}
        underlayColor={'lightgray'}
        titleStyle={{ fontSize: 14 }}
        bottomDivider={bottomDivider1}
      />
    )
  }
}