import React from 'react';
import { View, ScrollView, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, ListItem, BottomSheet } from 'react-native-elements';
import Url from '../../Url/Url';
import { RFT } from '../../Url/Pixal';
import { styles } from '../Components/XMCXStyles';

const { height, width } = Dimensions.get('window') //获取宽高

export default class ListItemScan extends React.Component {
  render() {
    const { title, rightTitle, focusStyle, containerStyle, rightTitleStyle, isButton, monitorSetup, bottomDivider } = this.props
    console.log("🚀 ~ file: ListItemScan.js ~ line 13 ~ ListItemScan ~ render ~ bottomDivider", bottomDivider)
    let bottomDivider_1 = true
    if (typeof bottomDivider != "undefined") {
      bottomDivider_1 = bottomDivider
    } else {
      bottomDivider_1 = true
    }
    return (
      <ListItem
        {...this.props}
        underlayColor={"lightgray"}
        containerStyle={[isButton ? { paddingVertical: 11 } : { paddingVertical: 18 }, { backgroundColor: 'transparent' }]}
        title={title}
        style={[focusStyle, { paddingHorizontal: 10 }]}
        titleStyle={{ fontSize: 14 }}
        rightTitle={rightTitle}
        rightTitleProps={{ numberOfLines: 1 }}
        rightTitleStyle={[rightTitleStyle, { width: width / 1.5, textAlign: 'right', fontSize: 14 }]}
        bottomDivider = {bottomDivider_1}
      />
    )
  }
}

