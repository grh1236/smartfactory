
import React, { Component } from 'react';
import { View, Text, StyleSheet, processColor, ActivityIndicator } from 'react-native';

import { CombinedChart } from 'react-native-charts-wrapper';
import { Echarts, echarts } from 'react-native-secharts';
import { RFT, deviceWidth } from '../../Url/Pixal';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
    backgroundColor: 'transparent'
  },
  text: {
    margin: 4,
    color: 'gray',
    fontSize: RFT * 2.6
  }
});

let accont = '\t' + '数量'

export default class Combined extends Component {

  constructor() {
    super();
    this.state = {
      legend: {
        enabled: true,
        textSize: RFT * 3,
        form: "SQUARE",
        //formSize: RFT * 4,
        xEntrySpace: 10,
        yEntrySpace: 5,
        wordWrapEnabled: true,
        horizontalAlignment: 'RIGHT',
        verticalAlignment: 'TOP',
      },
      xAxis: {
        valueFormatter: [1, 2, 3, 4, 5, 6, 7],
        granularityEnabled: true,
        granularity: 1,
        axisMaximum: 12,
        axisMinimum: -0.5,
        drawAxisLine: false,
        centerAxisLabels: false,
        setDrawGirdLines: false,
        drawGridLines: false,
        gridLineWidth: 1,
        position: "BOTTOM",
      },
      yAxis: {
        left: {
          granularityEnabled: true,
          granularity: 1,
          axisMinimum: 0,
          // drawAxisLine: false,
          drawGridLines: false,
          //valueFormatterPattern: "件",
          // spaceBottom: 0
        },
        right: {
          granularityEnabled: true,
          granularity: 1,
          axisMinimum: 0,
          //drawAxisLine: false,
          //drawGridLines: false,
          //valueFormatterPattern: "#.#'%'",
          //spaceBottom: 0
        }
      },
      marker: {
        enabled: true,
        markerColor: processColor('#999999'),
        textColor: processColor('white'),
        markerFontSize: 14,
        digits: 4,
      },

      chartData: {},
      chartLoading: true,
    };

  }

  componentDidMount() {
    // in this example, there are line, bar, candle, scatter, bubble in this combined chart.
    // according to MpAndroidChart, the default data sequence is line, bar, scatter, candle, bubble.
    // so 4 should be used as dataIndex to highlight bubble data.

    // if there is only bar, bubble in this combined chart.
    // 1 should be used as dataIndex to highlight bubble data.

    this.setState({ ...this.state, highlights: [{ x: 1, y: 150, dataIndex: 4 }, { x: 2, y: 106, dataIndex: 4 }] })
  }

  setStateLoaing = () => {
    this.setState({ chartLoading: true })
  }

  ReFlash = (propsData) => {
    this.setState({
      chartData: propsData,
      chartLoading: false
    });

  }

  static displayName = 'Combined';

  handleSelect(event) {
    let entry = event.nativeEvent
    if (entry == null) {
      this.setState({ ...this.state, selectedEntry: null })
    } else {
      this.setState({ ...this.state, selectedEntry: JSON.stringify(entry) })
    }

    console.log(event.nativeEvent)
  }

  render() {
    const { chartData, chartLoading } = this.state
    if (chartLoading === true) {
      return (
        <View style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#F5FCFF',
        }}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
    } else {
      return (
        <View style={{ flex: 1 ,zIndex: 3}}>
          <View style={{ flex: 1 }}>
            <Echarts
              //width={deviceWidth * 0.93}
              height={this.props.height}
              renderLoading={() => {
                <View style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: '#F5FCFF',
                }}>
                  <ActivityIndicator
                    animating={true}
                    color='#419F'
                    size="large" />
                </View>
              }}
              option={{
                tooltip: {
                  trigger: 'axis',
                  axisPointer: {
                    // type: 'cross',
                    crossStyle: {
                      color: '#999'
                    }
                  }
                },
                toolbox: {
                  feature: {
                    magicType: { show: true, type: ['line', 'bar',] },
                    dataView: {
                      show: false,
                      title: '每日统计分析',
                      readOnly: true,
                      textareaBorderColor: '#FFF',
                       /* optionToContent: (opt) => {
                        var axisData = opt.xAxis[0].data;
                        var series = opt.series;
                        var table = '<style>table,table tr th, table tr td { border:1px solid #ccc; font-family: Arial; font-size:14px; } table{  border-collapse: collapse;} td{padding: 3px 0px } </style>'
                          + '<table  cellpadding="0";cellspacing="0"; border-collapse= "collapse"; style="width:100%;text-align:center"><tbody>'
                          + '<tr style = " background: #419FFF; color: #fff">'
                          + '<td >名称</td>'
                          + '<td >' + series[0].name + '</td>'
                          + '<td >' + series[1].name + '</td>'
                          + '</tr>';
                        for (var i = 0, l = axisData.length; i < l; i++) {
                          table += '<tr style = "  color: #535c68;">'
                            + '<td >' + axisData[i] + '</td>'
                            + '<td>' + series[0].data[i] + '件' + '</td>'
                            + '<td>' + series[1].data[i] + 'm³' + '</td>'
                            + '</tr>';
                        }
                        table += '</tbody></table>';
                        
                        return table;
                      },  */
                      contentToOption: function (opt) {
                        return opt;
                      },
                    },
                    restore: { show: true },
                  }
                },
                legend: {
                  //data: ['数量', '方量']
                  show: false,
                  left: 'left'
                },
                xAxis: [
                  {
                    show: false,
                    type: 'category',
                    data: chartData.Name,
                    label: {
                      show: false,
                      backgroundColor: '#004E52'
                  },
                    axisPointer: {
                      snap: true,
                      type: 'shadow',
                      handle: {
                        show: false,
                        color: '#004E52'
                      },
                    }
                  }
                ],
                yAxis: [
                  /* {
                    show: false,
                    type: 'value',
                    name: '数量',
                    min: 0,
                    //interval: 1,
                    axisLabel: {
                      formatter: (value) => {
                        if (value >= 1000 && value < 10000) {
                          value = value / 1000 + "k";
                        }
                        return value;
                      }
                    },
                  }, */
                  {
                    show: true,
                    type: 'value',
                    name: 'm³',
                    min: 0,
                    // interval: 1,
                    axisLabel: {
                      formatter: (value) => {
                        if (value >= 1000 && value < 100000) {
                          value = value / 1000 + "k";
                        }
                        return value;
                      }
                    },
                    splitLine: {
                      show: false
                    }
                  }
                ],
                grid: {
                  left: '10%',
                  right: '10%'
                },
                
                series: [
                  /* {
                    name: '数量',
                    type: 'bar',
                    data: chartData.Num,
                    //barMaxWidth: chartData.Num.length <= 6 && '28%'
                    barMaxWidth: chartData.Num.length > 13 && chartData.Num.length < 20 ? chartData.Num.length - 5 : ((chartData.Num.length < 6 ? chartData.Num.length + 10 : chartData.Num.length + 5))
                  }, */
                  {
                    name: '方量',
                    type: 'bar',
                    //yAxisIndex: 1,
                    data: chartData.Vol,
                    barMaxWidth: chartData.Num.length > 10 && chartData.Num.length < 20 ? chartData.Num.length - 5 : ((chartData.Num.length < 6 ? chartData.Num.length + 13 : chartData.Num.length + 5)),
                    itemStyle: {
                      normal: {
                         //每根柱子颜色设置
                         color: function(params) {
                          const colorList = 
                          [
                            '#EE5A24',
                            '#009432',
                            '#FFCF18',
                            '#7158e2',
                            '#17c0eb',
                            '#ffb8b8',
                            '#c56cf0',
                            '#ff3838',
                            '#C4E538',
                            '#FFC312',
                            '#67e6dc',
                            '#ED4C67',
                            '#FDA7DF',
                            '#0652DD',
                            '#833471',
                            '#12CBC4',
                            '#D980FA',
                            '#ff9f1a',
                            '#1289A7',
                            '#A3CB38',
                            '#F79F1F',
                            '#9980FA',
                            '#B53471',
                            '#EE5A24',
                            '#009432',
                            '#fff200',
                            '#7158e2',
                            '#ff9f1a',
                            '#17c0eb',
                            '#ff3838',
                            '#ffb8b8',
                            '#c56cf0',
                            '#C4E538',
                            '#FFC312',
                            '#67e6dc',
                            '#ED4C67',
                            '#FDA7DF',
                            '#0652DD',
                            '#833471',
                            '#12CBC4',
                            '#D980FA',
                            '#1289A7',
                            '#A3CB38',
                            '#F79F1F',
                            '#9980FA',
                            '#B53471',
                            '#EE5A24',
                            '#009432',
                            '#fff200',
                            '#7158e2',
                            '#ff9f1a',
                            '#17c0eb',
                            '#ff3838',
                            '#ffb8b8',
                            '#c56cf0',
                            '#C4E538',
                            '#FFC312',
                            '#67e6dc',
                            '#ED4C67',
                            '#FDA7DF',
                            '#0652DD',
                            '#833471',
                            '#12CBC4',
                            '#D980FA',
                            '#1289A7',
                            '#A3CB38',
                            '#F79F1F',
                            '#9980FA',
                            '#B53471',
                          ] 
                          return colorList[params.dataIndex];
                      }
                    }
                  }
                  }
                ],
                //color: ['#2ecc71', '#f1c40f'],

              }} />
            {}


          </View>
        </View>
      );
    }
  }
}

























/* <CombinedChart
              data={{
                barData: {
                  dataSets: [{
                    values: chartData.Num || [0, 0, 0, 0, 0],
                    label: '数量',
                    config: {
                      drawValues: false,
                      colors: [processColor('#2ecc71')],
                      digits: -1,
                      valueFormatter: "#件",
                      valueTextSize: RFT * 2.5
                    }
                  }],
                  config: {
                    barWidth: 0.4,
                    
                    axisDependency: 'LEFT',
                  }
                },
                lineData: {
                  dataSets: [{
                    values: chartData.Vol || [0, 0, 0, 0, 0],
                    label: '方量',
                    config: {
                      drawValues: true,
                      valueTextSize: RFT * 2.5,
                      drawCircles: true,
                      circleColor: processColor('#f1c40f'),
                      circleRadius: 2,
                      colors: [processColor('#f1c40f')],
                      mode: "HORIZONTAL_BEZIER",
                      lineWidth: 2,
                      axisDependency: "RIGHT",
                    }
                  },],
                },
              }}
              dragDecelerationEnabled={true}
              dragDecelerationFrictionCoef={0.99}
              xAxis={{
                valueFormatter: chartData.Name,
                granularityEnabled: true,
                granularity: 1,
                axisMaximum: chartData.Name.length,
                axisMinimum: -0.5,
                drawAxisLine: false,
                centerAxisLabels: false,
                setDrawGirdLines: false,
                drawGridLines: false,
                gridLineWidth: 1,
                position: "BOTTOM",
                //valueFormatterPattern: "件",
              }}
              yAxis = {this.state.yAxis}
              onSelect={this.handleSelect.bind(this)}
              onChange={(event) => console.log(event.nativeEvent)}
              marker={this.state.marker}
              highlights={this.state.highlights}
              highlightFullBarEnabled={false}
              drawOrder={['LINE', 'BAR']}
              autoScaleMinMaxEnabled={false}
              style={styles.container}
              chartDescription={{ text: ' ' }}
              visibleRange={{
                x: { min: 1, max: 4 }
              }}
              animation={{
                durationX: 2000,
                durationY: 1500,
                easingY: 'EaseInOutQuart'
              }}
              legend={this.state.legend}
              scaleEnabled={false}
             // drawValueAboveBar = {false}
               /> */
