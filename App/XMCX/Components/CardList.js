import React from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Dimensions, ActivityIndicator, Platform, Alert } from 'react-native';
import { Button, Text, SearchBar, Icon, Card, Input, ButtonGroup, Image, Badge, ListItem, BottomSheet, CheckBox } from 'react-native-elements';
import Url from '../../Url/Url';
import { RFT } from '../../Url/Pixal';

const { height, width } = Dimensions.get('window') //获取宽高


export default class CardList extends React.Component {
  constructor() {
    super();
    this.state = {

    }
  }

  render() {
    const { compData } = this.props
    let result =
      console.log("🚀 ~ file: CardList.js ~ line 15 ~ CardList ~ render ~ compData", compData)
    console.log("🚀 ~ file: CardList.js ~ line 16 ~ CardList ~ render ~ compData.result", compData.result)
    if (compData.result instanceof Array) {
      result = compData.result[0]
    } else {
      result = compData.result
    }
    return (
      <View>
        <Card containerStyle={styles.card_containerStyle}>
          <ListItem
            containerStyle={styles.list_container_style}
            title='项目名称'
            rightTitle={result.projectName || result.ProjectName}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='构件类型'
            rightTitle={result.compTypeName || result.CompTypeName}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='设计型号'
            rightTitle={result.designType || result.DesignType}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          <ListItem
            containerStyle={styles.list_container_style}
            title='楼号(楼层)'
            rightTitle={(result.floorNoName || result.FloorNoName) + '(' + (result.floorName || result.FloorName) + ')'}
            titleStyle={styles.title}
            rightTitleStyle={styles.rightTitle}
          />
          
        </Card>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  card_containerStyle: {
    borderRadius: 10,
    shadowOpacity: 0,
    backgroundColor: '#f8f8f8',
    borderWidth: 0,
    paddingVertical: 13,
    elevation: 0
  },
  title: {
    fontSize: 13,
    color: '#999'
  },
  rightTitle: {
    fontSize: 13,
    textAlign: 'right',
    lineHeight: 14,
    flexWrap: 'wrap',
    width: width / 2,
    color: '#333'
  },
  list_container_style: { marginVertical: 0, paddingVertical: 9, backgroundColor: 'transparent' },

})