import React from 'react';
import {
  StyleSheet,
  Text,
  View, 
  processColor,
  ActivityIndicator
} from 'react-native';
import { BarChart } from 'react-native-charts-wrapper';
import { RFT, deviceWidth } from '../../Url/Pixal';

class StackedBarChartScreen extends React.Component {

  constructor() {
    super();
    this.state = {
      legend: {
        enabled: true,
        // textSize: 14,
        form: "SQUARE",
        //formSize: 14,
        xEntrySpace: 10,
        yEntrySpace: 5,
        wordWrapEnabled: true,
        horizontalAlignment: 'RIGHT',
        verticalAlignment: 'TOP'
      },
      data: {
        dataSets: [{
          values: [5, 40, 77, 81, 43],
          label: 'Company A',
          config: {
            drawValues: false,
            colors: [processColor('red')],
          }
        }, {
          values: [40, 5, 50, 23, 79],
          label: 'Company B',
          config: {
            drawValues: false,
            colors: [processColor('blue')],
          }
        }, {
          values: [10, 55, 35, 90, 82],
          label: 'Company C',
          config: {
            drawValues: false,
            colors: [processColor('green')],
          }
        }],
        config: {
          barWidth: 0.2,
          group: {
            fromX: 0,
            groupSpace: 0.1,
            barSpace: 0.1,
          },
        }
      },
      xAxis: {
        valueFormatter: ['1990', '1991', '1992', '1993', '1994'],
        granularityEnabled: true,
        granularity: 1,
        axisMaximum: 5,
        axisMinimum: 0,
        centerAxisLabels: true
      },
      yAxis: {
        left: {
          enabled: true,
          //drawLimitLinesBehindData: true
        },
        right: {
          enabled: false,

        }
      },
      marker: {
        enabled: true,
        digits: 4,
        markerColor: processColor('#999999'),
        textColor: processColor('white'),
        markerFontSize: 16,
      },
      chartData: {},
      chartXAxis: {},
      chartLoading: true
    };
  }

  setStateLoaing = () => {
    this.setState({
      chartLoading: true
    });
  }

  ReFlash = (chartData, chartXAxis) => {
    this.setState({
      chartData: chartData,
      chartXAxis: chartXAxis,
      chartLoading: false
    });
  }

  componentDidMount() {
    
  }

  handleSelect(event) {
    let entry = event.nativeEvent
    if (entry == null) {
      this.setState({ ...this.state, selectedEntry: null })
    } else {
      this.setState({ ...this.state, selectedEntry: JSON.stringify(entry) })
    }

    console.log(event.nativeEvent)
  }

  render() {
    const { chartData, chartLoading, chartXAxis } = this.state
    if (chartLoading === true) {
      return (
        <View style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#F5FCFF',
        }}>
          <ActivityIndicator
            animating={true}
            color='#419FFF'
            size="large" />
        </View>
      )
    } else {
      return (
        <View style={{ flex: 1 }}>
          <View style={{ flexDirection: 'row', }}>
            <Text style={[styles.text, { textAlign: 'justify' }]}>单位：m³</Text>
          </View>
          <View style={{ flex: 1 }}>
            <BarChart
              style={styles.container}
              xAxis={chartXAxis}
              yAxis={this.state.yAxis}
              data={chartData}
              legend={this.state.legend}
              drawValueAboveBar={false}
              onSelect={this.handleSelect.bind(this)}
              onChange={(event) => console.log(event.nativeEvent)}
              highlights={this.state.highlights}
              marker={this.state.marker}
              scaleEnabled = {false}
              autoScaleMinMaxEnabled={true}
              animation={{
                durationX: 2000,
                durationY: 1500,
                easingY: 'EaseInOutQuart'
              }}
              visibleRange={{
                x: { min: 4, max: 4 }
              }}
              chartDescription={{ text: '' }}
            />
          </View>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
    backgroundColor: 'transparent'
  },
  text: {
    marginLeft: 4,
    color: 'gray',
    fontSize: RFT * 2
  }
});


export default StackedBarChartScreen;
