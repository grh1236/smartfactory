import React, { Component } from 'react';
import { View, StyleSheet, ListView } from 'react-native';
import { ButtonGroup, Button, Text } from 'react-native-elements'
import Url from '../Url/Url'
import CardCommon from '../ChildView/CardSH'


//按钮组定义，整个组有四个按钮
const component1 = () => <Text style={styles.txt}>今日</Text>;
const component2 = () => <Text style={styles.txt}>昨日</Text>
const component3 = () => <Text style={styles.txt}>近7天</Text>
const component4 = () => <Text style={styles.txt}>近30天</Text>

export default class ButtonGroupMain extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //按钮组传参，还没明白
      selectedIndex: [],
      testDataToday: [{ username: ' ', fangliang: '0m³', jianshu: '0件', color: 'gray' },
      { username: ' ', fangliang: '0m³', jianshu: '0件', color: 'gray' },
      { username: ' ', fangliang: '0m³', jianshu: '0件', color: 'gray' }],
      testDataYesterday: [{ username: ' ', fangliang: '0m³', jianshu: '0件', color: 'gray' },
      { username: ' ', fangliang: '0m³', jianshu: '0件', color: 'gray' },
      { username: ' ', fangliang: '0m³', jianshu: '0件', color: 'gray' }],
      testDataSevenDay: [{ username: ' ', fangliang: '0m³', jianshu: '0件', color: 'gray' },
      { username: ' ', fangliang: '0m³', jianshu: '0件', color: 'gray' },
      { username: ' ', fangliang: '0m³', jianshu: '0件', color: 'gray' }],
      testDataMonth: [{ username: ' ', fangliang: '0m³', jianshu: '0件', color: 'gray' },
      { username: ' ', fangliang: '0m³', jianshu: '0件', color: 'gray' },
      { username: ' ', fangliang: '0m³', jianshu: '0件', color: 'gray' }],
    };
    this.updateIndex = this.updateIndex.bind(this);
  }

  updateIndex(selectedIndex) {
    this.setState({ selectedIndex })
    if (selectedIndex === 0) {
        const url = Url.url + Url.factory + Url.Today;
        fetch(url).then(res => {
          return res.json()
        }).then(resData => {
          this.setState({
            resData: resData,
          });
        }).catch((error) => {
          console.log(error);
        });
      this.refs.testDataReflash.cardReflash(this.props.testDataToday)
    } else if (selectedIndex == 1) {
      const url = Url.url + Url.factory + Url.Yesterday;
        fetch(url).then(res => {
          return res.json()
        }).then(resData => {
          this.setState({
            resData: resData,
          });
        }).catch((error) => {
          console.log(error);
        });
      this.refs.testDataReflash.cardReflash(this.props.testDataYesterday)
    } else if (selectedIndex == 2) {
      this.refs.testDataReflash.cardReflash(this.props.testDataSevenDay)
    } else if (selectedIndex == 3) {
      this.refs.testDataReflash.cardReflash(this.props.testDataMonth)
    }

  }
  _onPress(selectedIndex) {

  }

  render() {
    //按钮组元素
    const buttons = [{ element: component1 }, { element: component2 }, { element: component3 }, { element: component4 }]
    const { selectedIndex } = this.state;
    return (
      
      <View>
        <ButtonGroup
          onPress={this.updateIndex}
          selectedIndex={this.state.selectedIndex}
          buttons={buttons}
          containerStyle={{ height: 40, borderRadius: 20 }}
          selectedButtonStyle={{ backgroundColor: '#419FFF', }}
        />

        <CardCommon
          ref='testDataReflash'
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  txt: {
    color: 'black',
  },
});


