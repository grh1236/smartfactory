import React, { Component } from 'react';
import { View, TouchableNativeFeedback, ListView, Dimensions } from 'react-native';
import { Card, Text } from 'react-native-elements';

const {height, width} = Dimensions.get('screen')

//卡片
export default class CardToday extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //初始显示数据，应该从接口返回今天的数据
      testData: {
        proNumber: 0,
        proVolume: 0,
        storageNumber: 0,
        storageVolume: 0,
        outNumber: 0,
        outVolume: 0
      },
    }

  }

  //卡片更新
  cardReflash(newTestData) {
    //更新卡片中的数据
    this.setState({
      //newTestData来自按钮选择参数传值
      testData: newTestData
    })
  }
  //卡片子函数
  renderCard() {

    const testData = this.state.testData;
    
    return (
      <View style={{ flexDirection: 'row', justifyContent: 'space-around', flex: 1 }}>
        <Card title='任务单'
          titleStyle={{ color: 'white' }}
          containerStyle={{ backgroundColor: '#32D185', borderRadius: 20, width: 100 }} >
          <Text style={{ color: 'white', fontSize: 18, fontWeight: '900' }}>{testData.proVolume + 'm³'} </Text>
          <Text style={{ color: 'white' }}>{testData.proNumber + '件'}</Text>
        </Card>
        <Card title= '生产'
          titleStyle={{ color: 'white' }}
          containerStyle={{ backgroundColor: '#FF9D18', borderRadius: 20, width: 100 }} >
          <Text style={{ color: 'white', fontSize: 18, fontWeight: '900' }} >{testData.storageVolume + 'm³'}</Text>
          <Text style={{ color: 'white' }}>{testData.storageNumber + '件'}</Text>
        </Card>
        <Card title= '脱模待检'
          titleStyle={{ color: 'white' }}
          containerStyle={{ backgroundColor: '#41CCFF', borderRadius: 20, width: 100 }} >
          <Text style={{ color: 'white', fontSize: 18, fontWeight: '900' }} >{testData.outVolume + 'm³'}</Text>
          <Text style={{ color: 'white' }}>{testData.outNumber + '件'}</Text>
        </Card>
      </View>
    )
  }


  render() {
    return (
      <View>
        {this.renderCard()}
      </View>
    );
  }
}

