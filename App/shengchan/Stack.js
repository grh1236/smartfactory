import React from 'react'
import { View } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import ShengChanView from './View';
import ChildOne from '../ChildrenPages/ChildOneFlat'
import ChildTwo from '../ChildrenPages/ChildrenPageTwo'

const ShengChanStack = createStackNavigator({
  Route: {
    screen: ShengChanView,
    navigationOptions: {
      headerTitle: '生产管理', headerTintColor: 'black'
    }
  },
  ChildOne: {
    screen: ChildOne,
    navigationOptions: {
      /*headerTitle: '任务单',*/ headerTintColor: 'black',
    } 
  },
  ChildTwo: {
    screen: ChildTwo,
    navigationOptions: {
      headerTintColor: 'black'
    }
  }
},
  {
    defaultNavigationOptions: {
      headerStyle: {//设置导航条的样式。背景色，宽高等
        backgroundColor: 'white'
      },
      headerTintColor: '#eee',

      headerTitleStyle: {//设置导航条文字样式
        fontWeight: 'bold'
      }
    }
  });

export default ShengChanStack;