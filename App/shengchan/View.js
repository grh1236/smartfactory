import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, Dimensions, ScrollView, RefreshControl } from 'react-native';
import { Icon } from 'react-native-elements';
import ButtonGroup from '../CommonComponents/ButtonGroupTest'
import Url from '../Url/Url';
import ActionButton from 'react-native-action-button';

var myDate = new Date();
var year = myDate.getFullYear();    //获取完整的年份(4位,1970-????)
var month = (Array(2).join(0) + (myDate.getMonth() + 1)).slice(-2);       //获取当前月份(1-12)
var day = (Array(2).join(0) + myDate.getDate()).slice(-2);        //获取当前日(1-31)
//获取完整年月日
var newDay = year + '-' + month + '-' + day;
var YesterDay = year + '-' + month + '-' + (day - 1);

const { height, width } = Dimensions.get('screen');

const defaultReloadIcon = () => ({
  //type: '',
  name: 'reload1',
  color: 'white',
});

const Urlnew = Url.url + Url.Produce + Url.Fid


const CardSource = [
  { img: require('../viewImg/DaiChanchi.png'), text: '待产池', title: '待产池', sonTitle: '构件信息', url: Urlnew + 'GetWaitingPoolComp', sonUrl: '' },
  { img: require('../viewImg/RiShengChanJiLu.png'), text: '日生产记录', title: '日生产记录', sonTitle: '构件生产信息', url: Url.url + Url.Produce + Url.Fid + 'GetProduceDailyRecord/' + newDay, sonUrl: '' },
  { img: require('../viewImg/TuoMoDaiJian.png'), text: '脱模待检', title: '脱模待检', sonTitle: '脱模待检', url: Urlnew + 'GetProduceRelease', sonUrl: '' },
  { img: require('../viewImg/YiShengChanGouJian.png'), text: '已生产构件明细', title: '已生产构件明细', sonTitle: '构件列表', url: Urlnew + 'GetProduceProject', sonUrl: Urlnew + 'GetProjectProducedComponents/' },
  { img: require('../viewImg/RenWuDan.png'), text: '任务单', title: '任务单', sonTitle: '任务单', url: Urlnew + 'GetProduceTask', sonUrl: 'http://47.94.205.217:910/api/Produce/683BF474B82A4D33890299608C885330/GetProduceTaskCompDetails' },
]

export default class ShengchanScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      refreshing: false,
      title: {
        first: '任务单',
        second: '生产',
        third: '脱模待检'
      },
      dataTest: [
        { bianhao: '0070-A-YBS-13-a-005', xiangmuming: '同心花苑一期', leixing: '叠合板', weizhi: '5# (7F)', riqi: '计划生产日期： 2019-1-4' },
        { bianhao: '0070-A-YBS-13-a-005', xiangmuming: '同心花苑一期', leixing: '叠合板', weizhi: '5# (7F)', riqi: '计划生产日期： 2019-1-4' },
        { bianhao: '0070-A-YBS-13-a-005', xiangmuming: '同心花苑一期', leixing: '叠合板', weizhi: '5# (7F)', riqi: '计划生产日期： 2019-1-4' },
        { bianhao: '0070-A-YBS-13-a-005', xiangmuming: '同心花苑一期', leixing: '叠合板', weizhi: '5# (7F)', riqi: '计划生产日期： 2019-1-4' },
        { bianhao: '0070-A-YBS-13-a-005', xiangmuming: '同心花苑一期', leixing: '叠合板', weizhi: '5# (7F)', riqi: '计划生产日期： 2019-1-4' }],
      resDataToday: {
        firstNumber: 0,
        firstVolume: 0,
        secondNumber: 0,
        secondVolume: 0,
        thirdNumber: 0,
        thirdVolume: 0
      }, resDataYesterday: {
        firstNumber: 0,
        firstVolume: 0,
        secondNumber: 0,
        secondVolume: 0,
        thirdNumber: 0,
        thirdVolume: 0
      }, resDataWeek: {
        firstNumber: 0,
        firstVolume: 0,
        secondNumber: 0,
        secondVolume: 0,
        thirdNumber: 0,
        thirdVolume: 0
      }, resDataMonth: {
        firstNumber: 0,
        firstVolume: 0,
        secondNumber: 0,
        secondVolume: 0,
        thirdNumber: 0,
        thirdVolume: 0
      }
    };
    this.requestDataToday = this.requestDataToday.bind(this);
    this.requestDataYesterday = this.requestDataYesterday.bind(this);
    this.requestDataWeek = this.requestDataWeek.bind(this);
    this.requestDataMonth = this.requestDataMonth.bind(this);
  }

  Date = () => {
    if (month < 10) {
      month = '0' + month
      if (day < 10) {
        day = '0' + day
      }
    }
  }

  requestDataToday = () => {
    const url = Url.url + Url.factory + Url.Fid + Url.Today;
    console.log(url);
    fetch(url).then(res => {
      return res.json()
    }).then(resData => {
      this.setState({
        resDataToday: resData,
      });
      console.log(this.state.resDataToday)
      const resDataToday = JSON.parse(JSON.stringify(this.state.resDataToday)
        .replace(/taskNumber/g, "firstNumber")
        .replace(/taskVolume/g, "firstVolume")
        .replace(/proNumber/g, "secondNumber")
        .replace(/proVolume/g, "secondVolume")
        .replace(/demouldNumber/g, "thirdNumber")
        .replace(/demouldVolume/g, "thirdVolume"));
      this.setState({
        resDataToday: resDataToday,
        refreshing: false
      })
      console.log(this.state.resDataToday)
    }).catch((error) => {
      console.log(error);
    });
  }

  requestDataYesterday = () => {
    const url = Url.url + Url.factory + Url.Fid + Url.Yesterday;
    fetch(url).then(resYesterday => {
      return resYesterday.json()
    }).then(resDataYesterday => {
      this.setState({
        resDataYesterday: resDataYesterday,
        refreshing: false
      });
      const resDataYesterdaynew = JSON.parse(JSON.stringify(this.state.resDataYesterday)
        .replace(/taskNumber/g, "firstNumber")
        .replace(/taskVolume/g, "firstVolume")
        .replace(/proNumber/g, "secondNumber")
        .replace(/proVolume/g, "secondVolume")
        .replace(/demouldNumber/g, "thirdNumber")
        .replace(/demouldVolume/g, "thirdVolume"));
      this.setState({
        resDataYesterday: resDataYesterdaynew
      })
      console.log(this.state.resDataYesterday)
    }).catch((error) => {
      console.log(error);
    });
  }

  requestDataWeek = () => {
    const url = Url.url + Url.factory + Url.Fid + Url.Week;
    console.log(url)
    fetch(url).then(resWeek => {
      return resWeek.json()
    }).then(resDataWeek => {
      this.setState({
        resDataWeek: resDataWeek,
        refreshing: false
      });
      const resDataWeeknew = JSON.parse(JSON.stringify(this.state.resDataWeek)
        .replace(/taskNumber/g, "firstNumber")
        .replace(/taskVolume/g, "firstVolume")
        .replace(/proNumber/g, "secondNumber")
        .replace(/proVolume/g, "secondVolume")
        .replace(/demouldNumber/g, "thirdNumber")
        .replace(/demouldVolume/g, "thirdVolume"));
      this.setState({
        resDataWeek: resDataWeeknew
      })
      console.log(this.state.resDataWeek)
    }).catch((error) => {
      console.log(error);
    });
  }

  requestDataMonth = () => {
    const url = Url.url + Url.factory + Url.Fid + Url.Month;
    fetch(url).then(resMonth => {
      return resMonth.json()
    }).then(resDataMonth => {
      this.setState({
        resDataMonth: resDataMonth,
        refreshing: false
      });
      const resDataMonthnew = JSON.parse(JSON.stringify(this.state.resDataMonth)
        .replace(/taskNumber/g, "firstNumber")
        .replace(/taskVolume/g, "firstVolume")
        .replace(/proNumber/g, "secondNumber")
        .replace(/proVolume/g, "secondVolume")
        .replace(/demouldNumber/g, "thirdNumber")
        .replace(/demouldVolume/g, "thirdVolume"));
      this.setState({
        resDataMonth: resDataMonthnew
      })
      console.log(this.state.resDataMonth)
    }).catch((error) => {
      console.log(error);
    });
  }

 componentDidMount() {
    this.requestDataToday();
    this.requestDataYesterday();
    this.requestDataWeek();
    this.requestDataMonth();
  }

  _onRefresh = () => {
    this.setState({refreshing: true});
    this.requestDataToday();
    this.requestDataYesterday();
    this.requestDataWeek();
    this.requestDataMonth();
  }

  render() {
    const { testDataToday, testDataYesterday, resDataWeek, testDataMonth } = this.state;
    return (
      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
          />
        } >
        <ButtonGroup
          onPress={
            () => {
              this.requestDataToday();
              this.requestDataYesterday();
              this.requestDataWeek();
              this.requestDataMonth();
            }
          }
          testDataToday={this.state.resDataToday}
          testDataYesterday={this.state.resDataYesterday}
          testDataSevenDay={this.state.resDataWeek}
          testDataMonth={this.state.resDataMonth}
          title={this.state.title} />

        <View style={styles.ViewCard}>
          {
            CardSource.map((u, i) => {
              return (
                <View style={{ marginVertical: 20 }}>
                  <TouchableOpacity
                    onPress={() => {
                      this.props.navigation.navigate('ChildOne', {
                        title: u.title,
                        sonTitle: u.sonTitle,
                        data: this.state.dataTest,
                        url: u.url,
                        sonUrl: u.sonUrl
                      })
                      console.log(u.sonTitle)
                    }}>
                    <Image
                      source={u.img}
                      style={styles.img}
                    ></Image>
                  </TouchableOpacity>
                  <Text style={{ color: 'black', textAlign: 'center' }}> {u.text} </Text>
                </View>
              );
            })
          }
        </View>
        <View style={{ height: 80 }}>
          <ActionButton
            buttonColor='#00CCFF'
            renderIcon={() => {
              return (
                <Icon
                  name='refresh'
                  color='white' />
              )
            }}
            onPress={this.refresh} />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  ViewCard: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    marginTop: 20,
    justifyContent: 'flex-start',
    flex: 1
  },
  img: {
    height: width / 3,
    width: width / 3
  }
})