import React from 'react'
import { View, Text, Dimensions, StatusBar, DeviceEventEmitter } from 'react-native'
import { WebView } from 'react-native-webview';
import { ScrollView } from 'react-native-gesture-handler';

let url = ''

const { height, width } = Dimensions.get('window')

export default class webProcess extends React.Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title'),
    };
  };

  /*  componentDidMount() {
     //testName:通知的名称    param:接收到的消息（传参）
     DeviceEventEmitter.addListener('reflash',url => {
     //  自定义页面刷新动作
     url = url
     });
 }
 
 componentWillUnmount(){
   // 移除通知
   DeviceEventEmitter.remove();
 } */

  render() {
    StatusBar.setTranslucent(false)
    //let url =  'http://121.36.55.207:9100/appviews/flowcenter/index.html?s_c_bizrangeid=74e1cbc6-2cfc-4d6f-bcd1-739d28e4c74d&treeno=0181002001&s_employeeid=04c41369-8737-4151-84b6-5da37d32196e'
    let url = this.props.navigation.getParam('url')
    console.log("render -> url", url)
    return (
      <ScrollView
        scrollEnabled={false}
      /* refreshControl = {() => {
          
      }} */
      >
        <View style={{ height: height * 0.9 }}>
          <WebView
            source={{ uri: url }}
            incognito={true}
          ></WebView>
        </View>
      </ScrollView>
    )
  }
}