var myDate = new Date();
var year = myDate.getFullYear();    //获取完整的年份(4位,1970-????)
var month = (Array(2).join(0) + (myDate.getMonth() + 1)).slice(-2);       //获取当前月份(01-12)
var day = (Array(2).join(0) + myDate.getDate()).slice(-2);        //获取当前日(01-31)
//获取完整年月日
var ToDay = year + '-' + month + '-' + day;
var YesterDay = year + '-' + month + '-' + (Array(2).join(0) + (day - 1)).slice(-2);
var BeforeYesterDay = year + '-' + month + '-' + (Array(2).join(0) + (day - 2)).slice(-2);

export {ToDay, YesterDay, BeforeYesterDay}