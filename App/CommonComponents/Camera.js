import React, { PureComponent } from 'react';
import {
  StatusBar,
  StyleSheet,
  FlatList,
  Modal,
  Vibration,
  SectionList,
  TouchableOpacity,
  Animated,
  PermissionsAndroid,
  default as Easing,
  ImageBackground,
  View
} from 'react-native';
import { withNavigation } from 'react-navigation';
import { RNCamera } from 'react-native-camera';
import { Button, Text } from 'react-native-elements';

class QRcode extends PureComponent {
  constructor() {
    super()
    this.state = {
      //中间横线动画初始值
      moveAnim: new Animated.Value(0),
      isScan: false
    }
  }

  componentDidMount() {
    this.startAnimation();
    console.log('QRCode')
  }

  /** 扫描框动画*/
  startAnimation = () => {
    this.state.moveAnim.setValue(0);
    Animated.timing(
      this.state.moveAnim,
      {
        toValue: -200,
        duration: 500,
        easing: Easing.linear
      }
    ).start(() => this.startAnimation());

  }

  onBarCodeRead = (result) => {
    this.setState({
      isScan: true
    }, () => {
      const { navigation } = this.props;
      let mainpage = navigation.getParam('mainpage') || 'MainTab'
      console.log('onBarCodeRead')
      const { data, type, bounds } = result; //只要拿到data就可以了
      console.log('type', type)
      console.log('bounds', bounds)
      //扫码后的操作
      console.log(data)
      this.props.navigation.setParams({
        title: '构件信息',
        url: data,
        mainpage: mainpage
      })
      // alert(data)
      this.props.navigation.replace('WebMain', {
        title: '构件信息',
        url: data,
        mainpage: mainpage
      })
    })


  };

  takePicture = async () => {
    if (this.camera) {
      const options = { quality: 0.5, base64: true };
      const data = await this.camera.takePictureAsync(options);
      console.log(data.uri);
    }
  };

  render() {
    const { navigation } = this.props;
    const type = navigation.getParam('type') || '';
    return (
      <View style={styles.container}>
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          autoFocus={RNCamera.Constants.AutoFocus.on}/*自动对焦*/
          style={[styles.preview,]}

          type={RNCamera.Constants.Type.back}/*切换前后摄像头 front前back后*/
          flashMode={RNCamera.Constants.FlashMode.off}/*相机闪光模式*/
          onBarCodeRead={(result) => {
            console.log('QR1')
            // if (!this.state.isScan) {
            this.onBarCodeRead(result)
            //}
          }}
          onCameraReady={() => {
            console.log('ready')
          }}
        >
          <View style={styles.rectangleContainer}>
            <View style={styles.rectangle} />
            <Animated.View style={[
              styles.border,
              { transform: [{ translateY: this.state.moveAnim }] }]} />
            <Text style={styles.rectangleText}>将二维码放入框内，即可自动扫描</Text>
          </View>
        </RNCamera>
      </View>
    )
  }


}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    width: '100%',
    height: '100%'
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  rectangleContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  rectangle: {
    height: 200,
    width: 200,
    borderWidth: 1,
    borderColor: '#419FFF',
    backgroundColor: 'transparent'
  },
  rectangleText: {
    flex: 0,
    color: '#fff',
    marginTop: 10
  },
  border: {
    flex: 0,
    width: 200,
    height: 2,
    backgroundColor: '#419FFF',
  }
});

export default withNavigation(QRcode)