import React from 'react'
import { View, Text, Dimensions } from 'react-native'
import { Icon } from "react-native-elements";
import { WebView } from 'react-native-webview';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { RFT } from '../Url/Pixal';

const { height, width } = Dimensions.get('window')

export default class webView extends React.Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title'),
      headerLeft: (
        <TouchableOpacity onPress={() => { navigation.replace(navigation.getParam('mainpage')) }}>
          <View style={{ flexDirection: 'row' }}> 
            <Icon name='arrow-back' color='gary' />
            <Text style={{ fontSize: 18, top: RFT }}>返回</Text>
          </View>
        </TouchableOpacity>
      )
    };
  };
  render() {
    let url =  this.props.navigation.getParam('url')  //"http://smart.pkpm.cn:9100/PCIS/Production/ShowCompInfo.aspx?compid=E45E65EB439549C5842DAFBCE55EF63B"
    //let url =  "http://smart.pkpm.cn:9100/PCIS/Production/ShowCompInfo.aspx?compid=E45E65EB439549C5842DAFBCE55EF63B"
    
    console.log("render -> url", url)
    return (
      <ScrollView scrollEnabled = {false} >
        <View style = {{height: height* 0.92}}>
          <WebView
            source={{ uri: url }}
          ></WebView>
        </View>
      </ScrollView>
    )
  }
}