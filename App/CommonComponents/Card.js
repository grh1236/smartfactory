import React, { Component } from 'react';
import { View, TouchableNativeFeedback, Dimensions, StyleSheet } from 'react-native';
import { Card, Text } from 'react-native-elements';
import Url from '../Url/Url'

const { height, width } = Dimensions.get('window')

//卡片
export default class CardToday extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //初始显示数据，应该从接口返回今天的数据
      testData: {
        firstNumber: 0,
        firstVolume: 0,
        secondNumber: 0,
        secondVolume: 0,
        thirdNumber: 0,
        thirdVolume: 0,
        fourthNumber: 0,
        fourthVolume: 0,
      }
  }
  this.baseData = this.baseData.bind(this)
}

  componentDidMount() {
    //this.baseData();
  }

  CardFourth = (testData, title) => {
    if (this.props.title.fourth) {
      return (
        <View style={{ flexDirection: 'row', justifyContent: 'space-around', flex: 1 }}>
          <Card title={this.props.title.fourth}
            titleStyle={{ color: 'white' }}
            containerStyle={[{ backgroundColor: 'gray' }, styles.card]} >
            <Text style={{ color: 'white', fontSize: 18, fontWeight: '900' }} >{testData.fourthVolume + 'm³'}</Text>
            <Text style={{ color: 'white' }}>{testData.fourthNumber + '件'}</Text>
          </Card>
          <View style={styles.card} />
          <View style={styles.card} />
        </View>
      )
    }
  }

  baseData = () => {
    console.log(this.props.testDataToday)
    this.setState({
      testData: this.props.testDataToday
    })
  }

  //卡片更新
  cardReflash(newTestData) {
     console.log(newTestData)
    //更新卡片中的数据
    this.setState({
      //newTestData来自按钮选择参数传值
      testData: newTestData
    })
  }

  //卡片子函数
  renderCard() {
    const { title } = this.props;
    const { testData } = this.state
    console.log(testData)
    return (
      <View style={{ justifyContent: 'flex-start', flex: 1 }}>
        <View style={{ flexDirection: 'row', justifyContent: 'space-around', flex: 1 }}>
          <Card title={this.props.title.first}
            titleStyle={{ color: 'white' }}
            containerStyle={[{ backgroundColor: '#00CCFF' }, styles.card]} >
            <Text style={styles.text1}>{testData.firstVolume + 'm³'} </Text>
            <Text style={styles.text2}>{testData.firstNumber + '件'}</Text>
          </Card>
          <Card title={this.props.title.second}
            titleStyle={{ color: 'white' }}
            containerStyle={[{ backgroundColor: '#FF9D18' }, styles.card]} >
            <Text style={styles.text1}>{testData.secondVolume + 'm³'}</Text>
            <Text style={styles.text2}>{testData.secondNumber + '件'}</Text>
          </Card>
          <Card title={this.props.title.third}
            titleStyle={{ color: 'white' }}
            containerStyle={[{ backgroundColor: '#8579E8' }, styles.card]} >
            <Text style={styles.text1}>{testData.thirdVolume + 'm³'}</Text>
            <Text style={styles.text2}>{testData.thirdNumber + '件'}</Text>
          </Card>
        </View>
        {this.CardFourth(testData, title)}
      </View>
    )
  }


  render() {
    return (
      <View>
        {this.renderCard()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  card: {
    borderRadius: 20,
    width: (width - 20) / 3,
    height: (width / 3) + 20 ,
  },
  text1: {
    color: 'white', 
    fontSize: 16, 
    fontWeight: '800',
  }, 
  text2: {
    fontSize: 14, 
    color: 'white',
  }
})